<p align="center"><img src="http://ciudadania-express.com/wp-content/uploads/2017/04/LOGO-DIF.jpg"  height="150" width="auto"></p>

## Iniciar el Proyecto

- git clone http://192.168.0.149:32769/new-dif/intradif.git
- composer install || composer update
- cp .env.example .env
- php artisan k:g
- git checkout **develop** 


## Reglas

- No tocar la rama **master**
- Todos los cambios van a la rama **develop**
- ** Framework Laravel - Plantilla CSS AdminLTE2, Core UI, Limitless **
- Push diario por usuario obligatorio
- Nunca usar git reset --hard (Elimina todos tus cambios)
- ENSERIO PUSH DIARIOS OBLIGATORIO