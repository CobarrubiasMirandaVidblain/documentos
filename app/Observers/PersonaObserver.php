<?php

namespace App\Observers;

use App\Models\Persona;

class PersonaObserver
{
    /**
     * Listen to the Persona created event.
     *
     * @param  \App\Persona  $Persona
     * @return void
     */
    public function created(Persona $persona)
    {
        //
    }

    /**
     * Listen to the Persona deleting event.
     *
     * @param  \App\Persona  $Persona
     * @return void
     */
    public function deleting(Persona $persona)
    {
        if($persona->usuario2){
            $persona->usuario2->delete();
        }
    }
}