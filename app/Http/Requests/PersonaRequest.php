<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PersonaRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nombre' => [
                'required',
                'string',
                'max:255',
                Rule::unique('personas')->where(function($query) {
                    $query = $query->where('nombre', $this->nombre)
                    ->where('primer_apellido', $this->primer_apellido)
                    ->where('segundo_apellido', $this->segundo_apellido)                    
                    ->where('id', '!=', $this->id);

                    if($this->fecha_nacimiento){
                        $query->where('fecha_nacimiento', (\DateTime::createFromFormat('d/m/Y', $this->fecha_nacimiento))->format('Y-m-d'));
                    }

                    return $query;
                })
            ],
            'curp' => [
                'max:18',
                Rule::unique('personas')->where(function($query) {
                    return $query->whereNotNull('curp')->where('curp', $this->curp)->where('id', '!=', $this->id);
                })
            ],
            'clave_electoral' => [
                'max:18',
                Rule::unique('personas')->where(function($query) {
                    return $query->whereNotNull('clave_electoral')->where('clave_electoral', '=', $this->clave_electoral)->where('id', '!=', $this->id);
                })
            ]/*,
			'fecha_nacimiento' => [
				'date_format:"d/m/Y"',
				'after:01/01/1910',
				'before:' . date('d/m/Y')
            ]*/
        ];
    }

    public function messages() {
        return [
            'nombre.unique' => 'Ya se encuentra una persona registrada con el mismo nombre y la misma fecha de nacimiento.'
        ];
    }
}