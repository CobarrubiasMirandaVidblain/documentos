<?php

namespace App\Http\Requests\Giras;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class GiraRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        if($this->gira){
            return [
                'nombre' => [
                    'required',
                    'max:255',
                    'unique:atnc_cat_giras,nombre,' . $this->gira_id
                ]
            ];
        }
        return [
            'nombre' => [
                'required',
                'max:255',
                'unique:atnc_cat_giras,nombre,' . $this->id
            ]
        ];
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(response()->json( [
            'success' => false,
            'errors' => $validator->errors()->all()
        ], 422)); 
    }
    
}