<?php

namespace App\Http\Requests\Giras;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
use App\Models\EventoAreas;

class EventoAreaRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'area_id' => [
                'required',
                'integer',
                Rule::unique('areas_eventos')->where(function($query) {
                    return $query->where('area_id', $this->area_id)
                    ->where('evento_id', $this->evento_id)
                    ->whereNull('deleted_at');       
                })
            ],
            'observaciones' => [
                'nullable'
            ]            
        ];
    }

    public function messages() {
        return [
            'area_id.unique' => 'El área ya se encuentra registrada en el mismo evento.'
        ];
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(response()->json( [
            'success' => false,
            'errors' => $validator->errors()->all()
        ], 422)); 
    }
}
