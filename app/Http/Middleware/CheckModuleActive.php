<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Modulo;

class CheckModuleActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $modulos = new Modulo();
        $prefixes = $modulos->prefixes();
        foreach($prefixes as $prefix) {
            $resultado = strpos($request->path(), $prefix->prefix);
            if ($resultado !== FALSE) {
                if (! $prefix->activo) {
                    abort(403, 'Este modulo no ha sido liberado');
                }
            }
        }
        return $next($request);
    }
}
