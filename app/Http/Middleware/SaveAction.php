<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Bitacora;
use App\Models\Actividad;
use Illuminate\Support\Facades\Route;

class SaveAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $actividad = Actividad::where('actividad', Route::current()->uri)->first();
        if($actividad === null)
        {
            $actividad = Actividad::create([
                'actividad' => Route::current()->uri,
                'usuario_id' => isset(auth()->user()->id) ? auth()->user()->id : $request->usuario_id ? $request->usuario_id : null,
            ]);
        }
        Bitacora::create([
            'usuario_id' => isset(auth()->user()->id) ? auth()->user()->id : $request->usuario_id ? $request->usuario_id : null,
            'actividad_id' => $actividad->id,
            'modulo_id' => 1,
            'tabla' => isset($request->tabla) ? $request->tabla : 'ninguna',
            'registro' => isset($request->registro) ? $request->registro : 0,
            'ip' => $request->ip(),
            'so' => $request->header()['user-agent'][0],
            'usuario_red' => shell_exec('echo %username%'),
            'campos' => $request->campos ? $request->campos : '{}',
            'metodo' => request()->metodo
        ]);
    }
    
}
