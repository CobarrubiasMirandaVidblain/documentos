<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\File;

use App\Models\DocumentosPersona;

class DocumentosPersonasController extends Controller{
    public function __construct(){
        $this->middleware(['auth','authorized']);
    }

    public function store(Request $request){
        if($request->ajax()){
            try{
                $docPerson['persona_id']=$request->input('persona_id');
                // $docPerson['fechapresento']=date(); NO ES REQUERIDO Y HACE LO MISMO QUE CREATED_AT
                $docPerson['usuario_id']=$request->user()->id;

                $documentos=(object)json_decode($request->input('documentos'));

                DB::beginTransaction();
                foreach ($documentos as $documento) {
                    $docPerson['documentos_programa_id']=$documento->id;
                    $url = Storage::putFile('public/documentos', $request->file($documento->nombre));
                    $partes = explode('public', $url);
                    $docPerson['presento'] = 'storage'.$partes[1];
                    $new=DocumentosPersona::create($docPerson);
                }
                DB::commit();
                return response()->json(array('success' => true, 'message' => 'Escaneo guarado correctamente' ));
            }catch(Exeption $e){
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

}
