<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tiposrecepcion;


class RecepcionController extends Controller
{
    public function recepcionSelect(Request $request)
    {
        //
        if ($request->ajax())
        {
            $recepciones = Tiposrecepcion::where('tipo', 'LIKE', '%' . $request->input('search') . '%')
            ->get()
            ->toArray();
            return response()->json($recepciones);
        }
    }
}
