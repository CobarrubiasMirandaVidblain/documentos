<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AreaController extends Controller {
    
    public function select(Request $request) {
        
        $areas = Area::where('nombre', 'LIKE', '%' . $request->input('search') . '%');            

        if($request->has('padre_id')){
            $ids = array_map( function($o) { return $o->id_hijo; }, DB::select('call getAreasHijas(?)',[$request->input('padre_id')]));
            $ids[$request->input('padre_id')] = $request->input('padre_id');
            $areas = $areas->whereIn('id', $ids);                        
        }

        $areas = $areas//->take(10)
            ->get()
            ->toArray();
        
        return response()->json($areas);
    }

    public function select2(Request $request){
        return Area::select('id', 'nombre as text')->where('nombre', 'LIKE', "%$request->q%")->take(10)->get();
    }

}
