<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Beneficiariogira;
use App\Models\Programa;
use App\Models\BeneficiariogiraPrograma;

class SolicitudController extends Controller {
    
    public function store(Request $request) {
        $beneficiarios = [];
        
        foreach( $request->all() as $beneficiario ) {
            
            try {
                DB::beginTransaction();
                
                if($beneficiario['fecha_nacimiento']){
                    $beneficiario['fecha_nacimiento'] = (\DateTime::createFromFormat('d-m-Y', $beneficiario['fecha_nacimiento']))->format('Y-m-d');
                }
                $beneficiario['fecha_registro'] = date('Y-m-d h:i:s', strtotime( $beneficiario['fecha_registro'] ));
                $beneficiario['usuario_id'] = $request->user()->id;
                
                $beneficiario_gira = Beneficiariogira::create( array_except($beneficiario, ['folio']) );

                foreach( $beneficiario['apoyos'] as $apoyo ) {
                    $programa_id = Programa::firstOrCreate(['nombre' => $apoyo['nombre']], ['activo' => 1,'tipo' => 'PRODUCTO','oficial'=>0,'puede_agregar_benef'=>0,'usuario_id' => $beneficiario['usuario_id']])->id;

                    BeneficiariogiraPrograma::create([
                        'girapreregis_id' => $beneficiario_gira->id,
                        'programa_id' => $programa_id
                    ]);
                }                

                DB::commit();
                $beneficiario['estatus'] = "Sincronizado";
                $beneficiarios[] = $beneficiario;
            } catch(\Exception $e){                
                DB::rollBack();
                $beneficiario['estatus'] = $e->getMessage();
                $beneficiarios[] = $beneficiario;
            }
        }
        return response()->json( $beneficiarios );
    }
}
