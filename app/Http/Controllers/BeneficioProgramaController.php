<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\DataTables\ProgramasBeneficiosDataTable;
use App\Models\Beneficiosprograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use App\Models\Programa;
use View;

class BeneficioProgramaController extends Controller {
  public function index(ProgramasBeneficiosDataTable $tabla, Request $request,$programa_id){
    return $tabla->with('programa_id',$programa_id)
    ->render("programas.modals.$request->view.beneficiosPrograma",['programa'=>Programa::find($programa_id)]);
  }
  public function create(Request $request,$programa_id){
    return view::make("programas.modals.$request->view.beneficiosProgramaCreateEdit",
    ['programa_id'=>$programa_id])
    ->render();
  }
  public function store(Request $request,$programa_id){
    try {
      $apaux = AniosPrograma::where([['programa_id',$programa_id],['ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id]])->first()->id;
      DB::beginTransaction();
      $new=BeneficiosPrograma::Create([
        'anio_programa_id'=>$apaux,
        'nombre'=>$request->input('nombre'),
        'predeterminado'=>$request->input('predeterminado')?'1':'0',
        'usuario_id'=>Auth::user()->id
      ]);
      DB::commit();
      return new JsonResponse(['success'=>true,'new'=>$new],200);
    }catch(\Exeption $e) {
      DB::rollBack();
      abort(500,'Algo no salio bien');
    }
  }
  public function show(){
    
  }
  public function edit(Request $request,$programa_id,$beneficio_id){
    return view::make("programas.modals.$request->view.beneficiosProgramaCreateEdit",
    [
      'programa_id'=>$programa_id,
      'beneficio'=>BeneficiosPrograma::find($beneficio_id)
    ])
    ->render();
  }
  public function update(Request $request,$programa_id,$beneficio_id){
    try {
      DB::beginTransaction();
      $datos=$request->all();
      $datos['predeterminado']=$datos['predeterminado']=="true"?1:0;
      $new=BeneficiosPrograma::find($beneficio_id)->update($datos);
      DB::commit();
      return new JsonResponse(['success'=>true,'new'=>$datos],200);
    }catch(Exeption $e) {
      DB::rollBack();
      abort(500,'Algo no salio bien');
    }
  }
  public function destroy($programa_id,$beneficio_id){
    try{
      Beneficiosprograma::find($beneficio_id)->delete();
      return new JsonResponse(['success'=>true,'message'=>'eliminado con éxito'],200);
    }
    catch(\Exception $e){
      abort(500,'error al eliminar');
    }
  }
}