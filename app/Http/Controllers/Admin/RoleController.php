<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rol;
use Illuminate\Http\Request;
use Validator;
use App\Models\Usuario;
use App\Models\Modulo;
use App\Models\UsuariosRol;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('authorized');
        $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,COORDINADOR', ['except' => 'edit']);
        //$this->middleware('saveaction', ['except' => ['index', 'create', 'show', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Rol::orderBy('id')->paginate(10);

        return view('admin.roles.index', ['roles' => $roles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modulos = Modulo::all();
        return view('admin.roles.create', ['modulos' => $modulos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'usuario_id' => 'required|numeric',
            'modulo_id' => 'required|numeric',
            'nombre' => 'required|min:3|string',
            'descripcion' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('roles.create')
                ->withErrors($validator)
                ->withInput();
        }

        try{
            DB::beginTransaction();
            $rol = Rol::create([
                'nombre' => $request->nombre,
                'descripcion' => $request->descripcion,
                'usuario_id' => $request->usuario_id,
                'modulo_id' => $request->modulo_id,
                'valor' => $request->valor == 'on' ? 1 : 0
            ]);
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
        }


        if($rol){
            return redirect()->route('roles.create')->with(['status' => 'ok', 'msg' => 'Rol Agregado Correctamente!']);
        }

        return redirect()->route('roles.create')->with(['status' => 'error', 'msg' => 'Ocurrio un error! :(']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function show(Rol $rol)
    {
        return view('admin.roles.create', ['rol' => $rol]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function edit(Rol $rol)
    {        
        return view('admin.roles.create', ['rol' => $rol]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rol $rol)
    {
        $validator = Validator::make($request->all(), [
            'usuario_id' => 'required|numeric',
            'nombre' => 'required|min:3|string|unique:cat_roles',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('role.edit', $rol)
                ->withErrors($validator)
                ->withInput();
        }

        $rol->nombre = $request->nombre;

        $rol->usuario_id = $request->usuario_id;

        try{
            DB::beginTransaction();
            $rol->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('roles.edit', $rol)->with(['status' => 'error', 'msg' => 'Ocurrio un error !']);
        }

        return redirect()->route('roles.edit', $rol)->with(['status' => 'ok', 'msg' => 'Editado Correctamente !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rol  $rol
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rol $rol)
    {
        try{
            DB::beginTransaction();
            $rol->delete();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('role')->with(['status' => 'error', 'msg' => 'Ocurrio un error !']);
        }

        return redirect()->route('role')->with(['status' => 'ok', 'msg' => 'Eliminado Correctamente !']);
    }

    public function asign()
    {
        if(auth()->user()->hasRoles(['SUPERADMIN', 'ADMINISTRADOR'])){
            $modulos = Modulo::all();
            return view('admin.roles.asign', ['modulos' => $modulos]);
        }else{
            $modulo = Modulo::find(auth()->user()->adminModule('COORDINADOR')->modulo_id);
            return view('admin.roles.asign', ['modulo' => $modulo]);
        }
    }

    public function roles($modulo){
        $roles = Rol::where('modulo_id', $modulo)->get();
        return new JsonResponse($roles);
    }

    public function usuarios(Request $request)
    {
        $usuarios = Usuario::where('usuario', 'LIKE', '%' . $request->search . '%')->take(10)->get();
        
        return response()->json($usuarios);
    }

    public function addAsign(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'responsable' => 'required|numeric',
            'usuario_id' => 'required|numeric',
            'rol_id' => 'required|numeric',
            'modulo_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('roles.asign')
                ->withErrors($validator)
                ->withInput();
        }

        $rolesUsuario = UsuariosRol::where('usuario_id', $request->usuario_id)->where('modulo_id', $request->modulo_id)->get();

        if($rolesUsuario->count()){
            return redirect()->route('roles.asign.create')->with(['status' => 'error', 'msg' => 'Ya cuenta con un rol']);
        }
        try{
            DB::beginTransaction();
            $rol = UsuariosRol::create([
                'responsable' => $request->responsable,
                'usuario_id' =>  $request->usuario_id,
                'modulo_id' => $request->modulo_id,
                'rol_id' => $request->rol_id,
                'autorizado' => 1
            ]);
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('roles.asign.create')->with(['status' => 'error', 'msg' => 'Ocurrio un error... intentelo de nuevo']);
        }

        return redirect()->route('roles.asign.create')->with(['status' => 'ok', 'msg' => 'Rol agregado correctamente']);
    }

    public function showUserRol(Request $request)
    {
        return view('admin.roles.users');
    }

    public function rolesPaginado(Request $request){
        $usersRol = UsuariosRol::withTrashed()->with(['modulo', 'usuario', 'rol', 'usuario.persona:id,nombre,primer_apellido,segundo_apellido']);
        if($request->orderBy) {
            $ascending = $request->ascending;
            if($request->orderBy == 'usuarioNombre'){
                    $usersRol = $usersRol->whereHas('usuario', function($q) use($ascending) { orderBy('usuario', $ascending == 1 ? 'asc' : 'desc');});
                }
            if($request->orderBy == 'moduloNombre'){
                $usersRol = $usersRol->whereHas('modulo', function($q) use($ascending) { $q->orderBy('nombre', $ascending == 1 ? 'asc' : 'desc');});
            }
            if($request->orderBy == 'rolNombre'){
                $usersRol = $usersRol->whereHas('rol', function($q) use($ascending) { $q->orderBy('nombre', $ascending == 1 ? 'asc' : 'desc');});
            }
            if($request->orderBy == 'autorizado') {
                $usersRol = $usersRol->orderBy('autorizado', $ascending == 1 ? 'asc' : 'desc');
            }
            if($request->orderBy == 'deleted_at') {
                $usersRol = $usersRol->orderBy('deleted_at', $ascending == 1 ? 'asc' : 'desc');
            }
        }
        if($request->query) {
            $query = json_decode($request->input('query'));
            foreach ($query as $clave => $valor) {
                \Log::debug("$clave => $valor");
                // if($clave == 'nombre'){
                //     $usersRol = $usersRol->whereHas('usuario.persona', function($q) use($valor) { $q->where('usuario', 'like', "%$valor%");});
                // }
                // if($clave == 'primerApellido'){
                //     $usersRol = $usersRol->whereHas('usuario.persona', function($q) use($valor) { $q->where('usuario', 'like', "%$valor%");});
                // }
                // if($clave == 'segundoApellido'){
                //     $usersRol = $usersRol->whereHas('usuario.persona', function($q) use($valor) { $q->where('usuario', 'like', "%$valor%");});
                // }
                if($clave == 'usuarioNombre'){
                    $usersRol = $usersRol->whereHas('usuario', function($q) use($valor) { $q->where('usuario', 'like', "%$valor%");});
                }
                if($clave == 'moduloNombre'){
                    $usersRol = $usersRol->whereHas('modulo', function($q) use($valor) { $q->where('nombre', 'like', "%$valor%");});
                }
                if($clave == 'rolNombre'){
                    $usersRol = $usersRol->whereHas('rol', function($q) use($valor) { $q->where('nombre', 'like', "%$valor%");});
                }
                if($clave == 'autorizado') {
                    $usersRol = $usersRol->where('autorizado', $valor);
                }
                if($clave == 'deleted_at') {
                    $usersRol = $usersRol->where('deleted_at', 'like', "%$valor%");
                }
            }
        }
        $usersRol = $usersRol->paginate($request->limit)->toArray();
        $data = $usersRol['total'];
        unset($usersRol['total']);
        $usersRol['count'] = $data;
        for($i = 0; $i < count($usersRol['data']); $i++){
            $usersRol['data'][$i]['usuarioNombre'] = $usersRol['data'][$i]['usuario']['usuario'];
            $usersRol['data'][$i]['moduloNombre'] = $usersRol['data'][$i]['modulo']['nombre'];
            $usersRol['data'][$i]['rolNombre'] = $usersRol['data'][$i]['rol']['nombre'];
            $usersRol['data'][$i]['nombre'] = $usersRol['data'][$i]['usuario']['persona']['nombre'];
            $usersRol['data'][$i]['primerApellido'] = $usersRol['data'][$i]['usuario']['persona']['primer_apellido'];
            $usersRol['data'][$i]['segundoApellido'] = $usersRol['data'][$i]['usuario']['persona']['segundo_apellido'];
        }
        return new JsonResponse($usersRol);
    }

    public function deleteUserRol(UsuariosRol $usuariosRol, $id)
    {        
        try{
            DB::beginTransaction();
            UsuariosRol::destroy($id);
            //$usuariosRol->delete();
            DB::commit();
            if(request()->wantsJson()){
                return new JsonResponse(['status' => 'ok', 'msg' => 'Eliminado Correctamente !']);
            }
            return redirect()->route('roles.users.index')->with(['status' => 'ok', 'msg' => 'Eliminado Correctamente !']);
        }catch(Exception $e){
            DB::rollBack();
            if(request()->wantsJson()){
                return new JsonResponse($e, 422);
            }
            return redirect()->route('roles.users.index')->with(['status' => 'error', 'msg' => 'Ocurrio un error... intentelo de nuevo !']);
        }

    }

    public function autorizar(UsuariosRol $rol){
        if(request()->wantsJson()){
            try{
                DB::beginTransaction();
                $rol->autorizado = !$rol->autorizado;
                $rol->save();
                DB::commit();
                auth()->user()->bitacora(request(), [
                    'tabla' => $rol->getTable() . '',
                    'registro' => $rol->id . '',
                    'campos' => json_encode($rol) . '',
                    'metodo' => request()->method()
                ]);
                return new JsonResponse($rol);
            }catch(Exception $e){
                DB::rollBack();
                return new JsonResponse($e, 422);
            }
        }    
        else{
            abort(403);
        }
    }
}
