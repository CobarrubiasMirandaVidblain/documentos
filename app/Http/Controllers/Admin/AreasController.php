<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Models\Area;
use App\Models\AreasResponsable;

class AreasController extends Controller
{
    public function index()
    {
        return view('admin.areas.index');
    }

    public function areas($area = null){
        if($area == null){
            return new JsonResponse(AreasResponsable::with('empleado', 'area')->paginate(10));
        }else{
            return new JsonResponse(AreasResponsable::withTrashed()->whereHas('area', function($query) use($area){
                $query->where('nombre', 'like', "%$area%");
            })->with('empleado.persona', 'area')->paginate(10));
        }
    }
}
