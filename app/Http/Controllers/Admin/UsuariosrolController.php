<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Models\UsuariosRol;

class UsuariosrolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('authorized');
        $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,COORDINADOR');
        $this->middleware('saveaction');
    }

    public function store(Request $request){
        $request->tabla = 'usuarios_roles';
        try{
            DB::beginTransaction();
            $usuariorol = UsuariosRol::create([
                'usuario_id' => $request->usuario_id,
                'rol_id' => $request->rol_id,
                'modulo_id' => $request->modulo_id,
                'responsable' => auth()->user()->id,
                'autorizado' => $request->autorizado ? 1 : 0,
                ]);
                DB::commit();
            $request->registro = $usuariorol->id;
            $request->campos = 'ok';
            return new JsonResponse($usuariorol);
        }catch(Exception $e){
            $request->campos = 'error';
            DB::rollback();
            return new JsonResponse(['status' => 'error', 'msg' => 'Ocurrio un error inesperado :('], 422);
        }
    }
}
