<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TitularController extends Controller
{
    public function titularSelect(Request $request)
    {
        $titulares = DB::table('titulares')
            ->join('personas','titulares.persona_id','=','personas.id')
            ->select('personas.id','personas.nombre','personas.primer_apellido','personas.segundo_apellido')
            ->where('personas.nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->orWhere('personas.primer_apellido', 'LIKE', '%' . $request->input('search') . '%')
            ->orWhere('personas.segundo_apellido', 'LIKE', '%' . $request->input('search') . '%')
            ->get();
        return response()->json($titulares);
    }
}