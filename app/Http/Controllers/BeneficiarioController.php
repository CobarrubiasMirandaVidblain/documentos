<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Beneficiariogira;
use App\Models\Productosfoliados;
use App\Models\BeneficiariogiraPrograma;
use Illuminate\Support\Facades\Storage;

class BeneficiarioController extends Controller {
    public function index() {
        return Beneficiariogira::all();
    }
 
    public function show(Beneficiariogira $beneficiario) {
        return $beneficiario;
    }

    public function store(Request $request) {
        $beneficiarios = [];

        foreach( $request->all() as $beneficiario ) {
            
            if(Beneficiariogira::where('folio', $beneficiario['folio'])->count() > 0){
                $beneficiario['estatus'] = "Folio duplicado.";
                $beneficiarios[] = $beneficiario;
                continue;
            }

            $producto_foliado = Productosfoliados::where('folio', $beneficiario['folio'])->first();

            if(!$producto_foliado){
                $beneficiario['estatus'] = "El folio no corresponde a ningún apoyo funcional.";
                $beneficiarios[] = $beneficiario;
                continue;
            }

            try {
                DB::beginTransaction();
                $image = $beneficiario['foto'];
                if($beneficiario['fecha_nacimiento']){
                    $beneficiario['fecha_nacimiento'] = (\DateTime::createFromFormat('d-m-Y', $beneficiario['fecha_nacimiento']))->format('Y-m-d');
                }
                $beneficiario['fecha_registro'] = date('Y-m-d h:i:s', strtotime( $beneficiario['fecha_registro'] ));
                $beneficiario['usuario_id'] = $request->user()->id;
                $beneficiario['fotografia'] = null;
                if($image) {
                    $folio = str_replace('/', '_', $beneficiario['folio']);
                    Storage::makeDirectory("public/eventos_atnciudadana");
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $imageName = 'image_'.time().'_'.$folio.'.png';
                    \File::put(storage_path(). '/app/public/eventos_atnciudadana/' . $imageName, base64_decode($image) );
                    $beneficiario['fotografia'] = 'storage/eventos_atnciudadana/' . $imageName;
                }
                $beneficiario_gira = Beneficiariogira::create($beneficiario);

                BeneficiariogiraPrograma::create([
                    'girapreregis_id' => $beneficiario_gira->id,
                    'programa_id' => $producto_foliado->detalleentradasproductos->areasproducto->programa_id
                ]);

                DB::commit();
                $beneficiario['estatus'] = "Sincronizado";
                $beneficiarios[] = $beneficiario;
            } catch(\Exception $e){                
                DB::rollBack();
                $beneficiario['estatus'] = $e->getMessage();
                $beneficiarios[] = $beneficiario;
            }
        }
        return response()->json( $beneficiarios );
    }

    public function update(Request $request, Beneficiariogira $beneficiario) {
        $beneficiario->update($request->all());
        return response()->json($beneficiario, 200);
    }

    public function delete(Beneficiariogira $beneficiario) {
        $beneficiario->delete();
        return response()->json(null, 204);
    }
}
