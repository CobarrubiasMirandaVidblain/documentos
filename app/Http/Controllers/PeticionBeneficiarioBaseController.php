<?php

namespace App\Http\Controllers;

use View;
use App\Models\Persona;
use App\Models\Programa;
use Illuminate\Http\Request;
use App\Models\DocumentosPersona;
use App\Models\ProgramasSolicitud;
use App\Models\SolicitudesPersona;
use Illuminate\Support\Facades\DB;
use App\Models\DocumentosSolicitud;
use App\Models\BeneficiosprogramasSolicitud;

class PeticionBeneficiarioBaseController extends Controller
{

  //Para validar que la petición sea de ese programa
  private $programa_padre;

  public function __construct($programa_padre = "")
  {
    $this->programa_padre = $programa_padre;
    $this->middleware(['auth', 'authorized']);
  }

  public function store($peticion_id, Request $request)
  {
    if ($request->ajax()) {
      try {
        DB::beginTransaction();
        $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);

        if (($this->programa_padre != "" && $peticion->programa->padre->nombre != $this->programa_padre) || ($this->programa_padre == "" && !Programa::search("", auth()->user()->persona->empleado->area_id)->where('programas.id', $peticion->programa->id)->first())
        ) {
          abort(403, 'Permisos insuficientes');
        }

        $persona = Persona::findOrFail($request->persona_id);

        $beneficiario = SolicitudesPersona::withTrashed()->where('programas_solicitud_id', $peticion_id)->where('persona_id', $persona->id)->first();

        if ($beneficiario) {
          if ($beneficiario->trashed()) {
            //Fue eliminado el beneficiario pero podemos reactivar el registro en la BD
            $beneficiario->restore();
            $beneficiario->evaluado = null;
            $beneficiario->entregado = null;
            $beneficiario->observacionevaluado = null;
            $beneficiario->save();
          } else {
            //Significa que la persona ya es beneficiaria de esa petición
            return response()->json(['errors' => array(['code' => 400, 'message' => 'La persona ya se encuentra registrada como beneficiario de esta petición.'])], 400);
          }
        } else {
          //Nunca se ha registrado como beneficiario de esa petición
          //Asociando la persona con la peticion-programa
          $persona->solicitudespersonas()->create([
            'programas_solicitud_id' => $peticion->id
          ]);
        }

        //Guaradando actividad en la bitácora
        auth()->user()->bitacora(request(), [
          'tabla' => 'solicitudes_personas',
          'registro' => $peticion->id . '',
          'campos' => json_encode($persona->solicitudespersonas()->latest()->first()) . '',
          'metodo' => 'POST'
        ]);

        DB::commit();
        return response()->json(['status' => 'ok'], 200);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
      }
    }
  }

  public function edit($peticion_id, $persona_id)
  {
    $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
    /* if( ($this->programa_padre != "" && $peticion->programa->padre->nombre != $this->programa_padre) ||
			($this->programa_padre == "" && !Programa::search("", auth()->user()->persona->empleado->area_id)->where('programas.id',$peticion->programa->id)->first())
		){
            abort(403,'Permisos insuficientes');
        } */
    $persona = Persona::findOrFail($persona_id);

    $expediente = View::make('peticiones.beneficiarios.expediente')
      ->with("persona", $persona)
      ->with("peticion", $peticion);

    return response()->json([
      'expediente' => $expediente->render()
    ], 200);
  }

  public function show($peticion_id, $persona_id)
  {
    $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
    /* if( ($this->programa_padre != "" && $peticion->beneficioprograma->->padre->nombre != $this->programa_padre) ||
		  	($this->programa_padre == "" && !Programa::search("", auth()->user()->persona->empleado->area_id)->where('programas.id',$peticion->programa->id)->first())
        ){
          abort(403,'Permisos insuficientes');
        } */
    $persona = Persona::findOrFail($persona_id);

    $beneficiario = View::make('peticiones.beneficiarios.show')
      ->with([
        "persona" => $persona,
        "programa_padre" => $this->programa_padre,
        "peticion" => $peticion
      ]);

    return response()->json([
      'beneficiario' =>  $beneficiario->render()
    ], 200);
  }

  public function update($peticion_id, $persona_id, Request $request)
  {
    if ($request->ajax()) {
      try {
        DB::beginTransaction();
        $persona = Persona::find($persona_id);
        //Si check documentos
        if ($request->has('documentos_programas')) {
          foreach ($request->documentos_programas as $documento_programa) {

            //Busco o creo el documento_persona                        
            $documento_persona = DocumentosPersona::where('persona_id', $persona_id)->where('documentos_programa_id', $documento_programa)->first();
            // dd($documento_persona);
            if (!$documento_persona) {
              $documento_persona = DocumentosPersona::create([
                'persona_id' => $persona->id,
                'documentos_programa_id' => $documento_programa,
                'usuario_id' => $request->user()->id
              ]);
              auth()->user()->bitacora(request(), [
                'tabla' => 'documentos_personas',
                'registro' => $documento_persona->id . '',
                'campos' => json_encode($documento_persona) . '',
                'metodo' => 'POST'
              ]);
            }

            //Ahora busco o creo el documento_solicitud
            $documento_solicitud = DocumentosSolicitud::withTrashed()->where('documentos_persona_id', $documento_persona->id)->where('beneficio_solicitud_id', $peticion_id)->first();
            // dd($documento_solicitud);
            if (!$documento_solicitud) {
              $documento_solicitud = DocumentosSolicitud::create([
                'documentos_persona_id' => $documento_persona->id,
                'beneficio_solicitud_id' => $peticion_id
              ]);
              auth()->user()->bitacora(request(), [
                'tabla' => 'documentos_solicitudes',
                'registro' => $documento_solicitud->id . '',
                'campos' => json_encode($documento_solicitud) . '',
                'metodo' => 'POST'
              ]);
            }

            if ($documento_solicitud->trashed()) {
              $documento_solicitud->restore();
              auth()->user()->bitacora(request(), [
                'tabla' => 'documentos_solicitudes',
                'registro' => $documento_solicitud->id . '',
                'campos' => json_encode($documento_solicitud) . '',
                'metodo' => 'POST'
              ]);
            }
          }


          //Eliminando aquellos documentos que no se encuentran en el request
          foreach (DocumentosSolicitud::where('beneficio_solicitud_id', $peticion_id)->get() as $documento_solicitud) {
            $existe = false;
            foreach ($request->documentos_programas as $documento_programa) {
              if ($documento_solicitud->documentospersona->documentos_programa_id == $documento_programa && $documento_solicitud->documentospersona->persona_id == $persona_id) {
                $existe = true;
                break;
              }
            }

            if (!$existe) {
              DocumentosSolicitud::destroy($documento_solicitud->id);
              auth()->user()->bitacora(request(), [
                'tabla' => 'documentos_solicitudes',
                'registro' => $documento_solicitud->id . '',
                'campos' => json_encode($documento_solicitud) . '',
                'metodo' => 'DELETE'
              ]);
            }
          }
        }
        //En caso de que no haya checkeado quiere decir que se van a eliminar los documentos (si es que existen)
        else {
          foreach ($persona->documentospersonas as $documentoPersona) {
            foreach ($documentoPersona->documentossolicitudes as $documentoSolicitud) {
              DocumentosSolicitud::destroy($documentoSolicitud->id);
              auth()->user()->bitacora(request(), [
                'tabla' => 'documentos_solicitudes',
                'registro' => $documentoSolicitud->id . '',
                'campos' => json_encode($documentoSolicitud) . '',
                'metodo' => 'DELETE'
              ]);
            }
          }
        }

        $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
        $solicitante = $peticion->petcionespersonas()->wherehas("beneficiopersona",function($q) use($persona){
          $q->where("persona_id", $persona->id);
        })->first();
        if ($solicitante) {
          $this->validarExpediente($peticion, $solicitante);
        }

        DB::commit();
        return response()->json(['status' => true, 'persona_id' => $persona->id], 200);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e),500);
      }
    }
  }

  public function destroy($peticion_id, $persona_id, Request $request)
  {
    if ($request->ajax()) {
      try {
        DB::beginTransaction();
        $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
        $beneficiario = SolicitudesPersona::where("programas_solicitud_id", $peticion->id)
          ->where("persona_id", $persona_id)
          ->firstOrFail();
        $beneficiario->evaluado = false;
        $beneficiario->entregado = false;
        $beneficiario->observacionevaluado = $request->motivo;
        $beneficiario->save();
        auth()->user()->bitacora(request(), [
          'tabla' => 'solicitudes_personas',
          'registro' => $beneficiario->id . '',
          'campos' => json_encode($beneficiario) . '',
          'metodo' => 'PUT'
        ]);
        $beneficiario->delete();
        DB::commit();
        return response()->json(['status' => '', 'persona_id' => $persona_id], 200);
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
      }
    }
  }

  private function validarExpediente($peticion, $persona)
  {
    $status = true;

    //Si le falta algún documento por check entonces el estatus del beneficiario es VALIDANDO
    foreach ($peticion->beneficio->anioprograma->documentosprogramas as $documentoPrograma) {
      if (!$documentoPrograma->documentospersonas->contains('persona_id', $persona->beneficiopersona->persona_id)) {
        $status = false;
        break;
      }
    }

    if ($status) {
      $persona->evaluado = 2;
    } else {
      $persona->evaluado = 0;
    }
    $persona->save();
    auth()->user()->bitacora(request(), [
      'tabla' => 'peticiones_personas',
      'registro' => $persona->id . '',
      'campos' => json_encode($persona) . '',
      'metodo' => 'PUT'
    ]);
  }
}
