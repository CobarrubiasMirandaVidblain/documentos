<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\File;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\Persona;
use App\Models\Empleado;
use App\Http\Requests\PersonaRequest;

use App\DataTables\PersonasDataTable;

use View;

class PersonaController extends Controller {
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        //$this->middleware('roles:CAPTURISTA', ['except' => 'index']);
        //$this->middleware('roles:SUPERADMIN,ADMINISTRADOR,COORDINADOR,OPERATIVO,ADMINISTRATIVO,APOYO OPERATIVO');
        //$this->middleware('roles:ENLACE,LEGAL,AUXILIAR,FINANCIERO,MEDICO,CAPTURISTA', ['except' => ['index', 'create', 'show', 'edit', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PersonasDataTable $dataTable) {
        return $dataTable        
        //->with('tipo', 'normal')
        ->render('personas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('personas.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PersonaRequest $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $datos = $request->except(['fotografia', 'municipio']);
                $datos['fecha_nacimiento'] = ($datos['fecha_nacimiento']) ? (\DateTime::createFromFormat('d/m/Y', $datos['fecha_nacimiento']))->format('Y-m-d') : null;

                $datos['usuario_id'] = auth()->user()->id;

                $directorio = 'public/fotografia';

                if($request->has('fotografia')) {
                    $url = Storage::putFile($directorio, $request->fotografia);
                    $partes = explode('public', $url);
                    $datos['fotografia'] = 'storage' . $partes[1];
                }
                
                $persona = Persona::create($datos);

                DB::commit();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'personas',
                    'registro' => $persona->id . '',
                    'campos' => json_encode($persona) . '',
                    'metodo' => request()->method()
                ]);

                return response()->json(['success' => true, 'code' => 200, 'id' => $persona->id, 'nombre' => $persona->get_nombre_completo(), 'estatus' => 'nuevo', 'estatus_AFuncionales' => 'nuevo'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();

                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        $persona = Persona::findOrFail($request->id);
        if($request->has('tipo') && $request->tipo=='modal')
             return response()->json([
                'body' =>  view::make('personas.iframe.show')
                ->with([
                    'persona'=> $persona
                ])
                ->render()
            ], 200);
        return view('personas.show', array('persona' => $persona));
    }

    public function mostrar(Request $request) {
        $persona = Persona::findOrFail($request->id);

        $persona = View::make('peticiones.beneficiarios.show')
            ->with([
                "persona" => $persona,
                "programa_padre" => '',
                "peticion" => null
            ]);
        
        return response()->json([
            'persona' =>  $persona->render()
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Request $request) {
        $persona = Persona::findOrFail($request->id);
        if($request->tipo=='modal'){
          $view = View::make('personas.iframe.create_edit',['persona'=>$persona]);
          $html = $view->render();
          return response()->json(['html' => $html]);
        }
        if($request->tipo=='onlydata'){
          return ['persona'=>$persona,'mun'=>$persona->municipio->nombre,'loc'=>$persona->localidad->nombre];
        }
        return view('personas.create_edit', array('persona' => $persona));
    }
    //Función antes de residencia
    /* public function edit(Request $request) {
        $persona = Persona::findOrFail($request->id);
        return view('personas.create_edit', array('persona' => $persona));
    } */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PersonaRequest $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $imagen = $request->only('imagen')['imagen'];

                $datos = $request->except(['fotografia', 'municipio', 'imagen']);

                $datos['fecha_nacimiento'] = (\DateTime::createFromFormat('d/m/Y', $datos['fecha_nacimiento']))->format('Y-m-d');

                $persona = Persona::find($request->id);

                if($persona) {

                    $directorio = 'public/fotografia';

                    if($request->has('fotografia')) {
                        $url = Storage::putFile($directorio, $request->fotografia);
                        $partes = explode('public', $url);
                        $datos['fotografia'] = 'storage' . $partes[1];
                    }
                    else {
                        if($imagen === 'true' && isset($persona->fotografia)) {
                            $partes = explode('storage', $persona->fotografia);
                            Storage::delete('public' . $partes[1]);
                            $datos['fotografia'] = null;
                        }
                    }

                    $persona->update($datos);
                }

                DB::commit();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'personas',
                    'registro' => $persona->id . '',
                    'campos' => json_encode($persona) . '',
                    'metodo' => request()->method()
                ]);


                return response()->json(['success' => true, 'code' => 200, 'id' => $persona->id, 'nombre' => $persona->get_nombre_completo(), 'estatus' => 'update'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                Persona::destroy($request->id);

                DB::commit();

                return response()->json(['success' => true, 'code' => 200, 'id' => $request->id, 'estatus' => 'destroy'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {
        if($request->has('id')) {
            $persona = Persona::find($request->id);
        }
        
        if($request->has('curp')) {
            $persona = Persona::where('curp', $request->curp)->first();
        }      
        
        if(isset($persona)) {
            if($request->has('tipo')) {
                $view = View::make('personas.iframe.' . $request->tipo)->with('persona', $persona);
                $html = $view->render();
                return response()->json(['success' => true, 'code' => 200, 'id' => $persona->id, 'html' => $html], 200);
            }           
        }

        $view = View::make('personas.iframe.create_edit');
        $html = $view->render();
        return response()->json(['success' => false, 'code' => 404, 'message' => 'Error: No se encuentra una persona con esos criterios de búsqueda.', 'html' => $html], 404);
    }

     public function buscarpersona(Request $request){

        $personas=Empleado::select('empleados.id', 'personas.nombre')->join('personas', 'persona_id', '=', 'personas.id')->where('nombre', 'LIKE', '%'.$request->search.'%')->get();
        return response()->json($personas);
        // se hace la relación de la tabla empleado y persona, el empleado con su id y la persona porque tiene los campos que son el nombre//
        // '%'. comodin se utilizan para que al momento de hacer la busqueda regrese los valores que coincidan, aunque tenga mas datos antes o despues de los valores..

    }

}