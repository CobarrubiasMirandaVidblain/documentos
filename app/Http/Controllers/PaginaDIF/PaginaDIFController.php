<?php

namespace App\Http\Controllers\PaginaDIF;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Validator;

use App\Models\PaginaDIF\Denuncia;

class PaginaDIFController extends Controller
{
    
    public function denuncia(Request $request){

        $allowedOrigins = ['127.0.0.1', 'localhost', 'intranet.difoaxaca.gob.mx'];
        $origin = $request->server('REMOTE_ADDR');
        if(!in_array($origin, $allowedOrigins)) return new JsonResponse('No tienes acceso', 401);

        $validator = Validator::make($request->all(), [
            'nombre_completo' => 'string',
            'domicilio' => 'string',
            'email' => 'email|required_if:telefono,null',
            'telefono' => 'numeric|required_if:email,null',
            'asunto' => ['required', 'string', Rule::in(['QUEJA', 'SUGERENCIA', 'DENUNCIA'])],
            'comentario' => 'required|string',
            'archivos' => 'array',
            'archivos.*' => 'file'
        ]);

        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), 480);
        }
        try{

            DB::beginTransaction();

            $denuncia = Denuncia::create($request->all());

            foreach($request->archivos as $archivo){
                $path = $archivo->store('public/paginadif/denuncias');
                $denuncia->archivos()->create([
                    'nombre' => $archivo->getClientOriginalName(),
                    'tamanio' => $archivo->getClientSize(),
                    'url' => $path,
                    'tipo' => $archivo->getClientMimeType()
                ]);
            }

            DB::commit();
            return new JsonResponse($denuncia->load('archivos'), 201);

        }catch(\Exception $e){
            DB::rollBack();
            \Log::debug($e);
            return new JsonResponse($e->getMessage(), 409);
        }
    }

}