<?php

namespace App\Http\Controllers\SQL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class ValeController extends Controller
{
    public function __construct()
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vales = DB::connection('sqlsrv')->select('SELECT TOP (1000) * FROM [BdRecursosFinan].[dbo].[Tb_R_Vales]');
        if(request()->wantsJson()){
            return new JsonResponse($vales);
        }
        return view('sql.index', ['vales' => $vales]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->wantsJson()){
            $vale = DB::insert('INSERT INTO [BdRecursosFinan].[dbo].[Tb_R_Vales] (Cp_Ejercicio, Cp_FolioCaja, Cp_IdAreaTitular, Cp_IdTitular, Cp_AreaTitular, Cp_NombreTitular, Cp_IdSolicita, Cp_IdAreaSolicita, Cp_NombreSolicitante, Cp_CategoriaSol, Cp_RfcSol, Cp_DescripGasto, Cp_IdFuenteFto, Cp_IdAccion, Cp_EstadoVale, Cp_FechaVale, Cp_PrestamoPersonal, Cp_TipoEspecial, Cp_Usuario, cp_idSolicitudSICOPE) VALUES (?,?,?,?,)', [1, 'Dayle']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*public function lastID($anio)
    {
        if(request()->wantsJson()){
            $vales = DB::connection('sqlsrv')->select('SELECT MAX(Cp_FolioCaja)as ultimo_folio FROM [BdRecursosFinan].[dbo].[Tb_R_Vales] WHERE cp_ejercicio=?', [$anio]);
            return new JsonResponse($vales);
        }
        return new JsonResponse(['error' => 'No se pudo obtener el ultimo id insertado'], 403);
    }*/

    public function municipioIcono(){
        /*$datos = \App\Models\Region::with('distritos')->get();

        $municipiosF = [];
        foreach($datos as $region){
            foreach($region->distritos as $distrito){
                foreach($distrito->municipios as $municipio){
                    array_push($municipiosF, [
                        'nombre' => $municipio->nombre,
                        'id' => $municipio->id,
                        'icon' => 'icon-' . strtolower(str_replace(" ", "-", str_replace("Ñ", "ñ", $region->nombre)))
                    ]);
                }
            }
        }*/
        $municipiosF = \App\Models\Municipio::select('id', 'nombre')->get();

        return $municipiosF;
    }
}
