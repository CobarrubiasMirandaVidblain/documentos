<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Proveedor;

use View;

class ProveedorController extends Controller {

  public function select(Request $request) {
    $proveedores = Proveedor::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                            ->orWhere('rfc','LIKE', '%' . $request->input('search') . '%')
                            ->take(10)
                            ->get()
                            ->toArray();
    return response()->json($proveedores);
  }

  public function store(Request $request) {
    try {
      DB::beginTransaction();
        $exists_proveedor = Proveedor::where('nombre',$request['nombre'])
                ->orWhere('rfc',$request['rfc'])
                ->first();
        if($exists_proveedor)
          abort(409,'Conflicto, este proveeedor ya existe');
        $request['nombre'] = strtoupper($request['nombre']);
        $request['rfc'] = strtoupper($request['rfc']);
        $request['usuario_id'] = auth()->user()->id;
        Proveedor::create($request->all());
      DB::commit();
      return response()->json(array(true,'success'));
    } catch(Exception $e) {
      DB::rollBack();
      abort(500,$e-getMessage());
    }
  }

  public function create(Request $request) {
    //Si el usuario tiene un rol en el modulo alimentarios
    if(auth()->user()->usuariorolesalim->first()) {
      return view::make('asistenciaalimentaria::modal')
                  ->with([
                      'vista' => 'asistenciaalimentaria::licitaciones.create_proveedor',
                      'tipo'=>$request->get('id')
                  ])
                  ->render();
    }
  }
}