<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\AUTH;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\DataTables\DocumentosProgramasDataTable;
use App\Models\DocumentosPrograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use App\Models\Documento;
use App\Models\Programa;
use View;

class DocumentosProgramaController extends Controller{
    public function __construct(){
        $this->middleware(['auth','authorized']);
    }
    public function index(DocumentosProgramasDataTable $tabla,Request $request, $programa_id){
      return $tabla->with('programa_id',$programa_id)->render("programas.modals.$request->view.documentosProgramas",['programa'=>Programa::find($programa_id)]);
      //return AniosPrograma::where([['programa_id',$programa_id],['ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id]])->first()->id;
    }
    public function create(Request $request, $programa_id){
      return view::make("programas.modals.$request->view.documentosProgramaCreate",
      [
        'programa'=>Programa::find($programa_id)
      ])
      ->render();
      //return AniosPrograma::where([['programa_id',$programa_id],['ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id]])->first()->id;
    }
    public function store(Request $request,$programa_id){
      if($request->ajax()){
        try{
          if(!is_numeric($request->input('documento_id'))){
            $newDoc = Documento::create([
              'nombre'=>$request->input('documento_id'),
              'descripcion'=>$request->input('documento_id'),
              'usuario_id'=>Auth::User()->id
            ]);
            $request['documento_id']=$newDoc->id;
          }
          $DocProg['documento_id']=$request->input('documento_id');
          $DocProg['activo']=1;
          $DocProg['anios_programa_id']=AniosPrograma::where([['programa_id',$programa_id],['ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id]])->first()->id;
          $DocProg['usuario_id']=Auth::user()->id;
          DB::beginTransaction();
          $new=DocumentosPrograma::create($DocProg);
          DB::commit();
          return response()->json(array('success' => false, 'message' => 'insercion correcta '.$new->id   ));
        }catch(\Exception $e){
          DB::rollBack();
          return  new JsonResponse(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()),500);
        }
      }
    }
    public function update(){

    }
    public function destroy($programa_id,$docProg_id){
        try{
            DB::beginTransaction();
            DocumentosPrograma::find($docProg_id)->delete();
            DB::commit();
        }catch(\Exception $e){
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }
}
