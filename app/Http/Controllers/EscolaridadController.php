<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Escolaridad;

class EscolaridadController extends Controller {
    
    public function select(Request $request) {
        if($request->ajax()) {
            $escolaridades = Escolaridad::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                //->take(5)
                ->get()
                ->toArray();
            return response()->json($escolaridades);
        }
    }
}