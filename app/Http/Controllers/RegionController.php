<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Region;
use View;

class RegionController extends Controller{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('regiones.index');
    }

    public function list(Request $request) {
        $columns = array(
            0 => 'id',
            1 => 'nombre'
        );

        $totalData = Region::count();
        $start = $request->input('start');
        $limit = $request->input('length');
        $limit = ($limit==-1)?$totalData:$limit;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar=$request->input('columns.0.search.value','');

        $regiones = Region::where('id', 'like','%'.$buscar.'%')
        ->orwhere('nombre', 'like', '%'.$buscar.'%')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get();

        $totalFiltered=Region::where('id', 'like','%'.$buscar.'%')
        ->orwhere('nombre', 'like', '%'.$buscar.'%')
        ->offset($start)
        ->limit($limit)
        ->count();

        $data = array();

        if($regiones)
        {
            foreach($regiones as $region)
            {
                $nestedData['Identificador'] = $region->id;
                $nestedData['Nombre'] = $region->nombre;
                $nestedData['acciones'] ='
                <a href="' . route('regiones.edit', $region->id) . '" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Editar</a>
                <a class="btn btn-danger btn-sm" onclick="region_delete(' . $region->id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                ';
                // <a href="' . route('regiones.show', $region->id) . '" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Ver</a>
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('regiones.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if($request->ajax()){
            try {
                DB::beginTransaction();
                $new = Region::create($request->all());
                DB::commit();
                return response()->json(array('success' => true, 'id' => $new->id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $region = Region::findOrFail($id);
        return view('regiones.create_edit')->with('region',$region);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                Region::find($id)->update($request->all());

                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                Region::destroy($id);
                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
    
    public function select(Request $request){
        $items=Region::where('nombre', 'like', '%'.$request->input('search').'%')
                ->take(8)
                ->get();
        return response()->json($items);
    }
}
