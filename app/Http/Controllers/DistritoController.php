<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Distrito;
use View;


class DistritoController extends Controller
{
    public function index(){
        return view('distritos.index');
    }

    public function list(Request $request) {
        $columns = array(
            0 => 'cat_distritos.nombre',
            1 => 'cat_regiones.nombre'
        );

        $totalData = Distrito::count();
        $start = $request->input('start');
        $limit = $request->input('length');
        $limit = ($limit==-1)?$totalData:$limit;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar=$request->input('columns.0.search.value','');

        $distritos = Distrito::join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->where('cat_distritos.nombre', 'like','%'.$buscar.'%')
        ->orwhere('cat_regiones.nombre', 'like', '%'.$buscar.'%')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get(['cat_distritos.id','cat_distritos.nombre as distrito', 'cat_regiones.nombre as region']);

        $totalFiltered=Distrito::join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->where('cat_distritos.nombre', 'like','%'.$buscar.'%')
        ->orwhere('cat_regiones.nombre', 'like', '%'.$buscar.'%')
        ->count();

        $data = array();

        if($distritos){
            foreach($distritos as $distrito){
                $nestedData['Nombre'] = $distrito->distrito;
                $nestedData['Region'] = $distrito->region;
                $nestedData['Acciones'] ='
                <a href="' . route('distritos.edit', $distrito->id) . '" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Editar</a>
                <a class="btn btn-danger btn-sm" onclick="distrito_delete(' . $distrito->id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                ';
                // <a href="' . route('distritos.show', $distrito->id) . '" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Ver</a>
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('distritos.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if($request->ajax()){
            try {
                DB::beginTransaction();
                $new = Distrito::create($request->all());
                DB::commit();
                return response()->json(array('success' => true, 'id' => $new->id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $distrito = Distrito::findOrFail($id);
        return view('distritos.create_edit')->with('distrito',$distrito);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                Distrito::find($id)->update($request->all());

                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                Distrito::destroy($id);
                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
    public function select(Request $request){
        $items=Distrito::where('nombre', 'like', '%'.$request->input('search').'%')
                ->take(5)
                ->get();
        return response()->json($items);
    }
}
