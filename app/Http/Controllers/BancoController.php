<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Banco;

class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->wantsJson()){
            return new JsonResponse(Banco::all());
        }
        return view('bancos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banco = Banco::create($request->all());
        return new JsonResponse($banco);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Models\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function show(Banco $banco)
    {
        return new JsonResponse($banco);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Models\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banco $banco)
    {
        $banco->nombre = $request->nombre;

        $banco->save();

        return new JsonResponse($banco);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banco $banco)
    {
        $banco->delete();

        return new JsonResponse($banco);
    }

    public function bancosSelect(Request $request)
    {
        $bancos = Banco::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
        ->take(10)
        ->get()
        ->toArray();

        return response()->json($bancos);
    }
}
