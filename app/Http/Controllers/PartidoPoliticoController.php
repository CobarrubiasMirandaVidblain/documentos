<?php

namespace App\Http\Controllers;

use App\Models\PartidoPolitico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PartidoPoliticoController extends Controller {
    
    public function select(Request $request) {
        
        $partidos = PartidoPolitico::where('nombre', 'LIKE', '%' . $request->input('search') . '%')->orWhere('siglas', 'LIKE', '%'. $request->input('search') .'%')
            ->take(10)
            ->get()
            ->toArray();
        
        return response()->json($partidos);
    }
}