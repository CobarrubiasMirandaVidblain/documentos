<?php

namespace App\Http\Controllers;

use App\Models\Archivo;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller {
    public function store(Request $request) {
        $directorio_publico = 'public' . '/' . $request->carpeta . '/' . $request->id;
        $url_publico = Storage::putFile($directorio_publico, $request->file);
        $url_base = explode('public', $url_publico);
        $url_local = 'storage' . $url_base[1];

        $archivo = Archivo::create(array(
            'modulo_id' => 2,
            'tablabd_id' => 1,
            'registro_num' => $request->id,
            'url' => $url_local,
            'short_url' => '',
            'nombre' => basename($url_publico),
            'tamanio' => Storage::size($url_publico)
        ));
        
        $datos['id'] = $archivo->id;
        $datos['url'] = asset($url_local);
        $datos['nombre'] = $archivo->nombre;
        $datos['tamanio'] = $archivo->tamanio;
        
        return $datos;
    }
    
    public function destroy(Request $request) {
        Archivo::destroy($request->id);
    }
}