<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Dependencia;
use App\Models\Entidad;
use App\Models\TipoInstitucion;
use App\DataTables\DependenciasDataTable;
use View;

class DependenciaController extends Controller {

    public function __construct(){
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DependenciasDataTable $dataTable) {
        return $dataTable->render('dependencias.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $view = View::make('dependencias.iframe.create_edit')->with('entidades', Entidad::get())->with('tipos',TipoInstitucion::get());
        $html = $view->render();
        return response()->json(['html' => $html], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $request['usuario_id'] = $request->User()->id;
                $dependencia = Dependencia::findOrFail($id);
                $dependencia->update($request->all());
                auth()->user()->bitacora(request(), [
                  'tabla' => 'cat_instituciones',
                  'registro' => $dependencia->id . '',
                  'campos' => json_encode($dependencia) . '',
                  'metodo' => request()->method()
                ]);
                DB::commit();
                return response()->json(array('success' => true, 'id' => $id, 'tipo' => $dependencia->tipo));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $request["usuario_id"] = $request->user()->id;
                $dependencia = Dependencia::create($request->all());
                auth()->user()->bitacora(request(), [
                    'tabla' => 'cat_instituciones',
                    'registro' => $dependencia->id . '',
                    'campos' => json_encode($dependencia) . '',
                    'metodo' => request()->method()
                ]);
                DB::commit();
                return response()->json(array('success' => true, 'id' => $dependencia->id, 'nombre' => $dependencia->nombre, 'tipo' => $dependencia->tipo));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        return response()->json([
            'html' =>  view::make('dependencias.iframe.show')            
            ->with('dependencia', Dependencia::findOrFail($id))
            ->render() ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        return response()->json([
          'html' =>  view::make('dependencias.iframe.create_edit')
          ->with('entidades', Entidad::get())
          ->with('tipos',TipoInstitucion::get())
          ->with('dependencia', Dependencia::findOrFail($id))
          ->render() ], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $dependencia = Dependencia::findOrFail($id);
                $dependencia->delete();              
                auth()->user()->bitacora(request(), [
                  'tabla' => 'cat_instituciones',
                  'registro' => $dependencia->id . '',
                  'campos' => json_encode($dependencia) . '',
                  'metodo' => request()->method()
                ]);
                DB::commit();
                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
      }

    public function select(Request $request) {
        if ($request->ajax()) {            
            $dependencias = Dependencia::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->take(10)
            ->get()
            ->toArray();
            return response()->json($dependencias);
        }
    }
}