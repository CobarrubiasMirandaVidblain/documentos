<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\PreventBackHistory::class,
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        // \App\Http\Middleware\CheckModuleActive::class,
        \App\Http\Middleware\Cors::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\PreventBackHistory::class,
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        'api' => [
            \Modules\Fotradis\Http\Middleware\Api::class
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'preventBackHistory' => \App\Http\Middleware\PreventBackHistory::class,
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'authorized' => \App\Http\Middleware\VerifyAuthorized::class,
        'roles' => \App\Http\Middleware\CheckRoles::class,
        'rolModule' => \App\Http\Middleware\CheckRolModule::class,
        'saveaction' => \App\Http\Middleware\SaveAction::class,
        'cors' => \App\Http\Middleware\Cors::class,
        'recmat' => \Modules\Recmat\Http\Middleware\VerifyRecmatToken::class,
        'recmatRol' => \Modules\Recmat\Http\Middleware\CheckRol::class,
        'rolModuleV2' => \App\Http\Middleware\CheckRolModuloV2::class,
        'panaderia'=>\Modules\Panaderia\Http\Middleware\VerifyPanaderiaToken::class,
        'mitoken' => \Modules\Viaticos\Http\Middleware\VerificarToken::class,
        //Middleware para modulo de BancaDIF
        'apiBancaDIF' => \Modules\BancaDIF\Http\Middleware\VerifyToken::class,
        'apiSICODOC' => \Modules\Sicodoc\Http\Middleware\VerifySicodocToken::class,
        'taesir'=>\Modules\TAESIR\Http\Middleware\TallerSillas::class
    ];
}
