<?php

namespace App\DataTables\Canna;

use App\User;
use Yajra\DataTables\Services\DataTable;

use App\Models\Persona;

class CandidatosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
    	return datatables($query)
    	->editColumn('fecha_nacimiento', function($persona) {
    		return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
    	})
    	->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
    		$query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
    	})
    	->addColumn('edad', function($persona) {
    		return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
    	})
    	->orderColumn('edad', '-fecha_nacimiento $1')
    	->filterColumn('edad', function($query, $keyword) {
    		$edad = strtoupper($keyword);
    		if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)) {
    			preg_match_all('!\d+!', $edad, $valores);
    			preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);
    			if(count($valores[0]) === 2) {
    				$query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0] . " AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][1] . " " . $valores[0][1]);
    			}
    			else {
    				$query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0]);
    			}
    		}
    	})
    	->addColumn('consultar', function($persona) {
    		return '<a href="' . route('personas.show', $persona->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
    	})
    	->addColumn('editar', function($persona) {
    		// if($this->tipo === 'normal') {
    		//	return '<a href="' . route('personas.edit', $persona->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
    		// }
    		// if($this->tipo === 'bienestarsolicitantes' || $this->tipo === 'bienestartutores') {
    		return '<a class="btn btn-warning btn-xs" onclick="editar_persona(' . $persona->id . ');"><i class="fa fa-pencil"></i> Editar</a>';
    		// }
    	})
    	->addColumn('eliminar', function($persona) {
    		return '<a class="btn btn-danger btn-xs" onclick="persona_delete(' . $persona->id . ');"><i class="fa fa-trash"></i> Eliminar</a>';
    	})
    	->addColumn('seleccionar', function($persona) {
    		return '<a class="btn btn-success btn-xs" onclick="seleccionar_persona(' . "'" . $persona->id . "'" . ', ' . "'" . $persona->get_nombre_completo() . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
    	})
    	->rawColumns(['consultar', 'editar', 'eliminar', 'seleccionar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
    	$model = Persona::search()
    	->select(
    		[
    			'personas.id as id',
    			'personas.nombre as nombre',
    			'personas.primer_apellido as primer_apellido',
    			'personas.segundo_apellido as segundo_apellido',
    			'personas.fecha_nacimiento as fecha_nacimiento',
    			'personas.curp as curp',
    			'cat_regiones.nombre as region',
    			'cat_distritos.nombre as distrito',
    			'cat_municipios.nombre as municipio',
    			'cat_localidades.nombre as localidad'
    		]
    	);
    	if($this->request->input('tipo') === 'mayor') {
    		$model->whereRaw("TIMESTAMPDIFF(YEAR,personas.fecha_nacimiento,'".date('Y-m-d')."') >= 18");
    	}
    	elseif ($this->request->input('tipo') === 'menor') {
    		$model->whereRaw("TIMESTAMPDIFF(YEAR,personas.fecha_nacimiento,'".date('Y-m-d')."') < 18");
    	}
    	return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
    	return $this->builder()
    	->columns($this->getColumns())
    	->ajax([
            'data'=>'function(d){
                if(elemento !== undefined){
                    if(elemento.attr("id") == "tutor_id" || elemento.attr("id") == "tutor2_id"){
                        d.tipo = "mayor";
                    }
                    else if(elemento.attr("id") == "beneficiario_id"){
                    	d.tipo = "menor";
                    }
                }
            }'])
    	->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
    	return [
    		'id'                => ['data' => 'id', 'name' => 'personas.id'],
    		'nombre'            => ['data' => 'nombre', 'name' => 'personas.nombre'],
    		'primer_apellido'   => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
    		'segundo_apellido'  => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
    		'fecha_nacimiento'  => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
    		'edad'              => ['data' => 'edad', 'name' => 'edad'],
    		'curp'              => ['data' => 'curp', 'name' => 'personas.curp'],
    		'region'            => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
    		'distrito'          => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
    		'municipio'         => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
        'localidad'         => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
        //'editar'            => ['data' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
    		'seleccionar'       => ['data' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
    	];
    }

    protected function getBuilderParameters(){
    	$builderParameters =
    	[
    		'language' => [
    			'url' => 'http://intradif.oo/bower_components/datatables.net-responsive/js/Spanish.json'
    		],
    		'dom' => 'Btip',
    		'buttons' => [
    			[
    				'extend' => 'reset',
    				'text' => '<i class="fa fa-undo"></i> Reiniciar',
    				'className' => 'button-dt tool'
    			],
    			[
    				'extend' => 'reload',
    				'text' => '<i class="fa fa-refresh"></i> Recargar',
    				'className' => 'button-dt tool'
    			]
    		],
            'preDrawCallback' => 'function() { if(elemento !== undefined) block(); }',
            'drawCallback' => 'function() { unblock(); }',
    		'lengthMenu' => [ 10 ],
    		'responsive' => true,
    		'columnDefs' => [
    			[ 'className' => 'text-center', 'targets' => '_all' ]
    		]
    	];

    	return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
    	return 'Canna/Candidatos_' . date('YmdHis');
    }
}
