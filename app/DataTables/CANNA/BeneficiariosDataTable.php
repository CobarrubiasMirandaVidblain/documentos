<?php

namespace App\DataTables\Canna;

use App\User;
use Yajra\DataTables\Services\DataTable;

use App\Models\AlumnosCanna;

class BeneficiariosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
      return datatables($query)
      ->editColumn('folio', function($alumnoscanna){
        return ($alumnoscanna->folio);
      })
      ->editColumn('nombre', function($alumnoscanna){
        return ($alumnoscanna->persona->obtenerNombreCompleto());
      })
      ->editColumn('fecha_registro', function($alumnoscanna){
        setlocale(LC_ALL, "es_ES");
        $fecha = explode(',', date('m,d,Y', strtotime($alumnoscanna->created_at)));
        return mb_strtoupper(strftime('%A %e de %B de %Y', mktime(0,0,0, $fecha[0], $fecha[1], $fecha[2])));
      })
      ->addColumn('estado',function($alumnoscanna){
          if ($alumnoscanna->deleted_at == null)
              return '<input type="checkbox"><div class="slider"></div>';
          return 'Baja';
      })
      ->addColumn('ver', function($alumnoscanna) {
      	  if ($alumnoscanna->deleted_at != null)
      	  	return '<a class="btn btn-info btn-xs disabled" ><i class="fa fa-address-card"></i> Visualizar</a>';
          return '<a class="btn btn-info btn-xs" onclick="ver_alumno(' . "'" . $alumnoscanna->id . "'" . ');"><i class="fa fa-address-card"></i> Visualizar</a>';
      })
      ->addColumn('editar', function($alumnoscanna) {
          return '<a class="btn btn-warning btn-xs" onclick="editar_alumno(' . "'" . $alumnoscanna->id . "'" . ');"><i class="fa fa-fw fa-edit"></i> Editar</a>';
      })
      ->editColumn('estado',function($alumnoscanna){
      	if ($alumnoscanna->deleted_at == null)
      		return '<input id="'.$alumnoscanna->id.'" checked data-toggle="toggle" data-on="Activo" data-off="Inactivo" data-onstyle="success" data-offstyle="danger" data-size="mini" type="checkbox" class="myToggle">';
      	return '<input id="'.$alumnoscanna->id.'" data-toggle="toggle" data-on="Activo" data-off="Inactivo" data-onstyle="success" data-offstyle="danger" data-size="mini" type="checkbox" class="myToggle">';
        })
      ->rawColumns(['ver','editar','estado']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return AlumnosCanna::withTrashed()->orderBy('deleted_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
      $columns =
      [
        'Folio' => ['data' => 'folio', 'name' => 'folio'],
        'Nombre' => ['data' => 'nombre', 'name' => 'nombre'],
        'Fecha de registro' => ['data' => 'fecha_registro', 'name' => 'fecha_registro'],
        'Estado' => ['data' => 'estado', 'name' => 'estado'],
        'Ver' => ['data' => 'ver', 'searchable' => false, 'orderable' => false],
        //'Editar' => ['data' => 'editar', 'searchable' => false, 'orderable' => false]
        //'estado' => ['data' => 'estado', 'searchable' => false, 'orderable' => false]
      ];

      return $columns;
    }

    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Canna/Beneficiarios_' . date('YmdHis');
    }
}
