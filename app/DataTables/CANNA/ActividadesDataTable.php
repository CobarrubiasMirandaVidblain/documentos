<?php

namespace App\DataTables\Canna;

use App\User;
use Yajra\DataTables\Services\DataTable;

use App\Models\Programa;

class ActividadesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->editColumn('nombre', function($programa){
          return ($programa->nombre);
        })
        ->editColumn('descripcion', function($programa){
          return ($programa->descripcion);
        })
        ->editColumn('codigo', function($programa){
          return ($programa->codigo);
        })
        ->addColumn('editar', function($programa) {
            return '<a class="btn btn-warning btn-xs" onclick="editar_actividad(' . "'" . $programa->id . "'" . ');"><i class="fa fa-fw fa-edit"></i> Editar</a>';
        })
        ->rawColumns(['editar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Programa::whereRaw('padre_id = ' . Programa::where('nombre', 'CANNA')->first()->id)->orderBy('id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
          'Programa' => ['data' => 'nombre', 'name' => 'nombre'],
          'Descripción' => ['data' => 'descripcion', 'name' => 'descripcion', 'orderable' => false],
          'Codigo' => ['data' => 'codigo', 'name' => 'codigo'],
          'Editar' => ['data' => 'editar', 'searchable' => false, 'orderable' => false]
        ];
    }

    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => 'http://intradif.oo/bower_components/datatables.net-responsive/js/Spanish.json'
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Canna/Actividades_' . date('YmdHis');
    }
}
