<?php

namespace App\DataTables;

use App\Models\Programa;
use App\Models\BeneficiosprogramasSolicitud;
use App\Models\StatusSolicitud;
use App\Models\Ejercicio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;

class PeticionesDataTable extends DataTable {

    private $peticiones_nuevas = 0;
    private $peticiones_proceso = 0;
    private $peticiones_finalizadas = 0;
    private $peticiones_canceladas = 0;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)        
        ->editColumn('fecha', function($peticion) {
            return with(\Carbon\Carbon::parse($peticion->fecha))->format('d-m-Y');
        })
        ->filterColumn('fecha', function($query, $keyword) {
            $query->whereRaw("(select DATE_FORMAT(created_at, '%d-%m-%Y') from estados_solicitudes where beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id and statusproceso_id = 3) like ?", ["%$keyword%"]);
        })
        ->orderColumn('fecha', 'fecha $1')
        ->filterColumn('remitente', function($query, $keyword) {
            $query->whereRaw("CASE ".
	                        "WHEN solicitudes.tiposremitente_id = 4 THEN ".
                                "( SELECT CONCAT(p.nombre,' ',p.primer_apellido,' ',p.segundo_apellido) ".
                                    "FROM solicitudes_personales sp, personas p ".
                                        "WHERE sp.persona_id = p.id AND solicitudes.id = sp.solicitud_id) ".
                            "WHEN solicitudes.tiposremitente_id = 3 THEN ".
                                "( SELECT d.nombre ".
                                    "FROM solicitudes_dependencias sd, cat_instituciones d ".
                                        "WHERE sd.dependencia_id = d.id AND solicitudes.id = sd.solicitud_id) ".
                            "WHEN solicitudes.tiposremitente_id = 2 THEN ".
                                "( SELECT m.nombre ".
                                    "FROM solicitudes_municipios sm, cat_municipios m ".
                                        "WHERE sm.municipio_id = m.id AND solicitudes.id = sm.solicitud_id) ".
		                    "WHEN solicitudes.tiposremitente_id = 1 THEN ".
                                "( SELECT r.nombre ".
                                    "FROM solicitudes_regiones sr, cat_regiones r ".
                                        "WHERE sr.region_id = r.id AND solicitudes.id = sr.solicitud_id) ".
                        "END like ?", ["%$keyword%"]);
        })
        ->filterColumn('dias_transcurridos', function($query, $keyword){
            $query->whereRaw("CASE WHEN IFNULL((SELECT count(*) from cat_statusprocesos where beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id AND statusproceso_id >= 6 AND statusproceso_id != 9 ), -1) = 0 
            THEN (SELECT DATEDIFF(CURDATE(), (SELECT created_at FROM estados_solicitudes WHERE beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id AND statusproceso_id = 3))) 
            ELSE (SELECT DATEDIFF((SELECT created_at FROM estados_solicitudes WHERE	beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id AND statusproceso_id >= 6 AND statusproceso_id != 9 AND estados_solicitudes.deleted_at is null), 
            (SELECT created_at FROM estados_solicitudes WHERE beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id	AND statusproceso_id = 3))) END = ?", [$keyword]);
        })
        ->orderColumn('dias transcurridos', 'dias_transcurridos $1')
        ->filterColumn('estatus', function($query, $keyword) {
            $keyword = str_replace("NUEVO","VINCULADO, Vo. Bo.",$keyword);
            $keyword = str_replace("VALIDANDO","VALIDANDO, LISTA DE ESPERA",$keyword);
            $keyword = str_replace("CANCELADO","CANCELADO , RECHAZADO",$keyword);
            $aux = explode(',',$keyword);
            $query->where(function ($qry) use ($aux){
                for ($i=0; $i < count($aux); $i++) {
                    $qry->orwhere('cat_statusprocesos.status','like',"%$aux[$i]%");
                }
            });
            /* if($keyword === 'VALIDANDO'){
                $query->whereRaw("cat_statusprocesos.status like ? or cat_statusprocesos.status like ?", ["%$keyword%",'LISTA DE ESPERA']);
            }elseif($keyword === 'CANCELADO'){
                $query->whereRaw("cat_statusprocesos.status like ? or cat_statusprocesos.status like ?", ["%$keyword%",'RECHAZADO']);
            }else{
                if($this->programa_padre == 'TODOS' && $keyword == 'Vo. Bo.'){
                    $query->whereIn('cat_statusprocesos.status',['Vo. Bo.', 'SOLICITANTE']);
                }else{
                    $query->whereRaw("cat_statusprocesos.status like ?", ["%$keyword%"]);
                }
            } */            
        })
        ->addColumn('estatus', function($peticion) {
            $status = '';
            switch ($peticion->estatus) {
                case "SOLICITANTE":
                    $status = '<span class="label label-warning">SOLICITANTE</span>';
                    break;
                case "VINCULADO":
                    $status = '<span class="label label-success">VINCULADO</span>';
                    break;
                case "VALIDANDO":
                    $status = '<span class="label label-warning">VALIDANDO</span>';
                    break;
                case "LISTA DE ESPERA":
                    $status = '<span class="label label-info">LISTA DE ESPERA</span>';
                    break;
                case "CANCELADO":
                    $status = '<span class="label label-danger">CANCELADO</span>';
                    break;
                case "RECHAZADO":
                    $status = '<span class="label label-danger">RECHAZADO</span>';
                    break;
                case "FINALIZADO":
                    $status = '<span class="label label-primary">FINALIZADO</span>';
                    break;
                case "Vo. Bo.":
                    $status = '<span class="label label-primary">Vo. Bo.</span>';
                    break;
            }
            return $status;
        })
        ->orderColumn('estatus', 'estatus $1')
        ->addColumn('turnar', function($peticion) {
            // dd($peticion);
            if($this->permiso_para_turnar && $peticion->estatus === 'SOLICITANTE'){
                //Comprobando que si AtnCiudadana puede agregar beneficiarios y que el numero de eneficiariarios es igual al solicitado
                if( (Programa::find($peticion->beneficio->anioprograma->last()->programa_id)->puede_agregar_benef === 1 && 
                    ProgramasSolicitud::find($peticion->beneficioprograma_solicitud_id)->solicitudespersonas()->whereNull('observacionevaluado')->count() === ProgramasSolicitud::find($peticion->beneficioprograma_solicitud_id)->cantidad)
                    ||
                    (Programa::find($peticion->beneficio->anioprograma->last()->programa_id)->puede_agregar_benef === 0)){
                        return '<a onclick="turnar(' . $peticion->solicitud_id . ', ' . $peticion->programa_id . ');" class="btn btn-warning btn-xs"><i class="fa fa-paper-plane-o"></i> Turnar</a>';
                }                
            }
        })
        ->addColumn('seguimiento', function($peticion) {
            // dd($this->modulo);
            $modulo = $this->modulo ? "{$this->modulo}." : '';
            
            $ruta = route("{$modulo}peticiones.show", ['peticion_id' => $peticion->beneficioprograma_solicitud_id]);
            //Sólo para CAPTURISTAS
            // if(!auth()->user()->hasRolesModulo(['ADMINISTRADOR DE PETICIONES'],$this->request->modulo_id) && $this->modulo != ""){
                if($peticion->estatus !== 'SOLICITANTE' && $peticion->estatus !== 'VINCULADO')
                return "<a class='btn btn-primary btn-xs' href='" .$ruta."'><i class='fa fa-arrow-right'></i> Seguimiento</a>";
            // }
        })        
        ->addColumn('Vo Bo', function($peticion) {
            if($this->area_id && auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES']) && $peticion->estatus === 'VINCULADO'){
                return "<a class='btn btn-primary btn-xs' onclick='Peticion.autorizar(".$peticion->beneficioprograma_solicitud_id.")'><i class='fa fa-arrow-right'></i> Vo. Bo.</a>";
            }            
        })
        ->rawColumns(['estatus','seguimiento','Vo Bo','turnar'])
        ->with('peticiones_nuevas', $this->peticiones_nuevas)
        ->with('peticiones_proceso', $this->peticiones_proceso)
        ->with('peticiones_finalizadas', $this->peticiones_finalizadas)
        ->with('peticiones_canceladas', $this->peticiones_canceladas);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model;
        //dd('PP-'.$this->programa_padre.'AREA-'.$this->area_id);
        $model = BeneficiosprogramasSolicitud::peticiones($this->programa_padre, $this->area_id);
        if($this->area_id && auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES'])){
            $model->whereNotIn('cat_statusprocesos.status', ['SOLICITANTE']);
        }elseif($this->permiso_para_turnar){
        }      
        else{
            //PARA CAPTURISTAS Y JEFES O <COORDINADORES></COORDINADORES>
            $model->whereNotIn('cat_statusprocesos.status', ['SOLICITANTE','VINCULADO']);
        }
        if($this->tipo_solicitud){
          $model->where('solicitudes.folio','LIKE',$this->tipo_solicitud.'%');
        }
        
        $ejercicio = $this->request->ejercicio ? $this->request->ejercicio : date('Y');
        
        $model->whereRaw("(select DATE_FORMAT(created_at, '%d-%m-%Y') from estados_solicitudes where beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id and statusproceso_id = 3) like ?", ["%$ejercicio%"]);
        if($this->area_id)
            $model->where(function($query){
                $area_id = Auth::user()->persona->empleado->area_id;
                if(!$this->atnCiudadana || $area_id != 1){
                    $beneficios = array_map( function($o) { return $o->id; }, DB::select('call getProgramas(?)',[$area_id]));
                    $beneficios[$area_id] = $area_id;
                    $query->whereIn('programas.id', $beneficios);                        
                }
            });
        else if ($this->programas_id){
            $model->whereIn('programas.id',$this->programas_id);
        }
        
        $peticiones_nuevas = clone $model;
        $peticiones_proceso = clone $model;
        $peticiones_finalizadas = clone $model;
        $peticiones_canceladas = clone $model;

        if($this->area_id && auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES'])){
            $this->peticiones_nuevas = $peticiones_nuevas->where('cat_statusprocesos.status','VINCULADO')->count();
        }else{
            /* if($this->programa_padre == 'TODOS'){
                $this->peticiones_nuevas = $peticiones_nuevas->whereIn('cat_statusprocesos.status',['Vo. Bo.', 'SOLICITANTE'])->count();
            }else{ */
                $this->peticiones_nuevas = $peticiones_nuevas->where('cat_statusprocesos.status','Vo. Bo.')->count();
            // }
        }
        $this->peticiones_proceso = $peticiones_proceso->whereIn('cat_statusprocesos.status',['VALIDANDO', 'LISTA DE ESPERA'])->count();
        $this->peticiones_finalizadas = $peticiones_finalizadas->where('cat_statusprocesos.status','FINALIZADO')->count();
        $this->peticiones_canceladas = $peticiones_canceladas->whereIn('cat_statusprocesos.status',['CANCELADO','RECHAZADO'])->count();

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.ejercicio = Peticion.ejercicio; }'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns = [
            'folio' => ['data' => 'folio', 'name' => 'beneficiosprogramas_solicitudes.folio'],
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'remitente' => ['data' => 'remitente', 'name' => 'remitente'],
            'tipo' => ['data' => 'tipo', 'name' => 'cat_tiposremitentes.tipo'],
            'apoyo' => ['data' => 'apoyo', 'name' => 'beneficiosprogramas.nombre'],
            'cantidad' => ['data' => 'cantidad', 'name' => 'beneficiosprogramas_solicitudes.cantidad'],
            'área responsable' => ['data' => 'area_responsable', 'name' => 'cat_areas.nombre'],
            'días transcurridos' => ['data' => 'dias_transcurridos' ],
            'estatus' => ['data' => 'estatus', 'name' => 'estatus', 'searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false]            
        ];
        
        //Si es DIRECTOR se le muestra el botón para dar el Vo. Bo.
        if(auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES']))
            $columns['Vo Bo'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        if($this->permiso_para_turnar)
            $columns['turnar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];    
        //SI ES UN CAPTURISTA o mas bien, NO ES DIRECTOR
        if(auth()->user()->hasRoles(['SEGUIMIENTO DE PETICIONES'])){
          $columns['seguimiento'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }
        return $columns;
    }

    protected function getBuilderParameters() {
        $ejercicios = '<div class="dropdown"><a style="color: #d12654" id="btnEjercicio" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
        $ejercicios = $ejercicios . 'Ejercicio: ' . date('Y') . ' <span class="caret"></span></a><ul id="menuEjercicio" class="dropdown-menu">';
          foreach(Ejercicio::orderBy('anio', 'desc')->take(3)->get() as $ejercicio){
            $ejercicios = $ejercicios . '<li><a href="#">' . $ejercicio->anio . '</a></li>';
        }
        $ejercicios = $ejercicios . '</ul></div>';

        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'initComplete' => 'function(){
                $("#menuEjercicio li a").click(function(){
                    Peticion.ejercicio = $(this).text()
                    Peticion.filtrar_peticiones(1, $(this).text())
                    $("#btnEjercicio").html("Ejercicio: " + $(this).text() + " <span class=\"fa fa-caret-down\"></span>")
                    $("#btnEjercicio").val("Ejercicio: " + $(this).text());
                });
                var api = this.api()
                api.columns().every(function () {
                    var column = this;
                    if(api.table().init().columns[column[0]].searchable){
                        var input = document.createElement("input")
                        $(input).addClass("column-filter")
                        $(input).addClass("text-center")
                        $(input).attr("placeholder","Filtrar "+column.header().textContent.toLowerCase())
                        $(input).appendTo($(column.footer()).empty())
                        .on("change", function () {
                            column.search($(this).val(), false, false, true).draw()
                        });
                    }
                });
                /* $("#peticiones thead").append($("#peticiones thead")[0].innerHTML)
                $("#peticiones thead tr:eq(1) th").each( function () {
                        var title = $(this).text();
                        $(this).removeClass("sorting").removeClass("sorting_desc")
                        $(this).html( LaravelDataTables.peticiones.init().columns[$(this).index()].searchable ? "<input type=\"text\" placeholder=\"Search "+title+"\" class=\"column_search\" />" : "" );
                    } );
                $( "#peticiones thead").on("change", ".column_search",function () {
                        LaravelDataTables.peticiones
                            .column( $(this).parent().index() )
                            .search( this.value )
                            .draw();
                    } ); */
            }',
            'buttons' => [
                [
                    'className' => 'button-dt tool dropdown-toggle',
                    'text' => $ejercicios
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]                            
            ],
            // 'scrollX'=>true,
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'stateSave'  => true,
            'order' => [
                1,
                'desc'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Peticiones_' . date('YmdHis');
    }
}