<?php

namespace App\DataTables\Fotradis;

use Illuminate\Support\Facades\DB;
use App\Models\ControlRutas;
use Yajra\DataTables\Services\DataTable;

class HistorialConductoresAsignados extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return dataTables($query)
                ->addColumn('ruta', function($rutas) {
                  switch ($rutas->recorrido_id) {
                    case 1:
                    return '<span class="label label-r01">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                    case 2:
                    return '<span class="label label-r02">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                    case 3:
                    return '<span class="label label-r03">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                    case 4:
                    return '<span class="label label-r04">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                    case 5:
                    return '<span class="label label-r05">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                    case 6:
                    return '<span class="label label-r06">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                    case 100:
                    return '<span class="label label-r07">'. strtoupper($rutas->recorrido) .'</span>';
                        break;
                  }
                })
                ->addColumn('servicio', function($rutas) {
                    if ($rutas->tiposervicio == 'taxi') {
                      return '<span class="label label-default"><i class="fa fa-fw fa-taxi text-green"></i>TAXI</span>';                        
                    } elseif ($rutas->tiposervicio == 'urban') {
                      return '<span class="label label-default"><i class="fa fa-fw fa-bus text-red"></i>URBAN</span>';
                    }
                })
                ->filterColumn('conductor', function($query, $keyword) {
                  $query->whereRaw("concat(personas.nombre,' ',personas.primer_apellido,' ',personas.segundo_apellido) like ?", ["%$keyword%"]);
                })
                ->rawColumns(['ruta','servicio']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $rutas = ControlRutas::withTrashed()
          ->HistorialConductores()
          ->whereNotNull('controlrutas.deleted_at')
          ->select([
            'unidades.numero_unidad as unidad',
            DB::raw('concat(personas.nombre,\' \',personas.primer_apellido,\' \',personas.segundo_apellido) as conductor'),
            'unidades.tipo_servicio as tiposervicio',
            'recorridos.id as recorrido_id',
            'recorridos.nombre as recorrido',
            'controlrutas.created_at',
            'controlrutas.deleted_at'
          ]);
        return $rutas;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            '# Unidad'    => ['data' => 'unidad', 'name' => 'unidades.numero_unidad'],
            'Conductor' => ['data' => 'conductor', 'name' => 'conductor'],
            'Servicio' => ['data' => 'servicio', 'name' => 'unidades.tipo_servicio'],
            'Recorrido' => ['data' => 'ruta', 'name' => 'recorridos.nombre'],
            'Fecha creación asignación' => ['data' => 'created_at', 'name' => 'controlrutas.created_at'],
            'Fecha eliminación asignación' => ['data' => 'deleted_at', 'name' => 'controlrutas.deleted_at']
        ];
        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                  'extend' => 'reset',
                  'text' => '<i class="fa fa-undo"></i> Reiniciar',
                  'className' => 'button-dt tool'
                ],
                [
                  'extend' => 'reload',
                  'text' => '<i class="fa fa-refresh"></i> Recargar',
                  'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        
    }
}
