<?php

namespace App\DataTables\Fotradis;

use App\Models\Unidad;
use App\Models\ControlRutas;
use Yajra\DataTables\Services\DataTable;

class UnidadesActuales extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->editColumn('matricula', function($unidades) {
        	return strtoupper($unidades->matricula);
        })
        ->editColumn('tipo_servicio', function($unidades) {
        	return strtoupper($unidades->tipo_servicio);	
        })
        ->addColumn('ver', function($unidades) {
            return '<a class="btn btn-info btn-sm" onclick="ver.unidad(' . "'" . $unidades->matricula . "'" . ');"><i class="fa fa-fw fa-eye"></i></a>';
        })
        ->addColumn('editar', function($unidades) {
            return '<a class="btn btn-warning btn-sm" onclick="lanzar.mensaje_de_alerta(' . "'" . $unidades->matricula . "','" . "editar". "'" . ');"><i class="fa fa-fw fa-edit"></i></a>';
        })
        ->addColumn('eliminar', function($unidades) {
            $count = ControlRutas::join('unidades','unidades.id','controlrutas.unidad_id')
                        ->where('unidades.id','=',$unidades->id)
                        ->whereNull('controlrutas.fechahora_sesion_fin')->count();
            if($count > 0)
                return '<a class="btn btn-danger btn-sm disabled"><i class="fa fa-trash-o"></i></a>';
            return '<a class="btn btn-danger btn-sm" onclick="lanzar.mensaje_de_alerta(' . "'" . $unidades->id . "','" . "eliminar". "'" . ');"><i class="fa fa-trash-o"></i></a>';
        })
        ->addColumn('servicios', function($unidades) {
            return '<a class="btn btn-error btn-xs" onclick="addMantenimiento(' . $unidades->id . ',0);"><i class="fa fa-fw fa-plus-circle"></i></a>'.
            '<a class="btn btn-success btn-xs" onclick="listMantenimiento('  . $unidades->id . ');"><i class="fa fa-fw fa-list-ol"></i></a>';
        })
        ->rawColumns(['ver', 'editar', 'eliminar', 'servicios']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return Unidad::all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            '# Unidad'    => ['data' => 'numero_unidad', 'name' => 'numero_unidad'],
            'Matrícula'    => ['data' => 'matricula', 'name' => 'matricula'],
            'Servicio'       => ['data' => 'tipo_servicio', 'name' => 'tipo_servicio'],
            'Ver'  => ['data' => 'ver', 'searchable' => false, 'orderable' => false],
			'Editar'   => ['data' => 'editar', 'searchable' => false, 'orderable' => false],
            'Eliminar'     => ['data' => 'eliminar', 'searchable' => false, 'orderable' => false],
            'Servicios/Mantenimientos'     => ['data' => 'servicios', 'searchable' => false, 'orderable' => false]
            // 'Generar'  => ['data' => 'bitacora', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        if ($this->tipo=='detalles') {
            return "Lista dispersion {$this->lista->nombre}";
        }
        return 'listas de dispersion_' . date('YmdHis');
    }
}
