<?php

namespace App\DataTables\Fotradis;

use Illuminate\Support\Facades\DB;

use App\Models\ControlRutas;
use App\Models\Rutas;
use App\Models\Unidad;
use App\Models\Conductor;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Services\DataTable;

class Sesiones extends DataTable{

    public function dataTable($query){
      $table = dataTables($query)
        ->addColumn('detalles', function($sesiones) {
            return "<span class='label label-info' onclick='showSession($sesiones->id)'> <i class='fa fa-eye'></i> DETALLES</span>";
        })
        /* ->addColumn('servicios', function($sesiones) {
            return ;
         })*/
        ->addColumn('estatus', function($sesiones) {
          setlocale(LC_TIME, "es_MX.UTF-8");
          $preIni = explode(',', date('m,d,Y', strtotime($sesiones->fechahora_sesion_ini)));
          $preFin = explode(',', date('m,d,Y', strtotime($sesiones->fechahora_sesion_ini)));
          $fechaIni = mb_strtoupper(strftime('%A %e de %B de %Y', mktime(0,0,0, $preIni[0], $preIni[1], $preIni[2])));
          $fechaFin = mb_strtoupper(strftime('%A %e de %B de %Y', mktime(0,0,0, $preFin[0], $preFin[1], $preFin[2])));
          if ($sesiones->fechahora_sesion_fin) {
            return '<span class="label label-danger" title="sesion terminada el '.$fechaFin.'"> Finalizado </span>';
        }
            return '<span class="label label-success" title="sesion iniciada el '.$fechaIni.'"> Activo </span>';
        })
        ->addColumn('ruta', function($sesiones) {
            if ($sesiones->recorrido == 'TAXI') {
                return '<span class="label label-default text-green"><i class="fa fa-fw fa-taxi text-green"></i> TAXI</span>';
            } else {
                  switch ($sesiones->recorrido_id) {
                    case 1:
                    return '<span class="label label-r01"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                    case 2:
                    return '<span class="label label-r02"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                    case 3:
                    return '<span class="label label-r03"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                    case 4:
                    return '<span class="label label-r04"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                    case 5:
                    return '<span class="label label-r05"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                    case 6:
                    return '<span class="label label-r06"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                    case 7:
                    return '<span class="label label-r06"><i class="fa fa-fw fa-bus"></i>'. strtoupper($sesiones->recorrido) .'</span>';
                        break;
                  }
            }
        })
        ->rawColumns(['detalles', 'estatus', 'ruta']);
        return $table;
    }


    public function query(){
        $sesiones = ControlRutas::join('unidades','unidades.id','controlrutas.unidad_id')
        ->join('conductores','conductores.id','controlrutas.conductor_id')
        ->join('empleados','empleados.id','conductores.empleado_id')
        ->join('personas','personas.id','empleados.persona_id')
        ->join('recorridos','recorridos.id','controlrutas.recorrido_id');
        if(!($this->request->input('showRecord')=='true'))
            $sesiones->whereNull('controlrutas.fechahora_sesion_fin');
        $sesiones->select('controlrutas.id','controlrutas.fechahora_sesion_fin','controlrutas.fechahora_sesion_ini','recorridos.id as recorrido_id','recorridos.nombre as recorrido','conductores.clave_conductor',DB::raw('CONCAT(personas.nombre," ",primer_apellido," ",segundo_apellido) as conductor'),'matricula','unidades.numero_unidad','fechahora_sesion_ini','fechahora_sesion_fin');
        return $sesiones;
    }


    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->ajax(['data' => 'function(d) { d.showRecord = $("#showRecord").prop("checked"); }'])
        ->parameters($this->getBuilderParameters());
    }

    protected function getColumns(){
        $columns =
        [
            '# Unidad'          => ['data' => 'numero_unidad', 'name' => 'unidades.numero_unidad'],
            'Matricula'         => ['data' => 'matricula', 'name' => 'matricula'],
            'Conductor'         => ['data' => 'conductor', 'name' => 'conductor'],
            'Clave'             => ['data' => 'clave_conductor', 'name' => 'clave_conductor'],
            'EstadoServicio'    => ['data' => 'estatus', 'name' => 'estatus'],
            'Recorrido'         => ['data' => 'ruta', 'name' => 'recorrido'],
            //'Ver'               => ['data' => 'detalles', 'searchable' => false, 'orderable' => false],
            // 'Eliminar' => ['data' => 'eliminar', 'searchable' => false, 'orderable' => false]
        ];
        return $columns;
    }


    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            /* 'preDrawCallback' => 'function() { block(); }', */
            'drawCallback' => 'function() {
                var aux = $("#showRecord").iCheck({
                    checkboxClass: "icheckbox_flat-orange",
                });
                aux.on("ifToggled", function(event){
                    $("#sesiones").DataTable().ajax.reload();
                });
                /* unblock(); */
            }',
            //'buttons' => null,
            //'autoWidth' => true,
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }


    protected function filename(){
        
    }
}
