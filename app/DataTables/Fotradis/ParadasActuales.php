<?php

namespace App\DataTables\Fotradis;

use App\Models\Paradas;
use App\Models\ParadasRutas;
use App\Models\Rutas;
use Yajra\DataTables\Services\DataTable;

class ParadasActuales extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->editColumn('parada', function($parada) {
            return strtoupper($parada->parada);
        })
        ->editColumn('referencia', function($parada) {
            return strtoupper($parada->referencia);
        })
        ->editColumn('numero_estacion', function($parada) {
            return strtoupper($parada->numero_estacion);
        })
        ->addColumn('ver', function($parada) {
            return '<a class="btn btn-success btn-xs" onclick="ver.ubicacion_parada(' . $parada->latitud . "," . $parada->longitud . ');"><i class="fa fa-fw fa-map-marker"></i> parada</a>';
        })
        ->addColumn('editar', function($parada) {
            return '<a class="btn btn-warning btn-xs" onclick="obtener_datos_parada(' . $parada->parada_id . ',' . $parada->ruta_id . ')"><i class="fa fa-fw fa-edit"></i> parada</a>';
        })
        ->addColumn('eliminar', function($parada) {
            return '<a class="btn btn-danger btn-xs" onclick="eliminar.parada_ruta(' . "'" . $parada->parada_id . "','" . $parada->ruta_id . "'" . ');"><i class="fa fa-trash-o"></i> parada</a>';
        })
        ->rawColumns(['ver', 'editar', 'eliminar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $paradas = ParadasRutas::join("paradas", "paradas.id", "=", "paradas_rutas.parada_id")
        ->where("paradas_rutas.ruta_id", "=", $this->request->input('ruta'))->where("paradas_rutas.tipoparada", "!=", "F")->orderBy('paradas_rutas.numero_estacion')->get(['paradas.id as parada_id', 'paradas_rutas.ruta_id as ruta_id', 'paradas.latitud as latitud', 'paradas.longitud as longitud', 'paradas.nombre as parada', 'paradas.referencia as referencia', 'paradas_rutas.numero_estacion']);

        return $paradas;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'Nombre'    => ['data' => 'parada', 'name' => 'parada'],
            'Referencia'    => ['data' => 'referencia', 'name' => 'referencia'],
			'Ver'    => ['data' => 'ver', 'searchable' => false, 'orderable' => false],
            'Editar'    => ['data' => 'editar', 'searchable' => false, 'orderable' => false],
            'Eliminar'    => ['data' => 'eliminar', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => 'http://intradif.oo/bower_components/datatables.net-responsive/js/Spanish.json'
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        if ($this->tipo=='detalles') {
            return "Lista dispersion {$this->lista->nombre}";
        }
        return 'listas de dispersion_' . date('YmdHis');
    }
}
