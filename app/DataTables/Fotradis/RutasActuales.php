<?php

namespace App\DataTables\Fotradis;

use App\Models\Rutas;
use App\Models\Recorridos;
use Yajra\DataTables\Services\DataTable;

class RutasActuales extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->editColumn('nombre', function($ruta) {
            return strtoupper($ruta->nombre);
        })
        ->addColumn('ver', function($ruta) {
            return '<a class="btn btn-info btn-sm" onclick="ver.ruta(' . "'" . $ruta->id . "'" . ');"><i class="fa fa-fw fa-eye"></i></a>';
        })
        ->addColumn('editar', function($ruta) {
            return '<a class="btn btn-warning btn-sm" onclick="lanzar.mensaje_de_alerta(' . "'" . $ruta->id . "','" . "editar". "'" . ');"><i class="fa fa-fw fa-edit"></i></a>';
        })
        ->addColumn('eliminar', function($ruta) {
            $count = Recorridos::join('rutas','rutas.recorrido_id','recorridos.id')
                        ->join('controlrutas','controlrutas.recorrido_id','recorridos.id')
                        ->where('rutas.id','=',$ruta->id)
                        ->whereNull('controlrutas.deleted_at')->count();
            if($count > 0)
                return '<a class="btn btn-danger btn-sm disabled"><i class="fa fa-trash-o"></i></a>';
            return '<a class="btn btn-danger btn-sm" onclick="lanzar.mensaje_de_alerta(' . "'" . $ruta->id . "','" . "eliminar". "'" . ');"><i class="fa fa-trash-o"></i></a>';
        })
        ->rawColumns(['ver', 'editar', 'eliminar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return Rutas::where("id", "!=", 100);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'Nombre'    => ['data' => 'nombre', 'name' => 'nombre'],
			      'Ver'    => ['data' => 'ver', 'searchable' => false, 'orderable' => false],
            'Editar'    => ['data' => 'editar', 'searchable' => false, 'orderable' => false],
            'Eliminar'    => ['data' => 'eliminar', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        if ($this->tipo=='detalles') {
            return "Lista dispersion {$this->lista->nombre}";
        }
        return 'listas de dispersion_' . date('YmdHis');
    }
}
