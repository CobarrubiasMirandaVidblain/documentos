<?php

namespace App\DataTables;

use App\Models\PeticionesPersonas;
use Yajra\DataTables\Services\DataTable;

class BeneficiariosDataTable extends DataTable {

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->editColumn('fecha_oficio', function($peticion) {
            return with(\Carbon\Carbon::parse($peticion->fecha_oficio))->format('d-m-Y');
        })
        ->filterColumn('solicitudes.fechaoficio', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(solicitudes.fechaoficio, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->orderColumn('fecha_oficio', 'fecha_oficio $1')
        ->addColumn('edad', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
        })
        ->orderColumn('edad', '-fecha_nacimiento $1')
        ->filterColumn('edad', function($query, $keyword){
            $edad = strtoupper($keyword);
            if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)){
                preg_match_all('!\d+!', $edad, $valores);
                preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);                
                if(count($valores[0]) == 2){                    
                    $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]." AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][1]." ".$valores[0][1]);
                }else{
                    $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]);
                }                
            }
        })
        ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->filterColumn('cat_regiones.nombre', function($query, $keyword) {
            $keyword = strtoupper($keyword);
            if(preg_match("/REGION=([A-ZñÑ]+)$/", $keyword)) {
                $region = substr($keyword, 7);
                $query->whereRaw("cat_regiones.nombre like ?", ["%$region%"]);
            }
        })
        ->filterColumn('cat_distritos.nombre', function($query, $keyword) {
            $keyword = strtoupper($keyword);
            if(preg_match("/DISTRITO=([A-ZñÑ]+)$/", $keyword)) {
                $distrito = substr($keyword, 9);
                $query->whereRaw("cat_distritos.nombre like ?", ["$distrito%"]);
            }
        })
        ->addColumn('estatus', function($solicitud) {
            // dd($solicitud);
            // if($solicitud->programas_solicitud->statusActual() == "VINCULADO" || $solicitud->programas_solicitud->statusActual() == "VALIDANDO"){
            if($solicitud->beneficioprograma_solicitud->estadoActual() == "VINCULADO" || $solicitud->beneficioprograma_solicitud->estadoActual() == "VALIDANDO"){
                if($solicitud->evaluado == 2) {
                    return '<span class="label label-success">APROBADO</span>';
                } elseif($solicitud->evaluado === 1) {
                    return '<span class="label label-danger">RECHAZADO</span>';
                } else {
                    return '<span class="label label-warning">VALIDANDO</span>';
                }
            }else {
                if($solicitud->entregado == 2) {
                    return '<span class="label label-success">ENTREGADO</span>';
                } elseif($solicitud->entregado === 1) {
                    return '<span class="label label-danger">CANCELADO</span>';
                } else {
                    return '<span class="label label-warning">EN ESPERA</span>';
                }
            }
        })
        ->filterColumn('estatus', function($query, $keyword){
            switch($keyword) {
                case "APROBADO":
                    $query->whereRaw("solicitudes_personas.evaluado = 1");
                    break;
                case "RECHAZADO":
                    $query->whereRaw("solicitudes_personas.evaluado = 0");
                    break;
                case "VALIDANDO":
                    $query->whereRaw("solicitudes_personas.evaluado is null");
                    break;
                case "ENTREGADO":
                    $query->whereRaw("solicitudes_personas.entregado = 1");
                    break;
                case "CANCELADO":
                    $query->whereRaw("solicitudes_personas.entregado = 0");
                    break;
                case "ESPERA":
                    $query->whereRaw("solicitudes_personas.entregado is null");
                    break;
            }
        })
        ->addColumn('consultar', function($solicitud) {
            return '<button style="margin-right: 10px;" class="btn btn-info btn-xs" data-toggle="tooltip" title="Consultar" onclick="mostrar_persona('.
            $solicitud->beneficiopersona->persona->id .', \'' . $this->modulo .'\')"><i class="fa fa-address-card"></i> Consultar</button>';
        })
        ->addColumn('entregar', function($solicitud) {
            if($solicitud->beneficioprograma_solicitud->estadoActual() == "LISTA DE ESPERA"){
               if($solicitud->entregado === 0) {
                   return '<button style="margin-right: 10px;" type="button" class="btn btn-success btn-xs" data-toggle="tooltip" title="Entregar" data-target="#entregar" onclick="Beneficiario.set_persona('. 
                   $solicitud->beneficiopersona->persona->id . ', \''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\',\'entregar\')"><i class="fa fa-truck"></i> Entregar</button>';
               }
           }    
           if($solicitud->entregado){
               //return '<button data-toggle="tooltip" title="Consultar entrega" onclick="consultar_entrega('.$solicitud->beneficiopersona->persona->id.');" type="button" class="btn btn-success btn-xs"><i class="fa fa-file-pdf-o"></i> Consultar</button>';
           }  
        })

        ->addColumn('cancelar', function($solicitud) {
            if($solicitud->beneficioprograma_solicitud->estadoActual() == "VINCULADO" || $solicitud->beneficioprograma_solicitud->estadoActual() == "VALIDANDO") {
                if($solicitud->evaluado === 0 || $solicitud->evaluado === 1) {
                    return '<button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Cancelar" onclick="abrir_modal(\'modal-cancelar\','.
                    $solicitud->beneficiopersona->persona->id.',\''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\')"> <i class="fa fa-ban"></i> Cancelar</button>';
                }
            } elseif($solicitud->beneficioprograma_solicitud->estadoActual() == "LISTA DE ESPERA"){
                if($solicitud->entregado === 0) {
                    return '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Rechazar" data-target="#rechazar" onclick="setPersona('. 
                    $solicitud->beneficiopersona->persona->id .', \''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\',\'rechazar\')"><i class="fa fa-ban"></i> Cancelar</button>';
                }
            }
        })

        /* ->addColumn('reactivar', function($solicitud){
            if($solicitud->programas_solicitud->statusActual() == "VINCULADO" || $solicitud->programas_solicitud->statusActual() == "VALIDANDO") {                
                if($solicitud->evaluado !== 0 && $solicitud->evaluado !== 1) {
                    return '<button class="btn btn-warning btn-xs" data-toggle="tooltip" title="Reactivar" onclick="reactivar_solicitante('.
                    $solicitud->beneficiopersona->persona->id.',\''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\')"> <i class="fa fa-reply"></i> Reactivar</button>';
                }
             } 
             //Si fue cancelado en VALIDACION no se puede reactivar en LISTA DE ESPERA
             elseif($solicitud->programas_solicitud->statusActual() == "LISTA DE ESPERA" && $solicitud->entregado === 0 && $solicitud->observacionevaluado === null){
                return '<button class="btn btn-warning btn-xs" data-toggle="tooltip" title="Reactivar" onclick="reactivar_solicitante('.
                $solicitud->beneficiopersona->persona->id.',\''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\')"> <i class="fa fa-reply"></i> Reactivar</button>';
            }
        }) */

        ->addColumn('observación', function($solicitud) {
            if($solicitud->entregado === 1) {
                return $solicitud->observacionentrega;                    
            }else if($solicitud->observacionentrega !== null) {
                return $solicitud->observacionentrega;                    
            }else{
                return $solicitud->observacionevaluado;                    
            }
        })

        ->addColumn('seguimiento', function($peticion) {
            if(!$this->beneficiosprogramas_solicitud_id){
                return "<a class='btn btn-primary btn-xs' href='" .
                    route($this->modulo.'.peticiones.show', ['peticion_id' => $peticion->programa_solicitud_id])."'><i class='fa fa-arrow-right'></i> Seguimiento</a>";
            }
        })
        ->rawColumns(['estatus', 'consultar', 'entregar', 'cancelar', 'reactivar', 'seguimiento'])
        ->with('permiso_solo_lectura', $this->permiso_solo_lectura)
        ->with('estatus', $this->estatus)
        ->with('modulo', $this->modulo);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        // dd($this->beneficiosprogramas_solicitud_id);
        $model = PeticionesPersonas::join('beneficiosprog_personas','beneficiosprog_personas.id','beneficiosprogsol_benefpersonas.beneficiopersona_id')
        ->join('personas', 'personas.id', 'beneficiosprog_personas.persona_id')
        ->leftJoin('cat_localidades', 'cat_localidades.id', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', 'cat_distritos.region_id')
        ->join('beneficiosprogramas_solicitudes','beneficiosprogramas_solicitudes.id','beneficiosprogsol_benefpersonas.beneficiosprogramas_solicitud_id')
        ->join('solicitudes','solicitudes.id','beneficiosprogramas_solicitudes.solicitud_id')
        ->join('beneficiosprogramas','beneficiosprogramas.id','beneficiosprogramas_solicitudes.beneficioprograma_id')
        ->join('anios_programas', 'anios_programas.id', 'beneficiosprogramas.anio_programa_id')
        ->join('programas', 'programas.id', 'anios_programas.programa_id')
        /* $model = SolicitudesPersona::solicitantes($this->beneficiosprogramas_solicitud_id) */
        ->where('beneficiosprogramas_solicitud_id',$this->beneficiosprogramas_solicitud_id)
        ->select(
            [   
                'beneficiosprogsol_benefpersonas.id',
                'beneficiosprogsol_benefpersonas.beneficiopersona_id',
                'beneficiosprogsol_benefpersonas.id as solicitud_persona',
                'beneficiosprogsol_benefpersonas.beneficiosprogramas_solicitud_id as beneficiosprogramas_solicitud_id',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.curp as curp',
                'personas.fecha_nacimiento as fecha_nacimiento',                
                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',
                'beneficiosprogsol_benefpersonas.entregado as entregado',
                'beneficiosprogsol_benefpersonas.evaluado as evaluado',
                'beneficiosprogsol_benefpersonas.observacion_evaluado as observacionevaluado',
                'beneficiosprogsol_benefpersonas.observacion_entrega as observacionentrega',
                'solicitudes.folio as folio',
                'beneficiosprogramas.nombre as apoyo',
                'solicitudes.fechaoficio as fecha_oficio',
                'solicitudes.numoficio as numero_oficio',
                // 'programas_solicitudes.id as programa_solicitud_id'
            ]
        );
        
        if($this->tipo_solicitud){
            $model = $model->where('folio','LIKE',$this->tipo_solicitud.'%');
        }

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.table = "beneficiarios"; }'])                
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        if(!$this->beneficiosprogramas_solicitud_id){
            $columns['folio'] = ['data' => 'folio', 'name' => 'solicitudes.folio'];
            $columns['apoyo'] = ['data' => 'apoyo', 'name' => 'programas.nombre'];
            $columns['no. oficio'] = ['data' => 'numero_oficio', 'name' => 'solicitudes.numoficio'];
            $columns['fecha del oficio'] = ['data' => 'fecha_oficio', 'name' => 'solicitudes.fechaoficio'];
        }
        
        $columns['nombre'] = ['data' => 'nombre', 'name' => 'personas.nombre'];
        $columns['primer_apellido'] = ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'];
        $columns['segundo_apellido'] = ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'];
        $columns['CURP'] = [ 'title' => 'CURP', 'data' => 'curp', 'name' => 'personas.curp'];
        $columns['edad'] = ['data' => 'edad', 'name' => 'edad'];
        $columns['localidad'] = ['data' => 'localidad', 'name' => 'cat_localidades.nombre'];
        $columns['municipio'] = ['data' => 'municipio', 'name' => 'cat_municipios.nombre'];
        $columns['distrito'] = ['data' => 'distrito', 'name' => 'cat_distritos.nombre'];
        $columns['region'] = ['data' => 'region', 'name' => 'cat_regiones.nombre'];
            
            //'reactivar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            //'observación' => ['searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => false],
        if($this->modulo === 'afuncionales'){
            $columns['estatus'] = ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false];
            $columns['entregar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];       
        }

        if(!$this->permiso_solo_lectura){
            if(!$this->beneficiosprogramas_solicitud_id){
                $columns['seguimiento'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            }else{
                $columns['consultar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
                $columns['cancelar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            }
        }       

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            // 'preDrawCallback' => 'function() { block(); }',
            // 'drawCallback' => 'function() { $(\'[data-toggle="tooltip"]\').tooltip(); unblock(); }',
            'buttons' => [
                [
                    'className' => 'button-dt tool nuevo-beneficiario',
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { abrir_modal("modal-personas"); }'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Beneficiarios_' . date('YmdHis');
    }
}