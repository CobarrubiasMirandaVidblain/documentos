<?php

namespace App\DataTables\ConsultasMedicas;

use App\Models\Persona;
use Yajra\DataTables\Services\DataTable;

class PersonasTabla extends DataTable{
    /**
     * Construye el dataTable
     * @param mixed $query Resultado obtenido a partir del método query()
     * @return \Yajra\DataTables\DataTableAbstract El dataTable
     */
    public function dataTable($query){
        return datatables()->eloquent($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            $fecha = time() - strtotime($persona->fecha_nacimiento);
            return floor($fecha / 31556926);
        })
        ->addColumn('seleccionar', function($persona) {
          return '<a class="btn btn-success btn-xs" onclick="seleccionar_persona(' . "'" . $persona->id . "'" . ', ' . "'" . $persona->obtenerNombreCompleto(). "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
      })
        ->rawColumns(['seleccionar']);
    }

    /**
     * Genera la fuente de datos para el dataTable
     * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
*/
    public function query(){
        $model = Persona::leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
    		->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
    		->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
    		->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->leftjoin('empleados', 'empleados.persona_id', '=', 'personas.id')
        ->leftjoin('pacientes','pacientes.persona_id','=','personas.id')
        ->whereNull('empleados.persona_id')
        ->whereNull('pacientes.persona_id')
        ->select(
            [
                'personas.id as id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',
                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad'
            ]
        );

        return $model;
    }

    /**
     * Método pata configurar el constructor html
     * @return \Yajra\DataTables\Html\Builder Constructor de html
     */
    public function html(){
        return $this->builder()
        ->columns($this->getColumns())
        ->ajax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Define las columnas que mostrara el html
     * @return array Arreglo con la configuración de columnas
     */
    protected function getColumns(){
        $columns =
        [
            'id' => ['data' => 'id', 'name' => 'personas.id'],
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'apellido_paterno' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'apellido_materno' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'edad' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'curp' => ['data' => 'curp', 'name' => 'personas.curp'],
            'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'seleccionar' => ['data' => 'seleccionar', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    /**
 * Define los parámetros de configuración para la librería de jquery dataTables
 * @return Array Parametros de inicializacion para la librería
 */
protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'sEmptyTable' => 'No hay personas disponibles'
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { if(elemento !== undefined) block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Define un nombre para el archivo que se exportara
     * @return string Nombre del archivo, sin extension
     */
    protected function filename(){
        return 'personas_' . date('YmdHis');
    }
}
