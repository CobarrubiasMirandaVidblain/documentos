<?php

namespace App\DataTables\ConsultasMedicas;

use Yajra\DataTables\Services\DataTable;

use App\Models\Cita;

class CitasTabla extends DataTable{
  
  /**
  * Construye el dataTable
  * @param mixed $query Resultado obtenido a partir del método query()
  * @return \Yajra\DataTables\DataTableAbstract El dataTable
  */
  public function dataTable($query){
    return datatables($query)
    ->addColumn('edit',function($cita){
      if ($cita->estado == null)
      return '<a onClick="cargarModal('."'".route('consultasmedicas.citas.show',$cita->id)."')".'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Editar</a>';
      return '<a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Editar</a>';
    })
    ->addColumn('consulta',function($cita){
      return '<button class="btn btn-xs btn-success" onclick="atenderPaciente('."'".
      route('consultasmedicas.pacientes.consultas.create',$cita->paciente)."','".
      route('consultasmedicas.citas.update',$cita->id)."')".
      '"><i class="fa fa-check"> Atender</button>';
    })
    ->rawColumns(['edit','consulta']);
  }
  
  /**
  * Genera la fuente de datos para el dataTable
  * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
  */
  public function query(){
    return Cita::join('personas','personas.id','citas.persona_id')
    ->join('pacientes','pacientes.persona_id','personas.id')
    ->where([
      ['citas.fecha',date('Y-m-d')],
      ['citas.empleado_id',$this->request->User()->persona->empleado->id],
      ['citas.asistencia',0]
    ])
    ->select([
      'pacientes.id as paciente',
      'personas.nombre',
      'personas.primer_apellido',
      'personas.segundo_apellido',
      'citas.hora_fin',
      'citas.hora_inicio',
      'citas.observaciones',
      'citas.id'
    ]);
  }
  
  /**
  * Método pata configurar el constructor html
  * @return \Yajra\DataTables\Html\Builder Constructor de html
  */
  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }
  
  /**
  * Define las columnas que mostrara el html
  * @return array Arreglo con la configuración de columnas
  */
  protected function getColumns(){
    $columns =
    [
      'nombre'                      => ['data' => 'nombre', 'name' => 'personas.nombre'],
      'primer_apellido'             => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
      'segundo_apellido'            => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
      'hora inicio'                 => ['data' => 'hora_inicio', 'name' => 'citas.hora_inicio'],
      'observaciones'               => ['data' => 'observaciones', 'name' => 'citas.observaciones'],
      'ver'                         => ['data' => 'edit', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
      'consulta'                    => ['data' => 'consulta', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false]
      
      //'Eliminar'                    => ['data' => 'destroy', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false]
    ];
    return $columns;
  }
  
  /**
  * Define los parámetros de configuración para la librería de jquery dataTables
  * @return Array Parametros de inicializacion para la librería
  */
  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'sEmptyTable' => 'No hay citas para hoy'
      ],
      'dom' => 'Btip',
      'buttons' => [
        [
          'extend' => 'create',
          'text' => '<i class="fa fa-plus"></i> Nuevo',
          'className' => 'button-dt tool',
          'action' => '
          function (e, dt, button, config) {
            var url = "'.route('consultasmedicas.citas.create').'";
            cargarModal(url);
          }
          '
        ],
        [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => 'button-dt tool'
        ]
      ],
      'lengthMenu' => [ 10 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];
    
    return $builderParameters;
  }
  
  /**
  * Define un nombre para el archivo que se exportara
  * @return string Nombre del archivo, sin extension
  */
  protected function filename(){
    return 'cursos braile al ' . date('Ymd');
  }
}