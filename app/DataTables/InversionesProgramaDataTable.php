<?php

namespace App\DataTables;

use App\Models\Ejercicio;
use App\Models\InversionPrograma;
use Yajra\DataTables\Services\DataTable;

class InversionesProgramaDataTable extends DataTable {
    
    public function dataTable($query) {
		return datatables($query)
			->editColumn('rubro', function($inversionprograma) {
      			return $inversionprograma->rubro->nombre;
			})
			->editColumn('programa', function($inversionprograma) {
      			return $inversionprograma->anioprogramas->programa->nombre;
			})
			->editColumn('ejercicio', function($inversionprograma) {
      			return $inversionprograma->anioprogramas->ejercicio->anio;
			})
			->editColumn('beneficiarios', function($inversionprograma) {
      			return number_format($inversionprograma->numbeneficiarios,0,".",",");
			})
			->editColumn('dotaciones', function($inversionprograma) {
      			return number_format($inversionprograma->dotaciones_totales,0,".",",");
			})
			->editColumn('inversion', function($inversionprograma){
				return '$ ' . number_format($inversionprograma->inversion,2,".",",");
			});
    }

    public function query(InversionPrograma $model) {
		return $model->where('municipio_id',$this->municipio_id)->get();
	}
	
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
	}
	
    protected function getColumns() {
        return [
            'rubro' => ['data' => 'rubro', 'name' => 'rubro', 'searchable' => false, 'orderable' => false],
            'programa' => ['data' => 'programa', 'name' => 'programa'],
            'ejercicio' => ['data' => 'ejercicio', 'name' => 'ejercicio'],
            'beneficiarios' => ['data' => 'beneficiarios', 'name' => 'beneficiarios'],
            'dotaciones' => ['data' => 'dotaciones', 'name' => 'dotaciones', 'visible' => false],
            'inversion' => ['data' => 'inversion', 'name' => 'inversion']            
        ];
    }

    protected function getBuilderParameters() {
		$ejercicios = '<div class="dropdown"><a style="color: #d12654" id="btnEjercicio" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
        $ejercicios = $ejercicios . 'Ejercicio: ' . date('Y') . ' <span class="caret"></span></a><ul id="menuEjercicio" class="dropdown-menu">';
        foreach(Ejercicio::orderBy('anio', 'desc')->take(3)->get() as $ejercicio){
            $ejercicios = $ejercicios . '<li><a href="#">' . $ejercicio->anio . '</a></li>';
        }
		$ejercicios = $ejercicios . '</ul></div>';
		
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            //'preDrawCallback' => 'function() { block(); }',
            //'drawCallback' => 'function() { unblock(); }',
            'initComplete' => 'function(){ $("#menuEjercicio li a").click(function(){ ejercicio = $(this).text();  table.filtrar(2, $(this).text()); $("#btnEjercicio").html("Ejercicio: " + $(this).text() + " <span class=\"fa fa-caret-down\"></span>"); $("#btnEjercicio").val("Ejercicio: " + $(this).text()); }); }',
            'buttons' => [
                [
                    'className' => 'button-dt tool dropdown-toggle',
                    'text' => $ejercicios
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'className' => 'button-dt tool',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel'
                ],                
                [
                    'extend' => 'pdf',
                    'className' => 'button-dt tool',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF'
                ]                
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
				[ 'className' => 'text-center', 'targets' => [2] ],
				[ 'className' => 'text-right', 'targets' => [3,4,5] ],
                [ 'visible' => false, 'targets' => 0 ]
            ],
            'rowGroup' => [
                'dataSrc' => 'rubro'
            ]
        ];

        return $builderParameters;
    }

    protected function filename() {
        return 'InversionesPrograma_' . date('YmdHis');
    }
}
