<?php

namespace App\DataTables\Afuncionales;
use App\Models\StatusSolicitud;
use App\Models\ProgramasSolicitud;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PeticionesDataTable extends DataTable
{

    private $solicitudes_nuevas = 0;
    private $solicitudes_espera = 0;
    private $solicitudes_finalizadas = 0;
    private $solicitudes_canceladas = 0;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('fecha', function($peticiones) {
            return with(\Carbon\Carbon::parse($peticiones->fecha))->format('d-m-Y');
        })
        ->filterColumn('fecha', function($query, $keyword) {
            $query->whereRaw("select DATE_FORMAT(fecha,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->orderColumn('dias transcurridos', 'dias_transcurridos $1')
        ->filterColumn('estatus', function($query, $keyword) {
            if(strtoupper($keyword) === 'NUEVO'){
                $keyword = 'VINCULADO';
            }
            $query->whereRaw("estatus like ?", ["%$keyword%"]);
        })          
        ->addColumn('estatus', function($peticiones) {
            $status = '';
            switch ($peticiones->estatus) {
                case "VINCULADO":
                    $status = '<span class="label label-success">NUEVO</span>';
                    break;
                case "VALIDANDO":
                    $status = '<span class="label label-warning">VALIDANDO</span>';
                    break;
                case "LISTA DE ESPERA":
                    $status = '<span class="label label-info">LISTA DE ESPERA</span>';
                    break;
                case "CANCELADO":
                    $status = '<span class="label label-danger">CANCELADO</span>';
                    break;
                case "RECHAZADO":
                    $status = '<span class="label label-danger">RECHAZADO</span>';
                    break;
                case "FINALIZADO":
                    $status = '<span class="label label-primary">FINALIZADO</span>';
                    break;
            }
            return $status;
        })
        ->orderColumn('estatus', 'estatus $1')
        ->addColumn('autorizar', function($peticiones) {
            return "<a class='btn btn-primary btn-xs' href='" .
            route('afuncionales.peticiones.programas.show', ['peticion_id' => $peticiones->solicitud_id, 'programa_id' => $peticiones->programa_id])."'><i class='fa fa-arrow-right'></i> Autorizar</a>";
        })
        ->addColumn('seguimiento', function($peticiones) {
            return "<a class='btn btn-primary btn-xs' href='" .
            route('afuncionales.peticiones.programas.show', ['peticion_id' => $peticiones->solicitud_id, 'programa_id' => $peticiones->programa_id])."'><i class='fa fa-arrow-right'></i> Seguimiento</a>";
        })
        ->addColumn('cancelar', function($peticiones) {
            if($peticiones->estatus != "CANCELADO" && $peticiones->estatus != "FINALIZADO" && $peticiones->estatus != "LISTA DE ESPERA"){
                return "<a class='btn btn-danger btn-xs' onclick='cancelar(\"" .
                route('afuncionales.peticiones.programas.update', ['peticion_id' => $peticiones->solicitud_id, 'programa_id' => $peticiones->programa_id]). "\",\"" . $peticiones->folio . "\",\"" . $peticiones->apoyo ."\",\"". $peticiones->remitente ."\")'><i class='fa fa-ban'></i> Cancelar</a>";
            }          
        })
        ->addColumn('reactivar', function($peticiones) {
            if($peticiones->estatus == "CANCELADO"){
                return "<a class='btn btn-warning btn-xs' onclick='reactivar(\"" .
                route('afuncionales.peticiones.programas.update', ['peticion_id' => $peticiones->solicitud_id, 'programa_id' => $peticiones->programa_id]). "\",\"" . $peticiones->folio . "\",\"" . $peticiones->apoyo ."\",\"". $peticiones->remitente ."\")'><i class='fa fa-reply'></i> Reactivar</a>";
            }          
        })
        ->rawColumns(['estatus','seguimiento','cancelar','reactivar','autorizar'])
        ->with('solicitudes_nuevas', $this->solicitudes_nuevas)
        ->with('solicitudes_espera', $this->solicitudes_espera)
        ->with('solicitudes_finalizadas', $this->solicitudes_finalizadas)
        ->with('solicitudes_canceladas', $this->solicitudes_canceladas);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = ProgramasSolicitud::search('APOYOS FUNCIONALES');
        
        $solicitudes_nuevas = clone $model;
        $solicitudes_espera = clone $model;
        $solicitudes_finalizadas = clone $model;
        $solicitudes_canceladas = clone $model;

        $this->solicitudes_nuevas = $solicitudes_nuevas->where('estatus','VINCULADO')->count();
        $this->solicitudes_espera = $solicitudes_espera->where('estatus','LISTA DE ESPERA')->count();        
        $this->solicitudes_finalizadas = $solicitudes_finalizadas->where('estatus','FINALIZADO')->count();
        $this->solicitudes_canceladas = $solicitudes_canceladas->where('estatus','CANCELADO')->count();
        if(Auth::user()->hasRolesModulo(['ADMINISTRADOR DE PETICIONES'], 3)) {
            $model->where('estatus','VINCULADO');
        }
        if(Auth::user()->hasRolesModulo(['CAPTURISTA'], 3)) {
            $model->where('estatus','!=','VINCULADO');
        }
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns = [
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
            'folio' => ['data' => 'folio', 'name' => 'folio'],
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'remitente' => ['data' => 'remitente', 'name' => 'remitente'],
            'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
            'apoyo' => ['data' => 'apoyo', 'name' => 'apoyo'],
            'cantidad' => ['data' => 'cantidad', 'name' => 'cantidad'],
            'días transcurridos' => ['data' => 'dias_transcurridos', 'name' => 'dias_transcurridos'],
            'estatus' => ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false],
            'seguimiento' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'cancelar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'reactivar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];

        if(Auth::user()->hasRolesModulo(['SUPERADMIN', 'ADMINISTRADOR', 'ADMINISTRADOR DE PETICIONES'], 3)) {
            $columns['autorizar'] = ['data' => 'autorizar', 'name' => 'autorizar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'order' => [
                2,
                'desc'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Peticiones_' . date('YmdHis');
    }
}
