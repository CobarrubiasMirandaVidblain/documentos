<?php

namespace App\DataTables\Afuncionales;

use App\Models\DetalleEntradasproductos;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

class DetalleEntradaDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->editColumn('precio', function($producto) {
            return number_format($producto->precio, 2, '.', ',');;
        })
        ->with(['fecha' => $this->fecha, 'tipo' => $this->tipo, 'remitente' => $this->remitente, 'receptor' => $this->receptor]);        
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return DetalleEntradasproductos::
        join('productosfoliados','productosfoliados.detallesentradas_producto_id','detallesentradas_productos.id')
        ->join('entradas_productos','entradas_productos.id','detallesentradas_productos.entradas_producto_id')
        // ->join('areas_productos','areas_productos.id','detallesentradas_productos.areas_producto_id')
        // ->join('cat_productos','cat_productos.id','areas_productos.producto_id')
        ->join('beneficiosprogramas','beneficiosprogramas.id','detallesentradas_productos.beneficioprograma_id')
        ->join('anios_programas','anios_programas.id','beneficiosprogramas.anio_programa_id')
        ->join('programas','programas.id','anios_programas.programa_id')
        ->join('programas as padre','padre.id','programas.padre_id')
        ->join('areas_programas','areas_programas.programa_id','padre.id')
        ->join('cat_areas','cat_areas.id','areas_programas.area_id')
        ->where('entradas_productos.id',$this->request->entrada_id)
        ->select('beneficiosprogramas.nombre as producto', 'detallesentradas_productos.precio as precio', 'productosfoliados.folio as folio');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.table = "detalle"; d.entrada_id = entrada_id; }'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'producto' => ['data' => 'producto', 'name' => 'producto'],
            'folio' => ['data' => 'folio', 'name' => 'folio'],
            'precio' => ['data' => 'precio', 'name' => 'precio']
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Afuncionales/DetalleEntrada_' . date('YmdHis');
    }
}
