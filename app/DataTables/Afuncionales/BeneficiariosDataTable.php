<?php

namespace App\DataTables\Afuncionales;

use App\Models\SolicitudesPersona;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

class BeneficiariosDataTable extends DataTable {
    
    private $beneficiarios_nuevos = 0;
    private $beneficiarios_proceso = 0;
    private $beneficiarios_finalizados = 0;
    private $beneficiarios_cancelados = 0;

    public function dataTable($query) {
        return datatables($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->addColumn('edad', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
        })
        ->orderColumn('edad', '-fecha_nacimiento $1')
        ->filterColumn('edad', function($query, $keyword){
            $edad = strtoupper($keyword);
            if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)){
                preg_match_all('!\d+!', $edad, $valores);
                preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);                
                if(count($valores[0]) == 2){                    
                    $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]." AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][1]." ".$valores[0][1]);
                }else{
                    $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]);
                }                
            }
        })
        ->filterColumn('cat_regiones.nombre', function($query, $keyword) {
            $keyword = strtoupper($keyword);
            if(preg_match("/REGION=([A-ZñÑ]+)$/", $keyword)) {
                $region = substr($keyword, 7);
                $query->whereRaw("cat_regiones.nombre like ?", ["%$region%"]);
            }
        })
        ->filterColumn('cat_distritos.nombre', function($query, $keyword) {
            $keyword = strtoupper($keyword);
            if(preg_match("/DISTRITO=([A-ZñÑ]+)$/", $keyword)) {
                $distrito = substr($keyword, 9);
                $query->whereRaw("cat_distritos.nombre like ?", ["$distrito%"]);
            }
        })
        ->editColumn('fecha_entrega', function($beneficiario) {
            if($beneficiario->fecha_entrega){
                return with(\Carbon\Carbon::parse($beneficiario->fecha_entrega))->format('d-m-Y');
            }
        })
        ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->filterColumn('salidas_productos.fechasalida', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(salidas_productos.fechasalida, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->filterColumn('estatus', function($query, $keyword) {
            if($keyword === 'NUEVO'){
                $query->where('cat_statusprocesos.status','Vo. Bo.')->whereNull('solicitudes_personas.entregado');
            }else if($keyword === 'VALIDANDO'){
                $query->where(function ($query) {
                    $query->where('cat_statusprocesos.status','=','LISTA DE ESPERA')->orWhere('cat_statusprocesos.status','=','VALIDANDO');
                })->whereNull('solicitudes_personas.entregado');
            }elseif($keyword === 'CANCELADO'){
                $query->whereRaw('(cat_statusprocesos.status = "CANCELADO" or cat_statusprocesos.status = "RECHAZADO") or solicitudes_personas.entregado = 0');
            }elseif($keyword === 'FINALIZADO'){
                $query->whereRaw('solicitudes_personas.entregado = 1');
            }            
        })
        ->addColumn('estatus', function($peticion) {
            $status = '';
            switch ($peticion->estatus) {
                case "SOLICITANTE":
                    $status = '<span class="label label-warning">SOLICITANTE</span>';
                    break;
                case "VINCULADO":
                    $status = '<span class="label label-success">NUEVO</span>';
                    break;
                case "VALIDANDO":
                    $status = '<span class="label label-warning">VALIDANDO</span>';
                    break;
                case "LISTA DE ESPERA":
                    $status = '<span class="label label-info">LISTA DE ESPERA</span>';
                    break;
                case "CANCELADO":
                    $status = '<span class="label label-danger">CANCELADO</span>';
                    break;
                case "RECHAZADO":
                    $status = '<span class="label label-danger">RECHAZADO</span>';
                    break;
                case "FINALIZADO":
                    $status = '<span class="label label-primary">FINALIZADO</span>';
                    break;
                case "Vo. Bo.":
                    $status = '<span class="label label-primary">Vo. Bo.</span>';
                    break;
            }
            return $status;
        })
        ->rawColumns(['estatus'])
        ->with('beneficiarios_nuevos', $this->beneficiarios_nuevos)
        ->with('beneficiarios_proceso', $this->beneficiarios_proceso)
        ->with('beneficiarios_finalizados', $this->beneficiarios_finalizados)
        ->with('beneficiarios_cancelados', $this->beneficiarios_cancelados);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = SolicitudesPersona::beneficiarios('APOYOS FUNCIONALES');

        $beneficiarios_nuevos = clone $model;
        $beneficiarios_proceso = clone $model;
        $beneficiarios_finalizados = clone $model;
        $beneficiarios_cancelados = clone $model;

        $this->beneficiarios_nuevos = $beneficiarios_nuevos->where('cat_statusprocesos.status','Vo. Bo.')->whereNull('solicitudes_personas.entregado')->count();

        $this->beneficiarios_proceso = $beneficiarios_proceso->where(function ($query) {
            $query->where('cat_statusprocesos.status','=','LISTA DE ESPERA')->orWhere('cat_statusprocesos.status','=','VALIDANDO');
        })->whereNull('solicitudes_personas.entregado')->count();
        
        $this->beneficiarios_cancelados = $beneficiarios_cancelados->whereRaw('((cat_statusprocesos.status = "CANCELADO" or cat_statusprocesos.status = "RECHAZADO") or solicitudes_personas.entregado = 0)')->count();
//dd($this->beneficiarios_cancelados);
        $this->beneficiarios_finalizados = $beneficiarios_finalizados->whereRaw('solicitudes_personas.entregado = 1')->count();

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [            
            'folio' => ['data' => 'folio_solicitud', 'name' => 'solicitudes.folio'],
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'fecha de nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],      
            'edad' => ['data' => 'edad', 'name' => 'edad'],      
            'CURP' => ['data' => 'curp', 'name' => 'personas.curp'],
            'discapacidad' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
            'limitación' => ['data' => 'limitacion', 'name' => 'cat_limitaciones.nombre'],          
            'programa' => ['data' => 'programa', 'name' => 'programas.nombre'],            
            'apoyo otorgado' => ['data' => 'apoyo', 'name' => 'cat_productos.producto'],
            'folio apoyo' => ['data' => 'folio_apoyo', 'name' => 'productosfoliados.folio'],
            'fecha_de_entrega' => ['data' => 'fecha_entrega', 'name' => 'salidas_productos.fechasalida'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'región' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'estatus' => ['data' => 'estatus', 'name' => 'estatus', 'searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false]            
        ];
    }

    protected function getBuilderParameters()
    {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Beneficiarios_' . date('YmdHis');
    }
}