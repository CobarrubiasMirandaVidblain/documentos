<?php

namespace App\DataTables\Afuncionales;

use App\Models\EntradasProducto;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

class EntradasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
            ->editColumn('fecha', function($entradas) {
                return with(\Carbon\Carbon::parse($entradas->fecha))->format('d-m-Y');
            })
            ->filterColumn('fecha', function($query, $keyword) {
                $query->whereRaw("select DATE_FORMAT(fechaentrada,'%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('tipo', function($query, $keyword) {
                $query->whereRaw("cat_tiposentradas.tipo like ?", ["%$keyword%"]);            
            })
            ->orderColumn('fecha', 'fecha $1')            
            ->filterColumn('remitente', function($query, $keyword) {
                $query->whereRaw("(SELECT cat_instituciones.nombre FROM entradas_productos AS ep ".
                "INNER JOIN entradasproductos_dependencias ON entradasproductos_dependencias.entradas_producto_id = ep.id ".
                "INNER JOIN cat_instituciones ON cat_instituciones.id = entradasproductos_dependencias.dependencia_id ".
                "WHERE ep.id = entradas_productos.id AND entradasproductos_dependencias.deleted_at IS NULL) like ? ".
                " OR ".
                "(SELECT concat(personas.nombre,' ',personas.primer_apellido,' ',personas.segundo_apellido) ".
                "FROM entradas_productos AS ep ".
                "INNER JOIN entradasproductos_personas ON entradasproductos_personas.entradas_producto_id = ep.id ".
                "INNER JOIN personas ON personas.id = entradasproductos_personas.persona_id ".
                "WHERE ep.id = entradas_productos.id AND entradasproductos_personas.deleted_at IS NULL) like ?", ["%$keyword%","%$keyword%"]);
            })
            ->addColumn('consultar', function($entradas) {
                if(!EntradasProducto::onlyTrashed()->find($entradas->id)){
                    return '<a onclick="consultar(' . $entradas->id . ')" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
                    /* return '<a onclick="consultar(\'' . route('afuncionales.productos.entradas.show', $entradas->id) . '\')" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>'; */
                }                
            })
            ->addColumn('editar', function($entradas) {
                if(!EntradasProducto::onlyTrashed()->find($entradas->id)){
                    return '<a href="' . route('afuncionales.productos.entradas.edit', $entradas->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
                }
            })
            ->addColumn('cancelar', function($entradas) {
                if(!EntradasProducto::onlyTrashed()->find($entradas->id)){
                    return '<a class="btn btn-danger btn-xs" onclick="cancelar(' . $entradas->id . ');"><i class="fa fa-trash"></i> Cancelar</a>';
                }
            })
            ->addColumn('observación', function($entradas) {
                if(EntradasProducto::onlyTrashed()->find($entradas->id)){
                    return '<span class="label label-danger">CANCELADO</span>';
                }
            })
            ->rawColumns(['consultar', 'editar', 'cancelar', 'observación']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return EntradasProducto::search(['APOYOS FUNCIONALES','TRABAJO SOCIAL'])
        //El distinct es por si en la entrada se registraron diferentes productos, solo me importan los datos generales
        ->select(DB::raw(
            "entradas_productos.id AS id, entradas_productos.fechaentrada AS fecha, cat_tiposentradas.tipo AS tipo,".// cat_areas.id AS area, ".
            "IFNULL(".
                "( SELECT cat_instituciones.nombre FROM entradas_productos AS ep ".
                "INNER JOIN entradasproductos_dependencias ON entradasproductos_dependencias.entradas_producto_id = ep.id ".
                "INNER JOIN cat_instituciones ON cat_instituciones.id = entradasproductos_dependencias.dependencia_id ".
                "WHERE ep.id = entradas_productos.id AND entradasproductos_dependencias.deleted_at IS NULL ".
                "), ".
                "( SELECT concat(personas.nombre,' ',personas.primer_apellido,' ',personas.segundo_apellido) FROM entradas_productos AS ep ".
                "INNER JOIN entradasproductos_personas ON entradasproductos_personas.entradas_producto_id = ep.id ".
                "INNER JOIN personas ON personas.id = entradasproductos_personas.persona_id ".
                "WHERE ep.id = entradas_productos.id AND entradasproductos_personas.deleted_at IS NULL ".
                ")".
            ") AS remitente"))->groupby(DB::raw("1,2,3,4"));
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
            'remitente' => ['data' => 'remitente', 'name' => 'remitente'],
            'consultar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            //'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'cancelar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]//,
            //'observación' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { registrar_entrada(); }',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            /*'order' => [
                1,
                'asc'
            ]*/
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Afuncionales/Entradas_' . date('YmdHis');
    }
}