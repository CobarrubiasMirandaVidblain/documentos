<?php

namespace App\DataTables\Braille;

use App\Models\Persona;
use Yajra\DataTables\Services\DataTable;

class Personas extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables()->eloquent($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            $fecha = time() - strtotime($persona->fecha_nacimiento);
            return floor($fecha / 31556926);
        })
        ->addColumn('seleccionar', function($persona) {
          return '<a class="btn btn-success btn-xs" onclick="seleccionar_persona(' . "'" . $persona->id . "'" . ', ' . "'" . $persona->obtenerNombreCompleto(). "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
      })    
        ->rawColumns(['seleccionar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $model = Persona::leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
    		->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
    		->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
    		->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->leftjoin('empleados', 'empleados.persona_id', '=', 'personas.id')
        ->leftjoin('braillealumnos','braillealumnos.persona_id','=','personas.id')
        ->whereNull('empleados.persona_id')
        ->whereNull('braillealumnos.persona_id')
        ->select(
            [
                'personas.id as id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',
                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad'
            ]
        );
        if($this->request->input('tipo') === 'mayor') {
            $model->whereRaw("TIMESTAMPDIFF(YEAR,personas.fecha_nacimiento,'".date('Y-m-d')."') >= 18");
        }
        elseif ($this->request->input('tipo') === 'menor') {
            $model->whereRaw("TIMESTAMPDIFF(YEAR,personas.fecha_nacimiento,'".date('Y-m-d')."') < 18");
        }

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this->builder()
        ->columns($this->getColumns())
        ->ajax([
            'data'=>'function(d){
                if(elemento !== undefined){
                    if(elemento.attr("id") == "tutor_id"){
                        d.tipo = "mayor";
                    }
                }
            }'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'id' => ['data' => 'id', 'name' => 'personas.id'],
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'apellido_paterno' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'apellido_materno' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'edad' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'curp' => ['data' => 'curp', 'name' => 'personas.curp'],
            'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'seleccionar' => ['data' => 'seleccionar', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { if(elemento !== undefined) block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'personas_' . date('YmdHis');
    }
}
