<?php

namespace App\DataTables\Braille;

use Modules\Braille\Entities;
use Yajra\DataTables\Services\DataTable;

use App\Models\Braille\AlumnosBraille;
use App\Models\Persona;

class Beneficiarios extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        if($this->request->input('tipo')==='select'){
            return datatables($query)
            ->addColumn('completo',function($beneficiario){
                return $beneficiario->nombreCompleto();
            });
        }

        return datatables($query)
        ->addColumn('show',function($beneficiario){
            return '<a href="'.route('braille.beneficiarios.show',$beneficiario->id).'" class="btn btn-info btn-xs"><i class="fa fa-eye"> Informacion </i></a>';
        })
        // ->addColumn('edit',function($beneficiario){
        //     if ($beneficiario->estado == null)
        //         return '<a href="'.route('braille.beneficiarios.edit',$beneficiario->id).'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Editar</i></a>';
        //     return '<a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Editar</a>';
        // })
        ->addColumn('destroy',function($beneficiario){
            if ($beneficiario->estado == null)
                return '<a class="btn btn-xs btn-danger" onclick="eliminarRegistro('."'".route('braille.beneficiarios.destroy',$beneficiario->id)."','beneficiario'".')"><i class="fa fa-remove"> Dar de baja</i></a>';
            return '<a class="btn btn-xs btn-success"><i class="fa fa-check"> Reactivar</i></a>';
        })
        ->editColumn('estado',function($beneficiario){
            if ($beneficiario->estado == null)
                return 'Activo';
            return 'Baja';
        })
        ->editColumn('edad',function($beneficiario){
            $fecha = time() - strtotime($beneficiario->edad);
            return floor($fecha / 31556926);
        })
        ->rawColumns(['show','edit','destroy']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        if($this->request->input('tipo')==='select'){
            return Persona::join('braillealumnos', 'personas.id', '=', 'braillealumnos.persona_id')
            ->wherenull('braillealumnos.deleted_at')
            ->select([
                'personas.id as id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido'
            ]);
        }

        return AlumnosBraille::join('personas', 'personas.id', '=', 'braillealumnos.persona_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'braillealumnos.discapacidad_id')
        ->select([
            'braillealumnos.id as id',
            'cat_discapacidades.nombre as discapacidad',
            'braillealumnos.deleted_at as estado',
            'personas.id as persona_id',
            'personas.nombre as nombre',
            'personas.primer_apellido as primer_apellido',
            'personas.segundo_apellido as segundo_apellido',
            'personas.fecha_nacimiento as edad',
            'personas.curp as curp',
            'cat_regiones.nombre as region',
            'cat_distritos.nombre as distrito',
            'cat_municipios.nombre as municipio',
            'cat_localidades.nombre as localidad'
        ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'id'                => ['data' => 'id', 'name' => 'braillealumnos.id'],
            'estado'            => ['data' => 'estado', 'name' => 'braillealumnos.deleted_at'],
            'nombre'            => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido'   => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido'  => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'discapacidad'      => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
            'edad'              => ['data' => 'edad', 'name' => 'personas.fecha_nacimiento'],
            'curp'              => ['data' => 'curp', 'name' => 'personas.curp'],
            'region'            => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'consultar'         => ['data' => 'show', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            //'Editar'            => ['data' => 'edit', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'Eliminar'          => ['data' => 'destroy', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'distrito'          => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio'         => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad'         => ['data' => 'localidad', 'name' => 'cat_localidades.nombre']
        ];
        if($this->request->input('tipo')==='select'){
            $columns =[
                'nombre'            => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer_apellido'   => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo_apellido'  => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'completo'          => ['data' => 'nombreCompleto']
            ];
        }
        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'create',
                    'text'   => '<i class="fa fa-plus"></i> Agregar',
                    'className' => ''
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'beneficiarios braile al ' . date('Ymd');
    }
}
