<?php

namespace App\DataTables\SillaDeRuedas;

use Yajra\DataTables\Services\DataTable;
use App\Models\Producto;

class ProductoDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('editar', function($producto) {
                return '<a onclick="abrir_modal(\'modal-regismaterial\', ' . $producto->id . ');" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
            })
            ->addColumn('eliminar', function($producto) {
                return '<a class="btn btn-danger btn-xs" onclick="material_delete(' . $producto->id . ');"><i class="fa fa-trash"></i> Eliminar</a>';
            })
            ->rawColumns(['editar', 'eliminar']);;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return Producto::select('id', 'producto', 'vigencia');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->addAction(['width' => '80px'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'producto' => ['data' => 'producto', 'name' => 'producto'],
            'vigencia',
            'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'eliminar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'SillaDeRuedas/Producto_' . date('YmdHis');
    }
}
