<?php

namespace App\DataTables;

use App\Models\Beneficiosprograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;

class ProgramasBeneficiosDataTable extends DataTable{
  /**
   * Build DataTable class.
   *
   * @param mixed $query Results from query() method.
   * @return \Yajra\DataTables\DataTableAbstract
   */
  protected $actions = ['new'];
  public function dataTable($query){
      return datatables($query)
      ->addColumn('eliminar',function($beneficio){
        return "<button class='btn btn-warning btn-sm' onClick='eliminarBeneficio($beneficio->id)'><i class='fa fa-trash'></i> Eliminar</button>";
      })
      ->addColumn('editar',function($beneficio){
        $ruta = route('programas.beneficios.edit',[$this->programa_id,$beneficio->id]);
        return "<button class='btn btn-warning btn-sm' onClick='abrirModal(\"ceBeneficio\",\"$ruta\")'><i class='fa fa-edit'></i> Editar</button>";
      })
      ->addColumn('mostrar',function($beneficio){
        $rutaUpdate = route('programas.beneficios.update',[$this->programa_id,$beneficio->id]);
        $aux=$beneficio->predeterminado==1?'checked':'';
        return "<div class='checkbox'><input url='$rutaUpdate' class='toggle' type='checkbox' data-style='ios' data-size='mini' data-onstyle='success' data-offstyle='danger' data-width='95' data-on='Mostrar' data-off='Ocultar' $aux></div>";
      })
      ->addColumn('usuario',function($beneficio){
        return $beneficio->usuario->persona->obtenerNombreCompleto();
      })->rawColumns(['eliminar','editar','mostrar']);
  }

  /**
   * Get query source of dataTable.
   *
   * @param \App\User $model
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function query() {
    $apaux = AniosPrograma::where([['programa_id',$this->programa_id],['ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id]])->first()->id;
    return Beneficiosprograma::where('anio_programa_id',$apaux)->select('beneficiosprogramas.id as id','beneficiosprogramas.nombre','beneficiosprogramas.usuario_id','predeterminado');
  }
  
  /**
    * Optional method if you want to use html builder.
    *
    * @return \Yajra\DataTables\Html\Builder
    */
  public function html() {
    return $this->builder()
    ->columns($this->getColumns())
    ->ajax([
      'url' => route('programas.beneficios.index',$this->programa_id),
      'type' => 'GET',
    ])
    ->parameters($this->getBuilderParameters());
  }
  
  /**
    * Get columns.
    *
    * @return array
    */
  protected function getColumns() {
    return [
      'nombreDelBeneficio'=>['data'=>'nombre','name'=>'beneficiosprogramas.nombre'],
      'agregadoPor'=>['data'=>'usuario'],
      ['title'=>'Atn. Ciudadana','data'=>'mostrar'],
      'editar',
      'eliminar'
    ];
  }

  protected function getBuilderParameters() {
    $rutaCrearBeneficio = route('programas.beneficios.create',$this->programa_id);
    $builderParameters = [
        'language' => [
            'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
        ],
        'dom' => 'Btip',
        'drawCallback' => 'function() {
          $(".toggle").bootstrapToggle();
          $(".toggle").change(function(event){
            togleMostrarAtn($(event.target).attr("url"),$(this).prop("checked"));
          })
        }',
        'buttons' => [
            'new',
            'excel',
            'pdf',
            'reset',
            'reload'                
        ],
        'lengthMenu' => [ [10,20,-1], [10,20,'TODO'] ],
        'responsive' => true,
        'columnDefs' => [
            [ 'className' => 'text-center', 'targets' => '_all' ],
            // [ 'visible' => false, 'targets' => 0 ]
        ]
    ];

    return $builderParameters;
  }
  /**
   * Get filename for export.
   *
   * @return string
   */
  protected function filename(){
      return 'Beneficios_' . date('YmdHis');
  }
}
