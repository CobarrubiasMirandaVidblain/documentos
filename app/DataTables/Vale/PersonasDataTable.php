<?php

namespace App\DataTables\Vale;

use Modules\Vale\Entities\Persona;
use Yajra\DataTables\Services\DataTable;

class PersonasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->addColumn('edad', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
        })
        ->orderColumn('edad', '-fecha_nacimiento $1')
        ->filterColumn('edad', function($query, $keyword) {
            $edad = strtoupper($keyword);
            if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)) {
                preg_match_all('!\d+!', $edad, $valores);
                preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);
                if(count($valores[0]) === 2) {
                    $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0] . " AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][1] . " " . $valores[0][1]);
                }
                else {
                    $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0]);
                }
            }
        })
        ->addColumn('editar', function($persona) {
            return '<a class="btn btn-warning btn-xs" onclick="bus.$emit(' . "'" . 'editarPersona' . "'" . ', ' . "'" . $persona->id . "'" . ', ' . "'" . $persona->nombreCompleto() . "'" . ');"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('eliminar', function($persona) {
            return '<a href="#/destroy/' . $persona->id . '" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Eliminar</a>';
        })
        ->addColumn('seleccionar', function($persona) {
            return '<a class="btn btn-success btn-xs" onclick="bus.$emit(' . "'" . 'seleccionarPersona' . "'" . ', ' . "'" . $persona->id . "'" . ', ' . "'" . $persona->nombreCompleto() . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
        })
        ->rawColumns(['editar', 'eliminar', 'seleccionar'])
        ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return Persona::
        select(
            'personas.id as id',
            'personas.nombre as nombre',
            'personas.primer_apellido as primer_apellido',
            'personas.segundo_apellido as segundo_apellido',
            'personas.fecha_nacimiento as fecha_nacimiento',
            'personas.curp as curp',
            'cat_regiones.nombre as region',
            'cat_distritos.nombre as distrito',
            'cat_municipios.nombre as municipio',
            'cat_localidades.nombre as localidad'
        )
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftjoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftjoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftjoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->ajax(['data' => 'function(d) { d.table = "personas"; }'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'id' => ['data' => 'id', 'name' => 'personas.id'],
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'fecha_nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'edad' => ['data' => 'edad', 'name' => 'edad'],
            'curp' => ['data' => 'curp', 'name' => 'personas.curp'],
            'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'editar' => ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'seleccionar' => ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        return [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            /*'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],*/
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'personas_' . date('YmdHis');
    }
}