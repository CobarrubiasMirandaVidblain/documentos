<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\Beneficiosprograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use Illuminate\Support\Facades\DB;
use App\User;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Auth;

class BeneficiosExternosDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('seleccionar', function($beneficio) {
                return '<a class="btn btn-success btn-xs" onclick="seleccionar_beneficioExterno(' . "'" . $beneficio->id . "'" . ', ' . "'" .  $beneficio->nombre . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
            })->rawColumns(['seleccionar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        // return $model->newQuery()->select('id', 'add-your-columns-here', 'created_at', 'updated_at');
        $programas = array_map(function($p) { return $p->programa_id; }, array_filter( DB::select('call getProgramasHijos(?)',[759]) ,function($p) { return ($p->programa_tipo === 'SERVICIO'); }));
        $aniosP = AniosPrograma::whereIn('programa_id',$programas)->where('ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id)->get()->pluck('id');
        //dd($aniosP);
        //  $aniosP = array_map(function($p) { return $p->id; }, $aniosPAUX);
        return Beneficiosprograma::whereIn('anio_programa_id',$aniosP)->where('predeterminado',1);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.table = "beneficiosExt"; }'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'nombre' => ['data' => 'nombre', 'name' => 'nombre'],
            'seleccionar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => ['reload'],
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => 1 ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AtnCiudadana/BeneficiosExternos_' . date('YmdHis');
    }
}
