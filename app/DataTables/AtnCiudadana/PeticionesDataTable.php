<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\StatusSolicitud;
use App\Models\ProgramasSolicitud;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

class PeticionesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->editColumn('fecha', function($peticiones) {
            return with(\Carbon\Carbon::parse($peticiones->fecha))->format('d-m-Y');
        })
        ->filterColumn('fecha', function($query, $keyword) {
            $query->whereRaw("select DATE_FORMAT(fecha,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->orderColumn('dias transcurridos', 'dias_transcurridos $1')
        ->filterColumn('estatus', function($query, $keyword) {
            if(strtoupper($keyword) === 'NUEVO'){
                $keyword = 'VINCULADO';
            }
            $query->whereRaw("estatus like ?", ["%$keyword%"]);
        })          
        ->addColumn('estatus', function($peticiones) {
            $status = '';
            switch ($peticiones->estatus) {
                case "SOLICITANTE":
                    $status = '<span class="label label-warning">SOLICITANTE</span>';
                    break;
                case "VINCULADO":
                    $status = '<span class="label label-success">VINCULADO</span>';
                    break;
                case "VALIDANDO":
                    $status = '<span class="label label-warning">VALIDANDO</span>';
                    break;
                case "LISTA DE ESPERA":
                    $status = '<span class="label label-info">LISTA DE ESPERA</span>';
                    break;
                case "CANCELADO":
                    $status = '<span class="label label-danger">CANCELADO</span>';
                    break;
                case "RECHAZADO":
                    $status = '<span class="label label-danger">RECHAZADO</span>';
                    break;
                case "FINALIZADO":
                    $status = '<span class="label label-primary">FINALIZADO</span>';
                    break;
                case "Vo. Bo.":
                    $status = '<span class="label label-primary">Vo. Bo.</span>';
                    break;
            }
            return $status;
        })
        ->addColumn('seguimiento', function($peticion) {
            return "<a class='btn btn-primary btn-xs' href='" .
                    route('atnciudadana.peticiones.show', ['peticion_id' => $peticion->programa_solicitud_id])."'><i class='fa fa-arrow-right'></i> Seguimiento</a>";            
        })
        ->orderColumn('estatus', 'estatus $1')        
        ->rawColumns(['estatus','seguimiento','cancelar','reactivar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return ProgramasSolicitud::search('TODOS');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'folio' => ['data' => 'folio', 'name' => 'folio', 'orderable' => false],
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'remitente' => ['data' => 'remitente', 'name' => 'remitente'],
            'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
            'apoyo' => ['data' => 'apoyo', 'name' => 'apoyo'],
            'cantidad' => ['data' => 'cantidad', 'name' => 'cantidad'],
            'días transcurridos' => ['data' => 'dias_transcurridos', 'name' => 'dias_transcurridos'],
            'estatus' => ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false],
            'seguimiento' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ],
                ['visible' => false, 'targets' => [0,2,3] ]
            ],
            'rowGroup' => [
                'startRender' => 'function (rows,group) { return group + " " + rows.context[0].json.data[rows[0][0]].remitente + " (" + rows.context[0].json.data[rows[0][0]].tipo + ")" ; }',
                'dataSrc' => 'folio'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Peticiones_' . date('YmdHis');
    }
}
