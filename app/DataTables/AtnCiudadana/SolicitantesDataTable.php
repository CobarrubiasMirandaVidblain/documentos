<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\PeticionesPersonas;
use Yajra\DataTables\Services\DataTable;

class SolicitantesDataTable extends DataTable {

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        /*if($this->estatus == 'VINCULADO'){
            return datatables($query)
            ->addIndexColumn()
            ->addColumn('edad', function($persona) {
                return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
            })
            ->orderColumn('edad', '-fecha_nacimiento $1')
            ->filterColumn('edad', function($query, $keyword){
                $edad = strtoupper($keyword);
                if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)){
                    preg_match_all('!\d+!', $edad, $valores);
                    preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);                
                    if(count($valores[0]) == 2){                    
                        $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]." AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][1]." ".$valores[0][1]);
                    }else{
                        $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]);
                    }                
                }
            })
            ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('cat_regiones.nombre', function($query, $keyword) {
                $keyword = strtoupper($keyword);
                if(preg_match("/REGION=([A-ZñÑ]+)$/", $keyword)) {
                    $region = substr($keyword, 7);
                    $query->whereRaw("cat_regiones.nombre like ?", ["%$region%"]);
                }
            })
            ->filterColumn('cat_distritos.nombre', function($query, $keyword) {
                $keyword = strtoupper($keyword);
                if(preg_match("/DISTRITO=([A-ZñÑ]+)$/", $keyword)) {
                    $distrito = substr($keyword, 9);
                    $query->whereRaw("cat_distritos.nombre like ?", ["$distrito%"]);
                }
            })        
            ->filterColumn('estatus', function($query, $keyword){
                switch($keyword) {
                    case "APROBADO":
                        $query->whereRaw("solicitudes_personas.evaluado = 1");
                        break;
                    case "RECHAZADO":
                        $query->whereRaw("solicitudes_personas.evaluado = 0");
                        break;
                    case "VALIDANDO":
                        $query->whereRaw("solicitudes_personas.evaluado is null");
                        break;
                    case "ENTREGADO":
                        $query->whereRaw("solicitudes_personas.entregado = 1");
                        break;
                    case "CANCELADO":
                        $query->whereRaw("solicitudes_personas.entregado = 0");
                        break;
                    case "ESPERA":
                        $query->whereRaw("solicitudes_personas.entregado is null");
                        break;
                }
            })
            ->addColumn('consultar', function($solicitud) {
                if($solicitud->evaluado === null){
                    return '<button style="margin-right: 10px;" class="btn btn-info btn-xs" data-toggle="tooltip" title="Consultar" onclick="Beneficiarios.mostrar_beneficiario('.$solicitud->persona->id .')"><i class="fa fa-address-card"></i> Consultar</button>';
                }
            })
            ->addColumn('estatus', function($solicitud) {
                if($solicitud->beneficioprograma_solicitud->estadoActual() == "VINCULADO" || $solicitud->beneficioprograma_solicitud->estadoActual() == "VALIDANDO"){
                    if($solicitud->evaluado) {
                        return '<span class="label label-success">APROBADO</span>';
                    } elseif($solicitud->evaluado === 0) {
                        return '<span class="label label-danger">RECHAZADO</span>';
                    } elseif($solicitud->observacionevaluado !== null){
                        return '<span class="label label-danger">CANCELADO</span>';
                    } else {
                        return '<span class="label label-warning">VALIDANDO</span>';
                    }
                } else if($solicitud->beneficioprograma_solicitud->estadoActual() == "SOLICITANTE") {
                    if($solicitud->observacionevaluado === null) {
                        return '<span class="label label-success">SOLICITANTE</span>';
                    }
                    return '<span class="label label-danger">CANCELADO</span>';
                } else {
                    if($solicitud->entregado) {
                        return '<span class="label label-success">ENTREGADO</span>';
                    } elseif($solicitud->entregado === 0) {
                        return '<span class="label label-danger">CANCELADO</span>';
                    } else {
                        return '<span class="label label-warning">EN ESPERA</span>';
                    }
                }
            })
            ->addColumn('observación', function($solicitud) {
                if($solicitud->entregado === 1) {
                    return $solicitud->observacionentrega;                    
                }else if($solicitud->observacionentrega !== null) {
                    return $solicitud->observacionentrega;                    
                }else{
                    return $solicitud->observacionevaluado;                    
                }
            })
            ->rawColumns(['estatus', 'consultar'])
            ->with('programa_nombre', $this->programa_nombre)
            ->with('programa_estatus', $this->estatus)
            ->with('programa_cantidad', $this->programa_cantidad)
            ->with('programa_solicitantes', $this->programa_solicitantes);
        } else {*/
            return datatables($query)
            ->addIndexColumn()
            ->addColumn('edad', function($persona) {
                return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
            })
            ->orderColumn('edad', '-fecha_nacimiento $1')
            ->filterColumn('edad', function($query, $keyword){
                $edad = strtoupper($keyword);
                if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)){
                    preg_match_all('!\d+!', $edad, $valores);
                    preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);                
                    if(count($valores[0]) == 2){                    
                        $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]." AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][1]." ".$valores[0][1]);
                    }else{
                        $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]);
                    }                
                }
            })
            ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('cat_regiones.nombre', function($query, $keyword) {
                $keyword = strtoupper($keyword);
                if(preg_match("/REGION=([A-ZñÑ]+)$/", $keyword)) {
                    $region = substr($keyword, 7);
                    $query->whereRaw("cat_regiones.nombre like ?", ["%$region%"]);
                }
            })
            ->filterColumn('cat_distritos.nombre', function($query, $keyword) {
                $keyword = strtoupper($keyword);
                if(preg_match("/DISTRITO=([A-ZñÑ]+)$/", $keyword)) {
                    $distrito = substr($keyword, 9);
                    $query->whereRaw("cat_distritos.nombre like ?", ["$distrito%"]);
                }
            })        
            ->filterColumn('estatus', function($query, $keyword){
                switch($keyword) {
                    case "APROBADO":
                        $query->whereRaw("solicitudes_personas.evaluado = 1");
                        break;
                    case "RECHAZADO":
                        $query->whereRaw("solicitudes_personas.evaluado = 0");
                        break;
                    case "VALIDANDO":
                        $query->whereRaw("solicitudes_personas.evaluado is null");
                        break;
                    case "ENTREGADO":
                        $query->whereRaw("solicitudes_personas.entregado = 1");
                        break;
                    case "CANCELADO":
                        $query->whereRaw("solicitudes_personas.entregado = 0");
                        break;
                    case "ESPERA":
                        $query->whereRaw("solicitudes_personas.entregado is null");
                        break;
                }
            })
            ->addColumn('consultar', function($solicitud) {
                if($solicitud->evaluado === 0){
                    return '<button style="margin-right: 10px;" class="btn btn-info btn-xs" data-toggle="tooltip" title="Consultar" onclick="Personas.mostrar('.$solicitud->beneficiopersona->persona->id . ', false' .')"><i class="fa fa-address-card"></i> Consultar</button>';
                }
            })
            ->addColumn('estatus', function($solicitud) {
                if($solicitud->beneficioprograma_solicitud->estadoActual() == "VINCULADO" || $solicitud->beneficioprograma_solicitud->estadoActual() == "VALIDANDO"){
                    if($solicitud->evaluado) {
                        return '<span class="label label-success">APROBADO</span>';
                    } elseif($solicitud->evaluado === 0) {
                        return '<span class="label label-danger">RECHAZADO</span>';
                    } elseif($solicitud->observacionevaluado !== null){
                        return '<span class="label label-danger">CANCELADO</span>';
                    } else {
                        return '<span class="label label-warning">VALIDANDO</span>';
                    }
                } else if($solicitud->beneficioprograma_solicitud->estadoActual() == "SOLICITANTE") {
                    if($solicitud->observacionevaluado === null) {
                        return '<span class="label label-success">SOLICITANTE</span>';
                    }
                    return '<span class="label label-danger">CANCELADO</span>';
                } else {
                    if($solicitud->entregado === 1) {
                        return '<span class="label label-success">ENTREGADO</span>';
                    } elseif($solicitud->entregado === 0) {
                        return '<span class="label label-danger">CANCELADO</span>';
                    } else {
                        return '<span class="label label-warning">EN ESPERA</span>';
                    }
                }
            })
            ->addColumn('observación', function($solicitud) {
                if($solicitud->entregado === 1) {
                    return $solicitud->observacionentrega;                    
                }else if($solicitud->observacionentrega !== null) {
                    return $solicitud->observacionentrega;                    
                }else{
                    return $solicitud->observacionevaluado;                    
                }
            })
            ->addColumn('eliminar', function($solicitud) {
                if($solicitud->deleted_at){
                    return '<span class="label label-info noeliminar" disabled><i class="fa fa-ban"></i> Eliminar</span>';
                }
                if($solicitud->beneficioprograma_solicitud->estadoActual() == "SOLICITANTE" && $solicitud->observacion_evaluado === null) {
                    return '<button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="Beneficiarios.abrir_modal_eliminar('.
                    $solicitud->beneficiopersona->persona->id.',\''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\')"> <i class="fa fa-ban"></i> Eliminar</button>';
                }
            })
            ->addColumn('reactivar', function($solicitud){
                if($solicitud->beneficioprograma_solicitud->estadoActual() == "SOLICITANTE" && $solicitud->observacionevaluado !== null){
                    return '<button class="btn btn-warning btn-xs" data-toggle="tooltip" title="Reactivar" onclick="Beneficiarios.reactivar('.
                    $solicitud->beneficiopersona->persona->id.',\''.$solicitud->beneficiopersona->persona->get_nombre_completo().'\')"> <i class="fa fa-reply"></i> Reactivar</button>';
                }
            })
            ->rawColumns(['estatus', 'consultar', 'eliminar', 'reactivar'])
            ->with('programa_nombre', $this->programa_nombre)
            ->with('programa_estatus', $this->estatus)
            ->with('programa_cantidad', $this->programa_cantidad)
            ->with('programa_solicitantes', $this->programa_solicitantes);
        //}
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $query = PeticionesPersonas::withTrashed()->join('beneficiosprog_personas','beneficiosprog_personas.id','beneficiosprogsol_benefpersonas.beneficiopersona_id')
        ->join('personas', 'personas.id', '=', 'beneficiosprog_personas.persona_id')
        ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->join('beneficiosprogramas_solicitudes', 'beneficiosprogramas_solicitudes.id', '=', 'beneficiosprogsol_benefpersonas.beneficiosprogramas_solicitud_id')
        ->join('solicitudes', 'solicitudes.id', '=', 'beneficiosprogramas_solicitudes.solicitud_id')
        ->join('beneficiosprogramas', 'beneficiosprogramas.id', '=', 'beneficiosprogramas_solicitudes.beneficioprograma_id')
        ->leftjoin('afu_peticionesproductos','afu_peticionesproductos.beneficioprogsol_benefpersona_id','beneficiosprogsol_benefpersonas.id');
        if($this->programas_solicitud_id){
          $query->where('beneficiosprogsol_benefpersonas.beneficiosprogramas_solicitud_id',$this->programas_solicitud_id);
        }
        $query->select(
            [   
                'beneficiosprogsol_benefpersonas.id',// as solicitud_persona',
                'beneficiosprogsol_benefpersonas.beneficiopersona_id',
                'beneficiosprogsol_benefpersonas.beneficiosprogramas_solicitud_id as beneficiosprogramas_solicitud_id',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.curp as curp',
                'personas.fecha_nacimiento as fecha_nacimiento',
                //'cat_discapacidades.nombre as discapacidad',
                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',
                'beneficiosprogsol_benefpersonas.entregado as entregado',
                'beneficiosprogsol_benefpersonas.evaluado as evaluado',
                'beneficiosprogsol_benefpersonas.observacion_evaluado as observacionevaluado',
                'beneficiosprogsol_benefpersonas.observacion_entrega as observacionentrega',
                'afu_peticionesproductos.salidas_producto_id as salidas_producto_id',
                'beneficiosprogsol_benefpersonas.deleted_at as deleted_at'
            ]
        );
        return $query;
        // return SolicitudesPersona::solicitantes($this->programas_solicitud_id)
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.table = "solicitantes"; d.programa_id = Beneficiarios.getProgramaId(); d.solicitud_id = Solicitud.getId(); d.beneficioprograma_solicitud_id = Beneficiarios.getBeneficioProgramaSolicitudID(); }'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {

        /*$columns = [
                '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
                'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'CURP' => ['data' => 'curp', 'name' => 'personas.curp'],
                'edad' => ['data' => 'edad', 'name' => 'edad'],
                'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
                'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                'estatus' => ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false],
                'consultar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]                
            ];        

        if($this->estatus != 'VINCULADO'){
        */    $columns = [
                '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
                'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'CURP' => ['data' => 'curp', 'name' => 'personas.curp'],
                'edad' => ['data' => 'edad', 'name' => 'edad'],
                'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
                'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                'estatus' => ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false],
                'consultar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
                'eliminar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
                'observación' => ['searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => false],
                'reactivar' => ['searchable' => false, 'orderable' => false, 'exportable' => true, 'printable' => false],
            ];
        //}

        return $columns;
    }

    protected function getBuilderParameters() {        
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { $(\'[data-toggle="tooltip"]\').tooltip(); unblock(); }',
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { Personas.abrir_modal("beneficiarios") }',
                    'className' => 'button-dt tool nuevo-solicitante'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ],
                //[ 'responsivePriority' => 2, 'targets' => [4,5,6,7,8,9] ],
                //[ 'responsivePriority' => 1, 'targets' => [0,1,2,3,10] ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'AtnCiudadana/Solicitantes_' . date('YmdHis');
    }
}