<?php

namespace App\DataTables\AtnCiudadana;

use Yajra\DataTables\Services\DataTable;
use Modules\AgendaEventos\Entities\Gira;
use Illuminate\Support\Facades\DB;

class GirasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
            ->addIndexColumn()
            ->editColumn('fecha', function($giras) {
                return with(\Carbon\Carbon::parse($giras->fecha))->format('d-m-Y');
            })
            ->orderColumn('fecha', 'fecha $1')
            ->filterColumn('fecha', function($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(fecha, '%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->filterColumn('municipio', function($query, $keyword) {
                $query->whereRaw("cat_municipios.nombre like ?", ["%$keyword%"]);
            })
            ->filterColumn('region', function($query, $keyword) {
                $query->whereRaw("cat_regiones.nombre like ?", ["%$keyword%"]);
            })
            ->addColumn('seleccionar', function($gira) {
                return '<a class="btn btn-success btn-xs" onclick="Giras.seleccionar_gira(' . "'" . $gira->id . "'" . ', ' . "'" . addslashes( $gira->nombre ). "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
            })
            ->addColumn('consultar', function($gira) {
                return "<a class='btn btn-primary btn-xs' href='" .
                route('giras.show', ['gira_id' => $gira->id])."'><i class='fa fa-arrow-right'></i> Consultar</a>";
            })
            ->rawColumns(['seleccionar','consultar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return $model = Gira::search()->select(
            DB::raw('even_eventos.id as id, 
            even_eventos.nombre as nombre, 
            even_eventos.fecha as fecha, 
            cat_municipios.nombre as municipio,
            IFNULL(cat_localidades.nombre, even_eventos.localidad) as localidad,
            cat_regiones.nombre as region')
        )->orderBy('even_eventos.nombre');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax(['data' => 'function(d) { d.table = "giras"; }'])                    
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        if($this->tipo === 'modal'){
            return [
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'evento' => ['data' => 'nombre', 'name' => 'nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'municipio'],
            'localidad' => ['data' => 'localidad', 'name' => 'localidad'],
            'región' => ['data' => 'region', 'name' => 'region'],
            'seleccionar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            ];
        }
        return [
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'evento' => ['data' => 'nombre', 'name' => 'nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'municipio'],
            'localidad' => ['data' => 'localidad', 'name' => 'localidad'],
            'región' => ['data' => 'region', 'name' => 'region'],
            'consultar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { abrir_modal("modal-gira"); }',
                    'className' => 'button-dt tool modal-gira'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool modal-gira'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool modal-gira'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ],
                [ 'visible' => false, 'targets' => 1 ]
            ],
            'rowGroup' => [
                'dataSrc' => 'nombre'
            ]/*,
            'order' => [
                0,
                'desc'
            ]*/
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AtnCiudadana/Giras_' . date('YmdHis');
    }
}
