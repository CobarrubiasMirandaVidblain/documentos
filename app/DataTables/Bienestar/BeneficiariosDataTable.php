<?php

namespace App\DataTables\Bienestar;

use App\Traits\Bienestar;

use Yajra\DataTables\Services\DataTable;

use App\Models\AniosPrograma;
use App\Models\PersonasPrograma;
use App\Models\BienestarDispersion;
use App\Models\BienestarPersona;

use Illuminate\Support\Facades\Auth;

class BeneficiariosDataTable extends DataTable {
    use Bienestar;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        if ($this->tipo=='refrendo') {
            return datatables($query)
            ->editColumn('fecha_nacimiento', function($candidato) {
                return $candidato->fecha_nacimiento ? date("d/m/Y", strtotime($candidato->fecha_nacimiento)) : '';
            })
            ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
                    $sql = "DATE_FORMAT(personas.fecha_nacimiento, '%d/%m/%Y') like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('refrendo',function($beneficiario){
                return '<a onclick="setPersona('.$beneficiario->id.')" class="btn btn-success" data-toggle="modal" data-target="#confirmacion"> Confirmar refrendo</a>';
            })->rawColumns(['refrendo']);
        }

        if ($this->tipo=='dispersion') {
            return datatables($query)
            ->editColumn('fecha_nacimiento', function($candidato) {
                return $candidato->fecha_nacimiento ? date("d/m/Y", strtotime($candidato->fecha_nacimiento)) : '';
            })
            ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
                    $sql = "DATE_FORMAT(personas.fecha_nacimiento, '%d/%m/%Y') like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('seleccionado',function($beneficiario){
                $lista = $this->lista->id;
                if($lista>0){
                    return '<input type="checkbox" class="seleccion" id="'. $beneficiario->id .'">
                    <label>'.$this->searchBeneficiarioLista($lista,$beneficiario->id).'</label>';
                }
                return '<input type="checkbox" class="seleccion" id="'. $beneficiario->id .'"><label></label>';
            })
            ->rawColumns(['seleccionado'])
            ;
        }


        return datatables($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->editColumn('tutor_fecha_nacimiento', function($persona) {
            return $persona->tutor_fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->tutor_fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('tutor.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(tutor.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })

        ->editColumn('genero', function($persona) {
            if($persona->genero === 'M') {
                return 'MASCULINO';
            }

            if($persona->genero === 'F') {
                return 'FEMENINO';
            }
        })
        ->filterColumn('personas.genero', function($query, $keyword) {
            $keyword = strtoupper($keyword);

            if($keyword === 'MASCULINO') {
                $query
                ->where('personas.genero', '=', 'M');
            }

            if($keyword === 'FEMENINO') {
                $query
                ->where('personas.genero', '=', 'F');
            }
        })


        /*->editColumn('tutor_genero', function($persona) {
            if($persona->genero === 'M') {
                return 'MASCULINO';
            }

            if($persona->genero === 'F') {
                return 'FEMENINO';
            }
        })
        ->filterColumn('tutor.genero', function($query, $keyword) {
            $pagado = strtoupper($keyword);

            if($pagado === 'MASCULINO') {
                $query
                ->where('tutor.genero', '=', 'M');
            }

            if($pagado === 'FEMENINO') {
                $query
                ->where('tutor.genero', '=', 'F');
            }
        })*/




        ->editColumn('estatus', function($bienestarpersona) {
            if($bienestarpersona->trashed()) {
                return '<span class="label label-danger">INACTIVO</span>';
            }
            else {
                return '<span class="label label-success">ACTIVO</span>';
            }
        })
        ->filterColumn('bienestarpersonas.deleted_at', function($query, $keyword) {
            if(strtoupper($keyword) === 'ACTIVO') {
                $query->whereRaw("bienestarpersonas.deleted_at is null");
            }
            if(strtoupper($keyword) === 'INACTIVO') {
                $query->whereRaw("bienestarpersonas.deleted_at is not null");
            }
        })
        ->addColumn('convertir', function($bienestarpersona) {
            if($bienestarpersona->trashed()) {
                //return '<a class="btn btn-success btn-xs" onclick="reactivar_beneficiario(' . $bienestarpersona->id . ');"><i class="fa fa-thumbs-o-up"></i> Reactivar</a>';
            }
            else {
                return '<a class="btn btn-danger btn-xs" onclick="abrir_modal(\'modal-inactivar\',' . $bienestarpersona->personas_programa_id . ', \'' . $bienestarpersona->id . '\', \'' . $bienestarpersona->foliounico . '\', \'' . $bienestarpersona->persona->get_nombre_completo() .'\');"><i class="fa fa-ban"></i> Inactivar</a>';
            }
        })
        ->addColumn('consultar', function($bienestarpersona) {
            if(!$bienestarpersona->trashed()) {
                return '<a href="' . route('bienestar.beneficiarios.show', $bienestarpersona->personas_programa_id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
            }
        })
        ->addColumn('editar', function($bienestarpersona) {
            if(!$bienestarpersona->trashed()) {
                return '<a href="' . route('bienestar.solicitantes.edit', $bienestarpersona->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
            }
        })
        /*->filterColumn('bienestarpersonas.foliounico', function($query, $keyword) {
            $sql = "bienestarpersonas.foliounico = ?";
            $foliounico = explode("F=", $keyword);
            $folio = $foliounico[1];
            //dd($folio);
            //$query->whereRaw($sql, ["%{$folio}%"]);
            $query->whereRaw("bienestarpersonas.foliounico = ?", ["$folio"]);
        })*/
        ->filter(function($query) {
            if($this->request()->has('foliounico')) {
                $query->where('foliounico', 'like', "%" . $this->request()->get('foliounico') . "%");
            }
        }, true)
        ->rawColumns(['estatus', 'convertir', 'consultar', 'editar']);
    }
    /***
    *   searchBeneficiarioLista recibe una lista
    *   y un beneficiario comprueba si dicho beneficiario
    *   pertenece  o no a esa lista.
    **/
    public function searchBeneficiarioLista($lista,$beneficiario){
        $registro=BienestarDispersion::where('personas_programa_id',$beneficiario)
        ->where('lista_id',$lista)
        ->count();

        if($registro>0)
            return 1;
        else
            return 0;
    }
    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $model = BienestarPersona::withTrashed()
        ->Beneficiarios()
        ->select(
            [
                'bienestarpersonas.id as id',
                'personas_programas.id as personas_programa_id',
                'bienestarpersonas.posicion as posicion',
                'bienestarpersonas.foliounico as foliounico',

                'discapacidades.nombre as general',

                'cat_discapacidades.nombre as especifica',

                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',
                'personas.genero as genero',

                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',

                'tutor.nombre as tutor_nombre',
                'tutor.primer_apellido as tutor_primer_apellido',
                'tutor.segundo_apellido as tutor_segundo_apellido',
                'tutor.fecha_nacimiento as tutor_fecha_nacimiento',
                'tutor.curp as tutor_curp',
                'tutor.genero as tutor_genero',

                'cuentasbancos.num_cuenta as num_cuenta',

                'tutor_cat_regiones.nombre as tutor_region',
                'tutor_cat_distritos.nombre as tutor_distrito',
                'tutor_cat_municipios.nombre as tutor_municipio',
                'tutor_cat_localidades.nombre as tutor_localidad',
                
                'bienestarpersonas.deleted_at as deleted_at'
            ]
        );

        $rol_enlace = 0;
        $rol_enlace_array = [];

        if(Auth::user()->hasRolesModulo(['ENLACE CAÑADA'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'CAÑADA');
            //$model->where('cat_regiones.nombre', '=', 'CAÑADA');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE COSTA'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'COSTA');
            //$model->where('cat_regiones.nombre', '=', 'COSTA');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE ISTMO'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'ISTMO');
            //$model->where('cat_regiones.nombre', '=', 'ISTMO');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE MIXTECA'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'MIXTECA');
            //$model->where('cat_regiones.nombre', '=', 'MIXTECA');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE PAPALOAPAM'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'PAPALOAPAM');
            //$model->where('cat_regiones.nombre', '=', 'PAPALOAPAM');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE SIERRA NORTE'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'SIERRA NORTE');
            //$model->where('cat_regiones.nombre', '=', 'SIERRA NORTE');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE SIERRA SUR'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'SIERRA SUR');
            //$model->where('cat_regiones.nombre', '=', 'SIERRA SUR');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE VALLES CENTRALES'], 2)) {
            $rol_enlace = 1;
            array_push($rol_enlace_array, 'VALLES CENTRALES');
            //$model->where('cat_regiones.nombre', '=', 'VALLES CENTRALES');
        }

        if($rol_enlace === 1) {
            $model->whereIn('cat_regiones.nombre', $rol_enlace_array);
        }

        if ($this->tipo=='refrendo') {
            $aniosPrograma = AniosPrograma::leftjoin('programas', 'programas.id', '=', 'anios_programas.programa_id')
            ->leftjoin('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
            ->where('programas.nombre', 'BIENESTAR')
            ->where('cat_ejercicios.anio',$this->anio - 1)->first(['anios_programas.id'])->id;

            $model=PersonasPrograma::datosPersonales()->where('anios_programa_id',$aniosPrograma)
            ->select(['personas_programas.id','personas_programas.persona_id as persona','foliounico',
            'personas.nombre as nombre','primer_apellido','segundo_apellido','fecha_nacimiento','curp',
            'cat_discapacidades.nombre as discapacidad','cat_regiones.nombre as region','cat_distritos.nombre as distrito',
            'cat_municipios.nombre as municipio','cat_localidades.nombre as localidad']);
        }

        if ($this->tipo=='dispersion') {
            $bimestre   = $this->lista->bimestre;
            $anio       = $this->lista->ejercicio->anio;
            $dispersion = $this->lista->id;

            $aniosPrograma = AniosPrograma::leftjoin('programas', 'programas.id', '=', 'anios_programas.programa_id')
            ->leftjoin('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
            ->where('programas.nombre', 'BIENESTAR')
            ->where('cat_ejercicios.anio',$anio)->first(['anios_programas.id'])->id;

            $beneficiariosSQL=PersonasPrograma::Search($this->programa, $this->anio)
            ->generarDispersion($anio,$bimestre,$aniosPrograma,$dispersion) 
            ->wherenotnull('cuentasbancos.num_cuenta')
            ->where('cuentasbancos.num_cuenta','!=','')

            //Para los que not tienen cuenta
            //->wherenull('cuentasbancos.num_cuenta')
            ;

            $model = $beneficiariosSQL->select([
                //'personas_programas.id as personas_programa_id',
                //'cuentasbancos.id as cuenta_id',

                //'bienestardispersiones.personas_programa_id as seleccionado',
                
                'personas_programas.id as id',
                'bienestarpersonas.id as bienestarpersonas_id',
                'bienestarpersonas.posicion as posicion',
                'bienestarpersonas.foliounico as foliounico',
                'cat_discapacidades.nombre as discapacidad',
                //'personas_programas.deleted_at as estatus',
                //'cat_ejercicios.anio as anio',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',

                /*'t.nombre as tutor_nombre',
                't.primer_apellido as tutor_primer_apellido',
                't.segundo_apellido as tutor_segundo_apellido',
                'cuentasbancos.num_cuenta as num_cuenta',*/
                'tutor.nombre as tutor_nombre',
                'tutor.primer_apellido as tutor_primer_apellido',
                'tutor.segundo_apellido as tutor_segundo_apellido',
                'cuentasbancos.num_cuenta as num_cuenta',

                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',
                'personas_programas.deleted_at as deleted_at'
            ])
            ;
        }

        /*if($this->request()->get('foliounico')) {
            $model->where('foliounico', $this->request()->get('foliounico'));
        }*/

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns =
        [
            //'id' => ['data' => 'id', 'name' => 'bienestarpersonas.id'],
            'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
            'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],

            'discapacidad general' => ['data' => 'general', 'name' => 'discapacidades.nombre'],
            
            'discapacidad especifica' => ['data' => 'especifica', 'name' => 'cat_discapacidades.nombre'],

            'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'bienestarpersonas.deleted_at'],
            'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],
            'genero del beneficiario' => ['data' => 'genero', 'name' => 'personas.genero'],

            'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            
            'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
            'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
            'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
            'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
            'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
            'genero del tutor' => ['data' => 'tutor_genero', 'name' => 'tutor.genero'],

            'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],

            'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
            'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre'],
            'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre'],
            'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre']
        ];

        if($this->tipo === 'dispersion') {
            $columns =
            [
                //'id' => ['data' => 'id', 'name' => 'bienestarpersonas.id'],
                'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
                'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],
                'discapacidad del beneficiario' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
                'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'bienestarpersonas.deleted_at'],
                'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
                'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],
    
                'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
                'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                
                'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
                'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
                'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
                //'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
                //'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
    
                'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta']//,
    
                //'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
                //'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre'],
                //'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre'],
                //'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre']
            ];
        }

        if($this->tipo === 'normal') {
            if(Auth::user()->hasRolesModulo(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO'], 2)) {
                $columns['convertir'] = ['data' => 'convertir', 'name' => 'convertir', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            }
            $columns['consultar'] = ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            if(Auth::user()->hasRolesModulo(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO'], 2)) {
                $columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            }
        }

        if($this->tipo === 'refrendo') {
            $columns =[
                'Folio unico'         => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
    			'Discapacidad'        => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
    			'Nombre'              => ['data' => 'nombre', 'name' => 'personas.nombre'],
    			'Primer apellido'     => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
    			'Segundo apellido'    => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
    			'Fecha de nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
    			'CURP'                => ['data' => 'curp', 'name' => 'personas.curp'],
    			'Region'              => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
    			'Distrito'            => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
    			'Municipio'           => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
    			'Locialidad'          => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                'Refrendar'           => ['data' => 'refrendo', 'orderable' => false, 'searchable' => false]
            ];
        }

        if($this->tipo === 'dispersion') {
        
        $columns =
        [
            //'id' => ['data' => 'id', 'name' => 'personas_programas.id'],
            //'seleccionar' => ['data' => 'seleccionado', 'name' => 'bienestardispersiones.personas_programa_id', 'searchable' => false, 'orderable' => true],

            'seleccionar' => ['data' => 'seleccionado', 'searchable' => false, 'orderable' => false],

            //'ppid' => ['data' => 'personas_programa_id', 'name' => ''],
            //'cid' => ['data' => 'cuenta_id', 'name' => ''],

            'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
            'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],
            'discapacidad del beneficiario' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
            //'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'personas_programas.deleted_at'],
            'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            //'edad del beneficiario' => ['data' => 'edad', 'name' => 'edad'],
            'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],

            /*'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion'],
            'foliounico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
            'discapacidad' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
            'estatus' => ['data' => 'estatus', 'name' => 'personas_programas.deleted_at'],
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'fecha_nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'curp' => ['data' => 'curp', 'name' => 'personas.curp'],*/

            'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
            'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
            'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
            'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],

            'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre']
        ];
        
            /*$columns =[
                'Seleccionar'         => ['data' => 'seleccionado', 'searchable' => false, 'orderable' =>false],
                'Folio unico'         => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
    			'Discapacidad'        => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
    			'Nombre'              => ['data' => 'nombre', 'name' => 'personas.nombre'],
    			'Primer apellido'     => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
    			'Segundo apellido'    => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
    			'Fecha de nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
    			'CURP'                => ['data' => 'curp', 'name' => 'personas.curp'],
    			'Region'              => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
    			'Distrito'            => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
    			'Municipio'           => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
    			'Locialidad'          => ['data' => 'localidad', 'name' => 'cat_localidades.nombre']
            ];*/
        }
        /*if($this->tipo === 'bienestarcandidatos') {
            $columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            $columns['seleccionar'] = ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }*/

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => ''
                ],
                /*[
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => ''
                ],*/
                /*[
                    'extend' => 'csv',
                    'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                    'className' => ''
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => ''
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => ''
                ],
                [
                    'extend' => 'print',
                    'text' => '<i class="fa fa-print"></i> Imprimir',
                    'className' => ''*//*,
                    'key' => [
                        'key' => 'p',
                        'ctrlKey' => true
                    ]*/
                //],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            //'autoWidth' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        /*if($this->tipo === 'bienestarcandidatos') {
            $builderParameters['buttons'] =
            [
                'extend' => 'csv',
                'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                'className' => ''
            ];
        }*/

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'beneficiarios_' . date('YmdHis');
    }
}