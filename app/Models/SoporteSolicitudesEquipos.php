<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoporteSolicitudesEquipos extends Model
{
    
    protected $table = 'soportesolicitudes_equipos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","tecnico_id","estado_equipo", "observaciones"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function soportesolicitudes(){
		return $this->hasMany('App\Models\SoporteSolicitudes', 'solicitud_id', 'id');
	}

public function empleado(){
	return $this->belongsTo('App\Models\Empleado', 'tecnico_id', 'id');
}

public function respuestassoportesolicitudes(){
		return $this->hasOne('App\Models\RespuestasSoporteSolicitudes', 'solicitud_id', 'id');
		}

}
