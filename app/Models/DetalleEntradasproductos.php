<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleEntradasproductos extends Model {
    protected $table = "detallesentradas_productos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["entradas_producto_id","cantidad","precio","beneficioprograma_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function areasproducto(){
    	return $this->belongsTo('App\Models\AreasProducto','areas_producto_id');
    }

    public function entradasproducto(){
    	return $this->belongsTo('App\Models\EntradasProducto');
    }

    public function productosfoliados(){
    	return $this->hasMany('App\Models\Productosfoliados','detallesentradas_producto_id');
    }
}
