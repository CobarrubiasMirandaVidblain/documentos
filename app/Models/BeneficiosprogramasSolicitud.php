<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeneficiosprogramasSolicitud extends Model{
	use SoftDeletes;
	
	protected $table = 'beneficiosprogramas_solicitudes';
	protected $dates = ['deleted_at'];
	protected $fillable=["solicitud_id","beneficioprograma_id","cantidad","folio","usuario_id"];
	protected $hidden = array('updated_at', 'deleted_at');
	
	public function usuario(){//quien dio de alta el registro
		return $this->belongsTo('App\Models\Usuario');
	}
	
	public function solicitud(){
		return $this->belongsTo('App\Models\Solicitud');
    }
    
	public function beneficio(){
		return $this->belongsTo('App\Models\Beneficiosprograma','beneficioprograma_id','id');
	}
    
  public function petcionespersonas() {
      return $this->hasMany('App\Models\PeticionesPersonas');
  }

  public function estadosSolicitud() {
      return $this->hasMany('App\Models\EstadosSolicitud','beneficioprograma_solicitud_id','id');
  }

  public function estadoActual() {
      return $this->estadossolicitud->last()->statusproceso->status;
      // return $this->hasOne('App\Models\EstadosSolicitud','beneficioprograma_solicitud_id','id')->latest();
  }
  
  public function statusActual() {
      //return $this->estadossolicitud->last()->statusproceso->status;
      return $this->hasOne('App\Models\EstadosSolicitud','beneficioprograma_solicitud_id','id')->latest();
  }

	public function scopeBeneficiosXsolicitud($query, $solicitud_id) {
        // 
        $beneficios = $query->select(DB::raw("beneficiosprogramas_solicitudes.id AS beneficioprograma_solicitud_id,
        IF(beneficiosprogramas_solicitudes.cantidad = 0, 'X', beneficiosprogramas_solicitudes.cantidad) as cantidad,
        beneficiosprogramas.puede_agregar_benef as beneficiarios,
        solicitudes.id AS solicitud_id,
        programas.id AS programa_id,
        beneficioprograma_id as beneficio_id,
        beneficiosprogramas_solicitudes.folio AS folio,
        cat_areas.nombre as area,
		CONCAT(personas.nombre,' ',personas.primer_apellido,' ',personas.segundo_apellido) as titular,
		cat_tiposremitentes.tipo as tipo,
        beneficiosprogramas.nombre AS apoyo,
        cat_statusprocesos.status AS estatus,
        cat_instituciones.nombre AS dependencia,
        (select created_at from estados_solicitudes where beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id and statusproceso_id = 3) as fecha"
        ))
		->join('beneficiosprogramas','beneficiosprogramas.id','beneficiosprogramas_solicitudes.beneficioprograma_id')
		->join('anios_programas','anios_programas.id','beneficiosprogramas.anio_programa_id')
		->join('programas','programas.id','anios_programas.programa_id')
		//Se obtiene el area responsable del PROGRAMA PADRE
        ->leftjoin('areas_programas','areas_programas.programa_id','programas.padre_id')
		->leftjoin('cat_areas','cat_areas.id','areas_programas.area_id')
		->leftjoin('areas_responsables','areas_responsables.area_id','areas_programas.area_id')
		->leftjoin('empleados','empleados.id','areas_responsables.empleado_id')
		->leftjoin('personas','personas.id','empleados.persona_id')
		->join('solicitudes', 'solicitudes.id',  'beneficiosprogramas_solicitudes.solicitud_id')
		->join('estados_solicitudes as ss', 'ss.beneficioprograma_solicitud_id', 'beneficiosprogramas_solicitudes.id')
		->join('cat_statusprocesos', 'cat_statusprocesos.id',  'ss.statusproceso_id' )
        ->join('cat_tiposremitentes','solicitudes.tiposremitente_id','cat_tiposremitentes.id')
        ->leftjoin('atnc_solicitudesexternas','solicitudes.id','atnc_solicitudesexternas.solicitud_id')
        ->leftjoin('cat_instituciones','atnc_solicitudesexternas.dependencia_id','cat_instituciones.id')
        ->whereNull('beneficiosprogramas.deleted_at')
        ->whereNull('programas.deleted_at')
        ->whereNull('areas_programas.deleted_at')
        ->whereNull('areas_responsables.deleted_at');
        //->groupBy(DB::raw('1,2,3,4,5,6,7,8,9,10,11,12,13'));
        if($solicitud_id!=0)
		    $beneficios->whereRaw('solicitudes.id = '. $solicitud_id .' and ss.created_at = (select max(sts.created_at) from estados_solicitudes as sts where ss.beneficioprograma_solicitud_id = sts.beneficioprograma_solicitud_id and sts.deleted_at is null) ');
		
		return DB::table(DB::raw("({$beneficios->toSql()}) as x"))
		->select(['beneficioprograma_solicitud_id', 'dependencia', 'solicitud_id', 'programa_id', 'beneficio_id', 'apoyo', 'folio', 'tipo', 'estatus', 'fecha', 'cantidad', 'area', 'titular', 'beneficiarios'])
		->where('estatus', '!=', 'CANCELADO');
  }
    
  public function scopePeticiones($query) {
      $db_raw =  "beneficiosprogramas_solicitudes.id as id, beneficiosprogramas_solicitudes.id AS beneficioprograma_solicitud_id, beneficiosprogramas_solicitudes.cantidad as cantidad,
                  solicitudes.id AS solicitud_id, programas.id AS programa_id, solicitudes.folio AS folioSolicitud, beneficiosprogramas_solicitudes.folio as folio,
                  beneficioprograma_id,
                  CASE 
                      WHEN solicitudes.tiposremitente_id = 4 THEN 
                          ( SELECT CONCAT(p.nombre,' ',p.primer_apellido,' ',p.segundo_apellido) 
                              FROM solicitudes_personales sp, personas p 
                                  WHERE sp.persona_id = p.id AND solicitudes.id = sp.solicitud_id) 
                      WHEN solicitudes.tiposremitente_id = 3 THEN 
                          ( SELECT d.nombre 
                              FROM solicitudes_dependencias sd, cat_instituciones d 
                                  WHERE sd.dependencia_id = d.id AND solicitudes.id = sd.solicitud_id) 
                      WHEN solicitudes.tiposremitente_id = 2 THEN 
                          ( SELECT m.nombre 
                              FROM solicitudes_municipios sm, cat_municipios m 
                                  WHERE sm.municipio_id = m.id AND solicitudes.id = sm.solicitud_id) 
                      WHEN solicitudes.tiposremitente_id = 1 THEN 
                          ( SELECT r.nombre 
                              FROM solicitudes_regiones sr, cat_regiones r 
                                  WHERE sr.region_id = r.id AND solicitudes.id = sr.solicitud_id) 
                  END AS remitente, 
                  cat_tiposremitentes.tipo as tipo, 
                  beneficiosprogramas.nombre AS apoyo, cat_statusprocesos.status AS estatus, 
                  CASE WHEN IFNULL((SELECT count(*) from cat_statusprocesos where beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id AND statusproceso_id >= 6 AND statusproceso_id != 9 ), -1) = 0 
                  THEN (SELECT DATEDIFF(CURDATE(), (SELECT created_at FROM estados_solicitudes WHERE beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id AND statusproceso_id = 3))) 
                  ELSE (SELECT DATEDIFF((SELECT created_at FROM estados_solicitudes WHERE	beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id AND statusproceso_id >= 6 AND statusproceso_id != 9 AND estados_solicitudes.deleted_at is null), 
                  (SELECT created_at FROM estados_solicitudes WHERE beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id	AND statusproceso_id = 3))) END as dias_transcurridos, 
                  (select created_at from estados_solicitudes where beneficioprograma_solicitud_id = beneficiosprogramas_solicitudes.id and statusproceso_id = 3) as fecha,
                  areas_programas.area_id,
                  cat_areas.nombre as area_responsable";

        $query ->select(DB::raw($db_raw))
              ->join('beneficiosprogramas','beneficiosprogramas.id','beneficiosprogramas_solicitudes.beneficioprograma_id')
              ->join('anios_programas','anios_programas.id','beneficiosprogramas.anio_programa_id')
              ->join('programas','programas.id','anios_programas.programa_id')
              ->join('solicitudes', 'solicitudes.id',  'beneficiosprogramas_solicitudes.solicitud_id')
              ->join('estados_solicitudes as ss', 'ss.beneficioprograma_solicitud_id','beneficiosprogramas_solicitudes.id')
              ->join('cat_statusprocesos', 'cat_statusprocesos.id',  'ss.statusproceso_id')
              ->join('cat_tiposremitentes','solicitudes.tiposremitente_id','cat_tiposremitentes.id')
              ->leftjoin('areas_programas','areas_programas.programa_id','programas.padre_id')
              ->leftjoin('cat_areas','cat_areas.id','areas_programas.area_id')
              ->leftjoin('areas_responsables','areas_responsables.area_id','areas_programas.area_id')
              ->whereRaw('ss.created_at = (select max(sts.created_at) from estados_solicitudes as sts where ss.beneficioprograma_solicitud_id = sts.beneficioprograma_solicitud_id and sts.deleted_at is null)')
              ->whereNull('beneficiosprogramas.deleted_at')
              ->whereNull('programas.deleted_at')
              ->whereNull('areas_programas.deleted_at')
              ->whereNull('areas_responsables.deleted_at');

      return $query;
  }
    
}