<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model
{
    protected $table = 'cat_documentos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","descripcion","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function usuario(){//quien dio de alta el documento
    	return $this->belongsTo('App\Models\Usuario');
    }


    public function documentosprogramas(){
    	return $this->hasMany('App\Models\DocumentosPrograma');
    }

    
}
