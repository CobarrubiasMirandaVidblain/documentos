<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Empleado extends Model
{
	protected $table = 'empleados'; 
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $fillable=['rfc', 'usuario_id', 'num_empleado','segurosocial','nivel_id','tiposempleado_id', 'persona_id', 'area_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function scopeSearch($query) {
        $query
        ->join('personas', 'personas.id', '=', 'empleados.persona_id')
        ->leftJoin('cat_tiposempleados', 'cat_tiposempleados.id', '=', 'empleados.tiposempleado_id')
        ->leftJoin('cat_areas', 'cat_areas.id', '=', 'empleados.area_id');
    }

	public function nivel(){
		return $this->belongsTo('App\Models\Nivel');
	}

	public function tiposempleado(){
		return $this->belongsTo('App\Models\Tiposempleado');
    }
    
    public function persona()
    {
        return $this->belongsTo('App\Models\Persona');
    }

	public function usuario(){//quien dio de alta el empleado
    	return $this->belongsTo('App\Usuario');
    }

    public function area(){
        return $this->belongsTo('App\Models\Area');
    }

    public function areasresponsables(){
        return $this->hasOne('App\AreasResponsable');
    }

    public function entradasproductos(){
        return $this->hasMany('App\EntradasProducto');
    }

    
    public function salidasproductos(){
      return $this->hasMany('App\SalidasProducto');
    }
    
    public function soportedictamenes(){
      return $this->belongsTo('App\Models\SoporteDictamenes');
    }

    public function equipos(){
        return $this->hasMany('App\Models\Equipos', 'resguardo_id', 'id');
    }

    public function soportesolicitudesequipos(){
        return $this->hasMany('App\Models\SoporteSolicitudesEquipos', 'tecnico_id', 'id');
    }
    
    public function consultas(){
        return $this->hasMany('App\Models\ConsultasMedicas\Consulta');
    }

    public function enlacefinanciero(){
        //Para modulo del Fondo Rotatorio
        return $this->hasOne('Modules\BancaDIF\Entities\AreaEnlace', 'empleado_id', 'id');
    }

    public function tarjeta(){
        //Para modulo del Fondo Rotatorio
        return $this->hasOne('Modules\BancaDIF\Entities\EmpleadoTarjeta', 'empleado_id', 'id');
    }
    
}
