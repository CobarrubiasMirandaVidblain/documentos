<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Remitente extends Model
{
    protected $table = 'atnc_remitentes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","primer_apellido","segundo_apellido","cargo_id","domicilio","email","telefono","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function nombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    public function cargo(){
    	return $this->belongsTo('App\Models\Cargo');
    }
}