<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmEntradasProductos extends Model
{
    use SoftDeletes;
    protected $table = 'recm_entradas_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['cantidad', 'entrada_id', 'presentacion_producto_id'];

    
}
