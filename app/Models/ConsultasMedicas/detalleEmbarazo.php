<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleEmbarazo extends Model{
  
  use SoftDeletes;

  protected $table = 'consulta_embarazo';
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id','trimestre_gestacional', 'primer_embarazo','acido_folico','anticonceptivo','terapia_hormonal','descanso_puerperio','riesgo_de','infeccion_urinaria','hemorragia','apoyo_traslado','resultado_analisis_clinicos'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro principal que detalla este registro
  */
  public function consultas(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }
}
