<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PiezasDentales extends Model{
  
  use SoftDeletes;

  protected $table = 'cat_piezas_dentales';
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre'];
  protected $hidden = array('updated_at', 'deleted_at');

  /**
   * Crea la consulta necesaria para obtener datos a partir de este registro
   *
   * @return ConsultasOdontologicas[Model] Colección de instancias de los registros de consultasodontologicas donde aparece este registro
   */
  public function consultasOdontologicas(){
    return $this->hasMany('App\Models\ConsultasMedicas\ConsultasOdontologicas');
  }
}
