<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiciosOdontologicos extends Model{

  use SoftDeletes;

  protected $table = 'cat_servicios_odontologicos';
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre','tipo'];
  protected $hidden = array('updated_at', 'deleted_at');

  /**
   * Crea la consulta necesaria para obtener datos a partir de este registro
   *
   * @return ConsultasOdontologicas[Model] Colección de instancias de los registros de consultasodontologicas donde aparece este registro
   */
  public function consultasOdontologicas(){
    return $this->hasMany('App\Models\ConsultasMedicas\ConsultasOdontologicas');
  }
}
