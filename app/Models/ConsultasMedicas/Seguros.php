<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seguros extends Model{
  
  use SoftDeletes;
  
  protected $table = 'cat_seguros';
  protected $dates = ['deleted_at'];
  protected $fillable = ['id','nombre'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  /**
   * Crea la consulta necesaria para obtener datos a partir de este registro
   *
   * @return Pacientes[Model] Instancia del registro en pacientes al que hace referencia este registro
   */
  public function paciente(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Pacientes');
  }
}
