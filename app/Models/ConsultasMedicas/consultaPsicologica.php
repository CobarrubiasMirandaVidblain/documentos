<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ConsultaPsicologica extends Model{
  
  use SoftDeletes;

  protected $table = 'consulta_psicologica';
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id','adiccion', 'entrevista','e_psicometrico','p_individual','p_grupo','p_pareja'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro en consultas al que hace referencia este registro
  */
  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }
}
