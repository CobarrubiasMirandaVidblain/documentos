<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleMenor extends Model{
  
  use SoftDeletes;
  
  protected $table = 'consulta_menor';
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id','ninio_sano', 'cancer','tipo_edi','res_edi','res_battelle'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro principal que detalla este registro
  */
  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }
}
