<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConsultaOdontologica extends Model{
  
  use SoftDeletes;
  
  protected $table = 'consulta_odontologica';
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id','servicio_id', 'pieza_dental_id','cantidad'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro en consultas al que hace referencia este registro
  */
  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return ServiciosOdontologicos[Model] Instancia del registro en cat_serviciosodontologicos relacionado a estos detalles
  */
  public function servicio(){
    return $this->belongsTo('App\Models\ConsultasMedicas\ServiciosOdontologicos');
  }
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return PiezasDentales[Model] Instancia del registro en cat_piezasdentales relacionado a estos detalles
  */
  public function dientes(){
    return $this->belongsTo('App\Models\ConsultasMedicas\PiezasDentales');
  }
  
}
