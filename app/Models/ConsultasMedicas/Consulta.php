<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consulta extends Model{

  protected $table = 'consultas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['paciente_id', 'empleado_id', 'horainicio', 'horafin', 'peso', 'talla', 'referido', 'contrareferido'];
  protected $hidden = array('updated_at', 'deleted_at');

  public function paciente(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Pacientes');
  }
  public function empleado(){
    return $this->belongsTo('App\Models\Empleado');
  }
  
  public function diagnosticos(){
    return $this->hasMany('App\Models\ConsultasMedicas\Diagnostico');
  }

  public function recetas(){
    return $this->hasMany('App\Models\ConsultasMedicas\Receta');
  }

  public function discapacidades(){
    return $this->hasMany('App\Models\ConsultasMedicas\DiscapacidadesConsulta');
  }
  
  public function hasDiscapacidad($discapacidad){
    foreach ($this->discapacidades() as $disc) {
      if($disc->discapacidad->id == $discapacidad)
        return true;
    }
    return false;
  }

}
