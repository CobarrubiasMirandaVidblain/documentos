<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiscapacidadesConsulta extends Model{
  
  use SoftDeletes;

  protected $table = 'discapacidades_consulta';
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id', 'discapacidad_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro en Consulta al que hace referencia este registro
  */
  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }

   /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Discapacidad[Model] Instancia del registro en Discapacidad al que hace referencia este registro
  */
  public function discapacidad(){
    return $this->belongsTo('App\Models\Discapacidad');
  }
}
