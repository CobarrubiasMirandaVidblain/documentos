<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discapacidad extends Model{

  protected $table = 'discapacidades_consulta';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id', 'discapacidad_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }

  public function discapacidad(){
    return $this->belongsTo('App\Models\Discapacidad');
  }
}
