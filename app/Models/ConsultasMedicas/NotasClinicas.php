<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotaClinica extends Model{
  
  protected $table = 'notasclinicas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['persona_programa_id', 'titulo', 'descripcion', 'usuario_id', 'curso_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function persona_programa(){
    return $this->belongsTo('App\Models\PersonasPrograma');
  }

  public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
  }

  public function curso(){
    return $this->belongsTo('App\Models\Braille\Curso');
  }
}
