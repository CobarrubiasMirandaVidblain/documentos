<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TiposEquipos extends Model
{
    protected $table = 'cat_tiposequipos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function equipos(){
	return $this->belongsTo('App\Models\Equipos', 'tipoequipo_id', 'id');
	}


}
