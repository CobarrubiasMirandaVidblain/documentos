<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nivel extends Model
{
    protected $table = 'cat_niveles';
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['nivel', 'usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


    public function usuario(){//quien dio de alta el nivel
    	return $this->belongsTo('App\Usuario');
    }

    public function empleados(){
    	return $this->hasMany('App\Empleado');
    }
}
