<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Licencias extends Model
{
    protected $table = 'licencias';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo","vigencia","numero_licencia", "fecha_expedicion", "oficina_expedidora", "entidad_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function entidad(){
    	return $this->belongsTo('App\Models\Entidad');
    }
}