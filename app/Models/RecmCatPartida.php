<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmCatPartida extends Model
{
    use SoftDeletes;
    protected $table = 'recm_cat_partidas';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['tipo','objetivo','capitulo','concep','partida_generica','partida_especifica_c','idp','capitulo_d','concep_d','partida_generica_d','partida_especifica','concepto'];

    // public function productos(){
    //     return $this->hasMany('App\Models\RecmPresentacionProducto','id','partida_id');
    // }

    public function productos (){
        return $this->belongsToMany('App\Models\RecmCatProducto', 'recm_presentacion_productos', 'partida_id', 'id')->withPivot('precio','presentacion_id');
    }
    
}


