<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmHistorialRequisicion extends Model
{
    use SoftDeletes;
    protected $table = 'recm_historial_requisicions';
    protected $dates = ['deleted_at'];
    protected $hidden = array('updated_at', 'deleted_at');

    protected $fillable = ['requisicion_id', 'status_id', 'usuario_id'];

    public function status (){
        return $this->belongsTo('App\Models\RecmCatEstatus');
    }

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario');
    }
}
