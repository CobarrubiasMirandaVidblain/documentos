<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vale extends Model
{
	protected $table = 'solicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["folio","fechaoficio","fecharecepcion","numoficio","asunto","observaciones","solicitante_id","tiposrecepcion_id","tiposremitente_id","titular_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function solicitante(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function titular(){
        return $this->belongsTo('App\Models\Titular');
    }

    public function tiposrecepcion(){
    	return $this->belongsTo('App\Models\Tiposrecepcion');
    }

    public function tiposremitente(){
    	return $this->belongsTo('App\Models\Tiposremitente');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

    public function dependenciassolicitudes(){
        return $this->hasMany('App\Models\DependenciasSolicitud');
    }

    public function programassolicitudes(){
        return $this->hasMany('App\Models\ProgramasSolicitud');
    }

    public function statussolicitudes(){
        return $this->hasMany('App\Models\StatusSolicitud');
    }

    public function documentossolicitudes(){
        return $this->hasMany('App\Models\DocumentosSolicitud');
    }

    public function solicitudespersonas(){
        return $this->hasMany('App\Models\SolicitudesPersona');
    }
    public function formato_fecha_oficio() {
        return (\DateTime::createFromFormat('Y-m-d', $this->fecha_oficio))->format('d/m/Y');
    }
}
