<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntradasproductosPersona extends Model {
    protected $table = "entradasproductos_personas";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["entradas_producto_id","persona_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function entradasproducto(){
        return $this->belongsTo('App\Models\EntradasProducto');
    }

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }    
}
