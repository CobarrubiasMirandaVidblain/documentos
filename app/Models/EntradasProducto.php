<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntradasProducto extends Model {
    protected $table = "entradas_productos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["fechaentrada","empleado_id","tiposentrada_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');  
    
    public function scopeSearch($query, $programa_padre) {
        // $query->join('cat_tiposentradas','cat_tiposentradas.id','entradas_productos.tiposentrada_id')
        // ->join('detallesentradas_productos','detallesentradas_productos.entradas_producto_id','entradas_productos.id')
        // ->join('areas_productos','areas_productos.id','detallesentradas_productos.areas_producto_id')
        // ->join('programas','programas.id','areas_productos.programa_id')
        // ->whereRaw('(select nombre from programas as programas_padres where programas_padres.id = programas.padre_id ) = "'. $programa_padre .'"');
        $query->join('cat_tiposentradas','cat_tiposentradas.id','entradas_productos.tiposentrada_id')
        ->join('detallesentradas_productos','detallesentradas_productos.entradas_producto_id','entradas_productos.id')
        ->join('beneficiosprogramas','beneficiosprogramas.id','detallesentradas_productos.beneficioprograma_id')
        ->join('anios_programas','anios_programas.id','beneficiosprogramas.anio_programa_id')
        ->join('programas','programas.id','anios_programas.programa_id')
        ->join('programas as padre','padre.id','programas.padre_id')
        ->join('areas_programas','areas_programas.programa_id','padre.id')
        ->join('cat_areas','cat_areas.id','areas_programas.area_id')
        ->whereIn('padre.nombre',$programa_padre);
    }

    public function empleado(){
    	return $this->belongsTo('App\Models\Empleado');
    }

    public function tiposentrada(){
    	return $this->belongsTo('App\Models\Tiposentrada');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function entradasproductosdependencia(){
        return $this->hasOne('App\Models\EntradasproductosDependencia');
    }

    public function entradasproductospersona(){
        return $this->hasOne('App\Models\EntradasproductosPersona');
    }

    public function get_formato_fecha() {
        return (\DateTime::createFromFormat('Y-m-d H:i:s', $this->fechaentrada))->format('d-m-Y');
    }

    public function detalleentradasproductos(){
    	return $this->hasMany('App\Models\DetalleEntradasproductos');
    }

    /**
        * Override parent boot and Call deleting event
        *
        * @return void
    */
    protected static function boot() {
        parent::boot();
        static::deleting(function($entrada) {
            if($entrada->entradasproductosdependencia) {
                $entrada->entradasproductosdependencia->delete();
            }
            if($entrada->entradasproductospersona) {
                $entrada->entradasproductospersona->delete();
            }
        });
    }
}