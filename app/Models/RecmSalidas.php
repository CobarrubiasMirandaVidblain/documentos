<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmSalidas extends Model
{
    use SoftDeletes;
    protected $table = 'recm_salidas';
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['fecha_hora', 'usuario_id', 'requisicion_id', 'recibe_id','folio','estatus_id'];

    // public function productos(){
    //     return $this->hasMany('App\Models\RecmProductosSalidas', 'salida_id', 'id');
    // }

    public function productos(){
        return $this->belongsToMany('App\Models\RecmPresentacionProducto', 'recm_productos_salidas', 'salida_id', 'presentacion_producto_id')->withPivot('cantidad');
    }

    public function recibe(){
        return $this->belongsTo('App\Models\Usuario', 'recibe_id', 'id');
    }
    public function estatus (){
        return $this->belongsTo('App\Models\RecmCatEstatus');
    }
    public function requisicion(){
        return $this->belongsTo('App\Models\RecmRequisicion','requisicion_id','id');
    }
}
