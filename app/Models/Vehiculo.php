<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehiculo extends Model {
  use SoftDeletes;

  protected $table = 'vehiculos';
  protected $dates = ['deleted_at'];
  protected $fillable=['marca_id','tipo_id','modelo','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function marcavehiculo() {
    $this->belongsTo('App\Models\MarcaVehiculo');
  }

  public function tipovehiculo() {
    $this->belongsTo('App\Models\TipoVehiculo');
  }
}