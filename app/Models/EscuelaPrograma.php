<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EscuelaPrograma extends Model
{
  protected $table = 'escuelas_programas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['escuela_id', 'programa_id', 'fecha_actividad'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function escuela(){
    return $this->belongsTo('App\Models\Escuela');
  }

  public function programa(){
    return $this->belongsTo('App\Models\Programa');
  }
}
