<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actividad extends Model
{
    use SoftDeletes;
    protected $table = 'cat_actividades';
    protected $dates = ['deleted_at'];
    protected $fillable=["actividad","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function usuario(){//quien dio de alta la actividad
    	return $this->belongsTo('App\Usuario');
	}

	public function bitacoras(){
        return $this->hasMany('App\Bitacora');
    }

}

