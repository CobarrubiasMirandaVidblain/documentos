<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class EstadosSolicitud extends Model{
  protected $table = "estados_solicitudes";
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable=["beneficioprograma_solicitud_id","statusproceso_id","motivo_programa_id","observacion","usuario_id"];
  protected $hidden = array('updated_at', 'deleted_at');

  public function beneficioprogramaSolicitud(){
    return $this->belongsTo('App\Models\BeneficioprogramasSolicitud');
  }

  public function statusproceso(){
    return $this->belongsTo('App\Models\Statusproceso');
  }

  public function motivoprograma(){
    return $this->belongsTo('App\Models\MotivosPrograma','motivo_programa_id','id');
  }

  public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
  }

  public function getCreatedAtAttribute($date){
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
  }

  public function scopeTimeLine($query,$bps_id,$padre_id) {
    $query->select(
      DB::raw(
        'status, abuelo.nombre as beneficio, ps.id as peticion_id, '.
        "UPPER(observacion) as observacion, ".
        "DATE_FORMAT(estados_solicitudes.created_at, '%d-%m-%Y %r') AS fecha, ".
        "pe.nombre as usuario "
      )
    )
    ->join('cat_statusprocesos as csp','csp.id','estados_solicitudes.statusproceso_id')
    ->join('beneficiosprogramas_solicitudes as ps','ps.id','estados_solicitudes.beneficioprograma_solicitud_id')
    ->join('beneficiosprogramas as bp','bp.id','ps.beneficioprograma_id')
    ->join('anios_programas as ap', 'ap.id','bp.anio_programa_id')
    ->join('programas as p','p.id','ap.programa_id')
    ->join('programas as padre','padre.id','p.padre_id')
    ->join('programas as abuelo','abuelo.id','padre.padre_id')
    ->join('usuarios as u','u.id','estados_solicitudes.usuario_id')
    ->join('personas as pe','pe.id','u.persona_id')
    ->where([
      ['ps.id',$bps_id],
      ['bp.id',$padre_id]
    ])->whereNull('ps.deleted_at');

    return $query;
  }

  public function scopeTimeLinePeticion($query,$peticion_id) {
    $query->select(
      DB::raw(
        "status,
        UPPER(observacion) as observacion,
        DATE_FORMAT(estados_solicitudes.created_at, '%d-%m-%Y %r') AS fecha,
        pe.nombre as usuario"
      )
    )
    ->join('cat_statusprocesos as csp','csp.id','estados_solicitudes.statusproceso_id')
    ->join('beneficiosprogramas_solicitudes as ps','ps.id','estados_solicitudes.beneficioprograma_solicitud_id')
    ->join('programas as p','p.id','ps.beneficioprograma_id')
    ->join('usuarios as u','u.id','estados_solicitudes.usuario_id')
    ->join('personas as pe','pe.id','u.persona_id')
    ->where('ps.id',$peticion_id);

    return $query;
  }

  public function scopeTimeLineSolicitud($query,$solicitud_id) {
    $query->select(
      DB::raw(
        "status,
        UPPER(observacion) as observacion,
        DATE_FORMAT(estados_solicitudes.created_at, '%d-%m-%Y %r') AS fecha,
        pe.nombre as usuario"
      )
    )
    ->join('cat_statusprocesos as csp','csp.id','estados_solicitudes.statusproceso_id')
    ->join('beneficiosprogramas_solicitudes as ps','ps.id','estados_solicitudes.beneficioprograma_solicitud_id')
    ->join('programas as p','p.id','ps.beneficioprograma_id')
    ->join('usuarios as u','u.id','estados_solicitudes.usuario_id')
    ->join('personas as pe','pe.id','u.persona_id')
    ->where('ps.solicitud_id',$solicitud_id);

    return $query;
  }
}
