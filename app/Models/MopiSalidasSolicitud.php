<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiSalidasSolicitud extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_salidas_solicitudes';
    protected $dates = ['deleted_at'];
    protected $hidden = array('updated_at', 'deleted_at');

    protected $fillable = ['folio','subtotal','total','nombre_recibe','primer_apellido_recibe','segundo_apellido_recibe','nombre_transporta','primer_apellido_transporta','segundo_apellido_transporta','created_at'];

    public function solicitud(){ 
        return $this->belongsTo('App\Models\MopiSolicitudes');
    }
    public function productos(){ 
        return $this->hasMany('App\Models\MopiSalidasProductos','salida_id');
    }
    public function usuario(){ 
        return $this->belongsTo('App\Models\Usuario');
    }
}
