<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuentasBancos extends Model
{
	use SoftDeletes;

    protected $table = 'cuentasbancos';

    protected $dates = ['deleted_at'];

    protected $fillable = ['persona_id', 'banco_id', 'num_cuenta', 'num_tarjeta', 'activo', 'usuario_id'];

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function persona() {
		return $this->belongsTo('App\Models\Persona');
	}

	public function banco() {
		return $this->belongsTo('App\Models\Banco');
	}
}