<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentosPrograma extends Model
{
    use SoftDeletes;

    protected $table = 'documentos_programas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['documento_id', 'anios_programa_id', 'activo', 'usuario_id'];

    public function documento() {
    	return $this->belongsTo('App\Models\Documento');
    }

    public function programa() {
    	return $this->belongsTo('App\Models\Programa');
    }

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function aniosprogramas() {
        return $this->belongsTo('App\Models\AniosPrograma');
    }

    public function documentospersonas() {
        return $this->hasMany('App\Models\DocumentosPersona');
    }
}