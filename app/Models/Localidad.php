<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Localidad extends Model
{
    use SoftDeletes;

    protected $table = 'cat_localidades';
    
    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $fillable = array('nombre', 'municipio_id', 'latitud', 'longitud');
    
    public function personas()
    {
    	return $this->hasMany('App\Models\Persona');
    }

    public function municipio()
    {
        return $this->belongsTo('App\Models\Municipio');
    }

    public function evento() // para el modulo orgEventos
    {
        return $this->hasMany('App\Models\EventosTaller');
    }
}