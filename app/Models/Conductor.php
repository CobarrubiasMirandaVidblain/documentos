<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conductor extends Model
{
  use SoftDeletes;

/* protected $connection = 'mysql_server_real'; */
  
  protected $table = 'conductores';
  protected $dates = ['deleted_at'];
  protected $fillable=["id", "empleado_id", "licencia_id", "donador_organos", "restricciones", "padecimientos_medicos", "clave_conductor"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function empleado() {
    return $this->belongsTo('App\Models\Empleado');
  }
  
  public function licencia() {
    return $this->belongsTo('App\Models\Licencias');
  }
}
