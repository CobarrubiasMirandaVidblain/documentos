<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmOrdenCompraProductos extends Model
{
    use SoftDeletes;
    protected $table = 'recm_orden_compra_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['subtotal','precio_unitario_real'];
    
    protected $with = ['productos'];

    public function ordencompra()
    {
        return $this->belongsTo('App\Models\RecmOrdeneCompra');
    }
    public function productos()
    {
        return $this->belongsTo('App\Models\RecmPresentacionProducto','presentacion_producto_id');
    }

}
