<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cita extends Model
{
  protected $table = 'citas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['persona_id', 'empleado_id', 'fecha', 'hora_inicio', 'hora_fin', 'color', 'observaciones', 'programa_id', 'asistencia'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function persona(){
    return $this->belongsTo('App\Models\Persona');
  }

  public function empleado(){
    return $this->belongsTo('App\Models\Empleado');
  }

  public function programa(){
    return $this->belongsTo('App\Models\Programa');
  }

}
