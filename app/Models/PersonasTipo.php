<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonasTipo extends Model
{
    protected $table = 'personas_tipos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipospersona_id","persona_id","tablabd_id", 'modulo_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }

    public function tipospersona(){
        return $this->belongsTo('App\Models\Tipospersona');
    }

    public function tabla()
    {
      return $this->belongsTo('app\Models\Tabla');
    }

    public function modulo(){
      return $this->belongsTo('app\Models\Modulo');
    }

    public function credImpresion()
    {
         return $this->hasMany('App\Models\CredImpresion','personas_tipos_id','id');
    }
}
