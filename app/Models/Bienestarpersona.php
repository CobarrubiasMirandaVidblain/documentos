<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BienestarPersona extends Model
{
    use SoftDeletes;

    protected $table = 'bienestarpersonas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['prefolio', 'foliounico', 'posicion', 'discapacidad_id', 'persona_id', 'usuario_id', 'ejercicio_id', 'estadocivil_id', 'ocupacion_id', 'escolaridad_id'];

    public function scopeSearch($query, $ejercicio_id) {
        $query
        ->join('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')
        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->leftjoin('personas_programas', 'personas.id', '=', 'personas_programas.persona_id')
        ->whereNull('personas_programas.persona_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        ->join('tutores', 'tutores.tutorado_id', '=', 'bienestarpersonas.persona_id')
        ->where('tutores.programa_id', '=', 1)
        ->whereNull('tutores.deleted_at')

        ->leftjoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutores.persona_id')
        ->whereNull('cuentasbancos.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')
        ->where('bienestarpersonas.ejercicio_id', '=', $ejercicio_id)
        //->whereNotNull('cuentasbancos.persona_id')
        ;
    }

    public function scopeSolicitantes($query) {
        $query
        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->leftJoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')

        ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftJoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftJoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftJoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        ->leftJoin('personas_programas', 'personas_programas.persona_id', '=', 'personas.id')
        //->where('personas_programas.anios_programa_id', '=', 2)
        ->whereNull('personas_programas.persona_id')
        ->whereNull('personas_programas.deleted_at')

        ->leftJoin('tutores', 'tutores.tutorado_id', '=', 'personas.id')
        ->where('tutores.programa_id', '=', 1)
        ->whereNull('tutores.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')

        ->leftJoin('cat_localidades as tutor_cat_localidades', 'tutor_cat_localidades.id', '=', 'tutor.localidad_id')
        ->leftJoin('cat_municipios as tutor_cat_municipios', 'tutor_cat_municipios.id', '=', 'tutor.municipio_id')
        ->leftJoin('cat_distritos as tutor_cat_distritos', 'tutor_cat_distritos.id', '=', 'tutor_cat_municipios.distrito_id')
        ->leftJoin('cat_regiones as tutor_cat_regiones', 'tutor_cat_regiones.id', '=', 'tutor_cat_distritos.region_id')

        ->leftJoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutor.id')
        ->whereNull('cuentasbancos.deleted_at')
        ;
    }

    public function scopeBeneficiarios($query) {
        $query
        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->leftJoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')

        ->leftJoin('cat_discapacidades as discapacidades', 'discapacidades.id', '=', 'cat_discapacidades.padre_id')

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')

        ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftJoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftJoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftJoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        ->join('personas_programas', 'personas_programas.persona_id', '=', 'personas.id')
        ->where('personas_programas.anios_programa_id', '=', 2)

        ->leftJoin('tutores', 'tutores.tutorado_id', '=', 'personas.id')
        ->where('tutores.programa_id', '=', 1)
        ->whereNull('tutores.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')

        ->leftJoin('cat_localidades as tutor_cat_localidades', 'tutor_cat_localidades.id', '=', 'tutor.localidad_id')
        ->leftJoin('cat_municipios as tutor_cat_municipios', 'tutor_cat_municipios.id', '=', 'tutor.municipio_id')
        ->leftJoin('cat_distritos as tutor_cat_distritos', 'tutor_cat_distritos.id', '=', 'tutor_cat_municipios.distrito_id')
        ->leftJoin('cat_regiones as tutor_cat_regiones', 'tutor_cat_regiones.id', '=', 'tutor_cat_distritos.region_id')

        ->leftJoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutor.id')
        ->whereNull('cuentasbancos.deleted_at')
        ;
    }

    public function scopeBancos($query, $ejercicio_id) {
        /*$query
        ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')
        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        //->leftjoin('personas_programas', 'personas.id', '=', 'personas_programas.persona_id')
        //->whereNull('personas_programas.persona_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        ->join('tutores', 'tutores.tutorado_id', '=', 'bienestarpersonas.persona_id')
        ->whereNull('tutores.deleted_at')

        ->leftjoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutores.persona_id')
        ->whereNull('cuentasbancos.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')
        ->where('bienestarpersonas.ejercicio_id', '=', $ejercicio_id)
        //->whereNotNull('cuentasbancos.persona_id')
        ;*/
        $query
        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->leftJoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')

        ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftJoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftJoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftJoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        //->leftJoin('personas_programas', 'personas_programas.persona_id', '=', 'personas.id')
        //->where('personas_programas.anios_programa_id', '=', 2)
        //->whereNull('personas_programas.persona_id')
        //->whereNull('personas_programas.deleted_at')

        ->leftJoin('tutores', 'tutores.tutorado_id', '=', 'personas.id')
        ->where('tutores.programa_id', '=', 1)
        ->whereNull('tutores.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')

        ->leftJoin('cat_localidades as tutor_cat_localidades', 'tutor_cat_localidades.id', '=', 'tutor.localidad_id')
        ->leftJoin('cat_municipios as tutor_cat_municipios', 'tutor_cat_municipios.id', '=', 'tutor.municipio_id')
        ->leftJoin('cat_distritos as tutor_cat_distritos', 'tutor_cat_distritos.id', '=', 'tutor_cat_municipios.distrito_id')
        ->leftJoin('cat_regiones as tutor_cat_regiones', 'tutor_cat_regiones.id', '=', 'tutor_cat_distritos.region_id')

        ->leftJoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutor.id')
        ->whereNull('cuentasbancos.deleted_at')
        ;
    }

    public function scopeFiltrarPosicionesPagadas($query, $anio, $bimestre) {
        $query
        ->leftjoin('bienestardispersiones', 'bienestardispersiones.bienestarpersonas_id', '=', 'bienestarpersonas.id')

        ->leftjoin('listas_dispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->leftjoin('cat_ejercicios', 'listas_dispersiones.ejercicio_id', '=', 'cat_ejercicios.id')
        
        ->where(
            [
                [
                    'cat_ejercicios.anio', $anio
                ],
                [
                    'listas_dispersiones.bimestre', $bimestre
                ],
                [
                    'bienestardispersiones.pagado', 1
                ]
            ]
        )
        ;
    }

    public function scopeFiltrarPersonasBimestre($query, $anio, $bimestre) {
        $query
        ->leftjoin('bienestardispersiones', 'bienestardispersiones.bienestarpersonas_id', '=', 'bienestarpersonas.id')

        ->leftjoin('listas_dispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->leftjoin('cat_ejercicios', 'listas_dispersiones.ejercicio_id', '=', 'cat_ejercicios.id')
        
        ->where(
            [
                [
                    'cat_ejercicios.anio', $anio
                ],
                [
                    'listas_dispersiones.bimestre', $bimestre
                ]
            ]
        )
        ;
    }

    public function scopeGenerarListadoDispersion($query, $dispersion, $anio, $bimestre, $personasBimestre, $posicionesPagadas) {
        $query
        ->leftjoin('bienestardispersiones', 'bienestardispersiones.bienestarpersonas_id', '=', 'bienestarpersonas.id')

        ->leftjoin('listas_dispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->leftjoin('cat_ejercicios', 'bienestarpersonas.ejercicio_id', '=', 'cat_ejercicios.id')

        ->where('cat_ejercicios.anio', $anio)

        ->where(function($query) use ($bimestre, $dispersion, $personasBimestre) {
            $query
            ->whereNull('lista_id')
            ->orwhere('lista_id', $dispersion)
            ->orwhere(function($query) use ($bimestre, $dispersion, $personasBimestre) {
                $query->where('bimestre', '!=', $bimestre)->whereNotIn('bienestarpersonas.id', $personasBimestre);
            })
            ->orwhere(function($query) use ($bimestre, $dispersion) {
                $query
                ->where('pagado', 2)
                ->where('listas_dispersiones.status', 'FINALIZADA');
            })
            ;
        })
        ->whereNotIn('bienestarpersonas.posicion', $posicionesPagadas)
        ->distinct('bienestarpersonas.id');
    }

    public function personasprogramas() {
        return $this->belongsTo('App\Models\PersonasPrograma', 'persona_id', 'persona_id');
        //return $this->hasMany('App\Models\PersonasPrograma', 'persona_id', 'persona_id');
    }

    public function usuariocreate() {
        return $this->belongsTo('App\Models\Usuario');
    }

    public function persona() {
        return $this->belongsTo('App\Models\Persona');
    }

    public function discapacidad() {
        return $this->belongsTo('App\Models\Discapacidad');
    }

    public function estadocivil() {
        return $this->belongsTo('App\Models\EstadoCivil');
    }

    public function escolaridad() {
        return $this->belongsTo('App\Models\Escolaridad');
    }

    public function ocupacion() {
        return $this->belongsTo('App\Models\Ocupacion','ocupacion_id');
    }

    public function miocupacion() {
        return $this->belongsTo('App\Models\Ocupacion','ocupacion_id');
    }

    public function statusbienestarpersona() {
        return $this->hasMany('App\Models\StatusBienestarPersona');
    }

    public function ejercicio() {
        return $this->belongsTo('App\Models\Ejercicio');
    }
}