<?php

namespace App\Models\Fotradis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatTipoPasajero extends Model
{
  protected $table = 'dtll_cat_tipospasajeros';
  use softDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ["id","nombre"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function pasajero ()
  {
    return $this->hasMany('App\Models\Fotradis\Pasajero','id','tipopasajero_id');
  }
}
