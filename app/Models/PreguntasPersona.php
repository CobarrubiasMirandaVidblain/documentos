<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreguntasPersona extends Model
{
  protected $table = 'preguntas_personas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['pregunta_id', 'persona_id', 'empleado_id', 'fecha_encuesta'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function pregunta(){
      return $this->belongsTo('App\Models\Pregunta');
  }

  public function persona(){
    return $this->belongsTo('App\Models\Persona');
  }

  public function empleado(){
    return $this->belongsTo('App\Models\Empleado');
  }

}
