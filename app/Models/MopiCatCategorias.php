<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiCatCategorias extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_cat_categorias';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['categoria','presupuesto','descripcion','compremetido','disponible'];

    public function programa(){ // programa al cual pertenece
        return $this->belongsTo('App\Models\MopiCatProgamas');
    }
}
