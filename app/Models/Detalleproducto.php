<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Detalleproducto extends Model
{
    protected $table = 'detalleproductos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["producto_id","descripcion","modelo","marca","serie"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');    

    public function producto(){
        return $this->belongsTo('App\Models\Producto');
    }
}
