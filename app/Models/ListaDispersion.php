<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListaDispersion extends Model
{
    use SoftDeletes;

    protected $table = 'listas_dispersiones';

    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre_listado', 'bimestre', 'importe', 'fecha', 'status', 'usuario_id', 'ejercicio_id'];

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario');
    }

    public function ejercicio() {
        return $this->belongsTo('App\Models\Ejercicio');
    }

    public function scopeSearch($query, $search) {
        $query->join('usuarios', 'usuarios.id', '=', 'listas_dispersiones.usuario_id')
        ->where('usuarios.usuario', 'like', '%' . $search . '%')
        ->orwhere('nombre_listado', 'like', '%' . $search . '%')
        ->orwhere('fecha', 'like', '%' . $search . '%')
        ->orwhere('bimestre', $search)
        ->orwhere('importe', $search);
    }
}