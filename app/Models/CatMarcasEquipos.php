<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CatMarcasEquipos extends Model
{
    protected $table = 'cat_marcasequipos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function tiposequipos(){
	return $this->belongsTo('App\Models\TiposEquipos');
}

public function dispositivos(){
	return $this->hasMany('App\Models\Dispositivos');
}

}
