<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    protected $table = 'cat_productos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["producto","vigencia","unidadmedida_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query, $programa_padre) {
        $query->join('cat_unidadmedidas','cat_unidadmedidas.id','cat_productos.unidadmedida_id')
        ->join('areas_productos','areas_productos.producto_id','cat_productos.id')
        ->join('programas','programas.id','areas_productos.programa_id')
        ->whereRaw('(select nombre from programas as programas_padres where programas_padres.id = programas.padre_id ) = "'. $programa_padre .'"');
    }

    public function usuario(){//quien dio de alta el producto
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function unidadmedida(){
    	return $this->belongsTo('App\Models\Unidadmedida');
    }

    public function areasproductos(){
        return $this->hasMany('App\Models\AreasProducto');
    }

    public function detalleproductos(){
        return $this->hasMany('App\Models\Detalleproducto');
    }

    /**
        * Override parent boot and Call deleting event
        *
        * @return void
    */
    protected static function boot() {
        parent::boot();
        static::deleting(function($producto) {
            foreach ($producto->areasproductos()->get() as $area_producto) {
                $area_producto->delete();
            }
        });
    }
}
