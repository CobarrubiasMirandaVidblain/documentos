<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoporteServicios extends Model
{
	protected $table = 'soporteservicios';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["numero_servicio","nombre","tiposervicio_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
public function tipossoporteservicios(){
		return $this->belongsTo('App\Models\TiposSoporteServicios', 'tiposervicio_id', 'id');
	}

public function soportesolicitudes(){
	return $this->belongsTo('App\Models\SoporteSolicitudes');
	}
}
