<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlumnosCanna extends Model
{
  protected $table = 'alumnoscanna';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['persona_id', 'folio', 'gruponivel_id', 'diagnostico', 'medico', 'medicamento', 'observaciones'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function persona(){
    return $this->belongsTo('App\Models\Persona');
  }

  public function gruponivel(){
    return $this->belongsTo('App\Models\vale\ValeGrupoNiveles');
  }
}
