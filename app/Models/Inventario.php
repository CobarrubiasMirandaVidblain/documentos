<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Inventario extends Model
{
	protected $table = 'inventario';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["equipo_id","dispositivo_id","resguardo_id","oficial"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
public function dispositivos(){
	return $this->hasMany('App\Models\Dispositivos');
}

public function tiposequipos(){
	return $this->hasMany('App\Models\TiposEquipos');
}

public function soportesolicitudesequipos(){
	return $this->hasMany('App\Models\SoporteSolicitudesEquipos');
}

public function soportesolicitudesprestamos(){
	return $this->hasMany('App\Models\SoporteSolicitudesPrestamos');
}
}
