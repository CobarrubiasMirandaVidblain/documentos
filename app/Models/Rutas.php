<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rutas extends Model
{
    protected $table = 'rutas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","polyline","color"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function empleado() {
        return $this->belongsTo('App\Models\Empleado');
    }
}