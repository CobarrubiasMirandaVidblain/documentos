<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DocumentosPersona extends Model{
use SoftDeletes;
    protected $table = 'documentos_personas';
    protected $dates = ['deleted_at'];
    protected $fillable=["persona_id","documentos_programa_id","presento","fechapresento","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $with = ["documentossolicitudes"];

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function documentosprograma(){
    	return $this->belongsTo('App\Models\DocumentosPrograma');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function documentossolicitudes(){
        return $this->hasMany('App\Models\DocumentosSolicitud');
    }
}
