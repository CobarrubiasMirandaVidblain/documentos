<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{
	protected $connection = 'otra';

	protected $table = 'tutores';

	public function persona() {
		return $this->belongsTo('App\Models\Bienestar\Persona', 'tutorado_id', 'id');
	}
}