<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class BienestarPersona extends Model {
	protected $connection = 'otra';

	protected $table = 'bienestarpersonas';

	public function persona() {
		return $this->belongsTo('App\Models\Bienestar\Persona');
	}
}