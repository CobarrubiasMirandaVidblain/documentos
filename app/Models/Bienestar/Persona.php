<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model {
	//protected $connection = 'otra';

	protected $table = 'personas';

	protected $fillable = ['nuevo_id'];

	public $timestamps = false;

	public function scopeSearch($query) {
		$query
		->leftjoin('localidades', 'localidades.id', '=', 'personas.localidad_id')
		->join('municipios', 'municipios.id', '=', 'personas.municipio_id')
		->join('distritos', 'distritos.id', '=', 'municipios.distrito_id')
		->join('regiones', 'regiones.id', '=', 'distritos.region_id');
	}

	public function localidad() {
		return $this->belongsTo('App\Models\Bienestar\Localidad');
	}

	public function municipio() {
		return $this->belongsTo('App\Models\Bienestar\Municipio');
	}

	public function get_url_fotografia() {
		if(isset($this->fotografia)) {
			return $this->fotografia;
		}
		return 'images/no-image.png';
	}

	public function get_nombre_completo() {
		return $this->nombre . ' ' . $this->apellido_paterno . ' ' . $this->apellido_materno;
	}

	public function get_formato_fecha_nacimiento() {
		return (\DateTime::createFromFormat('Y-m-d', $this->fecha_nacimiento))->format('d/m/Y');
	}
}