<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
	protected $connection = 'otra';

	protected $table = 'localidades';

	public function municipio()
	{
		return $this->belongsTo('App\Models\Bienestar\Municipio');
	}
}