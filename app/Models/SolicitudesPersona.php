<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class SolicitudesPersona extends Model {
    protected $table = 'solicitudes_personas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["persona_id","programas_solicitud_id","entregado","observacionentrega","evaluado","observacionevaluado","salidas_producto_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    //Por el momento solo para AFuncionales
    public function scopeBeneficiarios($query) {
        $query->join('programas_solicitudes', 'programas_solicitudes.id', '=', 'solicitudes_personas.programas_solicitud_id')
              ->join('programas', 'programas.id', '=', 'programas_solicitudes.programa_id')
              ->join('solicitudes', 'solicitudes.id', '=', 'programas_solicitudes.solicitud_id')
              ->join('status_solicitudes', 'status_solicitudes.programas_solicitud_id', '=', 'programas_solicitudes.id')
              ->join('cat_statusprocesos','cat_statusprocesos.id', '=', 'status_solicitudes.statusproceso_id')
              ->join('personas', 'personas.id', '=', 'solicitudes_personas.persona_id')
              ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
              ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
              ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
              ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
              ->leftjoin('afuncionalespersonas', 'afuncionalespersonas.persona_id', '=', 'solicitudes_personas.persona_id')
              ->leftjoin('afuncionales_sillas','afuncionales_sillas.afuncionalespersona_id','=','afuncionalespersonas.id')
              ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'afuncionalespersonas.discapacidad_id')
              ->leftjoin('cat_limitaciones', 'cat_limitaciones.id', '=', 'afuncionalespersonas.limitacion_id')
              ->leftjoin('salidas_productos','salidas_productos.id','=','solicitudes_personas.salidas_producto_id')
              ->leftjoin('detallessalidas_productos','detallessalidas_productos.salidas_producto_id','=','salidas_productos.id')
              ->leftjoin('productosfoliados','productosfoliados.detallessalidas_producto_id','=','detallessalidas_productos.id')
              ->leftjoin('areas_productos','areas_productos.id','=','detallessalidas_productos.areas_producto_id')
              ->leftjoin('cat_productos','cat_productos.id','=','areas_productos.producto_id')
              ->whereRaw('(select nombre from programas as programas_padres where programas.padre_id = programas_padres.id ) = "APOYOS FUNCIONALES"')
              ->whereRaw('status_solicitudes.created_at = ( SELECT MAX(ss.created_at) FROM status_solicitudes ss WHERE ss.programas_solicitud_id = solicitudes_personas.programas_solicitud_id AND ss.deleted_at is null)')
              ->select(
            [           
                'solicitudes.folio as folio_solicitud',     
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'cat_discapacidades.nombre as discapacidad',
                'solicitudes.fecharecepcion as fecha_solicitud',
                'salidas_productos.fechasalida as fecha_entrega',
                'cat_productos.producto as apoyo',
                'productosfoliados.folio as folio_apoyo',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',
                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',
                'programas.nombre as programa',
                'afuncionalespersonas.tiempodiscapacidad as tiempo',
                'cat_limitaciones.nombre as limitacion',
                'afuncionales_sillas.altura as altura',
                'afuncionales_sillas.peso as peso',
                'afuncionales_sillas.terreno as terreno',
                'afuncionales_sillas.tiposilla as tiposilla',
                DB::raw('IFNULL(solicitudes_personas.observacionevaluado, IFNULL(solicitudes_personas.observacionentrega, status_solicitudes.observacion)) as motivo') //Para los cancelados
            ]

        );
    }

    //Esta función la utiliza el datatable de Beneficiarios y lo ocupan todos los módulos
    public function scopeSolicitantes($query, $programas_solicitud_id) {
        $query->join('personas', 'personas.id', '=', 'solicitudes_personas.persona_id')
              ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
              ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
              ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
              ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
              ->join('programas_solicitudes', 'programas_solicitudes.id', '=', 'solicitudes_personas.programas_solicitud_id')
              ->join('solicitudes', 'solicitudes.id', '=', 'programas_solicitudes.solicitud_id')
              ->join('programas', 'programas.id', '=', 'programas_solicitudes.programa_id');
              if($programas_solicitud_id){
                $query->where('solicitudes_personas.programas_solicitud_id',$programas_solicitud_id);
              }
    }

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function programas_solicitud(){
    	return $this->belongsTo('App\Models\ProgramasSolicitud');
    }

    public function salida_producto(){
        return $this->belongsTo('App\Models\SalidasProducto','salidas_producto_id');
    }

    public function get_fecha_updated() {
        return (\DateTime::createFromFormat('Y-m-d H:i:s', $this->updated_at))->format('d-m-Y');
    }
}