<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Area extends Model
{
    protected $table = 'cat_areas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","tipoarea_id","padre_id","usuario_id","siglas"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){//quien dio de alta el rol
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function tipoarea(){
    	return $this->belongsTo('App\Models\Tipoarea');
	}

	public function padre(){
    	return $this->belongsTo('App\Models\Area','id','padre_id');
	}

	public function areas(){
		return $this->hasMany('App\Models\Area','padre_id','id');
    }
    
    public function hijas()
    {
        return $this->areas()->with('hijas');
    }

    public function areasproductos(){
        return $this->hasMany('App\Models\AreasProducto');
    }

    public function areasresponsables(){
        return $this->hasOne('App\Models\AreasResponsable');
    }

    public function listadoareas(){
        return $this->belongsToMany('App\Models\Empleado', 'areas_responsables', 'area_id', 'empleado_id');
    }

    public function areasfondo(){
        return $this->hasOne('Modules\BancaDIF\Entities\FondoRotatorioArea', 'area_id', 'id');
    }

    public function programas(){
        return $this->hasMany('App\Models\AreasPrograma');
    }
}
