<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class StatusSoporteSolicitudesEquipos extends Model
{
    protected $table = 'status_soportesolicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["status_id","solicitud_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function soportesolicitudesequipos(){
		return $this->hasOne('App\Models\SoporteSolicitudesEquipos');
	}

}
