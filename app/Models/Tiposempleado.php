<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tiposempleado extends Model
{
    protected $table = 'cat_tiposempleados';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function empleados(){
    	return $this->hasMany('App\Empleado');
    }

}
