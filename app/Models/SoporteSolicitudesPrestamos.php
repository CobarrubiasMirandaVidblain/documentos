<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoporteSolicitudesPrestamos extends Model
{
	protected $table = 'soportesolicitudes_prestamos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","inventario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
 
public function soportesolicitudes(){
	return $this->hasMany('App\Models\SoporteSolicitudes');
		}  
public function inventario(){
	return $this->belongsTo('App\Models\Inventario');
		}
}
