<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productosfoliados extends Model {
    protected $table = "productosfoliados";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["detallesentradas_producto_id","folio","detallessalidas_producto_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function detalleentradasproductos(){
    	return $this->belongsTo('App\Models\DetalleEntradasproductos','detallesentradas_producto_id','id');
    }

    public function detallesalidasproductos(){
    	return $this->belongsTo('App\Models\DetalleSalidasproductos');
    }
}
