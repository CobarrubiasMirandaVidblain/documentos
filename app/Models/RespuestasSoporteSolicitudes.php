<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class RespuestasSoporteSolicitudes extends Model
{
    protected $table = 'respuestassoportesolicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","respuesta"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function soportesolicitudesequipos(){
	return $this->belongsTo('App\Models\SoporteSolicitudesEquipos', 'solicitud_id', 'id');
}

public function soportedictamenes(){
	return $this->hasOne('App\Models\SoporteDictamenes');
}

}
