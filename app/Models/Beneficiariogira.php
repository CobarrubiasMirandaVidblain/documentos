<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Beneficiariogira extends Model
{
    use SoftDeletes;

    protected $table = 'giraspreregis';

    protected $dates = ['deleted_at'];

    protected $fillable = ['evento_id','nombre', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'curp', 'telefono', 'calle', 'numero_exterior', 'numero_interior', 'colonia', 'localidad_id', 'municipio_id', 'folio', 'fecha_registro', 'fotografia', 'usuario_id'];
}
