<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Dispositivos extends Model
{
    protected $table = 'dispositivos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["marca_id","modelo_id","numero_serie","tipo","nombre","capacidad","observaciones"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function catmarcasequipos(){
	return $this->belongsTo('App\Models\CatMarcasEquipos', 'marca_id', 'id'); 
}

public function catmodelosequipos(){
	return $this->belongsTo('App\Models\CatModelosEquipos', 'modelo_id', 'id');
}

public function inventario(){
	return $this->belongsTo('App\Models\Inventario');
}

}
