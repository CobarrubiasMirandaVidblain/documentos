<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bitacora extends Model
{
    protected $table = 'bitacora';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["usuario_id","actividad_id","modulo_id","tabla","registro","ip","so","usuario_red","campos", "comentario", "metodo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){
    	return $this->belongsTo('App\Usuario');
	}

	public function actividad(){
		return $this->belongsTo('App\Actividad');
	}

	public function modulo(){
		return $this->belongsTo('App\Modulo');
	}
}
