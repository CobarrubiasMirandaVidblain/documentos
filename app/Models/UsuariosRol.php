<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsuariosRol extends Model
{
    protected $table = 'usuarios_roles';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=['usuario_id', 'rol_id', 'modulo_id', 'responsable', 'autorizado'];
    protected $hidden = array('created_at', 'updated_at');

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function rol(){
		return $this->belongsTo('App\Models\Rol');
	}

	public function modulo(){
		return $this->belongsTo('App\Models\Modulo');
	}

	public function responsable(){
    	return $this->belongsTo('App\Models\Usuario','id','responsable');
	}

	public function getTable(){
		return $this->table;
	}
}
