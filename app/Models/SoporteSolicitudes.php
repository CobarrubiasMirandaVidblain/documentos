<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoporteSolicitudes extends Model
{
	protected $table = 'soportesolicitudes';
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $fillable = ["numero_solicitud","solicitante_id","servicio_id","equipo_id","fecha_inicio","fecha_fin","asunto"];
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
public function empleado(){
		return $this->hasOne('App\Models\Empleado');
	}

public function tipossoporteservicios(){
	    return $this->belongsTo('App\Models\TiposSoporteServicios', 'servicio_id', 'id');
	}

public function catequipos(){
	    return $this->belongsTo('App\Models\Equipos', 'equipo_id', 'id');
	}

public function soportecalificaciones(){
		return $this->hasOne('App\Models\SoporteCalificaciones');
	}

public function soportesolicitudesequipos(){
		return $this->hasOne('App\Models\SoporteSolicitudesEquipos', 'solicitud_id');
	}

public function soportesolicitudesprestamos(){
		return $this->hasMany('App\Models\SoporteSolicitudesPrestamos');
	}
}
