<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiSalidasProductos extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_salidas_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['precio_unitario','subtotal','cantidad'];

    protected $with = ['detalle'];

    public function detalle(){ 
        return $this->belongsTo('App\Models\MopiPresentacionProducto','presentacion_producto_id');
    }
}
