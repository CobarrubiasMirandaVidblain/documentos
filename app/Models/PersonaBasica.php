<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonaBasica extends Model
{
  use SoftDeletes;

  protected $table = 'personasbasicas';
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre', 'primer_apellido', 'segundo_apellido', 'genero', 'fecha_nacimiento', 'numero_celular', 'numero_local', 'email'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function get_nombre_completo() {
    return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
  }

  public function get_edad(){
    return with(\Carbon\Carbon::parse($this->fecha_nacimiento))->age;
  }
}
