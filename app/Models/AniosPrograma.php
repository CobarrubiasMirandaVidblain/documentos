<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AniosPrograma extends Model
{
    use SoftDeletes;

    protected $table = 'anios_programas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['ejercicio_id', 'programa_id'];

    protected $with = ['ejercicio'];

    public function ejercicio() {
    	return $this->belongsTo('App\Models\Ejercicio');
    }

    public function programa() {
    	return $this->belongsTo('App\Models\Programa');
    }

    public function personasprogramas() {
        return $this->hasMany('App\Models\PersonasPrograma');
    }

    public function tutores() {
        return $this->hasMany('App\Models\Tutor');
    }

    public function documentosprogramas(){
        return $this->hasMany('App\Models\DocumentosPrograma');
    }

    public function beneficioprograma(){
        return $this->belongsTo('App\Models\Beneficiosprograma');
    }
}
