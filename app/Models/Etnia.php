<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Etnia extends Model{
  
  use SoftDeletes;
  
  protected $table = 'cat_etnias';
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
}
