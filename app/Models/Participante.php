<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participante extends Model
{
     use SoftDeletes;

    protected $table = 'participante';

    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre','primerApellido','segundoApellido','genero','fecha_nacimiento','telefono'];

    public function evento(){
    	return $this->belongsToMany('App\Models\EventosTaller','participante_evento','tipo_evento_id','participante_id');
    }
}