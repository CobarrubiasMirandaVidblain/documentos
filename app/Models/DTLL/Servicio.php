<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicio extends Model
{
    use SoftDeletes;

    // protected $connection = 'mysql_server_real';

    protected $table = 'dtll_servicios';
    protected $dates = ['deleted_at'];
    protected $fillable = ["controlruta_id", "pasajero_id", "subida", "latitud", "longitud", "usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function pasajeros()
    {
        return $this->hasMany('App\Models\DTLL\Pasajero');
    }
    public function controlruta()
    {
        return $this->belongsTo('App\Models\ControlRutas', 'controlruta_id');
    }
    public function bajaservicio()
    {
        return $this->hasOne('App\Models\DTLL\BajaServicio', 'servicio_id');
    }
}