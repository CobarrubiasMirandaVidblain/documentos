<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistorialPago extends Model{
  use SoftDeletes;

  protected $table = 'dtll_historialpagos';
  protected $dates = ['deleted_at'];
  protected $fillable=["pasajero_id","fechaInicio","fechaFin","folioPago","usuario_id"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function pasajero(){
    return $this->belongsTo('App\Models\DTLL\Pasajero');
	}
  
	public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
  }
  
}