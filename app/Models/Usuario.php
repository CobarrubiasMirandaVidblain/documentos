<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class Usuario extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuarios';
    use SoftDeletes;
    //protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable=['email', 'password', 'token', 'vida_token', 'usuario_id', 'usuario', 'remember_token', 'persona_id',  'autorizado', 'ficha'];
    protected $hidden = ['password', 'token', 'remember_token', 'vida_token'];

    protected $dateFormat = 'Y-m-d H:i:s';

    public function desayunousuario() {
        return $this->hasOne('Modules\DesayunosEscolaresRegistro\Entities\DesayunosUsuarios');
    }

    public function regionusuario() {
        return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\RegionesUsuarios','id','access_usuario_id');
    }

    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }

    public function agregado(){
        return $this->belongsTo('App\Models\Usuario');
    }

    public function bitacoras(){
        return $this->hasMany('App\Models\Bitacora');
    }

    public function usuarioroles(){
        return $this->hasMany('App\Models\UsuariosRol');
    }

    public function rolespanaderia(){
        $modulo = Modulo::where('prefix', 'like', 'panaderia')->first();
        return $this->belongsToMany('App\Models\Rol', 'usuarios_roles', 'usuario_id', 'rol_id')->withPivot('modulo_id', 'autorizado')->where('usuarios_roles.modulo_id', $modulo->id)->where('usuarios_roles.autorizado', 1);
    }

    public function rolesrecmat(){
        $modulo = Modulo::where('prefix', 'like', 'recmat')->first();
        return $this->belongsToMany('App\Models\Rol', 'usuarios_roles', 'usuario_id', 'rol_id')->withPivot('modulo_id', 'autorizado')->where('usuarios_roles.modulo_id', $modulo->id)->where('usuarios_roles.autorizado', 1);
    }

    public function usuariorolesalim() {
        $modulo = Modulo::where('prefix', 'like', 'alimentarios')->first();
        return $this->belongsToMany('App\Models\Rol', 'usuarios_roles', 'usuario_id', 'rol_id')->withPivot('modulo_id', 'autorizado')->where('usuarios_roles.modulo_id',$modulo->id)->where('usuarios_roles.autorizado', 1);
    }

    
    public function rolestaesir(){
        $modulo = Modulo::where('prefix', 'like', 'taesir')->first();
        return $this->belongsToMany('App\Models\Rol', 'usuarios_roles', 'usuario_id', 'rol_id')->withPivot('modulo_id', 'autorizado')->where('usuarios_roles.modulo_id', $modulo->id)->where('usuarios_roles.autorizado', 1);
    }
    
    public function adminModule($role){
        foreach($this->usuarioroles as $uroles){
            if($uroles->rol->nombre === $role){
                return $uroles;
            }
        }
    }

    public function whereIsAdmin($uroles){
        $modulos_id = []; 
        foreach($uroles as $rol){
            if($rol->rol->valor == 1){
                array_push($modulos_id, $rol->modulo_id);
            }
        }
        $rolesModulo = Modulo::whereIn('id', $modulos_id)->get();
        return ['rolesModulo' => $rolesModulo, 'modulos_id' => $modulos_id];
    }

    public function hasRoles(array $roles){
        foreach($roles as $role)
        {
            foreach($this->usuarioroles as $uroles)
            {
                if($uroles->rol->nombre === $role)
                {
                    return true;
                }
            }

        }
        return false;
    }

    public function isSuperAdmin()
    {
        return $this->hasRoles(['SUPERADMIN']);
    }

    public function isAdmin()
    {
        return $this->hasRoles(['ADMINISTRADOR']);
    }

    public function routeNotificationForSlack() {
        return env('SLACK_WEBHOOK_URL');
    }

    public function bitacora(Request $request, array $datos){
        try{
            $actividad = Actividad::where('actividad', Route::current()->uri)->first();
            if($actividad === NULL){
                $actividad = Actividad::create([
                    'actividad' => Route::current()->uri,
                    'usuario_id' => isset(auth()->user()->id) ? auth()->user()->id : $request->usuario_id ? $request->usuario_id : null,
                ]);
            }
            $r = url()->current();
            $rutasModulos = Modulo::all();
            $modulo_id = 1;
            foreach($rutasModulos as $rutas){
                $pos = strpos($r, $rutas->prefix);
                if(!$pos === false){
                    $modulo_id = $rutas->id;
                }
            }
            $bitacora = Bitacora::create([
                'usuario_id' => isset(auth()->user()->id) ? auth()->user()->id : ($request->usuario_id ? $request->usuario_id : null),
                'actividad_id' => $actividad->id,
                'modulo_id' => $modulo_id,
                'tabla' => isset($datos['tabla']) ? $datos['tabla'] : 'ninguna',
                'registro' => isset($datos['registro']) ? $datos['registro'] : 0,
                'ip' => $request->ip(),
                'so' => $request->header()['user-agent'][0],
                'usuario_red' => shell_exec('echo %username%'),
                'campos' => $datos['campos'],
                'comentario' => isset($datos['comentario']) ? $datos['comentario'] : null,
                'metodo' => $datos['metodo']
            ]);
            return $bitacora;
        }catch(Exception $e){
            return $e;
        }
    }

    public function hasRolesModulo(array $roles, $modulo){
        /* Deprecated */
        foreach($roles as $role)
        {
            foreach($this->usuarioroles->where('modulo_id', $modulo) as $uroles)
            {
                if($uroles->rol->nombre === $role)
                {
                    return true;
                }
            }

        }
        return false;
    }

    public function hasRolesStrModulo(array $roles, $strmodulo){
        $modulo = Modulo::where('prefix', 'like', $strmodulo)->first();
        if($modulo == null){
            return false;
        }
        foreach($roles as $role)
        {
            foreach($this->usuarioroles->where('modulo_id', $modulo->id) as $uroles)
            {
                if($uroles->rol->nombre === $role)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public function generateToken() {
        $this->token = str_random(60);
        $this->save();
        return $this->token;
    }

    public function area(){
        return $this->hasOne('Modules\ControlInterno\Entities\AreaUsuario', 'usuario_id', 'id');
    }

    public function hasArea($find){
        $has = auth()->user()->area->area_id;
        if($has == $find){
            return true;
        }
        $subareas = Area::where('padre_id', $has)->get();
        foreach($subareas as $sub){
            $massubareas = Area::where('padre_id', $sub->id)->get();
            if($massubareas->count() > 0){
                foreach($massubareas as $otrasub){
                    if($find == $otrasub->id){
                        return true;
                    } 
                }
            }
            if($find == $sub->id){
                return true;
            }
        }
        return false;
    }

    public function getDatos($modulo_id){
        return [
            'id' => $this->id, 
            'usuario' => $this->usuario, 
            'persona_id' => $this->persona_id, 
            'persona' => [
                'id' => $this->persona->id, 
                'nombre' => $this->persona->nombre, 
                'primer_apellido' => $this->persona->primer_apellido, 
                'segundo_apellido' => $this->persona->segundo_apellido
            ], 
            'roles' => UsuariosRol::where('modulo_id', $modulo_id)->where('usuario_id', $this->id)->with(['rol', 'modulo'])->get()->toArray()
        ];
    }

    public function getRolesModulo($modulo_id) {
        return UsuariosRol::select('cat_roles.nombre')
        ->where('usuarios_roles.modulo_id', $modulo_id)
        ->where('usuarios_roles.usuario_id', $this->id)
        ->join('cat_roles', 'cat_roles.id', '=', 'usuarios_roles.rol_id')
        
        ->get()->pluck('nombre')
        ->toArray();
    }
}