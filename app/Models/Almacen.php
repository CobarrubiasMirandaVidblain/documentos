<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Almacen extends Model
{
    protected $table = "almacenes";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["cantidad","ubicacion","areas_producto_id", "usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function areasproducto(){
    	return $this->belongsTo('App\AreasProducto');
    }

    public function usuario(){
    	return $this->belongsTo('App\Usuario');
    }
}
