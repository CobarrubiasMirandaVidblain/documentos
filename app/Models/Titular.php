<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Titular extends Model
{
    use SoftDeletes;
    protected $table = 'titulares';
    protected $dates = ['deleted_at'];
    protected $fillable=["persona_id","fechainicio","fechafin","activo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function solicitudes(){
        return $this->hasMany('App\Models\Solicitud');
    }

}
