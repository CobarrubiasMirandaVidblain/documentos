<?php

namespace App\Models\Braille;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AlumnosBraille extends Model{

    use SoftDeletes;

    protected $table = 'braillealumnos';
    protected $dates = ['deleted_at'];
    protected $fillable = ['id','persona_id','discapacidad_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }

    public function discapacidad(){
        return $this->belongsTo('App\Models\Discapacidad');
    }

}
