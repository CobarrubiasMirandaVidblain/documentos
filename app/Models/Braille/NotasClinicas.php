<?php

namespace App\Models\Braille;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NotasClinicas extends Model{
    
    use SoftDeletes;

   	protected $table = 'notasclinicas';
    protected $dates = ['deleted_at'];
    protected $fillable = ['persona_id','titulo','descripcion','curso_id','usuario_id'];
    protected $hidden = array('updated_at', 'deleted_at');

    public function personas(){
      return $this->belongsTo('App\Models\Persona');
    }
}
