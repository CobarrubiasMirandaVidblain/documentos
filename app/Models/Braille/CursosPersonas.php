<?php

namespace App\Models\Braille;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CursosPersonas extends Model{
    
    use SoftDeletes;

   	protected $table = 'cursos_personas';
    protected $dates = ['deleted_at'];
    protected $fillable = ['persona_id','curso_id'];
    protected $hidden = array('updated_at', 'deleted_at');

}
