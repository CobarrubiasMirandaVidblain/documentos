<?php

namespace App\Models\Braille;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Curso extends Model{

    use SoftDeletes;

    protected $table = 'cat_cursos';
    protected $dates = ['deleted_at'];
    protected $fillable = ['id','nombre','descripcion','programa_id'];
    protected $hidden = array('updated_at', 'deleted_at');

    public function programa(){
      return $this->belongsTo('App\Models\Persona');
    }

}
