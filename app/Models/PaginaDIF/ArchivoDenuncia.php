<?php

namespace App\Models\PaginaDIF;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArchivoDenuncia extends Model
{
    use SoftDeletes;
    
    protected $table = 'pag_archivos_denuncias';

    protected $fillable = ['nombre', 'tamanio', 'url', 'tipo', 'buzon_id'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function denuncia()
    {
        return $this->belongsTo('App\Models\PaginaDIF\Denuncia', 'buzon_id', 'id');
    }
}
