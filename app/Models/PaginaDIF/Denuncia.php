<?php

namespace App\Models\PaginaDIF;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Denuncia extends Model
{
    use SoftDeletes;

    protected $table = 'pag_denuncias';

    protected $fillable = ['nombre_completo', 'domicilio', 'email', 'telefono', 'asunto', 'comentario'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function archivos()
    {
        return $this->hasMany('App\Models\PaginaDIF\ArchivoDenuncia', 'buzon_id', 'id');
    }
}
