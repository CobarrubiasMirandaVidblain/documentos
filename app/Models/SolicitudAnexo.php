<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudAnexo extends Model {
    protected $table = 'solicitudanexos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","anexo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

	public function solicitud(){
		return $this->belongsTo('App\Models\Solicitud');
	}
}
