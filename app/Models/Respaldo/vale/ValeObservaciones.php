<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeObservaciones extends Model
{
	protected $table = 'vale_observaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["grupos_alumno_id","observaciones"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
