<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeGrupos extends Model
{
	protected $table = 'vale_grupos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","descripcion"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
