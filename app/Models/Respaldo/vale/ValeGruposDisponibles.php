<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeGruposDisponibles extends Model
{
	protected $table = 'vale_gruposdisponibles';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nivel_id","grupo_id","hora_inicio","hora_fin","activo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    // public function scopeLoadDireccion($query){
    //     $query->join('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
    //     ->join('cat_municipios', 'cat_municipios.id', '=', 'cat_localidades.municipio_id')
    //     ->join('cat_distritos', 'cat_distritos.id' ,'=', 'cat_municipios.distrito_id')
    //     ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');
    // }


    //  $query->join('personas_programas','personas_programas.persona_id','=','personas.id')->
    // join('personas_tipos','personas_tipos.persona_id','=','personas.id')->
    // where('personas_tipos.tipospersona_id',6)->
    // get(['personas.id as id',DB::raw("CONCAT(personas.nombre,' ',personas.primer_apellido,' ', personas.segundo_apellido) as nombre")]);

}

