<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeEvaluaciones extends Model
{
	protected $table = 'vale_evaluaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["grupos_alumnos_id","valoracion"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
