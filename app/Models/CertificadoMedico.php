<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CertificadoMedico extends Model
{
	use SoftDeletes;

    protected $table = 'cred_certificadomedico';
    
    protected $dates = ['deleted_at'];

    protected $fillable=["persona_id","ruta_imagen","institucion_rubro_id", 'modulo_id'];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}

// faltan relaciones
 