<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventosTaller extends Model
{
    use SoftDeletes;
    protected $table = 'orev_eventos';
    protected $dates = ['deleted_at'];
    protected $fillable = ['tipo_evento_id','localidad_id','status_id','municipio_id','beneficiosprogramas_id','fechaEvento','calle','numExt','numInt','colonia','folio','codigoPostal','area_id','descripcionEvento','usuario_id','folio_peticion','peticion_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function tipoEvento (){
    	return $this->belongsTo('App\Models\CatEvento');
    }
    public function estatus (){
        return $this->belongsTo('App\Models\Statusproceso','status_id','id');
    }

    public function municipio() {
        return $this->belongsTo('App\Models\Municipio');
    }

    public function localidad() {
        return $this->belongsTo('App\Models\Localidad');
    }

   /* public function participantes(){
    	return $this->belongsToMany('App\Models\Participante','participante_evento','participante_id','tipo_evento_id');
    }*/

    public function participantes(){
        // return $this->belongsToMany('App\Models\Persona','participante_evento','tipo_evento_id','participante_id'); // antes
        return $this->belongsToMany('App\Models\Persona','orev_eventos_participantes','evento_id','persona_id')->where('orev_eventos_participantes.deleted_at',null)->withPivot('id','evaluado','constancia'); // depues
    }
    public function beneficio() {
        return $this->belongsTo('App\Models\Beneficiosprograma','beneficiosprogramas_id','id');
    }
    public function area(){
        return $this->belongsTo('App\Models\Area');
    }
}
