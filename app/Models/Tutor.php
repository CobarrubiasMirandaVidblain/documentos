<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tutor extends Model
{
    use SoftDeletes;

    protected $table = 'tutores';

    protected $dates = ['deleted_at'];

    protected $fillable = ['persona_id', 'tutorado_id', 'programa_id', 'activo', 'usuario_id'];

    public function persona() {
        return $this->belongsTo('App\Models\Persona', 'persona_id', 'id');
    }

    public function tutorado() {
    	return $this->belongsTo('App\Models\Persona', 'tutorado_id', 'id');
    }

    public function programa() {
    	return $this->belongsTo('App\Models\Programa');
    }

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
	}
}
