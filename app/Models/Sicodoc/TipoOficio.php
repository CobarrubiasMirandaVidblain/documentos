<?php

namespace App\Models\Sicodoc;

use Illuminate\Database\Eloquent\Model;

class TipoOficio extends Model
{
	protected $table = 'cat_tiposoficios';

    protected $fillable = ['tipo'];
}
