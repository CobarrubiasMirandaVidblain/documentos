<?php

namespace App\Models\Sicodoc;

use Illuminate\Database\Eloquent\Model;

class Escaneo extends Model
{
    protected $fillable = ['url'];
}
