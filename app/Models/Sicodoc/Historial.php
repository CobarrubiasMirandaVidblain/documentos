<?php

namespace App\Models\Sicodoc;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
	protected $table = 'historiales';

    protected $fillable = [
    	'id_oficio',
    	'observaciones'
    ];
}
