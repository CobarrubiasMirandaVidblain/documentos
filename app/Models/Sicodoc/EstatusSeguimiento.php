<?php

namespace App\Models\Sicodoc;

use Illuminate\Database\Eloquent\Model;

class EstatusSeguimiento extends Model
{
	protected $table = 'cat_estatusseguimientos';

    protected $fillable = ['estatus'];
}
