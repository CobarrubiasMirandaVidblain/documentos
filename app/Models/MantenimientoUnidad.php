<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class MantenimientoUnidad extends Model
{
    use SoftDeletes;
    protected $table = 'dtll_mantenimientosunidad';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = array('unidad_id', 'costo', 'descripcion', 'mantenimiento_id', 'fecha_mantenimiento');
    
}