<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archivo extends Model {
    use SoftDeletes;

    protected $table = 'archivos';

    protected $dates = ['deleted_at'];

    protected $fillable = ['modulo_id', 'tablabd_id', 'registro_num', 'url', 'short_url', 'nombre', 'tamanio'];
}