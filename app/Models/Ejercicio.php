<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ejercicio extends Model
{
	use SoftDeletes;

    protected $table = 'cat_ejercicios';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['anio'];

    public function aniosprogramas() {
    	return $this->hasMany('App\Models\AniosPrograma');
    }
}