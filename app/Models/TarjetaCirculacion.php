<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TarjetaCirculacion extends Model
{
    protected $table = 'circulaciontarjetas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["unidad_id", "oficina_expedidora", "fecha_expedicion", "vigencia", "clave_repuve", "reg_ent", "verificacion_vehicular"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function unidad(){
    	return $this->belongsTo('App\Models\Unidad');
    }
}
