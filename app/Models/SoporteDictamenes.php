<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoporteDictamenes extends Model
{
    protected $table = 'soportedictamenes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["respuesta_id","dirigidoa_id","realizo_id","autorizo_id","leyenda","detalles","diagnostico","nota","tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function respuestassoportesolicitudes(){
	return $this->belongsTo('App\Models\RespuestasSoporteSolicitudes');
}

public function empleado(){
	return $this->hasMany('App\Models\Empleado');
}

}
