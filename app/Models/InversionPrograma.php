<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class InversionPrograma extends Model {
    protected $table = 'inve_inversionesprogramas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["anio_programa_id","rubro_id","municipio_id","numbeneficiarios","dotaciones_totales","inversion"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $with = [
        "anioprogramas.ejercicio",
        "anioprogramas.programa",
        "rubro"
    ];

    public function municipio() {
        return $this->belongsTo('App\Models\Municipio');
    }

    public function anioprogramas() {
    	  return $this->belongsTo('App\Models\AniosPrograma','anio_programa_id');
    }

    public function rubro() {
        return $this->belongsTo('App\Models\Rubro');
    }
}