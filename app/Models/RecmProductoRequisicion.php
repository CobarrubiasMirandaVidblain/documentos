<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmProductoRequisicion extends Model
{
    use SoftDeletes;
    protected $table = 'recm_producto_requisicions';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['cantidad','precio', 'requisicion_id', 'presentacion_producto_id', 'status_id','observacion'];

    public function estatus (){ // Estatus del producto
        return $this->belongsTo('App\Models\RecmCatEstatus');
    }

}
