<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiCatProductos extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_cat_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['nombre'];

    public function presentaciones(){
        return $this->belongsToMany('App\Models\MopiCatPresentaciones', 'mopi_presentacion_productos', 'producto_id', 'presentacion_id')->withPivot('precio_estimado','id','descripcion');
    }

    public function categoria(){
        return $this->belongsToMany('App\Models\MopiCatCategorias', 'mopi_presentacion_productos', 'producto_id', 'categoria_id');
        // return $this->belongsTo('App\Models\MopiCatCategorias');
    }

}
