<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreasProducto extends Model
{
    protected $table = 'areas_productos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["producto_id","area_id","programa_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function producto(){
    	return $this->belongsTo('App\Models\Producto');
	}

	public function area(){
    	return $this->belongsTo('App\Models\Area');
	}

    public function programa(){
        return $this->belongsTo('App\Models\Programa');
    }

    public function detalleentradasproductos(){
    	return $this->hasMany('App\Models\DetalleEntradasproductos');
    }

    public function entradasproductos(){
        return $this->hasMany('App\Models\EntradasProducto');
    }

    public function salidasproductos(){
        return $this->hasMany('App\Models\SalidasProducto');
    }
}
