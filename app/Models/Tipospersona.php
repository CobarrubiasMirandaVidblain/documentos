<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipospersona extends Model
{
    protected $table = 'cat_tipospersonas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function personastipos(){
    	return $this->hasMany('App\PersonasTipo');
    }
}
