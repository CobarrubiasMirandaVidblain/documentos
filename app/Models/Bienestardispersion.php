<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BienestarDispersion extends Model
{
    use SoftDeletes;

    protected $table = 'bienestardispersiones';

    protected $dates = ['deleted_at'];

    protected $fillable = ['personas_programa_id', 'lista_id', 'pagado', 'observacion', 'usuario_id', 'cuenta_id'];

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
    }
    
	public function bienestarpersonas() {
    	return $this->belongsTo('App\Models\BienestarPersona');
	}

	public function personasprograma() {
    	return $this->belongsTo('App\Models\PersonasPrograma');
	}

    public function listasdispersiones() {
        return $this->belongsTo('App\Models\ListaDispersion', 'lista_id', 'id');
    }

    public function scopeSearch($query, $programa, $anio) {
        $query
        ->join('personas_programas', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')

        ->join('personas', 'personas.id', '=', 'personas_programas.persona_id')

        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->where('bienestarpersonas.ejercicio_id', '=', 8)
        ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'bienestarpersonas.discapacidad_id')

        ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftJoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftJoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftJoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        ->join('cuentasbancos', 'cuentasbancos.id', 'bienestardispersiones.cuenta_id')

        ->join('tutores', 'tutores.persona_id', '=', 'cuentasbancos.persona_id')
        ->where('tutores.programa_id', '=', 1)
        //->whereNull('tutores.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')

        ->leftJoin('cat_localidades as tutor_cat_localidades', 'tutor_cat_localidades.id', '=', 'tutor.localidad_id')
        ->leftJoin('cat_municipios as tutor_cat_municipios', 'tutor_cat_municipios.id', '=', 'tutor.municipio_id')
        ->leftJoin('cat_distritos as tutor_cat_distritos', 'tutor_cat_distritos.id', '=', 'tutor_cat_municipios.distrito_id')
        ->leftJoin('cat_regiones as tutor_cat_regiones', 'tutor_cat_regiones.id', '=', 'tutor_cat_distritos.region_id')
        ->distinct('bienestarpersonas.foliounico')
        ;

        /*$query
        ->join('personas_programas', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')
        ->join('personas', 'personas.id', '=', 'personas_programas.persona_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
        ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
        ->where('programas.nombre', 'LIKE', $programa)
        ->where('cat_ejercicios.anio', '=', $anio)
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->where('bienestarpersonas.ejercicio_id', '=', 8)
        ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'bienestarpersonas.discapacidad_id')

        ->join('tutores', 'tutores.tutorado_id', '=', 'bienestarpersonas.persona_id')
        ->whereNull('tutores.deleted_at')

        ->leftjoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutores.persona_id')
        ->whereNull('cuentasbancos.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')*/

        /*->join('tutores', 'tutores.tutorado_id', '=', 'bienestarpersonas.persona_id')
        ->whereNull('tutores.deleted_at')
        ->leftjoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutores.persona_id')
        ->whereNull('cuentasbancos.deleted_at')

        ->join('personas as t', 't.id', '=', 'tutores.persona_id')*/

        //->whereRaw('personas_programas.anios_programa_id = (select max(pp.anios_programa_id) from personas_programas as pp where personas_programas.persona_id = pp.persona_id)')
        //->orderBy('bienestarpersonas.posicion')
        //->orderBy('personas_programas.deleted_at')
        //;
    }

    /*public function scopeLoadDireccion($query) {
        $query->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');
    }

    public function scopeLoadPersona($query) {
        $query->join('personas_programas', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')
        ->join('personas', 'personas.id', '=', 'personas_programas.persona_id')
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id');
    }*/
}
