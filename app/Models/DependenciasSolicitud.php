<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DependenciasSolicitud extends Model
{
    protected $table = 'dependencias_solicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["dependencia_id","solicitud_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function dependencia(){
    	return $this->belongsTo('App\Models\Dependencia');
	}

	public function solicitud(){
		return $this->belongsTo('App\Models\Solicitud');
	}
}
