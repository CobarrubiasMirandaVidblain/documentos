<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidad extends Model
{
  use SoftDeletes;

/* protected $connection = 'mysql_server_real'; */

  protected $table = 'unidades';
  protected $dates = ['deleted_at'];
  protected $fillable=["matricula","modelo","marca","anio","condiciones","kilometraje_inicial","tipo_servicio","observaciones","poliza_garantia", "numero_unidad",
  "numero_serie","numero_motor","numero_cilindros","clave_vehicular","tipo","combustible","litros","toneladas","uso_vehiculo", "clase",
  "origen_vehiculo","fabricante","procedencia","color_exterior","color_interior","capacidad_pasajeros","numero_puertas","numero_inventario","numero_pedido", "descripcion"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
}