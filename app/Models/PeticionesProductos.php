<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeticionesProductos extends Model{
    use SoftDeletes;

    protected $table = 'afu_peticionesproductos';
    protected $dates = ['deleted_at'];
    protected $fillable=["salidas_producto_id","beneficioprogsol_benefpersona_id","usuario_id"];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');    
}
