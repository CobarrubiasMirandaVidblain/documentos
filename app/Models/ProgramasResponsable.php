<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramasResponsable extends Model
{
    protected $table = 'programas_responsables';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["programa_id","areas_responsable_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){//quien dio de alta el programa
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function programa(){
    	return $this->belongsTo('App\Models\Programa');
    }

    public function areasresponsable(){
        return $this->belongsTo('App\Models\AreasResponsable','areas_responsable_id');
    }

}
