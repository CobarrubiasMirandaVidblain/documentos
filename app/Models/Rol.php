<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    protected $table = 'cat_roles';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=['nombre', 'descripcion', 'valor', 'modulo_id', 'usuario_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){//quien dio de alta el rol
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function usuarioroles(){
        return $this->hasMany('App\Models\UsuariosRol');
    }

    public function modulo(){
        return $this->belongsTo('App\Models\Modulo');
    }

}
