<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CredImpresion extends Model
{
    use SoftDeletes;

    protected $table = 'cred_impresion';
    
    protected $dates = ['deleted_at'];

    protected $fillable=["personas_tipos_id","tipo_impresion_id","fecha_impresion", "usuario_id"];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function PersonasTipo(){
    	return $this->belongsTo('App\Models\PersonasTipo');
	}

	public function tipoImpresion(){
    	return $this->belongsTo('App\Models\CredTipoImpresion');
	}
}

// faltan relaciones