<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CargoPrograma extends Model{
  use SoftDeletes;

  protected $table = 'cargos_programas';
  protected $dates = ['deleted_at'];
  protected $fillable=['cargo_id','programa_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function programa()
  {
      return $this->belongsTo('App\Models\Programa');
  }
  public function cargo()
  {
      return $this->belongsTo('App\Models\Cargo');
  }
  
}