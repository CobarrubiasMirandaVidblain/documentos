<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Beneficiosprograma extends Model{
	use SoftDeletes;
	
	protected $table = 'beneficiosprogramas';
	protected $dates = ['deleted_at'];
	protected $fillable=["anio_programa_id","nombre","usuario_id","predeterminado","puede_agregar_benef"];
	protected $hidden = array('updated_at', 'deleted_at');
	
	public function usuario(){//quien dio de alta el registro
		return $this->belongsTo('App\Models\Usuario');
	}
	
	public function anioprograma(){
		return $this->hasOne('App\Models\AniosPrograma','id','anio_programa_id');
	}
	
}