<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleSalidasproductos extends Model {
    protected $table = "detallessalidas_productos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["salidas_producto_id","cantidad","beneficioprograma_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

 /*    public function areasproducto(){
    	return $this->belongsTo('App\Models\AreasProducto','areas_producto_id');
    } */

    public function salidasproducto(){
    	return $this->belongsTo('App\Models\SalidasProducto');
    }

    public function productosfoliados(){
    	return $this->hasMany('App\Models\Productosfoliados','detallessalidas_producto_id');
    }
}
