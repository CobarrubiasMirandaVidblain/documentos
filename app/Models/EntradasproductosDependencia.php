<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntradasproductosDependencia extends Model {
    protected $table = "entradasproductos_dependencias";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["entradas_producto_id","dependencia_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function entradasproducto(){
        return $this->belongsTo('App\Models\EntradasProducto');
    }

    public function dependencia(){
    	return $this->belongsTo('App\Models\Dependencia');
    }
}
