<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Distrito extends Model
{
    protected $table = 'cat_distritos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","region_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    //protected $with = ['municipios'];


    public function municipios(){
    	return $this->hasMany('App\Models\Municipio');
    }

    public function region(){
    	return $this->belongsTo('App\Models\Region');
    }
}
