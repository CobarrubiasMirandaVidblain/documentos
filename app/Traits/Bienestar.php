<?php namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait Bienestar
{
    private $programa = 'BIENESTAR';

    private $anio = 2018;

    private $vigencia = 2018;

    private $cupo = 21108;

    private $usuario = 'BANCO';

    private $contrasenia = '21QWERTY43';

    public function acceso(array $roles) {
        if(!Auth::user()->hasRolesModulo($roles, 2)) {
            abort(403, 'No cuentas con los permisos necesarios.');
        }
    }
}