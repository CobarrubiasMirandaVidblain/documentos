<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Error</title>
  <style>
    body {
  background: rgba(200, 200, 200, 0.1);
}
h1 {
  text-align: center;
  font-family: helvetica;
  font-weight: lighter;
  padding: 0;
  margin: 0;
  font-size: 3em;
  margin-top: 10px;
}
p {
  text-align: center;
  font-family: helvetica;
  color: #aaa;
  font-size: 12px;
}
.window {
  margin: 0 auto;
  margin-top: 15px;
  height: 150px;
  width: 200px;
  background: #fff;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  -webkit-box-shadow: 0 3px 5px 1px rgba(0, 0, 0, 0.1);
  box-shadow: 0 3px 5px 1px rgba(0, 0, 0, 0.1);
}
.window::before {
  content: "";
  position: absolute;
  width: 100px;
  height: 150px;
  background: rgba(230, 230, 230, 0.3);
  z-index: 999;
  margin-left: 100px;
  -webkit-border-top-right-radius: 5px;
  -webkit-border-bottom-right-radius: 5px;
  -moz-border-radius-topright: 5px;
  -moz-border-radius-bottomright: 5px;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
}
.window::after {
  content: "";
  position: absolute;
  width: 200px;
  height: 20px;
  background: #e3e3e3;
  -webkit-border-top-left-radius: 5px;
  -webkit-border-top-right-radius: 5px;
  -moz-border-radius-topleft: 5px;
  -moz-border-radius-topright: 5px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
}
.window .button {
  height: 7px;
  width: 7px;
  position: absolute;
  background: #A60303;
  z-index: 999;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  margin-top: 6px;
  margin-left: 10px;
}
.window .button::before {
  content: "";
  height: 7px;
  width: 7px;
  position: absolute;
  background: #F2CB05;
  z-index: 999;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  margin-left: 10px;
}
.window .button::after {
  content: "";
  height: 7px;
  width: 7px;
  position: absolute;
  background: #098429;
  z-index: 999;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  margin-left: 20px;
}
.window .display {
  position: absolute;
  color: #A60303;
  z-index: 999;
  font-family: helvetica;
  font-size: 75px;
  margin-left: 78px;
  margin-top: 40px;
  font-weight: bold;
}
.window .display::before {
  content: "{";
  position: absolute;
  color: #c3c3c3;
  margin-left: -50px;
  font-size: 90px;
  margin-top: -15px;
}
.window .display::after {
  content: "}";
  position: absolute;
  color: #c3c3c3;
  margin-left: 15px;
  font-size: 90px;
  margin-top: -15px;
}

  </style>
</head>

<body>
  <h1>Error: Su sesión ha expirado.</h1>
  <div class="window">
    <div class="button"></div>
    <div class="display">?</div>
  </div>
  <!--end window-->
  <p>Code? What code?</p>
  <h1>Por favor <a href="javascript:location.reload()">inicia sesión</a> de nuevo</h1>
</body>

</html>
