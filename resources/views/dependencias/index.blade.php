@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle', '')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Catálogo de dependencias e instituciones</h3>		
	</div>
	<div class="box-body">
		<!-- /.box-header -->
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>

		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'dependencias', 'name' => 'dependencias', 'style' => 'width: 100%']) !!}
		
	</div>
</div>

<div class="modal fade" id="modal-dependencia" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="cerrar_modal('modal-dependencia')" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modal-title-dependencia">Registrar dependencia o institución</h4>
			</div>
			<div class="modal-body" id="modal-body-dependencia"></div>
			<div class="modal-footer" id="modal-footer-dependencia">
				<button id="btn-guardar" type="button" class="btn btn-success" onclick="dependencia.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
				<button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-dependencia')"><i class="fa fa-close"></i> Cerrar</button>
			</div>      
		</div>
	</div>
</div>

@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
@include('dependencias.js.dependencia')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
	var tabla = $('#dependencias').DataTable();
	
	$('#search').keypress(function(e) {
		if(e.which === 13) {
			tabla.search($('#search').val()).draw();
		}
	});

	$('#btn_buscar').on('click', function() {
		tabla.search($('#search').val()).draw();
	});

	function abrir_modal(url, title, ocultar_boton) {
      	$("#modal-title-dependencia").text(title.toUpperCase());
      	$.get(url, function(data) {
	        $('#modal-body-dependencia').html(data.html);                            
    	    dependencia.init();
			$("#btn-guardar").removeClass('hide');
			if(ocultar_boton){
				$("#btn-guardar").addClass('hide');
			}
	        $('#modal-dependencia').modal('show');            
    	});
	};

    function cerrar_modal(modal){
        $("#"+modal).modal("hide");
    }

	function dependencia_create_edit_success(response, method) {
		var mensaje = (method === "POST") ? 'registrada' : 'actualizada';
		swal({
      		title: '¡Correcto!',
      		text: response.tipo.nombre.charAt(0).toUpperCase() + response.tipo.nombre.slice(1).toLowerCase() + ' ' + mensaje + '.',
      		type: 'success',
      		timer: 2000
    	});
    	tabla.ajax.reload(null, false);
		app.set_bloqueo(false);
        cerrar_modal('modal-dependencia');
	};

	function dependencia_create_edit_error(response) {
		unblock();
    };

	function dependencia_delete(dependencia_id, tipo) {
		swal({
			title: '¿Está seguro de eliminar la ' + tipo.toLowerCase() + '?',
			text: '¡La ' + tipo.toLowerCase() + ' se eliminará de forma permanente!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Sí, eliminar',
			cancelButtonText: 'No, regresar'
		}).then((result) => {
			if(result.value) {

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: "{{ URL::to('dependencias/destroy') }}" + '/' + dependencia_id,
					type: 'DELETE',
					success: function(response) {
						tabla.ajax.reload(null, false);
						swal({
							title: '¡Eliminado!',
							text: 'La dependencia ha sido eliminada.',
							type: 'success',
							timer: 3000
						});
					},
					error: function(response) {
						unblock();
					}
				});

			}
		});
	}
</script>
@endpush