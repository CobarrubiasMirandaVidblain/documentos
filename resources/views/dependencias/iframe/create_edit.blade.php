<form data-toggle="validator" role="form" id="form_dependencia" action="{{ isset($dependencia) ? URL::to('dependencias/update') . '/' . $dependencia->id : URL::to('dependencias/store') }}" method="{{ isset($dependencia) ? 'PUT' : 'POST' }}">

    

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nombre">Nombre de la dependencia o institución* :</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $dependencia->nombre or '' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="form-group">
                        <label for="tipoinstitucion_id">Tipo* :</label>
                        <select id="tipoinstitucion_id" name="tipoinstitucion_id" class="form-control select2" style="width: 100%;">
                            @foreach($tipos as $tipo)
                                @if(isset($dependencia) && $tipo->id == $dependencia->tipo->id)
                                    <option value="{{ $tipo->id }}" selected>{{ $tipo->nombre }}</option>
                                @else
                                    <option value="{{ $tipo->id }}" {{ !isset($dependencia) && $tipo->id == 1 ? 'selected' : '' }}>{{ $tipo->nombre }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                {{-- <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label for="entidad_id">Sector al que pertenece* :</label>
                        <select id="entidad_id" name="entidad_id" class="form-control select2" style="width: 100%;">
                            @foreach($entidades as $entidad)
                                @if(isset($dependencia) && $entidad->id == $dependencia->entidad->id)
                                    <option value="{{ $entidad->id }}" selected>{{ $entidad->entidad }}</option>
                                @else
                                    <option value="{{ $entidad->id }}" {{ !isset($dependencia) && $entidad->id == 20 ? 'selected' : '' }}>{{ $entidad->entidad }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div> --}}
            </div>         
            

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <div class="form-group">
                        <label for="encargado">Nombre del titular* :</label>
                        <input type="text" class="form-control" id="encargado" name="encargado" value="{{ $dependencia->encargado or '' }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                    <div class="form-group">
                        <label for="cargoencargado">Cargo del titular* :</label>
                        <input type="text" class="form-control" id="cargoencargado" name="cargoencargado" value="{{ $dependencia->cargoencargado or '' }}">
                    </div>
                </div>
                
            </div>

    <h4>Dirección:</h4>

    <hr>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="form-group">
                <label for="calle">Calle:</label>
                <input type="text" class="form-control" id="calle" name="calle" value="{{ $dependencia->calle or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="form-group">
                <label for="numero_exterior">Número:</label>
                <input type="text" class="form-control" id="numero" name="numero" value="{{ $dependencia->numero or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="colonia">Colonia:</label>
                <input type="text" class="form-control" id="colonia" name="colonia" value="{{ $dependencia->colonia or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <div class="form-group">
                <label for="entidad_id">Entidad:</label>
                <select id="entidad_id" name="entidad_id" class="form-control select2" style="width: 100%;">
                    @foreach($entidades as $entidad)
                        @if(isset($dependencia) && $entidad->id == $dependencia->entidad->id)
                            <option value="{{ $entidad->id }}" selected>{{ $entidad->entidad }}</option>
                        @else
                        <option value="{{ $entidad->id }}" {{ !isset($dependencia) && $entidad->id == 20 ? 'selected' : '' }}>{{ $entidad->entidad }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="form-group">
                <label for="telefono">Télefono:</label>
                <input type="text" class="form-control" id="telefono" name="telefono" value="{{ $dependencia->telefono or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="email">Correo electrónico:</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ $dependencia->email or '' }}">
            </div>
        </div>

    </div>

</form>