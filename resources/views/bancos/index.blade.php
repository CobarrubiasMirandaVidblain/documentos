@extends('layouts.app2')

@section('content-title', 'Bancos')
@section('content-subtitle', 'index')

@section('mystyles')
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity 1s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
</style>
@endsection

@section('content')
<div class="row" id="app">
    <div class="col-md-4">
        <!-- general form elements -->
        <div class="box" :class="[active ? 'box-primary' : '']" v-on:mouseover="active = true; active2 = false">
            <div class="box-header with-border">
              <h3 class="box-title">Agregar Nuevo</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nombre del Banco</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nombre del banco" v-model="banco.nombre">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" v-on:click.prevent="create(banco)">Guardar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
    <div class="col-md-8">
        <div class="box" :class="[active2 ? 'box-primary' : '']" v-on:mouseover="active = false; active2 = true">
            <div class="box-header with-border">
              <h3 class="box-title">Todos los Bancos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered" v-if="bancos">
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Nombre</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                    <template v-for="banco in bancos">
                        <transition name="fade">
                            <note v-bind:banco="banco"></note>
                        </transition>
                    </template>
                </table>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">&laquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">&raquo;</a></li>
              </ul>
            </div>
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection

                    
@section('cdn-scripts')
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>


<script>
Vue.component('note', {
  template: `
    <tr>
        <td>@{{ banco.id }}</td>
        <td v-if="!editing">@{{ banco.nombre }}</td>
        <td v-else><input v-model="banco.nombre"></td>
        <td v-if="!editing"><button v-on:click="edit()" class="btn btn-flat btn-xs btn-warning"><i class="fa fa-pencil"></i></button></td>
        <td v-else>
            <button v-on:click="update(banco, $data)" class="btn btn-flat btn-xs btn-success"><i class="fa fa-check"></i></button>
            <button v-on:click="cancel()" class="btn btn-flat btn-xs btn-danger"><i class="fa fa-remove"></i></button>
        </td>
        <td><button v-on:click="destroy(banco)" class="btn btn-flat btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>
    </tr>`,
  props: ['banco', 'fila'],
  data: function () {
    return {
      editing: false
    }
  },
  methods: {
    edit(){
        this.editing = true;
    },
    cancel(){
        this.editing = false;
    },
    update(banco, data){
        axios.put('{{ url("/") }}' + '/bancos/' + banco.id, {
            nombre: this.banco.nombre
        })
        .then(function (response) {
            banco.nombre = response.data.nombre;
            data.editing = false;
            swal(
                'Genial!',
                'Se guardo correctamente!',
                'success'
                );
        })
        .catch(function (error) {
            swal(
                'Oops!',
                'Ocurrio un error: ' + error,
                'question'
                );
        });
    },
    destroy(banco){
        swal({
            title: 'Estás seguro de eliminar?',
            text: "Esto no se puede revertir!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminalo!'
            }).then((result) => {
                if (result.value) {
                    axios.delete('{{ url("/") }}' + '/bancos/' + banco.id, {})
                        .then(response => {
                            swal(
                                'Eliminado!',
                                'Se ha borrado',
                                'success'
                            ).then(result => {
                                let p = app.bancos.map((b)=>{return b.id}).indexOf(banco.id);
                                app.bancos.splice(p, 1);
                            });
                        })
                        .catch(error => {
                            swal(
                                'Error!',
                                'Ocurrio un error. ' + error,
                                'error'
                            )
                        });
                }
            });
        }
    }
});

const app = new Vue({
  el: '#app',
  data: {
        msg: 'CRUD Bancos',
        url: '{{route('bancos.store')}}',
        active: false,
        active2: false,
        banco: {
            id: null,
            nombre: '',
            edit: false
        },
        bancos: []
    },
    computed:{
        nombreToUpperCase(){
            return this.banco.nombre.toUpperCase();
        }
    },
    methods: {
        create(banco){
            axios.post('{{ route('bancos.store') }}', {
                nombre: this.nombreToUpperCase
            })
            .then(function (response) {
                this.banco.nombre = '';
                this.bancos.push(response.data);
                swal("Genial!", "Se agrego correctamente, ", "success");
            })
            .catch(function (error) {
                swal("Oops!", "Ocurrio un error. " + error, "error");
            });
        },
        getAll(){
            axios.get('{{ route('bancos.index') }}')
                .then(response => {
                    this.bancos = response.data;
                })
                .catch(error => { console.log(error); 
                    swal("Oops!", "Ocurrio un error. " + error, "error");
                });
        }
    },
    mounted(){
        this.getAll();
    }
});

</script>
@endsection