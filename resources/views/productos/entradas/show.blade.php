<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form class="form-horizontal">
            <div class="row">
                <label class="col-sm-2 control-label">Fecha:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{ $entrada->get_formato_fecha() }}</p>
                </div>
                <label class="col-sm-2 control-label">Tipo:</label>
                <div class="col-sm-4">
                    <p class="form-control-static">{{ $entrada->tiposentrada->tipo }}</p>
                </div>
            </div>
            <div class="row">                        
                <label class="col-sm-2 control-label">Remitente:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ isset($entrada->entradasproductosdependencia) ? $entrada->entradasproductosdependencia->dependencia->nombre : $entrada->entradasproductospersona->persona->get_nombre_completo() }}</p>
                </div>
            </div>
            <div class="row">
                <label class="col-sm-2 control-label">Recibido por:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $entrada->empleado->persona->get_nombre_completo() }}</p>
                </div>
            </div>
        </form>
    </div>
</div>
<br>

<div class="box box-primary">
    <div class="box-header with-border">
        <h5 class="box-title">Lista de productos</h5>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="productos" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">PRODUCTO</th>
                            {{-- En el caso de afuncionales que tiene productos foliados --}}
                            @if($entrada->detalleentradasproductos->first()->productosfoliados->count() > 0)
                                <th class="text-center">FOLIO</th>                            
                            {{-- En el caso de dregionales que NO tiene productos foliados --}}
                            @else
                                <th class="text-center">CANTIDAD</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($entrada->detalleentradasproductos as $detalle)

                            {{-- En el caso de afuncionales que tiene productos foliados --}}
                            @if($detalle->productosfoliados->count() > 0)
                                @foreach($detalle->productosfoliados as $productofoliado)
                                    <tr>                        
                                        <td>{{$detalle->areasproducto->producto->producto}}</td>
                                        <td class="text-center">{{$productofoliado->folio}}</td>
                                    </tr>
                                @endforeach
                            {{-- En el caso de dregionales que NO tiene productos foliados --}}
                            @else
                                <tr>                        
                                    <td>{{$detalle->areasproducto->producto->producto}}</td>
                                    <td class="text-center">{{$detalle->cantidad}}</td>
                                </tr>
                            @endif                            
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center">PRODUCTO</th>
                            {{-- En el caso de afuncionales que tiene productos foliados --}}
                            @if($entrada->detalleentradasproductos->first()->productosfoliados->count() > 0)
                                <th class="text-center">FOLIO</th>                            
                            {{-- En el caso de dregionales que NO tiene productos foliados --}}
                            @else
                                <th class="text-center">CANTIDAD</th>
                            @endif
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>