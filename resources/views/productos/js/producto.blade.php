<script type="text/javascript">	

	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Estás seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	function producto_create_edit_success(response, method) {};
	
	function producto_create_edit_error(response) {};

	var producto = (function() {
		var form = $('#form_producto'),
		unidadmedida = $('#unidadmedida_id'),
		action = form.attr('action'),
		method = form.attr('method');

		function init() {
			form = $('#form_producto'),
            unidadmedida = $('#unidadmedida_id'),
			action = form.attr('action'),
			method = form.attr('method');	
			unidadmedida.select2({
				language: 'es',
				placeholder: 'SELECCIONE UNA OPCIÓN',
				minimumResultsForSearch: Infinity				
			}).change(function(event) {
				unidadmedida.valid();				
			});
            $('#programa_id').select2({
                language: 'es',
                allowClear : true,
                placeholder : 'SELECCIONE EL PROGRAMA RESPONSABLE',
                ajax: {
                    delay: 500,
                    url: "{{ route('beneficio.select') }}",
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term,
                            programa_padre: 'APOYOS FUNCIONALES'
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        }
                    },
                    cache: true                    
                }
            }).change(function(event) {
				$('#programa_id').valid();				
			});

			agregar_validacion();
			app.to_upper_case();
		};

		function create_edit() {
			if(form.valid()) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: form.serialize(),
					success: function(response) {
						producto_create_edit_success(response, method);
					},
					error: function(response) {
						producto_create_edit_error(response);
					}
				});

			}
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					producto: {
						required: true
					},
					descripcion: {
						required: true
					},
					marca: {
						required: true
					},
                    modelo: {
						required: true
					},
					unidadmedida_id: {
						required: true
					},
					vigencia: {
						required: true
					},
					serie: {
						required: true
					},
                    programa_id: {
                        required: true
                    }					
				},
				messages: {
				}
			});
		};

		return {
			init: init,			
			create_edit: create_edit
		};
	})();
</script>