@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle', 'Productos')

@section('li-breadcrumbs')
    <li class="active">Productos</li>
@endsection

@section('content')	
	<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary shadow">          
          <div class="box-header with-border">
            <h3 class="box-title">Catálogo de productos</h3>
          </div>
          <div class="box-body">
              <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                  <input type="text" id="search" name="search" class="form-control">
                  <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                  </span>
              </div>
              {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'productos', 'name' => 'productos', 'style' => 'width: 100%']) !!}
            </div>
        </div>
      </div>
    </div>
  </section>

<div class="modal fade" id="modal-producto">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" onclick="cerrar_modal('modal-producto')" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="modal-producto-title"></h4>
          </div>
          <div class="modal-body" id="modal-body-producto"></div>
          <div class="modal-footer" id="modal-footer-producto">
              <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-producto')"><i class="fa fa-ban"></i> Cancelar</button>
              <button type="button" class="btn btn-success" onclick="producto.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
          </div>      
      </div>
  </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
@include('productos.js.producto')
{!! $dataTable->scripts() !!}

<script type="text/javascript">
    $(document).ready(function() {
        var productos = $('#productos');  
		datatable_productos = $(productos).DataTable();

        $('#search').keypress(function(e) {
			if(e.which === 13) {
				datatable_productos.search($('#search').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_productos.search($('#search').val()).draw();
		});
	});

    function producto_delete(producto_id) {
        swal({
            title: '¿Está seguro de eliminar el producto?',
            text: '¡El producto se eliminará de forma permanente!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar',
            cancelButtonText: 'No, regresar',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                block();
                $.ajaxSetup({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                });
                $.ajax({
                    url: "/{{$modulo}}/productos" + '/' + producto_id,
                    type: 'POST',
                    data: {_method: 'delete'},
                    success: function(response) {
                        unblock();
                        datatable_productos.ajax.reload(null, false);
                        swal({
                            title: '¡Eliminado!',
                            text: 'El producto ha sido eliminado.',
                            type: 'success',
                            timer: 3000
                        });
                    },
                    error: function(response) {
                        unblock();                                                
                        if(response.status === 422) {
			                swal({
				                title: 'Error al eliminar el producto',				
				                text: response.responseJSON.errors[0].message,
				                type: 'error',
				                confirmButtonColor: '#3085d6',
				                confirmButtonText: 'Regresar',
				                allowEscapeKey: false,
				                allowOutsideClick: false
			                });
		                }
                    }
                });
            }
        });
    }

    function abrir_modal(modal, id) {
        var url = (id) ? '/{{$modulo}}/productos/' + id +'/edit' : '/{{$modulo}}/productos/create';
        var title = (id) ? 'Editar producto' : 'Registrar producto';
        $("#modal-producto-title").text(title);
        $.get(url, function(data) {            
            $('#modal-body-producto').html(data.html);                            
            producto.init();                
            $('#modal-producto').modal('show');            
        });
	}

    function cerrar_modal(modal){
        $("#"+modal).modal("hide");
    }

    function producto_create_edit_success(response, method) {
        var mensaje = (method === "POST") ? 'registrado' : 'actualizado';
        swal({
            title: '¡Correcto!',
            text: 'Producto ' + mensaje + '.',
            type: 'success',
            timer: 2000
        });
        datatable_productos.ajax.reload(null, false);
		app.set_bloqueo(false);
		unblock();
        cerrar_modal('modal-producto');      
	};

</script>
@endpush