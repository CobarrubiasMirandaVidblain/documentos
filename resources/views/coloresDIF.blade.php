<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DIF | Colores</title>   
    <link rel="stylesheet" href="css/materialize.min.css">
    <link rel="stylesheet" href="css/dif.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <style>
    .color{
        /* outline: 1px solid green; */
        display: flex;
        align-items: center;
        width: 100%;
    }
    .icon{
        padding: 0;
        width: 50%;
        /* outline: 1px solid red; */
        margin-right: 10px;
    }
    .colores{
        width: 50%;
    }
    .colores .colori{
        /* outline: 1px solid blue; */
        margin: 3px 0;
        display: flex;
        justify-content: center;
    }
    .colores .colori a{
        text-decoration: none;
        color: white;
    }
    .colores .colori a:hover{
        cursor: pointer;
    }
    h5{
        text-align: center;
    }
    </style>
</head>
<body>
    <div class="container-fluid">
        <div id="app">
            <div class="row">
                <div v-for="(color, key) in colors" class="col s6 m4 color">
                    <div class="icon">
                        <i class="icon-dif large" :style="'color: ' + color.hex + ';'"></i>
                        <h5> @{{ color.hex }} </h5>
                    </div>
                    <div class="colores">
                       <div v-for="(c, index) in color.derived" class="colori" :style="'background-color: ' + c + ';'">
                           <a href="#" @click.prevent="changeColor(color, c, index)" class="color" :data-clipboard-text="c">@{{ c }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset("js/materialize.min.js") }}"></script>
    <script src="{{ asset("js/vue.js") }}"></script>
    <script src="{{ asset("js/clipboard.min.js") }}"></script>
    <script src="{{ asset("js/sweetalert2.all.js") }}"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                colors: [
                    {
                        name: 'Verde',
                        pantone: '345 PC',
                        rgb: [160, 207, 168],
                        hex: '#A0CFA8',
                        cmyk: [43, 0, 41, 0],
                        derived: ['#F9FCFA', '#ECF5EE', '#D9ECDC', '#C6E2CB', '#B3D9B9', '#A0CFA8', '#88C391', '#6FB77B', '#57AA65', '#4A9356', '#3E7A48']
                    },
                    {
                        name: 'Agua',
                        pantone: '3242 C',
                        rgb: [113, 219, 212],
                        hex: '#71DBD4',
                        cmyk: [44, 0, 20, 0],
                        derived: ['#F6FDFC', '#E2F8F6', '#C6F1EE', '#A9EAE5', '#8DE2DD', '#71DBD4',  '#56D5CC', '#3BCEC4', '#2EBAB0', '#289F97', '#21857E']
                    },
                    {
                        name: 'Azul',
                        pantone: 'BluE 0821 C',
                        rgb: [116, 209, 234],
                        hex: '#74D1EA',
                        cmyk: [47, 0, 7, 0],
                        derived: ['#F7FCFE', '#E3F6FB', '#C8EDF7', '#ACE3F3', '#91DAEF', '#74D1EA', '#57C7E6', '#38BDE1', '#20AFD6', '#1B96B8', '#177D99']
                    },
                    {
                        name: 'Rosa',
                        pantone: '211 CP',
                        rgb: [229, 139, 173],
                        hex: '#E58BAD',
                        cmyk: [0, 61, 6, 0],
                        derived: ['#FDF8FA', '#FAE8EF', '#F4D0DE', '#EFB9CE', '#EAA2BD', '#E58BAD', '#DE6D98', '#D74F83', '#D0316E', '#B4295E', '#96224E']
                    },
                    {
                        name: 'Fiusha',
                        pantone: '191 CP',
                        rgb: [239, 66, 111],
                        hex: '#EF426F',
                        cmyk: [0, 79, 36, 0],
                        derived: ['#FEF4F6', '#FCD9E3', '#F8B4C6', '#F58EAA', '#F2698D', '#EF426F', '#EC275C', '#E1144A', '#C51141', '#A90F38', '#8D0C2F']
                    },
                    {
                        name: 'Vino',
                        pantone: '1925 CP',
                        rgb: [219, 55, 98],
                        hex: '#DB3762',
                        cmyk: [ 0, 97, 50, 0],
                        derived: ['#FDF3F6', '#F8D7E0', '#F0B0C1', '#E988A2', '#E26083', '#DB3762', '#D12654', '#BA224B', '#A31E41', '#8C1A38', '#74152F']
                    },
                    {
                        name: 'Rojo',
                        pantone: '178 CP',
                        rgb: [255, 88, 93],
                        hex: '#FF585D',
                        cmyk: [0, 70, 58, 0],
                        derived: ['#FFF5F5', '#FFDDDE', '#FFBCBE', '#FF9A9D', '#FF787D', '#FF585D', '#FF353B', '#FF121A', '#EF0008', '#CD0007', '#AB0006']
                    },
                    { 
                        name: 'Morado',
                        pantone: '2582 CP',
                        rgb: [152, 87, 155],
                        hex: '#98579B',
                        cmyk: [48, 80, 0, 0],
                        derived: ['#F9F5F9', '#EBDCEC', '#D7BAD8', '#C297C5', '#AE75B1', '#98579B', '#874E8A', '#78457B', '#693C6B', '#5A345C', '#4B2B4D']
                    },
                    {
                        name: 'Amarillo',
                        pantone: '116 CP',
                        rgb: [245, 203, 8],
                        hex: '#F5CB08',
                        cmyk: [0, 14, 100, 0],
                        derived: ['#FFFCF0', '#FDF5CE', '#FCEA9C', '#FAE06B', '#F9D639', '#F5CB08', '#DFB707', '#C6A306', '#AD8E05', '#947A05', '#7C6604']
                    },
                    {
                        name: 'Naranja',
                        pantone: '1375 CP',
                        rgb: [233, 156, 52],
                        hex: '#E99C34',
                        cmyk: [0, 45, 94, 0],
                        derived: ['#FEF9F3', '#FBEBD7', '#F6D7AE', '#F2C386', '#EDAF5D', '#E99C34', '#E68E1B', '#CE7E17', '#B46F14', '#9A5F11', '#814F0E']
                    },
                    {
                        name: '',
                        pantone: '388 C',
                        rgb: [224, 231, 33],
                        hex: '#E0E721',
                        cmyk: [15, 0, 80, 0],
                        derived: ['#FDFEF2', '#F9FAD3', '#F3F5A7', '#ECF07B', '#E6EB4F', '#E0E721', '#D0D718', '#B9BF15', '#A2A713', '#8B8F10', '#74770D']
                    },
                    {
                        name: 'Negro',
                        pantone: '',
                        rgb: [],
                        hex: '#1E282C',
                        cmyk: [],
                        derived: []
                    },
                    {
                        name: 'Azul Oscuro',
                        pantone: '',
                        rgb: [],
                        hex: '#002b59',
                        cmyk: [],
                        derived: ['#002b59']
                    }
                ]
            },
            methods: {
                changeColor(color, newColor, index){
                    color.hex = newColor;
                    let c = '.' + color.name + '-' + index;
                    var clipboard = new ClipboardJS('.color');
                    clipboard.on('success', function(e) {
                        // console.info('Accion:', e.action);
                        // console.info('Texto:', e.text);
                        // console.info('Trigger:', e.trigger);

                        e.clearSelection();
                    });
                    swal({
                        title: 'Copiado!',
                        text: newColor,
                        timer: 500,
                        background: newColor,
                        onOpen: () => {
                            swal.showLoading()
                        }
                        }).then((result) => {
                        if (
                            // Read more about handling dismissals
                            result.dismiss === swal.DismissReason.timer
                        ) {
                            // console.log('I was closed by the timer')
                        }
                        });
                    clipboard.on('error', function(e) {
                        console.error('Accion:', e.action);
                        console.error('Trigger:', e.trigger);
                    });
                }
            }
        });
    </script>
</body>
</html>


