<!DOCTYPE html>
<html lang="@yield('lang', config('app.locale', 'es'))">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>{{ config('app.name', 'AdminLTE') }} | Login</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Styles -->
  @section('styles')
  <link rel="stylesheet" href="{{ mix('/css/admin-lte.css') }}">
  <link rel="stylesheet" href="{{ mix('/css/auth.css') }}">
  
  <style>

 .btn-primary {
    background-color: #DB3762 !important;
    border-color: #A31E41 !important;
    transition: all 0.5s;
}
.btn-primary:hover {
    background-color: #A31E41 !important;
    border-color: #DB3762 !important;
}
.form-control:focus {
    border-color: #DB3762 !important;
}

.hover-dif {
    color: #db376299;
    transition: all 0.3s;
}

.form-control:focus ~ span {
    color: #DB3762 !important;
}

.hover-dif:hover {
    color: #DB3762 !important;
    
    text-shadow: 1px 1px 5px #DB376299;
}

.hover-dif-2 {
    transition: all 0.3s;
}

.hover-dif-2:hover {
    color: #001d56 !important;
    
    text-shadow: 1px 1px 5px #001d5699;
}

#img-logo {
    vertical-align: middle !important;
    width: 100% !important;
}

#img-demo {
    vertical-align: middle !important;
    width: 50% !important;
}


body {
  background: url("{{ asset('images/TxPGrayDIF.png') }}") !important;
  background-size: cover !important;
	background-position: center;
	background-repeat: no-repeat;
  background-color: #fff !important;
  display:flex;
  flex-direction: column;
  min-height: 100vh !important;
}

main {
    flex: 1 0 auto;
  }

footer {
  background-color: black;
  color: white;
}
  </style>
  @show

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  @stack('head')
</head>

<body class="hold-transition login-page">
<header class="main-header" style="background-color: #1a2226;">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="" class="navbar-brand" style="padding-top: 0px;padding-left: 0px;padding-bottom: 0px;padding-right: 0px;"><img id="img-logo" src="@yield('logo-init', asset('images/intra.png'))" alt="DIF"></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars" style="color: #1a2226;"></i>
          </button>
        </div>
      </div>
    </nav>
  </header>
  <main>
  <div class="login-box">
    <div class="login-logo">
      {{--<a href="/"><b>{{ config('app.name', 'AdminLTE') }}</b></a>--}}
      <img id="img-logo" src="{{ asset('images/logo_color.png') }}" alt="DIF">
      @if( env('DB_DATABASE') == 'difoaxaca_demo')
        <img id="img-demo" src="{{ asset('images/demo.png') }}" alt="DIF">
      @endif  
    </div>
    <!-- /.auth-logo -->

    @yield('content')
  </div>
  </main>

  <!-- Scripts -->
  @section('scripts')
  <script src="{{ mix('/js/manifest.js') }}" charset="utf-8"></script>
  <script src="{{ mix('/js/vendor.js') }}" charset="utf-8"></script>
  <script src="{{ mix('/js/auth.js') }}"></script>
  @show
  @stack('body')
  <footer class="main-footer" style="margin-left: 0px;">
    <div class="container">
<!-- To the right -->
<div class="pull-right hidden-xs">
    <a href style="color: #444; font-weight: bold;" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i> Ayuda</a>
  </div>
  <!-- Default to the left -->
  DIF Oaxaca<strong> © Copyright {{ date('Y') }}.</strong>
    </div>
  
</footer>
</body>
</html>
