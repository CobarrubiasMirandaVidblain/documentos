@extends('layouts.app')

@section('content-title', 'Rol')
@section('content-subtitle', 'Usuarios')

@section('li-breadcrumbs')
  <li class="active">Rol Usuarios</li>
@endsection

@section('content')
<div class="row" id="app">

  @if (session('status'))
      <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
          {{ session('msg') }}
      </div>
  @endif

  <div class="col-sm-12">
    <div class="box box-primary shadow">
      <div class="box-header">
        <h3 class="box-title">Inicio</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <!-- <input type="text" name="table_search" class="form-control pull-right" placeholder="Search"> -->

            <div class="input-group-btn">
              <!-- <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button> -->
            </div>
          </div>
        </div>
      </div>
      <div class="box-body table-response no-padding">
        <v-server-table url="/roles/usuarios/vue" :columns="columns" :options="options">
            <span slot="autorizado" slot-scope="props" :class="['label', props.row.autorizado == 1 ? 'label-success' : 'label-warning']">
                @{{props.row.autorizado == 1 ? 'OK' : 'Sin autorización'}}
            </span>
            <button slot="accion" slot-scope="props" :class="['btn', 'btn-danger', 'btn-sm']" @click="eliminar(props.row.id)">
                <i class="fa fa-trash"></i>
            </button>
        </v-server-table>
        {{-- <v-client-table :data="tableData" :columns="columns" :options="options">
            
        </v-client-table> --}}
      </div>
      {{-- <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID</th>
            <th>Usuario</th>
            <th>Módulo</th>
            <th>Rol</th>
            <th>Estatus</th>
            <th>Acción</th>
          </tr>
          @foreach($usersRol as $userRol)
          <tr id="users-{{$userRol->id}}">
            <td>{{ $userRol->id }}</td>
            <td>{{ $userRol->usuario->usuario }}</td>
            <td>{{ $userRol->modulo->nombre }}</td>
            <td>{{ $userRol->rol->nombre }}</td>
            <td>
              @if(auth()->user()->hasRoles(['SUPERADMIN']))
              <button id="auto-{{$userRol->id}}" class="btn {{$userRol->autorizado ? 'bg-teal' : 'bg-orange'}} btn-xs" title="Cambiar" data-toggle="tooltip" @click.prevent="autorizar({{$userRol->id}})">{{ $userRol->autorizado ? 'OK' : 'Sin autorización'}}</button>
              @else
              <button id="auto-{{$userRol->id}}" class="btn {{$userRol->autorizado ? 'bg-teal' : 'bg-orange'}} btn-xs" title="Cambiar" data-toggle="tooltip" @click.prevent="autorizar({{$userRol->id}})" {{$userRol->rol->valor == 1 ? 'disabled' : ''}}>{{ $userRol->autorizado ? 'OK' : 'Sin autorización'}}</button>
              @endif
            </td>
            <td>
              @if(auth()->user()->hasRoles(['SUPERADMIN']))
              <button class="btn btn-danger btn-sm" @click.prevent="eliminar({{$userRol->id}})"><i class="fa fa-trash"></i></button>
              @else
              <button class="btn btn-danger btn-sm" @click.prevent="eliminar({{$userRol->id}})" {{$userRol->rol->valor == 1 ? 'disabled' : ''}}><i class="fa fa-trash"></i></button>
              @endif
            </td>
          </tr>
          @endforeach
        </table>
      </div>
      <div class="box-footer clearfix">
        {{ $usersRol->links() }}
      </div> --}}
    </div>
  </div>
</div>
@endsection


@section('other-scripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.all.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/vue/vue-tables-2.min.js') }}"></script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
Vue.use(VueTables.ServerTable);
Vue.use(VueTables.ClientTable);

var app = new Vue({
  el: '#app',
  data: {
    columns: ['id', 'nombre', 'primerApellido', 'segundoApellido', 'usuarioNombre', 'moduloNombre', 'rolNombre', 'autorizado', 'deleted_at', 'accion'],
    tableData: [],
    options: {
        headings: {usuarioNombre: 'Usuario', moduloNombre: 'Modulo', rolNombre: 'Rol', deleted_at: 'Eliminado'},
        filterByColumn: true,
        filterable:['id', 'nombre', 'primerApellido', 'segundoApellido', 'usuarioNombre', 'moduloNombre', 'rolNombre', 'autorizado', 'deleted_at'],
        pagination: {
            dropdown: false,
            nav: 'scroll'
        }
    }
  },
  mounted(){
    // this.obtenerDatos(1);
  },
  methods: {
    obtenerDatos(pagina = null){
        axios.get('/roles/usuarios/vue').then(response => { 
            this.tableData = response.data.data.map(item => { 
                return {
                    id: item.id, 
                    usuario: item.usuario.usuario,
                    modulo: item.modulo.nombre,
                    rol: item.rol.nombre,
                    estatus: item.autorizado,
                    eliminado: item.deleted_at,
                    autorizado: item.autorizado
                    }
                });
            })
        .catch(error => { console.log(error); });
    },
    eliminar(id){
      axios.delete('/roles/usuarios/' + id).then(response => {
        console.log(response);
        swal({
          type: 'success',
          title: 'Great...',
          text: 'Se elimino correctamente!',
        });
        $(`#users-${id}`).remove();
      }).catch(error => { console.log(error); });
    },
    autorizar(id){
      console.log(`auto-${id}`);
      document.getElementById(`auto-${id}`).disabled = true;
      axios.put('/roles/autorizar/' + id, {}).then(response => {
        swal({
          type: 'success',
          title: 'Great...',
          text: 'Se actualizo correctamente!',
        });
        document.getElementById(`auto-${id}`).disabled = false;
      }).catch(error => {
        swal({
          type: 'error',
          title: 'Oops ...',
          text: 'Ocurrio un error! ' + error,
        });
        document.getElementById(`auto-${id}`).disabled = false;
      });
    }
  }
});
</script>
@endsection