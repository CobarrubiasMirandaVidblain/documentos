@extends('layouts.app')

@section('content-title', 'Rol')
@section('content-subtitle', 'Inicio')

@section('li-breadcrumbs')
  <li class="active">Roles</li>
@endsection

@section('content')
<div class="row">

  @if (session('status'))
      <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
          {{ session('msg') }}
      </div>
  @endif

  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary shadow">
      <div class="box-header">
        <h3 class="box-title">Inicio</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <!-- <input type="text" name="table_search" class="form-control pull-right" placeholder="Search"> -->

            <div class="input-group-btn">
              <!-- <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button> -->
            </div>
          </div>
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Modulo</th>
            <th>Agregado Por:</th>
            <th>Acción</th>
          </tr>
          @foreach($roles as $rol)
          <tr>
            <td>{{ $rol->id }}</td>
            <td>{{ $rol->nombre }}</td>
            <td>{{ $rol->modulo->nombre }}</td>
            <td>{{ $rol->usuario->usuario }}</td>
            <td>
              <a href="{{ route('roles.edit', $rol) }}" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i>
              <a href="{{ route('roles.destroy', $rol) }}" class="btn btn-danger btn-flat" 
                onclick="event.preventDefault(); document.getElementById('delete-form' + {{ $rol->id }}).submit();"><i class="fa fa-trash"></i></a>
              </a>
              <form id="delete-form{{ $rol->id }}" action="{{ route('roles.destroy', $rol) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
            </td>
          </tr>
          @endforeach
        </table>
      </div>
      <div class="box-footer">
        {{ $roles->links() }}
      </div>
    </div>
  </div>
</div>
@endsection

                    