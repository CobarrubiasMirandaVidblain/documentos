@extends('layouts.app')

@push('head')

@endpush

@section('li-breadcrumbs')
  <li class="active">Nuevo Rol</li>
@endsection

@section('content-title', 'Rol')
@section('content-subtitle', 'Nuevo')

@section('content')

@if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
@endif

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary shadow">
        <div class="box-header">
            <h3 class="box-title">{{ isset($rol) ? 'Editar' : 'Crear' }} Rol</h3>
        </div>
        <form role="form" action="{{ isset($rol) ? route('roles.update', $rol) : route('roles.store')}}" method="POST" id="form-vic">
            <div class="box-body">
                {{ csrf_field() }}
                {{ isset($rol) ? method_field('PUT') : ''}}
                <input type="hidden" name="usuario_id" value="{{ Auth::user()->id }}">
                <div class="form-group has-feedback{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="rol">Rol</label>
                    <input type="text" name="nombre" class="form-control" id="rol" placeholder="Rol" required minlength="2" value="{{ $rol->nombre or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('nombre'))
                    <span class="help-block">{{ $errors->first('nombre') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                    <label for="rol">Descripción</label>
                    <input type="text" name="descripcion" class="form-control" id="rol" placeholder="Descripción" required minlength="2" value="{{ $rol->descripcion or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('descripcion'))
                    <span class="help-block">{{ $errors->first('descripcion') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('modulo_id') ? ' has-error' : '' }}">
                    <label for="modulo">Modulo</label>
                    <select class="form-control" style="width: 100%;" name="modulo_id">
                        @foreach($modulos as $modulo)
                        <option value="{{$modulo->id}}">{{$modulo->nombre}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('modulo_id'))
                    <span class="help-block">{{ $errors->first('modulo_id') }}</span>
                    @endif
                </div>

                <div class="checkbox">
                    <label for="valor">
                        <input type="checkbox" id="valor" name="valor">Es Admin ?
                    </label>
                    @if ($errors->has('valor'))
                    <span class="help-block">{{ $errors->first('valor') }}</span>
                    @endif
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{ isset($rol) ? 'Guardar Cambios' : 'Guardar' }}</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('other-scripts')
<script>
$('#form-vic').validator();
</script>
@endsection