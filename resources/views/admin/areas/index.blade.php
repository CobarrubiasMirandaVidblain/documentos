@extends('layouts.app')

@push('head')

@endpush

@section('content-title', 'Áreas')
@section('content-subtitle',  'Áreas Responsables' )

@section('content')
  <div id="app">
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <label for="">Ingresa nombre del area o responsable</label>
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Buscar área" v-model="nombreArea">
            <span class="input-group-addon"><i :class="['fa', buscando ? 'fa-spinner' : 'fa-check']"></i></span>
          </div>
        </div>
      </div>
    </div>
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Responsable</th>
          <th>Deleted_at</th>
          <th>Editar</th>
          <th>Eliminar</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="area in areas">
          <td>@{{area.id}}</td>
          <td>@{{area.area.nombre}}</td>
          <td>@{{area.empleado.persona.nombre}} @{{area.empleado.persona.primer_apellido}} @{{area.empleado.persona.segundo_apellido}}</td>
          <td>@{{area.deleted_at}}</td>
          <td><button type="button" @click.prevent="editar(area)" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></button></td>
          <td><button type="button" @click.prevent="eliminar(area)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button></td>
        </tr>
      </tbody>
    </table>

    <div class="modal modal-primary fade" id="modal-primary">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Editar Area</h4>
          </div>
          <form class="form-horizontal">
            <div class="modal-body">
              
              <div class="form-group">
                <label for="id" class="col-sm-3 control-label text-primary">ID</label>
                <div class="col-sm-9">
                  <input type="number" class="form-control" id="id" disabled>
                </div>
              </div>

              <div class="form-group">
                <label for="producto" class="col-sm-3 control-label text-primary">Producto</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="producto">
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
              <button type="button" class="btn btn-outline btn-outline-primary" :disabled="updateButton" @click="updateProduct(editProduct)"><i class="fa fa-check"></i> Guardar Cambios</button>
            </div>
          </form>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
        
  </div>
@endsection

@section('other-scripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script>
var app = new Vue({
  el: '#app',
  data: {
    areas: [],
    nombreArea: null,
    buscando: false
  },
  mounted() {
    axios.get('/areas/general').then(response => { console.log(response); this.areas = response.data.data; }).catch(error => { console.log(error); });
  },
  watch: {
    nombreArea(val) {
      if (val.length < 4) return;
      console.log(val);
      this.obtenerAreas(val);
    }
  },
  methods: {
    obtenerAreas(val) {
      this.buscando = true;
      axios.get(`/areas/${val}`).then(response => { this.areas = response.data.data; this.buscando = false; }).catch(error => { swal('Oops!', 'Ocurrio un erro' + error, 'error'); this.buscando = false; });
    },
    editar(area) {
      $('#modal-primary').modal('show');
    },
    eliminar(area) {

    }
  }
});
</script>
@endsection