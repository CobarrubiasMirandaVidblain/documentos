@extends('layouts.app')

@push('head')

@endpush

@section('content-title', 'Módulo')
@section('content-subtitle',  isset($module) ? 'Editar' : 'Nuevo' )

@section('li-breadcrumbs')
  <li class="active">{{ isset($module) ? 'Editar Módulo' : 'Nuevo Módulo' }}</li>
@endsection

@section('content')

@if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
@endif

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary shadow">
        <div class="box-header">
            <h3 class="box-title">{{ isset($module) ? 'Editar' : 'Crear' }} Módulo</h3>
        </div>
        <form role="form" action="{{ isset($module) ? route('modules.update', $module) : route('modules.store')}}" method="POST" id="form-vic">
            <div class="box-body">
                {{ csrf_field() }}
                {{ isset($module) ? method_field('PUT') : ''}}
                <input type="hidden" name="usuario_id" value="{{ Auth::user()->id }}">
                <div class="form-group has-feedback{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="rol">Módulo</label>
                    <input type="text" name="nombre" class="form-control" id="rol" placeholder="Modulo" required minlength="2" value="{{ $module->nombre or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('nombre'))
                    <span class="help-block">{{ $errors->first('nombre') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('descripcion') ? ' has-error' : '' }}">
                    <label for="rol">Descripción</label>
                    <input type="text" name="descripcion" class="form-control" id="rol" placeholder="Descrpción del sistema" required minlength="2" value="{{ $module->descripcion or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('descripcion'))
                    <span class="help-block">{{ $errors->first('descripcion') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('prefix') ? ' has-error' : '' }}">
                    <label for="rol">Prefijo Url</label>
                    <input type="text" name="prefix" class="form-control" id="rol" placeholder="Descrpción del sistema" required minlength="2" value="{{ $module->prefix or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('prefix'))
                    <span class="help-block">{{ $errors->first('prefix') }}</span>
                    @endif
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{ isset($module) ? 'Guardar Cambios' : 'Guardar' }}</button>
            </div>
        </form>
    </div>
  </div>
</div>
<div class="row">
      <div class="col-xs-12" style="text-align: center;">
      <a href="{{ route('modules.index') }}" class="text-center" style="font-size: 15px;color: #db3762;"><i class="fa fa-list"></i> Lista de Modulos</a>
      </div>
      <!-- /.col -->
    </div>
@endsection

@section('other-scripts')
<script>
$('#form-vic').validator();
</script>
@endsection