@extends('layouts.app')

@push('head')

@endpush

@section('content-title', 'Módulo')
@section('content-subtitle',  'Solicitar Acceso' )

@section('content')

@if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
@endif

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Solicitar Acceso a Módulo</h3>
        </div>
        <form role="form" action="{{ route('modules.access')}}" method="POST" id="form-vic">
            <div class="box-body">
                {{ csrf_field() }}
                <input type="hidden" name="usuario_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="responsable" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <label>Módulo</label>
                    <select class="form-control select2" style="width: 100%;" required name="modulo_id">
                    <option disabled>Seleccione el módulo donde desea entrar</option>
                    @foreach($modules as $module)
                        <option value="{{ $module->id }}">{{ $module->nombre }}</option>
                    @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Rol</label>
                    <select class="form-control select2" style="width: 100%;" required name="rol_id">
                    <option disabled>Seleccione su rol que tendrá</option>
                    @foreach($roles as $rol)
                        <option value="{{ $rol->id }}">{{ $rol->nombre }}</option>
                    @endforeach
                    </select>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" required> Se le notificará a su Jefe Inmediato de esta acción
                    </label>
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Solicitar Acceso</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('other-scripts')
<script>
$('#form-vic').validator();
</script>
@endsection