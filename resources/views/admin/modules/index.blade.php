@extends('layouts.app')

@section('content-title', 'Modulos')
@section('content-subtitle', 'Lista')

@section('li-breadcrumbs')
  <li class="active">Módulos</li>
@endsection

@section('content')
<div class="row">

  @if (session('status'))
      <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
          {{ session('msg') }}
      </div>
  @endif

  <div class="col-md-12">
    <div class="box box-primary shadow">
      <div class="box-header">
        <h3 class="box-title">Lista de Módulos</h3>
        <div class="box-tools pull-right">
              <a type="button" class="btn btn-box-tool" href="{{ route('modules.create') }}" style="color: #d12654;"><i class="fa fa-fw fa-plus-square"></i> Agregar Módulo</a>
          </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Agregado Por:</th>
            <th>Editar</th>
            <th>Eliminar</th>
          </tr>
          @foreach($modules as $module)
          <tr>
            <td>{{ $module->id }}</td>
            <td>{{ $module->nombre }}</td>
            <td>{{ $module->descripcion }}</td>
            <td>{{ $module->usuario->usuario }}</td>
            <td>
              <a href="{{ route('modules.edit', $module) }}" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i>
            </td>
            <td>
              <a href="{{ route('modules.destroy', $module) }}" class="btn btn-danger btn-flat" 
                onclick="event.preventDefault(); document.getElementById('delete-form' + {{ $module->id }}).submit();"><i class="fa fa-trash"></i></a>
              </a>
              <form id="delete-form{{ $module->id }}" action="{{ route('modules.destroy', $module) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
            </td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

                    