@extends("layouts.app")

@push('head')
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
@endpush
@section('content-title', 'INVERSIÓN PROGRAMAS')
@section('content-subtitle', '')

@section('content')

<div class="box box-primary shadow">   

	<div class="box-header with-border">
		<h3 class="box-title">Datos del municipio</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="table-responsive">
					<table class="table">
						<tr>
							<th style="width:30%">
								<i class="fa fa-id-card margin-r-5"></i>
								Municipio:
							</th>
							<td>{{ $municipio->nombre }}</td>
						</tr>
						<tr>
							<th>
								<i class="fa fa-calendar margin-r-5"></i>
								Distrito:
							</th>
							<td>{{ $municipio->distrito->nombre }}</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="table-responsive">
					<table class="table">
						<tr>
							<th style="width:30%; color: white;">
								No visible
							</th>
							<td  style="color: white;">No visible</td>
						</tr>
						<tr>
							<th>
								<i class="fa fa-university margin-r-5"></i>
								Región:
							</th>
							<td>{{ $municipio->distrito->region->nombre }}</td>
						</tr>                    
					</table>
				</div>
			</div>
		</div>

		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>
		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'inversiones', 'name' => 'inversiones', 'style' => 'width: 100%']) !!}
	</div>
</div>
@stop

@push('body')

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">

	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#inversiones').dataTable());
	table.init();	
</script>
@endpush