<div class="nav-tabs-custom" id="tabs">
    <ul class="nav nav-tabs">
    <li class="active"><a href="#evento">Evento</a></li>
    <li><a href="#areas">Áreas participantes</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="evento">
            <form data-toggle="validator" role="form" id="form_evento">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">
                        <div class="form-group">
                            <label for="nombre">Nombre de la gira o del evento* :</label>
                            <select id="nombre" name="nombre" class="form-control select2" style="width: 100%;">
                                @if(isset($evento))
                                    <option value="{{ $evento->gira->nombre }}" selected>{{ $evento->gira->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="fecha">Fecha* :</label>
                            <input type="text" class="form-control" id="fecha" name="fecha" autocomplete="off" value="{{ isset($evento) ? $evento->get_formato_fecha() : '' }}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="calle">Calle* :</label>
                            <input type="text" class="form-control" id="calle" name="calle" autocomplete="off" value="{{ isset($evento) ? $evento->calle : '' }}">
                        </div>
                    </div>                 
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="municipio_id">Municipio* :</label>
                            <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
                                @if(isset($evento) && isset($evento->municipio))
                                <option value="{{ $evento->municipio->id }}" selected>({{ $evento->municipio->distrito->region->nombre }}) ({{ $evento->municipio->distrito->nombre }}) {{ $evento->municipio->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="localidad_id">Localidad:</label>
                            <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;">
                                @if(isset($evento) && isset($evento->cat_localidad))
                                    <option value="{{ $evento->cat_localidad->id }}" selected>{{ $evento->cat_localidad->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="localidad">Otra localidad:</label>
                            <input type="text" id="localidad" name="localidad" class="form-control" value="{{ isset($evento) ? $evento->localidad : '' }}">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="areas">
            <form data-toggle="validator" role="form" id="form_area">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="area_id">Área* :</label>
                            <select id="area_id" name="area_id" class="form-control select2" style="width: 100%;"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="observaciones">Observación:</label>
                            <textarea id="observaciones" name="observaciones" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group" style="margin-top: 20px;">
                            <label for="" style="color:white">Agregar:</label>
                            <button id="btnAgregar" type="button" class="btn btn-block btn-info" onclick="Evento.agregar_area()">
                                Agregar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table id="tblAreas" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center">Área</th>
                                <th class="text-center">Observación</th>
                                <th class="text-center">Editar</th>
                                <th class="text-center">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>