@extends('bienestar::layouts.master')

@push('head')
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">CONSULTAR PERSONA</h3>
		<!--<a href="{{ url('empleados') . '/' . $persona->id }} " class="btn btn-success"><i class="fa fa-reply"></i> Empleado</a>-->
	</div>
	<div class="box-body">
		@include('personas.iframe.show')
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<div class="pull-right">
			<a href="{{ route('personas.edit', $persona->id) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Editar</a>
			<a href="{{ route('personas.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
		</div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript">
</script>
@endpush