@extends("layouts.app")

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
    <style>
        .small-box {
            cursor: pointer;
        }

        #btnEjercicio:hover, .button-dt.tool.dropdown-toggle:hover a{
            color: white !important;
        }

        ul#menuEjercicio a {
            color: black !important;
        }
    </style>
@endpush

@section('content-title', auth()->user()->persona->empleado->area->nombre)
@section('content-subtitle', 'Peticiones')

@section('li-breadcrumbs')
    <li class="active">Peticiones</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-green" onclick="Peticion.filtrar_peticiones(8,'{{ auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES']) ? 'NUEVO' : 'Vo. Bo.' }}' )">
                    <div class="inner">
                        <h3 id="peticiones_nuevas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones nuevas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-edit"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-yellow" onclick="Peticion.filtrar_peticiones(8, 'VALIDANDO')">
                    <div class="inner">
                        <h3 id="peticiones_proceso"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones en proceso</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-aqua" onclick="Peticion.filtrar_peticiones(8, 'FINALIZADO')">
                    <div class="inner">
                        <h3 id="peticiones_finalizadas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones finalizadas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-red" onclick="Peticion.filtrar_peticiones(8, 'CANCELADO')">
                    <div class="inner">
                        <h3 id="peticiones_canceladas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones canceladas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-minus-square"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
    	    <div class="col-xs-12">
    		    <div class="box box-primary shadow">
                    <div class="box-header with-border">
		                <h3 class="box-title">Lista de peticiones</h3>
                    </div>
				    <div class="box-body">
                        {{--  <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                            <input type="text" id="buscar" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                            </span>
                        </div>  --}}
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'peticiones', 'name' => 'peticiones', 'style' => 'width: 100%']) !!}
				    </div>
    		    </div>
    	    </div>
        </div>
    </section>    
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>

{!! $dataTable->scripts() !!}

<script type="text/javascript">

    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    
    function block() {
        baseZ = 1030;        
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: baseZ,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}

		setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
    
    var Peticion = (() => {

        //DataTable de peticiones
        var peticiones;
        var ejercicio;

        //Inicializa DataTable
        var inicializar = () => {
            peticiones = $('#peticiones').DataTable();
            peticiones.on( 'xhr', function () {
                var data = peticiones.ajax.json();
                if(data != undefined){
                  $("#peticiones_nuevas").text( data.peticiones_nuevas );
                  $("#peticiones_proceso").text( data.peticiones_proceso );
                  $("#peticiones_finalizadas").text( data.peticiones_finalizadas );
                  $("#peticiones_canceladas").text( data.peticiones_canceladas );
                }                
            });

            $('#buscar').keypress(function(e) {
			    if(e.which === 13) {
				    peticiones.search($('#buscar').val()).draw();
			    }
		    });

		    $('#btn_buscar').on('click', function() {
			    peticiones.search($('#buscar').val()).draw();
            });

            // filtrar_peticiones(8, 'NUEVO');
        }
        

        var cerrar_modal = (modal) => {
            $("#"+modal).modal('hide');
        }

        //La columna del DataTable, recuerden que empieza con el indice 0 y se tambien cuentan las columnas no visibles
        //El valor a ser buscado en esa columna
        var filtrar_peticiones = (columna, valor) => {
            $('#peticiones').DataTable().columns(columna).search(valor).draw();
        }

        var autorizar = (peticion_id) => {
            block();
            $.ajax({
                url: window.location.href + '/' + peticion_id,				
			    type: "PUT",
			    data: {estatus: 9},
			    success: function(response) {                  
                    $.get("{{ route('peticiones.cantidad') }}",(data) => {
                        if(data > 0){
                            $('#lbl_Peticiones_Nuevas_Director').text(data);
                            $('#lbl_Peticiones_Nuevas_Director').parent().removeClass('hidden');
                        }else{
                            $('#lbl_Peticiones_Nuevas_Director').parent().addClass('hidden');
                        }
                        peticiones.ajax.reload(null, false);
                        unblock();
                    });
                },
                error: function(response){
                    unblock();              
                }
		    });
        }

        return {
            inicializar,
            filtrar_peticiones,
            autorizar
        }

    })();

    $(document).ready(function() {
        Peticion.inicializar();
    });
    
</script>
@endpush