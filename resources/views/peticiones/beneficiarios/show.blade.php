<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 0px 50px 0px 50px;">
        <div class="modal-header" style="padding: 0px;">
            <div class="row">
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="background-color: #D12654; padding-left: 0px;">
                <img class="img-thumbnail" style="border-radius: 0px; height: 137px; padding: 0 !important; border: 0px !important;" src="{{ asset($persona->get_url_fotografia()) }}">
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-right: 0px; margin-top: -10px; padding-left: 0px;">             
                    <div class="text-center" style="background-color: #D12654; border-radius: 0px 50px 0px 0px;">                        
                        <h4 style="background-color: #D12654; color: white;padding-top: 20px;margin-top: 10px; border-radius: 0px 50px 0px 0px; margin-left: 20px;">
                            <b>{{ $persona->nombreCompleto() }}</b>
                            <button type="button" data-dismiss="modal" style="background-color: #D12654;border-left-width: 0px;border-left-style: solid;border-right-width: 0px;border-right-style: solid;border-top-width: 0px;border-top-style: solid;border-bottom-width: 0px;border-bottom-style: solid;margin-right: 20px;margin-left: -20px;" class="pull-right">
                            <i class="fa fa-close"></i>
                        </button>
                        </h4>
                        <ul id="navFicha" class="nav navbar-nav" style="float: none; display: inline-block;">
                            <li class="active text-center" style="padding: 2px 5px;">
                                <a href="#tab_Datos" data-toggle="tab" style="color: #f9f8f8;">
                                    <i class="fa fa-user fa-2x"></i>
                                    <br> Datos Generales
                                </a>
                            </li>
                        @if($programa_padre === 'APOYOS FUNCIONALES')
                            <li class="text-center" style="padding: 2px 5px;">
                                <a href="#tab_Discapacidad" data-toggle="tab" style="color: #f9f8f8;">
                                    <i class="fa fa-wheelchair fa-2x"></i>
                                    <br> Discapacidad
                                </a>
                              </li>
                        @endif
                            <li class="text-center" style="padding: 2px 5px;">
                                <a href="#tab_Expediente" data-toggle="tab" style="color: #f9f8f8;">
                                    <i class="fa fa-folder fa-2x"></i>
                                    <br> Expediente
                                </a>
                            </li>
                            <li class="text-center" style="padding: 2px 5px;">
                                <a href="#tab_Historial" data-toggle="tab" style="color: #f9f8f8;">
                                    <i class="fa fa-list fa-2x"></i>
                                    <br> Historial de apoyos
                                </a>
                            </li>                                    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="tab-content" style="height: 350px">
                <div class="tab-pane active" id="tab_Datos">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <form class="form-horizontal">
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Nombre:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">{{ $persona->nombreCompleto() }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Género:</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">{{ $persona->genero == 'F' ? 'FEMENINO' : 'MASCULINO' }}</p>
                                    </div>
                                    <label class="col-sm-3 control-label" style="text-align: left;">Fecha de nacimiento:</label>
                                    <div class="col-sm-3">
                                        <p class="form-control-static">{{ isset($persona->fecha_nacimiento) ? $persona->get_formato_fecha_nacimiento() : '' }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">CURP:</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static" id="curpBeneficiario" data-persona-id="{{ $persona->id }}">{{ $persona->curp }}</p>
                                    </div>
                                    <label class="col-sm-3 control-label" style="text-align: left;">Clave electoral:</label>
                                    <div class="col-sm-3">
                                        <p class="form-control-static">{{ $persona->clave_electoral }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Etnia:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">{{ $persona->etnia->nombre }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Teléfono:</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">{{ $persona->numero_local }}</p>
                                    </div>
                                    <label class="col-sm-3 control-label" style="text-align: left;">Celular:</label>
                                    <div class="col-sm-3">
                                        <p class="form-control-static">{{ $persona->numero_celular }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Correo electrónico:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">{{ (isset($persona->email) ? $persona->email : '') }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Domicilio:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">{{ $persona->calle . ' ' . $persona->numero_exterior . ((isset($persona->numero_interior) && $persona->numero_interior != '') ? '-'.$persona->numero_interior : '') }}
                                            @if($persona->colonia && $persona->colonia != '')
                                                <br> {{ $persona->colonia }}
                                            @endif

                                            @if($persona->localidad || $persona->municipio)
                                                <br> 
                                            @endif
                                            {{ (isset($persona->localidad) ? $persona->localidad->nombre.', ' : '') . ' ' . $persona->municipio->nombre . (isset($persona->codigopostal) ? ', C.P. ' . $persona->codigopostal : '') }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label" style="text-align: left;">Referencias del domicilio:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">{{ (isset($persona->referencia_domicilio) ? $persona->referencia_domicilio : '') }}</p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>            
                </div>
            @if($programa_padre === 'APOYOS FUNCIONALES')
                <div class="tab-pane disabled" id="tab_Discapacidad">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <form class="form-horizontal">
                                <div class="row">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Discapacidad:</label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">{{ $persona->afuncionalespersona->discapacidad->nombre or '' }}</p>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Tipo de limitación en la actividad:</label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">{{ $persona->afuncionalespersona->limitacion->nombre or '' }}</p>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Tiempo con discapacidad:</label>
                                    <div class="col-sm-8">
                                        <p class="form-control-static">{{ isset($persona->afuncionalespersona) ? $persona->afuncionalespersona->tiempodiscapacidad . ' año(s)' : '' }}</p>
                                    </div>
                                </div>
                                @if( isset($peticion) && strpos($peticion->beneficio->nombre, "SILLA") !== false)
                                <div class="row">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Altura:</label>
                                    <div class="col-sm-3">
                                        <p class="form-control-static">{{ isset($persona->afuncionalespersona->afuncionalessilla) ? $persona->afuncionalespersona->afuncionalessilla->altura . ' metros' : '' }}</p>
                                    </div>
                                    <label class="col-sm-3 control-label" style="text-align: left;">Peso:</label>
                                    <div class="col-sm-2">
                                        <p class="form-control-static">{{ isset($persona->afuncionalespersona->afuncionalessilla) ? $persona->afuncionalespersona->afuncionalessilla->peso . ' kg' : '' }}</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-4 control-label" style="text-align: left;">Tipo de silla de ruedas:</label>
                                    <div class="col-sm-3">
                                        <p class="form-control-static">{{ $persona->afuncionalespersona->afuncionalessilla->tiposilla or '' }}</p>
                                    </div>
                                    <label class="col-sm-3 control-label" style="text-align: left;">Tipo de terreno:</label>
                                    <div class="col-sm-2">
                                        <p class="form-control-static">{{ $persona->afuncionalespersona->afuncionalessilla->terreno or '' }}</p>
                                    </div>
                                </div>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            @endif
                <div class="tab-pane disabled" id="tab_Expediente">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <form role="form">
                                <div class="box-body">
                                    @if(isset($peticion))
                                    @php
                                        $anioprograma = $peticion->beneficio->anioprograma;
                                    @endphp
                                        @for($i = 0; $i < $anioprograma->documentosprogramas->count(); $i++)
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                    <div class="form-group">
                                                        <h5>
                                                            @php
                                                                $documentosPersonas = $anioprograma->documentosprogramas[$i]->documentospersonas;
                                                            @endphp
                                                            @if($documentosPersonas->where('persona_id',$persona->id)->count() > 0 && $documentosPersonas->where('persona_id',$persona->id)->first()->documentossolicitudes->contains('beneficio_solicitud_id', $peticion->id))
                                                                <i class="icon fa fa-check"></i>
                                                            @else
                                                                <i class="icon fa fa-close"></i>
                                                            @endif
                                                            {{ $anioprograma->documentosprogramas[$i]->documento->nombre }}
                                                        </h5>
                                                    </div>
                                                </div>
                                                
                                                @if(($i+1) < $anioprograma->documentosprogramas->count())
                                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                        <div class="form-group">
                                                            <h5>    
                                                                @if($anioprograma->documentosprogramas[$i+1]->documentospersonas->where('persona_id',$persona->id)->count() > 0 && $anioprograma->documentosprogramas[$i+1]->documentospersonas->where('persona_id',$persona->id)->first()->documentossolicitudes->contains('beneficio_solicitud_id', $peticion->id))
                                                                    <i class="icon fa fa-check"></i>      
                                                                @else
                                                                    <i class="icon fa fa-close"></i>
                                                                @endif                                    
                                                                {{ $anioprograma->documentosprogramas[$i+1]->documento->nombre }}      
                                                            </h5>
                                                        </div>
                                                    </div>
                                                    <?php $i = $i+1; ?>                                        
                                                @endif  
                                            </div>                              
                                        @endfor
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="tab-pane disabled" id="tab_Historial">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <table id="apoyos-otorgados" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">PETICIÓN</th>
                                        <th class="text-center">FECHA</th>
                                        <th class="text-center">BENEFICIO</th>
                                        <th class="text-center">PRODUCTO</th>
                                    </tr>   
                                </thead>
                                <tbody>
                                    @foreach($persona->beneficiosPersonas->where("entregado",1) as $beneficioOtorgado)
                                        <tr>
                                            <td class="text-center">{{ $beneficioOtorgado->programas_solicitud->solicitud->folio }}</td>
                                            <td class="text-center">
                                                @if($beneficioOtorgado->salida_producto)
                                                    {{ $beneficioOtorgado->salida_producto->get_formato_fecha() }}
                                                @else
                                                    {{ $beneficioOtorgado->get_fecha_updated() }}
                                                @endif
                                            </td>
                                            <td class="text-center">{{ $beneficioOtorgado->programas_solicitud->programa->nombre }}</td>
                                            <td class="text-center">
                                                @if($beneficioOtorgado->salida_producto)
                                                    {{ $beneficioOtorgado->salida_producto->detallesalidasproductos->first()->areasproducto->producto->producto }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="border-top-color: #cd343400" id="modal-footer-beneficiario">
            <button type="button" style="border-radius: 24px; display: none;" class="btn btn-warning" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>
            <button type="button" style="border-radius: 24px;" class="btn btn-success" onclick="Beneficiarios.guardar({{ $persona->id }})"><i class="fa fa-database"></i> Aceptar</button>
        </div>
    </div>
</div>