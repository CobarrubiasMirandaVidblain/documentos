<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <form role="form" id="form-persona-expediente">
    <div class="box-body">
      @php
      $anioprograma = $peticion->beneficio->anioprograma;//->where('ejercicio.anio', $peticion->getAnio())->first();
      @endphp
      @for($i = 0; $i < $anioprograma->documentosprogramas->count(); $i++)
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
              <label>
                @if($anioprograma->documentosprogramas[$i]->documentospersonas->where('persona_id',$persona->id)->count()
                > 0 &&
                $anioprograma->documentosprogramas[$i]->documentospersonas->where('persona_id',$persona->id)->first()->documentossolicitudes->contains('beneficio_solicitud_id',$peticion->id))
                <input type="checkbox" name="documentos_programas[]"
                  value="{{ $anioprograma->documentosprogramas[$i]->id }} " class="flat-red" checked>
                @else
                <input type="checkbox" name="documentos_programas[]"
                  value="{{ $anioprograma->documentosprogramas[$i]->id }} " class="flat-red">
                @endif

                {{ $anioprograma->documentosprogramas[$i]->documento->nombre }}
              </label>
            </div>
          </div>

          @if(($i+1) < $anioprograma->documentosprogramas->count())
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">
                <label>

                  @if($anioprograma->documentosprogramas[$i+1]->documentospersonas->where('persona_id',$persona->id)->count()
                  > 0 &&
                  $anioprograma->documentosprogramas[$i+1]->documentospersonas->where('persona_id',$persona->id)->first()->documentossolicitudes->contains('beneficio_solicitud_id',$peticion->id))
                  <input type="checkbox" name="documentos_programas[]"
                    value="{{ $anioprograma->documentosprogramas[$i+1]->id }} " class="flat-red" checked>
                  @else
                  <input type="checkbox" name="documentos_programas[]"
                    value="{{ $anioprograma->documentosprogramas[$i+1]->id }} " class="flat-red">
                  @endif

                  {{ $anioprograma->documentosprogramas[$i+1]->documento->nombre }}
                </label>
              </div>
            </div>
            <?php $i = $i+1; ?>
            @endif
        </div>
        @endfor
    </div>
  </form>
</div>