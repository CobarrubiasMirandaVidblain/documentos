<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form class="form-horizontal">
            <div class="row">
                <label class="col-sm-2 control-label" style="text-align: left;">Nombre:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $programa->nombre }}</p>
                </div>
            </div>

            <div class="row">
                <label class="col-sm-2 control-label" style="text-align: left;">Descripción:</label>
                <div class="col-sm-10">
                    <p class="form-control-static">{{ $programa->descripcion }}</p>
                </div>
            </div>

            <div class="row">
                <label class="col-sm-3 control-label" style="text-align: left;">Fecha de inicio:</label>
                <div class="col-sm-3">
                    <p class="form-control-static">{{ $programa->get_formato_fecha_inicio() }}</p>
                </div>
                <label class="col-sm-3 control-label" style="text-align: left;">Fecha de término:</label>
                <div class="col-sm-3">
                    <p class="form-control-static">{{ $programa->get_formato_fecha_inicio() }}</p>
                </div>
            </div>

            <div class="row">
                <label class="col-sm-3 control-label" style="text-align: left;">Tipo:</label>
                <div class="col-sm-3">
                    <p class="form-control-static">{{ $programa->tipo }}</p>
                </div>
                <label class="col-sm-3 control-label" style="text-align: left;">Oficial:</label>
                <div class="col-sm-3">
                    <p class="form-control-static">{{ $programa->oficial ? 'SÍ' : 'NO' }}</p>
                </div>
            </div>            
        </form>
    </div>
</div>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Documentación</h3>            
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="documentos-programa" class="table table-striped table-bordered dt-responsive nowrap table-condensed" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">Ejercicio</th>
                            <th class="text-center">Documento</th>
                            <th class="text-center">Descripcición</th>
                        </tr>   
                    </thead>
                    <tbody>
                        @foreach($programa->aniosprogramas as $anioprograma)
                            @foreach($anioprograma->documentosprogramas as $documentoprograma)
                                <tr>
                                    <td class="text-center">{{ $anioprograma->ejercicio->anio }}</td>
                                    <td class="text-center">{{ $documentoprograma->documento->nombre }}</td>
                                    <td class="text-center">{{ $documentoprograma->documento->descripcion }}</td>                            
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>    
    </div>
</div>