<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h3 class="modal-title" id="modal-detalle-title">ASIGNAR AREA A {{ $programa->nombre }}</h3>
</div>
<div class="modal-body" id="modal-body-detalle">
  <label for="area_id">Seleccione el area</label>
  <select name="area_id" id="area_id" style="width:100%" required>
    <option></option>
  </select>  
</div>
<div class="modal-footer" id="modal-footer-detalle">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-primary" onClick="asignarArea()">
    <i class="fa fa-save"></i> Guardar
  </button>
</div>
<script>
  $("#area_id").select2({
    language: 'es',
    ajax: {
      delay: 500,
      url: '{{ route('areas.select') }}',
      dataType: 'JSON',
      type: 'GET',
      data: function(params) {
        return {
          search: params.term,
          padre_id: {{ Auth::User()->persona->empleado->area_id }}
        };
      },
      processResults: function(data, params) {                    
        params.page = params.page || 1;
        return {
          results: $.map(data, function(item) {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        };
      },
      cache: true
    }
  });
  function asignarArea(area_id) {
    $.post( '{{ route("programas.area.store",$programa->id) }}', { area_id: $('#area_id').val()})
    .done(function( data ) {
      swal('Guardado','se vinculo el programa al area','success')
      window.LaravelDataTables.programas.ajax.reload();
      $("#modal-area").modal('hide');
    })
    .fail(function( data ) {
      swal('Error','ocurrio un problema','error')
    });
  } 
</script>