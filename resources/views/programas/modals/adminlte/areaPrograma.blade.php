@php
  Carbon\Carbon::setLocale('es'); 
  $date=Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$areaPrograma->created_at);
@endphp
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <h3 class="modal-title" id="modal-detalle-title">AREA VINCULADA A {{ $areaPrograma->programa->nombre }}</h3>
  </div>
  <div class="modal-body" id="modal-body-detalle" style="text-transform:uppercase">
    <p>
      SE VINCULO EL PROGRAMA <Strong>{{ $areaPrograma->programa->nombre }}</Strong> AL AREA <Strong>{{ $areaPrograma->area->nombre }}</Strong>
      <Strong>{{ $date->diffForHumans() }}</Strong> POR <Strong>{{ $areaPrograma->usuario->persona->obtenerNombreCompleto() }}</Strong>
    </p>
    <button class="btn btn-sm btn-warning" style="width:100%;" data-toggle="modal" onClick="abrirModal('area','/programas/{{ $areaPrograma->programa_id }}/area/create')">CAMBIAR AREA</button>
  </div>
  <div class="modal-footer" id="modal-footer-detalle">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  </div>
  <script>
  </script>