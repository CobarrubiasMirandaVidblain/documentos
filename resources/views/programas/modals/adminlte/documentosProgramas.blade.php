<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h3 class="modal-title">DOCUMENTOS REQUERIDOS PARA <strong>{{ $programa->nombre }}</strong></h3>
</div>
<div class="modal-body">
  {!! $dataTable->table(['class' => 'table table-sm table-bordered table-striped dt-responsive nowrap', 'style'=>'width:100%', 'id' => 'documentos']) !!}
</div>
  {!! $dataTable->scripts() !!}
  <script>
    (function ($,DataTable){
      DataTable.ext.buttons.new = {
        className: '',
        text: function (dt) {
          return  '<i class="fa fa-plus"></i> Agregar'; //+ dt.i18n('buttons.print', 'Print');
        },
        action: function (e,dt,button,config) {
          var url = "{{route('programas.documentos.create',$programa->id)}}";
          abrirModal('ceDocumento',url);
          //alert('click');
        }
      };
    })(jQuery, jQuery.fn.dataTable);
    function eliminarDocumento(documento_id){
      $.ajax({
        url : '{{ route("programas.documentos.index",$programa->id) }}'+`/${documento_id}`,
        method : 'delete',
        success : function name(response) {
          window.LaravelDataTables.documentos.ajax.reload();
          swal('Exito','eliminado','success')
        },
        error : function (response) {
          swal('ERROR','no se elimino','error')
        }
      })
    }
  </script>