<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h3 class="modal-title" id="modal-detalle-title">ASIGNAR DOCUMENTO A <strong>{{ $programa->nombre }}</strong></h3>
</div>
<div class="modal-body" id="modal-body-detalle">
  <label for="area_id">Seleccione el documento</label>
  <select name="documento_id" id="documento_id" style="width:100%" required>
    <option></option>
  </select>  
</div>
<div class="modal-footer" id="modal-footer-detalle">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-primary" onClick="asignarDocumento()">
    <i class="fa fa-save"></i> Guardar
  </button>
</div>
<script>
  $("#documento_id").select2({
    language: 'es',
    tags : true,
    ajax: {
      delay: 500,
      url: '{{ route('documentos.select') }}',
      dataType: 'JSON',
      type: 'GET',
      data: function(params) {
        return {
          search: params.term
        };
      },
      processResults: function(data, params) {                    
        params.page = params.page || 1;
        return {
          results: $.map(data, function(item) {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        };
      },
      cache: true
    },
  });
  function asignarDocumento(area_id) {
    $.post( '{{ route("programas.documentos.store",$programa->id) }}', { documento_id: $('#documento_id').val()})
    .done(function( data ) {
      swal('Guardado','se agrego el documento','success')
      window.LaravelDataTables.documentos.ajax.reload();
      $("#modal-ceDocumento").modal('hide');
    })
    .fail(function( data ) {
      swal('Error','ocurrio un problema','error')
    });
  } 
</script>