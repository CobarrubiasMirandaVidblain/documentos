@extends("$modulo::layouts.master")

<?php $ruta = \Request::route()->getName(); ?>

@push('head')
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
  <style>
    body {
      padding-right: 0 !important;
    }
    .select2-selection__rendered {
        text-transform: uppercase
    }
  </style>
@endpush


@if($ruta == "programas.externos")
    @section('content-title', 'COORDINACIÓN DE ATENCIÓN CIUDADANA Y VINCULACIÓN SOCIAL')
@elseif($programa_padre != "")
  @section('content-title', $programa_padre)
@else
  @section('content-title', auth()->user()->persona->empleado->area->nombre)
@endif

@if($ruta == "programas.externos")
    @section('content-subtitle', 'Beneficios Externos')
@else
    @section('content-subtitle', 'Beneficios')
@endif

@section('content')
<div class="box box-primary shadow">
	<div class="box-header with-border">
		<h3 class="box-title">Beneficios</h3>
	</div>
	<div class="box-body">
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>
		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'programas', 'name' => 'programas', 'style' => 'width: 100%']) !!}
	</div>
</div>

<div class="modal fade" id="modal-programa" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-programa')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-programa-title">Registrar beneficio</h4>
            </div>
            <div class="modal-body" id="modal-body-programa"></div>
            <div class="modal-footer" id="modal-footer-programa">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-programa')"><i class="fa fa-ban"></i> Cancelar</button>
                <button type="button" class="btn btn-success" onclick="programa.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detalle" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-detalle')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-detalle-title">Información del beneficio</h4>
            </div>
            <div class="modal-body" id="modal-body-detalle"></div>
            <div class="modal-footer" id="modal-footer-detalle">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-detalle')"><i class="fa fa-ban"></i> Cerrar</button>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-documento" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="cerrar_modal('modal-documento')" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Registrar documento</h4>
			</div>
			<div class="modal-body">
				<form data-toggle="validator" role="form" id="form_documento" action="{{ URL::to('documentos') }}" method="POST">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		
							<div class="row" style="padding-left: 0; padding-right: 0;">						
								<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
									<div class="form-group">
										<label for="nombre">Nombre</label>
										<input type="text" class="form-control" id="nombre" name="nombre">
									</div>
								</div>
							</div>						
							<div class="row" style="padding-left: 0; padding-right: 0;">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group">
										<label for="descripcion">Descripción</label>
										<input type="text" class="form-control" id="descripcion" name="descripcion">
									</div>
								</div>
							</div>		
						</div>
					</div>			
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-documento')"><i class="fa fa-ban"></i> Cancelar</button>
				<button type="button" class="btn btn-success" onclick="documento.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
			</div>      
		</div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@include('programas.js.programa')
{!! $dataTable->scripts() !!}

<script type="text/javascript">
	function colapsar(div, btn){
		if($("#"+div).hasClass('collapse')){
			$("#"+div).removeClass('collapse');
			$("#"+btn).html('<i class="fa fa-minus"></i>');
		}else{
			$("#"+div).addClass('collapse');
			$("#"+btn).html('<i class="fa fa-plus"></i>');						
		}
	}

    $("form").submit(function(e){
        e.preventDefault();
    });

	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#programas').dataTable());
	table.init();

	function programa_create_edit_success(response, method) {
		var mensaje = (method === "POST") ? 'registrado' : 'actualizado';
    	swal({
      		title: '¡Correcto!',
      		text: 'Beneficio ' + mensaje + '.',
      		type: 'success',
      		timer: 2000
    	});
		table.get_table().DataTable().ajax.reload(null, false);
		app.set_bloqueo(false);
		unblock();
    	cerrar_modal('modal-programa');
	}

	function programa_create_edit_error(response) {
		unblock();
		if(response.status === 422) {
			swal({
				title: 'Error al registrar el beneficio.',				
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Regresar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
	};

	function documento_create_edit_success(response, method) {
		var mensaje = (method === "POST") ? 'registrado' : 'actualizado';
    	swal({
      		title: '¡Correcto!',
      		text: 'Documento ' + mensaje + '.',
      		type: 'success',
      		timer: 2000
    	});		
		var nuevoDocumento = new Option(response.documento.nombre, response.documento.id, true, true);
    	$('#documentos').append(nuevoDocumento).trigger('change');
		app.set_bloqueo(false);
		unblock();
    	cerrar_modal('modal-documento');
	}

	function documento_create_edit_error(response) {
		unblock();
	};

	function abrir_modal(modal, id, activar) {
		if(modal === "modal-programa"){
			block();
			var url = (id) ? window.location.href + '/' + id +'/edit' : window.location.href+'/create';
      		var title = (id) ? 'Editar beneficio' : 'Registrar beneficio';
            var datos = activar ? 'activar=true' : '';
      		$("#modal-programa-title").text(title);
      		$.get(url, datos, function(data) {            
        		$('#modal-body-programa').html(data.html);                            
        		programa.init();
				unblock();
        		$('#modal-programa').modal('show');            
      		});
		} else if(modal === "modal-documento"){
			documento.init();
			$("#form_documento")[0].reset();
        	$('#modal-documento').modal('show');            
		} else if(modal === "modal-detalle"){
            $.get(window.location.href + '/' + id, function(data) {
        		$('#modal-body-detalle').html(data.html);
                var groupColumn = 0;
                $("#documentos-programa").DataTable({
			        language: {
				        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
			        },
			        dom: 'itp',
			        lengthMenu: [ [5], [5] ],
			        responsive: true,
			        autoWidth: true,
			        processing: true,
                    columnDefs: [
                        { visible: false, targets: groupColumn },
                        { className: 'text-center', targets: '_all' }
                    ],
                    order: [[ groupColumn, 'desc' ]],
                    drawCallback: function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last = null; 
                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                            if ( last !== group ) {
                                $(rows).eq( i ).before(
                                    '<tr class="group"><td colspan="2">'+group+'</td></tr>'
                                );
                                last = group;
                            }
                        })
		            }
                });
				unblock();
        		$('#modal-detalle').modal('show');            
      		});
        } 			
	};

  function cerrar_modal(modal){
   	$("#"+modal).modal("hide");
  }	

	function eliminar(id, nombre){
        swal({
            title: '',
            text: '¿Está seguro de eliminar el beneficio '+nombre+'?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'No',
            confirmButtonText: 'Sí, eliminar'
        }).then((result) => {
            if(result.value) {
				block();
                $.ajax({
          			url: window.location.href + '/' + id,
          			type: 'DELETE',
          			success: function(response) {
            			table.get_table().DataTable().ajax.reload(null, false);
						unblock();
            			swal({
              				title: '¡Correcto!',
              				text: 'El beneficio ha sido eliminado.',
              				type: 'success',
              				timer: 3000
            			});
          			},
          			error: function(response) {
						unblock();						
          			}
        		});
            }
        });
    }

	function activar(id, nombre) {
        swal({
            title: '',
            text: '¿Está seguro de activar el beneficio ' + nombre + '?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'No',
            confirmButtonText: 'Sí, activar'
        }).then((result) => {
            if(result.value) {
				block();
                abrir_modal('modal-programa', id, true);
            }else{
                table.get_table().DataTable().ajax.reload(null, false);
            }
        });
    }

    function inactivar(id, nombre){
        swal({
            title: '',
            text: '¿Está seguro de inactivar el beneficio '+nombre+'?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'No',
            confirmButtonText: 'Sí, inactivar'
        }).then((result) => {
            if(result.value) {
                block();
                $.ajax({
          			url: window.location.href + '/' + id,
                    data: 'inactivar=true',
          			type: 'DELETE',
          			success: function(response) {
            			table.get_table().DataTable().ajax.reload(null, false);
						unblock();
            			swal({
              				title: '¡Correcto!',
              				text: 'El beneficio ha sido inactivado.',
              				type: 'success',
              				timer: 3000
            			});
          			},
          			error: function(response) {
						unblock();						
          			}
        		});
            }else{
                table.get_table().DataTable().ajax.reload(null, false);
            }
        });
    }
</script>
@endpush