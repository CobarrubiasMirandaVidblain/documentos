@php
    $modulo = $modulo!=""? "$modulo::" : "";
@endphp
@extends("${modulo}layouts.master")
<?php $ruta = \Request::route()->getName(); ?>


@push('head')
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('css/animate.css') }}" type="text/css" rel="stylesheet">
  <style>
    body {
      padding-right: 0 !important;
    }
    .select2-selection__rendered {
      text-transform: uppercase
    }
    .select2-results__option{
      text-transform: uppercase
    }
    .dtrg-group.dtrg-start.dtrg-level-0 {
      background-color: #eae9e9;
    }
    td{
      text-transform: uppercase
    }
  </style>
@endpush

@if($ruta == "programas.externos")
    @section('content-title', 'COORDINACIÓN DE ATENCIÓN CIUDADANA Y VINCULACIÓN SOCIAL')
@elseif($programa_padre != "")
  @section('content-title', $programa_padre)
@else
  @section('content-title', auth()->user()->persona->empleado->area->nombre)
@endif

@if($ruta == "programas.externos")
    @section('content-subtitle', 'Beneficios Externos')
@else
    @section('content-subtitle', 'Beneficios')
@endif

@section('content')
<div class="box box-primary shadow">
	<div class="box-header with-border">
		<h3 class="box-title">
            @if($ruta == "programas.externos")
                Beneficios Externos
            @else
                Beneficios
            @endif
        </h3>
	</div>
	<div class="box-body">
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>
		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'programas', 'name' => 'programas', 'style' => 'width: 100%']) !!}
	</div>
</div>

<div class="modal" id="modal-area" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div class="modal" id="modal-beneficio" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div class="modal" id="modal-ceBeneficio" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<div class="modal" id="modal-documento" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			
		</div>
	</div>
</div>

<div class="modal" id="modal-ceDocumento" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
		<div class="modal-content">
			
		</div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
{{-- @include('programas.js.programa') --}}
{!! $dataTable->scripts() !!}

<script type="text/javascript">

	$("form").submit(function(e){
      e.preventDefault();
  });
	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
  $.fn.modal.Constructor.prototype.enforceFocus = function() {};
  
  function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});
	}

	function unblock() {
		$.unblockUI();
	}

  $(document).bind("ajaxStart", function(){
    block()
  }).bind("ajaxStop", function(){
    unblock()
  });

    /* $(".toogle").change(function(event) {
    if( $(this).prop("checked") ) {
      activar($(event.target).attr("id"),
      $(event.target).attr("name")); 
    } else { 
      inactivar($(event.target).attr("id"), $(event.target).attr("name"));
      } 
    }) } */

	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#programas').dataTable());
	table.init();

  function abrirModal(modal_id,url) {
    $.get(url,{view:'adminlte'},function(data) {            
      $('#modal-'+modal_id+'>.modal-dialog>.modal-content').html(data);
      $('#modal-'+modal_id).modal('show');            
    });
  }

  function cerrarModal(modal){
   	$("#"+modal).modal("hide");
  }	

</script>
@endpush