<script type="text/javascript">	

	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Está seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	function programa_create_edit_success(response, method) {};
	
	function programa_create_edit_error(response) {};

	var programa = (function() {
		var form = $('#form_programa'),
		tipo = $('#tipo'),
		action = form.attr('action'),
        method = form.attr('method');      

		function init() {
			form = $('#form_programa'),
            tipo = $('#tipo'),
			action = form.attr('action'),
			method = form.attr('method');	
			$('#tipo').select2({
                language: 'es',
                placeholder: 'SELECCIONE EL TIPO',
                minimumResultsForSearch: Infinity
            }).change(function(event){
                $(this).valid();
            });

             $('#area_id').select2({
                language: 'es',
                placeholder: 'Seleccione el área responsable',
                ajax:{
                    delay: 500,
                    url: '{{ route('areas.select') }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term,
                            padre_id: {{ auth()->user()->persona->empleado->area->id }}
                        };
                    },
                    processResults:(data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(function(event){
                $(this).valid();
                $('#programa_padre').empty();
			    $('#programa_padre').select2({
                    language: 'es',
                    tags: true,
                    ajax: {
                        url: '{{ route('programasPadres.select') }}',
                        dataType: 'JSON',
                        type: 'GET',
                        data: function(params) {
                            return {
                                search: params.term,
                                area_id: $('#area_id').val()
                            };
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: $.map(data, function(item) {
                                    return {
                                        id: item.nombre,
                                        text: item.nombre,
                                        slug: item.nombre,
                                        results: item
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                }).change(function(event) {
                    $(this).valid();
                });
            });

            $('#programa_padre').select2({
                    language: 'es',
                    tags: true,
                    ajax: {
                        url: '{{ route('programasPadres.select') }}',
                        dataType: 'JSON',
                        type: 'GET',
                        data: function(params) {
                            return {
                                search: params.term,
                                area_id: $('#area_id').val()
                            };
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: $.map(data, function(item) {
                                    return {
                                        id: item.nombre,
                                        text: item.nombre,
                                        slug: item.nombre,
                                        results: item
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                }).change(function(event) {
                    $(this).valid();
                });


			$('#documentos').select2({
                language: 'es',
                placeholder: 'Seleccione la documentación requerida',
                ajax:{
                    delay: 500,
                    url: '{{ route('documentos.select') }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults:(data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            $('#fecha_inicio').datepicker({
                autoclose: true,
                language: 'es',
                format: 'dd-mm-yyyy'
            }).on('changeDate', function(selected){
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                startDate.setDate(startDate.getDate()+1);
                $('#fecha_fin').datepicker('setStartDate', startDate);
            }).change(function(event) {
		        $("#fecha_inicio").valid();	
            });

            $('#fecha_fin').datepicker({
                language: 'es',
                format: 'dd-mm-yyyy',
                autoclose: true
            }).on('changeDate', function(selected){
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                FromEndDate.setDate(FromEndDate.getDate()-1);
                $('#fecha_inicio').datepicker('setEndDate', FromEndDate);
            }).change(function(event) {
		        $("#fecha_inicio, #fecha_fin").valid();	
            });            

            $('#oficial').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

			agregar_validacion();
			app.to_upper_case();
		};

		function create_edit() {
			if(form.valid()) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action,
					type: method,
					data: form.serialize(),
					success: function(response) {
						programa_create_edit_success(response, method);
					},
					error: function(response) {
						programa_create_edit_error(response);
					}
				});

			}
        };       

		function agregar_validacion() {
			form.validate({
				rules: {                    
					nombre: {
						required: true
					},
					codigo: {
						required: true
					},
					descripcion: {
						required: true,
            maxlength: 555
					},
                    tipo: {
						required: true
					},
					fecha_inicio: {
						required:{ 
							depends: function(element) {
                                return ($('#fecha_fin').val() != '');
                            }
                        }
					},
					fecha_fin: {
						required:{ 
							depends: function(element) {
                                return ($('#fecha_inicio').val() != '');
                            }
                        }
					},
					oficial: {
						required: true
					}					
				},
				messages: {
				}
			});
		};

		return {
			init: init,			
            create_edit: create_edit
		};
	})();

	function documento_create_edit_success(response, method) {};
	
	function documento_create_edit_error(response) {};

	var documento = (function() {
		var form = $('#form_documento'),
		action = form.attr('action'),
		method = form.attr('method');

		function init() {
			form = $('#form_documento'),
			action = form.attr('action'),
			method = form.attr('method');
			agregar_validacion();
			app.to_upper_case();
		};

		function create_edit() {
			if(form.valid()) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action,
					type: method,
					data: form.serialize(),
					success: function(response) {
						documento_create_edit_success(response, method);
					},
					error: function(response) {
						documento_create_edit_error(response);
					}
				});

			}
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					nombre: {
						required: true
					},
					descripcion: {
						required: true
					}					
				},
				messages: {
				}
			});
		};

		return {
			init: init,			
            create_edit: create_edit            
		};
	})();
</script>