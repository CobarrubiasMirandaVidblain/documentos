<?php
// DB::listen(function($query){
  //Imprimimos la consulta ejecutada
//   echo "<pre> {$query->sql } </pre>";
// });
// Route::get('/updated-activity', 'ArduinoController@updatedActivity');
Route::resource('/files', 'FileController');

Route::group(['middleware' => 'web', 'prefix' => 'bancos'], function() {
    Route::get('/select', 'BancoController@bancosSelect')->name('bancos.select');
});

Route::group(['middleware' => 'web', 'prefix' => 'personas'], function() {
    Route::get('/', 'PersonaController@index')->name('personas.index');
    Route::post('/personas/datatable', 'PersonaController@datatable')->name('personas.datatable');
    Route::get('/create', 'PersonaController@create')->name('personas.create');
    Route::post('/store', 'PersonaController@store');
    Route::get('/show/{id}', 'PersonaController@show')->where('id', '[0-9]+')->name('personas.show');
    //Si ya se que estoy repetiendo una ruta no se enojen
    Route::get('/mostrar/{id}', 'PersonaController@mostrar')->where('id', '[0-9]+')->name('personas.mostrar');
    Route::get('/edit/{id}', 'PersonaController@edit')->where('id', '[0-9]+')->name('personas.edit');
    Route::put('/update/{id}', 'PersonaController@update')->where('id', '[0-9]+');
    Route::delete('/destroy/{id}', 'PersonaController@destroy')->where('id', '[0-9]+');
});

Route::get('/regiones/select','RegionController@select')->name('regiones.select');
Route::get('/regiones/datatable','RegionController@list')->name('regiones.datatable');
Route::resource('/regiones', 'RegionController', ['except' => ['show']]);

Route::get('/distritos/select','DistritoController@select')->name('distritos.select');
Route::get('/distritos/datatable','DistritoController@list')->name('distritos.datatable');
Route::resource('/distritos', 'DistritoController', ['except' => ['show']]);

Route::group(['middleware' => 'web','prefix' => 'municipios'], function(){
    Route::get('/datatable','MunicipioController@list')->name('municipios.datatable');
    Route::get('/select', 'MunicipioController@municipiosSelect')->name('municipios.select');
    Route::get('/buscar/{token}/{municipio}', 'MunicipioController@buscarMunicpio')->middleware(['recmat', 'recmatRol:9,ADMINISTRADOR,ALMACEN']);
    Route::get('/{id}/inversiones','MunicipioController@inversiones')->name('municipios.inversiones');
});
Route::resource('municipios','MunicipioController');

Route::group(['middleware' => 'web', 'prefix' => 'localidades'], function() {
    Route::get('/localidades/datatable', 'LocalidadController@list')->name('localidades.datatable');
    Route::get('/localidades/select', 'LocalidadController@localidadesSelect')->name('localidades.select');
});
Route::resource('/localidades','LocalidadController');

//Comentado para quitar el BUENOS DIAS ADMINISTRADOR
Route::get('/', 'HomeController@welcome');
//Route::get('/', 'PeticionBaseController@directores')->name('peticiones.index');

Auth::routes();
require (__DIR__ . '/backend/usuariosroles.php');
require (__DIR__ . '/backend/areas.php');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
//Route::get('/home', 'PeticionBaseController@directores')->name('home')->middleware('auth');
Route::get('/home2', 'HomeController@index2')->name('home2')->middleware('auth');
Route::get('/home3', 'HomeController@index3')->name('home3')->middleware('auth');
Route::get('/home4', 'HomeController@index4')->name('home4')->middleware('auth');
Route::get('/home5', 'HomeController@index5')->name('home5')->middleware('auth');
Route::get('/completar', 'HomeController@completar')->middleware('auth');
Route::post('/registro/{idusuario}/ficha', 'HomeController@ficha')->middleware('auth');
Route::get('/registro/{idusuario}/datos', 'HomeController@datosFicha')->middleware('auth');

Route::resource('modules', 'Admin\ModuleController');
Route::get('modules/access/new', 'Admin\ModuleController@access')->name('modules.access.create');
Route::post('modules/access/new', 'Admin\ModuleController@paccess')->name('modules.access');
Route::get('modules/access/authorized', 'Admin\ModuleController@authorized')->name('modules.access.authorized.index');
Route::put('modules/access/authorized/{req}', 'Admin\ModuleController@pauthorized')->name('modules.access.authorized');

Route::resource('users', 'Admin\UsersController');
Route::get('/users', 'Admin\UsersController@ficha')->name('users.ficha');
Route::put('/users/{user}/status', 'Admin\UsersController@status')->name('users.status');
Route::get('/users/find/select', 'Admin\UsersController@buscarUsuarios');
Route::get('/users/ficha/{usuario}', 'Admin\UsersController@usuarioFicha');
Route::get('/users/{idusuario}/datos', 'Admin\UsersController@obtenerDatos');
Route::get('/usuarios/{token}/{usuario}', 'Admin\UsersController@buscarUsuariosV')->middleware(['recmat', 'recmatRol:9,ADMINISTRADOR,ALMACEN']);

Route::resource('bancos', 'BancoController');

Route::group(['middleware' => 'web', 'prefix' => 'discapacidades'], function() {
    Route::get('/', 'DiscapacidadController@index');
    Route::get('/create', 'DiscapacidadController@create');
    Route::post('/store', 'DiscapacidadController@store');
    Route::get('/show', 'DiscapacidadController@show');
    Route::get('/edit', 'DiscapacidadController@edit');
    Route::put('/update', 'DiscapacidadController@update');
    Route::delete('/destroy', 'DiscapacidadController@destroy');
    Route::get('/select', 'DiscapacidadController@select')->name('discapacidades.select');
});

Route::group(['middleware' => 'web', 'prefix' => 'recepciones'], function() {
    Route::get('/recepcionselect', 'RecepcionController@recepcionSelect')->name('recepcion.select');
});

Route::group(['middleware' => 'web', 'prefix' => 'tiporemitente'], function() {
    Route::get('/tiporemitente', 'TipoRemitenteController@tiporemitenteSelect')->name('remitente.select');
});

Route::group(['middleware' => 'web', 'prefix' => 'personas'], function() {
    Route::get('search', 'PersonaController@search')->name('persona.search');
});

Route::group(['middleware' => 'web', 'prefix' => 'titular'], function() {
    Route::get('/titular', 'TitularController@titularSelect')->name('titular.select');
});

Route::POST('/personas/documentos','DocumentosPersonasController@store')->name('personas.documentos.store');

Route::group(['middleware' => 'web','prefix' => 'empleados'], function(){
    Route::get('/select', 'EmpleadoController@empleadosSelect')->name('empleados.select');
});

Route::group(['middleware' => 'web', 'prefix' => 'dependencias'], function() {
    Route::get('/', 'DependenciaController@index')->name('dependencias.index');
    Route::get('/create', 'DependenciaController@create')->name('dependencias.create');
    Route::post('/store', 'DependenciaController@store');
    Route::get('/show/{id}', 'DependenciaController@show')->where('id', '[0-9]+')->name('dependencias.show');
    Route::get('/edit/{id}', 'DependenciaController@edit')->where('id', '[0-9]+')->name('dependencias.edit');
    Route::put('/update/{id}', 'DependenciaController@update')->where('id', '[0-9]+');
    Route::delete('/destroy/{id}', 'DependenciaController@destroy')->where('id', '[0-9]+');
    Route::get('/select', 'DependenciaController@select')->name('dependencias.select');
});

Route::get('/documentos/select', 'DocumentoController@select')->name('documentos.select');
Route::resource('documentos', 'DocumentoController');

Route::get('/empleados/{persona}', 'EmpleadoController@index');
Route::post('/empleados', 'EmpleadoController@store')->name('empleado.store');
Route::get('/escolaridades/select', 'EscolaridadController@select')->name('escolaridades.select');
Route::get('/estadosciviles/select', 'EstadoCivilController@select')->name('estadosciviles.select');
Route::get('/ocupaciones/select', 'OcupacionController@select')->name('ocupaciones.select');
Route::get('/areas/select', 'AreaController@select')->name('areas.select');
Route::get('/areas/select2', 'AreaController@select2');
Route::get('/cargos/select', 'CargoController@select')->name('cargos.select');
Route::get('/partidos/select', 'PartidoPoliticoController@select')->name('partidos.select');

/* Route::get('/beneficios', 'ProgramaBaseController@directores')->name('beneficios.index');
Route::delete('/beneficios/{id}', 'ProgramaBaseController@destroy')->name('beneficios.destroy');
Route::get('/beneficios/create', 'ProgramaBaseController@create')->name('beneficios.create');
Route::get('/beneficios/{id}/edit', 'ProgramaBaseController@edit')->name('beneficios.edit');
Route::post('/beneficios', 'ProgramaBaseController@store')->name('programas.store');
Route::put('/beneficios/{id}', 'ProgramaBaseController@update')->name('beneficios.update'); */

// Route::get('/peticiones', 'PeticionBaseController@directores')->name('peticiones.index');
Route::get('/peticiones/cantidad', 'PeticionBaseController@getCantidad')->name('peticiones.cantidad');
Route::resource('/peticiones', 'PeticionBaseController');
// Route::put('/peticiones/{id}', 'PeticionBaseController@update')->name('peticiones.update');

//Route::get('/programaspadres', 'BeneficiosController@programasPadresSelect')->name('programasPadres.select');
/*** nuevo componente ***/
Route::resource('/programas', 'ProgramaController',['only'=>['index']]);
Route::resource('/programas/{padre_id}', 'ProgramaController',['only'=>['index']]);
Route::get('/programasExternos', 'ProgramaController@beneficiosExternos')->name('programas.externos');
Route::get('/programa/{padre_id}/subprogramas', 'ProgramaController@getProgramas')->name('programa.subprogramas');
Route::resource('/programas/{programa_id}/area', 'AreaProgramaController',['as'=>'programas','only'=>['index','create','store']]);
Route::resource('/programas/{programa_id}/beneficios', 'BeneficioProgramaController',['as'=>'programas','except'=>['show']]);
Route::resource('/programas/{programa_id}/documentos','DocumentosProgramaController',['except'=>['update','edit','show'], 'as'=>'programas']);
Route::group(['middleware' => 'web', 'prefix' => 'beneficios'], function() {
  //Route::get('/','BeneficiosController@index')->name('beneficios.index');
  Route::get('/', 'BeneficiosController@beneficiosSelect')->name('beneficio.select');
  Route::get('/{beneficio_id}', 'BeneficiosController@obtenerDatosBeneficio')->name('beneficio.show');
});

Route::get('/proveedores/select','ProveedorController@select')->name('proveedores.select');

Route::resource('/proveedor','ProveedorController');
Route::get('/colores','HomeController@colores');
Route::get('/serverinfo','HomeController@info');
