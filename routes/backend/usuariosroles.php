<?php 
Route::group(['middleware' => 'auth'], function() {
    Route::get('/roles', 'Admin\RoleController@index')->name('roles.index');
    Route::get('/roles/usuarios/index', 'Admin\RoleController@showUserRol')->name('roles.users.index');
    Route::get('/roles/usuarios/vue', 'Admin\RoleController@rolesPaginado')->name('roles.users.json');
    Route::get('/roles/create', 'Admin\RoleController@create')->name('roles.create');
    Route::post('/roles', 'Admin\RoleController@store')->name('roles.store');
    Route::get('/roles/{rol}/edit', 'Admin\RoleController@edit')->name('roles.edit');
    Route::put('/roles/{rol}', 'Admin\RoleController@update')->name('roles.update');
    Route::delete('/roles/{rol}', 'Admin\RoleController@destroy')->name('roles.destroy');
    Route::get('/roles/asign/create', 'Admin\RoleController@asign')->name('roles.asign.create');
    Route::post('/roles/asign', 'Admin\RoleController@addAsign')->name('roles.asign');
    Route::get('/roles/usuarios', 'Admin\RoleController@usuarios')->name('roles.usuarios');
    Route::delete('/roles/usuarios/{usuariosrol}', 'Admin\RoleController@deleteUserRol')->name('roles.users.destroy')->where('usuariosrol', '[0-9]+');
    Route::get('/roles/modulos/{modulo}', 'Admin\RoleController@roles');
    Route::put('/roles/autorizar/{rol}', 'Admin\RoleController@autorizar');
});
    
Route::group(['prefix' => 'usuarios/roles'], function() {
    Route::post('/add', 'Admin\UsuariosrolController@store');
});

?>