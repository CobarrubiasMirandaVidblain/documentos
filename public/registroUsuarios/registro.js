new Vue({
	el:'#usuario',
	data:{
		nuevoUsuario 	:{
			nombre 				: '',
			primerApellido 		: '',
			segundoApellido 	: '',
		},
	},
	mounted (){
			let me = this;
			me.getDatos();
	},
	computed:{
       
    },
	methods:{
		getDatos(){
			let me = this;
			var url = "/orgeventos/eventos/datos";
			axios.get(url).then(function(response){
				me.participantes	= response.data.participantes
				me.urgentes 		= response.data.eventos 
				me.finalizado 		= response.data.finalizado 
				me.eventos	 		= response.data.eventos 
			}).catch(function(error){
				location.reload();
			})
		},
	}

})