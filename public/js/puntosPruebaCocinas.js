var puntos = [
  {
    Latitude: 16.31652075,
    Longitude: -97.42614668
  },
  {
    Latitude: 17.90696399,
    Longitude: -97.22501319
  },
  {
    Latitude: 17.05838086,
    Longitude: -97.18391212
  },
  {
    Latitude: 16.38179531,
    Longitude: -97.92766989
  },
  {
    Latitude: 16.95247015,
    Longitude: -96.27154511
  },
  {
    Latitude: 16.8901093,
    Longitude: -95.49490834
  },
  {
    Latitude: 18.00967592,
    Longitude: -97.23849466
  },
  {
    Latitude: 17.47064409,
    Longitude: -95.80391434
  },
  {
    Latitude: 16.43582782,
    Longitude: -96.56804533
  },
  {
    Latitude: 17.65106458,
    Longitude: -96.57267574
  },
  {
    Latitude: 16.33956572,
    Longitude: -95.913943
  },
  {
    Latitude: 17.82550907,
    Longitude: -96.10879213
  },
  {
    Latitude: 17.65657988,
    Longitude: -96.72411422
  },
  {
    Latitude: 16.73838685,
    Longitude: -97.80518384
  },
  {
    Latitude: 18.3101713,
    Longitude: -96.88283847
  },
  {
    Latitude: 17.35113778,
    Longitude: -96.63527249
  },
  {
    Latitude: 16.99406642,
    Longitude: -96.75439242
  },
  {
    Latitude: 16.84369761,
    Longitude: -95.637757
  },
  {
    Latitude: 16.91146142,
    Longitude: -97.97226834
  },
  {
    Latitude: 17.09750636,
    Longitude: -97.61024442
  },
  {
    Latitude: 16.22564557,
    Longitude: -95.69277385
  },
  {
    Latitude: 17.31058723,
    Longitude: -95.97933713
  },
  {
    Latitude: 17.47276441,
    Longitude: -96.43070639
  },
  {
    Latitude: 16.59996937,
    Longitude: -95.82979178
  },
  {
    Latitude: 17.32159922,
    Longitude: -97.14350526
  },
  {
    Latitude: 18.16775235,
    Longitude: -96.38162576
  },
  {
    Latitude: 16.28727806,
    Longitude: -95.86204002
  },
  {
    Latitude: 16.29295742,
    Longitude: -96.00451449
  },
  {
    Latitude: 17.89071643,
    Longitude: -96.0547156
  },
  {
    Latitude: 16.17922454,
    Longitude: -95.58186249
  },
  {
    Latitude: 15.73478662,
    Longitude: -97.12052724
  },
  {
    Latitude: 16.59176014,
    Longitude: -96.78619571
  },
  {
    Latitude: 16.5779828,
    Longitude: -96.05990361
  },
  {
    Latitude: 17.82018739,
    Longitude: -96.83592242
  },
  {
    Latitude: 17.19295161,
    Longitude: -97.86120526
  },
  {
    Latitude: 16.98915139,
    Longitude: -97.77363465
  },
  {
    Latitude: 17.29225172,
    Longitude: -96.80277048
  },
  {
    Latitude: 16.64206516,
    Longitude: -97.87795587
  },
  {
    Latitude: 17.02323478,
    Longitude: -97.00797287
  },
  {
    Latitude: 16.95665456,
    Longitude: -96.37231444
  },
  {
    Latitude: 16.00707369,
    Longitude: -96.74772485
  },
  {
    Latitude: 16.60878577,
    Longitude: -96.37662973
  },
  {
    Latitude: 17.60628331,
    Longitude: -97.60619476
  },
  {
    Latitude: 17.33433404,
    Longitude: -97.0434272
  },
  {
    Latitude: 15.98564643,
    Longitude: -95.77537581
  },
  {
    Latitude: 17.06530276,
    Longitude: -95.8491674
  },
  {
    Latitude: 16.29639104,
    Longitude: -97.02193251
  },
  {
    Latitude: 15.87672909,
    Longitude: -97.22877695
  },
  {
    Latitude: 17.27997058,
    Longitude: -95.74702762
  },
  {
    Latitude: 17.58290542,
    Longitude: -95.55407868
  },
  {
    Latitude: 16.47826565,
    Longitude: -96.45872477
  },
  {
    Latitude: 17.82671886,
    Longitude: -97.20326773
  },
  {
    Latitude: 16.83340016,
    Longitude: -97.54755001
  },
  {
    Latitude: 17.64604172,
    Longitude: -97.44686419
  },
  {
    Latitude: 17.69515305,
    Longitude: -96.77161219
  },
  {
    Latitude: 17.82732246,
    Longitude: -96.6527347
  },
  {
    Latitude: 16.12598086,
    Longitude: -96.25801609
  },
  {
    Latitude: 17.74730329,
    Longitude: -96.21816664
  },
  {
    Latitude: 16.40442031,
    Longitude: -97.26851158
  },
  {
    Latitude: 17.28049759,
    Longitude: -97.92715148
  },
  {
    Latitude: 17.30355618,
    Longitude: -98.00609475
  },
  {
    Latitude: 17.37936474,
    Longitude: -96.19894342
  },
  {
    Latitude: 15.79651704,
    Longitude: -96.2063422
  },
  {
    Latitude: 17.96546839,
    Longitude: -97.33938522
  },
  {
    Latitude: 17.37304682,
    Longitude: -97.20791793
  },
  {
    Latitude: 17.37807087,
    Longitude: -97.87450151
  },
  {
    Latitude: 17.7103162,
    Longitude: -96.05440579
  },
  {
    Latitude: 18.24255065,
    Longitude: -97.17692901
  },
  {
    Latitude: 17.41991243,
    Longitude: -96.86060581
  },
  {
    Latitude: 17.02311685,
    Longitude: -97.44532209
  },
  {
    Latitude: 16.80585865,
    Longitude: -96.70632421
  },
  {
    Latitude: 17.38305436,
    Longitude: -97.38557564
  },
  {
    Latitude: 17.32775643,
    Longitude: -96.79084156
  },
  {
    Latitude: 16.3342854,
    Longitude: -97.69094148
  },
  {
    Latitude: 16.40550468,
    Longitude: -96.214414
  },
  {
    Latitude: 16.50730756,
    Longitude: -96.44743808
  },
  {
    Latitude: 17.19874568,
    Longitude: -96.91097422
  },
  {
    Latitude: 17.74562678,
    Longitude: -97.80323946
  },
  {
    Latitude: 16.81516246,
    Longitude: -96.94726757
  },
  {
    Latitude: 17.52741925,
    Longitude: -95.40811821
  },
  {
    Latitude: 16.77835802,
    Longitude: -96.58706862
  },
  {
    Latitude: 15.87841925,
    Longitude: -97.15101194
  },
  {
    Latitude: 15.76530138,
    Longitude: -97.25452615
  },
  {
    Latitude: 16.49762164,
    Longitude: -95.49753906
  },
  {
    Latitude: 17.19779419,
    Longitude: -97.35680361
  },
  {
    Latitude: 16.03405999,
    Longitude: -96.78009374
  },
  {
    Latitude: 16.62989189,
    Longitude: -96.88416145
  },
  {
    Latitude: 17.60703083,
    Longitude: -95.51106544
  },
  {
    Latitude: 17.53620705,
    Longitude: -95.63282132
  },
  {
    Latitude: 16.87520807,
    Longitude: -96.17603971
  },
  {
    Latitude: 17.74336605,
    Longitude: -96.24123113
  },
  {
    Latitude: 17.46279727,
    Longitude: -97.0614445
  },
  {
    Latitude: 16.37800402,
    Longitude: -97.3438581
  },
  {
    Latitude: 16.88264453,
    Longitude: -98.04735762
  },
  {
    Latitude: 17.43873795,
    Longitude: -95.75674777
  },
  {
    Latitude: 16.15458204,
    Longitude: -97.45619347
  },
  {
    Latitude: 16.04941384,
    Longitude: -97.36754775
  },
  {
    Latitude: 17.06283372,
    Longitude: -97.22924967
  },
  {
    Latitude: 17.65385085,
    Longitude: -96.10110502
  },
  {
    Latitude: 17.06015578,
    Longitude: -96.18595143
  },
  {
    Latitude: 16.43348565,
    Longitude: -97.33367249
  },
  {
    Latitude: 18.21717572,
    Longitude: -97.19720634
  },
  {
    Latitude: 16.34632302,
    Longitude: -97.90585535
  },
  {
    Latitude: 17.73860938,
    Longitude: -96.19198781
  },
  {
    Latitude: 15.67539958,
    Longitude: -96.55563422
  },
  {
    Latitude: 17.82799445,
    Longitude: -96.16870729
  },
  {
    Latitude: 15.67801717,
    Longitude: -96.67804712
  },
  {
    Latitude: 17.9313609,
    Longitude: -97.55283448
  },
  {
    Latitude: 16.41605388,
    Longitude: -97.36161292
  },
  {
    Latitude: 16.89978539,
    Longitude: -97.46379412
  },
  {
    Latitude: 17.08168188,
    Longitude: -96.83636432
  },
  {
    Latitude: 16.86703791,
    Longitude: -95.8465261
  },
  {
    Latitude: 16.55107482,
    Longitude: -97.13354263
  },
  {
    Latitude: 16.61484582,
    Longitude: -96.38489956
  },
  {
    Latitude: 15.91400311,
    Longitude: -96.38160833
  },
  {
    Latitude: 16.88906811,
    Longitude: -97.11855278
  },
  {
    Latitude: 15.97849679,
    Longitude: -95.81591034
  },
  {
    Latitude: 16.70641831,
    Longitude: -96.87119923
  },
  {
    Latitude: 17.80463421,
    Longitude: -97.60814529
  },
  {
    Latitude: 17.05445067,
    Longitude: -95.45137385
  },
  {
    Latitude: 15.85488765,
    Longitude: -96.12071285
  },
  {
    Latitude: 17.27545,
    Longitude: -95.59040386
  },
  {
    Latitude: 16.72429638,
    Longitude: -96.13123963
  },
  {
    Latitude: 17.80012085,
    Longitude: -97.42536929
  },
  {
    Latitude: 17.41089085,
    Longitude: -95.90620081
  },
  {
    Latitude: 17.30314476,
    Longitude: -97.07220405
  },
  {
    Latitude: 16.82855338,
    Longitude: -95.88682627
  },
  {
    Latitude: 17.11847698,
    Longitude: -97.24863324
  },
  {
    Latitude: 16.82387124,
    Longitude: -96.79395228
  },
  {
    Latitude: 17.65767883,
    Longitude: -97.64117198
  },
  {
    Latitude: 17.60328512,
    Longitude: -97.15673133
  },
  {
    Latitude: 16.27289753,
    Longitude: -97.83436749
  },
  {
    Latitude: 16.90573823,
    Longitude: -98.0288863
  },
  {
    Latitude: 16.25328321,
    Longitude: -97.09407827
  },
  {
    Latitude: 16.10094779,
    Longitude: -96.90366968
  },
  {
    Latitude: 17.65461606,
    Longitude: -96.65349464
  },
  {
    Latitude: 17.69853075,
    Longitude: -97.83192792
  },
  {
    Latitude: 15.81093337,
    Longitude: -96.76690745
  },
  {
    Latitude: 16.37723837,
    Longitude: -96.88429941
  },
  {
    Latitude: 17.30918915,
    Longitude: -97.86621922
  },
  {
    Latitude: 16.5344211,
    Longitude: -95.40507215
  },
  {
    Latitude: 15.85990026,
    Longitude: -96.05742529
  },
  {
    Latitude: 15.89469559,
    Longitude: -96.47866406
  },
  {
    Latitude: 16.98321551,
    Longitude: -97.9234758
  },
  {
    Latitude: 16.51564687,
    Longitude: -96.80161417
  },
  {
    Latitude: 17.0028277,
    Longitude: -97.19926757
  },
  {
    Latitude: 16.0008062,
    Longitude: -97.14635424
  },
  {
    Latitude: 17.35936309,
    Longitude: -96.46279444
  },
  {
    Latitude: 17.37198473,
    Longitude: -96.16579471
  },
  {
    Latitude: 16.07503833,
    Longitude: -97.64434513
  },
  {
    Latitude: 17.71942697,
    Longitude: -97.08143137
  },
  {
    Latitude: 17.07895797,
    Longitude: -97.99051745
  },
  {
    Latitude: 17.46573643,
    Longitude: -97.06410877
  },
  {
    Latitude: 17.08843603,
    Longitude: -96.98223459
  },
  {
    Latitude: 17.49728883,
    Longitude: -96.74776627
  },
  {
    Latitude: 16.8478013,
    Longitude: -97.45678357
  },
  {
    Latitude: 16.78971163,
    Longitude: -95.36073955
  },
  {
    Latitude: 18.19185812,
    Longitude: -96.69082204
  },
  {
    Latitude: 16.96695048,
    Longitude: -97.30462855
  },
  {
    Latitude: 15.66409271,
    Longitude: -96.8675083
  },
  {
    Latitude: 17.45925267,
    Longitude: -96.58490359
  },
  {
    Latitude: 16.3458816,
    Longitude: -97.91303817
  },
  {
    Latitude: 15.86428079,
    Longitude: -96.64645274
  },
  {
    Latitude: 17.36995843,
    Longitude: -96.42324683
  },
  {
    Latitude: 17.23088704,
    Longitude: -97.00868668
  },
  {
    Latitude: 17.302157,
    Longitude: -96.01910075
  },
  {
    Latitude: 17.65509013,
    Longitude: -97.08265051
  },
  {
    Latitude: 15.91934257,
    Longitude: -96.55868411
  },
  {
    Latitude: 17.69382009,
    Longitude: -95.79240383
  },
  {
    Latitude: 17.37705769,
    Longitude: -96.26569739
  },
  {
    Latitude: 16.65184417,
    Longitude: -95.47592953
  },
  {
    Latitude: 16.85707647,
    Longitude: -95.65935807
  },
  {
    Latitude: 15.93979867,
    Longitude: -96.83027901
  },
  {
    Latitude: 16.02929598,
    Longitude: -96.57312101
  },
  {
    Latitude: 17.18178673,
    Longitude: -96.0579311
  },
  {
    Latitude: 18.14429004,
    Longitude: -96.52432086
  },
  {
    Latitude: 17.76059929,
    Longitude: -96.41190038
  },
  {
    Latitude: 16.91387349,
    Longitude: -97.68610873
  },
  {
    Latitude: 17.60738605,
    Longitude: -96.73768553
  },
  {
    Latitude: 16.00008411,
    Longitude: -96.53567725
  },
  {
    Latitude: 18.22352425,
    Longitude: -96.83347573
  },
  {
    Latitude: 16.76520033,
    Longitude: -95.95026344
  },
  {
    Latitude: 16.35211714,
    Longitude: -96.52940914
  },
  {
    Latitude: 17.08767996,
    Longitude: -96.19666812
  },
  {
    Latitude: 17.1876131,
    Longitude: -98.06244553
  },
  {
    Latitude: 17.81519744,
    Longitude: -96.97245345
  },
  {
    Latitude: 16.69192103,
    Longitude: -97.05725409
  },
  {
    Latitude: 17.21613624,
    Longitude: -97.0432071
  },
  {
    Latitude: 17.52682764,
    Longitude: -97.05397987
  },
  {
    Latitude: 15.76875159,
    Longitude: -96.71642233
  },
  {
    Latitude: 17.32470829,
    Longitude: -97.69222902
  },
  {
    Latitude: 17.51991258,
    Longitude: -95.7248553
  },
  {
    Latitude: 17.07593372,
    Longitude: -97.04283261
  },
  {
    Latitude: 17.49162144,
    Longitude: -97.0841249
  },
  {
    Latitude: 16.08039316,
    Longitude: -95.99221461
  },
  {
    Latitude: 17.13572118,
    Longitude: -96.53550078
  },
  {
    Latitude: 16.35126283,
    Longitude: -96.69202256
  },
  {
    Latitude: 16.11622935,
    Longitude: -95.65146467
  },
  {
    Latitude: 17.95129908,
    Longitude: -95.86835105
  },
  {
    Latitude: 16.5002206,
    Longitude: -96.46275526
  },
  {
    Latitude: 17.44959715,
    Longitude: -95.93501968
  },
  {
    Latitude: 17.2078742,
    Longitude: -97.59036295
  },
  {
    Latitude: 16.73500352,
    Longitude: -96.53084149
  },
  {
    Latitude: 16.84440357,
    Longitude: -95.74421324
  },
  {
    Latitude: 18.06536825,
    Longitude: -97.32474379
  },
  {
    Latitude: 16.63098147,
    Longitude: -97.28832493
  },
  {
    Latitude: 15.66327987,
    Longitude: -96.88843326
  },
  {
    Latitude: 17.8710159,
    Longitude: -97.19362655
  },
  {
    Latitude: 16.64740364,
    Longitude: -97.2022805
  },
  {
    Latitude: 18.03854223,
    Longitude: -95.9438236
  },
  {
    Latitude: 17.45790986,
    Longitude: -95.74869542
  },
  {
    Latitude: 15.79022236,
    Longitude: -96.84607033
  },
  {
    Latitude: 15.92824698,
    Longitude: -96.78860188
  },
  {
    Latitude: 17.32838251,
    Longitude: -95.71075705
  },
  {
    Latitude: 17.53891854,
    Longitude: -96.27825719
  },
  {
    Latitude: 17.06349373,
    Longitude: -96.95798611
  },
  {
    Latitude: 17.96703275,
    Longitude: -96.09015353
  },
  {
    Latitude: 15.86860312,
    Longitude: -96.72419914
  },
  {
    Latitude: 17.72128381,
    Longitude: -96.112449
  },
  {
    Latitude: 15.77458562,
    Longitude: -96.15848301
  },
  {
    Latitude: 17.8721204,
    Longitude: -97.10866595
  },
  {
    Latitude: 16.26186631,
    Longitude: -97.87392867
  },
  {
    Latitude: 17.12326588,
    Longitude: -95.65267138
  },
  {
    Latitude: 17.53309389,
    Longitude: -96.37976847
  },
  {
    Latitude: 17.28050253,
    Longitude: -96.37948313
  },
  {
    Latitude: 16.82942312,
    Longitude: -97.68094648
  },
  {
    Latitude: 17.31314236,
    Longitude: -96.95449561
  },
  {
    Latitude: 17.00193712,
    Longitude: -96.41891966
  },
  {
    Latitude: 17.11026434,
    Longitude: -97.15102227
  },
  {
    Latitude: 17.46366376,
    Longitude: -97.68936047
  },
  {
    Latitude: 17.49378041,
    Longitude: -96.72063313
  },
  {
    Latitude: 16.93120308,
    Longitude: -96.51828707
  },
  {
    Latitude: 16.08696113,
    Longitude: -97.05464407
  },
  {
    Latitude: 18.01585611,
    Longitude: -96.66763199
  },
  {
    Latitude: 18.06252192,
    Longitude: -96.222322
  },
  {
    Latitude: 18.19030055,
    Longitude: -96.37680098
  },
  {
    Latitude: 17.05326618,
    Longitude: -95.48078824
  },
  {
    Latitude: 16.65310917,
    Longitude: -96.79773062
  },
  {
    Latitude: 17.81913265,
    Longitude: -97.33927386
  },
  {
    Latitude: 18.25180799,
    Longitude: -97.04401503
  },
  {
    Latitude: 16.47308133,
    Longitude: -97.44033604
  },
  {
    Latitude: 16.38840794,
    Longitude: -95.49617166
  },
  {
    Latitude: 17.24977085,
    Longitude: -96.60104834
  },
  {
    Latitude: 16.4839415,
    Longitude: -96.64117214
  },
  {
    Latitude: 17.13828177,
    Longitude: -96.67649868
  },
  {
    Latitude: 16.35936222,
    Longitude: -96.00406207
  },
  {
    Latitude: 18.27072348,
    Longitude: -97.01772571
  },
  {
    Latitude: 17.61670781,
    Longitude: -95.78377615
  },
  {
    Latitude: 17.11895781,
    Longitude: -97.95915415
  },
  {
    Latitude: 17.41484945,
    Longitude: -97.07275764
  },
  {
    Latitude: 17.64959633,
    Longitude: -97.55147956
  },
  {
    Latitude: 17.21640189,
    Longitude: -96.55473225
  },
  {
    Latitude: 17.41502153,
    Longitude: -95.7670104
  },
  {
    Latitude: 16.51861682,
    Longitude: -97.86265266
  },
  {
    Latitude: 17.02914961,
    Longitude: -95.65667522
  },
  {
    Latitude: 18.11279678,
    Longitude: -96.64653164
  },
  {
    Latitude: 15.8426588,
    Longitude: -97.29030718
  },
  {
    Latitude: 15.96105751,
    Longitude: -97.25658316
  },
  {
    Latitude: 18.00579813,
    Longitude: -97.41286888
  },
  {
    Latitude: 16.0338784,
    Longitude: -96.78762075
  },
  {
    Latitude: 16.37022753,
    Longitude: -96.93841398
  },
  {
    Latitude: 17.68958383,
    Longitude: -95.84034633
  },
  {
    Latitude: 17.63920044,
    Longitude: -97.44483198
  },
  {
    Latitude: 17.85322252,
    Longitude: -97.40133872
  },
  {
    Latitude: 17.65453263,
    Longitude: -96.3254565
  },
  {
    Latitude: 17.71056378,
    Longitude: -95.53120297
  },
  {
    Latitude: 16.03743369,
    Longitude: -96.14641789
  },
  {
    Latitude: 17.81457368,
    Longitude: -97.36790812
  },
  {
    Latitude: 17.77503774,
    Longitude: -96.07002261
  },
  {
    Latitude: 17.8867505,
    Longitude: -96.33254032
  },
  {
    Latitude: 17.94728681,
    Longitude: -97.17842304
  },
  {
    Latitude: 17.88610674,
    Longitude: -96.77493618
  },
  {
    Latitude: 17.81269506,
    Longitude: -97.00478591
  },
  {
    Latitude: 17.93438082,
    Longitude: -96.8548348
  },
  {
    Latitude: 17.00440524,
    Longitude: -98.01405813
  },
  {
    Latitude: 16.38491804,
    Longitude: -95.47894381
  },
  {
    Latitude: 16.60390724,
    Longitude: -96.58292769
  },
  {
    Latitude: 17.33678393,
    Longitude: -97.70557144
  },
  {
    Latitude: 17.40174816,
    Longitude: -96.71290495
  },
  {
    Latitude: 16.02199474,
    Longitude: -96.83355748
  },
  {
    Latitude: 17.52095584,
    Longitude: -96.51933436
  },
  {
    Latitude: 16.4722815,
    Longitude: -95.7363751
  },
  {
    Latitude: 16.68205858,
    Longitude: -96.92040929
  },
  {
    Latitude: 15.74299802,
    Longitude: -96.88108928
  },
  {
    Latitude: 16.62811083,
    Longitude: -97.92433481
  },
  {
    Latitude: 18.05525699,
    Longitude: -97.17989885
  },
  {
    Latitude: 17.8067507,
    Longitude: -97.1347925
  },
  {
    Latitude: 17.97618203,
    Longitude: -97.23885678
  },
  {
    Latitude: 16.40368163,
    Longitude: -97.02264601
  },
  {
    Latitude: 17.39137359,
    Longitude: -97.40765989
  },
  {
    Latitude: 15.97215143,
    Longitude: -96.01531946
  },
  {
    Latitude: 17.66488838,
    Longitude: -95.75336576
  },
  {
    Latitude: 16.23554996,
    Longitude: -95.71471062
  },
  {
    Latitude: 17.27673846,
    Longitude: -95.91966437
  },
  {
    Latitude: 16.75740843,
    Longitude: -96.61234265
  },
  {
    Latitude: 17.66808331,
    Longitude: -96.03748847
  },
  {
    Latitude: 17.79323463,
    Longitude: -96.82884488
  },
  {
    Latitude: 16.14511583,
    Longitude: -96.22870962
  },
  {
    Latitude: 17.07765614,
    Longitude: -95.3251585
  },
  {
    Latitude: 16.62079247,
    Longitude: -98.04102142
  },
  {
    Latitude: 16.39136031,
    Longitude: -96.80549836
  },
  {
    Latitude: 18.10542452,
    Longitude: -97.37550271
  },
  {
    Latitude: 17.02359157,
    Longitude: -97.31311362
  },
  {
    Latitude: 17.13312062,
    Longitude: -96.42139281
  },
  {
    Latitude: 18.2483994,
    Longitude: -96.45045181
  },
  {
    Latitude: 17.86391501,
    Longitude: -97.15750467
  },
  {
    Latitude: 17.14288038,
    Longitude: -96.74498908
  },
  {
    Latitude: 16.03465818,
    Longitude: -95.89384172
  },
  {
    Latitude: 16.47618009,
    Longitude: -96.2307357
  },
  {
    Latitude: 17.99985448,
    Longitude: -96.42844744
  },
  {
    Latitude: 16.83981487,
    Longitude: -95.42026446
  },
  {
    Latitude: 16.64055088,
    Longitude: -96.78476177
  },
  {
    Latitude: 17.78660394,
    Longitude: -95.64992216
  },
  {
    Latitude: 17.50594974,
    Longitude: -96.68502933
  },
  {
    Latitude: 16.54433505,
    Longitude: -96.26625985
  },
  {
    Latitude: 15.74705599,
    Longitude: -96.59454774
  },
  {
    Latitude: 15.69488188,
    Longitude: -96.7711893
  },
  {
    Latitude: 17.37594118,
    Longitude: -96.79455487
  },
  {
    Latitude: 17.67064225,
    Longitude: -96.16765666
  },
  {
    Latitude: 16.36094394,
    Longitude: -97.24593752
  },
  {
    Latitude: 17.40097045,
    Longitude: -96.8555194
  },
  {
    Latitude: 16.51433242,
    Longitude: -97.27009835
  },
  {
    Latitude: 16.70178684,
    Longitude: -95.59411493
  },
  {
    Latitude: 16.73427073,
    Longitude: -95.49818957
  },
  {
    Latitude: 15.69284907,
    Longitude: -96.60028596
  },
  {
    Latitude: 16.34161804,
    Longitude: -97.26284681
  },
  {
    Latitude: 17.33053696,
    Longitude: -95.47866821
  },
  {
    Latitude: 17.40794005,
    Longitude: -96.47336477
  },
  {
    Latitude: 16.5096781,
    Longitude: -97.72724652
  },
  {
    Latitude: 16.66658719,
    Longitude: -97.36933265
  },
  {
    Latitude: 16.77684162,
    Longitude: -97.73671176
  },
  {
    Latitude: 17.75424892,
    Longitude: -96.21014967
  },
  {
    Latitude: 16.3738609,
    Longitude: -96.71743151
  },
  {
    Latitude: 17.61948885,
    Longitude: -96.73631431
  },
  {
    Latitude: 17.2668265,
    Longitude: -97.44018782
  },
  {
    Latitude: 17.97647375,
    Longitude: -96.9715464
  },
  {
    Latitude: 17.35730488,
    Longitude: -97.24915352
  },
  {
    Latitude: 16.07163152,
    Longitude: -96.49883615
  },
  {
    Latitude: 16.85935097,
    Longitude: -95.71414463
  },
  {
    Latitude: 17.07955333,
    Longitude: -95.48690455
  },
  {
    Latitude: 16.37026318,
    Longitude: -97.01944741
  },
  {
    Latitude: 15.88381842,
    Longitude: -97.19866373
  },
  {
    Latitude: 15.94043883,
    Longitude: -96.32219137
  },
  {
    Latitude: 16.4794453,
    Longitude: -97.65921751
  },
  {
    Latitude: 16.34385079,
    Longitude: -96.31122345
  },
  {
    Latitude: 17.87790201,
    Longitude: -96.66821044
  },
  {
    Latitude: 15.97945761,
    Longitude: -96.42557535
  },
  {
    Latitude: 16.250111,
    Longitude: -97.35241741
  },
  {
    Latitude: 15.95676579,
    Longitude: -96.97544992
  },
  {
    Latitude: 17.31186001,
    Longitude: -96.82378764
  },
  {
    Latitude: 17.02634991,
    Longitude: -97.88024654
  },
  {
    Latitude: 18.11636511,
    Longitude: -96.96141106
  },
  {
    Latitude: 18.26517949,
    Longitude: -96.30567171
  },
  {
    Latitude: 16.43517001,
    Longitude: -96.50396445
  },
  {
    Latitude: 15.6406959,
    Longitude: -96.6342481
  },
  {
    Latitude: 17.40809551,
    Longitude: -95.62017119
  },
  {
    Latitude: 17.43487237,
    Longitude: -96.25595689
  },
  {
    Latitude: 16.42196568,
    Longitude: -97.41804895
  },
  {
    Latitude: 18.18430419,
    Longitude: -96.35214934
  },
  {
    Latitude: 16.8054621,
    Longitude: -96.85600175
  },
  {
    Latitude: 16.87959478,
    Longitude: -95.95411709
  },
  {
    Latitude: 17.19284615,
    Longitude: -95.93140109
  },
  {
    Latitude: 17.47526083,
    Longitude: -97.00069351
  },
  {
    Latitude: 16.0950142,
    Longitude: -96.11772885
  },
  {
    Latitude: 16.86888442,
    Longitude: -97.16237951
  },
  {
    Latitude: 17.87935169,
    Longitude: -95.64382936
  },
  {
    Latitude: 17.57537653,
    Longitude: -97.8540783
  },
  {
    Latitude: 17.76421001,
    Longitude: -96.05846253
  },
  {
    Latitude: 16.78837414,
    Longitude: -95.30869037
  },
  {
    Latitude: 17.05377278,
    Longitude: -95.87769884
  },
  {
    Latitude: 17.65971338,
    Longitude: -97.91010874
  },
  {
    Latitude: 18.04495008,
    Longitude: -96.72994777
  },
  {
    Latitude: 16.32047584,
    Longitude: -96.36987903
  },
  {
    Latitude: 17.96143351,
    Longitude: -96.4403267
  },
  {
    Latitude: 17.45316099,
    Longitude: -96.79507583
  },
  {
    Latitude: 16.97845643,
    Longitude: -97.5173789
  },
  {
    Latitude: 16.7748215,
    Longitude: -96.46281689
  },
  {
    Latitude: 15.9395118,
    Longitude: -96.44225365
  },
  {
    Latitude: 16.28119531,
    Longitude: -96.56902966
  },
  {
    Latitude: 17.41618384,
    Longitude: -97.60699138
  },
  {
    Latitude: 16.93073954,
    Longitude: -97.60292756
  },
  {
    Latitude: 17.29308379,
    Longitude: -96.1740233
  },
  {
    Latitude: 16.92307654,
    Longitude: -96.78235193
  },
  {
    Latitude: 16.1582325,
    Longitude: -96.84189125
  },
  {
    Latitude: 18.06580237,
    Longitude: -96.45164717
  },
  {
    Latitude: 16.35403144,
    Longitude: -97.62822136
  },
  {
    Latitude: 15.94043358,
    Longitude: -96.40991295
  },
  {
    Latitude: 17.94112981,
    Longitude: -96.73487008
  },
  {
    Latitude: 17.14334427,
    Longitude: -97.91562176
  },
  {
    Latitude: 16.9633133,
    Longitude: -96.69672513
  },
  {
    Latitude: 16.34104369,
    Longitude: -96.90339096
  },
  {
    Latitude: 17.46403321,
    Longitude: -95.54465518
  },
  {
    Latitude: 16.95490923,
    Longitude: -96.30917527
  },
  {
    Latitude: 17.12340669,
    Longitude: -96.65936694
  },
  {
    Latitude: 17.24286715,
    Longitude: -95.92308116
  },
  {
    Latitude: 15.68221127,
    Longitude: -96.71351447
  },
  {
    Latitude: 17.20486839,
    Longitude: -96.72298978
  },
  {
    Latitude: 17.25580618,
    Longitude: -96.65843902
  },
  {
    Latitude: 15.93671132,
    Longitude: -96.00367334
  },
  {
    Latitude: 18.00373698,
    Longitude: -97.08991065
  },
  {
    Latitude: 17.5875794,
    Longitude: -96.29169535
  },
  {
    Latitude: 16.26882354,
    Longitude: -96.44863908
  },
  {
    Latitude: 18.29769713,
    Longitude: -96.39003318
  },
  {
    Latitude: 16.99522285,
    Longitude: -98.01862296
  },
  {
    Latitude: 16.49458892,
    Longitude: -95.60443551
  },
  {
    Latitude: 16.03105957,
    Longitude: -96.47451432
  },
  {
    Latitude: 16.29068811,
    Longitude: -96.15072752
  },
  {
    Latitude: 16.68518651,
    Longitude: -96.0998844
  },
  {
    Latitude: 18.10517404,
    Longitude: -96.4556343
  },
  {
    Latitude: 16.88221375,
    Longitude: -96.59711365
  },
  {
    Latitude: 15.91261655,
    Longitude: -96.06803434
  },
  {
    Latitude: 16.66052099,
    Longitude: -96.22325057
  },
  {
    Latitude: 17.14349279,
    Longitude: -98.03640688
  },
  {
    Latitude: 16.52134213,
    Longitude: -97.96184055
  },
  {
    Latitude: 16.04568799,
    Longitude: -97.060881
  },
  {
    Latitude: 16.65138309,
    Longitude: -97.87442585
  },
  {
    Latitude: 17.46794551,
    Longitude: -96.6838363
  },
  {
    Latitude: 17.18262687,
    Longitude: -95.53170145
  },
  {
    Latitude: 17.21335421,
    Longitude: -96.95801792
  },
  {
    Latitude: 17.85749117,
    Longitude: -96.7199303
  },
  {
    Latitude: 16.25226796,
    Longitude: -96.05109145
  },
  {
    Latitude: 16.73580819,
    Longitude: -96.5352702
  },
  {
    Latitude: 17.16151279,
    Longitude: -95.70342657
  },
  {
    Latitude: 15.95990418,
    Longitude: -96.08685029
  },
  {
    Latitude: 17.93184893,
    Longitude: -97.0993764
  },
  {
    Latitude: 16.51808523,
    Longitude: -97.05783284
  },
  {
    Latitude: 15.89890608,
    Longitude: -96.97479733
  },
  {
    Latitude: 16.55594503,
    Longitude: -97.55060323
  },
  {
    Latitude: 17.80017811,
    Longitude: -96.62170142
  },
  {
    Latitude: 17.90069252,
    Longitude: -97.03402157
  },
  {
    Latitude: 17.74309339,
    Longitude: -97.05825167
  },
  {
    Latitude: 16.5980227,
    Longitude: -95.49309207
  },
  {
    Latitude: 17.82570151,
    Longitude: -97.74700735
  },
  {
    Latitude: 17.46462192,
    Longitude: -96.91545719
  },
  {
    Latitude: 17.09291958,
    Longitude: -96.3915485
  },
  {
    Latitude: 17.01590267,
    Longitude: -96.44672706
  },
  {
    Latitude: 17.93157463,
    Longitude: -96.167012
  },
  {
    Latitude: 17.7899581,
    Longitude: -97.40629228
  },
  {
    Latitude: 16.49297987,
    Longitude: -97.09952644
  },
  {
    Latitude: 17.56103983,
    Longitude: -95.7270849
  },
  {
    Latitude: 18.17667082,
    Longitude: -96.99954636
  },
  {
    Latitude: 16.88325215,
    Longitude: -96.38513021
  },
  {
    Latitude: 15.67123678,
    Longitude: -96.91440329
  },
  {
    Latitude: 16.44162334,
    Longitude: -97.26069185
  },
  {
    Latitude: 16.65115216,
    Longitude: -97.17977143
  },
  {
    Latitude: 16.58279369,
    Longitude: -95.91195122
  },
  {
    Latitude: 15.71736533,
    Longitude: -96.37366201
  },
  {
    Latitude: 17.68495631,
    Longitude: -96.61486679
  },
  {
    Latitude: 16.35632225,
    Longitude: -95.70437285
  },
  {
    Latitude: 16.54911209,
    Longitude: -95.62196202
  },
  {
    Latitude: 16.6676173,
    Longitude: -95.60971432
  },
  {
    Latitude: 17.24440864,
    Longitude: -97.26974963
  },
  {
    Latitude: 16.9890859,
    Longitude: -96.39989811
  },
  {
    Latitude: 17.70386339,
    Longitude: -96.97695298
  },
  {
    Latitude: 17.19649488,
    Longitude: -97.4181838
  },
  {
    Latitude: 17.46020933,
    Longitude: -97.10174529
  },
  {
    Latitude: 16.04174105,
    Longitude: -97.65874616
  },
  {
    Latitude: 15.79684098,
    Longitude: -96.17214732
  },
  {
    Latitude: 17.99426428,
    Longitude: -95.8839933
  },
  {
    Latitude: 16.3730032,
    Longitude: -96.23217375
  },
  {
    Latitude: 17.39249945,
    Longitude: -95.56727792
  },
  {
    Latitude: 18.00058165,
    Longitude: -97.23214596
  },
  {
    Latitude: 18.13932767,
    Longitude: -96.17919327
  },
  {
    Latitude: 16.20453135,
    Longitude: -95.57248121
  },
  {
    Latitude: 17.27269364,
    Longitude: -97.36978871
  },
  {
    Latitude: 16.94598768,
    Longitude: -97.60431931
  },
  {
    Latitude: 16.65201978,
    Longitude: -95.78205088
  },
  {
    Latitude: 17.25900806,
    Longitude: -95.52248818
  },
  {
    Latitude: 18.04126713,
    Longitude: -97.05066877
  },
  {
    Latitude: 17.40228402,
    Longitude: -95.45863129
  },
  {
    Latitude: 17.73861154,
    Longitude: -96.26611252
  },
  {
    Latitude: 18.00174771,
    Longitude: -95.92302252
  },
  {
    Latitude: 17.70087713,
    Longitude: -95.67658026
  },
  {
    Latitude: 16.52643093,
    Longitude: -96.65212808
  },
  {
    Latitude: 16.34202713,
    Longitude: -97.3335787
  },
  {
    Latitude: 16.6518866,
    Longitude: -95.71846553
  },
  {
    Latitude: 16.35303976,
    Longitude: -95.91617891
  },
  {
    Latitude: 17.18217738,
    Longitude: -96.96648069
  },
  {
    Latitude: 16.41575242,
    Longitude: -97.59398983
  },
  {
    Latitude: 16.94616388,
    Longitude: -95.52335
  },
  {
    Latitude: 17.00150888,
    Longitude: -96.81181413
  },
  {
    Latitude: 16.90559831,
    Longitude: -97.74254272
  },
  {
    Latitude: 18.28028916,
    Longitude: -96.39488632
  },
  {
    Latitude: 17.92338861,
    Longitude: -96.08415114
  },
  {
    Latitude: 17.70842769,
    Longitude: -95.92967223
  },
  {
    Latitude: 17.74513452,
    Longitude: -96.57512222
  },
  {
    Latitude: 17.10239123,
    Longitude: -95.89478727
  },
  {
    Latitude: 17.27717774,
    Longitude: -95.43690459
  },
  {
    Latitude: 17.43351404,
    Longitude: -97.52513122
  },
  {
    Latitude: 16.98048043,
    Longitude: -97.49540977
  },
  {
    Latitude: 17.87397148,
    Longitude: -95.66235651
  },
  {
    Latitude: 16.7956329,
    Longitude: -97.99098922
  },
  {
    Latitude: 16.21171349,
    Longitude: -96.38505465
  },
  {
    Latitude: 17.10204853,
    Longitude: -95.37925326
  },
  {
    Latitude: 17.68359335,
    Longitude: -96.04529697
  },
  {
    Latitude: 16.4945144,
    Longitude: -96.59473744
  },
  {
    Latitude: 17.83290798,
    Longitude: -96.6510451
  },
  {
    Latitude: 16.18774901,
    Longitude: -97.12875671
  },
  {
    Latitude: 16.88068778,
    Longitude: -96.84171848
  },
  {
    Latitude: 17.60389149,
    Longitude: -96.8874071
  },
  {
    Latitude: 17.74598901,
    Longitude: -97.51035205
  },
  {
    Latitude: 16.7360054,
    Longitude: -97.84116972
  },
  {
    Latitude: 16.84125024,
    Longitude: -96.51550627
  },
  {
    Latitude: 16.4098541,
    Longitude: -96.867038
  },
  {
    Latitude: 16.25692367,
    Longitude: -95.97128536
  },
  {
    Latitude: 16.90868574,
    Longitude: -96.72632139
  },
  {
    Latitude: 18.27684506,
    Longitude: -97.04515283
  },
  {
    Latitude: 16.18136284,
    Longitude: -95.77159765
  },
  {
    Latitude: 17.91101142,
    Longitude: -95.73347893
  },
  {
    Latitude: 16.93040933,
    Longitude: -95.68288082
  },
  {
    Latitude: 17.80208498,
    Longitude: -95.91875138
  },
  {
    Latitude: 16.78975545,
    Longitude: -97.19633302
  },
  {
    Latitude: 16.6333686,
    Longitude: -95.45782118
  },
  {
    Latitude: 16.34147385,
    Longitude: -97.81709073
  },
  {
    Latitude: 17.74865593,
    Longitude: -96.09615275
  },
  {
    Latitude: 18.12749209,
    Longitude: -96.64412465
  },
  {
    Latitude: 16.34668418,
    Longitude: -96.44609011
  },
  {
    Latitude: 16.40373472,
    Longitude: -96.86919044
  },
  {
    Latitude: 16.75306675,
    Longitude: -96.17451468
  },
  {
    Latitude: 16.93514936,
    Longitude: -96.85464012
  },
  {
    Latitude: 16.89238831,
    Longitude: -96.39354184
  },
  {
    Latitude: 17.28655399,
    Longitude: -95.55592605
  },
  {
    Latitude: 16.17894109,
    Longitude: -97.67682349
  },
  {
    Latitude: 16.37521396,
    Longitude: -96.5312115
  },
  {
    Latitude: 17.64910081,
    Longitude: -96.24137157
  },
  {
    Latitude: 16.73414999,
    Longitude: -96.2723372
  },
  {
    Latitude: 17.38742303,
    Longitude: -97.99995999
  },
  {
    Latitude: 17.90350735,
    Longitude: -97.11013789
  },
  {
    Latitude: 18.02297843,
    Longitude: -95.82492378
  },
  {
    Latitude: 16.6550901,
    Longitude: -97.07763193
  },
  {
    Latitude: 16.4079082,
    Longitude: -97.08683804
  },
  {
    Latitude: 16.7866329,
    Longitude: -96.72028312
  },
  {
    Latitude: 15.87761077,
    Longitude: -96.6162902
  },
  {
    Latitude: 17.04506894,
    Longitude: -97.16155431
  },
  {
    Latitude: 17.16862196,
    Longitude: -95.77917369
  },
  {
    Latitude: 16.36910949,
    Longitude: -95.57207058
  },
  {
    Latitude: 16.15515758,
    Longitude: -97.70411726
  },
  {
    Latitude: 16.66151441,
    Longitude: -97.26607357
  },
  {
    Latitude: 17.6524893,
    Longitude: -95.84955126
  },
  {
    Latitude: 17.32400051,
    Longitude: -97.57431707
  },
  {
    Latitude: 17.90974634,
    Longitude: -97.26814124
  },
  {
    Latitude: 16.70603351,
    Longitude: -95.50521502
  },
  {
    Latitude: 17.17932736,
    Longitude: -96.18655333
  },
  {
    Latitude: 16.32442224,
    Longitude: -96.65697541
  },
  {
    Latitude: 16.78061798,
    Longitude: -96.47826497
  },
  {
    Latitude: 17.52789991,
    Longitude: -97.86543368
  },
  {
    Latitude: 17.03479542,
    Longitude: -95.28958107
  },
  {
    Latitude: 17.48255302,
    Longitude: -97.43399742
  },
  {
    Latitude: 17.5069468,
    Longitude: -96.5152206
  },
  {
    Latitude: 16.00251487,
    Longitude: -97.44423688
  },
  {
    Latitude: 17.22653172,
    Longitude: -97.04444399
  },
  {
    Latitude: 17.21179372,
    Longitude: -97.86591846
  },
  {
    Latitude: 16.6648813,
    Longitude: -96.93159462
  },
  {
    Latitude: 16.70672702,
    Longitude: -96.65110855
  },
  {
    Latitude: 17.22611819,
    Longitude: -97.35548843
  },
  {
    Latitude: 17.03126811,
    Longitude: -97.62428098
  },
  {
    Latitude: 17.69195298,
    Longitude: -97.12906691
  },
  {
    Latitude: 16.41335525,
    Longitude: -95.93656541
  },
  {
    Latitude: 18.07414576,
    Longitude: -96.6210802
  },
  {
    Latitude: 17.11598454,
    Longitude: -96.32406828
  },
  {
    Latitude: 17.5386659,
    Longitude: -95.48266249
  },
  {
    Latitude: 17.79198191,
    Longitude: -96.59960312
  },
  {
    Latitude: 17.48875667,
    Longitude: -95.80138576
  },
  {
    Latitude: 18.19036625,
    Longitude: -96.6051351
  },
  {
    Latitude: 16.45969333,
    Longitude: -95.41247259
  },
  {
    Latitude: 16.68093863,
    Longitude: -95.79490147
  },
  {
    Latitude: 15.86638695,
    Longitude: -96.35561756
  },
  {
    Latitude: 16.15406322,
    Longitude: -96.9165499
  },
  {
    Latitude: 17.60330062,
    Longitude: -96.39153515
  },
  {
    Latitude: 17.89956055,
    Longitude: -97.66842901
  },
  {
    Latitude: 17.57972944,
    Longitude: -97.79158973
  },
  {
    Latitude: 16.25796069,
    Longitude: -97.73307756
  },
  {
    Latitude: 17.10033887,
    Longitude: -95.39054894
  },
  {
    Latitude: 16.75208004,
    Longitude: -96.70096101
  },
  {
    Latitude: 16.63029911,
    Longitude: -97.94052588
  },
  {
    Latitude: 16.24107245,
    Longitude: -97.76519243
  },
  {
    Latitude: 15.93452626,
    Longitude: -95.88661629
  },
  {
    Latitude: 16.54372429,
    Longitude: -96.49595838
  },
  {
    Latitude: 15.77976416,
    Longitude: -96.44732009
  },
  {
    Latitude: 15.68617263,
    Longitude: -96.37254465
  },
  {
    Latitude: 16.7642275,
    Longitude: -95.60292869
  },
  {
    Latitude: 16.4689295,
    Longitude: -96.32265555
  },
  {
    Latitude: 17.63510816,
    Longitude: -97.91781983
  },
  {
    Latitude: 17.06604406,
    Longitude: -95.33858787
  },
  {
    Latitude: 16.9145047,
    Longitude: -97.27042641
  },
  {
    Latitude: 17.42454903,
    Longitude: -97.13346818
  },
  {
    Latitude: 16.82454923,
    Longitude: -95.53338686
  },
  {
    Latitude: 15.88610488,
    Longitude: -96.73095201
  },
  {
    Latitude: 17.89911526,
    Longitude: -96.63640194
  },
  {
    Latitude: 16.38288726,
    Longitude: -97.83043866
  },
  {
    Latitude: 16.91756521,
    Longitude: -97.35398756
  },
  {
    Latitude: 16.78883883,
    Longitude: -95.41776293
  },
  {
    Latitude: 15.74673589,
    Longitude: -96.81558805
  },
  {
    Latitude: 17.06455392,
    Longitude: -95.41667478
  },
  {
    Latitude: 17.25296458,
    Longitude: -96.28420994
  },
  {
    Latitude: 17.5569544,
    Longitude: -96.50625313
  },
  {
    Latitude: 17.22186143,
    Longitude: -96.20135374
  },
  {
    Latitude: 16.33219256,
    Longitude: -97.05821266
  },
  {
    Latitude: 18.16254142,
    Longitude: -96.04284802
  },
  {
    Latitude: 16.11946512,
    Longitude: -95.71743359
  },
  {
    Latitude: 16.41425202,
    Longitude: -95.55056673
  },
  {
    Latitude: 16.74018208,
    Longitude: -96.43012845
  },
  {
    Latitude: 16.18848164,
    Longitude: -96.8657585
  },
  {
    Latitude: 16.76705876,
    Longitude: -95.91729177
  },
  {
    Latitude: 17.82557387,
    Longitude: -96.08597339
  },
  {
    Latitude: 16.9753559,
    Longitude: -97.03877267
  },
  {
    Latitude: 17.67899147,
    Longitude: -95.92056726
  },
  {
    Latitude: 17.05343139,
    Longitude: -97.4626714
  },
  {
    Latitude: 15.76095919,
    Longitude: -97.25905409
  },
  {
    Latitude: 16.61718441,
    Longitude: -95.63043034
  },
  {
    Latitude: 16.72846771,
    Longitude: -97.54041597
  },
  {
    Latitude: 17.47587154,
    Longitude: -96.20398563
  },
  {
    Latitude: 16.47595549,
    Longitude: -95.59175916
  },
  {
    Latitude: 16.78092488,
    Longitude: -97.67749437
  },
  {
    Latitude: 17.34408,
    Longitude: -97.74218634
  },
  {
    Latitude: 17.12798151,
    Longitude: -97.34976975
  },
  {
    Latitude: 16.83703395,
    Longitude: -95.43452089
  },
  {
    Latitude: 18.04170937,
    Longitude: -96.21299994
  },
  {
    Latitude: 17.05711369,
    Longitude: -97.67787256
  },
  {
    Latitude: 17.36523893,
    Longitude: -97.50975344
  },
  {
    Latitude: 17.59987257,
    Longitude: -96.41519922
  },
  {
    Latitude: 17.34721539,
    Longitude: -96.09561842
  },
  {
    Latitude: 17.03001034,
    Longitude: -95.41067984
  },
  {
    Latitude: 18.0856964,
    Longitude: -95.9609419
  },
  {
    Latitude: 16.06371107,
    Longitude: -96.50160023
  },
  {
    Latitude: 16.59284992,
    Longitude: -97.65489048
  },
  {
    Latitude: 16.15599477,
    Longitude: -96.99085249
  },
  {
    Latitude: 17.26025102,
    Longitude: -96.20052969
  },
  {
    Latitude: 16.36538666,
    Longitude: -95.61946131
  },
  {
    Latitude: 17.82709873,
    Longitude: -96.73890151
  },
  {
    Latitude: 17.36377305,
    Longitude: -97.07886429
  },
  {
    Latitude: 17.72151056,
    Longitude: -96.76104149
  },
  {
    Latitude: 16.57893632,
    Longitude: -96.2613972
  },
  {
    Latitude: 17.96797552,
    Longitude: -96.78250467
  },
  {
    Latitude: 18.26157527,
    Longitude: -96.80363029
  },
  {
    Latitude: 16.83221136,
    Longitude: -97.47334285
  },
  {
    Latitude: 15.94869175,
    Longitude: -96.57867234
  },
  {
    Latitude: 16.00937669,
    Longitude: -97.23332816
  },
  {
    Latitude: 17.34789879,
    Longitude: -95.71526972
  },
  {
    Latitude: 16.92956267,
    Longitude: -96.7044437
  },
  {
    Latitude: 18.12963841,
    Longitude: -97.42486234
  },
  {
    Latitude: 16.53477691,
    Longitude: -96.80877575
  },
  {
    Latitude: 17.0460109,
    Longitude: -96.45433277
  },
  {
    Latitude: 17.06752591,
    Longitude: -96.09513687
  },
  {
    Latitude: 15.85262339,
    Longitude: -96.48209258
  },
  {
    Latitude: 17.19641392,
    Longitude: -97.42425225
  },
  {
    Latitude: 17.41770557,
    Longitude: -97.60808848
  },
  {
    Latitude: 17.39000521,
    Longitude: -95.69874145
  },
  {
    Latitude: 17.27883803,
    Longitude: -97.47714549
  },
  {
    Latitude: 16.107429,
    Longitude: -96.87689998
  },
  {
    Latitude: 18.30837021,
    Longitude: -96.75053927
  },
  {
    Latitude: 17.50999064,
    Longitude: -96.15077807
  },
  {
    Latitude: 17.35170953,
    Longitude: -95.66061794
  },
  {
    Latitude: 17.80886205,
    Longitude: -96.12374086
  },
  {
    Latitude: 17.03792417,
    Longitude: -96.08579995
  },
  {
    Latitude: 17.53720631,
    Longitude: -96.51387497
  },
  {
    Latitude: 16.63602059,
    Longitude: -96.78457766
  },
  {
    Latitude: 16.15133038,
    Longitude: -96.08472678
  },
  {
    Latitude: 15.82446209,
    Longitude: -96.90569762
  },
  {
    Latitude: 17.67655658,
    Longitude: -96.06279713
  },
  {
    Latitude: 17.01170255,
    Longitude: -97.03278157
  },
  {
    Latitude: 18.10660733,
    Longitude: -97.31751528
  },
  {
    Latitude: 17.33152172,
    Longitude: -95.44239347
  },
  {
    Latitude: 15.73945777,
    Longitude: -96.99779681
  },
  {
    Latitude: 17.19962926,
    Longitude: -96.96385985
  },
  {
    Latitude: 16.1064984,
    Longitude: -97.05797437
  },
  {
    Latitude: 17.20980152,
    Longitude: -96.67102426
  },
  {
    Latitude: 17.79854736,
    Longitude: -96.39809465
  },
  {
    Latitude: 16.66027336,
    Longitude: -95.57011644
  },
  {
    Latitude: 18.09400953,
    Longitude: -96.81393327
  },
  {
    Latitude: 15.64058653,
    Longitude: -96.80856447
  },
  {
    Latitude: 16.96976248,
    Longitude: -97.70297122
  },
  {
    Latitude: 16.28562774,
    Longitude: -96.95321376
  },
  {
    Latitude: 16.62543426,
    Longitude: -95.87516794
  },
  {
    Latitude: 16.63543792,
    Longitude: -97.63890729
  },
  {
    Latitude: 17.53115421,
    Longitude: -95.6264527
  },
  {
    Latitude: 18.07053677,
    Longitude: -97.05215161
  },
  {
    Latitude: 15.89997199,
    Longitude: -96.038824
  },
  {
    Latitude: 17.82382415,
    Longitude: -97.19934936
  },
  {
    Latitude: 16.78002827,
    Longitude: -98.00654042
  },
  {
    Latitude: 18.04318845,
    Longitude: -97.1409682
  },
  {
    Latitude: 17.47489328,
    Longitude: -97.69546332
  },
  {
    Latitude: 16.80786218,
    Longitude: -97.21406896
  },
  {
    Latitude: 16.54722723,
    Longitude: -97.32929911
  },
  {
    Latitude: 16.47823397,
    Longitude: -95.52713609
  },
  {
    Latitude: 16.06297894,
    Longitude: -96.62268189
  },
  {
    Latitude: 16.74037574,
    Longitude: -97.98775181
  },
  {
    Latitude: 16.32080383,
    Longitude: -95.91980813
  },
  {
    Latitude: 16.21262141,
    Longitude: -95.55044042
  },
  {
    Latitude: 16.69466951,
    Longitude: -96.12942398
  },
  {
    Latitude: 17.04602471,
    Longitude: -97.21901238
  },
  {
    Latitude: 18.11232359,
    Longitude: -97.15986056
  },
  {
    Latitude: 17.31751286,
    Longitude: -97.6014701
  },
  {
    Latitude: 16.74600965,
    Longitude: -97.47703302
  },
  {
    Latitude: 17.75902749,
    Longitude: -95.93411955
  },
  {
    Latitude: 16.67047753,
    Longitude: -96.43771784
  },
  {
    Latitude: 17.6103316,
    Longitude: -97.35126805
  },
  {
    Latitude: 17.41380352,
    Longitude: -95.67316599
  },
  {
    Latitude: 17.10822389,
    Longitude: -97.69515921
  },
  {
    Latitude: 17.48507877,
    Longitude: -96.06510261
  },
  {
    Latitude: 16.31905848,
    Longitude: -96.94705597
  },
  {
    Latitude: 17.66879865,
    Longitude: -95.76136476
  },
  {
    Latitude: 16.82452415,
    Longitude: -96.65175053
  },
  {
    Latitude: 17.29707975,
    Longitude: -95.73306215
  },
  {
    Latitude: 18.00867224,
    Longitude: -95.99644167
  },
  {
    Latitude: 17.54264564,
    Longitude: -96.31726304
  },
  {
    Latitude: 16.658476,
    Longitude: -95.37309603
  },
  {
    Latitude: 17.67045961,
    Longitude: -95.73334487
  },
  {
    Latitude: 17.52408803,
    Longitude: -97.94609091
  },
  {
    Latitude: 16.10025322,
    Longitude: -97.37553323
  },
  {
    Latitude: 17.13826875,
    Longitude: -97.11770412
  },
  {
    Latitude: 16.42941708,
    Longitude: -97.34521553
  },
  {
    Latitude: 18.00141037,
    Longitude: -97.36998309
  },
  {
    Latitude: 16.27362064,
    Longitude: -97.13268381
  },
  {
    Latitude: 17.36846764,
    Longitude: -96.52446789
  },
  {
    Latitude: 18.25414265,
    Longitude: -96.91234276
  },
  {
    Latitude: 17.3311895,
    Longitude: -96.5416466
  },
  {
    Latitude: 17.88765295,
    Longitude: -96.59979712
  },
  {
    Latitude: 17.78430829,
    Longitude: -95.55840685
  },
  {
    Latitude: 16.15652531,
    Longitude: -96.46529596
  },
  {
    Latitude: 16.30016642,
    Longitude: -96.61009459
  },
  {
    Latitude: 16.52069022,
    Longitude: -95.93762099
  },
  {
    Latitude: 16.30336212,
    Longitude: -95.79817756
  },
  {
    Latitude: 16.79478307,
    Longitude: -96.61394768
  },
  {
    Latitude: 16.90359094,
    Longitude: -95.53923952
  },
  {
    Latitude: 16.62709977,
    Longitude: -97.7466615
  },
  {
    Latitude: 15.75661488,
    Longitude: -96.96560255
  },
  {
    Latitude: 17.47713002,
    Longitude: -96.75447705
  },
  {
    Latitude: 17.40470188,
    Longitude: -95.3889394
  },
  {
    Latitude: 16.53340429,
    Longitude: -97.73252865
  },
  {
    Latitude: 17.34802782,
    Longitude: -95.37368015
  },
  {
    Latitude: 18.20237373,
    Longitude: -96.17898394
  },
  {
    Latitude: 17.23492832,
    Longitude: -96.00855085
  },
  {
    Latitude: 16.0695637,
    Longitude: -96.73036839
  },
  {
    Latitude: 17.00292872,
    Longitude: -95.97701093
  },
  {
    Latitude: 16.69858116,
    Longitude: -96.22484359
  },
  {
    Latitude: 15.90267503,
    Longitude: -97.25526105
  },
  {
    Latitude: 18.25072488,
    Longitude: -96.78789802
  },
  {
    Latitude: 17.32071358,
    Longitude: -96.05924314
  },
  {
    Latitude: 17.59053913,
    Longitude: -96.26655325
  },
  {
    Latitude: 17.66039826,
    Longitude: -95.96854133
  },
  {
    Latitude: 17.05590825,
    Longitude: -97.99077008
  },
  {
    Latitude: 18.18465489,
    Longitude: -96.20613081
  },
  {
    Latitude: 16.56049782,
    Longitude: -96.5657122
  },
  {
    Latitude: 17.24515328,
    Longitude: -96.2299378
  },
  {
    Latitude: 15.82617014,
    Longitude: -96.41554448
  },
  {
    Latitude: 17.49636983,
    Longitude: -96.32016935
  },
  {
    Latitude: 16.47496657,
    Longitude: -96.76314795
  },
  {
    Latitude: 17.13254146,
    Longitude: -96.72697839
  },
  {
    Latitude: 16.863754,
    Longitude: -97.13460813
  },
  {
    Latitude: 16.29203593,
    Longitude: -95.58331417
  },
  {
    Latitude: 16.3982718,
    Longitude: -96.9702668
  },
  {
    Latitude: 16.57761761,
    Longitude: -96.88575238
  },
  {
    Latitude: 16.70392973,
    Longitude: -98.06860199
  },
  {
    Latitude: 16.5968442,
    Longitude: -97.17677337
  },
  {
    Latitude: 16.2852317,
    Longitude: -96.77934657
  },
  {
    Latitude: 16.55790969,
    Longitude: -95.95871152
  },
  {
    Latitude: 16.00897549,
    Longitude: -97.53940712
  },
  {
    Latitude: 16.28593521,
    Longitude: -96.97720509
  },
  {
    Latitude: 17.11649795,
    Longitude: -96.65379112
  },
  {
    Latitude: 16.00658578,
    Longitude: -96.69677335
  },
  {
    Latitude: 17.81704311,
    Longitude: -96.48627051
  },
  {
    Latitude: 16.24540373,
    Longitude: -97.45686703
  },
  {
    Latitude: 17.26787003,
    Longitude: -97.81857597
  },
  {
    Latitude: 16.54732998,
    Longitude: -96.19090874
  },
  {
    Latitude: 17.47331632,
    Longitude: -97.19945088
  },
  {
    Latitude: 16.20681055,
    Longitude: -95.95816215
  },
  {
    Latitude: 16.24826666,
    Longitude: -95.89870643
  },
  {
    Latitude: 16.58627838,
    Longitude: -96.25596222
  },
  {
    Latitude: 17.60192553,
    Longitude: -96.49440782
  },
  {
    Latitude: 16.50628807,
    Longitude: -97.15163494
  },
  {
    Latitude: 17.93330882,
    Longitude: -96.0436083
  },
  {
    Latitude: 17.30106334,
    Longitude: -97.50039823
  },
  {
    Latitude: 16.82729074,
    Longitude: -97.0216653
  },
  {
    Latitude: 16.76358944,
    Longitude: -96.46672568
  },
  {
    Latitude: 16.80624586,
    Longitude: -97.69163877
  },
  {
    Latitude: 17.90343783,
    Longitude: -96.32499306
  },
  {
    Latitude: 16.50418139,
    Longitude: -97.68104096
  },
  {
    Latitude: 16.96394903,
    Longitude: -97.05834735
  },
  {
    Latitude: 16.44055326,
    Longitude: -95.77913973
  },
  {
    Latitude: 17.73421465,
    Longitude: -97.24501226
  },
  {
    Latitude: 17.56428007,
    Longitude: -96.98232264
  },
  {
    Latitude: 16.15225039,
    Longitude: -95.89661491
  },
  {
    Latitude: 16.18157927,
    Longitude: -97.64952981
  },
  {
    Latitude: 16.44876323,
    Longitude: -96.40166225
  },
  {
    Latitude: 15.9699918,
    Longitude: -97.32504418
  },
  {
    Latitude: 15.88993,
    Longitude: -96.84115531
  },
  {
    Latitude: 16.5813484,
    Longitude: -96.96293808
  },
  {
    Latitude: 17.41653951,
    Longitude: -95.59331549
  },
  {
    Latitude: 17.3988992,
    Longitude: -97.4071077
  },
  {
    Latitude: 16.80934997,
    Longitude: -96.81400159
  },
  {
    Latitude: 16.59458246,
    Longitude: -97.38858759
  },
  {
    Latitude: 17.11673928,
    Longitude: -97.45524036
  },
  {
    Latitude: 18.13014947,
    Longitude: -96.02089755
  },
  {
    Latitude: 16.5676693,
    Longitude: -96.65457117
  },
  {
    Latitude: 16.63610196,
    Longitude: -97.33377883
  },
  {
    Latitude: 17.38220206,
    Longitude: -97.76924471
  },
  {
    Latitude: 16.66738375,
    Longitude: -95.36117881
  },
  {
    Latitude: 16.37050213,
    Longitude: -97.14003091
  },
  {
    Latitude: 16.85422742,
    Longitude: -97.81100465
  },
  {
    Latitude: 16.29630205,
    Longitude: -95.61145839
  },
  {
    Latitude: 16.72774176,
    Longitude: -95.44341709
  },
  {
    Latitude: 17.48597717,
    Longitude: -95.90718996
  },
  {
    Latitude: 16.86540578,
    Longitude: -97.59327974
  },
  {
    Latitude: 16.02779087,
    Longitude: -96.62810727
  },
  {
    Latitude: 16.05556732,
    Longitude: -97.39592326
  },
  {
    Latitude: 17.54396047,
    Longitude: -96.2931828
  },
  {
    Latitude: 17.80604039,
    Longitude: -97.10184909
  },
  {
    Latitude: 15.88512552,
    Longitude: -96.83315976
  },
  {
    Latitude: 16.62210878,
    Longitude: -95.50304743
  },
  {
    Latitude: 16.16703128,
    Longitude: -95.63963718
  },
  {
    Latitude: 17.16863614,
    Longitude: -97.1448918
  },
  {
    Latitude: 16.2598292,
    Longitude: -97.40954131
  },
  {
    Latitude: 16.02530069,
    Longitude: -97.62018126
  },
  {
    Latitude: 16.73001155,
    Longitude: -98.02586396
  },
  {
    Latitude: 16.23316966,
    Longitude: -96.39733862
  },
  {
    Latitude: 17.07604984,
    Longitude: -95.50341058
  },
  {
    Latitude: 17.12156178,
    Longitude: -97.30325545
  },
  {
    Latitude: 17.6360032,
    Longitude: -95.92844386
  },
  {
    Latitude: 16.70846265,
    Longitude: -96.86836378
  },
  {
    Latitude: 17.30944598,
    Longitude: -97.40293848
  },
  {
    Latitude: 15.84672478,
    Longitude: -96.76287617
  },
  {
    Latitude: 17.63843271,
    Longitude: -97.58087607
  },
  {
    Latitude: 18.01958054,
    Longitude: -97.26530704
  },
  {
    Latitude: 17.05763396,
    Longitude: -95.35985208
  },
  {
    Latitude: 16.17729041,
    Longitude: -97.25795543
  },
  {
    Latitude: 15.97626147,
    Longitude: -96.09553607
  },
  {
    Latitude: 16.19117786,
    Longitude: -96.49938667
  },
  {
    Latitude: 16.67449692,
    Longitude: -95.78967085
  },
  {
    Latitude: 16.22408375,
    Longitude: -95.84642257
  },
  {
    Latitude: 17.05876539,
    Longitude: -96.9483373
  },
  {
    Latitude: 17.50340651,
    Longitude: -96.82182813
  },
  {
    Latitude: 16.38591027,
    Longitude: -97.55910571
  },
  {
    Latitude: 16.78246153,
    Longitude: -96.80876917
  },
  {
    Latitude: 17.40350718,
    Longitude: -97.80769566
  },
  {
    Latitude: 17.43977772,
    Longitude: -97.03841972
  },
  {
    Latitude: 17.81221636,
    Longitude: -96.78429704
  },
  {
    Latitude: 16.28922019,
    Longitude: -95.63680887
  },
  {
    Latitude: 17.20526568,
    Longitude: -96.86336101
  },
  {
    Latitude: 16.76275779,
    Longitude: -97.60837547
  },
  {
    Latitude: 16.44818619,
    Longitude: -95.5235086
  },
  {
    Latitude: 18.20084634,
    Longitude: -97.27576856
  },
  {
    Latitude: 16.05792896,
    Longitude: -97.00754148
  },
  {
    Latitude: 16.22335628,
    Longitude: -97.44011545
  },
  {
    Latitude: 16.69225748,
    Longitude: -97.63358308
  },
  {
    Latitude: 17.06825165,
    Longitude: -95.40255305
  },
  {
    Latitude: 16.2834639,
    Longitude: -96.26887477
  },
  {
    Latitude: 16.40592887,
    Longitude: -96.86983705
  },
  {
    Latitude: 16.81330726,
    Longitude: -95.29739591
  },
  {
    Latitude: 17.08645343,
    Longitude: -97.05066139
  },
  {
    Latitude: 17.29960064,
    Longitude: -96.74369997
  },
  {
    Latitude: 15.67026798,
    Longitude: -96.92189399
  },
  {
    Latitude: 17.28107225,
    Longitude: -96.40691088
  },
  {
    Latitude: 16.52726885,
    Longitude: -97.13103333
  },
  {
    Latitude: 16.642825,
    Longitude: -97.92460377
  },
  {
    Latitude: 16.71865776,
    Longitude: -97.7529055
  },
  {
    Latitude: 16.08157993,
    Longitude: -96.6214899
  },
  {
    Latitude: 16.02714488,
    Longitude: -97.05782394
  },
  {
    Latitude: 15.7207358,
    Longitude: -97.14504439
  },
  {
    Latitude: 15.94810102,
    Longitude: -96.8247165
  },
  {
    Latitude: 16.91566661,
    Longitude: -95.76656881
  },
  {
    Latitude: 16.55944448,
    Longitude: -95.93513096
  },
  {
    Latitude: 16.57652092,
    Longitude: -95.88954255
  },
  {
    Latitude: 16.65917632,
    Longitude: -98.02740623
  },
  {
    Latitude: 17.12636807,
    Longitude: -95.8639883
  },
  {
    Latitude: 16.25578992,
    Longitude: -95.82739671
  },
  {
    Latitude: 17.18522721,
    Longitude: -97.50549357
  },
  {
    Latitude: 16.61087408,
    Longitude: -96.71967865
  },
  {
    Latitude: 16.55764747,
    Longitude: -96.4110071
  },
  {
    Latitude: 16.90100123,
    Longitude: -95.41604939
  },
  {
    Latitude: 16.13017714,
    Longitude: -96.10010702
  },
  {
    Latitude: 15.95364446,
    Longitude: -96.70234294
  },
  {
    Latitude: 16.10336232,
    Longitude: -96.41464593
  },
  {
    Latitude: 18.20877741,
    Longitude: -97.03419688
  },
  {
    Latitude: 16.13926867,
    Longitude: -96.93923366
  },
  {
    Latitude: 16.71245926,
    Longitude: -95.56580809
  },
  {
    Latitude: 16.71476704,
    Longitude: -97.19621027
  },
  {
    Latitude: 17.78714455,
    Longitude: -95.8239278
  },
  {
    Latitude: 16.20348614,
    Longitude: -95.67847922
  },
  {
    Latitude: 18.19997254,
    Longitude: -96.25283651
  },
  {
    Latitude: 16.74018329,
    Longitude: -95.71850677
  },
  {
    Latitude: 17.67535104,
    Longitude: -96.99739827
  },
  {
    Latitude: 17.66012544,
    Longitude: -95.83195854
  },
  {
    Latitude: 17.19606887,
    Longitude: -97.5998613
  },
  {
    Latitude: 16.61033,
    Longitude: -97.46399268
  },
  {
    Latitude: 16.08609375,
    Longitude: -97.25116746
  },
  {
    Latitude: 16.0439188,
    Longitude: -97.1512159
  },
  {
    Latitude: 17.60252889,
    Longitude: -97.08390962
  },
  {
    Latitude: 17.48743577,
    Longitude: -95.5643779
  },
  {
    Latitude: 16.09551849,
    Longitude: -96.66492723
  },
  {
    Latitude: 16.38572103,
    Longitude: -96.47928774
  },
  {
    Latitude: 16.11186557,
    Longitude: -96.30819204
  },
  {
    Latitude: 16.26948359,
    Longitude: -97.28198147
  },
  {
    Latitude: 16.63077124,
    Longitude: -97.94026945
  },
  {
    Latitude: 17.401443,
    Longitude: -96.5304736
  },
  {
    Latitude: 16.04145977,
    Longitude: -96.52700438
  },
  {
    Latitude: 17.03349458,
    Longitude: -96.54332944
  },
  {
    Latitude: 17.2574753,
    Longitude: -96.89439802
  },
  {
    Latitude: 17.19938558,
    Longitude: -96.07632074
  },
  {
    Latitude: 16.66646751,
    Longitude: -97.54101846
  },
  {
    Latitude: 17.09965207,
    Longitude: -96.118184
  },
  {
    Latitude: 16.57301496,
    Longitude: -97.14081064
  },
  {
    Latitude: 17.64953323,
    Longitude: -96.99521982
  },
  {
    Latitude: 17.51028723,
    Longitude: -96.78474332
  },
  {
    Latitude: 16.41310851,
    Longitude: -97.28903138
  },
  {
    Latitude: 17.9595106,
    Longitude: -96.87007676
  },
  {
    Latitude: 16.18336179,
    Longitude: -95.9871007
  },
  {
    Latitude: 16.45670336,
    Longitude: -96.93140341
  },
  {
    Latitude: 17.0865965,
    Longitude: -96.39658302
  },
  {
    Latitude: 17.67740195,
    Longitude: -95.94769468
  },
  {
    Latitude: 17.78074773,
    Longitude: -97.80909959
  },
  {
    Latitude: 17.9911018,
    Longitude: -96.7162984
  },
  {
    Latitude: 16.12238006,
    Longitude: -96.08600441
  },
  {
    Latitude: 17.26623294,
    Longitude: -97.56681353
  },
  {
    Latitude: 17.09748973,
    Longitude: -97.39614175
  },
  {
    Latitude: 16.47251502,
    Longitude: -95.59481715
  },
  {
    Latitude: 17.18891683,
    Longitude: -96.47827403
  },
  {
    Latitude: 17.78426903,
    Longitude: -97.33268564
  },
  {
    Latitude: 16.02366428,
    Longitude: -96.95089449
  },
  {
    Latitude: 16.50250692,
    Longitude: -96.63799071
  },
  {
    Latitude: 18.2357792,
    Longitude: -96.40793177
  },
  {
    Latitude: 16.94117764,
    Longitude: -97.80317002
  },
  {
    Latitude: 17.16640403,
    Longitude: -95.88122755
  },
  {
    Latitude: 17.75870885,
    Longitude: -96.1063084
  },
  {
    Latitude: 16.05803484,
    Longitude: -96.97477249
  },
  {
    Latitude: 17.20501759,
    Longitude: -96.11168528
  },
  {
    Latitude: 15.91480143,
    Longitude: -96.41336319
  },
  {
    Latitude: 18.16974201,
    Longitude: -96.76947101
  },
  {
    Latitude: 15.81121802,
    Longitude: -97.29326348
  },
  {
    Latitude: 17.3957268,
    Longitude: -96.43938892
  },
  {
    Latitude: 17.81552185,
    Longitude: -96.78392392
  },
  {
    Latitude: 17.67218246,
    Longitude: -95.83248439
  },
  {
    Latitude: 16.4637497,
    Longitude: -95.9076046
  },
  {
    Latitude: 17.02429465,
    Longitude: -95.66661199
  },
  {
    Latitude: 17.77648797,
    Longitude: -97.15497188
  },
  {
    Latitude: 17.8946631,
    Longitude: -96.74145535
  },
  {
    Latitude: 16.82480049,
    Longitude: -95.76454715
  },
  {
    Latitude: 17.23501551,
    Longitude: -95.34330189
  },
  {
    Latitude: 17.32281804,
    Longitude: -97.31403442
  },
  {
    Latitude: 18.11325959,
    Longitude: -96.72149358
  },
  {
    Latitude: 16.20835608,
    Longitude: -96.0217217
  },
  {
    Latitude: 17.87946724,
    Longitude: -96.77661127
  },
  {
    Latitude: 17.74485197,
    Longitude: -97.53432069
  },
  {
    Latitude: 18.27608372,
    Longitude: -96.85419715
  },
  {
    Latitude: 16.38943913,
    Longitude: -97.43061428
  },
  {
    Latitude: 16.50410252,
    Longitude: -95.57894221
  },
  {
    Latitude: 17.65042141,
    Longitude: -97.52638211
  },
  {
    Latitude: 17.95085735,
    Longitude: -96.38500816
  },
  {
    Latitude: 16.20946733,
    Longitude: -97.4673039
  },
  {
    Latitude: 16.56682859,
    Longitude: -96.66689784
  },
  {
    Latitude: 17.75213224,
    Longitude: -95.9436814
  },
  {
    Latitude: 16.08382423,
    Longitude: -97.70142283
  },
  {
    Latitude: 17.51702265,
    Longitude: -96.58087511
  },
  {
    Latitude: 18.17161458,
    Longitude: -96.05426762
  },
  {
    Latitude: 16.91023135,
    Longitude: -95.92592736
  },
  {
    Latitude: 16.28210449,
    Longitude: -96.98626018
  },
  {
    Latitude: 16.22585772,
    Longitude: -96.64694112
  },
  {
    Latitude: 15.95619819,
    Longitude: -95.81236819
  },
  {
    Latitude: 16.8239951,
    Longitude: -97.83365127
  },
  {
    Latitude: 16.04582246,
    Longitude: -97.24013953
  },
  {
    Latitude: 16.3825994,
    Longitude: -97.65547408
  },
  {
    Latitude: 16.60365291,
    Longitude: -97.15233301
  },
  {
    Latitude: 16.41659548,
    Longitude: -96.49164059
  },
  {
    Latitude: 17.21921217,
    Longitude: -95.4513222
  },
  {
    Latitude: 16.87730597,
    Longitude: -95.50515181
  },
  {
    Latitude: 16.07927058,
    Longitude: -95.78427448
  },
  {
    Latitude: 16.95840858,
    Longitude: -97.00854959
  },
  {
    Latitude: 17.59908616,
    Longitude: -97.2213868
  },
  {
    Latitude: 16.91896533,
    Longitude: -97.78514923
  },
  {
    Latitude: 17.16642678,
    Longitude: -97.44155069
  },
  {
    Latitude: 16.07977257,
    Longitude: -96.57794037
  },
  {
    Latitude: 18.06414739,
    Longitude: -95.90246544
  },
  {
    Latitude: 17.46000393,
    Longitude: -95.80789644
  },
  {
    Latitude: 17.03925499,
    Longitude: -96.16682069
  },
  {
    Latitude: 16.8538366,
    Longitude: -95.57370016
  },
  {
    Latitude: 16.498914,
    Longitude: -96.65284135
  },
  {
    Latitude: 17.14885672,
    Longitude: -97.48911752
  },
  {
    Latitude: 16.9877719,
    Longitude: -97.71610825
  },
  {
    Latitude: 16.31499519,
    Longitude: -96.79620362
  },
  {
    Latitude: 17.77341658,
    Longitude: -96.14984866
  },
  {
    Latitude: 17.74694728,
    Longitude: -96.08991786
  },
  {
    Latitude: 17.25212735,
    Longitude: -97.68757195
  },
  {
    Latitude: 16.79532224,
    Longitude: -95.94544349
  },
  {
    Latitude: 15.98465178,
    Longitude: -97.62976178
  },
  {
    Latitude: 16.76517296,
    Longitude: -95.36139972
  },
  {
    Latitude: 16.76054298,
    Longitude: -95.33234765
  },
  {
    Latitude: 17.09580074,
    Longitude: -96.91878346
  },
  {
    Latitude: 17.16151762,
    Longitude: -95.41958379
  },
  {
    Latitude: 17.27118439,
    Longitude: -97.27943199
  },
  {
    Latitude: 17.37364821,
    Longitude: -97.53205401
  },
  {
    Latitude: 16.68085695,
    Longitude: -95.97730943
  },
  {
    Latitude: 17.49134308,
    Longitude: -97.82351174
  },
  {
    Latitude: 16.13232879,
    Longitude: -97.64884552
  },
  {
    Latitude: 16.9995807,
    Longitude: -96.83123541
  },
  {
    Latitude: 17.31091224,
    Longitude: -95.83896346
  },
  {
    Latitude: 17.9437282,
    Longitude: -96.56346391
  },
  {
    Latitude: 17.13108667,
    Longitude: -95.69537402
  },
  {
    Latitude: 17.45064096,
    Longitude: -96.57872079
  },
  {
    Latitude: 17.16338808,
    Longitude: -98.0171311
  },
  {
    Latitude: 16.51985284,
    Longitude: -95.799281
  },
  {
    Latitude: 16.93995239,
    Longitude: -96.25375503
  },
  {
    Latitude: 17.47902605,
    Longitude: -97.84416778
  },
  {
    Latitude: 17.19915679,
    Longitude: -97.55450233
  },
  {
    Latitude: 17.72662827,
    Longitude: -97.6079572
  },
  {
    Latitude: 18.00951592,
    Longitude: -97.42070474
  },
  {
    Latitude: 16.35413444,
    Longitude: -97.67250642
  },
  {
    Latitude: 17.21337243,
    Longitude: -96.68945513
  },
  {
    Latitude: 17.85756727,
    Longitude: -96.40549412
  },
  {
    Latitude: 16.72777355,
    Longitude: -97.68790904
  },
  {
    Latitude: 18.03818817,
    Longitude: -95.82969278
  },
  {
    Latitude: 17.7578284,
    Longitude: -95.54354653
  },
  {
    Latitude: 17.24846113,
    Longitude: -96.48057022
  },
  {
    Latitude: 18.11234484,
    Longitude: -97.21834537
  },
  {
    Latitude: 16.42028254,
    Longitude: -96.9607642
  },
  {
    Latitude: 17.82537363,
    Longitude: -96.25848422
  },
  {
    Latitude: 16.50479009,
    Longitude: -96.64751739
  },
  {
    Latitude: 16.51526924,
    Longitude: -96.93236839
  },
  {
    Latitude: 17.38303394,
    Longitude: -96.86904748
  },
  {
    Latitude: 17.379281,
    Longitude: -96.96771278
  },
  {
    Latitude: 17.85988563,
    Longitude: -96.34760422
  },
  {
    Latitude: 16.48180433,
    Longitude: -96.2037346
  },
  {
    Latitude: 16.380903,
    Longitude: -96.88467915
  },
  {
    Latitude: 16.00371118,
    Longitude: -97.51163833
  },
  {
    Latitude: 16.22492091,
    Longitude: -95.80771604
  },
  {
    Latitude: 17.25882174,
    Longitude: -96.2594972
  },
  {
    Latitude: 18.09294156,
    Longitude: -96.05753632
  },
  {
    Latitude: 15.7864465,
    Longitude: -96.87957329
  },
  {
    Latitude: 16.90911409,
    Longitude: -97.34172747
  },
  {
    Latitude: 16.44596706,
    Longitude: -96.54796994
  },
  {
    Latitude: 17.93876474,
    Longitude: -96.18958131
  },
  {
    Latitude: 16.44164691,
    Longitude: -97.5894388
  },
  {
    Latitude: 15.70565289,
    Longitude: -96.90697568
  },
  {
    Latitude: 17.11338127,
    Longitude: -96.75823858
  },
  {
    Latitude: 15.9721692,
    Longitude: -96.66017855
  },
  {
    Latitude: 16.6235502,
    Longitude: -97.90269856
  },
  {
    Latitude: 16.57895779,
    Longitude: -95.44627827
  },
  {
    Latitude: 15.87126611,
    Longitude: -96.46102576
  },
  {
    Latitude: 17.87123386,
    Longitude: -97.52745469
  },
  {
    Latitude: 17.01136756,
    Longitude: -97.81028302
  },
  {
    Latitude: 18.16486647,
    Longitude: -96.28602928
  },
  {
    Latitude: 16.30887375,
    Longitude: -97.80410943
  },
  {
    Latitude: 16.08205147,
    Longitude: -96.30698196
  },
  {
    Latitude: 15.97699561,
    Longitude: -96.01743119
  },
  {
    Latitude: 17.35943841,
    Longitude: -95.62011129
  },
  {
    Latitude: 17.2574047,
    Longitude: -95.53891686
  },
  {
    Latitude: 17.79548404,
    Longitude: -96.52923423
  },
  {
    Latitude: 16.44312805,
    Longitude: -96.48960947
  },
  {
    Latitude: 17.79013333,
    Longitude: -96.41342353
  },
  {
    Latitude: 17.48909521,
    Longitude: -95.98249269
  },
  {
    Latitude: 16.21950493,
    Longitude: -97.66450348
  },
  {
    Latitude: 16.16404625,
    Longitude: -97.63802343
  },
  {
    Latitude: 16.88321942,
    Longitude: -95.5786867
  },
  {
    Latitude: 17.78438934,
    Longitude: -96.54621166
  },
  {
    Latitude: 17.78063407,
    Longitude: -96.91076844
  },
  {
    Latitude: 17.14883636,
    Longitude: -95.68264187
  },
  {
    Latitude: 17.00450781,
    Longitude: -95.61928198
  },
  {
    Latitude: 17.7385463,
    Longitude: -97.0742418
  },
  {
    Latitude: 17.21354353,
    Longitude: -97.78283024
  },
  {
    Latitude: 16.60679602,
    Longitude: -95.35080835
  },
  {
    Latitude: 16.71629419,
    Longitude: -97.01162537
  },
  {
    Latitude: 17.37253818,
    Longitude: -96.15428626
  },
  {
    Latitude: 18.07742652,
    Longitude: -96.38587175
  },
  {
    Latitude: 16.26064465,
    Longitude: -97.10093071
  },
  {
    Latitude: 16.11689236,
    Longitude: -97.71747352
  },
  {
    Latitude: 16.44597042,
    Longitude: -95.62649011
  },
  {
    Latitude: 17.15288893,
    Longitude: -95.67677451
  },
  {
    Latitude: 16.74964435,
    Longitude: -96.209283
  },
  {
    Latitude: 16.52114188,
    Longitude: -96.13531071
  },
  {
    Latitude: 17.21239893,
    Longitude: -96.65328109
  },
  {
    Latitude: 17.12010744,
    Longitude: -97.51683804
  },
  {
    Latitude: 16.17508555,
    Longitude: -96.58328194
  },
  {
    Latitude: 18.14515582,
    Longitude: -96.13957786
  },
  {
    Latitude: 17.16870277,
    Longitude: -97.54863793
  },
  {
    Latitude: 16.36075874,
    Longitude: -96.42288463
  },
  {
    Latitude: 16.21035288,
    Longitude: -95.62111542
  },
  {
    Latitude: 17.36388874,
    Longitude: -96.68655979
  },
  {
    Latitude: 15.94721137,
    Longitude: -96.95266766
  },
  {
    Latitude: 16.34644347,
    Longitude: -96.17162444
  },
  {
    Latitude: 16.84409971,
    Longitude: -97.5512858
  },
  {
    Latitude: 16.91941547,
    Longitude: -96.75888588
  },
  {
    Latitude: 17.21522553,
    Longitude: -97.8655276
  },
  {
    Latitude: 17.09224947,
    Longitude: -97.02218226
  },
  {
    Latitude: 17.06318811,
    Longitude: -95.95858602
  },
  {
    Latitude: 15.9752945,
    Longitude: -96.44468145
  },
  {
    Latitude: 15.78596642,
    Longitude: -96.76586128
  },
  {
    Latitude: 17.33843212,
    Longitude: -97.58041984
  },
  {
    Latitude: 18.04415057,
    Longitude: -96.71827683
  },
  {
    Latitude: 16.62915366,
    Longitude: -95.88359618
  },
  {
    Latitude: 16.81256185,
    Longitude: -95.38243576
  },
  {
    Latitude: 16.84038866,
    Longitude: -96.67658596
  },
  {
    Latitude: 17.98076773,
    Longitude: -97.34575833
  },
  {
    Latitude: 17.97682097,
    Longitude: -96.79670492
  },
  {
    Latitude: 16.089008,
    Longitude: -96.40175988
  },
  {
    Latitude: 16.86071635,
    Longitude: -95.37451566
  },
  {
    Latitude: 16.02262662,
    Longitude: -96.23038744
  },
  {
    Latitude: 16.39529254,
    Longitude: -96.87230203
  },
  {
    Latitude: 17.64008789,
    Longitude: -96.53111548
  },
  {
    Latitude: 17.19774442,
    Longitude: -96.73628067
  },
  {
    Latitude: 17.31661066,
    Longitude: -96.27217831
  },
  {
    Latitude: 18.23082585,
    Longitude: -96.24260698
  },
  {
    Latitude: 17.76540078,
    Longitude: -96.19024721
  },
  {
    Latitude: 16.29976253,
    Longitude: -97.77568819
  },
  {
    Latitude: 17.74320035,
    Longitude: -96.35433789
  },
  {
    Latitude: 16.75638029,
    Longitude: -96.0806173
  },
  {
    Latitude: 16.14880918,
    Longitude: -96.94885959
  },
  {
    Latitude: 17.0067327,
    Longitude: -96.8899715
  },
  {
    Latitude: 16.33715079,
    Longitude: -95.67044054
  },
  {
    Latitude: 17.20878414,
    Longitude: -95.40948066
  },
  {
    Latitude: 17.57240906,
    Longitude: -96.86621332
  },
  {
    Latitude: 17.10044242,
    Longitude: -95.56890331
  },
  {
    Latitude: 16.3246671,
    Longitude: -97.3573322
  },
  {
    Latitude: 15.96899417,
    Longitude: -96.08967131
  },
  {
    Latitude: 16.75484004,
    Longitude: -97.63669539
  },
  {
    Latitude: 17.33842402,
    Longitude: -96.74802518
  },
  {
    Latitude: 16.21480865,
    Longitude: -95.56607136
  },
  {
    Latitude: 16.46167476,
    Longitude: -97.43205774
  },
  {
    Latitude: 17.93489556,
    Longitude: -96.36961625
  },
  {
    Latitude: 16.27506088,
    Longitude: -97.54246429
  },
  {
    Latitude: 16.27462673,
    Longitude: -97.28372477
  },
  {
    Latitude: 17.1311182,
    Longitude: -95.72716936
  },
  {
    Latitude: 16.55827783,
    Longitude: -97.18002697
  },
  {
    Latitude: 16.56776825,
    Longitude: -97.51485767
  },
  {
    Latitude: 17.60985646,
    Longitude: -96.17258164
  },
  {
    Latitude: 16.99880724,
    Longitude: -97.61601782
  },
  {
    Latitude: 17.80288486,
    Longitude: -96.743204
  },
  {
    Latitude: 16.75007017,
    Longitude: -96.21429211
  },
  {
    Latitude: 15.70396247,
    Longitude: -96.58506475
  },
  {
    Latitude: 15.64678736,
    Longitude: -96.55458983
  },
  {
    Latitude: 16.30085067,
    Longitude: -96.04405317
  },
  {
    Latitude: 16.41775186,
    Longitude: -97.73668139
  },
  {
    Latitude: 17.55504412,
    Longitude: -96.81515922
  },
  {
    Latitude: 17.62866108,
    Longitude: -97.48432464
  },
  {
    Latitude: 17.91971427,
    Longitude: -96.34949739
  },
  {
    Latitude: 16.05997191,
    Longitude: -96.09789621
  },
  {
    Latitude: 16.58668917,
    Longitude: -97.42237567
  },
  {
    Latitude: 15.95582538,
    Longitude: -96.7324269
  },
  {
    Latitude: 16.50096343,
    Longitude: -96.78121055
  },
  {
    Latitude: 16.00488112,
    Longitude: -96.38690279
  },
  {
    Latitude: 17.63933907,
    Longitude: -97.69952707
  },
  {
    Latitude: 17.09722396,
    Longitude: -96.22103409
  },
  {
    Latitude: 18.03714789,
    Longitude: -95.89269169
  },
  {
    Latitude: 15.77646068,
    Longitude: -96.38382923
  },
  {
    Latitude: 15.95058099,
    Longitude: -96.14881619
  },
  {
    Latitude: 17.61188452,
    Longitude: -97.78413247
  },
  {
    Latitude: 16.47330649,
    Longitude: -95.39517098
  },
  {
    Latitude: 17.33977222,
    Longitude: -95.4647247
  },
  {
    Latitude: 16.68604079,
    Longitude: -96.38747887
  },
  {
    Latitude: 16.09908882,
    Longitude: -96.76607468
  },
  {
    Latitude: 17.11047714,
    Longitude: -96.29095996
  },
  {
    Latitude: 16.65596436,
    Longitude: -97.4315821
  },
  {
    Latitude: 17.39642023,
    Longitude: -97.81270296
  },
  {
    Latitude: 16.89370236,
    Longitude: -95.58308823
  },
  {
    Latitude: 18.28538229,
    Longitude: -96.79442445
  },
  {
    Latitude: 15.82623127,
    Longitude: -96.90642571
  },
  {
    Latitude: 16.92843095,
    Longitude: -97.68269456
  },
  {
    Latitude: 17.49562705,
    Longitude: -95.9631349
  },
  {
    Latitude: 15.79611133,
    Longitude: -96.03337457
  },
  {
    Latitude: 18.03902837,
    Longitude: -97.32580216
  },
  {
    Latitude: 17.49090286,
    Longitude: -97.42916754
  },
  {
    Latitude: 17.06423032,
    Longitude: -96.07963096
  },
  {
    Latitude: 16.50170195,
    Longitude: -97.26828374
  },
  {
    Latitude: 17.30772323,
    Longitude: -97.48816922
  },
  {
    Latitude: 16.02907108,
    Longitude: -96.51053723
  },
  {
    Latitude: 16.53822665,
    Longitude: -96.9765266
  },
  {
    Latitude: 16.44539196,
    Longitude: -97.09329791
  },
  {
    Latitude: 16.72679922,
    Longitude: -97.91871562
  },
  {
    Latitude: 16.24579942,
    Longitude: -97.14697398
  },
  {
    Latitude: 17.58052706,
    Longitude: -96.92501904
  },
  {
    Latitude: 17.24530054,
    Longitude: -97.82109837
  },
  {
    Latitude: 16.31821038,
    Longitude: -97.49902261
  },
  {
    Latitude: 16.60337913,
    Longitude: -97.78843696
  },
  {
    Latitude: 18.07917895,
    Longitude: -96.22776521
  },
  {
    Latitude: 16.36059066,
    Longitude: -97.10913479
  },
  {
    Latitude: 17.56377151,
    Longitude: -95.85257297
  },
  {
    Latitude: 17.80566164,
    Longitude: -97.63280474
  },
  {
    Latitude: 17.8300777,
    Longitude: -97.19018739
  },
  {
    Latitude: 16.05853496,
    Longitude: -96.89286061
  },
  {
    Latitude: 17.80241567,
    Longitude: -97.54698077
  },
  {
    Latitude: 16.96738948,
    Longitude: -97.51486508
  },
  {
    Latitude: 17.60591863,
    Longitude: -95.97888727
  },
  {
    Latitude: 16.40352468,
    Longitude: -97.38551913
  },
  {
    Latitude: 16.65591638,
    Longitude: -97.68341473
  },
  {
    Latitude: 16.02020667,
    Longitude: -96.45576714
  },
  {
    Latitude: 16.35532294,
    Longitude: -95.83313097
  },
  {
    Latitude: 17.04342142,
    Longitude: -95.85307434
  },
  {
    Latitude: 18.27563926,
    Longitude: -96.39520103
  },
  {
    Latitude: 17.2087718,
    Longitude: -96.6304701
  },
  {
    Latitude: 16.24002359,
    Longitude: -97.69395416
  },
  {
    Latitude: 17.2004884,
    Longitude: -97.73463385
  },
  {
    Latitude: 16.81699512,
    Longitude: -95.84851287
  },
  {
    Latitude: 17.11204942,
    Longitude: -97.89213361
  },
  {
    Latitude: 17.14989431,
    Longitude: -97.24941715
  },
  {
    Latitude: 15.79856157,
    Longitude: -96.60095104
  },
  {
    Latitude: 16.24923245,
    Longitude: -95.75424335
  },
  {
    Latitude: 16.62924493,
    Longitude: -97.65357289
  },
  {
    Latitude: 16.41156175,
    Longitude: -97.04930428
  },
  {
    Latitude: 17.61990546,
    Longitude: -97.36821049
  },
  {
    Latitude: 18.23884747,
    Longitude: -96.57655514
  },
  {
    Latitude: 16.16454951,
    Longitude: -97.80079441
  },
  {
    Latitude: 18.21305344,
    Longitude: -97.15057392
  },
  {
    Latitude: 16.3479801,
    Longitude: -95.54427343
  },
  {
    Latitude: 16.76792971,
    Longitude: -97.82732453
  },
  {
    Latitude: 17.16302851,
    Longitude: -96.99500312
  },
  {
    Latitude: 17.35648112,
    Longitude: -97.23000234
  },
  {
    Latitude: 17.54318569,
    Longitude: -96.31574901
  },
  {
    Latitude: 18.15836039,
    Longitude: -96.63152741
  },
  {
    Latitude: 17.00946868,
    Longitude: -97.98725566
  },
  {
    Latitude: 16.02036489,
    Longitude: -97.35185073
  },
  {
    Latitude: 16.30366683,
    Longitude: -96.73412979
  },
  {
    Latitude: 17.06091165,
    Longitude: -96.08409311
  },
  {
    Latitude: 16.25107633,
    Longitude: -97.03371785
  },
  {
    Latitude: 16.72015767,
    Longitude: -97.05664288
  },
  {
    Latitude: 16.92671919,
    Longitude: -97.36851923
  },
  {
    Latitude: 17.06830556,
    Longitude: -98.01539615
  },
  {
    Latitude: 17.25354321,
    Longitude: -97.98499969
  },
  {
    Latitude: 18.16052368,
    Longitude: -96.56931346
  },
  {
    Latitude: 17.01946288,
    Longitude: -97.33621383
  },
  {
    Latitude: 17.06675285,
    Longitude: -96.1425316
  },
  {
    Latitude: 16.82206076,
    Longitude: -96.20783382
  },
  {
    Latitude: 16.51638788,
    Longitude: -95.56134198
  },
  {
    Latitude: 17.94224598,
    Longitude: -97.339826
  },
  {
    Latitude: 17.28923601,
    Longitude: -97.59794822
  },
  {
    Latitude: 17.93050205,
    Longitude: -96.39153406
  },
  {
    Latitude: 17.43285194,
    Longitude: -97.79141356
  },
  {
    Latitude: 16.71589381,
    Longitude: -97.28262335
  },
  {
    Latitude: 17.67553845,
    Longitude: -97.57577753
  },
  {
    Latitude: 16.27937651,
    Longitude: -97.40992942
  },
  {
    Latitude: 16.18435776,
    Longitude: -96.67633225
  },
  {
    Latitude: 16.22305156,
    Longitude: -96.95099855
  },
  {
    Latitude: 16.61630949,
    Longitude: -98.03029442
  },
  {
    Latitude: 16.03272441,
    Longitude: -96.82469732
  },
  {
    Latitude: 16.49277131,
    Longitude: -96.85921039
  },
  {
    Latitude: 16.96438286,
    Longitude: -95.45633631
  },
  {
    Latitude: 16.01152923,
    Longitude: -96.47181507
  },
  {
    Latitude: 17.61771484,
    Longitude: -96.12254688
  },
  {
    Latitude: 16.97411237,
    Longitude: -96.13272472
  },
  {
    Latitude: 16.397771,
    Longitude: -97.04732736
  },
  {
    Latitude: 15.84295618,
    Longitude: -97.02894651
  },
  {
    Latitude: 17.05671008,
    Longitude: -96.24727015
  },
  {
    Latitude: 16.15275599,
    Longitude: -97.01356944
  },
  {
    Latitude: 16.57021633,
    Longitude: -95.90625004
  },
  {
    Latitude: 16.17292885,
    Longitude: -95.82161354
  },
  {
    Latitude: 16.81732947,
    Longitude: -95.49923347
  },
  {
    Latitude: 16.19492846,
    Longitude: -96.07270123
  },
  {
    Latitude: 16.26528158,
    Longitude: -97.80850067
  },
  {
    Latitude: 16.36753373,
    Longitude: -95.59169485
  },
  {
    Latitude: 17.15612457,
    Longitude: -97.79123551
  },
  {
    Latitude: 16.07841403,
    Longitude: -95.86700491
  },
  {
    Latitude: 16.66620391,
    Longitude: -95.81866403
  },
  {
    Latitude: 16.41603007,
    Longitude: -97.94031855
  },
  {
    Latitude: 17.35140038,
    Longitude: -96.63330317
  },
  {
    Latitude: 15.89652985,
    Longitude: -96.82569448
  },
  {
    Latitude: 16.42527769,
    Longitude: -96.68913395
  },
  {
    Latitude: 18.21987542,
    Longitude: -96.35711044
  },
  {
    Latitude: 16.7590086,
    Longitude: -95.38980735
  },
  {
    Latitude: 15.80474724,
    Longitude: -97.36542144
  },
  {
    Latitude: 16.42365686,
    Longitude: -96.34276276
  },
  {
    Latitude: 17.20036269,
    Longitude: -96.0879964
  },
  {
    Latitude: 16.54615352,
    Longitude: -97.78160147
  },
  {
    Latitude: 16.93513226,
    Longitude: -95.97494853
  },
  {
    Latitude: 17.42822724,
    Longitude: -96.90969754
  },
  {
    Latitude: 17.478784,
    Longitude: -97.35451253
  },
  {
    Latitude: 17.59200105,
    Longitude: -95.75711909
  },
  {
    Latitude: 16.04246288,
    Longitude: -97.5966737
  },
  {
    Latitude: 16.4871364,
    Longitude: -97.23378875
  },
  {
    Latitude: 16.41095868,
    Longitude: -97.7705881
  },
  {
    Latitude: 18.14715375,
    Longitude: -97.29216631
  },
  {
    Latitude: 15.81153226,
    Longitude: -96.69421779
  },
  {
    Latitude: 17.60271449,
    Longitude: -96.53828048
  },
  {
    Latitude: 16.54422369,
    Longitude: -95.38191095
  },
  {
    Latitude: 17.4903076,
    Longitude: -97.98345041
  },
  {
    Latitude: 17.49119172,
    Longitude: -97.89704013
  },
  {
    Latitude: 17.52711825,
    Longitude: -96.79997533
  },
  {
    Latitude: 16.96761183,
    Longitude: -96.74074241
  },
  {
    Latitude: 17.25012183,
    Longitude: -97.75833412
  },
  {
    Latitude: 17.25603482,
    Longitude: -96.32611907
  },
  {
    Latitude: 16.0542472,
    Longitude: -96.08225754
  },
  {
    Latitude: 18.15242516,
    Longitude: -96.78009914
  },
  {
    Latitude: 17.03668367,
    Longitude: -97.02800561
  },
  {
    Latitude: 17.70143645,
    Longitude: -95.85390186
  },
  {
    Latitude: 17.70195375,
    Longitude: -96.93607025
  },
  {
    Latitude: 17.19651162,
    Longitude: -95.65654335
  },
  {
    Latitude: 17.20230177,
    Longitude: -96.56860076
  },
  {
    Latitude: 15.77645686,
    Longitude: -96.98109691
  },
  {
    Latitude: 17.21825312,
    Longitude: -96.56516581
  },
  {
    Latitude: 17.80735845,
    Longitude: -96.21050043
  },
  {
    Latitude: 16.50932413,
    Longitude: -96.56058821
  },
  {
    Latitude: 16.99991729,
    Longitude: -97.34065333
  },
  {
    Latitude: 16.7070471,
    Longitude: -97.8188506
  },
  {
    Latitude: 17.07373968,
    Longitude: -97.61338864
  },
  {
    Latitude: 16.54881383,
    Longitude: -96.05402457
  },
  {
    Latitude: 17.16050021,
    Longitude: -97.04295774
  },
  {
    Latitude: 16.99798562,
    Longitude: -97.95417123
  },
  {
    Latitude: 17.37803058,
    Longitude: -96.8485607
  },
  {
    Latitude: 16.11110998,
    Longitude: -96.63201915
  },
  {
    Latitude: 16.57351922,
    Longitude: -95.74454823
  },
  {
    Latitude: 16.23565634,
    Longitude: -97.7063095
  },
  {
    Latitude: 17.69138643,
    Longitude: -96.15665605
  },
  {
    Latitude: 17.30138881,
    Longitude: -97.19206983
  },
  {
    Latitude: 17.35346251,
    Longitude: -97.4613571
  },
  {
    Latitude: 17.22886902,
    Longitude: -97.11731065
  },
  {
    Latitude: 16.48121445,
    Longitude: -96.41737077
  },
  {
    Latitude: 17.55592339,
    Longitude: -95.51812846
  },
  {
    Latitude: 17.08412957,
    Longitude: -98.03479627
  },
  {
    Latitude: 16.90097709,
    Longitude: -97.69226251
  },
  {
    Latitude: 17.03565186,
    Longitude: -95.47506043
  },
  {
    Latitude: 16.71247997,
    Longitude: -97.89580497
  },
  {
    Latitude: 16.2249222,
    Longitude: -96.32741988
  },
  {
    Latitude: 16.43434268,
    Longitude: -97.17981447
  },
  {
    Latitude: 17.09640229,
    Longitude: -96.54441538
  },
  {
    Latitude: 16.82833411,
    Longitude: -97.09160617
  },
  {
    Latitude: 17.19591176,
    Longitude: -95.55084192
  },
  {
    Latitude: 16.97850472,
    Longitude: -95.76815635
  },
  {
    Latitude: 16.15740353,
    Longitude: -97.0863235
  },
  {
    Latitude: 18.07122959,
    Longitude: -97.45318133
  },
  {
    Latitude: 18.00520811,
    Longitude: -96.10274864
  },
  {
    Latitude: 17.48479214,
    Longitude: -95.79937248
  },
  {
    Latitude: 18.18786312,
    Longitude: -96.67220683
  },
  {
    Latitude: 17.07496626,
    Longitude: -97.5713144
  },
  {
    Latitude: 17.14442155,
    Longitude: -97.71474877
  },
  {
    Latitude: 16.56337061,
    Longitude: -97.77469047
  },
  {
    Latitude: 17.86355211,
    Longitude: -96.84315194
  },
  {
    Latitude: 16.62827172,
    Longitude: -96.05233738
  },
  {
    Latitude: 18.06288058,
    Longitude: -97.10986046
  },
  {
    Latitude: 16.70433097,
    Longitude: -97.79270064
  },
  {
    Latitude: 18.00500423,
    Longitude: -96.70766443
  },
  {
    Latitude: 16.43317893,
    Longitude: -96.06884771
  },
  {
    Latitude: 16.45572916,
    Longitude: -95.53524875
  },
  {
    Latitude: 16.76507221,
    Longitude: -97.55109253
  },
  {
    Latitude: 18.07584472,
    Longitude: -97.13038628
  },
  {
    Latitude: 17.23329398,
    Longitude: -97.06348117
  },
  {
    Latitude: 17.00681228,
    Longitude: -97.40521281
  },
  {
    Latitude: 16.97490379,
    Longitude: -96.77056901
  },
  {
    Latitude: 17.57234626,
    Longitude: -95.93222904
  },
  {
    Latitude: 17.09236866,
    Longitude: -97.55619676
  },
  {
    Latitude: 15.70166001,
    Longitude: -96.59811944
  },
  {
    Latitude: 16.20127873,
    Longitude: -97.23895814
  },
  {
    Latitude: 15.64561636,
    Longitude: -96.50334346
  },
  {
    Latitude: 17.2434535,
    Longitude: -96.18084605
  },
  {
    Latitude: 16.66955169,
    Longitude: -97.21335241
  },
  {
    Latitude: 15.72965247,
    Longitude: -96.73234873
  },
  {
    Latitude: 17.7206625,
    Longitude: -96.29104081
  },
  {
    Latitude: 15.75303761,
    Longitude: -96.72787379
  },
  {
    Latitude: 17.85585471,
    Longitude: -95.92986107
  },
  {
    Latitude: 16.39882417,
    Longitude: -96.37243704
  },
  {
    Latitude: 16.26448187,
    Longitude: -97.78401668
  },
  {
    Latitude: 16.94683365,
    Longitude: -95.54519344
  },
  {
    Latitude: 17.63752975,
    Longitude: -95.983977
  },
  {
    Latitude: 17.00875018,
    Longitude: -97.46471827
  },
  {
    Latitude: 17.2057247,
    Longitude: -98.07735152
  },
  {
    Latitude: 16.46832349,
    Longitude: -96.71096808
  },
  {
    Latitude: 16.67815446,
    Longitude: -97.0470748
  },
  {
    Latitude: 16.2923116,
    Longitude: -97.43812536
  },
  {
    Latitude: 17.5412535,
    Longitude: -96.9953676
  },
  {
    Latitude: 15.80190179,
    Longitude: -96.90829366
  },
  {
    Latitude: 17.60961588,
    Longitude: -97.7466882
  },
  {
    Latitude: 17.32393416,
    Longitude: -96.08260931
  },
  {
    Latitude: 17.23361001,
    Longitude: -96.77113485
  },
  {
    Latitude: 16.99055532,
    Longitude: -96.68502799
  },
  {
    Latitude: 16.60174719,
    Longitude: -97.83255729
  },
  {
    Latitude: 17.43118989,
    Longitude: -97.58315891
  },
  {
    Latitude: 16.87617824,
    Longitude: -97.10298166
  },
  {
    Latitude: 17.14300447,
    Longitude: -96.7467693
  },
  {
    Latitude: 17.34927039,
    Longitude: -96.06667782
  },
  {
    Latitude: 16.77833742,
    Longitude: -97.23311861
  },
  {
    Latitude: 16.92309189,
    Longitude: -95.33932185
  },
  {
    Latitude: 16.54516162,
    Longitude: -96.54611711
  },
  {
    Latitude: 16.18337385,
    Longitude: -96.34524937
  },
  {
    Latitude: 17.0905242,
    Longitude: -96.40097997
  },
  {
    Latitude: 17.89454764,
    Longitude: -96.76652002
  },
  {
    Latitude: 17.640899,
    Longitude: -97.32846042
  },
  {
    Latitude: 17.3428052,
    Longitude: -96.54984313
  },
  {
    Latitude: 18.24929085,
    Longitude: -96.46210877
  },
  {
    Latitude: 17.49094642,
    Longitude: -95.85453124
  },
  {
    Latitude: 16.80292371,
    Longitude: -97.99235452
  },
  {
    Latitude: 15.75832393,
    Longitude: -96.81362078
  },
  {
    Latitude: 17.2683834,
    Longitude: -96.45401038
  },
  {
    Latitude: 17.26188358,
    Longitude: -96.03778183
  },
  {
    Latitude: 17.70244811,
    Longitude: -97.02781988
  },
  {
    Latitude: 17.41816481,
    Longitude: -96.4589147
  },
  {
    Latitude: 16.21893054,
    Longitude: -97.78839276
  },
  {
    Latitude: 16.93678238,
    Longitude: -95.67590224
  },
  {
    Latitude: 17.93002046,
    Longitude: -97.44699879
  },
  {
    Latitude: 17.74711108,
    Longitude: -96.61397857
  },
  {
    Latitude: 16.34664215,
    Longitude: -96.35615451
  },
  {
    Latitude: 17.65439123,
    Longitude: -96.73709212
  },
  {
    Latitude: 17.49733762,
    Longitude: -95.89815934
  },
  {
    Latitude: 16.56630755,
    Longitude: -97.28134916
  },
  {
    Latitude: 16.56314013,
    Longitude: -97.9904819
  },
  {
    Latitude: 16.87667238,
    Longitude: -97.18754053
  },
  {
    Latitude: 16.86336,
    Longitude: -95.92463697
  },
  {
    Latitude: 15.9092517,
    Longitude: -96.7640621
  },
  {
    Latitude: 17.32604417,
    Longitude: -96.07831589
  },
  {
    Latitude: 17.53634396,
    Longitude: -97.55819367
  },
  {
    Latitude: 16.61933375,
    Longitude: -97.34732158
  },
  {
    Latitude: 17.62011013,
    Longitude: -97.097806
  },
  {
    Latitude: 16.07821664,
    Longitude: -97.48238598
  },
  {
    Latitude: 16.57503215,
    Longitude: -95.94521371
  },
  {
    Latitude: 16.1565666,
    Longitude: -95.71331541
  },
  {
    Latitude: 17.5828732,
    Longitude: -97.18306114
  },
  {
    Latitude: 17.42008087,
    Longitude: -97.53566098
  },
  {
    Latitude: 15.89343546,
    Longitude: -96.54350224
  },
  {
    Latitude: 16.46163651,
    Longitude: -95.43438182
  },
  {
    Latitude: 17.43489281,
    Longitude: -96.30619032
  },
  {
    Latitude: 17.18363515,
    Longitude: -95.29884882
  },
  {
    Latitude: 16.38113096,
    Longitude: -97.94365543
  },
  {
    Latitude: 17.47655692,
    Longitude: -95.9936768
  },
  {
    Latitude: 16.79233664,
    Longitude: -95.38438587
  },
  {
    Latitude: 16.137272,
    Longitude: -96.01932226
  },
  {
    Latitude: 17.83577776,
    Longitude: -96.57098106
  },
  {
    Latitude: 16.76893357,
    Longitude: -95.68929389
  },
  {
    Latitude: 17.59934036,
    Longitude: -97.05222885
  },
  {
    Latitude: 15.84103408,
    Longitude: -96.12034934
  },
  {
    Latitude: 16.70746245,
    Longitude: -96.48486831
  },
  {
    Latitude: 17.30631092,
    Longitude: -96.96520833
  },
  {
    Latitude: 17.38842567,
    Longitude: -96.67543123
  },
  {
    Latitude: 17.41424665,
    Longitude: -95.41995546
  },
  {
    Latitude: 17.56012015,
    Longitude: -96.2507661
  },
  {
    Latitude: 16.93985462,
    Longitude: -97.84133527
  },
  {
    Latitude: 17.01171534,
    Longitude: -96.48466989
  },
  {
    Latitude: 16.49141135,
    Longitude: -95.39926122
  },
  {
    Latitude: 17.56267104,
    Longitude: -97.73259679
  },
  {
    Latitude: 17.53105948,
    Longitude: -95.75145388
  },
  {
    Latitude: 18.05227383,
    Longitude: -97.22203334
  },
  {
    Latitude: 16.53268563,
    Longitude: -96.71289362
  },
  {
    Latitude: 17.38369672,
    Longitude: -96.85418965
  },
  {
    Latitude: 16.21588044,
    Longitude: -95.72648678
  },
  {
    Latitude: 16.92345663,
    Longitude: -96.00570605
  },
  {
    Latitude: 16.14731272,
    Longitude: -96.08289006
  },
  {
    Latitude: 17.67828126,
    Longitude: -97.28542032
  },
  {
    Latitude: 17.84881316,
    Longitude: -95.87991323
  },
  {
    Latitude: 17.02167444,
    Longitude: -95.6482444
  },
  {
    Latitude: 16.50998762,
    Longitude: -95.55751982
  },
  {
    Latitude: 18.03038148,
    Longitude: -96.79190325
  },
  {
    Latitude: 17.95184166,
    Longitude: -96.1220492
  },
  {
    Latitude: 16.39922156,
    Longitude: -97.12702206
  },
  {
    Latitude: 17.93403863,
    Longitude: -96.47842331
  },
  {
    Latitude: 16.0324695,
    Longitude: -96.7780839
  },
  {
    Latitude: 18.00773449,
    Longitude: -95.87995792
  },
  {
    Latitude: 16.67135405,
    Longitude: -97.30024115
  },
  {
    Latitude: 16.33878732,
    Longitude: -97.54718395
  },
  {
    Latitude: 16.2925648,
    Longitude: -96.98243936
  },
  {
    Latitude: 17.52595212,
    Longitude: -96.37101487
  },
  {
    Latitude: 17.25402992,
    Longitude: -96.24663046
  },
  {
    Latitude: 15.90552746,
    Longitude: -95.97299517
  },
  {
    Latitude: 15.77328606,
    Longitude: -96.85850663
  },
  {
    Latitude: 16.03267764,
    Longitude: -97.26034329
  },
  {
    Latitude: 17.90821909,
    Longitude: -95.85922275
  },
  {
    Latitude: 17.23921342,
    Longitude: -95.50175464
  },
  {
    Latitude: 16.34800117,
    Longitude: -95.64451404
  },
  {
    Latitude: 16.18952499,
    Longitude: -95.7279331
  },
  {
    Latitude: 16.61941257,
    Longitude: -96.87586492
  },
  {
    Latitude: 17.2074223,
    Longitude: -97.22504281
  },
  {
    Latitude: 15.78813875,
    Longitude: -97.22600441
  },
  {
    Latitude: 15.93411337,
    Longitude: -96.08829444
  },
  {
    Latitude: 17.27241367,
    Longitude: -97.19346725
  },
  {
    Latitude: 17.68713366,
    Longitude: -97.47239149
  },
  {
    Latitude: 17.99618046,
    Longitude: -96.61995518
  },
  {
    Latitude: 16.47653914,
    Longitude: -97.76311211
  },
  {
    Latitude: 17.51319743,
    Longitude: -97.43355881
  },
  {
    Latitude: 16.99623145,
    Longitude: -95.40316103
  },
  {
    Latitude: 17.2765856,
    Longitude: -96.50780967
  },
  {
    Latitude: 15.71068985,
    Longitude: -96.49600951
  },
  {
    Latitude: 17.84090339,
    Longitude: -96.76424604
  },
  {
    Latitude: 16.51011344,
    Longitude: -96.6423644
  },
  {
    Latitude: 16.68794831,
    Longitude: -97.02620203
  },
  {
    Latitude: 17.20043921,
    Longitude: -97.46861641
  },
  {
    Latitude: 17.42848546,
    Longitude: -96.93903861
  },
  {
    Latitude: 18.15476783,
    Longitude: -96.97402205
  },
  {
    Latitude: 17.11861855,
    Longitude: -95.49240487
  },
  {
    Latitude: 16.76013,
    Longitude: -95.68927466
  },
  {
    Latitude: 17.45393305,
    Longitude: -97.52893985
  },
  {
    Latitude: 16.50000982,
    Longitude: -96.98771511
  },
  {
    Latitude: 16.80964331,
    Longitude: -96.77300009
  },
  {
    Latitude: 16.38016918,
    Longitude: -96.07802864
  },
  {
    Latitude: 18.1602851,
    Longitude: -96.43768798
  },
  {
    Latitude: 17.28694704,
    Longitude: -95.31825992
  },
  {
    Latitude: 16.59973971,
    Longitude: -96.74371873
  },
  {
    Latitude: 16.24521006,
    Longitude: -97.41015244
  },
  {
    Latitude: 16.82268472,
    Longitude: -97.15670642
  },
  {
    Latitude: 18.08036297,
    Longitude: -97.45888375
  },
  {
    Latitude: 16.86078412,
    Longitude: -95.47201098
  },
  {
    Latitude: 17.5684214,
    Longitude: -96.7774443
  },
  {
    Latitude: 16.99139674,
    Longitude: -96.45179015
  },
  {
    Latitude: 18.17364691,
    Longitude: -96.54853491
  },
  {
    Latitude: 17.50459994,
    Longitude: -97.14321636
  },
  {
    Latitude: 16.0915005,
    Longitude: -96.44828932
  },
  {
    Latitude: 16.37055557,
    Longitude: -96.00357435
  },
  {
    Latitude: 17.57555667,
    Longitude: -97.90228921
  },
  {
    Latitude: 15.73237186,
    Longitude: -97.10815375
  },
  {
    Latitude: 16.04628978,
    Longitude: -95.77233284
  },
  {
    Latitude: 16.97430722,
    Longitude: -96.13427597
  },
  {
    Latitude: 18.08365188,
    Longitude: -96.85598547
  },
  {
    Latitude: 17.06692335,
    Longitude: -98.04823291
  },
  {
    Latitude: 16.47752764,
    Longitude: -97.56294826
  },
  {
    Latitude: 17.44901327,
    Longitude: -96.35364132
  },
  {
    Latitude: 16.18010973,
    Longitude: -95.77486763
  },
  {
    Latitude: 16.77439317,
    Longitude: -97.97688113
  },
  {
    Latitude: 17.93812987,
    Longitude: -96.56610408
  },
  {
    Latitude: 16.81435733,
    Longitude: -97.58314826
  }
]
