Vue.use(VeeValidate);
new Vue({
	el:'#participante',
	components: {
        Multiselect: window.VueMultiselect.default,
        vuejsDatepicker
    },
	data:{
		
        offset              :3,
        participantesList 	: [],
		titulo 				: 'Nuevo',
		editMode 			: true,
		detalle 			: false,
		busquedaDatos 		: '',
		pagination:{
            total        :0, 
            current_page :0, 
            per_page     :0, 
            last_page    :0, 
            from         :0, 
            to           :0
        },

		datosInicio	: {
			hombres	: '',
			mujeres	: '',
			menorH 	: '',
			menorM	: ''
		},
		newParticipante 	:{
			nombre 				: '',
			primer_apellido 	: '',
			segundo_apellido 	: '',
			fecha_nacimiento 	: '',
			genero 				: '',
			numero_local		: '',
			idEvento 			: '',
			email 				: '',
			curp 				: ''
		}
	},
	mounted (){
			let me = this;
			me.getDatos();
	},
	computed:{
        isActived:function(){
            return this.pagination.current_page;
        },
        pagesNumber: function(){
            if(!this.pagination.to){
                return[];
            }
            var from = this.pagination.current_page - this.offset;
            if(from < 1){
                from = 1;
            }
            var to = from + (this.offset*2);
            if(to >= this.pagination.last_page){
                to = this.pagination.last_page;
            }

            var pageArray = []
            while(from <= to){
                pageArray.push(from);
                from++;
            }
            return pageArray;
        }
    },
	methods:{
		changePage: function(page){
            this.pagination.current_page = page;
            this.getListaParticipantes(page)
        },
		getDatos(){
			block();
			let me = this;
			var url = "/orgeventos/participantes/lista";
			axios.get(url).then(function(response){
				unblock();
				me.datosInicio = response.data
				me.getListaParticipantes();
			}).catch(function(error){
				location.reload();
			})
		},
		getListaParticipantes(page){
			let me = this;
			var url = page?"/orgeventos/participantes/listall?page="+page:"/orgeventos/participantes/listall?page=1";
			axios.get(url).then(function(response){
				me.participantesList = response.data.lista.data
				me.pagination = response.data.pagination
			}).catch(function(error){
				location.reload();
				console.error("Epic fail")
			})
		},
		participanteShow(participante){
			let me = this;
			var url = "/orgeventos/participantes/showPersona/"+participante.persona_id;
			axios.get(url).then(function(response){
				me.newParticipante = response.data;
			}).catch(function(error){
				// location.reload();
				console.error("Epic fail")
			})
			// me.newParticipante = participante;
		},
		resetNewParticipante(){
			let me = this;
			me.newParticipante 	= {
				nombre 				: '',
				primer_apellido 	: '',
				segundo_apellido 	: '',
				fecha_nacimiento 	: '',
				genero 				: '',
				numero_local		: '',
				idEvento 			: '',
				email 				: '',
				curp 				: ''
			}
			me.$validator.reset();
		},
		/*saveParticipante(){
			let me = this;				
            this.$validator.validateAll().then((result) => {
                if(result) {

                	var fecha = JSON.stringify(me.newParticipante.fecha_nacimiento);
					var curp = me.newParticipante.curp = me.newParticipante.primer_apellido.substring(0, 2)+me.newParticipante.segundo_apellido.substring(0,1)+me.newParticipante.nombre.substring(0,1)+fecha.substring(3, 5)+fecha.substring(6,8)+fecha.substring(9,11)+me.newParticipante.genero;
					let participantes = this.participantesList;
					participantes = participantes.filter(participante => {
						return participante.curp.toLowerCase().match(curp.toLowerCase());
						})
					if (participantes.length > 0) {
						swal("Participante ya se encuentra registrado")
						return;
					}
                	me.newParticipante.curp = curp;
                	block();
					var url = "/orgeventos/participantes/registrar"
					axios.post(url,me.newParticipante).then(function (response) {
						unblock();
						me.notificacion('success','Se guardo correctamente');
						$('#modalParticipante').modal('toggle');
						me.resetNewParticipante();
						me.getListaParticipantes();
						me.getDatos();
					}).catch(function (error){
						$.each((error.response.data.errors),function(index,value){
							me.notificacion('error',value[0])
						})
					})
                }
            });
		},*/
		updateParticipante(){
			let me = this;	
			this.$validator.validateAll().then((result) => {
                if(result) {
                	block();
					var url = "/orgeventos/participantes/actualizar";
					axios.put(url,me.newParticipante).then(function (response) {
						unblock();
						if (response.data.success) {
							me.notificacion('success','Se actualizo correctamente');
							$('#modalParticipante').modal('toggle');
							me.resetNewParticipante();
							me.getListaParticipantes();
							me.getDatos();
						}else{
							me.notificacion('error','Error al actualizar');

						}
						
					}).catch(function (error){
						$.each((error.response.data.errors),function(index,value){
							me.notificacion('error',value[0])
						})
					})
                }
            });

		},
		notificacion(tipo,mensaje){
			unblock();
			swal({
                  position: 'top-end',
                  type: tipo,
                  title: mensaje,
                  showConfirmButton: false,
                  timer: 1700
            })
		},
		busquedaParticipante(){
            block();
            let me = this;
			var url = "/orgeventos/participantes/busqueda"
            axios.post(url, {dato:me.busquedaDatos}).then( function(response) {
                 unblock();

                if (response.data.lista.data.length > 0) {
                    me.participantesList=response.data.lista.data
                    me.pagination = response.data.pagination
                }else{
                    swal({
                      type: 'info',
                      title: 'Oops...',
                      text: 'No se encontraron resultados!',
                      // footer: 'Contacte al Administrador',
                      showConfirmButton: false,
                      timer: 1700
                    })
                }
            }).catch(function(error) {
                unblock();
                console.log("error "+error)
            })
        },
	}

})