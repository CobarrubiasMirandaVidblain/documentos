Vue.use(VueTables.ClientTable);
Vue.use(VeeValidate);
    var credencial = new Vue ({
        components: {
            Multiselect: window.VueMultiselect.default
        },
        el:'#dCredencial',
        data:{            
            pCredencial:{
                curp                :"",
                nombre              :"",
                primer_apellido     :"",
                segundo_apellido    :"",
                genero              :"",
                clave_electoral     :"",
                referencia_domicilio:"",
                calle               :"",     
                colonia             :"",
                numero_exterior     :"",
                numero_interior     :"",
                codigopostal        :"",
                numero_celular      :"",
                numero_local        :"",
                email               :"",
                municipio_id        :"",
                localidad_id        :"",
                fecha_nacimiento    :"", // hasta voy cambiando noombres
                imagen              :"",
                imagensrc           :"",
                firma               :"",
                nombreC             :"",
                apellidoC           :"",
                apellidoSC          :"",
                calleC              :"",
                coloniaC            :"",
                numeroC             :"",
                cpC                 :"",
                muniC               :"",
                locC                :"",
                telefonoC           :"",
                institucion         :""
            },
            listIntituciones        :"",
            listaMunicipios         :[],
            listaLocalidad          :[],
            listaLocalidadC         :[],
            nombreMuni              :"",
            nombreLoc               :"",
            listaPersonas           :[],
            editMode                :false,
            solicitudMode           :false,
            imageUrl                :"",
            imageUrl3               :"",
            imagen2                 :"",
            imagen3                 :"",
            comprobante             :"",
            imgvista                :"",
            descripcion             :"",
            municipioC              :"",
            localidadC              :"",
            offset              :3,
            busquedaDatos :{
                nombrePersona : "",
                curpPersona   : "" 
						},
						columns: ['nombre','primerApellido','segundoApellido','fechaNacimiento','genero','telefono','opciones'],
						options: {
							filterable: ["folio", "nombre", "fecha","estatus","primerApellido","segundoApellido","fechaNacimiento","genero","telefono"],
							texts: {
								count:
									" {from} a {to} de {count} registros|{count} Registros|1 Registro",
								first: "First",
								last: "Last",
								filter: "Filtrar:",
								filterPlaceholder: "Buscar",
								limit: "Registros:",
								page: "Pagina:",
								noResults: "No se encontraron coincidencias",
								filterBy: "Filtrar",
								loading: "Cargando...",
								defaultOption: "Seleccionar {column}",
								columns: "Columnas"
							},
							highlightMatches: true,
							filterByColumn: true
						}
						

        },
        computed:{
        },
        methods:{
            modal(){
                this.resetDatos();
                this.errors.clear();
                $('#modalDcredencial').modal('toggle');
            },
            validateBeforeSubmit() {
              this.$validator.validateAll().then((result) => {
                if (result) {
									this.savepCredencial();
									return;
                }

                swal('Existen campos vacios');
              });
            },
            limpiarFirma(){
                this.pCredencial.firma=""
            },
            onFileChange(e) {
                this.imagen2 = event.target.files[0];
              var files = e.target.files || e.dataTransfer.files;
              if (!files.length)
                return;
              this.createImage(files[0]);
            },
            getImage(event){
                //Asignamos la imagen a  nuestra data
                this.imagen3 = event.target.files[0];
                this.imageUrl3 = URL.createObjectURL(this.imagen3);
            },
            createImage(file) {
              var image = new Image();
              var reader = new FileReader();
              var vm = this;

              reader.onload = (e) => {
                vm.imageUrl = e.target.result;
              };
              reader.readAsDataURL(file);
            },
            removeImage: function (e) {
              if(e == 2)
                this.imageUrl3 = '';
              else
                this.imageUrl = '';
            },
            savepCredencial:function(){
                block();
                
                let me = this;
                var url ="dcredencial/nuevo"

                var data = new  FormData();
                data.append('fotografia', this.imagen2);
                data.append('certificado', this.imagen3);
                $.each( this.pCredencial, function( key, value ) {
                  data.append(key, value);
                });
                data.append('_method', 'post');
                axios.post(url,data).then(response => {
                    unblock();
                    if (response.data.success) {
                        swal({
                          type: 'success',
                          title: 'Bien',
                          text: 'Se guardo correctamente!',
                          footer: 'Clave: '+response.data.id
                        })
                        $('#modalDcredencial').modal('toggle');
                        me.listaPCredenciales();
                    }else{
                        swal({
                          type: 'error',
                          title: 'Oops...',
                          text: 'Ocurrio un error!',
                          footer: "Error "+response.data.message
                        })
                    }
                }).catch(error => {
                    unblock();
                    swal("Error!", "Error al guardar", "warning");
                })

            },
            updatepCredencial:function(){
                let me = this;
                var url ="dcredencial/edit"

                var data = new  FormData();
                data.append('fotografia', this.imagen2);
                data.append('certificado', this.imagen3);
                $.each( this.pCredencial, function( key, value ) {
                  data.append(key, value);
                });
                data.append('_method', 'post');
                axios.post(url,data).then(response => {
                    unblock();
                    if (response.data.success) {
                        swal({
                          type: 'success',
                          title: 'Bien',
                          text: 'Se actualizo correctamente!',
                          footer: 'Clave: '+response.data.id
                        })
                        $('#modalDcredencial').modal('toggle');
                        me.listaPCredenciales();
                    }else{
                        swal({
                          type: 'error',
                          title: 'Oops...',
                          text: 'Ocurrio un error!',
                          footer: "Error "+response.data.message
                        })
                    }
                }).catch(error => {
                    unblock();
                    swal("Error!", "Error al guardar", "warning");
                })
                
            },
            resetDatos:function(){
                this.pCredencial.curp                   ="";
                this.pCredencial.nombre                 ="";
                this.pCredencial.primer_apellido        ="";
                this.pCredencial.segundo_apellido       ="";
                this.pCredencial.genero                 ="";
                this.pCredencial.clave_electoral        ="";
                this.pCredencial.referencia_domicilio   ="";
                this.pCredencial.calle                  ="";
                this.pCredencial.colonia                ="";
                this.pCredencial.numero_interior        ="";
                this.pCredencial.numero_exterior        ="";
                this.pCredencial.codigopostal           ="";
                this.pCredencial.numero_celular         ="";
                this.pCredencial.numero_local           ="";
                this.pCredencial.email                  ="";
                this.pCredencial.municipio_id           ="";
                this.pCredencial.localidad_id           ="";
                this.pCredencial.fecha_nacimiento       ="";
                this.pCredencial.imagensrc              ="";
                this.pCredencial.imagen                 ="";
                this.pCredencial.firma                  ="";
                this.pCredencial.nombreC                ="";
                this.pCredencial.apellidoC              ="";
                this.pCredencial.apellidoSC             ="";
                this.pCredencial.calleC                 ="";
                this.pCredencial.coloniaC               ="";
                this.pCredencial.numeroC                ="";
                this.pCredencial.cpC                    ="";
                this.pCredencial.muniC                  ="";
                this.pCredencial.locC                   ="";
                this.pCredencial.telefonoC              ="";
                this.pCredencial.institucion            ="";
                this.nombreMuni                         ="";
                this.nombreLoc                          ="";
                this.imagen2                            ="";
                this.imageUrl                           ="";
                this.pCredencial.bitacora               ="";
                this.descripcion                        ="";
                this.municipioC                         ="";
                this.localidadC                         ="";
                this.imagen3                            ="";
                this.imageUrl3                          ="";

            },
            listaPCredenciales:function(page){
							block();
							var url = "dcredencial/list";
							let me = this
							me.busquedaDatos.curpPersona = "";
							axios.get(url).then(response => {
								unblock();
								me.listaPersonas    = response.data.lista.data
							}).catch( error =>{
								unblock();
								swal({
									position: 'top-end',
									type: 'error',
									title: 'Error al consultar los datos',
									showConfirmButton: false,
									timer: 1700
								})			
							})
            },
            eliminarPersona:function(person){
                
                let me = this
                var url = "dcredencial/destroy/"+person.idTipo
                swal({
                  title: 'Estas seguro de eliminar',
                  text: person.nombre,
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si'
                }).then((result) => {
                  if (result.value) {
                     axios.delete(url).then(response =>{
                        swal(
                          'Eliminado!',
                          'Se elimino correctamente.',
                          'success'
                        )
                        me.listaPCredenciales();
                    }).catch(fail =>{
                        swal(
                          'Error!',
                          'Ocurrio un error al eliminar.',
                          'warning'
                        )
                    })
                  }
                })
            },
           showPerson(person){
    
                block();
                this.resetDatos()
                var url = "dcredencial/getPerson"
                if (person.curp) {
                    let me = this;
                    axios.post(url, {curp:person.curp}).then(response => {
                      unblock();
                    if(response.data.success){
                        me.editMode     = false
                        me.pCredencial  = response.data.persona
                        me.nombreMuni   = response.data.nombreMuni
                        me.nombreLoc    = response.data.nombreLoc
                        me.imageUrl     = response.data.persona.imagensrc
                        me.localidadC   = response.data.localidadC
                        me.municipioC   = response.data.municipioC
                        me.imageUrl3    = response.data.persona.imageUrl3
                        $('#modalDcredencial').modal('show');
                        unblock();
                    }
                    }).catch(error => {
                        unblock();
                         swal({
                              type: 'error',
                              title: 'Oops...',
                              text: 'Ocurrio un error!',
                              footer: 'Contacte al Administrador',
                              showConfirmButton: false,
                              timer: 1700
                            })
                    })
                    }else{
                        unblock();
                        swal({
                              type: 'error',
                              title: 'Oops...',
                              text: 'Ocurrio un error!',
                              footer: 'La persona no tiene CURP',
                              showConfirmButton: false,
                              timer: 1700
                            })
                    }                
            },
            getMunicipios:function(){
                var response
                var url ="dcredencial/listMunicipio";
                let me = this;
                axios.get(url).then(response => {
                  me.listaMunicipios=response.data
                }).catch(error => {
                     
                   console.log("Error al consultar los municipios")
                })
            },
            solicitudUpdate:function(person){
                
                let me = this
                me.resetDatos()
                me.descripcion = ""
                me.pCredencial.curp = person.curp
                me.pCredencial.bitacora = person.id
                $('#modalDcredencial').modal('toggle');
            },
            saveSolicitud:function(){
                if (this.descripcion == '' || this.imageUrl == '') {
                    swal("Tiene campos vacios")
                    return;
                }
                block();
                let me = this
                var url = "dcredencial/solicitud" 
                var data = new  FormData();
                data.append('comprobante', me.imagen2);
                data.append('id', me.pCredencial.bitacora);
                data.append('dato', me.descripcion);
                data.append('_method', 'post');
                    axios.post(url,data).then(response => {
                        unblock();
                        if (response.data.success) 
                            {
                                swal({
                                  type: 'success',
                                  title: 'Bien',
                                  text: 'Se envio la solicitud!',
                                  showConfirmButton: false,
                                  timer: 1700
                                  // footer: response.data.id,
                                });
                                $('#modalDcredencial').modal('toggle');
                            }else{
                              swal({
                                  type: 'error',
                                  title: 'Oops...',
                                  text: 'Ocurrio un error!',
                                  footer: 'Intente de nuevo',
                                  showConfirmButton: false,
                                  timer: 1700
                                })  
                            }
                        }).catch(error => {
                            unblock();
                            swal({
                                  type: 'error',
                                  title: 'Oops...',
                                  text: 'Ocurrio un error!',
                                  footer: 'Intente de nuevo',
                                  showConfirmButton: false,
                                  timer: 1700
                                }) 
                        })
               
            },
            reimprimir:function(person){
                swal({
                  title: 'Estas seguro?',
                  text: "Se va a reimprimir la credencial de "+person.nombre,
                  type: 'info',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Si, reimprimir'
                }).then((result) => {
                    let me = this
                    var url =  "dcredencial/reimpresion" 
                    if (result.value) {
                      axios.post(url,{id:person.id}).then(response => {
                        if (response.data.success) {
                           swal(
                              'Correcto!',
                              'Se reimprimio correctamente.',
                              'success'
                            ) 
                           me.listaPCredenciales()
                        }else{
                           swal(
                              'Ocurrio un error!',
                              'No se realizo ningun cambio',
                              'error'
                            )  
                        }
                      }).catch(error =>{
                          swal(
                                'Ocurrio un error!',
                                'Intente de nuevo',
                                'error'
                              ) 
                      })
                    
                    }
                    
                })
                
            },
            busquedaPersona(){
                block();
                let me = this;
                var url = "dcredencial/busquedaPersona";
                axios.post(url, me.busquedaDatos).then( response => {
                     unblock();

                    if (response.data.lista.data.length > 0) {
                        me.listaPersonas=response.data.lista.data
                    }else{
                        swal({
                          type: 'info',
                          title: 'Oops...',
                          text: 'No se encontraron resultados!',
                          // footer: 'Contacte al Administrador',
                          showConfirmButton: false,
                          timer: 1700
                        })
                    }
                }).catch(error => {
                     unblock();
                    console.log("error "+error)
                })
            },
            getInstituciones(){
                let me = this
                var url = "dcredencial/getInstituciones"
                axios.get(url).then(response =>{
                    me.listIntituciones = response.data
                }).catch(error =>{
                     
                    console.log("Error en el servidor "+error)
                })
            }
        },
       
        mounted () {
             $ ('#datepicker'). datepicker ({
                autoclose: true,
                language: 'es',
                startDate: '01-01-1900',
                endDate: '0d',
                orientation: 'bottom'
            }).on(
               ' changeDate ' , () => { credencial.pCredencial.fecha_nacimiento  =  $('#datepicker').val()}
            ),
            this.getMunicipios();
            this.listaPCredenciales();
            this.getInstituciones();
          },
        watch:{
            nombreMuni:function(){
                if (this.nombreMuni.hasOwnProperty('id')) {
                    let me = this;
                    credencial.pCredencial.municipio_id = me.nombreMuni.id
                    
                  }
            },
            municipioC:function(){
              
                if (this.municipioC.hasOwnProperty('id')) {
                    let me = this;
                    credencial.pCredencial.muniC = me.municipioC.id

                      var url ="dcredencial/listLocalidad/"+me.municipioC.id
                      axios.get(url).then(response => {
                        me.pCredencial.locC=""
                        me.listaLocalidadC=response.data
                      }).catch(error => {
                        console.log(error + " error")
                      })
                  }
            },
            nombreLoc:function(){
                if (this.nombreLoc.hasOwnProperty('id'))
                    credencial.pCredencial.localidad_id = this.nombreLoc.id
            },
            localidadC:function(){
                if (this.localidadC.hasOwnProperty('id'))
                    credencial.pCredencial.locC = this.localidadC.id
            },
            'pCredencial.curp':function(){
                let me = this
                if (this.editMode != false) {
                    if(me.pCredencial.curp.length == 18){
                        var url = "dcredencial/consulta" 
                        axios.post(url, {curp:me.pCredencial.curp}).then(response => {
                            if(response.data.success){
                                me.pCredencial  = response.data.persona
                                me.nombreLoc    = response.data.persona.localidad
                                me.nombreMuni   = response.data.persona.municipio
                            }
                            else if(response.data.status == 'Repetido'){
                                me.pCredencial.curp = ""
                                  swal({
                                      type: 'error',
                                      title: 'Oops...',
                                      text: 'La curp ya esta registrada en el programa!',
                                      footer: 'Contacte al Administrador',
                                      showConfirmButton: false,
                                      timer: 1700
                                    })
                            }

                        }).catch(error => {
                             swal({
                                  type: 'error',
                                  title: 'Oops...',
                                  text: 'Ocurrio un error!',
                                  footer: 'Contacte al Administrador',
                                  showConfirmButton: false,
                                  timer: 1700
                                })
                        })
                    }
                }
            },
            'pCredencial.municipio_id':function(){

                if(credencial.pCredencial.municipio_id != ""){
                    let me = this
                    var url ="dcredencial/listLocalidad/"+me.pCredencial.municipio_id
                    axios.get(url).then(response => {
                      me.pCredencial.localidad_id=""
                      me.listaLocalidad=response.data
                    }).catch(error => {
                      console.log("Error al consultar las localidades")
                    })
                }
            },
            
            /*'pCredencial.cvelectoral':function(){

                if (this.editMode != false) {
                    if (credencial.pCredencial.cvelectoral.length == 13) {
                    let me = this;
                    var url = "dcredencial/consultacve" 
                    axios.post(url, {cve:this.pCredencial.cvelectoral}).then(response => {
                        if (response.data.success) 
                            {
                                swal({
                                  type: 'warning',
                                  title: 'Oops...',
                                  text: 'La clave de electror ya se encuentra registrada!',
                                  footer: 'Contacte al Administrador',
                                });
                                me.pCredencial.cvelectoral = ""
                            }
                        }).catch(error => {
                            console.log("ERROR ")
                        })
                    }
                }
                
            }*/
        }
    })
// funcion para obtener la firma del disposotibo
var imgWidth;
var imgHeight;
    function StartSign()
     {      
         credencial.pCredencial.firma ="";
        var message = { "firstName": "", "lastName": "", "eMail": "", "location": "", "imageFormat": 1, "imageX": imgWidth, "imageY": imgHeight, "imageTransparency": false, "imageScaling": false, "maxUpScalePercent": 0.0, "rawDataFormat": "ENC", "minSigPoints": 25 };
            
        document.addEventListener('SignResponse', SignResponse, false);
        var messageData = JSON.stringify(message);
        var element = document.createElement("MyExtensionDataElement");
        element.setAttribute("messageAttribute", messageData);
        document.documentElement.appendChild(element);
        var evt = document.createEvent("Events");
        evt.initEvent("SignStartEvent", true, false);               
        element.dispatchEvent(evt);     
    }
    function SignResponse(event)
    {   
        var str = event.target.getAttribute("msgAttribute");
        var obj = JSON.parse(str);
        SetValues(obj, imgWidth, imgHeight);
    }
    function SetValues(objResponse, imageWidth, imageHeight)
    {
        var obj = JSON.parse(JSON.stringify(objResponse));

            if (obj.errorMsg != null && obj.errorMsg!="" && obj.errorMsg!="undefined")
            {
                swal(obj.errorMsg);
            }
            else
            {
                if (obj.isSigned)
                {
                    credencial.pCredencial.firma += "data:image/png;base64," + obj.imageData;
                }
            }
    }
