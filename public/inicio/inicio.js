
var dInicio = new Vue({
    el:'#dInicioMenu',
    data:{
        hombres     : "",
        mujeres     : "", 
        menorH      : "", 
        menorM      : "",
        datos       : "",
        labels      : "",
        color       : "",
        borde       : ""
    },
    mounted(){
        this.getDatos();
    },
    methods:{
        getDatos(){
            let me  = this;
            url     = "inicio/consulta"
            axios.get(url).then(response =>{
                console.log(response)
                
                me.hombres  =   response.data.hombres
                me.mujeres  =   response.data.mujeres
                me.menorH   =   response.data.menorH
                me.menorM   =   response.data.menorM
                me.datos    =   response.data.datos
                me.labels   =   response.data.labels
                me.color    =   response.data.color
                me.borde    =   response.data.borde
            }).catch(error =>{
                swal({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Ocurrio un error en el servidor!',
                  showConfirmButton: false,
                  timer: 1000
                })
            })
        },
    }
})
setTimeout(function(){ 

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {

            labels: dInicio.labels,
            datasets: [{
                // label: dInicio.labels,
                data: dInicio.datos,
                backgroundColor: dInicio.color,
                borderColor: dInicio.color,
                borderWidth: 1
            }]
        },
        options: {
            legend: {
                display: false
            },
            'responsive': true ,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
    // myChart.update();
    

}, 1000);

