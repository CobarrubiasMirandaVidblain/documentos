<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObservacionesSeguimiento extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_observaciones_adm';
    protected $fillable = ['seguimiento_id','observaciones','usuario_id','observaciones','created_at'];
    protected $hidden = [ 'updated_at', 'deleted_at'];

    public function seguimiento()
    {
        return $this->belongsTo('Modules\Sicodoc\Entities\SeguimientoDocumentos','seguimiento_id','id');
    }

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario', 'usuario_id', 'id');
    }


}
