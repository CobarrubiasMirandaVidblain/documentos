<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentoDetalles extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_detallesdocumentos';
    protected $fillable = ['documento_id','institucionresponsable_id','separador_institucion_cargo','copia_conocimiento','usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function atencion()
    {
      return $this->belongsTo('Modules\Sicodoc\Entities\AllResponsables','institucionresponsable_id','id');
    }
    
}
