<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;

class AllResponsables extends Model
{
    protected $table = 'sdoc_responsables_all';
    protected $fillable = ['id','area','responsable','cargo']; // id del responsable
}
