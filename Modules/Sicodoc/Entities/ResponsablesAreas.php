<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;

class ResponsablesAreas extends Model
{
    protected $table = 'responsables_areas';
    protected $fillable = ['id','area_id','area','responsable','cargo'];
}
