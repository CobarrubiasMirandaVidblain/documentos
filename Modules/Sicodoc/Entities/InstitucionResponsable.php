<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InstitucionResponsable extends Model
{
    use SoftDeletes;
    protected $table = 'cat_instituciones';
    protected $fillable = ['id','nombre','encargado','cargoencargado','tipoinstitucion_id','entidad_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
