<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class HistorialContenido extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_historial_contenido';
    protected $fillable = ['documento_id','contenido','usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
