<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estatus extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_cat_estatus';
    protected $fillable = ['nombre','color','icono','descripcion'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
