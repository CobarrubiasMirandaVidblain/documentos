<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDocumento extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_cat_tiposdocumento';
    protected $fillable = ['nombre'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
