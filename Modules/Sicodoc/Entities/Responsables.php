<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Responsables extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_responsables';
    protected $fillable = ['areas_responsables_id','instituciones_responsables_id','tipo','usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function responsableArea()
    {
      return $this->belongsTo('App\Models\AreasResponsable','areas_responsables_id','id');
    }
}
