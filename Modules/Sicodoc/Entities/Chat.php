<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_chat';
    protected $fillable = ['seguimiento_documento_id', 'area_id','texto', 'usuario_id', 'created_at'];
    protected $hidden = ['updated_at', 'deleted_at'];

    public function usuario()
    {
      return $this->belongsTo('App\Models\Usuario','usuario_id','id');
    }
}