<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Folio extends Model
{
		protected $table = 'sdoc_foliodisponible';
		protected $fillable = ['id','folio_correspondencia','numero_oficio','usuario_id'];
		protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
