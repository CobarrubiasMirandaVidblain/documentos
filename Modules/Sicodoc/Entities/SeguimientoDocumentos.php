<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeguimientoDocumentos extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_seguimiento_documentos';
    protected $fillable = ['documento_id','estatus_id','areaturnado_id','usuario_id','observaciones','created_at'];
    protected $hidden = [ 'updated_at', 'deleted_at'];
    
    protected $with = ['observacionadm'];

    public function documento()
    {
        return $this->belongsTo('Modules\Sicodoc\Entities\Documentos','documento_id','id');
    }

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario', 'usuario_id', 'id');
    }

    public function estatus()
    {
        return $this->hasOne('Modules\Sicodoc\Entities\Estatus','id','estatus_id');
    }

    public function area()
    {
        return $this->belongsTo('App\Models\Area','areaturnado_id','id');
    }

    public function observacionadm()
    {
        return $this->hasOne('Modules\Sicodoc\Entities\ObservacionesSeguimiento','seguimiento_id','id');
    }
    
    public function chat()
    {
        return $this->hasMany('Modules\Sicodoc\Entities\Chat','seguimiento_documento_id','id');
    }

}
