@extends('sicodoc::layouts.master')

@section('content')
    <div id="app">
        {{-- <h1>Hola Mundo</h1>
        <p>Esta es la vista para el modulo de: {!! config('bancadif.name') !!}</p> --}}
        {{-- <example-component></example-component> --}}
        <router-view></router-view>
    </div>
@stop
