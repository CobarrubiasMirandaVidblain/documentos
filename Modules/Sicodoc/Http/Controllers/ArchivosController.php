<?php

namespace Modules\Sicodoc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Modules\Sicodoc\Entities\Archivo;
use Modules\Sicodoc\Entities\Documentos;
use Illuminate\Http\JsonResponse;
use Modules\Sicodoc\Entities\ArchivosDocumento;

class ArchivosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sicodoc::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sicodoc::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request,$tid,$did)
    {
        //\Log::debug($request);
        // dd($request->all());
        /*$path = $request->file('file')->store('public/sicodoc_files');
        $path = str_replace('public', 'storage', $path);*/
        $file = $request->file('file');

        $path = $file->store('public/sicodoc_files');
        $path = str_replace('public', 'storage', $path);

        $fileName = $file->getClientOriginalName();
        $fileSize = $file->getClientSize();
        $fileMime = $file->getClientMimeType();
        
        /*$path = public_path().'/storage/sicodoc_files/';
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $file->move($path, $fileName);*/
        try{
        
            DB::beginTransaction();

                $archivo = Archivo::create([
                    'tipoarchivo_id' => $tid,
                    'nombre' => $fileName,
                    'url' => $path,
                    'tamanio' => $fileSize,
                    'mime' => $fileMime,
                    'usuario_id' => auth()->user()->id]);
                
                $archdocs = ArchivosDocumento::create([
                    'archivo_id' => $archivo->id,
                    'documento_id' => $did,
                    'usuario_id' => auth()->user()->id,
                ]);
            
            DB::commit();
                
            return new JsonResponse(['data' => $archivo, 'message'=>"Se guardó el archivo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sicodoc::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sicodoc::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$tid,$did)
    {

        $file = $request->file('file');

        $path = $file->store('public/sicodoc_files');
        $path = str_replace('public', 'storage', $path);

        $fileName = $file->getClientOriginalName();
        $fileSize = $file->getClientSize();
        $fileMime = $file->getClientMimeType();
        
        try{
        
            DB::beginTransaction();

                $archivo = Archivo::create([
                    'tipoarchivo_id' => $tid,
                    'nombre' => $fileName,
                    'url' => $path,
                    'tamanio' => $fileSize,
                    'mime' => $fileMime,
                    'usuario_id' => auth()->user()->id
                ]);
                
                $archdocs = ArchivosDocumento::create([
                    'archivo_id' => $archivo->id,
                    'documento_id' => $did,
                    'usuario_id' => auth()->user()->id,
                ]);
            
            DB::commit();
                
            return new JsonResponse(['data' => $archivo, 'message'=>"Se guardo el archivo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Delete the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function deleteFiles(Request $request,$tid,$did)
    {
        $documento = Documentos::with('archivos.archivo')->findOrfail($did);

        foreach ($documento->archivos as $archivo) {
            if ($archivo->archivo->tipoarchivo_id === $tid) {
                $file = $archivo->archivo->url;
                $file = str_replace('storage', 'public', $file);
                $path = storage_path('app') . '/' . $file ;
                
                if ( file_exists($path) ) unlink($path);  
                
                Archivo::find($archivo->archivo->id)->delete();
                ArchivosDocumento::find($archivo->id)->delete();
                
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    
}
