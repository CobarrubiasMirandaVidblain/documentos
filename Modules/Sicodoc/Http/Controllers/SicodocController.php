<?php

namespace Modules\Sicodoc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Empleado;
use App\Models\Modulo;
use Modules\Sicodoc\Entities\InstitucionResponsable;
use Illuminate\Http\JsonResponse;
use \Firebase\JWT\JWT;
use Illuminate\Routing\Controller;

class SicodocController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => 'index']);
        // $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR', ['only' => 'index']);
    }

    public function index()
    {
        //return view('sicodoc::index');

        $usuario = auth()->user();
        \Log::debug($usuario->id);
        $token = [
            'usuario_id' => $usuario->id,
            'modulo_id' => Modulo::where('nombre', 'like', 'SICOSED')->first()->id,
            //'modulo_id' => 100,
            // 'rol_id' => $usuario->rolesrecmat->first()->pivot->rol_id
            'rol_id' => 100,
            'area_id' => Empleado::where('persona_id',$usuario->persona_id)->first()->area_id
        ];
        $encoded = JWT::encode($token, config('app.jwt_sicodoc_token'), 'HS256');
        return view('sicodoc::index', ['sicodoc_token' => $encoded]);
    }


    public function empleados($nombre)
    {
        $personas = Empleado::select('id', 'rfc', 'persona_id', 'area_id')->with('persona:id,nombre,primer_apellido,segundo_apellido')->whereHas('persona', function($query) use($nombre){
            $query->where('nombre', 'like', "%$nombre%")->orWhere('primer_apellido', 'like', "%$nombre%")->orWhere('segundo_apellido', 'like', "$nombre%");
        })->get();
        return new JsonResponse(['data' => $personas, 'message' => 'Ok']);
    }

    public function institucione($encargado)
    {
        $encargados = InstitucionResponsable::select('id', 'nombre', 'encargado', 'cargoencargado')->where('encargado', 'like', "%$encargado%")->take(10)
        ->get();
        return new JsonResponse(['data' => $encargados, 'message' => 'Ok']);
    }

    public function permissions(){
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        $modulo = Modulo::where('nombre', 'like', 'SICOSED')->first();
        $roles = \App\Models\UsuariosRol::select('id', 'modulo_id', 'rol_id', 'usuario_id')->where('modulo_id', $modulo->id)->where('usuario_id', $usuario->id)->with('rol:id,usuario_id,nombre')->get();

        if(count($roles) <= 0) return new JsonResponse([['id' => 0, 'name' => 'VISITANTE']]);

        foreach($roles as $role){
            $role->name = $role->rol->nombre;
            $role->area_id = Empleado::where('persona_id',$usuario->persona_id)->first()->area_id;
        }
        return new JsonResponse($roles);
    }
}
