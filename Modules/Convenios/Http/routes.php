<?php

Route::group(['middleware' => 'web', 'prefix' => 'convenios', 'namespace' => 'Modules\Convenios\Http\Controllers'], function()
{
    Route::get('/{id}', 'ConveniosController@index');
    Route::get('/home', 'ConveniosController@index')->name('conveinos.home');
    Route::get('/convenios', 'ConveniosController@obtenerConvenios');
    Route::get('/convenios/{id}/detalle', 'ConveniosController@detalleConvenio');

    Route::get('/finanzas', 'FinanzasController@index');
    Route::get('/finanzas/datos', 'FinanzasController@obtenerDatos');
    Route::get('/finanzas/convenios', 'FinanzasController@obtenerConvenios');
    Route::post('/finanzas/convenios', 'FinanzasController@guardarConvenio');
    Route::put('/finanzas/convenio/{id}', 'FinanzasController@validarConvenio');
    Route::get('/finanzas/convenio/{id}/pago', 'FinanzasController@historialPagos');
    Route::post('/finanzas/convenio/{id}/pago', 'FinanzasController@guardarPago');
    Route::put('/finanzas/comprobacion/{id}', 'FinanzasController@validarComprobacion');
    Route::put('/finanzas/factura/{id}', 'FinanzasController@validarFactura');
    Route::put('/finanzas/pago/{id}', 'FinanzasController@validarPago');

    Route::post('/finanzas/convenio/pdf', 'PDFController@subirPDF')->name('convenio.pdf');

    Route::get('/juridico', 'JuridicoController@index');
    Route::get('/juridico/convenios', 'JuridicoController@obtenerConvenios');
    Route::put('/juridico/convenio/{id}', 'JuridicoController@validarConvenio');

    Route::get('/area', 'AreaController@index');
    Route::get('/areas/{area}', 'AreaController@buscarArea');
    Route::get('/area/convenios', 'AreaController@obtenerConvenios');
    Route::put('/area/convenio/{id}', 'AreaController@validarConvenio');
    Route::put('/area/comprobacion/{id}', 'AreaController@validarComprobacion');
    Route::put('/area/factura/{id}', 'AreaController@validarFactura');

    Route::get('/catalogos', 'CatalogoController@index');
    Route::post('/catalogos/asociacion', 'CatalogoController@guardarAsociacion');
    Route::post('/catalogos/direccion', 'CatalogoController@guardarDireccion');
    Route::post('/catalogos/responsable', 'CatalogoController@guardarResponsable');
});
