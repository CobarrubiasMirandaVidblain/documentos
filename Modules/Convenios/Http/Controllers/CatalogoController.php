<?php

namespace Modules\Convenios\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Asociacion;
use App\Direccion;
use App\Responsable;

class CatalogoController extends Controller
{

    public function index()
    {
        return view('convenios::finanzas.catalogos');
    }

    public function guardarAsociacion(Request $request){
        $this->validate($request, [
            'nombre' => 'required|min:4'
        ]);
        if($request->wantsJson()){
            $asociacion = Asociacion::create($request->all());
            if($asociacion->id){
                return $asociacion;
            }
            return new JsonResponse(['error' => 'No se pudo guardar. Reintente porfavor'], 422);
        }
        abort(403);
    }

    public function guardarDireccion(Request $request){
        if(!auth()->user()->hasRoles(['CATALOGOS'])){
            abort(403);
        }
        $this->validate($request, [
            'nombre' => 'required|min:4'
        ]);
        if($request->wantsJson()){
            $direccion = Direccion::create($request->all());
            if($direccion->id){
                return $direccion;
            }
            return new JsonResponse(['error' => 'No se pudo guardar. Reintente porfavor'], 422);
        }
        abort(403);
    }

    public function guardarResponsable(Request $request){
        if(!auth()->user()->hasRoles(['CATALOGOS'])){
            abort(403);
        }
        $this->validate($request, [
            'titulo' => 'required|min:2',
            'nombre' => 'required|min:2',
            'primer_apellido' => 'required|min:2',
        ]);
        if($request->wantsJson()){
            $responsable = Responsable::create($request->all());
            if($responsable->id){
                return $responsable;
            }
            return new JsonResponse(['error' => 'No se pudo guardar. Reintente porfavor'], 422);
        }
        abort(403);
    }
}
