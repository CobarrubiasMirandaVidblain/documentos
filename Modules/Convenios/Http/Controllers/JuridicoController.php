<?php

namespace Modules\Convenios\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Convenio;
use App\Validacion;

class JuridicoController extends Controller
{
    public function index(){
        // if(! auth()->user()->hasRoles(['JURIDICO'])){
        //     return view('errors.403');
        // }
        return view('convenios::juridico.index');
    }

    public function obtenerConvenios(Request $request){
        if(! auth()->user()->hasRoles(['JURIDICO'])){
            return new JsonResponse([], 403);
        }
        if(request()->wantsJson()){
            $convenios = Convenio::with(['direccion', 'responsable', 'asociacion', 'validacion'])->get();
            foreach($convenios as $convenio){
                $convenio->procesando = false;
            }
            return new JsonResponse($convenios);
        }
        return new JsonResponse([error =>  'Error']);
    }

    public function validarConvenio(Request $request, $id){
        if(! auth()->user()->hasRoles(['JURIDICO'])){
            return new JsonResponse([], 403);
        }
        $validacion = Validacion::where('convenio_id', $id)->get()->first();
        if($validacion && !$validacion->validacion_juridico){
            $validacion->validacion_juridico = !$validacion->validacion_juridico;
            $validacion->save();
            return new JsonResponse($validacion);
        }
        return new JsonResponse(['msg' => 'Ya no puede cambiar el estado'], 422);
    }
}
