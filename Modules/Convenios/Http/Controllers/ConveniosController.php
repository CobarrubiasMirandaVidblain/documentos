<?php

namespace Modules\Convenios\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;

use Modules\Convenios\Entities\Convenio;

class ConveniosController extends Controller
{
    // Constructor para implementar el middleware de autentificacion y roles
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('rolModuleV2:convenios,FINANZAS');
    }

    // Verificar que solo ingrese si tiene el rol de DIRECTOR
    public function index()
    {
        return view('convenios::home');
    }

    // Enviar los convenios en formato json
    public function obtenerConvenios(Request $request){
        if(request()->wantsJson()){
            $convenios = Convenio::with(['direccion', 'responsable', 'asociacion', 'validacion'])->get();
            foreach($convenios as $convenio){
                $convenio->procesando = false;
            }
            return new JsonResponse($convenios);
        }
        return new JsonResponse(['error' =>  'Error']);
    }

    // Mostrar un convenio con sus respectivos pagos
    public function detalleConvenio($id){
        $validacion = Convenio::where('id', $id)->with(['pagos'])->first();
        if($validacion){
            return new JsonResponse($validacion);
        }
        return new JsonResponse(['msg' => 'No hay registro'], 404);
    }
}
