@extends('convenios::layouts.master')

@section('styles')
@endsection

@section('content')
<div class="section-top-border" id="app">
    <h2>Dirección Jurídica</h2>
    <h3 class="mb-30">Convenios y Contratos</h3>
    <p>Para cambiar la validación solo debe dar clic en el botón correspondiente.</p>
    <div class="progress-table-wrap">
        <table class="table tabel-sm table-striped table-bordered" id="dataTable">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Asociación</th>
                <th scope="col">Convenio</th>
                <th scope="col">Dirección</th>
                <th scope="col">PDF</th>
                <th scope="col">Contrato</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="convenio in convenios">
                    <th scope="row">@{{convenio.id}}</th>
                    <td>@{{convenio.asociacion.nombre}}</td>
                    <td>@{{convenio.informe}}</td>
                    <td>@{{convenio.direccion.nombre}}</td>
                    <td><a :href="convenio.pdf" target="_blank" :class="['btn', convenio.pdf ? 'btn-info' : 'btn-default']"><span class="lnr lnr-file-empty"></span></a></td>
                    <td>
                        <button :class="['genric-btn', convenio.validacion.validacion_juridico? 'primary-border' : 'danger-border', 'small']" @click="validarConvenio(convenio)">@{{convenio.validacion.validacion_juridico ? 'Validado' : 'No Validado'}}</button>
                    </td>
                </tr>
            </tbody>

        </table>
    </div>
</div>
@endsection

@section('scripts')
<script>
new Vue({
    el: '#app',
    data: {
        convenios: []
    },
    mounted(){
        axios.get('/convenios/juridico/convenios')
            .then(response => {
                this.convenios = response.data;
            })
            .catch(error => {
                console.log(error);
            });
    },
    methods: {
        validarConvenio(convenio){
            axios.put(`/convenios/juridico/convenio/${convenio.id}`, {})
                .then(response => {
                    console.log(response);
                    if(response.data.id){
                        console.log('hecho');
                        convenio.validacion.validacion_juridico = response.data.validacion_juridico;
                        swal('Great !', 'Actualizado correctamente', 'success');
                    }
                })
                .catch(error => { 
                    console.log(error);
                    swal('Oops !', 'Ocurrio un error. ' + error.response.data.msg, 'warning');
                });
        }
    }
});

</script>
@endsection