@extends('convenios::layouts.master')

@section('styles')
<link rel="stylesheet" href="/css/dropzone.css">
<style>
    .pdf{
        color: #EF426F;
    }
    .pdf:hover, .pdf:focus, .pdf:active{
        color: #F58EAA;
    }
</style>
@endsection

@section('content')
<div class="section-top-border" id="app">
    <h2>Administración y Finanzas</h2>
    <h3 class="mb-30">Convenios y Contratos</h3>
    <button class="btn btn-success btn-outline btn-sm" @click.prevent="lanzarModal()"><span class="lnr lnr-plus-circle"></span> Convenio</button>
    <p>Para cambiar la validación solo debe dar clic en el botón correspondiente.</p>
    <div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item" v-if="pagination.current_page > 1">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                </li>
                <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                </li>
                <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="progress-table-wrap">
        <table class="table tabel-sm table-striped table-bordered" id="dataTable">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Asociación</th>
                <th scope="col">Dirección</th>
                <th scope="col">Responsable</th>
                <th scope="col">Datos</th>
                <th scope="col">Contrato</th>
                <th scope="col">Comprobación</th>
                <th scope="col">Factura</th>
                <th scope="col">Pago</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="convenio in convenios">
                    <th scope="row">@{{convenio.id}}</th>
                    <td>@{{convenio.asociacion.nombre}}</td>
                    <td>@{{convenio.direccion.nombre}}</td>
                    <td>@{{convenio.responsable.titulo + ' ' + convenio.responsable.nombre + ' ' + convenio.responsable.primer_apellido + ' ' + convenio.responsable.segundo_apellido}}</td>
                    <td>
                        <button class="genric-btn info-border small" @click="informeModal(convenio)"><span class="lnr lnr-eye"></span></button>
                        <button class="genric-btn info-border small" @click.prevent="abrirDetalles(convenio.id, convenio.monto)"><span class="lnr lnr-list"></span></button>
                        <button class="genric-btn info-border small" @click.prevent="agregarPDF(convenio.id)"><span class="lnr lnr-file-add"></span></button>
                    </td>
                    <td>
                        <button :class="['genric-btn', convenio.validacion.validacion_finanzas? 'primary-border' : 'danger-border', 'small']" @click="validarConvenio(convenio)">@{{convenio.validacion.validacion_finanzas ? 'Validado' : 'No Validado'}}</button>
                    </td>
                    <td>
                        <button :class="['genric-btn', convenio.validacion.comprobacion_finanzas? 'primary-border' : 'danger-border', 'small']" @click="validarComprobacion(convenio)">@{{convenio.validacion.comprobacion_finanzas ? 'Validado' : 'No Validado'}}</button>
                    </td>
                    <td>
                        <button :class="['genric-btn', convenio.validacion.factura_finanzas? 'primary-border' : 'danger-border', 'small']" @click="validarFactura(convenio)">@{{convenio.validacion.factura_finanzas ? 'Validado' : 'No Validado'}}</button>
                    </td>
                    <td>
                        <button class="genric-btn info-border small" @click.prevent="abrirModalPago(convenio.id)">Pagar</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item" v-if="pagination.current_page > 1">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                </li>
                <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                </li>
                <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                </li>
            </ul>
        </nav>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="informeModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Convenio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-justify">@{{informe}}</p>
                    <a class="pdf" target="_blank" :href="archivo.url" v-if="archivo.url">@{{archivo.nombre}}</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="agregarPago">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form @submit.prevent="guardarPago(pago.convenio_id)">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="convenio_id">ID Convenio</label>
                            <input type="number" class="form-control disabled" id="convenio_id" v-model="pago.convenio_id">
                        </div>
                        <div class="form-group">
                            <label for="anio">Año</label>
                            <input type="number" class="form-control" id="anio" v-model="pago.anio">
                        </div>
                        <div class="form-group">
                            <label for="mes">Mes</label>
                            <select class="form-control" id="mes">
                                <option value="1">ENERO</option>
                                <option value="2">FEBRERO</option>
                                <option value="3">MARZO</option>
                                <option value="4">ABRIL</option>
                                <option value="5">MAYO</option>
                                <option value="6">JUNIO</option>
                                <option value="7">JULIO</option>
                                <option value="8">AGOSTO</option>
                                <option value="9">SEPTIEMBRE</option>
                                <option value="10">OCTUBRE</option>
                                <option value="11">NOVIEMBRE</option>
                                <option value="12">DICIEMBRE</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="monto">Monto</label>
                            <input type="number" class="form-control" id="monto" v-model="pago.monto">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary" id="btnPago">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modalConvenio" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar Convenio</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form @submit.prevent="guardarConvenio()" id="formConvenio">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Asociacion</label>
                            <select class="form-control" id="asociacion" name="asociacion_id">
                                @foreach($asociaciones as $asociacion)
                                <option value="{{$asociacion->id}}">{{$asociacion->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Dirección</label>
                            <select class="form-control" id="direccion" name="direccion_id">
                                @foreach($direcciones as $direccion)
                                <option value="{{$direccion->id}}">{{$direccion->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Responsable</label>
                            <select class="form-control" id="responsable" name="responsable_id">
                                @foreach($responsables as $responsable)
                                <option value="{{$responsable->id}}">{{$responsable->nombre . ' ' . $responsable->primer_apellido . ' ' . $responsable->segundo_apellido}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="informe">Informe</label>
                            <textarea class="form-control" id="informe" rows="5" name="informe" v-model="convenio.informe"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="monto">Monto Anual</label>
                            <input type="number" class="form-control" id="monto" name="monto" v-model="convenio.monto">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalDetalles">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalles de Pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Convenio ID: @{{detalle.convenio_id}} Monto Anual: $ @{{detalle.anual}}</p>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Año</th>
                                <th scope="col">Mes</th>
                                <th scope="col">Monto</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="pago in detalle.pagos">
                                <th scope="row">@{{pago.id}}</th>
                                <td>@{{pago.anio}}</td>
                                <td>@{{obtenerMes(pago.mes)}}</td>
                                <td>@{{pago.monto}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <p>Acumulado: $ @{{acumulado(detalle.pagos)}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalPDF">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agrear Archivo PDF</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p><strong>Nota: Si existe un pdf anterior se sustituirá</strong> (id: @{{pdf}})</p>
                    <div id="pdf" class="dropzone"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/dropzone.js"></script>
<script>
Dropzone.autoDiscover = false;

new Vue({
    el: '#app',
    data: {
        convenios: [],
        pagination: {},
        offset: 2,
        convenio: {
            informe: '',
            monto: ''
        },
        informe: '',
        archivo: {
            url: '',
            nombre: 'Convenio PDF'
        },
        pago: {
            convenio_id: 0,
            anio: 2018,
            monto: '',
            mes: 0,
            disabled: false
        },
        detalle: {
            convenio_id: '',
            anual: 0,
            pagos: []
        },
        meses: [{numero: 1, mes: 'ENERO'}, {numero: 2, mes: 'FEBRERO'}, {numero: 3, mes: 'MARZO'}, {numero: 4, mes: 'ABRIL'}, {numero: 5, mes: 'MAYO'}, {numero: 6, mes: 'JUNIO'}, {numero: 7, mes: 'JULIO'}, {numero: 8, mes: 'AGOSTO'}, {numero: 9, mes: 'SEPTIEMBRE'}, {numero: 10, mes: 'OCTUBRE'}, {numero: 11, mes: 'NOVIEMBRE'}, {numero: 12, mes: 'DICIEMBRE'}],
        pdf: 0,
        myDropzone: null
    },
    computed: {
        isActive(){
            return this.pagination.current_page;
        },
        pageNumber(){
            if(!this.pagination.to){
                return [];
            }
            var desde = this.pagination.current_page - this.offset;
            if(desde < 1){
                desde = 1;
            }
            var hasta = desde + (this.offset * 2);
            if(hasta >= this.pagination.last_page) {
                hasta = this.pagination.last_page;
            }
            var pagesArray = [];
            while(desde <= hasta) {
                pagesArray.push(desde);
                desde++;
            }
            return pagesArray;
        }
    },
    mounted(){
        this.obtenerConvenios(1);
        this.myDropzone = new Dropzone("#pdf", 
            { 
                dictDefaultMessage: 'Arrastre sus archivos para subirlos',
                method: 'post',
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' }, 
                url: '/finanzas/convenio/pdf',
                success: function(file, response) {
                    var link = document.createElement('a');
                    link.setAttribute('href', response.url);
                    link.innerHTML = '<br>Descargar';
                    file.previewTemplate.appendChild(link);

                    file.previewElement.querySelector('[data-dz-name]').innerHTML = response.nombre;
                    file.id = response.id;
                }
            });
        let dropzone2 = this.myDropzone;
        this.myDropzone.on("complete", function(file) {
            dropzone2.removeFile(file);
        });
    },
    methods: {
        lanzarModal(){
            $('#modalConvenio').modal({});
        },
        abrirModalPago(convenio_id){
            this.pago.convenio_id = convenio_id;
            $('#agregarPago').modal({});
        },
        obtenerDatos(){
            axios.get('/finanzas/datos')
                .then(response => {
                    this.asociaciones = response.data.asociaciones;
                    this.direcciones = response.data.direcciones;
                    this.responsables = response.data.responsables;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        guardarConvenio(){
            axios.post('/finanzas/convenios', {
                asociacion_id: $('#asociacion').val(),
                direccion_id: $('#direccion').val(),
                responsable_id: $('#responsable').val(),
                informe: this.convenio.informe,
                monto: this.convenio.monto
            })
            .then(response => {
                console.log(response);
                if(response.data.convenio.id >= 0){
                    swal('Great !', 'Se registro correctamente', 'success');
                    this.convenio.informe = '';
                    this.convenio.monto = '';
                    $('#modalConvenio').modal('hide');
                }else{
                    swal('Oops !', 'Algo salio mal', 'warning');
                }
            })
            .catch(error => {
                swal('Oops !', 'Algo salio mal.' + error, 'warning');
                console.log(error);
            });
        },
        obtenerConvenios(page){
            axios.get(`/finanzas/convenios?page=${page}`)
                .then(response => {
                    console.log(response);
                    this.convenios = response.data.data;
                    this.pagination = response.data;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        informeModal(convenio){
            this.informe = convenio.informe;
            this.archivo.url = convenio.pdf;
            console.log(convenio);
            $('#informeModal').modal({});
        },
        cambiarPagina(page){
            this.obtenerConvenios(page);
        },
        validarConvenio(convenio){
            axios.put(`/finanzas/convenio/${convenio.id}`, {})
                .then(response => {
                    console.log(response);
                    if(response.data.id){
                        console.log('hecho');
                        convenio.validacion.validacion_finanzas = response.data.validacion_finanzas;
                    }
                })
                .catch(error => { console.log(error); });
        },
        validarComprobacion(convenio){
            axios.put(`/finanzas/comprobacion/${convenio.id}`, {})
                .then(response => {
                    console.log(response);
                    if(response.data.id){
                        console.log('comprobacion');
                        convenio.validacion.comprobacion_finanzas = response.data.comprobacion_finanzas;
                    }
                })
                .catch(error => { 
                    console.log(error.response);
                    if(error.response.data){
                        swal('Oops !', `Ocurrio un error. ${error.response.data.error.msg}`, 'warning');
                    }
                });
        },
        validarFactura(convenio){
            axios.put(`/finanzas/factura/${convenio.id}`, {})
                .then(response => {
                    console.log(response);
                    if(response.data.id){
                        console.log('factura');
                        convenio.validacion.factura_finanzas = response.data.factura_finanzas;
                    }
                })
                .catch(error => { 
                    console.log(error.response);
                    if(error.response.data){
                        swal('Oops !', `Ocurrio un error. ${error.response.data.error.msg}`, 'warning');
                    }
                });
        },
        validarPago(convenio){
            axios.put(`/finanzas/pago/${convenio.id}`, {})
                .then(response => {
                    console.log(response);
                    if(response.data.id){
                        console.log('pago');
                        convenio.validacion.pago_finanzas = response.data.pago_finanzas;
                    }
                })
                .catch(error => { 
                    console.log(error.response);
                    if(error.response.data){
                        swal('Oops !', `Ocurrio un error. ${error.response.data.error.msg}`, 'warning');
                    }
                });
        },
        guardarPago(convenio_id){
            $('#btnPago').attr('disabled', 'disabled');
            axios.post(`/finanzas/convenio/${convenio_id}/pago`, {
                convenio_id: convenio_id,
                mes: $('#mes').val(),
                anio: this.pago.anio,
                monto: this.pago.monto
            })
            .then(response => {
                console.log(response);
                if(response.data.id){
                    swal('Great !', 'Se guardo el pago', 'success');
                    $('#btnPago').removeAttr('disabled');
                    var $miSelect = $('#mes');
                    $miSelect.val($miSelect.children('option:first').val());
                    this.pago.monto = '';
                }
                else{
                    swal('Oops !', 'Ocurrio un error', 'warning');
                    $('#btnPago').removeAttr('disabled');
                }
            })
            .catch(error => {
                $('#btnPago').removeAttr('disabled');
                swal('Oops !', 'Ocurrio un error. ' + error.response.data.error.msg, 'warning');
                console.log(error);
            })
        },
        abrirDetalles(convenio_id, monto){
            this.detalle.convenio_id = convenio_id;
            this.detalle.anual = monto;
            axios.get(`/finanzas/convenio/${convenio_id}/pago`)
            .then(response => {
                console.log(response);
                this.detalle.pagos = response.data;
                $('#modalDetalles').modal({});
            })
            .catch(error => {
                console.log(response);
                swal('Oops !', 'Ocurrio un error. ' + error, 'warning');
            });
        },
        obtenerMes(numero){
            return this.meses[numero-1].mes;
        },
        acumulado(pagos){
            let total = 0;
            for(let pago in pagos){
                let m = parseFloat(pagos[pago].monto);
                total+= m;
            }
            return total;
        },
        agregarPDF(convenio_id){
            this.pdf = convenio_id;
            $('#modalPDF').modal();
            this.myDropzone.on("sending", function(file, xhr, formData) {
                // Will send the filesize along with the file as POST data.
                formData.append("filesize", file.size);
                formData.append("convenio_id", convenio_id);
            });
        },
        
    }
});

</script>
@endsection