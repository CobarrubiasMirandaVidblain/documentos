<!-- Breadcrumb-->
<ol class="breadcrumb">
  <li class="breadcrumb-item">
    <i class="fa fa-archive" aria-hidden="true"></i> &nbsp; <a href="/convenios">Convenios y Contratos</a>
  </li>
  @yield('breadcrumbs')
</ol>