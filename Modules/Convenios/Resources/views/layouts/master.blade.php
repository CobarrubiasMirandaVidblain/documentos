<!DOCTYPE html>

<html lang="es">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Convenios y Contratos</title>
    <!-- Main styles for this application-->
    {{--  <link href="{{ Module::asset('convenioss:css/app.css') }}" rel="stylesheet">  --}}
    <link href="{{ asset('convenioss/css/app.css') }}" rel="stylesheet">
    @yield('styles')
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    
    @include('convenios::layouts.includes.navbar')

    <div class="app-body">
      
      @include('convenios::layouts.includes.sidebar')
      
      <main class="main">
        @include('convenios::layouts.includes.breadcrumb')

        <div class="container-fluid">
          <div class="animated fadeIn">

            @yield('content')
          
          </div>
        </div>

      </main>
    </div>
    <footer class="app-footer">
      <div>
        <span>DIF Oaxaca &copy; Copyright 2019.</span>
      </div>
      <div class="ml-auto">
        <span>Unidad de Informática</span>
      </div>
    </footer>
    {{--  <script src="{{ Module::asset('convenioss:js/app.js') }}"></script>  --}}
    <script src="{{ asset('convenioss/js/app.js') }}"></script>
    @yield('scripts')
  </body>
</html>
