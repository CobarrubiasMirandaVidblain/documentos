<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycValidacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cyc_validaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('convenio_id');
            $table->boolean('validacion_juridico')->nullable();
            $table->boolean('validacion_direccion')->nullable();
            $table->boolean('validacion_finanzas')->nullable();
            $table->boolean('comprobacion_direccion')->nullable();
            $table->boolean('comprobacion_finanzas')->nullable();
            $table->boolean('factura_direccion')->nullable();
            $table->boolean('factura_finanzas')->nullable();
            $table->timestamps();
        });

        Schema::table('cyc_validaciones', function (Blueprint $table) {     
            $table->foreign('convenio_id')->references('id')->on('cyc_convenios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cyc_validaciones', function (Blueprint $table) {
            $table->dropForeign(['convenio_id']);
        });
        Schema::dropIfExists('cyc_validaciones');
    }
}
