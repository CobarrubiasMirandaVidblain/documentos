<?php

namespace Modules\Convenios\Entities;

use Illuminate\Database\Eloquent\Model;

class Validacion extends Model
{
    protected $table = 'cyc_validaciones';

    protected $fillable = ['convenio_id'];

    public function convenio(){
        return $this->belongsTo('Modules\Convenios\Entities\Convenio', 'convenio_id', 'id');
    }

}
