<?php

namespace Modules\Convenios\Entities;

use Illuminate\Database\Eloquent\Model;

class Convenio extends Model
{
    protected $table = 'cyc_convenios';

    protected $fillable = ['institucion_id', 'area_id', 'informe', 'monto', 'pdf'];

    public function institucion(){
        return $this->belongsTo('Modules\Convenios\Entities\Institucion', 'institucion_id', 'id')->where('tipoinstitucion_id', 1);
    }

    public function area(){
        return $this->belongsTo('App\Area', 'area_id', 'id');
    }

    public function validacion(){
        return $this->hasOne('Modules\Convenios\Entities\Validacion', 'convenio_id', 'id');
    }

    public function pagos(){
        return $this->hasMany('Modules\Convenios\Entities\Pago', 'convenio_id', 'id');
    }
}
