let mix = require('laravel-mix');

// mix.js('Resources/src/js/app.js', 'Resources/assets/js/')
//   .sass('Resources/src/sass/app.scss', 'Resources/assets/css/')
  // .options({
  //     processCssUrls: false
  // })
  // .setPublicPath('./../../public/bancadif');
  // .setPublicPath('Resources/assets/');

mix.js('Resources/src/js/app.js', './../../public/convenioss/js')
  .sass('Resources/src/sass/app.scss', './../../public/convenioss/css')
  .setPublicPath('./../../public/convenioss');

// mix.browserSync('intradif.test:90');