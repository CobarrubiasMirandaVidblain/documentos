const OrdendeCompraList = Vue.component('ordendecompralist', {
    template: `
    <div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ordenes de compra <button type="button" class="btn btn-box-tool" @click.prevent="listaOD(1), busqueda = ''"><i class="fa fa-refresh"></i></button></h3>
                    </div>
                    <div class="box-body">
                        <div class="box-group" id="accordion">
                            <div class="panel box box-warning">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#revision">
                                        En revision
                                        </a>
                                    </h4>
                                    <div class="box-tools">
                                        <div class="input-group input-group-sm" style="width: 150px;">
                                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" v-model="busqueda">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" @click.prevent="filter()"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="revision" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio Interno</th>
                                                    <th>Folio ODC</th>
                                                    <th>Estatus</th>
                                                    <th>Proveedor</th>
                                                    <th>Requisicion</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id > 3">
                                                        <th>@{{od.folio_interno}}</th>                                                           
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        
                                                        <th>@{{od.proveedor.nombre}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="imprimirFicha(od.id)"><i class="fa fa-print"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                            {{-- <div class="panel box box-danger">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#rechazado">
                                        Rechazado
                                        </a>
                                    </h4>
                                </div>
                                <div id="rechazado" class="panel-collapse collapse ">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 8">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="panel box " style="border-top-color: #3c8dbc">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Apresupuestal">
                                        Afectacion-Presupuestal
                                        </a>
                                    </h4>
                                </div>
                                <div id="Apresupuestal" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio Interno</th>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 9">
                                                        <th>@{{od.folio_interno}}</th>
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="panel box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#cobertura">
                                        Sin cobertura
                                        </a>
                                    </h4>
                                </div>
                                <div id="cobertura" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 10">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                      
                            <div class="panel box"  style="border-top-color: #00c0ef">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#EnviadoCLC">
                                        Enviado a CLC
                                        </a>
                                    </h4>
                                </div>
                                <div id="EnviadoCLC" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 11">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="panel box" style="border-top-color: #3c8dbc">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#CLCTradicional">
                                            CLC modo Tradicional
                                        </a>
                                    </h4>
                                </div>
                                <div id="CLCTradicional" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 13">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="panel box" style="border-top-color: #3c8dbc">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#pagoDirecto">
                                            Pago Directo Proveedor
                                        </a>
                                    </h4>
                                </div>
                                <div id="pagoDirecto" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 12">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="panel box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#SPEI">
                                            Enviado a Banco para SPEI
                                        </a>
                                    </h4>
                                </div>
                                <div id="SPEI" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 14">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                      
                            <div class="panel box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#finanzas">
                                            Enviado Pago Fianzas
                                        </a>
                                    </h4>
                                </div>
                                <div id="finanzas" class="panel-collapse collapse ">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 15">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                      
                            <div class="panel box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#SPEIEnviado">
                                            SPEI Enviado
                                        </a>
                                    </h4>
                                </div>
                                <div id="SPEIEnviado" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 16">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                      
                            <div class="panel box box-success">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#Contabilidad">
                                           Registro en contabilidad
                                        </a>
                                    </h4>
                                </div>
                                <div id="Contabilidad" class="panel-collapse collapse">
                                    <div class="box-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Folio</th>
                                                    <th>Estatus</th>
                                                    <th>Sub-total</th>
                                                    <th>IVA</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="od in listado">
                                                    <template v-if="od.estatus.id == 17">
                                                        <th>@{{od.folio}}</th>
                                                        <td ><span :class="'label '+od.estatus.color">@{{od.estatus.estatus}}</span></td>
                                                        <th>@{{parseFloat(od.subtotal).toFixed(2)}}</th>
                                                        <th>@{{od.requisicion.folio}}</th>
                                                        <th>@{{parseFloat(od.total).toFixed(2)}}</th>
                                                        <th>
                                                            <button class="btn btn-xs bg-success" title="Detalle" @click.prevent="showOD(od)"><i class="fa fa-eye"></i></button>
                                                        </th>
                                                    </template>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>                    
            </div>
            <div class="col-md-12">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item" v-if="pagination.current_page > 1">
                            <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                        </li>
                        <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                            <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                        </li>
                        <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                            <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div><p>Total de Registros : @{{pagination.total}}</p></div>
        </div>
        {{-- Modal --}}
        <div class="modal modal-primary fade" id="modalOD" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog ">{{-- modal-lg --}}
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Detalle orden de compra |  @{{datosOD.folio_interno}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div v-show="datosOD.status_id == 9" class="alert alert-danger alert-dismissible">
                                <h4><i class="icon fa fa-ban"></i> Motivo cancelacion!</h4>
                                <b>@{{datosOD.motivo}}</b>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Folio</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.folio}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Orden de compra</b> </h6>
                                        <span :class="'label '+datosOD.estatus.color">@{{datosOD.estatus.estatus}}</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Requisición</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.requisicion.folio}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr style="border-top: 1px solid #000;"></div>
                                <div class="col-md-12">
                                    <table class="table table-striped text-muted">
                                        <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Presentación</th>
                                                <th>Producto</th>
                                                <th style="width: 40px">Precio</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="p in datosOD.productos">
                                                <th>@{{p.cantidad}}</th>
                                                <template v-for="(info,index) in p.productos">
                                                    <th v-if="index == 'presentacion'">@{{info.presentacion}}</th>
                                                    <th v-else-if="index == 'producto'">@{{info.producto}}</th>
                                                </template>
                                                <th>@{{p.precio_unitario_real}}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Sub Total</b> </h6>
                                        <span class="text-muted"><b> $ @{{datosOD.subtotal}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Total</b> </h6>
                                        <span class="text-muted"><b>$ @{{datosOD.total}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-12"><hr style="border-top: 1px solid #000;"></div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Proveedor</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.proveedor.nombre}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>RFC</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.proveedor.rfc}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>No. Cuenta</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.proveedor.numero_cuenta}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Clabe</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.proveedor.clabe}}</b></span>
                                    </div>
                                </div>
                                
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Calle</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.proveedor.calle}}</b></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group text-center">
                                        <h6 class="text-light-blue "><b>Colonia</b> </h6>
                                        <span class="text-muted"><b> @{{datosOD.proveedor.colonia}}</b></span>
                                    </div>
                                </div>
                            </div>
                        </div>                
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                        @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','FINANCIERO'], 'recmat'))
                        <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(3)" ><i class="fa fa-ban"></i> Cancelar </button>

                            <template v-if="datosOD.estatus.id ==7">
                                {{-- <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(19)" ><i class="fa fa-ban"></i> Cancelar </button> --}}
                                <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(8)" ><i class="fa fa-send-o"></i> Enviar </button>
                            </template>
                            {{-- <button v-if="datosOD.estatus.id ==9" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(8)" ><i class="fa fa-mail-reply-all"></i> Enviar a revision</button>
                            <template v-if="datosOD.estatus.id ==9">
                                <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(10)" ><i class="fa fa-unlink"></i> Sin cobertura </button>
                            </template>
                            <button v-if="datosOD.estatus.id ==10 || datosOD.estatus.id ==9" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(11)" ><i class="fa fa-share-square-o"></i> Enviar a CLC </button>
                            <template v-if="datosOD.estatus.id ==11">
                                <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(12)" ><i class="fa fa-user"></i> Pago directo Proveedor </button>
                                <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(13)" ><i class="fa fa-money"></i> CLC Tradicional</button>
                            </template>
                            <button v-if="datosOD.estatus.id ==13" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(14)" > ENVIAR A BANCO PARA SPEI</button>
                            <button v-if="datosOD.estatus.id ==12" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(15)" > ENVIAR PAGO A FINANZAS</button>
                            <button v-if="datosOD.estatus.id ==14" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(16)" > SPEI Enviado</button>
                            <button v-if="datosOD.estatus.id ==15 || datosOD.estatus.id == 16" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="changeStatus(17)" > REGISTRO EN CONTABILIDA</button> --}}
                        @endif
                    </div> 
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        {{-- fin modal --}}
    </div>`,
    data() {
      return {
          listado:[],
          datosOD:{
              estatus:{},
              requisicion:{},
              proveedor:{},
              productos:{
                  producto:{
                      presentacion:{},
                      productos:''
                  }
              }
          },
        pagination:{},
        offset: 2,
        busqueda:''
      }
    },
    mounted(){
        this.listaOD(1);
    },
    computed: { 
        isActive(){
            return this.pagination.current_page;
        },
        pageNumber(){
            if(!this.pagination.to){
                return [];
            }
            var desde = this.pagination.current_page - this.offset;
            if(desde < 1){
                desde = 1;
            }
            var hasta = desde + (this.offset * 2);
            if(hasta >= this.pagination.last_page) {
                hasta = this.pagination.last_page;
            }
            var pagesArray = [];
            while(desde <= hasta) {
                pagesArray.push(desde);
                desde++;
            }
            return pagesArray;
        }
      },
    methods:{
        listaOD(page){
            let me = this;
            block();
            axios.get('/recmat/ordendecompra?page='+page).then(response => {
                me.listado = response.data.data;
                me.pagination = response.data;
                unblock();
            }).catch(error => {
                unblock();
                console.log(error);
            });
        },
        cambiarPagina(page){
            this.listaOD(page);
          },
        showOD(datos){
            let me = this;
            me.datosOD = datos;
            $('#modalOD').modal('show');    
        },
        changeStatus(estatus){
            let me = this;
            var url = "/recmat/ordendecompra/cambioestatus";
            const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 5000});
            if(estatus == 8 || estatus == 19)
            {
                Swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Guardar',
                  }).queue([
                    {
                      title: estatus == 19 ?'Rechazado' :'Aceptado',
                      text: estatus == 19?'Motivo':'Folio interno',
                    },
                  ]).then((result) => {
                    if (result.hasOwnProperty('value') && result.value[0]){
                        if(estatus == 9 && !/^([0-9])*$/.test(result.value)) return toast({type: 'error', title: 'No es numerico'});
                        block();
                        axios.post(url,{'id':me.datosOD.id,'status':estatus,'dato':result.value[0]}).then(function (response) {
                            unblock();
                            if(response.data.success){
                                $('#modalOD').modal('toggle');
                                toast({type: 'success', title: 'Correcto'})
                                me.listaOD();
                            }                    
                            else
                                Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
                
                        }).catch(function (error){
                            unblock();
                            console.log("Error "+error)
                        })
                    }else
                        toast({type: 'error', title: 'Existen campos vacios'})
                  })
            }
           else
           {
                axios.post(url,{'id':me.datosOD.id,'status':estatus}).then(function (response) {
                    if(response.data.success){
                        $('#modalOD').modal('toggle');
                        toast({type: 'success', title: 'Correcto'})
                        me.listaOD();
                    }                    
                    else
                        Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
        
                }).catch(function (error){
                    console.log("Error "+error)
                })
           }
            
        },
        imprimirFicha(id){
            let me = this;
            block();
            var url = `/recmat/ordendecompra/find/${localStorage.getItem('recm_token')}/${id}`;
            axios.get(url, me.ordenCompra).then(function (response) {
              fecha_plazo = response.data.ordencompra.fecha_captura.split("-");
              centavos = parseFloat(response.data.ordencompra.total).toFixed(2).split(".");
                unblock();
                jsreport.serverUrl = '{{config('app.reporter_url')}}';
                {{-- jsreport.serverUrl = 'http://192.168.0.149:5488'; --}}
                var request = {
                  template:{
                    {{-- shortid: "H12H2E3I4" --}}
                    shortid: "Hk5Op2juE"
                },
                header: {
                    Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
                },
                data:{
                  ordenCompra:{
                      folio:response.data.ordencompra.folio,
                      productos:response.data.ordencompra.productos_list,
                      subtotal:parseFloat(response.data.ordencompra.subtotal).toFixed(2),
                      totaIVA:parseFloat(response.data.ordencompra.subtotal).toFixed(2),
                      total:parseFloat(response.data.ordencompra.total).toFixed(2),
                      totalnoIVA:parseFloat(0).toFixed(2),
                      iva:parseFloat(response.data.ordencompra.subtotal*(16/100)).toFixed(2),
                      autoriza:response.data.datosResponsable.nombre + ' '+ response.data.datosResponsable.primer_apellido + ' '+response.data.datosResponsable.segundo_apellido,
                      area:response.data.responsableArea.area.nombre,
                      importeLetra:numeroALetras(centavos[0], {plural: "PESOS", singular: "PESO", centPlural: "CENTAVOS", centSingular: "CENTAVO"}),
                      anio:fecha_plazo[0],
                      mes:fecha_plazo[1],
                      dia:fecha_plazo[2],
                      centavos:centavos[1]+'/100 M.N.',
                      observacion:response.data.ordencompra.observacion
                  },
                  proveedor:{
                      nombre:response.data.ordencompra.proveedor.nombre,
                      banco:response.data.ordencompra.proveedor.banco,
                      domicilio:response.data.ordencompra.proveedor.calle+' '+response.data.ordencompra.proveedor.numero+' '+response.data.ordencompra.proveedor.colonia,
                      clabe:response.data.ordencompra.proveedor.clabe
                  }
                }
            };
            jsreport.render('_blank', request);
                
            }).catch(function (error){
                unblock();
                Swal({type: 'error',title: 'Oops...',text: error,footer: 'Ocurrio un error'})
            }) 
              
        },
        filter(){
            let me = this;
            if(me.busqueda)
            {
                block();
                var url = `/recmat/ordendecompra/busqueda/${localStorage.getItem('recm_token')}/${me.busqueda}`;
                axios.get(url).then(function (response) {
                    unblock();
                    if(response.data){
                        me.listado = response.data.data;
                        me.pagination = response.data;
                    }                    
                    else
                        Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
        
                }).catch(function (error){
                    unblock();
                    console.log("Error "+error)
                })
            }
            else
                return Swal({type: 'error',title: 'Oops...',text: 'El campo esta vacio',footer: 'Ocurrio un error'})
        }
    }
});