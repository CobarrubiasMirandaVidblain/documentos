
Vue.component('row-measure', {});

const ListMeasurements = Vue.component('listMeasures', {
    template: `
    <div>
        <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title">@{{msg}}</h3>
            <div class="box-tool">
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal-primary-medida"><i class="fa fa-plus"></i></button>
            </div>
            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right b-primary" placeholder="Buscar" v-model="smeasure" v-validate="'required'">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-primary" @click="validateBeforeSearchMeasures(smeasure)"><i class="fa fa-search"></i></button>
                </div>
                </div>
            </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example" class="table table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Producto</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="m in measures">
                        <td>@{{m.id}}</td>
                        <transition name="fade" mode="in-out">
                        <td v-show="!m.editing">@{{m.unidadmedida}}</td>
                        </transition>
                        <transition name="fade" mode="out-in">
                        <td v-show="m.editing"><input type="text" class="form-control" v-model="m.unidadmedida"></td>
                        </transition>
                        <td>
                            <button v-if="!m.editing" class="btn btn-sm btn-warning" @click="m.back = m.unidadmedida; m.editing = !m.editing"><i class="fa fa-pencil"></i></button>
                            <template v-else="m.editing">
                                <button class="btn btn-sm btn-success" @click="saveMeasure(m)"><i class="fa fa-check"></i></button>
                                <button class="btn btn-sm btn-primary" @click="m.unidadmedida = m.back; m.back = ''; m.editing = !m.editing"><i class="fa fa-close"></i></button>
                            </template>
                        </td>
                        <td><button class="btn btn-sm btn-danger" @click="deleteMeasure(m)"><i class="fa fa-trash"></i></button></td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary" @click="cleanSearch()">Limpiar</button>
            </div>
        </div>
        <!-- /.box -->
        <div class="modal modal-primary fade" id="modal-primary-medida">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Agregar Unidad de Medida</h4>
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">

                        <div class="form-group">
                        <label for="producto" class="col-sm-4 control-label text-primary">Unidad de medida</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="producto" v-model="newMeasure">
                        </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                        <button type="button" class="btn btn-outline btn-outline-primary" @click="saveNewMeasure()"><i class="fa fa-check"></i> Guardar</button>
                    </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
        <!-- /.modal-dialog -->
        </div>
    </div>
    `,
    data(){
        return {
            msg: 'Lista de Medidas',
            measures: [],
            measures_copy: [],
            newMeasure: '',
            smeasure: ''
        }
    },
    mounted(){
        this.loadMeasures();
    },
    methods: {
        loadMeasures(){
            axios.get('/recmat/admin/medidas')
                .then(response => {
                    this.measures = response.data;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        saveMeasure(measure){
            axios.put('/recmat/admin/medidas/' + measure.id, { unidadmedida: measure.unidadmedida})
                .then(response => {
                    console.log(response);
                    if(response.data){
                        swal('Great !', 'Guardado Correctamente', 'success');
                        measure.editing = false;
                    }else{
                        swal('Oops !', 'Ocurrio un error. ');
                        measure.editing = false;
                    }
                })
                .catch(error => {
                    console.log(error);
                    swal('Great !', 'Ocurrio un error. ' + error, 'error');
                    measure.editing = false;
                });
        },
        saveNewMeasure(){
            axios.post('/recmat/admin/medidas', { unidadmedida: this.newMeasure.toUpperCase() })
                .then(response => {
                    if(response.data.id){
                        swal('Great !', 'Guardado Correctamente', 'success');
                        this.measures.push(response.data);
                        $('#modal-primary-medida').modal('hide');
                        this.newMeasure = '';
                    }else{
                        swal('Oops !', 'Ocurrio un error', 'warning');
                    }
                })
                .catch(error => {
                    swal('Oops !', 'Ocurrio un error.' + error , 'warning');
                });
        },
        deleteMeasure(measure){
            axios.delete('/recmat/admin/medidas/' + measure.id, {})
                .then(response => {
                    if(response.data){
                        console.log(response.data);
                        let med = this.measures.map((p)=>{return p.id}).indexOf(response.data.id);
                        this.measures.splice(med, 1);
                        swal('Great !', 'Guardado Correctamente', 'success');
                    }else{
                        swal('Oops !', 'Ocurrio un error', 'warning');
                    }
                })
                .catch(error => {
                    swal('Oops !', 'Ocurrio un error. ' + error , 'warning');
                });
        },
        validateBeforeSearchMeasures(name){
            this.$validator.validate().then(result => {
                if (!result) {
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'El campo de búsqueda esta vacio !'
                    });
                }else{
                    this.searchMeasures(name);
                }
            });
        },
        searchMeasures(nombre){
            if(this.measures_copy.length <= 0){
                this.measures_copy = this.measures;
            }
            axios.get('/recmat/admin/medidas/s/' + nombre)
                .then(response => {
                    this.measures = response.data;
                    if(response.data.length <= 0){
                        swal({
                            type: 'error',
                            title: ':(',
                            text: 'No se encontraron resultados!',
                            footer: '',
                        })
                    }
                })
                .catch(error => {
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Ocurrio algo mal!',
                        footer: error,
                    })
                }); 
        },
        cleanSearch(){
            this.measures = this.measures_copy;
            this.smeasure = '';
        },
    }
});
