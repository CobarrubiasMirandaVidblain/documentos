@extends('recmat::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>

<style type="text/css">
      
  .modal-dialog:not([role="document"]) > .modal-content > .modal-body {
    overflow-y: auto;
    max-height: calc(100vh - 210px);
}
  .row {
    margin-right: 0px;
    margin-left: 0px;
  }

        #modal-body-beneficiario {
        min-height: 400px;
    }

    .modal-body {
        overflow-y: auto;
        max-height: calc(100vh - 210px);
    }

  .nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
      background: #d46985;
  }

  /* Timeline */
.timeline,
.timeline-horizontal {
list-style: none;
padding: 20px;
position: relative;
}
.timeline:before {
top: 40px;
bottom: 0;
position: absolute;
content: " ";
width: 3px;
background-color: #eeeeee;
left: 50%;
margin-left: -1.5px;
}
.timeline .timeline-item {
margin-bottom: 20px;
position: relative;
}
.timeline .timeline-item:before,
.timeline .timeline-item:after {
content: "";
display: table;
}
.timeline .timeline-item:after {
clear: both;
}
.timeline .timeline-item .timeline-badge {
color: #fff;
width: 54px;
height: 54px;
line-height: 52px;
font-size: 22px;
text-align: center;
position: absolute;
top: 18px;
left: 50%;
margin-left: -25px;
background-color: #7c7c7c;
border: 3px solid #ffffff;
z-index: 100;
border-top-right-radius: 50%;
border-top-left-radius: 50%;
border-bottom-right-radius: 50%;
border-bottom-left-radius: 50%;
}
.timeline .timeline-item .timeline-badge i,
.timeline .timeline-item .timeline-badge .fa,
.timeline .timeline-item .timeline-badge .glyphicon {
top: 2px;
left: 0px;
}
.timeline .timeline-item .timeline-badge.primary {
background-color: #1f9eba;
}
.timeline .timeline-item .timeline-badge.info {
background-color: #5bc0de;
}
.timeline .timeline-item .timeline-badge.success {
background-color: #59ba1f;
}
.timeline .timeline-item .timeline-badge.warning {
background-color: #d1bd10;
}
.timeline .timeline-item .timeline-badge.danger {
background-color: #ba1f1f;
}
.timeline .timeline-item .timeline-panel {
position: relative;
width: 46%;
float: left;
right: 16px;
border: 1px solid #c0c0c0;
background: #ffffff;
border-radius: 2px;
padding: 20px;
-webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline .timeline-item .timeline-panel:before {
position: absolute;
top: 26px;
right: -16px;
display: inline-block;
border-top: 16px solid transparent;
border-left: 16px solid #c0c0c0;
border-right: 0 solid #c0c0c0;
border-bottom: 16px solid transparent;
content: " ";
}
.timeline .timeline-item .timeline-panel .timeline-title {
margin-top: 0;
color: inherit;
}
.timeline .timeline-item .timeline-panel .timeline-body > p,
.timeline .timeline-item .timeline-panel .timeline-body > ul {
margin-bottom: 0;
}
.timeline .timeline-item .timeline-panel .timeline-body > p + p {
margin-top: 5px;
}
.timeline .timeline-item:last-child:nth-child(even) {
float: right;
}
.timeline .timeline-item:nth-child(even) .timeline-panel {
float: right;
left: 16px;
}
.timeline .timeline-item:nth-child(even) .timeline-panel:before {
border-left-width: 0;
border-right-width: 14px;
left: -14px;
right: auto;
}
.timeline-horizontal {
list-style: none;
position: relative;
padding: 20px 0px 20px 0px;
display: inline-block;
}
.timeline-horizontal:before {
height: 3px;
top: auto;
bottom: 26px;
left: 56px;
right: 0;
width: 100%;
margin-bottom: 20px;
}
.timeline-horizontal .timeline-item {
display: table-cell;
height: 200px;
width: 20%;
min-width: 320px;
float: none !important;
padding-left: 0px;
padding-right: 20px;
margin: 0 auto;
vertical-align: bottom;
}
.timeline-horizontal .timeline-item .timeline-panel {
top: auto;
bottom: 64px;
display: inline-block;
float: none !important;
left: 0 !important;
right: 0 !important;
width: 100%;
margin-bottom: 20px;
}
.timeline-horizontal .timeline-item .timeline-panel:before {
top: auto;
bottom: -16px;
left: 28px !important;
right: auto;
border-right: 16px solid transparent !important;
border-top: 16px solid #c0c0c0 !important;
border-bottom: 0 solid #c0c0c0 !important;
border-left: 16px solid transparent !important;
}
.timeline-horizontal .timeline-item:before,
.timeline-horizontal .timeline-item:after {
display: none;
}
.timeline-horizontal .timeline-item .timeline-badge {
top: auto;
bottom: 0px;
left: 43px;
}
</style>
@stop

@section('content-title', 'Recursos Materiales')
@section('content-subtitle', 'Administración')

@section('content')
<div id="app">        
    <transition name="fade" mode="out-in">
        <router-view></router-view>
    </transition>
</div>
@stop

@section('other-scripts')
<script>
Vue.use(VeeValidate);
Vue.component('v-select', VueSelect.VueSelect);
Vue.component('vue-multiselect', window.VueMultiselect.default);
Vue.component('multiselect', window.VueMultiselect.default);

@include('recmat::admin.products.index')
@include('recmat::admin.products.list')
@include('recmat::admin.medidas.index')
@include('recmat::requisiciones.index')
@include('recmat::requisiciones.list')
@include('recmat::ordendecompra.list')
@include('recmat::ordendecompra.print')
@include('recmat::requisiciones.listaOD')
@include('recmat::ordenmantenimiento.list')
@include('recmat::ordendecompra.new')
@include('recmat::partidas.listPartida')

const routes = [
  { path: '/productos/crear', component: Productos },
  { path: '/productos', component: ListProducts },
  { path: '/medidas', component: ListMeasurements },
  { path: '/requisiciones', component: Requisiciones },
  { path: '/requisiciones/lista', component: ListaRequisiciones },
  { path: '/ordendecompra', component: OrdendeCompraList },
  { path: '/ordendecompraprint', component: OrdendeCompraPrint },
  { path: '/requisiciones/listaOD', component: ListaOD },
  { path: '/ordenmantenimiento/lista', component: ListaODMantenimiento },
  { path: '/ordendecompra/new/:id', component: NewOrdenCompra, props: true }, // nueva orden de compra
  { path: '/partidas/lista', component: ListaPartida }, // nueva orden de compra
]
const router = new VueRouter({
  routes
});
const app = new Vue({
  router
}).$mount('#app')
</script>
@stop