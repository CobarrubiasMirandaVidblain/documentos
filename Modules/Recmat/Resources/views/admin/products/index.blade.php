const Productos = Vue.component('productos', {
    template: `
    <div>
      <h2>@{{msg}}</h2>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Agregar Producto</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form">
          <div class="box-body">
            <div class="form-group" :class="errors.first('nombreproducto') ? 'has-error' : ''" >
              <label for="producto">Nombre Producto</label>
              <vue-multiselect 
              name="producto" 
              v-model="producto" 
              :options="productos"
              :internal-search="false"
              :preserve-search="false"
              :clear-on-select="true"
              label="producto" :taggable="true"
              @search-change="buscarProducto"
              placeholder="Busca o agrega el nuevo producto"
              tag-placeholder="Enter para agregar nuevo producto"
              selectLabel="Enter para seleccionarlo" deselectLabel="Enter para deseleccionar"
              @tag="addTag"></vue-multiselect>
              <span class="text-danger">@{{ errors.first('nombreproducto') }}</span>
            </div>
            <div class="form-group" :class="errors.first('presentacion') ? 'has-error' : ''">
              <label>Presentación</label>
              <vue-multiselect
                name="presentacion"
                v-model="presentacion" 
                :options="presentaciones" 
                :multiple="true" 
                :close-on-select="false"
                :clear-on-select="true"
                :preserve-search="false"
                placeholder="Busca o agrega nueva presentación"
                label="presentacion"
                track-by="presentacion"
                :preselect-first="false"
                :internal-search="false"
                @search-change="buscarPresentacion"
                selectLabel="Enter para seleccionarlo"
                deselectLabel="Enter para deseleccionar"
                :taggable="true"
                tag-placeholder="Enter para agregar nueva presentación"
                @tag="agregarPresentacion">
              </vue-multiselect>
              <span class="text-danger">@{{ errors.first('presentacion') }}</span>
            </div>
            <div class="form-group" :class="errors.first('presentacion') ? 'has-error' : ''">
              <label>Partida</label>
              <vue-multiselect
                name="partida"
                v-model="partida_especifica" 
                :options="partidas" 
                :multiple="true"
                :clear-on-select="true"
                :preserve-search="false"
                placeholder="Busca las partidas"
                label="partida_especifica"
                track-by="partida_especifica"
                :preselect-first="false"
                :internal-search="false"
                @search-change="buscarPartida"
                selectLabel="Enter para seleccionarlo"
                deselectLabel="Enter para deseleccionar">
              </vue-multiselect>
              <span class="text-danger">@{{ errors.first('partida') }}</span>
            </div>

          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" @click.prevent="validateBeforeGuardarProducto()">Guardar</button>
          </div>
        </form>
      </div>
    <!-- /.box -->
    </div>`,
    data() {
      return {
          msg: 'Productos',
          producto: '',
          productos: [],
          presentaciones: [],
          presentacion: [],
          precios: [],
          partidas: [],
          partida_especifica: [],
          usuario: @json($usuario)
      }
    },
    mounted(){
      axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
    },
    methods:{
      buscarProducto(search) {
        if(search.length < 3) return;
        fetch(`/recmat/admin/productos/${localStorage.getItem('recm_token')}/${escape(search)}`)
          .then(res => {
            res.json()
              .then(json => {
                this.productos = json;
              });
          });
      },
      addTag (newTag) {
        const tag = {
          producto: newTag,
          id: newTag + Math.floor((Math.random() * 10000000)),
          nuevo: true
        };
        this.productos.push(tag);
        this.producto = tag;
      },
      buscarPresentacion(search){
        if(search.length < 3) return;
        fetch(`/recmat/admin/presentaciones/${localStorage.getItem('recm_token')}/${escape(search)}`)
          .then(res => {
            res.json()
              .then(json => {
                this.presentaciones = json;
              });
          });
      },
      agregarPresentacion (newTag) {
        const tag = {
          presentacion: newTag,
          id: Math.floor((Math.random() * 10000000)),
          nuevo: true
        };
        this.presentaciones.push(tag);
        this.presentacion.push(tag);
      },
      buscarPartida(search){
        fetch(`/recmat/admin/partidas/${localStorage.getItem('recm_token')}/${escape(search)}`)
          .then(res => {
            res.json()
              .then(json => {
                this.partidas = json.map(item => {
                  item.partida_especifica = item.partida_especifica_c+ ' - ' +item.partida_especifica; 
                  return item;
                })
              });
          });
      },
      validateBeforeGuardarProducto(e) {
        this.$validator.validate().then(result => {
          if (!result) {
            swal({
              type: 'error',
              title: 'Oops...',
              text: 'Tiene unos detalles !'
            });
          }else{
            this.guardarProducto();
          }
        });
      },
      guardarProducto(){
        Swal.mixin({
          input: 'text',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          progressSteps: this.presentacion.map( (item, key) => { return key + 1; } )
        }).queue(
          this.presentacion.map(item => { return {title: item.presentacion ? 'Precio de ' + item.presentacion : 'Precio de ' + item} })
        ).then((result) => {
          if (result.value) {
            {{-- Swal({
              title: 'All done!',
              html:
                'Your answers: <pre><code>' +
                  JSON.stringify(result.value) +
                '</code></pre>',
              confirmButtonText: 'Lovely!'
            }); --}}
            block();
            axios.post('/recmat/productos', {
              producto: this.producto,
              presentaciones: this.presentacion,
              partida: this.partida_especifica,
              precios: result.value.map(item => { return parseInt(item); }),
              usuario_id: this.usuario.id,
            })
              .then(response => {
                unblock();
                swal({
                  title: response.data.length > 0 ? 'Se agregaron ' + response.data.length + ' registros' : 'Ya se encontraba registrado',
                  timer: 1000,
                  onOpen: () => {
                    swal.showLoading()
                  }
                }).then((result) => {
                  if (result.dismiss === swal.DismissReason.timer) { // Read more about handling dismissals
                  }
                });
                this.producto = [];
                this.precios = [];
                this.presentacion = [];
                this.presentaciones = [];
                this.partidas = [];
                this.partida_especifica = [];
              })
              .catch(error => {
                unblock(); 
                swal({
                  type: 'error',
                  title: 'Oops...',
                  text: error.response.data.producto[0],
                  footer: '',
                })
                console.log(error.data); 
              });
          }
        })
      }
    }
});