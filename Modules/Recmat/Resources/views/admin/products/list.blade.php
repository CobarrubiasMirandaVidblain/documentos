const ListProducts = Vue.component('listProduct', {
  template: `
    <div>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">@{{msg}}</h3>
          <router-link to="/productos/crear" class="btn btn-xs btn-outline btn-outline-primary"><i class="fa fa-plus"></i> Agregar Producto</router-link>
          <div class="box-tools">
            
          </div>
        </div>
        <!-- /.box-header -->
          <div class="box-body">
            <div class="form-group">
              <label for="producto">Buscar producto</label>
              <input class="form-control" name="producto" v-model="product" @keyup="buscarProducto"/>
            </div>
            <h3>Presentaciones</h3>
            <table id="example" class="table table-hover">
              <thead>
                  <tr>
                      <th>N°</th>
                      <th>Nombre de la partida</th>
                      {{-- <th>Producto</th>
                      <th>Presentación</th>
                      <th>Editar</th>
                      <th>Eliminar</th> --}}
                  </tr>
              </thead>
              <tbody>
                  <template v-for="(p, index) in products">
                      <tr align="center" >
                          <th>@{{p.partida_especifica_c}}</th>
                          <th COLSPAN=5>@{{p.partida_especifica}}</th>
                      </tr>
                      <tr>
                          <th :ROWSPAN="p.productos.length + 1"></th>
                          <td><b>#</b></td> 
                          <td><b>Nombre</b></td> 
                          {{-- <td><b>Precio</b></td> --}}
                          <td><b>Presentacion</b></td>
                          <td><b>Editar</b></td>
                          <td><b>eliminar</b></td>
                      </tr>
                      <tr v-for="(sub, numero) in p.productos" >
                          <td>@{{numero + 1}}</td> 
                          <td>@{{sub.producto}}</td>
                          <td><template v-for="pre in sub.presentaciones">@{{pre.presentacion}},</template></td>
                          {{-- <td >$ @{{sub.pivot.precio.toLocaleString("en-US")}}</td> --}}
                          <td><button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-primary" @click="editProductModel(sub)"><i class="fa fa-pencil"></i></button></td>
                          <td><button class="btn btn-danger btn-sm" @click="deleteProduct(sub)"><i class="fa fa-trash"></i></button></td>
                      </tr>
                  </template>
                {{-- <tr v-for="p in products">
                    <td>@{{p.partida_especifica_c}}</td>
                    <td>@{{p.partida_especifica}}</td>
                    <td><button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-primary" @click="editProductModel(p)"><i class="fa fa-pencil"></i></button></td>
                    <td><button class="btn btn-danger btn-sm" @click="deleteProduct(p)"><i class="fa fa-trash"></i></button></td>
                </tr> --}}
              </tbody>
          </table>
          </div>
          <div class="col-md-12">
              <nav aria-label="Page navigation example">
                  <ul class="pagination">
                      <li class="page-item" v-if="pagination.current_page > 1">
                          <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                      </li>
                      <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                          <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                      </li>
                      <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                          <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                      </li>
                  </ul>
              </nav>
          </div>
          <!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" class="btn btn-primary" @click.prevent="cleanSearch()">Limpiar</button>
          </div>
      </div>
      <!-- /.box -->
      <div class="modal modal-primary fade" id="modal-primary">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Editar Producto</h4>
            </div>
            <form class="form-horizontal">
              <div class="modal-body">
                
                <div class="form-group">
                  <label for="id" class="col-sm-3 control-label text-primary">ID</label>
                  <div class="col-sm-9">
                    <input type="number" class="form-control" id="id" v-model="editProduct.id" disabled>
                  </div>
                </div>

                <div class="form-group">
                  <label for="producto" class="col-sm-3 control-label text-primary">Producto</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="producto" v-model="editProduct.producto">
                  </div>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                <button type="button" class="btn btn-outline btn-outline-primary" :disabled="updateButton" @click="updateProduct(editProduct)"><i class="fa fa-check"></i> Guardar Cambios</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </div>
  `,
  data(){
    return {
      msg: 'Listado de Productos',
      products: [],
      products_copy: [],
      product: '',
      editProduct: {},
      editID: 0,
      medidas: [],
      updateButton: false,
      pagination:{},
      offset: 2
    }
  },
  mounted(){
    this.loadProducts(1);
  },
  computed: {
    isActive(){
      return this.pagination.current_page;
    },
    pageNumber(){
      if(!this.pagination.to){
          return [];
      }
      var desde = this.pagination.current_page - this.offset;
      if(desde < 1){
          desde = 1;
      }
      var hasta = desde + (this.offset * 2);
      if(hasta >= this.pagination.last_page) {
          hasta = this.pagination.last_page;
      }
      var pagesArray = [];
      while(desde <= hasta) {
          pagesArray.push(desde);
          desde++;
      }
      return pagesArray;
    }
},
  methods: {
    cambiarPagina(page){
      this.loadProducts(page);
    },
    loadProducts(page){
      block();
      var url = "/recmat/productos?page="+page;
      axios.get(url)
        .then(response => {
          unblock();
          this.products = response.data.data;
          this.pagination = response.data;
        })
        .catch(error => {
          unblock();
          console.log(error);
        });
    },
    validateBeforeSearchProducts(nombre){
      this.$validator.validate().then(result => {
        if (!result) {
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'El campo de búsqueda esta vacio !'
          });
        }else{
          this.searchProducts(nombre);
        }
      });
    },
    cleanSearch(){
      this.products = this.products_copy;
      this.product = '';
    },
    editProductModel(product){
      console.log(product)
      this.editProduct = product;
      this.editID = product.id;
    },
    updateProduct(product){
      this.updateButton = true;
      axios.put('/recmat/admin/productos/' + this.editID, 
        { 
          id: product.id, 
          producto: product.producto.toUpperCase(), 
          {{-- vigencia: product.vigencia.toUpperCase(), --}}
          unidadmedida_id: product.unidadmedida_id,})
        .then(response => {
          swal({
            type: 'success',
            title: 'Great !',
            text: 'Actualizado correctamente',
          });
          this.updateButton = false;
        })
        .catch(error => {
          console.log(error);
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'Ocurrio algo mal!',
            footer: error,
          });
          this.updateButton = false;
        })
    },
    deleteProduct(product){
      axios.delete('/recmat/admin/productos/' + product.id)
        .then(response => {
          if(response.data.status == 'ok'){
            let prod = this.products.map((p)=>{return p.id}).indexOf(response.data.data.id);
            this.products.splice(prod, 1);
            swal({
              type: 'success',
              title: 'Great !',
              text: 'Eliminado correctamente',
            });
          }else{
            swal({
              type: 'warning',
              title: 'Oops...',
              text: 'Ocurrio algo mal!',
              footer: response.data,
            }); 
          }
        })
        .catch(error => { 
          console.log(error); 
          swal({
            type: 'error',
            title: 'Oops...',
            text: 'Ocurrio algo mal!',
            footer: error,
          }); 
        })
    },
    buscarProducto() {
      if(this.product.length < 3) return;
      axios.get(`/recmat/productos/partidas/${escape(this.product)}/${localStorage.getItem('recm_token')}`)
        .then(res => {
          this.products = res.data.data;
          console.log(res.data);
          this.pagination = res.data;
        });
    },
  }
});