@extends('recmat::layouts.master')

@section('content-title', 'Recursos Materiales')
@section('content-subtitle', 'Requisiciones')

@section('content')
<div id="requisicion">
    <div style="background-color: #74D1EA; color: white; padding-top: 2em; padding-bottom: 2em">
      <h1 class="text-center">Bienvenido al sistema de requisiciones.</h1>
      {{-- <h2 class="text-center">:(</h2> --}}
      {{-- <h3 class="text-center">Estamos trabajando para mejorar. Disculpe las molestias</h3> --}}
    </div>
    <transition name="fade" mode="out-in">
        <router-view></router-view>
    </transition>
</div>
@stop

@section('other-scripts')
<script>
try{
  localStorage.setItem('recm_token', '{{$token}}');
}catch(error){
  swal('Oops !', 'Algo salio mal. Intente recargar la página', 'warning');
}
Vue.use(VeeValidate);

const routes = [
  // { path: '/', component:  },
  // { path: '/', component:  },
  // { path: '/', component:  },
];

const router = new VueRouter({
  routes
});

const app = new Vue({
  router
}).$mount('#requisicion');
</script>
@stop