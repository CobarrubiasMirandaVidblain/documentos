@extends('recmat::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
.v-select input[type=search], .v-select input[type=search]:focus{
  border: none;
}
.lds-ripple {
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
}
.lds-ripple div {
  position: absolute;
  border: 4px solid #EF426F;
  opacity: 1;
  border-radius: 50%;
  animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
}
.lds-ripple div:nth-child(2) {
  animation-delay: -0.5s;
}
@keyframes lds-ripple {
  0% {
    top: 28px;
    left: 28px;
    width: 0;
    height: 0;
    opacity: 1;
  }
  100% {
    top: -1px;
    left: -1px;
    width: 58px;
    height: 58px;
    opacity: 0;
  }
}
.d-flex{
  display: flex;
}
.table-bordered, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td{
  border-color: #D12654;
}
/* .v-select .dropdown-toggle{
  border: 1px solid #D12654 !important;
} */
</style>
@stop

@section('content-title', 'Recursos Materiales')
@section('content-subtitle', 'Almacen')

@section('content')
<div id="app">        
    <transition name="fade" mode="out-in">
        <router-view></router-view>
    </transition>
</div>
@stop

@section('other-scripts')
<script>
Vue.component('v-select', VueSelect.VueSelect);
Vue.use(VeeValidate);

const EventBus = new Vue();

@include('recmat::almacen.in.orden')
@include('recmat::almacen.out.exit')
@include('recmat::almacen.reprint')
@include('recmat::almacen.list')
@include('recmat::almacen.listOut')

const routes = [
  { path: '/entrada', component: Salida },
  { path: '/salida', component: WarehouseExit },
  { path: '/reimpresion', component: Reimprimir },
  { path: '/listado', component: Listado },
  { path: '/listadoOut', component: ListadoOut }
//   { path: '/listado', component: WarehouseIn },
]
const router = new VueRouter({
  routes
});
const app = new Vue({
  router
}).$mount('#app')

if(! localStorage.getItem('recm_token')){
  $('#notificaciones').text('1');
  $('#notificaciones-titulo').text('Tienes notificaciones');
  $('#notificaciones-lista').html(`<a href="/recmat">
    <i class="fa fa-refresh text-yellow" id="reinicio"></i> Recargue la pagina
    </a>`);
}else{
  $('#notificaciones-ul').remove();
}
</script>
@stop