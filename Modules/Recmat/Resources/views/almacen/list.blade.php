const Listado = Vue.component('listado', {
  template: `
  <div>
    <div>
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Listado Entradas <button type="button" class="btn btn-box-tool" @click.prevent="listEntradas(1), busqueda = ''"><i class="fa fa-refresh"></i></button></h3>

          <div class="box-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control pull-right" placeholder="Folio" v-model="busqueda">

              <div class="input-group-btn">
                <button type="submit" class="btn btn-default" @click.prevent="searchEntrada(1)"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Folio requisicion</th>
                <th>Estatus</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="entrada in entradas">
                <th>@{{entrada.folio}}</th>
                <th>@{{entrada.fecha_hora}}</th>
                <th>@{{entrada.requisicion.folio}}</th>
                <th><span :class="'label '+entrada.estatus.color">@{{entrada.estatus.estatus}}</span></th>
                <th>
                    {{-- <button v-if="entrada.estatus_id != 19 && entrada.estatus_id < 7" class="btn btn-danger btn-xs" title="Cancelar" @click.prevent="changestatus(entrada.id,19,1)"><i class="fa fa-ban"></i></button> --}}
                    <button class="btn btn-xs " title="Vista Detalle" @click.prevent="showEntrada(entrada)"><i class="fa fa-file-text-o"></i></button>
                    <button class="btn btn-xs" title="Imprimir" @click.prevent="print(entrada)"><i class="fa fa-print"></i></button>
                </th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-12">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item" v-if="pagination.current_page > 1">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                </li>
                <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                </li>
                <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                    <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                </li>
            </ul>
        </nav>
      </div>
    </div>
    {{-- MODAL --}}
    <div class="modal modal-primary fade" id="modalRequisicion" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
              <h4 class="modal-title">Detalle de Entrada</h4>
          </div>
          <div class="modal-body">
              <h5 class="col-md-4 control-label text-primary">Folio: @{{entrada.folio}}</h5>
              <h5 class="col-md-4 control-label text-primary">Fecha: @{{entrada.fecha_hora}}</h5>
              <h5 class="col-md-4 control-label text-primary">Folio Requisicion: @{{entrada.requisicion.folio}}</h5>
              {{-- <h3 class="col-md-12 control-label text-primary">Proveedor :</h3> --}}
              {{-- <h5 class="col-md-12 control-label text-primary">Nombre: @{{entrada.ordencompra.proveedor.nombre}}</h5>
              <h5 class="col-md-12 control-label text-primary">Direccion: @{{entrada.ordencompra.proveedor.calle + ' - '+ entrada.ordencompra.proveedor.numero + ' - '+ entrada.ordencompra.proveedor.colonia}} </h5>
              <h5 class="col-md-12 control-label text-primary">RFC: @{{entrada.ordencompra.proveedor.rfc}} </h5> --}}
              <br>
              <br>
              <br>
              <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Productos</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <thead>
                      <tr class="text-muted">
                        <th>Cantidad</th>
                        <th>Presentacion</th>
                        <th>Producto</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="text-light-blue" v-for="prod in entrada.productos">
                        <th>@{{prod.pivot.cantidad}}</th>
                        <th>@{{prod.presentacion.presentacion}}</th>
                        <th>@{{prod.producto.producto}}</th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
              
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
          </div>
        </div>
      </div>
  </div>
    {{-- FIN MODAL --}}
  </div>`,
  data(){
    return {
      entradas: [],
      entrada:{requisicion:{}},
      pagination:{},
      offset: 2,
      busqueda:''
    }
  },
  mounted(){
    axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
    jsreport.serverUrl = '{{config('app.reporter_url')}}';
    this.listEntradas(1);
  },
  computed: { 
    isActive(){
        return this.pagination.current_page;
    },
    pageNumber(){
        if(!this.pagination.to){
            return [];
        }
        var desde = this.pagination.current_page - this.offset;
        if(desde < 1){
            desde = 1;
        }
        var hasta = desde + (this.offset * 2);
        if(hasta >= this.pagination.last_page) {
            hasta = this.pagination.last_page;
        }
        var pagesArray = [];
        while(desde <= hasta) {
            pagesArray.push(desde);
            desde++;
        }
        return pagesArray;
    }
  },
  methods: {
    listEntradas(page){
      let me = this;
      block();
      axios.get('/recmat/almacen/entradas?page='+page).then(response => {
        me.entradas = response.data.data;
        me.pagination = response.data;
        unblock();
      }).catch(error => {
        unblock();
        console.log(error);
      });
    },
    cambiarPagina(page){
      this.listEntradas(page);
    },
    changestatus(id,status,tipo){
      block();
      let me = this;
      var url = "/recmat/almacen/estatus/"+id+"/"+tipo;
      axios.get(url).then(function (response) {
        unblock();
        if(response.data.success){
          Swal({position: 'top-end',type: 'success', title: 'Correcto', showConfirmButton: false, timer: 1500})
          me.listEntradas();
        }                    
        else
          Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
      }).catch(function (error){
        unblock();
        Swal({type: 'error',title: 'Oops...',text: error ,footer: 'Ocurrio un error'})
        console.log("Error "+error)
      })
    },
    showEntrada(entrada){
      let me = this;
      me.entrada = null
      me.entrada = entrada;
      $('#modalRequisicion').modal('show'); 
    },
    print(entrada){
      var request = {
        template:{
            shortid: 'Bypxo_GFN'
        },
        header: {
            Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
        },
        data:{
            folio: entrada.folio,
            fecha_hora: entrada.fecha_hora,
            {{-- proveedor: {
                nombre: entrada.ordencompra.proveedor.nombre,
                rfc:  entrada.ordencompra.proveedor.rfc,
                direccion:  entrada.ordencompra.proveedor.calle + ' ' +  entrada.ordencompra.proveedor.numero + ' ' +  entrada.ordencompra.proveedor.colonia,
            }, --}}
            productos: entrada.productos,
            nombre: 'GENARO ZARAGOZA PEREZ',
            vistobueno: 'MARCELINO SANTOS ROJAS',
            requisicion_folio:entrada.requisicion.folio
        }
      };
      jsreport.render('_blank', request);
    },
    searchEntrada(tipo){
      let me = this;
      if(me.busqueda == '')
        return swal({type: 'error', title:' Error', text: 'Campo de busqueda vacio'})
        
      block();
      var url = "/recmat/almacen/folio/"+me.busqueda+"/"+tipo;
      axios.get(url).then(function (response) {
        unblock();
        if(response.data.length)
        {
          me.entradas = response.data;
          me.pagination = {};
        }
        else
          swal({type: 'info', title:' Sin resultados', text: 'Sin resultados'})
        
      }).catch(function (error){
        unblock();
        console.log("Error "+error)
      })
    }
  }
     
});