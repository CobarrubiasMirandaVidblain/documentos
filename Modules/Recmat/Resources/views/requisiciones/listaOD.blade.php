const ListaOD = Vue.component('listaod', {
  template: `
  <div>
    @if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
    @endif

  <div class="row">
      <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Lista de requisiciones</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Folio</th>
                      <th>Estatus</th>
                      <th>Fecha de creación</th>
                      <th>Solicitante</th>
                      <th>Tipo</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                      <tr v-for="l in listaRequisiciones">
                        <td>@{{l.folio}}</td>
                        <td ><span :class="'label '+l.estatus.color">@{{l.estatus.estatus}}</span></td>
                        <td>@{{l.fecha_captura}}</td>
                        <td>@{{l.usuario.usuario}}</td>
                        <td>@{{l.tipo.tipo}}</td>
                        <td >
                          <button v-if="l.estatus_id == 4" class="btn btn-primary btn-xs" title="Cancelar" @click.prevent="cancelRequisiciones(l.folio, l.id)"><i class="fa fa-ban"></i></button>
                          <button class="btn btn-xs " title="Vista Detalle" @click.prevent="showRequisiciones(l.id)"><i class="fa fa-file-text-o"></i></button>
                        </td>
                      </tr>
                  </tbody>               
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
  </div>
  <div class="col-md-12">
      <nav aria-label="Page navigation example">
          <ul class="pagination">
              <li class="page-item" v-if="pagination.current_page > 1">
                  <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
              </li>
              <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                  <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
              </li>
              <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                  <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
              </li>
          </ul>
      </nav>
  </div>

  <div class="modal modal-primary fade" id="modalRequisicion" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title">Vista detalle</h4>
              </div>
              <div class="modal-body">
                  <div class="col-md-12">
                      <div class="row">
                          <div v-show="requisicion.estatus_id == 3" class="alert alert-danger alert-dismissible">
                              <h4><i class="icon fa fa-ban"></i> Motivo!</h4>
                              @{{requisicion.observacion}}
                          </div>
                          <h5 for="id" class="col-md-6 control-label text-primary">Folio: @{{requisicion.folio}}</h5>
                          <h5 for="id" class="col-md-6 control-label text-primary">Fecha captura: @{{requisicion.fecha_captura}}</h5>
                          <h3 for="id" class="col-md-12 control-label text-primary text-center">Area : @{{area.nombre}}</h3>
                          <h5 for="id" class="col-md-4 control-label text-primary">Solicitante : </h5>
                          <h5 class="col-md-8 text-muted text-left"> @{{solicitante.usuario}}</h5>
                          <h5 for="id" class="col-md-4 control-label text-primary">Cargo solicitante : </h5>
                          <h5 class="col-md-8 text-muted text-left"> @{{nivel.nivel}}</h5>
                          <h5 for="id" class="col-md-4 control-label text-primary">Autoriza :</h5>
                          <h5 class="col-md-8 text-muted text-left">@{{autoriza.nombre}} @{{autoriza.primer_apellido}} @{{autoriza.segundo_apellido}}</h5>
                          <h5 for="id" class="col-md-4 control-label text-primary">Cargo Autoriza : </h5>
                          <h5 class="col-md-8 text-muted text-left">@{{requisicion.cargoAutoriza}}</h5>
                          <h5 class="col-md-4 control-label text-primary">Partida : </h5>
                          <h5 class="col-md-8 text-muted text-left">@{{requisicion.partida.partida_especifica_c}} - @{{requisicion.partida.partida_especifica}}</h5>
                          <h5 for="id" class="col-md-5 control-label text-primary">Justificación de compra :</h5> 
                          <h5 class="col-md-12 text-muted text-justify"> @{{requisicion.justificacion}}</h5>
                      </div>
                  </div>
                  <div class="col-xs-12">
                      <div class="box">
                          <div class="box-header">
                              <div class="col-md-8"> <h3 class="box-title">Productos</h3></div>
                              <div v-show="requisicion.estatus_id == 3 || requisicion.estatus_id == 1" class="col-md-4" align ="right"><button class="btn btn-xs bg-yellow" title="Editar" @click.prevent="editmode=true"> <i class="fa fa-pencil"></i></button></div>
                          </div>
                          <div class="box-body table-responsive no-padding">
                              <table class="table table-hover">
                                  <thead>
                                      <tr class="text-primary">
                                          <td>Cantidad</td>
                                          <td>Producto</td>
                                          <td>Presentación</td>
                                          <td>Observación</td>
                                          <td>Precio</td>
                                          <td>Estatus</td>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr v-for="(p,index) in productos"  :class="p.pivot.status_id == 5 ? 'text-muted': p.pivot.status_id == 6 ? 'text-success':'text-yellow'">
                                          <th >@{{p.cantidadT}}</th>{{-- <th>@{{p.pivot.cantidad}}</th> --}}
                                          <th>@{{p.producto.producto}}</th>
                                          <th>@{{p.presentacion.presentacion}}</th>
                                          <th>@{{p.pivot.observacion}}</th>
                                          <th>$ @{{p.precioT}}</th>{{-- <th>$ @{{p.pivot.precio}}</th> --}}
                                          <th v-if="p.pivot.status_id == 5"><span class="label label-default">En compra</span></th>
                                          <th v-else-if="p.pivot.status_id == 6"><span class="label label-success">Entregado</span></th>
                                          <th v-else><span class="label label-warning">Pendiente</span></th>                                      
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
              </div>
          </div>
      </div>
  </div>
</div>`,
  data() {
    return {
      listaRequisiciones:{},
      requisicion :{
        partida:{}
      },
      offset: 2,
      area:{},
      solicitante:{},
      nivel:{},
      autoriza:{},
      productos:{},
      pagination:{},
    }
  },
  mounted(){
    jsreport.serverUrl = '{{env('REPORTER_URL', 'http://192.168.0.10:3001')}}';
    axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
    this.listRequisiciones(1);
  },
  methods:{
    listRequisiciones(page){
      let me = this;
      block();
      var url = "/recmat/requisicion/listaRequisicionesAll?page="+page;
      axios.get(url).then(function (response) {
            unblock();
           me.listaRequisiciones = response.data.data;
           me.pagination = response.data;
      }).catch(function (error){
            unblock();
           console.log("Error "+error)
      })
  },
  cambiarPagina(page){
    this.listRequisiciones(page);
  },
  cancelRequisiciones(folio,id){
    swal({
        title: 'Cancelar requisicion?',
        text: "Folio : "+ folio,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, cancelar'
      }).then((result) => {
        if (result.value) {
            const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
            Swal.mixin({
                input: 'text',
                confirmButtonText: 'Guardar',
              }).queue([
                {
                  title: 'Observacion',
                  text: 'Motivo de la cancelacion'
                },
              ]).then((result) => {
                if (result.value[0])  {
                    let me = this;
                    var url = "/recmat/requisicion/cancelar/"+id+"/"+result.value;
                    axios.get(url).then(function (response) {
                        toast({type: 'success', title: 'Correcto'})
                        me.listRequisiciones();
                    }).catch(function (error){
                        console.log("Error "+error)
                    })
                }else
                    toast({type: 'error', title: 'Campo observacion vacio'})
              })
        }
    })
  },
  showRequisiciones(id){
    block();
    let me = this;
    me.timeline = false;
    var url = "/recmat/requisicion/"+id;
    axios.get(url).then(function (response) {
        unblock();
        me.requisicion = response.data.requisicion;
        me.area = response.data.areaResp.area;
        me.solicitante = response.data.solicitante;
        me.nivel = response.data.empleado.nivel;
        me.autoriza = response.data.autoriza;
        me.productos = response.data.requisicion.productos;
        $('#modalRequisicion').modal('show'); 
    }).catch(function (error){
        unblock();
        console.log("Error "+error)
    })
  },
  },
  computed: {
    isActive(){
        return this.pagination.current_page;
    },
    pageNumber(){
        if(!this.pagination.to){
            return [];
        }
        var desde = this.pagination.current_page - this.offset;
        if(desde < 1){
            desde = 1;
        }
        var hasta = desde + (this.offset * 2);
        if(hasta >= this.pagination.last_page) {
            hasta = this.pagination.last_page;
        }
        var pagesArray = [];
        while(desde <= hasta) {
            pagesArray.push(desde);
            desde++;
        }
        return pagesArray;
    }
},
});

