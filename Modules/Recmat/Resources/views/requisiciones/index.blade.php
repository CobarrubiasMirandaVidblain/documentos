const Requisiciones = Vue.component('requisiciones', {
    template: `
    <div class="row">
        <div class="col-md-12">
            <p>Tipo de Requisicion?</p>
            <div class="form-group col-md-12" :class="errors.first('tipoRequisicion') ? 'has-error' : ''" >
                <v-select :options="listtipoSolicitud" label="tipo" v-model="recq.tipoSelect" v-validate="'required:true'" name="tipoRequisicion"></v-select> 
                <span class="text-danger">@{{ errors.first('tipoRequisicion') }}</span>
            </div>
        </div>
        <div v-if="recq.tipoSelect">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Nueva requisicion</h3>
                        <div class="box-tools">
                        
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center">
                                    {{-- <strong>Fecha: @{{recq.fecha }} - @{{recq.fecha_plazo}}</strong> --}}
                                </p>
                                <div class="chart">
                                <!-- formulario -->
                                    <form autocomplete="off" id="formRequisicion">
                                        <div class="form-group col-md-12"  :class="errors.first('area') ? 'has-error' : ''" >
                                            <label for="area">AREA</label> 
                                            <v-select :options="listArea" label="area_nombre" v-model="recq.area" v-validate="'required:true'" name="area"></v-select>  
                                            <span class="text-danger">@{{ errors.first('area') }}</span>
                                        </div>
                                        <div class="form-group col-md-6"  >
                                            <label for="solicitante">SOLICITANTE</label> 
                                            <input type="text" id="solicitante" name="solicitante"  class="form-control" v-model="recq.area.responsable_nombre" disabled v-validate="'required:true'" > 
                                            <span class="text-danger"></span>
                                        </div>
                                        <div class="form-group col-md-6" :class="errors.first('cargoSolicitante') ? 'has-error' : ''" >
                                            <label for="cargoSolicitante">CARGO SOLICITANTE</label> 
                                            <input type="text" id="cargoSolicitante" name="cargoSolicitante" class="form-control" v-model="recq.area.cargo" disabled v-validate="'required:true'" > 
                                            <span class="text-danger"></span>
                                        </div>
                                        <div class="form-group col-md-6" :class="errors.first('autoriza') ? 'has-error' : ''" >
                                            <label for="autoriza">AUTORIZA</label>
                                            <input type="text" class="form-control" id="autoriza"  name="autoriza" v-model="recq.autoriza"  disabled v-validate="'required:true'">
                                            <span class="text-danger">@{{ errors.first('autoriza') }}</span>
                                        </div>
                                        <div class="form-group col-md-6" :class="errors.first('cargoAutoriza') ? 'has-error' : ''" >
                                            <label for="cargoAutoriza">CARGO AUTORIZA</label>
                                            <input type="text" class="form-control" id="cargoAutoriza"  name="cargoAutoriza" v-model="recq.cargoAutoriza"   disabled v-validate="'required:true'">
                                            <span class="text-danger">@{{ errors.first('cargoAutoriza') }}</span>
                                        </div>
                                        <div class="form-group col-md-12" :class="errors.first('partida') ? 'has-error' : ''" >
                                            {{-- <label for="partida">Partida</label>
                                            <v-select :options="partidas" label="partida_especifica" v-model="recq.partida" @search="findPartida" :filterable='false' :disabled="disabled"></v-select>
                                            <span class="text-danger">@{{ errors.first('partida') }}</span>
                                            <br>
																						<br> --}}
																						
																						<label class="col-form-label" for="appendedInputButton">Partida (s):</label>
																						<multiselect v-model="recq.partida" :options="partidas" label="partida_especifica" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="buscarPartida" selectLabel="Enter para seleccionar" deselectLabel="Enter para deseleccionar" :showNoOptions="false" placeholder="Selecciona">
																							<template slot="noOptions">Comience a escribir para buscar.</template>
																							<template slot="noResult"><em style="color:red;">No tenemos registro con lo que estas buscando</em></template>
																						</multiselect>
																						
                                        </div>
                                        <div class="col-md-12">
                                            <br><br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                            {{-- <div class="col-md-4">
                                <p class="text-center">
                                    <strong>Presupuesto</strong>
                                </p>
                                <div class="progress-group">
                                    <span class="progress-text">Partida 1</span>
                                    <span class="progress-number"><b>@{{money.partida1}}</b>/@{{money.partida1T}}</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-aqua" :style="money.styleP1T"></div>
                                    </div>
                                </div>
                                <div class="progress-group">
                                    <span class="progress-text">Partida 2</span>
                                    <span class="progress-number"><b>@{{money.partida2}}</b>/@{{money.partida2T}}</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-red" :style="money.styleP2T"></div>
                                    </div>
                                </div>
                                <div class="progress-group">
                                    <span class="progress-text">Total</span>
                                    <span class="progress-number"><b>@{{money.partida1 + money.partida2}}</b>/@{{ money.partida2T + money.partida1T}}</span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-green" :style="money.styleTotal"></div>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-group col-md-4" :class="errors.first('justificacion') ? 'has-error' : ''" >
                                <label for="justificacion">JUSTIFICACION DE LA COMPRA</label>
                                <textarea class="form-control" rows="13" name="justificacion"  v-validate="{required:true, max:253}" v-model="recq.justificacion"></textarea>
                                <span class="text-danger">@{{ errors.first('justificacion') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-success box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">AGREGAR PRODUCTOS</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-4" :class="errors.first('producto') ? 'has-error' : ''" >
                             <label for="producto">PRODUCTO</label>
                           {{-- <multiselect :options="products" label="productoNombre" v-model="producto.nombre" @search="findProduct" :filterable='false'></multiselect>
														<span class="text-danger">@{{ errors.first('producto') }}</span> --}}
														<multiselect v-model="producto.nombre" :options="products" label="productoNombre" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="findProduct" selectLabel="Enter para seleccionar" deselectLabel="Enter para deseleccionar" :showNoOptions="false" placeholder="Selecciona">
															<template slot="noOptions">Comience a escribir para buscar.</template>
															<template slot="noResult"><em style="color:red;">No tenemos registro con lo que estas buscando</em></template>
														</multiselect>
                        </div>
                        <div class="form-group col-md-4" :class="errors.first('observacion') ? 'has-error' : ''" >
                            <label for="observacion">Observación</label>
                            <textarea rows="2" name="observacion" v-model="producto.observacion" class="form-control" v-validate="'max:253'"></textarea>
                            <span class="text-danger">@{{ errors.first('observacion') }}</span>
                        </div>
                        {{-- <div class="form-group col-md-2" :class="errors.first('cantidad') ? 'has-error' : ''" >
                            <label for="cantidad">CANTIDAD</label>
                            <input type="text" class="form-control" id="cantidad"  name="cantidad" v-model="producto.cantidad" v-validate="'numeric'">
                            <span class="text-danger">@{{ errors.first('cantidad') }}</span>
                        </div> --}}

                        <div class="form-group col-md-4" :class="errors.first('cantidad') ? 'has-error' : ''" >
                            <label for="cantidad">CANTIDAD</label>
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control"  v-model="producto.cantidad" name="cantidad" v-validate="'numeric'">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat" @click.prevent="addProducto()"><i class="fa fa-plus"></i> Agregar</button>
                                </span>
                            </div>
                            <span class="text-danger">@{{ errors.first('cantidad') }}</span>
                        </div>
                        
                        {{-- <div class="form-group col-md-2" :class="errors.first('precio') ? 'has-error' : ''" > --}}
                            {{-- <label for="precio">PRECIO</label> --}}
                            {{-- <div class="input-group input-group-sm"> --}}
                                {{-- <input type="text" class="form-control"  v-model="producto.precio" name="precio" disabled> --}}
                                {{-- <span class="input-group-btn"> --}}
                                    {{-- <template v-if="((money.partida1 + money.partida2) < (money.partida2T + money.partida1T))"> --}}
                                        {{-- <button type="button" class="btn btn-info btn-flat" @click.prevent="addProducto()"><i class="fa fa-plus"></i> Agregar</button> --}}
                                    {{-- </template>                                 --}}
                                {{-- </span> --}}
                            {{-- </div> --}}
                            {{-- <span class="text-danger">@{{ errors.first('precio') }}</span> --}}
                        {{-- </div> --}}
                        <div class="col-md-12">
                            <div class="box box-success">
                                <div class="box-header">
                                </div>
                                <div class="box-body no-padding">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                            <th style="width: 60px">Cantidad</th>
                                            <th>Producto</th>
                                            <th>Presentacion</th>
                                            <th>Observacion</th>
                                            {{-- <th style="width: 90px">Precio</th> --}}
                                            <th style="width: 40px">Eliminar</th>
                                            </tr>
                                            <tr v-for="(p , index) in recq.listProductos">
                                                <th>@{{p.cantidad}}</th>
                                                <th>@{{p.nombre}}</th>
                                                <th>@{{p.presentacion}}</th>
                                                <th>@{{p.observacion}}</th>
                                                {{-- <th>$ @{{parseFloat(p.precio).toFixed(2)}}</th> --}}
                                                <th><button type="button"  class="btn btn-primary btn-xs" @click.prevent="deleteProductoList(index,p.precio)"><i class="fa fa-trash"></i></button></th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a class="btn btn-primary" @click.prevent="saveRequisicion()">Guardar</a>
            </div>
        </div>
    </div>
		`,
		
    props:['id'],
    data(){
      return {
          products: [],
          partidas: [],
          measures: [],
          disabled:false,
          recq:{
                fecha:'',
                fecha_plazo:'',
                area :{},
                solicitante:'',
                cargoSolicitante:'',
                autoriza:'',
                cargoAutoriza:'',
                cveAutoriza:'0074157961',
                partida:'PAPELERIA',
                justificacion:'',
                listProductos : [],
                partida:'',
                tipoSelect:''
          },
          money:{
              partida1T:6000,
              partida2T:400,
              partida1:0,
              partida2:0,
              styleP1T:'width: 0%',
              styleP2T:'width: 0%',
              styleTotal:'width: 0%',
          },
          producto:{
              nombre:'',
              presentacion:'',
              precio:'',
              cantidad:'',
              observacion:''
          },
          busqueda:{
              folio:''
          },
          listArea:[],
          listtipoSolicitud:[{"tipo":"REQUISICION","id":1},{"tipo":"MANTENIMIENTO","id":2}]

      }
    },
    mounted(){
        {{-- this.loadProducts(); --}}
        this.loadData();
        axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
        {{-- socket = io.connect('http://192.168.0.149:8081?rol=AREA CAPTURA', { 'forceNew': true }); --}}
    },
    methods: {
        {{-- loadProducts(){
            axios.get('/recmat/productos').then(response => {
                this.products = response.data;
              }).catch(error => {
                console.log(error);
              });
        }, --}}
        loadData(){
            block();
            axios.get('/recmat/admin/datos').then(response => {
                this.listArea = response.data.area
                {{-- this.recq.solicitante = response.data.solicitante.persona.nombre +' '+ response.data.solicitante.persona.primer_apellido
                this.recq.area = response.data.empleado.area.nombre
                this.recq.cargoSolicitante = response.data.empleado.nivel.nivel
                this.recq.autoriza = response.data.autoriza.nombre
                this.recq.cargoAutoriza = response.data.areaResp.area.nombre
                this.recq.fecha = response.data.fecha
                this.recq.fecha_plazo = response.data.fechaFin --}}
                unblock();
            }).catch(error => {
                unblock();
                console.log(error);
            });
        },
        addProducto(){
            let me = this;
            if(me.producto.nombre && me.producto.precio && me.producto.cantidad >= 1  && me.producto.observacion)
            {   
                {{-- if((me.producto.precio * me.producto.cantidad) > (me.money.partida1T - me.money.partida1))
                    return me.alerta('error','Error','Credito insuficiente'); --}}

                let productoFind = this.recq.listProductos;
                productoFind = productoFind.filter(p => {
                    return p.nombre == me.producto.nombre.producto.producto && p.presentacion == me.producto.nombre.presentacion.presentacion && p.observacion == me.producto.observacion;
                    {{-- return p.nombre.match(me.producto.nombre.producto.producto) && p.presentacion.match(me.producto.nombre.presentacion.presentacion); --}}
                    })
                if (productoFind.length > 0) {
                    var preciold = (productoFind[0].precio)/(productoFind[0].cantidad)
                    productoFind[0].cantidad = parseInt(productoFind[0].cantidad)  + parseInt(me.producto.cantidad)
                    productoFind[0].precio = parseFloat(preciold * productoFind[0].cantidad).toFixed(2)
                }
                else
                    me.recq.listProductos.push({'nombre': me.producto.nombre.producto.producto, 'presentacion':me.producto.nombre.presentacion.presentacion, 'idPresentacion':me.producto.nombre.id,'precio': parseFloat(me.producto.precio * me.producto.cantidad).toFixed(2), 'cantidad':me.producto.cantidad, 'precioU':parseFloat(me.producto.precio).toFixed(2), 'observacion': me.producto.observacion})

                me.moneyPorcentaje(null);
                me.producto.nombre = '';
                {{-- me.producto.presentacion = ''; --}}
                me.producto.cantidad = '';
                me.producto.precio = '';
                me.producto.observacion = '';
                me.$validator.reset();
            }
            else
                me.alerta('error','Error','Existen campos vacios');
            
        },
        deleteProductoList(producto,precio){
            let me = this;
            me.recq.listProductos.splice(producto,1);
            me.moneyPorcentaje(precio)
        },
        moneyPorcentaje(precio){
            let me = this;
            me.money.partida1 = (precio >0) ? me.money.partida1 - precio :  (me.producto.precio * me.producto.cantidad) + me.money.partida1;
            me.money.styleP1T = 'width:'+((me.money.partida1 / me.money.partida1T) * 100)+'%'
            me.money.styleTotal = 'width:'+(((me.money.partida1 + me.money.partida2 ) / (me.money.partida1T + me.money.partida2T)) * 100)+'%'
        },
        buscarFolio(){
            let me = this;
            if(me.busqueda.folio){
                me.busqueda.folio = '';
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Sin resultados!'
                  })
            }else
                me.alerta('error','Error','El campo de folio esta vacio !');            
        },
        saveRequisicion(){
            let me = this;
            if(me.recq.listProductos.length == 0)
                return me.alerta('error','Error','Lista de productos vacia');
            if(me.recq.area == null)
                return me.alerta('error','Error','Seleccione Area');
            this.$validator.validateAll().then((result) => {
                if(result) {
                    block();
                    var url = "/recmat/requisicion/save";
                    axios.post(url,this.recq).then(function (response) {
                        unblock();
                        if(response.data.success)
                        {
                            me.alerta('success','Bien','Folio : '+response.data.folio);
                            me.recq.listProductos = [];
                            me.recq.justificacion = '';
                            me.recq.fecha_plazo   = '';
                            me.money.styleP1T     = 'width: 0%',
                            me.money.styleP2T     = 'width: 0%',
                            me.money.styleTotal   = 'width: 0%',
                            me.money.partida1T    = 600,
                            me.money.partida2T    = 400,
                            me.money.partida1     = 0,
                            me.money.partida2     = 0,
                            me.$validator.reset();
                        }
                        else
                            me.alerta('error','Error','Ocurrio un error');
                    }).catch(function (error){
                        unblock();
                        me.alerta('error','Error','Problemas de conexion');
                    })
                }
                else
                    me.alerta('error','Error','Existen campos vacios');
            });
        },
        alerta(type,msg,text){
            swal({
                type: type,
                title: msg,
                text: text,
              })
        },
        findProduct(search, loading) {
            if(this.recq.partida)
            {
							let me = this;
							if(search.length < 2) return;
							axios.get(`/recmat/productos/partidaProd/${escape(search)}/${this.recq.partida.id}`).then(response => { 
								{{-- console.log(response.data) --}}
								me.products = response.data.map(item => {
									item.productoNombre = item.producto.producto + ' - ' + item.presentacion.presentacion;
									return item;
								});
							}).catch(error => { console.log(error) });
            }
            else
                this.alerta('error','Error','Selecciona una partida');
            
        },
        {{-- searchP: _.debounce((loading, search, vm) => {
            fetch(`/recmat/productos/partidaProd/${localStorage.getItem('recm_token')}/${escape(search)}/${vm.recq.partida.id}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.products = json.map(item => {
                                item.productoNombre = item.producto.producto + ' - ' + item.presentacion.presentacion;
                                return item;
                            });
                        });
                    loading(false);
                });
        }, 350), --}}
        {{-- findPartida(search, loading) {
            loading(true);
            this.searchPartida(loading, search, this);
        },
        searchPartida: _.debounce((loading, search, vm) => {
            fetch(`/recmat/partida/${localStorage.getItem('recm_token')}/${escape(search)}/${vm.recq.tipoSelect.id}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.partidas = json.map(item => {
                                item.partida_especifica = item.partida_especifica_c+ ' - ' +item.partida_especifica; 
                                return item;
                            });
                        });
                    loading(false);
                });
				}, 350), --}}
				buscarPartida(search){
					let me = this;
					if(search.length < 2) return;
					axios.get(`/recmat/partida/${escape(search)}/${me.recq.tipoSelect.id}`).then(response => { 
						{{-- console.log(response.data) --}}
						me.partidas = response.data.map(item => {
							item.partida_especifica = `${item.partida_especifica_c} - ${item.partida_especifica}`;
							return item;
						});
					}).catch(error => { console.log(error) });
				},
    },
    watch:{
        'producto.nombre':function(){
            let me = this;
            me.producto.precio = me.producto.nombre.precio;
        },
        'recq.partida':function(){
            let me = this;
            me.recq.listProductos = [];
            me.money.styleP1T     = 'width: 0%';
            me.money.styleP2T     = 'width: 0%';
            me.money.styleTotal   = 'width: 0%';
            me.money.partida1T    = 6000;
            me.money.partida2T    = 4000;
            me.money.partida1     = 0;
            me.money.partida2     = 0;
            me.partidas           = [];
            
        },
        'recq.area':function(){ 
            let me = this;
            if(me.recq.area != null && me.recq.area.hasOwnProperty('area_id'))
            {
                block();
                axios.get('/recmat/admin/area/'+me.recq.area.area_id).then(response => {
                    unblock();                    
                    if(response.data.length > 0){
                        me.recq.autoriza = response.data[0].responsable_nombre
                        me.recq.cargoAutoriza = response.data[0].cargo
                    }
                    else{
                        this.alerta('error','Error','N°: 00001');
                        me.recq.autoriza = ''
                        me.recq.cargoAutoriza = ''
                        me.recq.area = ''
                    }
                }).catch(error => {
                    unblock();
                    console.log(error);
                });
            }    
        },
        'recq.tipoSelect':function(){
            let me = this;
            me.recq.partida = null,
            me.recq.area = {},
            me.recq.autoriza = '',
            me.recq.cargoAutoriza = '',
            me.partidas = []
        }
    }
  });