const ListaODMantenimiento = Vue.component('listaodmantenimiento', {
    template: `
        <div class="row">
            <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Listado ordenes de mantenimiento  <button type="button" class="btn btn-box-tool" @click.prevent="listRequisiciones(1), busquedaFolio = ''"><i class="fa fa-refresh"></i></button></h3>
                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar" v-model="busquedaFolio"> 
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default" @click.prevent="searchRequisicion()"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div> 
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Folio</th>
                                        <th>Estatus</th>
                                        <th>Fecha de creación</th>
                                        <th>Solicitante</th>
                                        <th>Tipo</th>
                                        @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','MANTENIMIENTO'], 'recmat'))
                                            <th>OD</th>
                                        @endif
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="l in listaRequisiciones">
                                        <template v-if="l.estatus_id != 1 && l.estatus_id != 3">
                                            <td>@{{l.folio}}</td>
                                            <td ><span :class="'label '+l.estatus.color">@{{l.estatus.estatus}}</span></td>
                                            <td>@{{l.fecha_captura}}</td>
                                            <td>@{{l.usuario.usuario}}</td>
                                            <td>@{{l.tipo.tipo}}</td>
                                            @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','MANTENIMIENTO'], 'recmat'))
                                                <td><a v-if="l.estatus_id >= 5" :href="'/recmat/admin#/ordendecompra/new/'+l.id" class="btn btn-primary"></i> Orden de compra</a></td>
                                            @endif
                                            <td v-if="l.status != 0 ">
                                              <button v-if="l.estatus_id == 4" class="btn btn-success btn-xs" title="Proceso de entrega" @click.prevent="changestatus(l.id,5)"><i class="fa fa-check-square"></i></button>
                                              <button v-if="l.estatus_id < 7" class="btn btn-primary btn-xs" title="Cancelar" @click.prevent="cancelRequisiciones(l.folio, l.id)"><i class="fa fa-ban"></i></button>
                                              <button class="btn btn-xs" title="Imprimir" @click.prevent="print(l)"><i class="fa fa-print"></i></button>
                                              <button class="btn btn-xs bg-yellow" title="Vista Detalle" @click.prevent="showRequisiciones(l.id)"><i class="fa fa-pencil-square-o"></i></button>
                                              <button class="btn btn-xs bg-yellow" title="Proceso" @click.prevent="showTimeLine(l.id,l.folio)"><i class="fa fa-road"></i></button>
                                            </td>
                                            <td v-else ></td>
                                        </template>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item" v-if="pagination.current_page > 1">
                                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                            </li>
                            <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                                <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                            </li>
                            <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="row">
                        <div class="col-md-12" v-if="timeline">     
                            <div class="box-header with-border">
                                <h3 class="box-title">Folio @{{folio}}</h3>
                            </div>
                            <div style="display:inline-block;width:100%;overflow-y:auto;">
                                <ul class="timeline timeline-horizontal" style="margin-top: 10px;">                         
                                    <li class="timeline-item" v-for="h in historial">
                                        <div :class="'timeline-badge '+ h.status.color"><i class="glyphicon glyphicon-check"></i></div>
                                        <div class="timeline-panel">
                                            <div class="timeline-heading">
                                                <h4 :class="'timeline-title label '+h.status.color">@{{h.status.estatus}}</h4>
                                                <div class="timeline-body">
                                                    {{-- <p :class="'label '+h.status.color">algo</p> --}}
                                                </div>
                                                <p>
                                                    <small class="text-muted"><i class="glyphicon glyphicon-time"></i> @{{h.created_at}}
                                                        <br>Quien realizo el cambio: @{{h.usuario.usuario}}
                                                    </small>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal modal-primary fade" id="modalRequisicion" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title">Vista detalle</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div v-show="requisicion.estatus_id == 3" class="alert alert-danger alert-dismissible">
                                        <h4><i class="icon fa fa-ban"></i> Motivo!</h4>
                                        @{{requisicion.observacion}}
                                    </div>
                                    <h5 for="id" class="col-md-6 control-label text-primary">Folio: @{{requisicion.folio}}</h5>
                                    <h5 for="id" class="col-md-6 control-label text-primary">Fecha captura: @{{requisicion.fecha_captura}}</h5>
                                    <h3 for="id" class="col-md-12 control-label text-primary text-center">Area : @{{area.nombre}}</h3>
                                    <h5 for="id" class="col-md-4 control-label text-primary">Solicitante : </h5>
                                    <h5 class="col-md-8 text-muted text-left">@{{autoriza.nombre}} @{{autoriza.primer_apellido}} @{{autoriza.segundo_apellido}}</h5>
                                    <h5 for="id" class="col-md-4 control-label text-primary">Cargo solicitante : </h5>
                                    <h5 class="col-md-8 text-muted text-left"> @{{nivelResponsable.nivel}}</h5>
                                    <h5 for="id" class="col-md-4 control-label text-primary">Autoriza :</h5>
                                    <h5 class="col-md-8 text-muted text-left">@{{responsable.responsable_nombre}} </h5>
                                    <h5 for="id" class="col-md-4 control-label text-primary">Cargo Autoriza : </h5>
                                    <h5 class="col-md-8 text-muted text-left">@{{responsable.cargo}}</h5>
                                    <h5 class="col-md-4 control-label text-primary">Partida : </h5>
                                    <h5 class="col-md-8 text-muted text-left">@{{requisicion.partida.partida_especifica_c}} - @{{requisicion.partida.partida_especifica}}</h5>
                                    <h5 for="id" class="col-md-5 control-label text-primary">Justificación de compra :</h5> 
                                    <h5 class="col-md-12 text-muted text-justify"> @{{requisicion.justificacion}}</h5>
                                </div>
                            </div>
                            <div class="col-md-12 text-primary" v-show="editmode">
                                <div class="form-group col-md-6" :class="errors.first('producto') ? 'has-error' : ''" >
                                    <label for="producto">PRODUCTO</label>
                                    <v-select :options="products" label="productoNombre" v-model="producto.nombre" @search="findProduct" :filterable='false'></v-select>
                                    <span class="text-danger">@{{ errors.first('producto') }}</span>
                                </div>
                                <div class="form-group col-md-6" :class="errors.first('observacion') ? 'has-error' : ''" >
                                    <label for="observacion">Observación</label>
                                    <textarea rows="2" name="observacion" v-model="producto.observacion" class="form-control"></textarea>
                                    <span class="text-danger">@{{ errors.first('observacion') }}</span>
                                </div>
                                <div class="form-group col-md-6" :class="errors.first('cantidad') ? 'has-error' : ''" >
                                    <label for="cantidad">CANTIDAD</label>
                                    <input type="text" class="form-control" id="cantidad"  name="cantidad" v-model="producto.cantidad" >
                                    <span class="text-danger">@{{ errors.first('cantidad') }}</span>
                                </div>
                                <div class="form-group col-md-6" :class="errors.first('precio') ? 'has-error' : ''" >
                                    <label for="precio">PRECIO</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control"  v-model="producto.precio" name="precio" disabled>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-flat" @click.prevent="addProducto()"><i class="fa fa-plus"></i> Agregar</button>                 
                                        </span>
                                    </div>
                                    <span class="text-danger">@{{ errors.first('precio') }}</span>
                                </div>
                                
                            </div>
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <div class="col-md-8"> <h3 class="box-title">Productos</h3></div>
                                        <div v-show="requisicion.estatus_id == 3 || requisicion.estatus_id == 1" class="col-md-4" align ="right"><button class="btn btn-xs bg-yellow" title="Editar" @click.prevent="editmode=true"> <i class="fa fa-pencil"></i></button></div>
                                    </div>
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr class="text-primary">
                                                    <td>Cantidad</td>
                                                    <td>Producto</td>
                                                    <td>Presentación</td>
                                                    <td>Observación</td>
                                                    <td>Precio</td>
                                                    <td>Estatus</td>
                                                    <td v-show="editmode">Opciones</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(p,index) in productos"  :class="p.pivot.status_id == 5 ? 'text-muted': p.pivot.status_id == 6 ? 'text-success':'text-yellow'">
                                                    <th >@{{p.cantidadT}}</th>{{-- <th>@{{p.pivot.cantidad}}</th> --}}
                                                    <th>@{{p.producto.producto}}</th>
                                                    <th>@{{p.presentacion.presentacion}}</th>
                                                    <th>@{{p.pivot.observacion}}</th>
                                                    <th>$ @{{parseFloat(p.precioT).toFixed(2)}}</th>
                                                    <th v-if="p.pivot.status_id == 5"><span class="label label-default">En compra</span></th>
                                                    <th v-else-if="p.pivot.status_id == 6"><span class="label label-success">Entregado</span></th>
                                                    <th v-else><span class="label label-warning">Pendiente</span></th>
                                                    <th  v-show="editmode">
                                                      <button class="btn btn-xs btn-danger" title="Borrar" @click.prevent="editOptions(index,1,p)"><i class="fa fa-trash"></i></button>
                                                      <button class="btn btn-xs btn-success" title="Mas" @click.prevent="editOptions(index,2,p)"><i class="fa fa-plus-square"></i></button>
                                                      <button class="btn btn-xs btn-warning" title="Menos" @click.prevent="editOptions(index,3,p)"><i class="fa fa-minus-square"></i></button>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>`,
    data() {
      return {
          timeline:false,
          editmode : false,
          folio:'',
          listaRequisiciones:{},
          pagination:{},
          requisicion :{
            partida:{}
          },
          offset: 2,
          area:{},
          solicitante:{},
          nivel:{},
          autoriza:{},
          productos:{},
          productoscopy:[],
          historial:{},
          listProveedor:[],
          ordenCompra:{
              listaOrdenCompra:[],
              subtotal:0,
              iva:16,
              total:'',
              totalnoIVA:0,
              requisicion_id:'',
              providerFound:'',
              observacionOD:''
          },
          respuesta:'',
          providers: [],
          providerNotFound: false,
          producto:{
            nombre:'',
            presentacion:'',
            precio:'',
            cantidad:'',
            observacion:''
        },
        products: [],
        responsable:{},
        nivelResponsable:{},
        busquedaFolio:''
      }
    },
    {{-- Metodos para la paginacion --}}
    computed: { 
        isActive(){
            return this.pagination.current_page;
        },
        pageNumber(){
            if(!this.pagination.to){
                return [];
            }
            var desde = this.pagination.current_page - this.offset;
            if(desde < 1){
                desde = 1;
            }
            var hasta = desde + (this.offset * 2);
            if(hasta >= this.pagination.last_page) {
                hasta = this.pagination.last_page;
            }
            var pagesArray = [];
            while(desde <= hasta) {
                pagesArray.push(desde);
                desde++;
            }
            return pagesArray;
        }
    },
    mounted(){
        let me = this;
        me.listRequisiciones(1);
        axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
    },
    methods:{
      cancelRequisiciones(folio,id){
        swal({
            title: 'Cancelar mantenimiento?',
            text: "Folio : "+ folio,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, cancelar'
          }).then((result) => {
            if (result.value) {
                const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
                Swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Guardar',
                  }).queue([
                    {
                      title: 'Observacion',
                      text: 'Motivo de la cancelacion'
                    },
                  ]).then((result) => {
                    if (result.value[0])  {
                        let me = this;
                        var url = "/recmat/requisicion/cancelar/"+id+"/"+result.value;
                        axios.get(url).then(function (response) {
                            toast({type: 'success', title: 'Correcto'})
                            me.listRequisiciones();
                        }).catch(function (error){
                            console.log("Error "+error)
                        })
                    }else
                        toast({type: 'error', title: 'Campo observacion vacio'})
                  })
            }
        })
      },
      listRequisiciones(page){
          let me = this;
          block();
          var url = "/recmat/requisicion/odMantenimiento?page="+page;
          axios.get(url).then(function (response) {
                unblock();
               me.listaRequisiciones = response.data.data;
               me.pagination = response.data;
          }).catch(function (error){
                unblock();
               console.log("Error "+error)
          })
      },
      cambiarPagina(page){
        this.listRequisiciones(page);
      },
      {{-- Mostrar requisicion --}}
      showRequisiciones(id){
        block();
        let me = this;
        me.timeline = false;
        me.editmode = false;
        me.requisicion = {
            partida:{}
        };
        var url = "/recmat/requisicion/"+id;
        axios.get(url).then(function (response) {
            unblock();
            me.requisicion = response.data.requisicion;
            me.area = response.data.areaResp.area;
            me.solicitante = response.data.solicitante;
            me.nivel = response.data.empleado.nivel;
            me.autoriza = response.data.autoriza;
            me.productos = response.data.requisicion.productos;
            me.responsable = response.data.responsable[0];
            me.nivelResponsable = response.data.nivel.nivel;
            $('#modalRequisicion').modal('show'); 
        }).catch(function (error){
            unblock();
            console.log("Error "+error)
        })
      },
      {{-- para agregar, elimar productos de la requisicion --}}
      editOptions(posicion,opcion,dato){
          let me = this;
          if(me.productos.length == 1 && opcion == 1) {{-- validacion para que la lista no quede vacia--}}
            return Swal({position: 'top-end',type: 'error', title: 'No se puede quedar vacia la lista', showConfirmButton: false, timer: 1500});
          if(opcion == 1) {{-- Eliminar --}}
            me.productos.splice(posicion,1);
          else{ {{-- sumar ó restar --}}
            let productoFind = me.productos;
            productoFind = productoFind.filter(p => {
                return p.producto.producto == dato.producto.producto && p.presentacion.presentacion == dato.presentacion.presentacion;
                })
            if (productoFind.length > 0) {
                if(opcion ==2) {{-- sumar --}}
                    productoFind[0].cantidadT = parseInt(productoFind[0].cantidadT)+1
                else {{-- resta --}}
                    productoFind[0].cantidadT = (parseInt(productoFind[0].cantidadT)-1) <= 0 ? 1 : parseInt(productoFind[0].cantidadT)-1
            }
            return productoFind;
          }
      },
      updateRequisicion(){
            block();
          let me = this;
          var url = "/recmat/requisicion/update";
          axios.post(url,me.requisicion).then(function (response) {
            unblock();
            if(response.data.success)
            {
                $('#modalRequisicion').modal('toggle');
                Swal({position: 'top-end',type: 'success', title: 'Se actualizo correctamente', showConfirmButton: false, timer: 1500});
            }
            else
                Swal({position: 'top-end',type: 'error', title: 'Ocurrio un error', showConfirmButton: false, timer: 1500});
          }).catch(function (error){
                unblock();
              console.log("Error "+error)
          })
      },
      showTimeLine(id,folio){
        block();
        let me = this;
        me.timeline = true
        var url = "/recmat/requisicion/historial/"+id;
        axios.get(url).then(function (response) {
            unblock();
            me.historial = response.data
            me.folio = folio
        }).catch(function (error){
            unblock();
            console.log("Error "+error)
        })
      },
      {{--Cambiar estatus de la requisicion --}}
      changestatus(id,status){
        block();
        let me = this;
        var url = "/recmat/requisicion/cambioestatus";
        axios.post(url,{'id':id,'status':status}).then(function (response) {
            unblock();
            if(response.data.success){
                Swal({position: 'top-end',type: 'success', title: 'Correcto', showConfirmButton: false, timer: 1500})
                me.listRequisiciones();
            }                    
            else
                Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
  
        }).catch(function (error){
            unblock();
            console.log("Error "+error)
        })
      },
      {{-- Orden de compra --}}
      newOrdenCompra(){
        $('#modarordencompra').modal('show');
        this.productoscopy = this.productos.slice(0,this.productos.length); {{-- se hace una copia de la lista de productos --}}
        this.ordenCompra.listaOrdenCompra = []; {{-- se limpia la lista de productos que se van a enviar --}}
        this.ordenCompra.requisicion_id = this.requisicion.id
      },
      {{-- form de cantidad,precio,iva --}}
      async addListaOD(index,articulo,valor){ {{-- Agregar producto a la lista de compra --}}
        let me = this;
        if(valor == 1) {{-- se agrega producto --}}
        {
            const {value: formValues} = await Swal({
                html:
                    '<label>Cantidad</label><input id="swal-cantidad" class="swal2-input">' + '<label>Precio unitario</label><input id="swal-precio" class="swal2-input">'+'<label>IVA?</label><br><input type="checkbox" id="swal-iva" checked>',
                focusConfirm: false,
                preConfirm: () => {
                    return {
                        'cantidad':document.getElementById('swal-cantidad').value,
                        'precio':document.getElementById('swal-precio').value,
                        'iva':$('#swal-iva').is(":checked")
                    }
                }
            })
            if (formValues.cantidad && formValues.precio) { {{-- validacion de que no existan campos nulos --}}
            {{-- validacion para que no pueda comprar mas productos de los que estan solicitando --}}
                if(formValues.cantidad > articulo.pivot.cantidad)
                    return Swal("No puedes comprar una cantidad superior a la requisitada") 
  
                me.deleteListaOD(index,1); {{-- se elimina de la lista de compra pendiente --}}
  
                if(formValues.cantidad != articulo.pivot.cantidad) {{-- se agrega los productos restantes a la lista de articulos pendientes--}}
                    me.productoscopy.push({'id':articulo.id ,'producto':{'producto':articulo.producto.producto},'presentacion':{'presentacion':articulo.presentacion.presentacion},'pivot':{'status_id':1,'cantidad':(articulo.pivot.cantidad-formValues.cantidad),'observacion' : articulo.pivot.observacion}});
  
                me.respuesta = me.busqueda(1,articulo.producto.producto,articulo.presentacion.presentacion,formValues.cantidad,articulo.pivot.observacion,formValues).length ?false:true; {{-- se busca en la lista de compra existente --}}
                if(me.respuesta)
                {
                    {{-- if(formValues.cantidad != articulo.pivot.cantidad)
                        me.productoscopy.push({'id':articulo.id ,'producto':{'producto':articulo.producto.producto},'presentacion':{'presentacion':articulo.presentacion.presentacion},'pivot':{'status_id':1,'cantidad':(articulo.pivot.cantidad-formValues.cantidad),'observacion' : articulo.pivot.observacion}}); --}}
  
                    me.ordenCompra.listaOrdenCompra.push({'prodPresentacion':articulo.id,'producto':articulo.producto.producto,'presentacion':articulo.presentacion.presentacion,'cantidad':formValues.cantidad,'precioU':parseFloat(formValues.precio).toFixed(2),'precioT':parseFloat(formValues.precio*formValues.cantidad).toFixed(2), 'observacion' : articulo.pivot.observacion});
                    me.ordenCompra.subtotal = formValues.iva? parseFloat(me.ordenCompra.subtotal).toFixed(2) ? parseFloat(parseFloat(me.ordenCompra.subtotal)+parseFloat(formValues.precio*formValues.cantidad)).toFixed(2) : parseFloat(formValues.precio*formValues.cantidad).toFixed(2) : parseFloat(me.ordenCompra.subtotal).toFixed(2)
                    me.ordenCompra.totalnoIVA = !formValues.iva? parseFloat(parseFloat(me.ordenCompra.totalnoIVA)+parseFloat(formValues.precio*formValues.cantidad)).toFixed(2) : parseFloat(me.ordenCompra.totalnoIVA).toFixed(2)
                }
            }
            else
                Swal("Error campos vacios");                
        }
        else
        {    {{-- se elimina de la lista de compra --}}
            me.respuesta = me.busqueda(2,articulo.producto,articulo.presentacion,articulo.cantidad,articulo.observacion).length ?false:true;
            me.deleteListaOD(index,2);
            me.ordenCompra.subtotal =0
            me.ordenCompra.totalnoIVA = 0
            if(me.respuesta)
                me.productoscopy.push({'id':articulo.prodPresentacion,'producto':{'producto':articulo.producto},'presentacion':{'presentacion':articulo.presentacion},'pivot':{'status_id':1,'cantidad':articulo.cantidad, 'observacion' : articulo.observacion}});
        } 
      },
      deleteListaOD(index,valor){ {{-- eliminamos la informacion del arreglo dependiendo el caso --}}
        let me = this;
        if(valor == 1)
            me.productoscopy.splice(index,1)
        else
            me.ordenCompra.listaOrdenCompra.splice(index,1);
      },
      {{-- buscamos productos en las listas --}}
      busqueda(lista,producto,presentacion,cantidad,observacion,formValues){ 
        let productoFind = (lista==1)? this.ordenCompra.listaOrdenCompra : this.productoscopy;
        productoFind = productoFind.filter(p => {
            return (lista ==1)? p.producto == producto && p.presentacion == presentacion && p.observacion == observacion : p.producto.producto == producto && p.presentacion.presentacion == presentacion && p.pivot.observacion == observacion;
            })
        if (productoFind.length > 0) {
            var cantidadold = (lista ==1)?productoFind[0].cantidad :productoFind[0].pivot.cantidad;
            if(lista ==1)
            {
                productoFind[0].cantidad = parseInt(cantidadold)+parseInt(cantidad)
                productoFind[0].precioT =((parseInt(cantidadold)+parseInt(cantidad))*(productoFind[0].precioU))
                this.ordenCompra.subtotal = productoFind[0].precioT
                this.ordenCompra.totalnoIVA = formValues.iva? parseFloat(this.ordenCompra.totalnoIVA).toFixed(2) : parseFloat((formValues.precio * formValues.cantidad)+this.ordenCompra.totalnoIVA).toFixed(2)
            }
            else
                productoFind[0].pivot.cantidad = parseInt(cantidadold)+parseInt(cantidad)
                
        }
        return productoFind;
      },
      saveOrdenCompra(){
          let me = this;
          if(me.ordenCompra.listaOrdenCompra.length && me.ordenCompra.providerFound)
            {
                block();
                var url = "/recmat/ordendecompra/save";
                axios.post(url, me.ordenCompra).then(function (response) {
                    unblock();
                    if(response.data.success){
                        $('#modarordencompra,#modalRequisicion').modal('hide');
                        Swal({type: 'success', title: 'Bien', text: 'Se guardo correctamente!', footer: 'Folio: '+response.data.folio})
                        centavos = parseFloat(me.ordenCompra.total).toFixed(2).split(".");
                        {{-- generar pdf de orden de compra --}}
                        jsreport.serverUrl = '{{config('app.reporter_url')}}';
                        var request = {
                            template:{
                                shortid: "BkYjV4sBV"
                            },
                            header: {
                                Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
                            },
                            data:{
                                ordenCompra:{
                                    folio: response.data.folio,
                                    productos:me.ordenCompra.listaOrdenCompra,
                                    subtotal:parseFloat(me.ordenCompra.subtotal).toFixed(2),
                                    totalnoIVA:parseFloat(me.ordenCompra.totalnoIVA).toFixed(2),
                                    totaIVA:parseFloat(me.ordenCompra.subtotal).toFixed(2),
                                    total:parseFloat(me.ordenCompra.total).toFixed(2),
                                    iva:parseFloat(me.ordenCompra.subtotal*(me.ordenCompra.iva/100)).toFixed(2),
                                    autoriza:me.autoriza.nombre + ' '+ me.autoriza.primer_apellido + ' '+me.autoriza.segundo_apellido,
                                    area:me.area.nombre,
                                    observacion:me.ordenCompra.observacionOD,
                                    importeLetra:numeroALetras(parseFloat(me.ordenCompra.total).toFixed(2), {plural: "PESOS", singular: "PESO", centPlural: "CENTAVOS", centSingular: "CENTAVO"}),
                                    centavos:centavos[1]+'/100 M.N.'
                                },
                                proveedor:{
                                    nombre:me.ordenCompra.providerFound.nombre,
                                    banco:me.ordenCompra.providerFound.banco,
                                    domicilio:me.ordenCompra.providerFound.calle+' '+me.ordenCompra.providerFound.numero+' '+me.ordenCompra.providerFound.colonia,
                                    clabe:me.ordenCompra.providerFound.clabe
                                }
                            }
                        };
                        jsreport.render('_blank', request);
                        {{-- fin pdf de orden de compra --}}
                        me.ordenCompra.subtotal='',
                        me.ordenCompra.totalnoIVA=0,
                        me.ordenCompra.iva=16,
                        me.ordenCompra.total='',
                        me.ordenCompra.requisicion_id='',
                        me.ordenCompra.providerFound=''
                    }
                    else
                        Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
                    
                }).catch(function (error){
                    unblock();
                    Swal({type: 'error',title: 'Oops...',text: error,footer: 'Ocurrio un error'})
                })
            }
          else
            swal("Existen campos vacios") 
      },
      findProvider(search, loading) {
        loading(true);
        this.search(loading, search, this);
        },
        search: _.debounce((loading, search, vm) => {
            fetch(`/recmat/proveedores/${localStorage.getItem('recm_token')}/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.providers = json;
                        });
                    loading(false);
                });
        }, 350),
        print(datos){
            block();
            let me = this;            
            me.timeline = false;
            var url = "/recmat/requisicion/"+datos.id;
            axios.get(url).then(function (response) {
                fecha_plazo = datos.fecha_captura.split("-");
                jsreport.serverUrl = '{{config('app.reporter_url')}}';
                var request = {
                    template:{
                        shortid: "rkLtzNjS4"
                    },
                    header: {
                        Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
                    },
                    data:{
                        requisicion: {
                            folio: datos.folio,
                            area:response.data.areaResp.area.nombre,
                            justificacion:datos.justificacion,
                            productos:response.data.requisicion.productos,
                            solicitante: response.data.autoriza.nombre + ' '+ response.data.autoriza.primer_apellido + ' '+response.data.autoriza.segundo_apellido,
                            cargoSolicitante:response.data.nivel.nivel.nivel,
                            autoriza:response.data.responsable[0].responsable_nombre,
                            cargoAutoriza:response.data.responsable[0].cargo,
                            importeLetra:numeroALetras(parseFloat(response.data.sumTotal).toFixed(2), {plural: "PESOS", singular: "PESO", centPlural: "CENTAVOS", centSingular: "CENTAVO"}),
                            total:parseFloat(response.data.sumTotal).toFixed(2) ,
                            direccionR:response.data.direccion,
                            clavePrep:response.data.requisicion.partida.partida_especifica_c + ' - '+ response.data.requisicion.partida.partida_especifica,
                            anio:fecha_plazo[0],
                            mes:fecha_plazo[1],
                            dia:fecha_plazo[2]
                        },
                    }
                };
                unblock();
                jsreport.render('_blank', request);
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        findProduct(search, loading) {
            loading(true);
            this.searchP(loading, search, this);
        },
        searchP: _.debounce((loading, search, vm) => {
            fetch(`/recmat/productos/partidaProd/${localStorage.getItem('recm_token')}/${escape(search)}/${vm.requisicion.partida.id}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.products = json.map(item => {
                                item.productoNombre = item.producto.producto + ' - ' + item.presentacion.presentacion;
                                return item;
                            });
                        });
                    loading(false);
                });
        }, 350),
        addProducto(){
            let me = this;
            if(me.producto.nombre && me.producto.precio && me.producto.cantidad && me.producto.observacion)
            {  
                let productoFind = this.productos;
                productoFind = productoFind.filter(p => {
                    return p.producto.producto == me.producto.nombre.producto.producto && p.presentacion.presentacion == me.producto.nombre.presentacion.presentacion && p.pivot.observacion == me.producto.observacion;
                    })
                if (productoFind.length > 0) {
                    var preciold = productoFind[0].precio
                    productoFind[0].cantidadT = parseInt(productoFind[0].cantidadT)  + parseInt(me.producto.cantidad)
                    productoFind[0].precioT = preciold * productoFind[0].cantidadT
                }
                else
                    me.productos.push({'presentacion':{'presentacion':me.producto.nombre.presentacion.presentacion},'producto':{'producto':me.producto.nombre.producto.producto},'pivot':{'status_id':1, 'observacion':me.producto.observacion, 'cantidad':me.producto.cantidad, 'presentacion_producto_id':me.producto.nombre.id, 'precio':me.producto.precio}, 'precioT': (me.producto.precio * me.producto.cantidad), 'cantidadT':me.producto.cantidad})
  
                me.producto.nombre = '';
                me.producto.cantidad = '';
                me.producto.precio = '';
                me.producto.observacion = '';
            }
            else
                Swal({position: 'top-end',type: 'error', title: 'Existen campos vacios', showConfirmButton: false, timer: 1500});
        },
        searchRequisicion(){
            let me = this;
            if(me.busquedaFolio == '')
                return swal({type: 'error', title:' Error', text: 'Campo de busqueda vacio'})
            block();
            {{-- var url = "/recmat/requisicion/searFolio/"+me.busquedaFolio; --}}
            var url = `/recmat/requisicion/${localStorage.getItem('recm_token')}/${me.busquedaFolio}`;
            axios.get(url).then(function (response) {                
                unblock();
                if(response.data.length)
                {
                me.listaRequisiciones = response.data
                me.pagination = {};
                }
                else
                swal({type: 'info', title:' Sin resultados', text: 'Sin resultados'})
                
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        }
        
    },
    watch:{
        'ordenCompra.subtotal':function(){
            this.ordenCompra.total = this.ordenCompra.subtotal? parseFloat(parseFloat(this.ordenCompra.totalnoIVA) + parseFloat(this.ordenCompra.subtotal)+ parseFloat(this.ordenCompra.subtotal*(this.ordenCompra.iva/100))).toFixed(2):0
        },
        'ordenCompra.totalnoIVA':function(){
            this.ordenCompra.total = this.ordenCompra.totalnoIVA? parseFloat(parseFloat(this.ordenCompra.total)+ parseFloat(this.ordenCompra.totalnoIVA)).toFixed(2) : parseFloat(this.ordenCompra.totalnoIVA).toFixed(2)
        },
        'producto.nombre':function(){
            let me = this;
            me.producto.precio = me.producto.nombre.precio;
        },
    }
  });