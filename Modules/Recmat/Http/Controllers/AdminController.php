<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;

use Barryvdh\DomPDF\Facade as PDF;

use App\Models\Usuario;
use App\Models\Area;
use App\Models\Empleado;
use App\Models\AreasResponsable;
use App\Models\Persona;
use App\Models\RecmCatPartida;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('recmat::admin.index', ['usuario' => auth()->user() ? auth()->user()->getDatos(9) : null]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('recmat::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('recmat::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('recmat::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    public function userDatos ()
    {
        $empleado = Empleado::where('persona_id',auth()->user()->persona_id)->with('area','nivel')->firstOrFail();
        $autoriza = AreasResponsable::where('area_id',$empleado->area->id)->with('empleado','area')->firstOrFail();
                
        return new JsonResponse([
            'area' => DB::select('call getAreas(?)',[$autoriza->area->id])
        ]);
    }
    
    public function buscarPartida($token, $nombre)
    {
        return new JsonResponse(RecmCatPartida::where('partida_especifica', 'like', "%$nombre%")->orWhere('partida_especifica_c', $nombre)->take(10)->get());
    }

    public function areaFind($id)
    {
        return new JsonResponse(DB::select('call getPadre(?)',[$id]));
    }
    
}