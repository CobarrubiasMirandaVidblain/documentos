<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use App\Models\RecmOrdeneCompra;
use App\Models\RecmHistorialOrdenCompra;
use App\Models\RecmProductoRequisicion;
use App\Models\RecmOrdenCompraProductos;
use App\Models\Empleado;
use App\Models\Persona;
use App\Models\Usuario;
use App\Models\RecmRequisicion;
use App\Models\AreasResponsable;
use App\Models\RecmPresentacionProducto;

class OrdendecompraController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('recmat', ['only' => ['show', 'buscarPorFolio']]);
        $this->middleware('rolModuleV2:recmat,ADMINISTRADOR,FINANCIERO,USUARIORM,RMATERIALES,MANTENIMIENTO', ['only' => ['show', 'buscarPorFolio']]);
    }
    /**
     * Se muestra la lista de ordenes de compra, si la requisicon ya fue finalizada  FINANCIERO 
     */
    public function index()
    {
        return new JsonResponse(RecmOrdeneCompra::with('estatus','requisicion','productos','proveedor')->whereHas('requisicion', function ($query) {
            $query->where('estatus_id', 7);
        })->orderBy('id', 'DESC')->paginate(20));       
    }   

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('recmat::create');
    }

    /**
     * Se guarda la orden de compra
     */
    public function store(Request $request)
    {
        // return $request;
        try {
            DB::beginTransaction();
            // se actualiza el estatus de la requisicion
            $requisicion = RecmRequisicion::find($request->requisicion_id);
            $requisicion->fecha_captura = $request->fechaRequisicion;
            $requisicion->estatus_id =6; // pasa la requisicion a compra
            $requisicion->save();
            // se guarda la orden de compra
            $ordenCompra = new RecmOrdeneCompra();
            
            $ordenCompra->subtotal    = (float)$request->subtotalIva;
            $ordenCompra->iva         = $request->iva;
            $ordenCompra->total       = (float)$request->total;
            $ordenCompra->usuario_id  = auth()->user()->id;
            $ordenCompra->requisicion_id  = $request->requisicion_id;
            $ordenCompra->proveedor_id    = $request->providerFound['id'];        
            $ordenCompra->status_id    = 1; // estatus nuevo       
            $ordenCompra->observacion    = $request->observacionOD;   
            $ordenCompra->fecha_captura    = $request->fechaOD;   
            $ordenCompra->subtotal_no_iva    = $request->subtotalNoIVA;   
            $ordenCompra->entrega    = $request->entregar;  // en donde se tiene que entregar  
            $ordenCompra->save();

            $ordenCompra->folio= "RM-OD-".$ordenCompra->id."-".date("Y");
            $ordenCompra->save();
            // se guarda el cambio de estatus de la orden de compra
            $historial = new RecmHistorialOrdenCompra();
            $historial->orden_compra_id = $ordenCompra->id;
            $historial->status_id = 1; // estatus nuevo
            $historial->usuario_id = auth()->user()->id;
            $historial->save();
            // se guarda los productos de la orden de compra
            foreach ($request->listaProductos as $value) {      
                $productoCompra = new RecmOrdenCompraProductos();
                $productoCompra->presentacion_producto_id =$value['presentacion_producto_id']; // cambiar nombre de la columna se guarda el id producto_presentacion
                $productoCompra->orden_compra_id =$ordenCompra->id;
                $productoCompra->usuario_id =auth()->user()->id;
                $productoCompra->precio_unitario_real = ((float)$value['precioU']);
                $productoCompra->subtotal = ((float)$value['subtotal']); 
                $productoCompra->cantidad = (int)$value['cantidad'];
                $productoCompra->save();
                // actualizar precio del producto en el catalogo de presentacion producto
                $updatePrecio = RecmPresentacionProducto::find($value['presentacion_producto_id']);
                $updatePrecio->precio = ((float)$value['precioU']);
                $updatePrecio->save();
            };

            DB::commit();
            // se guarda en bitacora
            auth()->user()->bitacora(request(), [
                'tabla' => 'recm_ordenescompra',
                'registro' => $ordenCompra->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$ordenCompra->folio], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Se muestra el detalle de la orden de compra para visualizar e imprimir el reporte
     * @return Response
     */
    public function show($token,$idOD)
    {
        $requisicion = RecmOrdeneCompra::where('id',$idOD)->with('productosList', 'proveedor')->firstOrFail();
				$areaResponsable = RecmRequisicion::where('id',$requisicion->requisicion_id)->firstOrFail();
        $responsableArea = $areaResponsable->areas_responsable_id != null ? AreasResponsable::withTrashed()->where('id',$areaResponsable->areas_responsable_id)->with('empleado','area')->first() : AreasResponsable::where('area_id',$areaResponsable->area_id)->with('empleado','area')->firstOrFail();
        $datosResponsable = Persona::where('id',$responsableArea->empleado->persona_id)->select('nombre','primer_apellido','segundo_apellido')->firstOrFail();

        $prodRequisicion = RecmProductoRequisicion::where('requisicion_id',$requisicion->requisicion_id)->where('status_id','>',4)->get();

        return new JsonResponse([
            'ordencompra' =>  $requisicion,
            'areaResponsable' =>  $areaResponsable,
            'responsableArea' => $responsableArea,
            'datosResponsable' => $datosResponsable,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('recmat::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    // funcion para actualizar el estatus de la requisicion
    public function changestatus(Request $request)
    {
        // return $request;
        try {
            DB::beginTransaction();
            $ordenCompra = RecmOrdeneCompra::find($request->id);

            if($request->status == 0 ) // cuando cencela oh se elimina en recursos materiales
                $ordenCompra->delete();
            else
            {
                $ordenCompra->status_id = $request->status;
                $ordenCompra->folio_interno = $request->status == 8 ? $request->dato:null;
                $ordenCompra->motivo = $request->status == 8 ? null :$request->dato;
                $ordenCompra->save(); 
            } 
            
            if($request->status == 3) // cuando cancela la od en finanzas para que el usuario de materiales lo pueda eliminar y generar otra
            {
                $requisicion = RecmRequisicion::find($ordenCompra->requisicion_id);
                $requisicion->estatus_id = 6;
                $requisicion->save();
            }
                
            // se guarda el cambio de estatus en el historico
            $historial = new RecmHistorialOrdenCompra();
            $historial->orden_compra_id = $ordenCompra->id;
            $historial->status_id = $request->status == 0 ? 8:$request->status;
            $historial->usuario_id = auth()->user()->id;
            $historial->save();

            DB::commit();
            // se guarda en bitacora los movientos
            auth()->user()->bitacora(request(), [
                'tabla' => 'RecmRequisicion&RecmOrdeneCompra',
                'registro' => $ordenCompra->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$ordenCompra->folio], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }

    }
    // busqueda de orden de compra, donde puede ser por folio de lo orden de compra oh de la requisicion, recursos materiales
    public function buscarOrden($token, $folio)
    {
        return new JsonResponse( RecmOrdeneCompra::with('estatus','requisicion','productos','proveedor')->whereHas('requisicion', function ($query) use ($folio) {
            $query->where('folio', 'like', "%$folio%");
        })->Orwhere('folio', 'like', "%$folio%")->take(15)->get());        
    }
    // busqueda de orden de compra, donde puede ser por folio de lo orden de compra oh por el proveedor, financieros
    public function searchProveedor($token, $folio)
    {
        return new JsonResponse( RecmOrdeneCompra::with('estatus','requisicion','productos','proveedor')->where('status_id','>=', 7)->whereHas('proveedor', function ($query) use ($folio) {
            $query->where('nombre', 'like', "%$folio%");
        })->Orwhere('folio', 'like', "%$folio%")->paginate(40));        
    }
    // listado para los usuarios de recursos materiales y para los de mantenimiento
    public function listall()
    {
      if(auth()->user()->hasRolesStrModulo(['MANTENIMIENTO'], 'recmat'))
            return new JsonResponse(RecmOrdeneCompra::where('status_id','<',19)->with('estatus','requisicion','productos','proveedor')->orderBy('id', 'DESC')->where('usuario_id',156)->paginate(20));
        else
            return new JsonResponse(RecmOrdeneCompra::where('status_id','<',19)->with('estatus','requisicion','productos','proveedor')->orderBy('id', 'DESC')->where('usuario_id','!=',156)->paginate(20));
    }


}
