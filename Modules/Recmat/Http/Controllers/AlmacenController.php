<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Barryvdh\DomPDF\Facade as PDF;
use Spipu\Html2Pdf\Html2Pdf;
use App\Models\Modulo;
use App\Models\RecmEntradas;
use App\Models\RecmCatProveedor;
use App\Models\RecmEntradasProductos;
use App\Models\RecmInventario;
use App\Models\RecmRequisicion;
use App\Models\RecmSalidas;
use App\Models\RecmProductosSalidas;
use App\Models\RecmProductoRequisicion;
use App\Models\RecmHistorialRequisicion;
use App\Models\AreasResponsable;
use Illuminate\Support\Facades\DB;
use App\Models\RecmOrdeneCompra;

class AlmacenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR', ['only' => 'index']);
        $this->middleware('recmat', ['only' => ['entradas', 'detalleEntrada', 'guardarEntrada', 'productosDisponibles', 'surtirRequisicion', 'pasarCompra']]);
        $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR', ['only' => ['entradas', 'detalleEntrada', 'guardarEntrada', 'productosDisponibles', 'surtirRequisicion', 'pasarCompra']]);
    }

    public function index()
    {
        $modulo = Modulo::where('prefix', 'like', 'recmat')->first();
        return view('recmat::almacen.index', ['usuario' => auth()->user() ? auth()->user()->getDatos($modulo->id) : null]);
    }

    public function entradas()
    {
        return new JsonResponse(RecmEntradas::orderBy('id', 'DESC')->with('productos','estatus','requisicion')->paginate(20));
    }

    public function detalleEntrada($entrada)
    {
        return new JsonResponse(RecmEntradasProductos::where('entrada_id', $entrada)->get());
    }

    public function guardarEntrada(Request $request)
    {
        try{            
            DB::beginTransaction();
            $cantidad = RecmEntradas::count();
            $folio = (int)$cantidad + 1;
            $entrada = RecmEntradas::create([
                'nombre' => $request->descripcion,
                'fecha_hora' => $request->fecha,
                'orden_compra_id' => $request->orden["id"],
                'requisicion_id' => $request->orden['requisicion']['id'],
                'usuario_id' => $request->usuario_id,
                'estatus_id' => 4,
                'folio' => "EA-$folio-".date("Y"),
            ]);
            $productos = [];
            foreach($request->productos as $producto){
                $p = RecmEntradasProductos::create([
                    'cantidad' => $producto["cantidad"],
                    'entrada_id' => $entrada->id,
                    'presentacion_producto_id' => $producto["presentacion_producto_id"]
                ]);
                $inventario = RecmInventario::firstOrCreate(['presentacion_producto_id' => $producto["presentacion_producto_id"]], ['cantidad' => 0]);
                $inventario->cantidad = $inventario->cantidad + $producto["cantidad"];
                $inventario->save();
                array_push($productos, $p);
            }
            $odcompra = RecmOrdeneCompra::find($request->orden["id"]);
            $odcompra->status_id = 4;
            $odcompra->save();

            DB::commit();
            $entrada->productos = $productos;
            return new JsonResponse($entrada);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse($e->getError(), 422);
        }
    }

    //Buscar los productos de la requisicion en los productos del inventario
    public function productosDisponibles($requisicion_id)
    {
        $requisicion = RecmRequisicion::with('productos')->findOrFail($requisicion_id);
        $ids = [];
        foreach($requisicion->productos as $p){
            if($p->status_id != 6){
                array_push($ids, $p->id);
            }
        }
        $enAlmacen = RecmInventario::where('cantidad', '>', 0)->whereIn('presentacion_producto_id', $ids)->with('productos')->get();
        foreach($enAlmacen as $p){
            foreach($requisicion->productos as $p1){
                if($p->presentacion_producto_id === $p1->id){
                    $p->solicita = $p1->pivot->cantidad;
                }
            }
        }
        return $enAlmacen;
    }

    //Crear la salida de la requisicion con los productos disponibles en el inventario
    public function surtirRequisicion(Request $request, $requisicion_id)
    {   
        // return $request->id;
            
        $requisicion = RecmRequisicion::whereIn('estatus_id', [6, 5, 7])->with('productos')->findOrFail($requisicion_id); // solo lo encuentra si ya finazlizaron la requisicion 
        // $requisicion = RecmRequisicion::whereIn('estatus_id', [4, 5, 7])->with('productos')->findOrFail($requisicion_id); // solo lo encuentra su ya finazlizaron la requisicion 
        if($requisicion == null){
            return new JsonResponse([], 404);
        }
        try{
            DB::beginTransaction();
            // $salida = RecmSalidas::create($request->all());
            $cantidad = RecmSalidas::count();
            $folio = (int)$cantidad + 1;
            $recibe = AreasResponsable::with('empleado.persona')->where('area_id',$request->requisicion['area_id'])->firstOrFail();
            $salida = RecmSalidas::create([
                'fecha_hora' => date("Y-m-d"),
                'requisicion_id' => $request->requisicion['id'],
                'recibe_id' => $recibe->empleado->persona->id,
                'usuario_id' => auth()->user()->id,
                'estatus_id' => 7, // entregado
                'folio' => "SA-$folio-".date("Y"),
            ]);
            $odcompra = RecmOrdeneCompra::find($request->id);
            $odcompra->status_id = 7;
            $odcompra->save();
            $productos = [];
            //Actualizar el inventario con los productos otorgados
            foreach($request->productos as $surtido){
                $productoInventario = RecmInventario::where('presentacion_producto_id', $surtido["presentacion_producto_id"])->first();
                $productoInventario->cantidad -= $surtido["cantidad"];
                $productoInventario->save();
                $productoInventario = null;
                array_push($productos, RecmProductosSalidas::create([
                    'salida_id' => $salida->id,
                    'presentacion_producto_id' => $surtido["presentacion_producto_id"],
                    'cantidad' => $surtido["cantidad"]
                ]));
            }
            
            RecmHistorialRequisicion::create(['requisicion_id' => $requisicion->id, 'status_id' => 7, 'usuario_id' => $request->usuario_id]);
            $requisicion->save();
            DB::commit();
            $salida->productos = RecmProductosSalidas::where('salida_id', $salida->id)->get();
            return new JsonResponse(['success' => true,'salida'=>$salida, 'recibe'=> AreasResponsable::with('empleado.persona','empleado.area')->where('area_id',$request->requisicion['area_id'])->firstOrFail()]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse($e->getError(), 422);
        }
    }

    //Si no hay productos disponibles en inventario, pasa directo a compra
    public function pasarCompra(Request $request, $requisicion_id)
    {
        $requisicion = RecmRequisicion::findOrFail($requisicion_id);
        $requisicion->estatus_id = 5;
        $requisicion->save();
        return new JsonResponse($requisicion);
    }

    public function reciboEntrada($id)
    {
        $pdf = PDF::loadView('recmat::pdf.entry', ['id' => $id, 'productos' => [
            ['id' => 1, 'producto' => 'Lapiz Caja 10pz', 'cantidad' => 10],
            ['id' => 2, 'producto' => 'Lapicero Negro Caja 10pz', 'cantidad' => 10],
            ['id' => 3, 'producto' => 'Lapicero Azul Caja 10pz', 'cantidad' => 10],
            ['id' => 4, 'producto' => 'Goma Caja 10pz', 'cantidad' => 10],
            ['id' => 5, 'producto' => 'Hojas blancas Caja 20pz', 'cantidad' => 10],
            ['id' => 6, 'producto' => 'Toner HP', 'cantidad' => 10],
            ['id' => 7, 'producto' => 'Mouse Optico HP', 'cantidad' => 1]]
        ]);
        return $pdf->stream('entrada.pdf');
        // ob_start();
        // require_once('entry.php');
        // $layout = ob_get_clean();
        // $html2pdf = new Html2Pdf();
        // $html2pdf->writeHTML($layout);
        // return $html2pdf->output();
    }

    public function buscarEntrada($token, $folio)
    {
        return new JsonResponse(RecmEntradas::where('folio', 'like', "%$folio%")->with(['proveedor', 'productos', 'usuario.persona'])->take(10)->get());
    }

    public function buscarSalida($token, $folio)
    {
        return new JsonResponse(RecmSalidas::where('folio', 'like', "%$folio%")->with('productos', 'recibe')->take(10)->get());
    }
    
    public function changeStatus($id,$tipo)
    {
        try {
            DB::beginTransaction();
           
            $entrada = $tipo == 1 ? RecmEntradas::where('id',$id)->with('productos')->first() : RecmSalidas::where('id',$id)->with('productos')->first();
            $entrada->estatus_id = 19;
            $entrada->save(); 
           
            foreach($entrada->productos as $producto){
                $inventario = RecmInventario::where('presentacion_producto_id', $producto['pivot']['presentacion_producto_id'])->first();
                $inventario->cantidad = $tipo == 1 ? ((int)$inventario->cantidad - (int)$producto['pivot']['cantidad']) : ((int)$inventario->cantidad + (int)$producto['pivot']['cantidad']);
                $inventario->save();
            }

            if($tipo == 2)
            {
                $requisicion = RecmRequisicion::find($entrada->requisicion_id);
                $requisicion->estatus_id = 5;
                $requisicion->save();

                DB::table('recm_producto_requisicions')->where('requisicion_id',$requisicion->id)->update(['status_id'=>5]);
            }

            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'RecmEntradas',
                'registro' => $id . '',
                'campos' => json_encode($id) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$entrada->folio], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }
    public function searchEntrada($folio,$tipo)
    {
        if($tipo == 1)
        {
            return new JsonResponse( RecmEntradas::with('productos','estatus','requisicion')->whereHas('requisicion', function ($query) use ($folio) {
                $query->where('folio', 'like', "%$folio%");
            })->Orwhere('folio', 'like', "%$folio%")->take(25)->get());
        }
        else
        {
            return new JsonResponse( RecmSalidas::with('productos','estatus','requisicion')->whereHas('requisicion', function ($query) use ($folio) {
                $query->where('folio', 'like', "%$folio%");
            })->Orwhere('folio', 'like', "%$folio%")->take(25)->get());
        }
        
    }

    public function salidas()
    {
        return new JsonResponse(RecmSalidas::orderBy('id', 'DESC')->with('productos','estatus','requisicion.usuario.persona','requisicion.area')->paginate(20));
    }
}