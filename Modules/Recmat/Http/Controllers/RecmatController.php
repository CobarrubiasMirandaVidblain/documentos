<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Modulo;
use \Firebase\JWT\JWT;
use Carbon\Carbon;

class RecmatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $usuario = auth()->user();
        // dd($usuario->rolesrecmat);
        $token = [
            'usuario_id' => $usuario->id,
            'modulo_id' => Modulo::where('nombre', 'like', 'RECMAT')->first()->id,
            'rol_id' => $usuario->rolesrecmat->first()->pivot->rol_id
        ];
        $encoded = JWT::encode($token, config('app.jwt_recmat_token'), 'HS256');
        $usuario->token = $encoded;
        $usuario->vida_token = Carbon::now()->addMinutes(40); 
        $usuario->save();
        return view('recmat::index', ['token' => $encoded]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('recmat::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('recmat::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('recmat::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
