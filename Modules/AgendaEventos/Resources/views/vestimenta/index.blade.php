@extends('agendaeventos::layouts.master')

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">    
@endpush

@section('content-title', 'Agenda de giras y eventos')
@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li class="active">Vestimenta</li>
@endsection

@section('content')
    <section class="content">
        <div class="box box-primary shadow">
            <div class="box-header with-border">
                <h3 class="box-title">Catálogo de vestimenta</h3>
            </div>
            <div class="box-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="buscar" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                    </span>
                </div>
                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'vestimenta', 'name' => 'vestimenta', 'style' => 'width: 100%']) !!}
            </div>
        </div>        
    </section>    

    <div class="modal fade" id="modal-vestimenta">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-vestimenta-title"></h4>
                </div>
                <div class="modal-body">
                    <form data-toggle="validator" role="form" id="form_vestimenta">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="nombre">Nombre* :</label>
                                    <input type="text" id="nombre" name="nombre" placeholder="NOMBRE" class="form-control">
                                </div>
                            </div>                    
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="codigo">Código* :</label>
                                    <input type="text" id="codigo" name="codigo" placeholder="CODIGO" class="form-control">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                    <button type="button" class="btn btn-success" onclick="Vestimenta.guardar()"><i class="fa fa-database"></i> Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
{!! $dataTable->scripts() !!} 
@include('agendaeventos::eventos.js.app')
<script type="text/javascript">
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    
    $(".modal").modal({
        keyboard: false,
        backdrop: "static"
    }).modal('hide');
    
    $(document).ready(() => {
        Vestimenta.getVestimenta();
    });

    var Vestimenta = (() => {

        var vestimenta;
        var vestimenta_id;

        var getVestimenta = () => {
            app.to_upper_case();            
            vestimenta = $('#vestimenta').DataTable();
            $('#buscar').keypress(function(e) {
			    if(e.which === 13) {
				    tipos.search($('#buscar').val()).draw();
			    }
		    });
		    $('#btn_buscar').on('click', function() {
			    tipos.search($('#buscar').val()).draw();
            });
        }

        var guardar = () => {
            if($('#form_vestimenta').valid()) {
                var url = vestimenta_id ? ('/agenda/vestimenta/' + vestimenta_id) : ('/agenda/vestimenta');
                var metodo = vestimenta_id ? 'PUT' : 'POST';                

                app.ajaxHelper( url, metodo, $('#form_vestimenta').serialize() ).done( (response) => {
                    if(!response.success) {
                        swal(
                            '¡Error!',
                            response.message,
                            'error'
                        );
                    } else {                        
                        vestimenta.ajax.reload(null, false);
                        $('#modal-vestimenta').modal('hide');
                        unblock();
                        swal(
                            '¡Correcto!',
                            '',
                            'success'
                        );
                    }
                });
            }
		}

        var crear_o_editar = (vestimenta, nombre, codigo) => {
            vestimenta_id = vestimenta;            
            $('#modal-vestimenta-title').text(nombre || 'Registrar vestimenta');
            $("#nombre").val(nombre);
            $("#codigo").val(codigo);
            inicializar();
            $('#modal-vestimenta').modal('show');
        }

        var inicializar = () => {
            $('#modal-vestimenta').on('hidden.bs.modal', (e) => {
                vestimenta_id = undefined;
            });            

            $('#form_vestimenta').validate({
                rules: {
                    nombre: {
                        required: true,
                        maxlength: 30
                    },
                    codigo: {
                        required: true,
                        maxlength: 10
                    }
                }
            });
            app.to_upper_case();            
        }

        var eliminar = (id, nombre, codigo) => {
            swal({
                title: '¿Desea eliminar la vestimenta?',
                html: nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar',
                cancelButtonText: 'No, regresar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    var url = '/agenda/vestimenta/' + id;
                    app.ajaxHelper( url, 'DELETE').done( (response) => {
                        unblock();
                        if(!response.success){
                            swal(
                                '¡Error!',
                                response.message,
                                'error'
                            );
                        } else {
                            vestimenta.ajax.reload(null, false);
                            swal(
                                '¡Correcto!',
                                'Vestimenta eliminada',
                                'success'
                            );
                        }
                    });
                }
            });             
        }

        return {
            getVestimenta,
            crear_o_editar,
            guardar,
            eliminar
        }
    })();
</script>
@endpush