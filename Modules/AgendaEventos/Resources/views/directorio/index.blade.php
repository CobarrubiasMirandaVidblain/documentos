@extends('agendaeventos::layouts.app')

@section('content-subtitle')
DIRECTORIO
@endsection

@push('head')
@endpush

@section('content')

@auth
<div class="box box-primary shadow" id="content">

	<div class="box-header with-border">
		<h3 class="box-title"></h3>
	</div>
	
	<div class="box-body">

		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search_input" name="search_input" class="form-control">

			<span class="input-group-btn">
				<button type="submit" class="btn btn-default button-dt tool" id="search_button" name="search_button">BUSCAR</button>
			</span>
		</div>
		
		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
		
		</div>
</div>
@endauth

<div class="modal fade" id="modal_persona">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" onclick="cerrar_modal();" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				
				<h4 class="modal-title" id="modal_title_persona">ACTUALIZAR CORREO:</h4>

			</div>
			
			<div class="modal-body" id="modal_body_persona">
				<form id="form_email" name="form_email" onkeypress="return event.keyCode != 13;">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="email">Correo Electrónico:</label>
							<input type="text" class="form-control" id="email" name="email">
						</div>
					</div>
				</form>
			</div>
			
			<div class="modal-footer" id="modal_footer_persona">
				<button type="button" class="btn btn-success" onclick="create_edit()">
					<i class="fa fa-database"></i> Guardar
				</button>
				
				<button type="button" class="btn btn-danger" onclick="cerrar_modal();">
					<i class="fa fa-remove"></i> Cerrar
				</button>
			</div>

		</div>
	</div>
</div>
@stop

@push('body')

@auth
{!! $dataTable->scripts() !!}
@endauth

<script>
function block() {
	$('#app').block({
		css: {
			border: 'none',
			padding: '5px',
			backgroundColor: 'none',
			'-webkit-border-radius': '10px',
			'-moz-border-radius': '10px',
			opacity: .8,
			color: '#fff',
		},
		message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>'
	});
}

function unblock() {
	$('#app').unblock();
}
</script>

<script>
$('#search_button').on('click', function() {
	window.LaravelDataTables['personas'].search($('#search_input').val()).draw();
});

$('#search_input').keypress(function(e) {
	if(e.which === 13) {
		window.LaravelDataTables['personas'].search($('#search_input').val()).draw();
	}
});

function to_upper_case() {
	$(':input').on('propertychange input', function(e) {
		var ss = e.target.selectionStart;
		var se = e.target.selectionEnd;
		e.target.value = e.target.value.toUpperCase();
		e.target.selectionStart = ss;
		e.target.selectionEnd = se;
	});
};

to_upper_case();

var validator = $('#form_email').validate({
	rules: {
		email: {
			required: true,
			email: true
		}
	}
});

var persona_id, persona_email;

function abrir_modal(id, email) {
	$('#email').val(email);
	$('#modal_persona').modal('show');
	persona_id = id;
	persona_email = email;
}

function cerrar_modal() {
	$('#modal_persona').modal('hide');
	validator.resetForm();
}

function create_edit() {
	if($('#email').valid()) {
		axios.put('/agenda/api/v1/personas/' + persona_id, {
			email: $('#email').val()
		})
		.then(function(response) {
			//console.log(response.data);
			if("success" in response.data) {
				window.LaravelDataTables['personas'].ajax.reload(null, false);
				
				$('#modal_persona').modal('hide');
			}
		})
		.catch(function(error) {
			console.log(error);
			if("success" in response.data) {}
		})
		.then(function() {
		});
	}
}
</script>
@endpush