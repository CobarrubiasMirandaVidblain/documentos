<script>
  	var Actividad = (() => {

		var id = undefined;
		var actividad_responsable_id = undefined;
		var actividades = undefined;

		var inicializar = () => {

			$('#form_actividad').validate({
                rules: {
                    actividad: {
                        required: true
                    },
                    horax: {
                        required: true
                    },
                    tiempo: {
                        required: true,
						pattern_integer: ''
                    }
				}
			});

			$('#form_responsable').validate({
                rules: {
                    nombre_responsable: {
                        required: true
                    },
                    cargo_responsable: {
                        required: false
                    }
				}
			});
		}

		var guardar = () => {
			if($('#form_actividad').valid()) {
				block();
				var url = id ? '{{ route("agenda.actividades.update", ":actividad_id") }}' : '{{ route("agenda.actividades.store")}}'; 
				url = id ? url.replace(':actividad_id', id) : url;
				var type = id ? 'PUT' : 'POST';
				var data = $('#form_actividad').serialize() + '&fichainfo_id=' + Ficha.getId() + '&hora=' + convertTime12to24($('#horax').val());
				var title = id ? 'Actividad actualizada' : 'Actividad registrada';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							id = undefined;
							getActividades(Ficha.getId());
							$('#btnAgregarActividad').text("Agregar");
                        	$('#btnAgregarActividad').css('background-color','##00c0ef');
                        	$('#btnAgregarActividad').css('border-color', '##00c0ef');
							$('#actividad, #horax, #tiempo').val('');
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}else {
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}

		var getActividades = (fichainfo_id) => {
            if ($.fn.DataTable.isDataTable( '#actividades' ) ) {
                actividades.ajax.url( '/agenda/actividades?fichainfo_id=' + fichainfo_id ).load();
		    }else{
                actividades = $("#actividades").DataTable({
                    ajax: {
                        url: '/agenda/actividades?fichainfo_id=' + fichainfo_id,
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "hora" },
                        { "data": "actividad" },
						{ "data": "duracion" },
						{
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Actividad.getResponsables(" + data.id + ", \"" + data.actividad + "\")' class='btn btn-xs btn-info'>Consultar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Actividad.editar(" + data.id + ", \"" + data.actividad + "\", \"" + data.horax + "\", \"" + data.duracion + "\")' class='btn btn-xs btn-warning'>Editar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Actividad.eliminar(' + data.id + ', \'' + data.actividad + '\')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [0,2,3,4,5] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
            }
        }

		var editar = (actividad_id, actividad, horax, duracion) => {
            id = actividad_id;            
            $('#actividad').val(actividad);
			$('#horax').val(horax);
			$('#tiempo').val(duracion);
            $('#btnAgregarActividad').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');
        }

		var eliminar = (actividad_id, actividad) => {			
			swal({
				title: '¿Desea eliminar la actividad?',
				html: actividad,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar',
				cancelButtonText: 'No, regresar',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = '/agenda/actividades/' + actividad_id;
					app.ajaxHelper( url, 'DELETE').done( (response) => {
						unblock();
						if(!response.success){
							swal(
								'¡Error!',
								response.message,
								'error'
							);
						}else{							
							actividades.ajax.reload(null, false);
							$('#btnAgregarActividad').text("Agregar");
                        	$('#btnAgregarActividad').css('background-color','##00c0ef');
                        	$('#btnAgregarActividad').css('border-color', '##00c0ef');
							$('#actividad, #horax, #tiempo').val('');
							id = undefined;
							swal({
								title: response.message,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
					});
				}
			});
        }

		var getResponsables = (actividad_id, actividad, actividad_responsable) => {
			id = actividad_id;
			actividad_responsable_id = actividad_responsable;
            if ($.fn.DataTable.isDataTable( '#responsables' ) ) {
                responsables.ajax.url( '/agenda/actividades?actividad_id=' + id ).load();
		    }else{
                responsables = $("#responsables").DataTable({
                    ajax: {
                        url: '/agenda/actividades?actividad_id=' + id,
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "responsable" },
                        { "data": "cargo" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Actividad.editarResponsable(" + data.actividad_responsable_id + ", \"" + data.responsable + "\", \"" + data.cargo + "\")' class='btn btn-xs btn-warning'>Editar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Actividad.eliminarResponsable(' + data.actividad_responsable_id + ', \'' + data.responsable + '\')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,3] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
            }
			if(actividad){
				$('#btnAgregarResponsable').text("Agregar");
				$('#btnAgregarResponsable').css('background-color','##00c0ef');
				$('#btnAgregarResponsable').css('border-color', '##00c0ef');
				$('#nombre_responsable, #cargo_responsable').val('');
				$('#modal-title-responsable').text(actividad);
				$('#modal-responsables').on('hidden.bs.modal', (e) => {
                	id = undefined;
            	});
				$('#modal-responsables').modal('show');
			}
        }

		var convertTime12to24 = (time12h) => {
            const [time, modifier] = time12h.split(' ');
            let [hours, minutes] = time.split(':');
            if (hours === '12') {
                hours = '00';
            }
            if (modifier === 'PM') {
                hours = parseInt(hours, 10) + 12;
            }
            return hours + ':' + minutes;
        }

		var guardarResponsable = () => {
			if($('#form_responsable').valid()) {
				block();
				var url = actividad_responsable_id ? '{{ route("agenda.responsables.update", ":actividad_responsable_id") }}' : '{{ route("agenda.responsables.store")}}'; 
				url = actividad_responsable_id ? url.replace(':actividad_responsable_id', actividad_responsable_id) : url;
				var type = actividad_responsable_id ? 'PUT' : 'POST';
				var data = $('#form_responsable').serialize() + '&actividad_id=' + id;
				var title = actividad_responsable_id ? 'Responsable actualizado' : 'Responsable registrado';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							getResponsables(id);
							$('#btnAgregarResponsable').text("Agregar");
                        	$('#btnAgregarResponsable').css('background-color','##00c0ef');
                        	$('#btnAgregarResponsable').css('border-color', '##00c0ef');
							$('#nombre_responsable, #cargo_responsable').val('');
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
							actividad_responsable_id = undefined;
						}else {
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}

		var editarResponsable = (actividad_responsable, nombre, cargo) => {
			actividad_responsable_id = actividad_responsable;
			$('#nombre_responsable').val(nombre);
			$('#cargo_responsable').val(cargo);
            $('#btnAgregarResponsable').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');
        }

		var eliminarResponsable = (actividad_responsable, nombre) => {
			swal({
				title: '¿Desea eliminar al responsable?',
				html: nombre,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar',
				cancelButtonText: 'No, regresar',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = '/agenda/responsables/' + actividad_responsable;
					app.ajaxHelper( url, 'DELETE').done( (response) => {
						unblock();
						if(!response.success){
							swal(
								'¡Error!',
								response.message,
								'error'
							);
						}else{
							$('#btnAgregarResponsable').text("Agregar");
                        	$('#btnAgregarResponsable').css('background-color','##00c0ef');
                        	$('#btnAgregarResponsable').css('border-color', '##00c0ef');
							$('#nombre_responsable, #cargo_responsable').val('');				
							actividad_responsable_id = undefined;
							getResponsables(id);
							swal({
								title: response.message,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
					});
				}
			});
		}

		return {
			inicializar,
			guardar,
			editar,
			eliminar,
			guardarResponsable,
			editarResponsable,
			eliminarResponsable,
			getActividades,
			getResponsables
		}
	})();
</script>