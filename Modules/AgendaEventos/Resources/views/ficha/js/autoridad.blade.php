<script>
  	var Autoridad = (() => {

		var id = undefined;
		var autoridades = undefined;

		var inicializar = () => {
			/*$('#cargo_autoridad').select2({
                language: 'es',
                tags: true,
                placeholder : 'SELECCIONE O INGRESE EL CARGO',
                ajax: {
                    url: "{{ route('cargos.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.nombre,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

			$('#partidopolitico_id').select2({
                language: 'es',
                placeholder : 'SELECCIONE EL PARTIDO POLÍTICO',
                ajax: {
                    url: "{{ route('partidos.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });		

			$('#form_autoridad').validate({
                rules: {
                    nombre_autoridad: {
                        required: true
                    },
                    cargo_autoridad: {
                        required: true
                    },
                    partidopolitico_id: {
                        required: true
                    }
				}
			});*/
			$('#form_autoridad').validate({
				rules: {
					presidentemunicipal_id: {
						required: true
					}
				}
			});
		}

		var guardar = () => {
			/*if($('#form_autoridad').valid()) {
				block();
				var url = id ? '{{ route("agenda.autoridades.update", ":autoridad_id") }}' : '{{ route("agenda.autoridades.store")}}'; 
				url = id ? url.replace(':autoridad_id', id) : url;
				var type = id ? 'PUT' : 'POST';
				var data = $('#form_autoridad').serialize() + '&fichainfo_id=' + Ficha.getId();
				var title = id ? 'Autoridad actualizada' : 'Autoridad registrada';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							id = undefined;
							getAutoridades(Ficha.getId());
							$('#btnAgregarAutoridad').text("Agregar");
                        	$('#btnAgregarAutoridad').css('background-color','##00c0ef');
                        	$('#btnAgregarAutoridad').css('border-color', '##00c0ef');
							$('#cargo_autoridad, #partidopolitico_id').val(null).trigger('change');
							$('#nombre_autoridad').val('');
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}else{
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}*/
			if($('#form_autoridad').valid()) {
				block();
				var url = '{{ route("agenda.autoridadesmunicipios.store")}}';
				var type = 'POST';
				var data = $('#form_autoridad').serialize() + '&fichainfo_id=' + Ficha.getId();
				var title = 'Autoridad municipal registrada.';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							id = undefined;
							getAutoridades(Ficha.getId());
							$('#presidentemunicipal_id').empty();
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}else{
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}

		var getAutoridades = (fichainfo_id) => {
            /*if ($.fn.DataTable.isDataTable( '#autoridades' ) ) {
                autoridades.ajax.url( '/agenda/autoridades?fichainfo_id=' + fichainfo_id ).load();
		    }else{
                autoridades = $("#autoridades").DataTable({
                    ajax: {
                        url: '/agenda/autoridades?fichainfo_id=' + fichainfo_id,
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "nombre" },
                        { "data": "cargo" },
						            { "data": "siglas" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Autoridad.editar(" + data.id + ', ' + data.partido_id + ", \"" + data.partido + "\", \"" + data.cargo + "\", \"" + data.nombre + "\")' class='btn btn-xs btn-warning'>Editar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Autoridad.eliminar(' + data.id + ', \'' + data.nombre + '\')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,3,4] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
				}*/
				if($.fn.DataTable.isDataTable('#autoridades')) {
					autoridades.ajax.url('/agenda/autoridadesmunicipios?fichainfo_id=' + fichainfo_id).load();
		    }
				else {
                autoridades = $("#autoridades").DataTable({
                    ajax: {
                        url: '/agenda/autoridadesmunicipios?fichainfo_id=' + fichainfo_id,
                        dataSrc: ''
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { 'data': 'nombre' },
												{ 'data': 'primer_apellido' },
												{ 'data': 'segundo_apellido' },
                        { 'data': 'cargo' },
						            { 'data': 'siglas' },
                        /*{
                            data: null,
                            render: ( data, type, row ) => {
															return "<button type='button' onclick='Autoridad.editar(" + data.id + ', ' + data.partido_id + ", \"" + data.partido + "\", \"" + data.cargo + "\", \"" + data.nombre + "\")' class='btn btn-xs btn-warning'>Editar</button>";
														}
												},*/
                        {
                            data: null,
                            render: (data, type, row) => {
															return '<button type="button" onclick="Autoridad.eliminar(' + data.id + ', \'' + data.nombre + '\', \'' + data.primer_apellido + '\', \'' + data.segundo_apellido + '\')" class="btn btn-xs btn-danger">Eliminar</button>';
														}
                        }
                    ],
                    columnDefs: [
                        { className: 'text-center', targets: [2,3,4] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
				}
    }

		var editar = (autoridad_id, partido_id, partido, cargo, nombre) => {
            /*id = autoridad_id;
            var partidoOpt = new Option(partido, partido_id, true, true);
            $('#partidopolitico_id').append(partidoOpt).trigger('change');
						$('#partidopolitico_id').valid();
						var cargoOpt = new Option(cargo, cargo, true, true);
						$('#cargo_autoridad').append(cargoOpt).trigger('change');
						$('#cargo_autoridad').valid();
            $('#nombre_autoridad').val(nombre);
            $('#btnAgregarAutoridad').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');*/
    }

		var eliminar = (autoridad_id, nombre, primer_apellido, segundo_apellido) => {
			swal({
				title: '¿Desea eliminar a la autoridad?',
				html: nombre + ' ' + primer_apellido + ' ' + segundo_apellido,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar',
				cancelButtonText: 'No, regresar',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = '/agenda/autoridadesmunicipios/' + autoridad_id;
					app.ajaxHelper(url, 'DELETE').done((response) => {
						unblock();
						if(!response.success) {
							swal(
								'¡Error!',
								response.message,
								'error'
							);
						}
						else {
							autoridades.ajax.reload(null, false);
							id = undefined;
							/*$('#btnAgregarAutoridad').text('Agregar');
              $('#btnAgregarAutoridad').css('background-color','##00c0ef');
              $('#btnAgregarAutoridad').css('border-color', '##00c0ef');
							$('#cargo_autoridad, #partidopolitico_id').val(null).trigger('change');
							$('#nombre_autoridad').val('');*/
							swal({
								title: response.message,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
					});
				}
			});
    }

		var cargarListado = () => {
			block();
				var url = '{{ route("agenda.cargarpresidentes")}}' + '?fichainfo_id=' + Ficha.getId();
				var type = 'POST';
				var title = 'Presidentes municipales precargados.';

				$.ajax({
					url: url,
					type: type,
					success: (response) => {
						unblock();		
						if(response.success) {
							autoridades.ajax.reload(null, false);
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
						else {
							swal(
								'Error',
								response.message,
								'error'
							)
						}
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'',
							'error'
						)
					}
				});
		}

		return {
			inicializar,
			guardar,
			editar,
			eliminar,
			getAutoridades,
			cargarListado
		}
	})();
</script>