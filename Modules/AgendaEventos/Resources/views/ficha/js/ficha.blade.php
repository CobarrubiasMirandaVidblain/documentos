<script>
  	var Ficha = (() => {

		var id = undefined;
		@if(isset($evento->ficha))
            id = {{ $evento->ficha->id }};
        @endif

		var inicializar = () => {
			$("#vestimenta_id, #tipoevento").select2({
				language: 'es',
				placeholder: 'SELECCIONE UNA OPCIÓN',
				minimumResultsForSearch: Infinity
			}).change(evt => {
				$(evt.target).valid();
			});

			/*$("#participacion_presidenta, #participacion_director").select2({
				language: 'es',
				placeholder: 'SELECCIONE O INGRESE LA PARTICIPACIÓN',
				allowClear : true,
				tags: true
			}).change(evt => {
				$(evt.target).valid();
			});*/

			$("#participacion_presidenta, #participacion_director").select2({
                language: 'es',
                tags: true,
								placeholder : 'SELECCIONE O INGRESE EL CARGO',
								allowClear : true,
                ajax: {
                    url: "{{ route('agenda.participaciones.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.nombre,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(evt => {
                $(evt.target).valid();		
            });

			$('#cargo_persona_convoca, #cargo_persona_preside').select2({
                language: 'es',
                tags: true,
                placeholder : 'SELECCIONE O INGRESE EL CARGO',
                ajax: {
                    url: "{{ route('cargos.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.nombre,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(evt => {
                $(evt.target).valid();		
						});

			$("#tabFicha").tabs({
				beforeActivate: (event, ui) => {
					if(ui.oldPanel[0].id === 'tab_datos') {
						if($('#form_datos').valid()) {
							guardar(ui.oldTab[0], ui.newTab[0]);
						}else {
							return false;
						}
					}else {
						$(ui.newTab[0]).addClass('active');
						$(ui.oldTab[0]).removeClass('active');
					}
				}
			});

			$('#form_datos').validate({
                rules: {
                    numero_ficha: {
                        //required: true,
						maxlength: 20
                    },
                    tipoevento: {
                        //required: true
                    },
                    vestimenta_id: {
                        //required: true
                    },
                    aforo: {
						//required: true,
						pattern_integer: ''
                    },
                    temperatura: {
						//required: true,
						maxlength: 2,
						pattern_integer: ''
					},
					precipitacion: {
						//required: true,
						maxlength: 2,
						pattern_integer: ''
					},
					humedad: {
						//required: true,
						maxlength: 2,
						pattern_integer: ''
					},
					viento: {
						//required: true,
						maxlength: 2,
						pattern_integer: ''
					},
					objetivo: {
                        required: true,
						maxlength: 1000
					},
					participacion_presidenta: {
						required: true
					},
					/*participacion_director: {
						required: true
					},*/
					persona_convoca_id: {
						required: true
					},
					persona_preside_id: {
						required: true
					},
					cargo_persona_convoca: {
						required: true
					},
					cargo_persona_preside: {
						required: true
					}
        }
      });
		}

		var guardar = (oldTab, newTab) => {
			if($('#form_datos').valid()) {
				block();
				var url = id ? '{{ route("agenda.fichas.update", ":id") }}' : '{{ route("agenda.fichas.store")}}'; 
				url = id ? url.replace(':id', id) : url; 
				var type = id ? 'PUT' : 'POST';

				if($('#participacion_director').val() !== undefined) {
					var data = id ? $('#form_datos').serialize() : $('#form_datos').serialize() + '&evento_id={{ $evento->id }}';
				}
				else {
					var data = id ? $('#form_datos').serialize() : $('#form_datos').serialize() + '&evento_id={{ $evento->id }}' + '&participacion_director=null';
				}
				
				var title = id ? 'Ficha actualizada' : 'Ficha registrada';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(!response.success) {
							$( "#tabFicha" ).tabs({ active: 0 });
							swal(
								'Error',
								response.message,
								'error'
							)
						} else {
							id = response.ficha.id;
							Asistente.getAsistentes(id);
							$(newTab).addClass('active');
							$(oldTab).removeClass('active');
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}                    
					},
					error: (response) => {
						$( "#tabFicha" ).tabs({ active: 0 });
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}		

		var getId = () => {
			return id;
		}

		var imprimir = () => {
      		if(id) {
				$.ajax({
					url: "/agenda/fichas/"+id,
					type: 'GET',
					success: (response) => {
						console.log(response);
						jsreport.serverUrl = 'http://187.157.97.110:3001';
						{{-- jsreport.serverUrl = '{{ env("REPORTER_URL", "http://187.157.97.110:3001") }}'; --}}
						var request = {
							template:{
								shortid: "BytOF_sq4"
								//shortid: "SJzRJBsEV" // FichaInformativa
							},
							data: response
						};
						//jsreport.render('_blank', request);
						jsreport.renderAsync(request).then(function(res) {
								window.open(res.toDataURI())
								res.download('FichaInformativa.pdf')
						});
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
      		} else {
				swal(
					'',
					'Primero guarde los datos de la ficha',
					'info'
				)
			}
		}

		return {
			inicializar,
      		guardar,
			imprimir,
			getId
		}

	})();
</script>