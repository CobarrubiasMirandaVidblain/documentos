<script>
  	var Aportacion = (() => {

		var id = undefined;
		var aportaciones = undefined;

		var inicializar = () => {
			
			$('#dependencia_id').select2({
                language: 'es',
                placeholder : 'SELECCIONE O INGRESE LA DEPENDENCIA',
                ajax: {
                    url: "{{ route('dependencias.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });		

			$('#form_aportacion').validate({
                rules: {
                    dependencia_id: {
                        required: true
                    },
                    producto: {
                        required: true
                    },
                    cantidad: {
                        required: true,
						pattern_integer: ''
                    },
					monto: {
                        required: true,
						pattern_importe: ''
                    }
				}
			});
		}

		var guardar = () => {
			if($('#form_aportacion').valid()) {
				block();
				var url = id ? '{{ route("agenda.aportaciones.update", ":aportacion_id") }}' : '{{ route("agenda.aportaciones.store")}}'; 
				url = id ? url.replace(':aportacion_id', id) : url;
				var type = id ? 'PUT' : 'POST';
				var data = $('#form_aportacion').serialize() + '&fichainfo_id=' + Ficha.getId();
				var title = id ? 'Aportación actualizada' : 'Aportación registrada';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							id = undefined;
							getAportaciones(Ficha.getId());
							$('#btnAgregarAportacion').text("Agregar");
                        	$('#btnAgregarAportacion').css('background-color','##00c0ef');
                        	$('#btnAgregarAportacion').css('border-color', '##00c0ef');
							$('#dependencia_id').val(null).trigger('change');
							$('#cantidad, #monto, #producto').val('');
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}else{
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}

		var getAportaciones = (fichainfo_id) => {
            if ($.fn.DataTable.isDataTable( '#aportaciones' ) ) {
                aportaciones.ajax.url( '/agenda/aportaciones?fichainfo_id=' + fichainfo_id ).load();
		    }else{
                aportaciones = $("#aportaciones").DataTable({
                    ajax: {
                        url: '/agenda/aportaciones?fichainfo_id=' + fichainfo_id,
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "nombre" },
                        { "data": "producto" },
						{ "data": "cantidad" },
						{ "data": "monto" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Aportacion.editar(" + data.id + ', ' + data.dependencia_id + ", \"" + data.nombre + "\", \"" + data.producto + "\", " + data.cantidad + ", \"" + data.monto  + "\")' class='btn btn-xs btn-warning'>Editar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Aportacion.eliminar(' + data.id + ', \'' + data.producto + '\', \'' + data.nombre + '\')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,4,5] },
                        {className: 'text-right', targets: [3] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [10], [10] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
            }
        }

		var editar = (aportacion_id, dependencia_id, nombre, producto, cantidad, monto) => {
            id = aportacion_id;
            var dependenciaOpt = new Option(nombre, dependencia_id, true, true);
            $('#dependencia_id').append(dependenciaOpt).trigger('change');
			$('#dependencia_id').valid();
            $('#producto').val(producto);
			$('#cantidad').val(cantidad);
			$('#monto').val(monto);
            $('#btnAgregarAportacion').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');
        }

		var eliminar = (aportacion_id, producto, nombre) => {			
			swal({
				title: '¿Desea eliminar la aportación?',
				html: producto + '<br>' + nombre,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar',
				cancelButtonText: 'No, regresar',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = '/agenda/aportaciones/' + aportacion_id;
					app.ajaxHelper( url, 'DELETE').done( (response) => {
						unblock();
						if(!response.success){
							swal(
								'¡Error!',
								response.message,
								'error'
							);
						}else{							
							aportaciones.ajax.reload(null, false);
							id = undefined;
							$('#btnAgregarAportacion').text("Agregar");
                        	$('#btnAgregarAportacion').css('background-color','##00c0ef');
                        	$('#btnAgregarAportacion').css('border-color', '##00c0ef');
							$('#dependencia_id').val(null).trigger('change');
							$('#cantidad, #monto, #producto').val('');
							swal({
								title: response.message,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
					});
				}
			});
        }

		return {
			inicializar,
			guardar,
			editar,
			eliminar,
			getAportaciones
		}
	})();
</script>