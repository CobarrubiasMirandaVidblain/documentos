@extends('agendaeventos::layouts.master')

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">

    <link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
    <link rel="stylesheet" href="{{ asset('bower_components/datatables/rowGroup.bootstrap.min.css') }}">
    <style>
        span.select2-selection__rendered, .select2-results__option {
            text-transform: uppercase;
        }

        [class^="icheckbox_square-"]:not(.checked) {
            background-position: -24px 0 !important
        }
    </style>
@endpush

@section('content-title', 'Agenda de Giras y Eventos')
@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li class="active">Agenda de Giras y Eventos</li>
@endsection

@section('content')
<section class="content">
    <div class="box box-primary shadow">
        <div class="box-header with-border">
            <h3 class="box-title">Tabla de Solicitudes</h3>
						<div class="row">
              <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                  <div class="form-group">
                      <label for="anio">Año* :</label>
											<select class="form-control" id="anio">
												<option value="">DEFAULT</option>
												<option value="2019">2019</option>
												<option value="2018">2018</option>
												<option value="2017">2017</option>
											</select>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                  <div class="form-group">
                      <label for="estado">Estado* :</label>
											<select class="form-control" id="estado">
												<option value="">DEFAULT</option>
												<option value="N">NUEVO</option>
												<option value="E">EN PROCESO</option>
												<option value="F">FINALIZADO</option>
											</select>
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                  <div class="form-group">
                      <label for="producto">Producto* :</label>
											<input type="text" class="form-control" id="producto">
                  </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="form-group">
											<label for="municipio_id">Municipio* :</label>
											<select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
												<option value="" selected="selected">DEFAULT</option>
											</select>
									</div>
              </div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<div class="form-group">
								<button type="button" class="btn btn-block btn-info" onclick="filtrarTabla()"><i class="fa fa-table"></i> Filtrar Tabla</button>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<div class="form-group">
								<button type="button" class="btn btn-block btn-default" onclick="excel();"><i class="fa fa-file-excel-o"></i> Descargar Excel</button>
							</div>
						</div>
        </div>
        <div class="box-body">
        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'tipos', 'name' => 'tipos', 'style' => 'width: 100%']) !!}
        </div>
    </div>
</section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript">
	function block() {
        baseZ = 1030;        
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: baseZ,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

        function unblock_error() {
            if($.unblockUI()) {
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
            }
        }

	    setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
</script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
{!! $dataTable->scripts() !!}
<script>
    var excel = () => {
        block();
        var url = '/agenda/excelpeticion?anio=' + $("#anio").select2('val') + '&estado=' + $("#estado").select2('val') + '&producto=' + $("#producto").val() + '&municipio_id=' + $("#municipio_id").select2('val');
				
				$.ajax({
					url: url,
					type: 'GET',
					success: (response) => {
						console.log(response);
						
						var reformattedArray = response.map(function(obj) {
							return Object.values(obj);
						});
						
						jsreport.serverUrl = 'http://187.157.97.110:3001';
						
						var request = {
							template:{
								shortid: 'HJ5VttAyS'
							},
							data: {
								titulo: 'Agenda Eventos',
								subtitulo: 'Lista de datos',
								cabeceras: [
									'FOLIO',
                  'AÑO',
                  'REGION',
                  'MUNICIPIO',
                  'LOCALIDAD',
                  'SOLICITANTE',
                  'CARGO SOLICITANTE',
                  'ASUNTO',
                  'PRODUCTO',
                  'CANTIDAD',
                  'FECHA RECIBIDO',
                  'CURP',
									'RFC',
									'GENERO',
									'DIRECCION',
									'DEPENDENCIA EXTERNA',
									'ESTADO'
								],
								datos: reformattedArray
							}
						};
						
						jsreport.renderAsync(request).then(function(res) {
							res.download('datos.xlsx');
							unblock();
						});
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
    }

let filtrar = (columna, valor) => {
	LaravelDataTables["tipos"].columns(columna).search(valor).draw();
}

let filtrarTabla = () => {
	LaravelDataTables["tipos"].columns(1).search($('#anio').val());
	LaravelDataTables["tipos"].columns(16).search($('#estado').val());
	LaravelDataTables["tipos"].columns(8).search($('#producto').val());
	LaravelDataTables["tipos"].columns(17).search($('#municipio_id').val());
	LaravelDataTables["tipos"].draw();
}

let imprimir = (res) => {
	block();
	$.ajax({
		url: LaravelDataTables["tipos"].ajax.url() || window.location.href,
		data: { draw: 2, length: -1, action:'customExcel' },
		success: function (res) {
			var datos = {};
			datos.titulo = document.title;
			datos.cabeceras = res.cabeceras
			datos.datos = res.data.map(d => Object.values(d)).map(e => e.map(f => f != null ? f : '-'));
			window.varx = datos
			window.vary = res
			jsreport.serverUrl = "http://187.157.97.110:3001";
			var request = {
				template: {
					shortid: "HJ5VttAyS"
				},
				data: datos
			};
			jsreport.renderAsync(request).then(function(res2) {
				res2.download(res.name + '.xlsx');
				unblock();
			});
		},
		error: function (res, err) {
			unblock();
		}
	});
}

$(document).ready(function() {
	$('#municipio_id').select2({
				language: 'es',
				allowClear: true,
				placeholder: 'DEFAULT',
				minimumInputLength: 2,
				ajax: {
					url: '{{ route('agenda.peticiones.localidades') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			})
    $('#anio').select2();
		$('#estado').select2();
});
</script>
@endpush