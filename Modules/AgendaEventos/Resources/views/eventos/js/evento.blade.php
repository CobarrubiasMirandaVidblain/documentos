<script type="text/javascript">
    var Evento = (() => {

        var form_evento, form_area;
        var gira_id = undefined;
        var evento_id = undefined;
        var evento_google_id = undefined;
        var calendario_google_id = undefined;
        var area_id =  undefined;
        var areas = undefined;
        var compartidos = undefined;
        var giras;
        var form_compartir;

        var handleClientLoad = () => {
            gapi.load('client:auth2', initClient);
        }

        var initClient = () => {            
            gapi.client.init({
                apiKey: 'AIzaSyC9u0yWqGY5cXp0jqzX_snVN1YiriTHxUg',
                clientId: '489363224492-tob8tfhthj23q4vf21fss3gobbmamuig.apps.googleusercontent.com',   
                discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
                scope: "https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar"
            }).then( () => {                
            }, (error) => {
                console.log('Error', JSON.stringify(error, null, 2) );
            });
        }

        var verificarUsuario = () => {
            if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
                if (gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail() != 'agendadifoaxaca@gmail.com'){
                    swal(
                        '¡Sesión cerrada!',
                        'Esta cuenta no está autorizada para gestionar Calendarios de Google',
                        'error'
                    );
                    gapi.auth2.getAuthInstance().signOut();
                    return false;
                }else {
                    return true;
                }
            }else {
                gapi.auth2.getAuthInstance().signIn().then(function(response){
                    if (gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail() != 'agendadifoaxaca@gmail.com'){
                    swal(
                        '¡Sesión cerrada!',
                        'Esta cuenta no está autorizada para gestionar Calendarios de Google',
                        'error'
                    );
                    gapi.auth2.getAuthInstance().signOut();
                    return false;
                }else {
                    return true;
                }
                });
            }
        };        

        var get_giras = () => {
            $("[class^='square-']").each( function () {     
                $(this).iCheck({
                    checkboxClass: 'icheckbox_' + $(this).attr('class')
                });
            });

            $("[class^='square-']").on('ifChecked', function(event){
                $('#calendario').fullCalendar('refetchEvents');
            });

            $("[class^='square-']").on('ifUnchecked', function(event){
                $('#calendario').fullCalendar('refetchEvents');
            });        

            $('#calendario').fullCalendar({
                header    : {
                    left  : 'prev,next',
                    center: 'title',
                    right : 'month,agendaWeek,agendaDay,listMonth'
                },
                locale : 'es',
                loading: (isLoading, view) => {
                    if(isLoading){
                        block();                    
                    }else{
                        unblock();
                    }
                },
                dayClick: (date, jsEvent, view) => {
                    crear_o_editar(null, null);
                },            
                navLinks : true,
                selectHelper : false,
                eventLimit : true,
                events: (start, end, timezone, callback) => {
                    console.log(start);                    
                    $.ajax({
                        url: "{{ route('agenda.eventos.index') }}",
                        data: {
                                start: start.toISOString(),
                                end: end.add(-1).toISOString(),
                                tipos: () => {
                                    var tipos = [];
                                    $.each($("input[name='tipos_eventos[]']:checked"), function() {
                                        tipos.push($(this).val());
                                    });
                                    return JSON.stringify(tipos);
                                }                                
                        },
                        success: (data) => {
                            data.map( (evento) => {
                                evento.title = evento.nombre;
                                evento.start = evento.fecha;
                                evento.color = '#D12654';
                                return evento;
                            });
                            callback(data);
                        }
                    });
                },
                eventClick : (event, element) => {                    
                    crear_o_editar(event.evento_id, event.nombre, event.calendario_google, event.evento_google);
                },
            });

            app.to_upper_case();            
            giras = $('#giras').DataTable();
            $('#buscar').keypress(function(e) {
			    if(e.which === 13) {
				    giras.search($('#buscar').val()).draw();
			    }
		    });
		    $('#btn_buscar').on('click', function() {
			    giras.search($('#buscar').val()).draw();
            });
        }
        
        var inicializar = () => {
            $('#modal-evento').on('hidden.bs.modal', (e) => {
                evento_id = undefined;
                gira_id = undefined;
                area_id = undefined;
            });

            $('.timepicker').timepicker({
                showInputs: false,
                defaultTime: false
            });

            form_evento = $('#form_evento');
            form_area = $('#form_area');
            form_compartir = $('#form_compartir');
			agregar_validacion();
            app.to_upper_case();

            agregar_fecha();
			agregar_select_create();
            agregar_select_edit();
            $("#tabs").tabs({
                beforeActivate: (event, ui) => {  
                    if(ui.newPanel[0].id === 'areas' || ui.newPanel[0].id === 'compartir' || ui.newPanel[0].id === 'tablaCompartir'){
                        if(form_evento.valid() && evento_id) {                 
                                getAreas();
                                getCompartidos();
                            //guardar(ui.oldTab[0], ui.newTab[0]);
                        }else{
                            swal(
                                '',
                                'Primero guarde sus cambios',
                                'info'
                            );
                            return false;
                        }
                    }else{
                        $(ui.newTab[0]).addClass('active');
                        $(ui.oldTab[0]).removeClass('active');
                        $('#modal-footer-evento').show();
                    }
                }
            });
        }

        var agregar_select_create = () => {
            $('#tipoevento_id').select2({
                language: 'es',
                tags: true,
                allowClear : true,
                placeholder : 'Seleccione el calendario',
				ajax: {
					url: "{{ route('agenda.calendarios.search') }}",
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
            });

            $('#nombre').select2({
				language: 'es',
                tags: true,
                allowClear : true,
                placeholder : 'Seleccione el evento o la gira',
				ajax: {
					url: "{{ route('agenda.search') }}",
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.nombre,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change((event) => {
                $('#nombre').valid();
            });

			$('#municipio_id').select2({
				language: 'es',
				minimumInputLength: 2,
                allowClear : true,
                placeholder : 'Seleccione un municipio',
				ajax: {
					url: '{{ route('municipios.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change((event) => {
				$('#municipio_id').valid();
				$('#localidad_id').empty();
				$('#localidad_id').select2({
					language: 'es',
                    allowClear : true,
                    placeholder : 'Seleccione una localidad',
					ajax: {
						url: '{{ route('localidades.select') }}',
						delay: 500,
						dataType: 'JSON',
						type: 'GET',
						data: (params) => {
							return {
								search: params.term,
								municipio_id: $('#municipio_id').val()
							};
						},
						processResults: (data, params) => {
							params.page = params.page || 1;
							return {
								results: $.map(data, (item) => {
									return {
										id: item.id,
										text: item.nombre,
										slug: item.nombre,
										results: item
									}
								})
							};
						},
						cache: true
					}
				}).change((event) => {
					$('#localidad_id').valid();
                    $('#localidad').valid();
				});
			});

            $('#localidad').on('input',(e) => {
                $('#localidad_id').valid();
            });

			$('#localidad_id').select2({
				language: 'es',
                allowClear : true,
                placeholder : 'Seleccione una localidad',
			});

            $('#area_id').select2({
				language: 'es',
				ajax: {
					url: '{{ route('areas.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			});
		};

        function agregar_select_edit() {
			$('#localidad_id').select2({
				language: 'es',
				ajax: {
					url: '{{ route('localidades.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term,
							municipio_id: $('#municipio_id').val()
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				$('#localidad_id').valid();
			});
		};
        
        var agregar_fecha = () => {
			$('#fecha').datepicker({
				autoclose: true,
				language: 'es',
				startDate: '01-01-1900',
				orientation: 'bottom'
			}).change((event) => {
				$('#fecha').valid();
			});
		};

        var agregar_validacion = () => {
            form_evento.validate({
                rules: {
                    nombre: {
                        required: true
                    },
                    tipoevento_id: {
                        required: true
                    },
                    fecha: {
                        required: true
                    },
                    horax: {
                        required: true
                    },
                    calle: {
                        required: true
                    },
                    municipio_id: {
                        required: true
                    },
                    localidad_id:{
                        required:{ 
							depends: function(element) {
                                return ($('#localidad').val() == '' || $('#localidad').val() == null);
                            }
                        }
                    },
                    localidad:{
                        required:{ 
							depends: function(element) {
                                return ($('#localidad_id').val() == '' || $('#localidad_id').val() == null);
                            }
                        }
                    }
                }
            });

            form_area.validate({
                rules: {
                    area_id: {
                        required: true
                    }
                }
            });

            form_compartir.validate({
                rules: {
                    persona_id: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            }); 
        };

        var convertTime12to24 = (time12h) => {
            const [time, modifier] = time12h.split(' ');
            let [hours, minutes] = time.split(':');
            if (hours === '12') {
                hours = '00';
            }
            if (modifier === 'PM') {
                hours = parseInt(hours, 10) + 12;
            }
            return hours + ':' + minutes;
        }

        function ISODateString(d) {
            function pad(n){
                return n<10 ? '0'+n : n
            }
            return 2019+'-'+ pad(1)+'-'+ pad(15)+'T'+ pad(16)+':'+ pad(41)+':'+ pad(30+'Z')
        }

        var crearEvento =(oldTab, newTab) => {
            var fechas = $('#fecha').val().split('/');
            var fecha = fechas[2]+'-'+ fechas[1] +'-' + fechas[0];
            var hora = convertTime12to24($('#horax').val());
            var nombre = $('#nombre').select2('data')[0].text;
            var municipio = $('#municipio_id').select2('data')[0].text;
            var localidad = $('#localidad_id').select2('data')[0].text;
            var otra_localidad = $('#localidad').val();
            var calle = $('#calle').val();
            var observacion = $('#observacion').val();
            calendario_google_id = $('#tipoevento_id').select2('data')[0].results.id_calendario_google;

            console.log(hora);

            var fechita = fecha + 'T' + hora + ':00';// ISODateString(anio, mes, dia, horas, minutos);
						
            console.log(fechita);

            var evento = {
                'summary': nombre,
                'location': municipio,
                'description': localidad + ' ' + otra_localidad + ' ' + observacion,
                'start': {
                    "dateTime": fechita,
                    "timeZone": "America/Mexico_City"
                },
                'end': {
                    "dateTime": fechita,
                    "timeZone": "America/Mexico_City"
                }
            };

            var event_id = 0;
						
						app.ajaxHelper( '/agenda/eventos', 'POST', form_evento.serialize() + '&hora=' + hora +  '&id_evento_google=' + event_id).done( (response) => {
                if(!response.success) {
                    swal(
                        '¡Error!',
                        response.message,
                        'error'
                    );
                }else {
                    evento_id = response.evento.id;
                    gapi.client.calendar.events.insert({
                        'calendarId': calendario_google_id,
                        'resource': evento,
                        'sendNotifications': true,
                        'sendUpdates': 'all'
                    }).execute((event) => {
                        evento_google_id = event.id;
                        editarEvento();
                        $(newTab).addClass('active');
                        $(oldTab).removeClass('active');
                        getAreas();
                        getCompartidos();
                        $('#calendario').fullCalendar('refetchEvents');
                        giras.ajax.reload(null, false);
                        $('#modal-footer-evento').hide();
                        $('#tabs').tabs({ active: 1 });
                    });                    
                }
            });            
        }

        var editarEvento =(oldTab, newTab) => {
            var fechas = $('#fecha').val().split('/');
            var fecha = fechas[2]+'-'+ fechas[1] +'-' + fechas[0];
            var hora = convertTime12to24($('#horax').val());
            var nombre = $('#nombre').select2('data')[0].text;
            var municipio = $('#municipio_id').select2('data')[0].text;
            var localidad = $('#localidad_id').select2('data')[0].text;
            var otra_localidad = $('#localidad').val();
            var calle = $('#calle').val();
            var observacion = $('#observacion').val();

            var fechita = fecha + 'T' + hora + ':00';// ISODateString(anio, mes, dia, horas, minutos);
            
            //console.log(fechita);
            
            gapi.client.calendar.events.get({
                'calendarId': calendario_google_id,
                'eventId': evento_google_id
            }).execute((evento_de_google) => {
                //console.log(evento_de_google);
                var evento = {
                    'summary': nombre,
                    'location': municipio,
                    'description': localidad + ' ' + otra_localidad + ' ' + observacion,
                    'start': {
                        "dateTime": fechita,
                        "timeZone": "America/Mexico_City"
                    },
                    'end': {
                        "dateTime": fechita,
                        "timeZone": "America/Mexico_City"
                    },
                    'attendees': evento_de_google.attendees
                };

                app.ajaxHelper( '/agenda/eventos/' + evento_id, 'PUT', form_evento.serialize() + '&hora=' + hora +  '&id_evento_google=' + evento_google_id).done( (response) => {
                    if(!response.success) {
                        swal(
                            '¡Error!',
                            response.message,
                            'error'
                        );
                    } else {

                        if ( $('#tipoevento_id').select2('data')[0].results && $('#tipoevento_id').select2('data')[0].results.id_calendario_google !=  calendario_google_id){
                            //Quiere decir que el evento se cambia de calendario, entonces se elimina de uno y se agrega en el otro
                            console.log('Otro calendario.');
                            gapi.client.calendar.events.delete({
                                calendarId: calendario_google_id,
                                eventId: evento_google_id
                            }).execute(() => {
                                calendario_google_id = $('#tipoevento_id').select2('data')[0].results.id_calendario_google;
                                gapi.client.calendar.events.insert({
                                    'calendarId': calendario_google_id,
                                    'resource': evento,
                                    'sendNotifications': true,
                                    'sendUpdates': 'all'
                                }).execute((event) => {
                                    evento_google_id = event.id;
                                    editarEvento();
                                });
                            });
                        } else { 
                            console.log('Mismo calendario.');
                            //Update normal, mismo calendario
                            gapi.client.calendar.events.update({
                                'calendarId': calendario_google_id,
                                'resource': evento,
                                'eventId': evento_google_id,
                                'sendNotifications': true,
                                'sendUpdates': 'all'
                            }).execute((event) => {
                                $('#calendario').fullCalendar('refetchEvents');
                                $(newTab).addClass('active');
                                $(oldTab).removeClass('active');
                                getAreas();
                                getCompartidos();
                                giras.ajax.reload(null, false);
                                //$('#modal-footer-evento').hide();
                            });
                        }                   
                    }
                });
            });
        }

        var compartir = () => {
            if(form_compartir.valid()) {
                block();
                
                return gapi.client.calendar.events.get({
                    "calendarId": calendario_google_id,
                    "eventId": evento_google_id
                })
                .then(function(response) {
                    var attendees;

                    if(typeof response.result.attendees === 'undefined') {
                        attendees = [];
                    }
                    else {
                        attendees = response.result.attendees;
                    }

                    console.log(attendees);
                    console.log('calendarId: ' + calendario_google_id);
                    console.log('eventId: ' + evento_google_id);

                    var obj = {
                        'email': $('#email').val()
                    };

                    attendees.push(obj);

                    var patch_event = {
                        'attendees': attendees
                    };

                    return gapi.client.calendar.events.patch({
                        'calendarId': calendario_google_id,
                        'eventId': evento_google_id,
                        'sendNotifications': true,
                        'sendUpdates': 'all',
                        'resource': patch_event
                    })
                    .then(function(response) {
                        console.log(response);

                        var resultado = [{
                            evento_id: evento_id,
                            persona_id: $("#persona_id").val(),
                            nombre: $("#nombre_persona").val(),
                            primer_apellido: $("#primer_apellido").val(),
                            segundo_apellido: $("#segundo_apellido").val(),
                            correo: $('#email').val()
                        }];

                        axios({
                            method: 'post',
                            url: '/agenda/api/v1/eventospersonas',
                            data: resultado
                        })
                        .then(function(res) {
                            getCompartidos();
                            
                            unblock();

                            swal({
                                type: 'success',
                                title: '' + 'Ok',
                                html: 'Evento o gira compartido con exito.',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        })
                        .catch(function(err) {
                            unblock();
                        });
                    },
                    function(error) {
                        unblock();
                        console.log(error);
                    });

                },
                function(error) {
                    unblock();
                    console.log(error);
                });
            }
        }
        
        var guardar = (oldTab, newTab) => {
            if(form_evento.valid()) {                
                if( evento_id ) {
                    console.log("voy a editar");
                    
                    editarEvento(oldTab, newTab);
                } else {
                    console.log("voy a crear");
                    
                    crearEvento(oldTab, newTab);
                }                                
            }
		}

        var getAreas = () => {
            if($.fn.DataTable.isDataTable( '#tblAreas' )) {
                areas.ajax.url( '/agenda/eventos/' + evento_id + '/areas' ).load();
		    }
            else{
                areas = $("#tblAreas").DataTable({
                    ajax: {
                        url: '/agenda/eventos/' + evento_id + '/areas',
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "area" },
                        {
                            "data": "genera_ficha",
                            render: ( data, type, row ) => {
                                if(parseInt(data) === 1) {
                                    return 'SI';
                                }
                                return 'NO';
                            }
                        },
                        { "data": "observaciones" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Evento.editar_area(" + data.area_id + ", \"" + data.area + "\", " + JSON.stringify(data.observaciones) + ")' class='btn btn-xs btn-warning'>Editar</button>";
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Evento.eliminar_area(' + data.area_id + ', \'' + data.area + '\'' + ')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,3] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
            }
        }

        var getCompartidos = () => {
            if($.fn.DataTable.isDataTable('#evento_personas')) {
                compartidos.ajax.url('/agenda/api/v1/eventospersonas' + '?evento_id=' + get_evento_id()).load();
		    }
            else
            {
                compartidos = $('#evento_personas').DataTable({
                    language: {
                        'url': '{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}'
                    },
                    processing: true,
                    serverSide: true,
                    ajax: '/agenda/api/v1/eventospersonas' + '?evento_id=' + get_evento_id(),
                    columns: [
                        { data: 'nombre', name: 'nombre' },
                        { data: 'primer_apellido', name: 'primer_apellido' },
                        { data: 'segundo_apellido', name: 'segundo_apellido' },
                        { data: 'correo', name: 'correo' }
                    ]
                });
                /*areas = $("#tblAreas").DataTable({
                    ajax: {
                        url: '/agenda/eventos/' + evento_id + '/areas',
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "area" },
                        {
                            "data": "genera_ficha",
                            render: ( data, type, row ) => {
                                if(data === 1) {
                                    return 'SI';
                                }
                                return 'NO';
                            }
                        },
                        { "data": "observaciones" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Evento.editar_area(" + data.area_id + ", \"" + data.area + "\", " + JSON.stringify(data.observaciones) + ")' class='btn btn-xs btn-warning'>Editar</button>";
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Evento.eliminar_area(' + data.area_id + ', \'' + data.area + '\'' + ')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,3] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });*/
            }
        }

        var crear_o_editar = (evento, nombre, calendario, evento_google) => {
            if(gapi.auth2.getAuthInstance().isSignedIn.get()) {
                evento_id = evento;
                evento_google_id = evento_google;
                calendario_google_id = calendario;
                var url = evento ? '{{ route("agenda.eventos.edit", [":evento"]) }}'.replace(':evento', evento_id) : "{{ route('agenda.eventos.create') }}";
                app.ajaxHelper( url, 'GET').done( (response) => {                 
                    $('#modal-body-evento').html(response.html);
                    $('#modal-evento-title').text(nombre || 'Registrar evento');
                    inicializar();                
                    $('#modal-footer-evento').show();
                    $('#modal-evento').modal('show');
                });
            } else {
                verificarUsuario();
            }
        }

        var eliminar = (evento, nombre, fecha, municipio, localidad, calendario, evento_google) => {
            if(gapi.auth2.getAuthInstance().isSignedIn.get()) {
                swal({
                    title: '¿Desea eliminar el evento?',
                    html: nombre + '<br>' + fecha + '<br>' + municipio + '<br>' + localidad,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, eliminar',
                    cancelButtonText: 'No, regresar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {     
                        var url = '/agenda/eventos/' + evento;
                        app.ajaxHelper( url, 'DELETE').done( (response) => {
                            unblock();
                            if(!response.success){
                                swal(
                                    '¡Error!',
                                    response.message,
                                    'error'
                                );
                            }else{
                                gapi.client.calendar.events.delete({
                                    calendarId: calendario,
                                    eventId: evento_google
                                }).execute(() => {
                                    giras.ajax.reload(null, false);
                                    swal(
                                        '¡Correcto!',
                                        response.message,
                                        'success'
                                    );                          
                                });
                            }
                        });
                    }
                });
            } else {
                verificarUsuario();
            }
        }

        var agregar_area = () => {
            if(form_area.valid()) {
                var url = area_id ? ('/agenda/eventos/' + evento_id + '/areas/' + area_id) : ('/agenda/eventos/' + evento_id + '/areas');
                var metodo = area_id ? 'PUT' : 'POST';

                block();

                app.ajaxHelper( url, metodo,  form_area.serialize()).done((response) => {
                    if(!response.success) {
                        unblock();

                        swal(
                            '¡Error!',
                            response.message,
                            'error'
                        );
                    }
                    else {
                        area_id = undefined;
                        $('#area_id').removeAttr('disabled');
                        $('#btnAgregar').text("Agregar");
                        $('#btnAgregar').css('background-color','##00c0ef');
                        $('#btnAgregar').css('border-color', '##00c0ef');
                        $('#area_id').val(null).trigger('change');
                        $('#observaciones').val('');
                        areas.ajax.reload(null, false);
                        
                        var emails = response.emails;

                        gapi.client.calendar.events.get({
                            "calendarId": calendario_google_id,
                            "eventId": evento_google_id
                        })
                        .then(function(response) {
                            //block();
                            
                            var attendees;

                            if(typeof response.result.attendees === 'undefined') {
                                attendees = [];
                            }
                            else {
                                attendees = response.result.attendees;
                            }

                            console.log(attendees);
                            console.log('calendarId: ' + calendario_google_id);
                            console.log('eventId: ' + evento_google_id);

                            var resultado = [];

                            for(var i = 0; i < emails.length; i++) {
                                var obj = {
                                    'email': emails[i].email
                                };

                                var objRes = {
                                    evento_id: evento_id,
                                    persona_id: emails[i].persona_id,
                                    nombre: $("#nombre_persona").val(),
                                    primer_apellido: $("#primer_apellido").val(),
                                    segundo_apellido: $("#segundo_apellido").val(),
                                    correo: emails[i].email
                                };
                                
                                attendees.push(obj);

                                resultado.push(objRes);
                            }

                            console.log(attendees);
                            
                            var patch_event = {
                                'attendees': attendees
                            };
                            
                            return gapi.client.calendar.events.patch({
                                'calendarId': calendario_google_id,
                                'eventId': evento_google_id,
                                'sendNotifications': true,
                                'sendUpdates': 'all',
                                'resource': patch_event
                            })
                            .then(function(response) {
                                console.log(response);

                                //unblock();

                                axios({
                                    method: 'post',
                                    url: '/agenda/api/v1/eventospersonas',
                                    data: resultado
                                })
                                .then(function(res) {
                                    getCompartidos();
                                    
                                    unblock();

                                    swal({
                                        type: 'success',
                                        title: '' + 'Ok',
                                        html: 'Evento o gira compartido con exito.',
                                        confirmButtonText: 'Cerrar',
                                        allowEscapeKey: false,
                                        allowOutsideClick: false
                                    });
                                })
                                .catch(function(err) {
                                    unblock();
                                });
                            },
                            function(error) {
                                console.log(error);
                                unblock();
                            });

                        },
                        function(error) {
                            unblock();
                            console.log(error);
                        });

                        /*console.log(response);
                        area_id = undefined;
                        $('#area_id').removeAttr('disabled');
                        $('#btnAgregar').text("Agregar");
                        $('#btnAgregar').css('background-color','##00c0ef');
                        $('#btnAgregar').css('border-color', '##00c0ef');
                        $('#area_id').val(null).trigger('change');
                        $('#observaciones').val('');
                        areas.ajax.reload(null, false);*/
                    }
                });
            }
        }

        var eliminar_area = (area, nombre) => {
            swal({
                title: '¿Desea eliminar el área?',
                text: nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar',
                cancelButtonText: 'No, regresar',
                reverseButtons: true
            }).then((result) => {
                if(result.value) {
                    url = '/agenda/eventos/' + evento_id + '/areas/' + area;
                    app.ajaxHelper(url, 'DELETE').done( (response) => {
                        if(!response.success) {
                            swal(
                                '¡Error!',
                                response.message,
                                'error'
                            );
                        }
                        else {
                            areas.ajax.reload(null, false);
                            swal(
                                '¡Correcto!',
                                response.message,
                                'success'
                            );
                        }
                    });
                }
            });           
        }

        var editar_area = (area, nombre_area, observaciones) => {
            var areaOpt = new Option(nombre_area, area, true, true);
            area_id = area;
            $('#observaciones').val( observaciones === 'null' ? '' : observaciones );
            $('#area_id').append(areaOpt).trigger('change');
            $('#area_id').attr('disabled', 'disabled');
            $('#btnAgregar').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');
            $('#area_id').valid();
        }

        var get_evento_id = () => {
            return evento_id;
        }

        return {
            get_evento_id,
            get_giras,
            crear_o_editar,
            eliminar,
            agregar_area,
            eliminar_area,
            editar_area,
            guardar,
            handleClientLoad,
            compartir
        }

    })();
</script>