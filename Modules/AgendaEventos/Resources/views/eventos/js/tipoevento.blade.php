<script type="text/javascript">  
    var TipoEvento = (() => {
        
        var form_tipo;        
        var tipoevento_id = undefined;        
        var calendario_id = undefined;
        var tipos;
        var form_compartir;
        var google_calendario_id = undefined;

        var handleClientLoad = () => {
            gapi.load('client:auth2', initClient);
        }

        var initClient = () => {            
            gapi.client.init({
                apiKey: 'AIzaSyC9u0yWqGY5cXp0jqzX_snVN1YiriTHxUg',
                clientId: '489363224492-tob8tfhthj23q4vf21fss3gobbmamuig.apps.googleusercontent.com',
                discoveryDocs: ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"],
                scope: "https://www.googleapis.com/auth/calendar.events https://www.googleapis.com/auth/calendar"
            }).then( () => {                
            }, (error) => {
                console.log('Error', JSON.stringify(error, null, 2) );
            });
        }
        
        /* var  verificarUsuario = new Promise ((resolve, reject) => {
            if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
                if (gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail() != 'agendadifoaxaca@gmail.com'){
                    swal(
                        '¡Sesión cerrada!',
                        'Esta cuenta no está autorizada para gestionar Calendarios de Google',
                        'error'
                    );
                    gapi.auth2.getAuthInstance().signOut();
                    resolve( false );
                }else {
                    resolve( true );
                }
            }else {
                gapi.auth2.getAuthInstance().signIn().then(function(response){
                    if (gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail() != 'agendadifoaxaca@gmail.com'){
                    swal(
                        '¡Sesión cerrada!',
                        'Esta cuenta no está autorizada para gestionar Calendarios de Google',
                        'error'
                    );
                    gapi.auth2.getAuthInstance().signOut();
                    resolve( false );
                }else {
                    resolve( true );
                }
                });
            }
        }); */
        
        var verificarUsuario = () => {
            if (gapi.auth2.getAuthInstance().isSignedIn.get()) {
                if (gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail() != 'agendadifoaxaca@gmail.com'){
                    swal(
                        '¡Sesión cerrada!',
                        'Esta cuenta no está autorizada para gestionar Calendarios de Google',
                        'error'
                    );
                    gapi.auth2.getAuthInstance().signOut();
                    return false;
                }else {
                    return true;
                }
            }else {
                gapi.auth2.getAuthInstance().signIn().then(function(response){
                    if (gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail() != 'agendadifoaxaca@gmail.com'){
                    swal(
                        '¡Sesión cerrada!',
                        'Esta cuenta no está autorizada para gestionar Calendarios de Google',
                        'error'
                    );
                    gapi.auth2.getAuthInstance().signOut();
                    return false;
                }else {
                    return true;
                }
                });
            }
        };

        var get_tipos = () => {
            app.to_upper_case();            
            tipos = $('#tipos').DataTable();
            $('#buscar').keypress(function(e) {
			    if(e.which === 13) {
				    tipos.search($('#buscar').val()).draw();
			    }
		    });
		    $('#btn_buscar').on('click', function() {
			    tipos.search($('#buscar').val()).draw();
            });
        }
        
        var inicializar = () => {
            $('#modal-tipo').on('hidden.bs.modal', (e) => {
                tipoevento_id = undefined;
            });            

            form_tipo = $('#form_tipo');            
            form_compartir = $('#form_compartir');  
			agregar_validacion();
            app.to_upper_case();            
        }

        var agregar_validacion = () => {
            form_tipo.validate({
                rules: {
                    nombre: {
                        required: true
                    },
                    descripcion: {
                        required: false
                    }
                }
            });

            form_compartir.validate({
                rules: {
                    persona_id: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });  
        };
        
        var crearCalendario = (url, metodo) => {
            gapi.client.calendar.calendarList.list().execute((data) => {
                var calendarios = data.items.filter( calendario => {
                    return calendario.summary.toLowerCase() == $('#nombre').val().toLowerCase();
                });
                if(calendarios.length > 0) {
                    swal(
                        '¡Error!',
                        'Ya se encuentra registrado un calendario con ese nombre',
                        'error'
                    );
                }else {
                    block();
                    gapi.client.calendar.calendars.insert({
                        summary: $('#nombre').val(),
                        description: $('#descripcion').val()
                    }).execute((data) => {
                        app.ajaxHelper( url, metodo, form_tipo.serialize() + '&id_calendario_google=' + data.id ).done( (response) => {
                            if(!response.success) {
                                swal(
                                    '¡Error!',
                                    response.message,
                                    'error'
                                );
                                gapi.client.calendar.calendars.delete({
                                    calendarId: data.id
                                }).then( () => {
                                    unblock();
                                });
                            } else {
                                tipoevento_id = response.tipoevento.id;
                                tipos.ajax.reload(null, false);
                                $('#modal-tipo').modal('hide');
                                unblock();
                                swal(
                                    '¡Correcto!',
                                    'Calendario creado',
                                    'success'
                                );
                            }
                        });
                    });
                }
            });
        }

        var editarCalendario = (url, metodo) => {
            gapi.client.calendar.calendarList.list().execute((data) => {
                var calendarioActual = data.items.filter( calendario => {
                    return calendario_id == calendario.id;
                })[0];
                var calendarios = data.items.filter( calendario => {
                    return (calendario.summary.toLowerCase() == $('#nombre').val().toLowerCase() && calendario_id != calendario.id);
                });
                
                if(calendarios.length > 0) {
                    swal(
                        '¡Error!',
                        'Ya se encuentra registrado un calendario con ese nombre',
                        'error'
                    );
                }else {
                    block();
                    gapi.client.calendar.calendars.update({
                        calendarId: calendario_id,
                        summary: $('#nombre').val(),
                        description: $('#descripcion').val()
                    }).execute((data) => {
                        app.ajaxHelper( url, metodo, form_tipo.serialize()).done( (response) => {
                            if(!response.success) {
                                swal(
                                    '¡Error!',
                                    response.message,
                                    'error'
                                );
                                gapi.client.calendar.calendars.update({
                                    calendarId: calendario_id,
                                    summary: calendarioActual.summary,
                                    description: calendarioActual.description
                                }).then( () => {
                                    unblock();
                                });
                            } else {
                                tipos.ajax.reload(null, false);
                                $('#modal-tipo').modal('hide');
                                unblock();
                                swal(
                                    '¡Correcto!',
                                    'Calendario actualizado',
                                    'success'
                                );
                            }
                        });
                    });
                }
            });
        }

        var compartirCalendario = (c_calendario_id, nombre, c_google_calendario_id) => {
            if(gapi.auth2.getAuthInstance().isSignedIn.get()) {
                calendario_id = c_calendario_id;
                google_calendario_id = c_google_calendario_id;
                inicializar();
                oTable.ajax.url('/agenda/api/v1/calendariospersonas' + '?calendario_id=' + calendario_id).load();
                $('#modal_compartir').modal('show');
            }
            else {
                verificarUsuario()
            }
        }
        
        var compartir = () => {
            if(form_compartir.valid()) {
                var role = '', tipo = 'user';
                
                switch($('#rol').val()) {
                    case '1':
                        role = 'none';
                        break;
                    case '2':
                        role = 'freeBusyReader';
                        break;
                    case '3':
                        role = 'reader';
                        break;
                    case '4':
                        role = 'writer';
                        break;
                    case '5':
                        role = 'owner';
                        break;
                    default:
                        role = 'none';
                }

                /*switch($('#tipo').val()) {
                    case '1':
                        tipo = 'default';
                        break;
                    case '2':
                        tipo = 'user';
                        break;
                    case '3':
                        tipo = 'group';
                        break;
                    case '4':
                        tipo = 'domain';
                        break;
                    default:
                        tipo = 'default';
                }*/

                var formData = new FormData($('#form_compartir')[0]);
                
                block();

                return gapi.client.calendar.acl.insert({
                    "calendarId": google_calendario_id,
                    "resource": {
                        "role": role,
                        "scope": {
                            "type": tipo,
                            "value": $('#email').val()
                        }
                    }
                })
                .then(function(response) {
                    
                    var resultado = {
                        calendario_id: calendario_id,
                        persona_id: $("#persona_id").val(),
                        nombre: $("#nombre_persona").val(),
                        primer_apellido: $("#primer_apellido").val(),
                        segundo_apellido: $("#segundo_apellido").val(),
                        kind: response.result.kind,
                        etag: response.result.etag,
                        ruleid: response.result.id,
                        scope_type: response.result.scope.type,
                        scope_value: response.result.scope.value,
                        role: response.result.role,
                        correo: $('#email').val()
                    };
                    
                    axios({
                        method: 'post',
                        url: '/agenda/api/v1/calendariospersonas',
                        data: resultado
                    })
                    .then(function(res) {
                        console.log(res);
                        
                        $('#calendario_personas').DataTable().ajax.reload(null, false);

                        unblock();

                        swal({
                            type: 'success',
                            title: 'Guardado',
                            html: 'se compartió correctamente con '+response.result.scope.value,
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    })
                    .catch(function(err) {
                        unblock();
                    });

                    //console.log("Response", response);

                    //$('#modal_compartir').modal('hide');
                },
                function(error) {
                });
            }
        }
        
        var guardar = () => {
            if(form_tipo.valid()) {
                var url = tipoevento_id ? ('/agenda/calendarios/' + tipoevento_id) : ('/agenda/calendarios');
                var metodo = tipoevento_id ? 'PUT' : 'POST';                
                if(metodo == 'POST') {
                    crearCalendario(url, metodo);
                } else {
                    editarCalendario(url, metodo);
                }
            }
		}        

        var crear_o_editar = (tipo, nombre, calendario) => {
            if(gapi.auth2.getAuthInstance().isSignedIn.get()) {
                tipoevento_id = tipo;
                calendario_id = calendario;
                var url = tipo ? '{{ route("agenda.calendarios.edit", [":tipo"]) }}'.replace(':tipo', tipoevento_id) : "{{ route('agenda.calendarios.create') }}";
                app.ajaxHelper( url, 'GET').done( (response) => {
                    $('#modal-body-tipo').html(response.html);
                    $('#modal-tipo-title').text(nombre || 'Registrar tipo de evento');
                    inicializar();
                    $('#modal-tipo').modal('show');
                });                
            } else {
                verificarUsuario();
            }
        }

        var eliminar = (tipo, nombre, calendario) => {
            if(gapi.auth2.getAuthInstance().isSignedIn.get()) {
                swal({
                    title: '¿Desea eliminar el calendario y todos sus eventos?',
                    html: nombre,
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, eliminar',
                    cancelButtonText: 'No, regresar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        var url = '/agenda/calendarios/' + tipo;
                        app.ajaxHelper( url, 'DELETE').done( (response) => {
                            unblock();
                            if(!response.success){
                                swal(
                                    '¡Error!',
                                    response.message,
                                    'error'
                                );
                            } else {
                                gapi.client.calendar.calendars.delete({
                                    calendarId: calendario
                                }).execute(() => {
                                    tipos.ajax.reload(null, false);
                                    swal(
                                        '¡Correcto!',
                                        'Calendario eliminado',
                                        'success'
                                    );                            
                                });
                            }
                        });
                    }
                }); 
            } else {
                verificarUsuario();
            }
        }

        var get_calendario_id = () => {
            //console.log(calendario_id);
            return calendario_id;
        }

        return {
            get_tipos,
            crear_o_editar,
            eliminar,            
            guardar,
            handleClientLoad,
            compartirCalendario,
            compartir,
            get_calendario_id
        }

    })();
</script>