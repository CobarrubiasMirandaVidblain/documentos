@extends('agendaeventos::layouts.master')

@push('head')
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
  <style>
        #btnEjercicio:hover, .button-dt.tool.dropdown-toggle:hover a{
            color: white !important;
        }

        ul#menuEjercicio a {
            color: black !important;
        }
  </style>
@endpush
@section('content-title', 'INVERSIÓN EN PROGRAMAS')
@section('content-subtitle', '')

@section('content')

<div class="box box-primary shadow">   

	<div class="box-header with-border">
		<h3 class="box-title">Datos del municipio</h3>
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="table-responsive">
					<table class="table">
						<tr>
							<th style="width:30%">
								<i class="fa fa-id-card margin-r-5"></i>
								Municipio:
							</th>
							<td>{{ $municipio->nombre }}</td>
						</tr>
						<tr>
							<th>
								<i class="fa fa-calendar margin-r-5"></i>
								Distrito:
							</th>
							<td>{{ $municipio->distrito->nombre }}</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="table-responsive">
					<table class="table">
						<tr>
							<th style="width:30%; color: white;">
								No visible
							</th>
							<td  style="color: white;">No visible</td>
						</tr>
						<tr>
							<th>
								<i class="fa fa-university margin-r-5"></i>
								Región:
							</th>
							<td>{{ $municipio->distrito->region->nombre }}</td>
						</tr>                    
					</table>
				</div>
			</div>
		</div>

		{{-- <div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<form class="form-horizontal">
					<div class="row">
						<label class="col-sm-2 control-label" style="text-align: left;">Municipio:</label>
						<div class="col-sm-10">
							<p class="form-control-static">
								<select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;"></select>
							</p>
						</div>
					</div>
					<div class="row">
						<label class="col-sm-2 control-label" style="text-align: left;">Rubro:</label>
						<div class="col-sm-6">
							<p class="form-control-static">
								<select id="rubro_id" name="rubro_id" class="form-control select2" style="width: 100%;"></select>
							</p>
						</div>						
					</div>
					<div class="row">
						<label class="col-sm-2 control-label" style="text-align: left;">Año:</label>
						<div class="col-sm-2">
							<p class="form-control-static">
								<select id="ejercicio_id" name="ejercicio_id" class="form-control select2" style="width: 100%;"></select>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div> --}}

		<div class="row">
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>
		</div>
		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'inversiones', 'name' => 'inversiones', 'style' => 'width: 100%']) !!}
	</div>
</div>
@stop

@push('body')

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">

  $('#municipio_id').select2({
		language: 'es',
		minimumInputLength: 2,
		placeholder : 'SELECCIONE EL MUNICIPIO',
		ajax: {
			url: '{{ route('municipios.select') }}',
			delay: 500,
			dataType: 'JSON',
			type: 'GET',
			data: (params) => {
				return {
					search: params.term
				};
			},
			processResults: (data, params) => {
				params.page = params.page || 1;
				return {
					results: $.map(data, (item) => {
						return {
							id: item.id,
							text: item.nombre,
							slug: item.nombre,
							results: item
						}
					})
				};
			},
			cache: true
		}
	});

	$('#rubro_id').select2({
		language: 'es',
		minimumResultsForSearch: Infinity,
		placeholder : 'SELECCIONE EL RUBRO'
	});

	$('#ejercicio_id').select2({
		language: 'es',
		minimumResultsForSearch: Infinity,
		placeholder : 'SELECCIONE EL AÑO'
	});

	$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
  	var ejercicio;
	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

    function filtrar(columna, valor){
      tabla.DataTable().columns(columna).search(valor).draw();
    }

		return {
			init: init,
      filtrar: filtrar,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#inversiones').dataTable());
	table.init();	
</script>
@endpush