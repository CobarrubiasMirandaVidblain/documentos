@extends('agendaeventos::layouts.master')

@push('head')
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-title', 'Agenda de giras y eventos')
@section('content-subtitle', '')

@section('li-breadcrumbs')
<li class="active">Autoridades Municipales</li>
@endsection

@section('content')
    <section class="content">
        <div class="box box-primary shadow">
            <div class="box-header with-border">
                <h3 class="box-title">Autoridades Municipales</h3>
            </div>
            <div class="box-body">

              <form id="form" role="form">

                <div class="form-group">
                  <label for="nombre">Nombre</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                </div>

                <div class="form-group">
                  <label for="primer_apellido">Primer Apellido</label>
                  <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" placeholder="Primer Apellido">
                </div>

                <div class="form-group">
                  <label for="segundo_apellido">Segundo Apellido</label>
                  <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" placeholder="Segundo Apellido">
                </div>

                <div class="form-group">
                  <label for="cargo_id">Cargo* :</label>
                  <select class="form-control select2" id="cargo_id" name="cargo_id" style="width: 100%;">
                  </select>
                </div>

                <div class="form-group">
                  <label for="telefono_particular">Telefono Particular</label>
                  <input type="text" class="form-control" id="telefono_particular" name="telefono_particular" placeholder="Telefono Particular">
                </div>

                <div class="form-group">
                  <label for="telefono_fijo">Telefono Fijo</label>
                  <input type="text" class="form-control" id="telefono_fijo" name="telefono_fijo" placeholder="Telefono Fijo">
                </div>

                <div class="form-group">
                  <label for="mayoria_relativa">Mayoria Relativa</label>
                  <input type="text" class="form-control" id="mayoria_relativa" name="mayoria_relativa" placeholder="Mayoria Relativa">
                </div>

                <div class="form-group">
                  <label for="periodo">Periodo</label>
                  <input type="text" class="form-control" id="periodo" name="periodo" placeholder="Periodo">
                </div>

                <div class="form-group">
                  <label for="municipio_id">Municipio* :</label>
                  <select class="form-control select2" id="municipio_id" name="municipio_id" style="width: 100%;">
                  </select>
                </div>

              </form>

              <div class="box-footer">
                <button type="button" class="btn btn-primary" onclick="guardar();">Guardar</button>
              </div>

            </div>
        </div>
    </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script>
  $('#cargo_id').select2({
    language: 'es',
    tags: true,
    placeholder : 'Seleccione El Cargo',
    ajax: {
      url: "{{ route('cargos.select') }}",
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: params => {
        return {
          search: params.term
        };
      },
      processResults: (data, params) => {
        params.page = params.page || 1;
        return {
          results: $.map(data, item => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        };
      },
      cache: true
    }
    })
    .change(evt => {});

    $('#municipio_id').select2({
      language: 'es',
      placeholder : 'Seleccione El Municipio',
      minimumInputLength: 2,
      ajax: {
        url: '{{ route('municipios.select') }}',
        delay: 500,
        dataType: 'JSON',
        type: 'GET',
        data: function(params) {
          return {
            search: params.term
          };
        },
        processResults: function(data, params) {
          params.page = params.page || 1;
          return {
            results: $.map(data, function(item) {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    })
    .change(function(event) {
    });

    var guardar = function() {
      var formData = new FormData($('#form')[0]);

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: '/agenda/autoridadesmunicipios/create_edit',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
          alert('Success.');
        },
        error: function(response) {
          alert('Error.');
        }
      });
    }
</script>
@endpush
