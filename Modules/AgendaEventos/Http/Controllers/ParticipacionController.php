<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class ParticipacionController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('agendaeventos::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agendaeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
    
    public function select(Request $request) {
        if($request->ajax())
        {
        
            $model = DB::table('even_cat_participaciones')
            ->select([
                'even_cat_participaciones.nombre'
            ])
            ->where('even_cat_participaciones.nombre', 'LIKE', '%' . $request->input('search') . '%');

            $model = $model
            ->take(10)
            ->get()
            ->toArray();

            return response()->json($model);
        }
    }
}