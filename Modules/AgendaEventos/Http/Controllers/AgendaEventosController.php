<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\DataTables\AgendaEventos\GirasDataTable;
use App\DataTables\InversionesProgramaDataTable;
use App\Models\Modulo;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AgendaEventos\Entities\Gira;
use Modules\AgendaEventos\Entities\TipoEvento;

use Modules\AgendaEventos\DataTables\PersonasDataTable;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class AgendaEventosController extends Controller {

	public function __construct(){
		$this->middleware(['auth', 'authorized']);
		$this->middleware(function (Request $request, $next) {
			$modulito = Modulo::where('nombre', 'AGENDA DE EVENTOS')->first();
			$request['modulo_id'] = $modulito ? $modulito->id : '';
			return $next($request);
		});
	}

    public function index(PersonasDataTable $personas, GirasDataTable $dataTable) {
        /*$connection = 'sqlsrv';

        $sql = "SELECT TOP 100 * FROM BdSICOPE.dbo.v_Peticiones";
        
        $results = DB::connection($connection)
        ->select(DB::raw($sql));
        return $results;*/

        //return Auth::user()->persona->empleado->area;
        $personas->with('tipo', 'modal_personas');

        $tipos = TipoEvento::where('id', '!=', 0)->whereNotNull('id_calendario_google')->get();

        $colores = ['grey','orange','blue','red','aero','pink','green','yellow','purple'];

        foreach($tipos as $tipo){
            $tipo['color'] = array_random($colores);
        }

        if(request()->get('table') === 'personas') {
            return $personas->render('agendaeventos::index', compact('personas', 'dataTable', 'tipos'));
        }
        else {
            return $dataTable->render('agendaeventos::index', compact('personas', 'dataTable', 'tipos'));
        }
    }

    public function search(Request $request) {
        return Gira::where('nombre','like','%'.$request['search'].'%')->take(10)->get();
    }

    public function inversiones($id, InversionesProgramaDataTable $dataTable){
		    return $dataTable        
            ->with('municipio_id', $id)
            ->render('agendaeventos::inversiones', ['municipio' => Municipio::find($id)]);		
    }
    
    public function excelEvento(Request $request) {
        if($request->has('even_cat_tiposevento_id')) {
            return
                DB::select("SELECT
                te.nombre AS tipo_evento,
                g.nombre AS nombre_gira,
                r.nombre AS region,
                d.nombre AS distrito,
                m.nombre AS municipio,
                ti.nombre AS tipo_institucion,
                i.nombre AS institucion,
                fi.numero_ficha,
                p.nombre AS producto,
                e.fecha,
                a.cantidad,
                a.monto
            FROM
                even_aportaciones a,
                even_fichasinfo fi,
                even_cat_tiposevento te,
                even_eventos e,
                atnc_cat_giras g,
                cat_instituciones i,
                cat_tiposinstitucion ti,
                even_cat_productos p,
                cat_municipios m,
                cat_distritos d,
                cat_regiones r
            WHERE
                a.fichainfo_id = fi.id
            AND a.dependencia_id = i.id
            AND a.producto_id = p.id
            AND i.tipoinstitucion_id = ti.id
            AND e.tipoevento_id = te.id
            AND fi.evento_id = e.id
            AND e.gira_id = g.id
            AND e.municipio_id = m.id
            AND m.distrito_id = d.id
            AND d.region_id = r.id
            AND g.deleted_at IS NULL
            AND e.deleted_at IS NULL
            AND te.id = $request->even_cat_tiposevento_id
            ORDER BY
                m.nombre,
                i.nombre,
                ti.nombre");
        }

        if($request->has('evento_id')) {
            return
                DB::select("SELECT
                te.nombre AS tipo_evento,
                g.nombre AS nombre_gira,
                r.nombre AS region,
                d.nombre AS distrito,
                m.nombre AS municipio,
                ti.nombre AS tipo_institucion,
                i.nombre AS institucion,
                fi.numero_ficha,
                p.nombre AS producto,
                e.fecha,
                a.cantidad,
                a.monto
            FROM
                even_aportaciones a,
                even_fichasinfo fi,
                even_cat_tiposevento te,
                even_eventos e,
                atnc_cat_giras g,
                cat_instituciones i,
                cat_tiposinstitucion ti,
                even_cat_productos p,
                cat_municipios m,
                cat_distritos d,
                cat_regiones r
            WHERE
                a.fichainfo_id = fi.id
            AND a.dependencia_id = i.id
            AND a.producto_id = p.id
            AND i.tipoinstitucion_id = ti.id
            AND e.tipoevento_id = te.id
            AND fi.evento_id = e.id
            AND e.gira_id = g.id
            AND e.municipio_id = m.id
            AND m.distrito_id = d.id
            AND d.region_id = r.id
            AND g.deleted_at IS NULL
            AND e.deleted_at IS NULL
            AND e.id = $request->evento_id
            ORDER BY
                m.nombre,
                i.nombre,
                ti.nombre");
        }

        return
            DB::select("SELECT
            te.nombre AS tipo_evento,
            g.nombre AS nombre_gira,
            r.nombre AS region,
            d.nombre AS distrito,
            m.nombre AS municipio,
            ti.nombre AS tipo_institucion,
            i.nombre AS institucion,
            fi.numero_ficha,
            p.nombre AS producto,
            e.fecha,
            a.cantidad,
            a.monto
        FROM
            even_aportaciones a,
            even_fichasinfo fi,
            even_cat_tiposevento te,
            even_eventos e,
            atnc_cat_giras g,
            cat_instituciones i,
            cat_tiposinstitucion ti,
            even_cat_productos p,
            cat_municipios m,
            cat_distritos d,
            cat_regiones r
        WHERE
            a.fichainfo_id = fi.id
        AND a.dependencia_id = i.id
        AND a.producto_id = p.id
        AND i.tipoinstitucion_id = ti.id
        AND e.tipoevento_id = te.id
        AND fi.evento_id = e.id
        AND e.gira_id = g.id
        AND e.municipio_id = m.id
        AND m.distrito_id = d.id
        AND d.region_id = r.id
        AND g.deleted_at IS NULL
        AND e.deleted_at IS NULL
        ORDER BY
            m.nombre,
            i.nombre,
            ti.nombre");
		}
		
		public function excelPeticion(Request $request) {
			$selectString = "SELECT
			p.folio AS folio,
			p.CP_AÑO AS anio,
			p.Region AS region,
			p.Municipio AS municipio,
			p.Localidad AS localidad,
			p.cp_solicitante AS solicitante,
			p.cp_cargo_sol AS cargo_solicitante,
			p.cp_asunto AS asunto,
			p.producto AS producto,
			p.cp_cantidad AS cantidad,
			p.recibido_en AS fecha_recibido,
			b.Cp_Curp AS curp,
			b.Cp_Rfc AS rfc,
			b.Cp_Sexo AS genero,
			p.Direccion AS direccion,
			p.DepExterna AS dependencia_externa,
			p.cp_st_seguimiento AS estado,
			p.Clave_Municipio AS municipio_id
	FROM
		BdSICOPE.dbo.v_Peticiones AS p
		LEFT JOIN BdSICOPE.dbo.v_Beneficiarios AS b on b.cp_id_peticion = p.cp_id
	WHERE
		p.CP_AÑO IN (2019, 2018, 2017)";
		
		if($request->anio !== NULL) {
			$selectString = $selectString . " AND p.CP_AÑO = " . $request->anio;
		}

		if($request->estado !== NULL) {
			$selectString = $selectString . " AND p.cp_st_seguimiento = '" . $request->estado . "'";
		}

		if($request->producto !== NULL) {
			$selectString = $selectString . " AND p.producto LIKE '%" . $request->producto . "%'";
		}

		if($request->municipio_id !== NULL) {
			$selectString = $selectString . " AND p.Clave_Municipio = '" . $request->municipio_id . "'";
		}
		
		return
		DB::connection('sqlsrv')
		->select($selectString);
	}
}