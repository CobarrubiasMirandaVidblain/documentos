<?php

namespace Modules\AgendaEventos\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use Modules\AgendaEventos\Entities\CalendarioPersona;

use Yajra\Datatables\Datatables;

class CalendarioPersonaController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $calendario_id = $request->calendario_id;

        if($calendario_id === 'undefined') {
            $calendario_id = 0;
        }
        
        $calendarioPersona = DB::select(DB::raw("SELECT
            ecp.id id,
        
        IF (
            ecp.persona_id IS NULL,
            ecp.nombre,
            p.nombre
        ) nombre,
        
        IF (
            ecp.persona_id IS NULL,
            ecp.primer_apellido,
            p.primer_apellido
        ) primer_apellido,
        
        IF (
            ecp.persona_id IS NULL,
            ecp.segundo_apellido,
            p.segundo_apellido
        ) segundo_apellido,
        ecp.correo correo
        FROM
            even_calendarios_personas ecp
        LEFT JOIN personas p ON ecp.persona_id = p.id WHERE ecp.calendario_id = $calendario_id"));

        return Datatables::of($calendarioPersona)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agendaeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            
            $data = $request->except('calendario_id', 'correo');

            $data['usuario_id'] = auth()->user()->id;

            $calendarioPersona = CalendarioPersona::firstOrCreate(
                [
                    'calendario_id' => $request->calendario_id,
                    'correo' => $request->correo
                ],
                $data
            );

            if($calendarioPersona->wasRecentlyCreated) {
            }
            else {
                $calendarioPersona->update($data);
            }
            
            DB::commit();
            
            return response()->json(['success' => ['code' => 200, 'message' => 'OK', 'id' => $calendarioPersona->id, 'calendarioPersona' => $calendarioPersona]], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();

            return $e->getMessage();
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $calendarioPersona = CalendarioPersona::find($id);

            if(!$calendarioPersona) {
                return response()->json(['error' => ['code' => 404, 'message' => 'Not Found', 'id' => $id]], 404);
            }
            
            $calendarioPersona->update($request->all());

            DB::commit();
            
            return response()->json(['success' => ['code' => 200, 'message' => 'OK', 'id' => $id, 'calendarioPersona' => $calendarioPersona]], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}