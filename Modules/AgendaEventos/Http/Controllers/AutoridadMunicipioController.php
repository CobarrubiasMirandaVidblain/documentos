<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AgendaEventos\Entities\AutoridadMunicipio;
use Modules\AgendaEventos\Entities\Ficha;
use Modules\AgendaEventos\Entities\PresidenteMunicipal;

class AutoridadMunicipioController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        return AutoridadMunicipio::
        select([
            'even_autoridadesmuni.id as id',
            'even_autoridadesmuni.fichainfo_id as fichainfo_id',
            'presidentesmunicipales.nombre as nombre',
            'presidentesmunicipales.primer_apellido as primer_apellido',
            'presidentesmunicipales.segundo_apellido as segundo_apellido',
            'cat_cargos.nombre as cargo',
            'presidentesmunicipales.mayoria_relativa as siglas'
        ])

        ->join('presidentesmunicipales', 'presidentesmunicipales.id', 'even_autoridadesmuni.presidentemunicipal_id')
        ->join('cat_cargos', 'cat_cargos.id', 'presidentesmunicipales.cargo_id')

        ->where('even_autoridadesmuni.fichainfo_id', $request['fichainfo_id'])
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
      return view('agendaeventos::autoridadesmunicipales.create_edit');
    }

    public function create_edit(Request $request) {
      $request['usuario_id'] = auth()->user()->id;

      $presidenteMunicipal = PresidenteMunicipal::create($request->all());

      return $presidenteMunicipal;
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $request['usuario_id'] = auth()->user()->id;

            if(AutoridadMunicipio::where('fichainfo_id', $request['fichainfo_id'])->where('presidentemunicipal_id', $request['presidentemunicipal_id'])->count() > 0) {
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra registrad(o)a esta autoridad municipal.'));
            }

            $autoridad = AutoridadMunicipio::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'autoridad' => $autoridad));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
		$autoridad = AutoridadMunicipio::find($id);
        if(!$autoridad) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $autoridad->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Autoridad eliminada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    public function cargar(Request $request) {
        $ficha = Ficha::where('id', $request['fichainfo_id'])->get();
        $presidentes = PresidenteMunicipal::where('municipio_id', $ficha[0]->evento->municipio->id)->get();
        if(!$presidentes) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            for($i = 0; $i < count($presidentes); $i++) {
                if(AutoridadMunicipio::where('fichainfo_id', $request['fichainfo_id'])->where('presidentemunicipal_id', $presidentes[$i]->id)->count() === 0) {
                    $autoridad = AutoridadMunicipio::create([
                        'fichainfo_id' => $request['fichainfo_id'],
                        'presidentemunicipal_id' => $presidentes[$i]->id,
                        'usuario_id' => auth()->user()->id
                    ]);
                }
            }
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Autoridad cargada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
