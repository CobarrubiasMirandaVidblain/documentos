<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\DataTables\AgendaEventos\VestimentaDataTable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\Vestimenta;
use View;

class VestimentaController extends Controller {
    
    public function index(VestimentaDataTable $dataTable) {
        return $dataTable->render('agendaeventos::vestimenta.index');
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();

            if(Vestimenta::where([
                ['nombre', $request['nombre']],
                ['codigo', $request['codigo']]
            ])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra un registro con el mismo nombre y código.'));
            }

            $vestimenta = Vestimenta::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'vestimenta' => $vestimenta));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    public function update($id, Request $request) {
        $vestimenta = Vestimenta::findOrFail($id);
        if(!$vestimenta){
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }

        try {
            DB::beginTransaction();

            if(Vestimenta::where([
                ['nombre', $request['nombre']],
                ['codigo', $request['codigo']],
                ['id', '!=', $id]                            
            ])->count() > 0) {
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra un registro con el mismo nombre y código.'));
            }

            $vestimenta = $vestimenta->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'vestimenta' => $vestimenta));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
    
    public function destroy($id) {
        $vestimenta = Vestimenta::find($id);
        if(!$vestimenta) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $vestimenta->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Vestimenta eliminada exitosamente.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
