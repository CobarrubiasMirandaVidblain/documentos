<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\Models\Cargo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\Ficha;
use Modules\AgendaEventos\Entities\Asistente;

class AsistenteController extends Controller {
    
    public function index(Request $request) {
        return Asistente::search()->where('even_asistentes.fichainfo_id', $request['fichainfo_id'])->get();
    }

    public function store(Request $request) {
		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['persona_id'] = $request->input('asistente_id');
      		$request['cargo_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_asistente'))])->id;      		

            if(Asistente::where('fichainfo_id', $request['fichainfo_id'])->where('persona_id', $request['persona_id'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Esta persona ya se encuentra registrada como aistente para ese evento.'));
			}
			
			if(Asistente::where('fichainfo_id', $request['fichainfo_id'])->where('persona_id', '!=', $request['persona_id'])->where('posicion', $request['posicion'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'La posición ' . $request['posicion'] . ' ya se encuentra ocupada en la fila de honor.'));
			}

            $asistente = Asistente::create($request->all());

            $ficha = Ficha::findOrFail($request['fichainfo_id']);

            if($ficha->asistentes->count() > 0 && $ficha->aportaciones->count() > 0) {
                $ficha->evento->update(['estado_modulo_id' => 3]);
            }
            else {
                $ficha->evento->update(['estado_modulo_id' => 2]);
            }

            DB::commit();
            return response()->json(array('success' => true, 'asistente' => $asistente));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function update($asistente_id, Request $request) {
		$asistente = Asistente::findOrFail($asistente_id);

		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['persona_id'] = $request->input('asistente_id');
      		$request['cargo_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_asistente'))])->id;
			
			if(Asistente::where('fichainfo_id', $asistente->fichainfo_id)->where('persona_id', '!=', $request['persona_id'])->where('posicion', $request['posicion'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'La posición ' . $request['posicion'] . ' ya se encuentra ocupada en la fila de honor.'));
			}

            $asistente->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'asistente' => $asistente));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function destroy($id) {
		$asistente = Asistente::find($id);
        if(!$asistente) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();

            $request['fichainfo_id'] = $asistente->fichainfo_id;

            $asistente->delete();

            $ficha = Ficha::findOrFail($request['fichainfo_id']);

            if($ficha->asistentes->count() > 0 && $ficha->aportaciones->count() > 0) {
                $ficha->evento->update(['estado_modulo_id' => 3]);
            }
            else {
                $ficha->evento->update(['estado_modulo_id' => 2]);
            }

            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Asistente eliminado.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
