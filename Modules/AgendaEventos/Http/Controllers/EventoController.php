<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\Models\Modulo;
use Modules\AgendaEventos\Entities\Evento;
use Modules\AgendaEventos\Entities\Gira;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use View;

use App\DataTables\AtnCiudadana\BeneficiariosgiraDataTable;
use App\DataTables\AtnCiudadana\SolicitantesGiraDataTable;

class EventoController extends Controller {
    
    public function __construct() {
        //$this->middleware(['auth', 'authorized']);
        $modulito = Modulo::where('nombre', 'AGENDA DE EVENTOS')->first();

        if($modulito)//revisen sus cosas ¬¬
            $this->middleware("rolModule:$modulito->id,ADMINISTRADOR DE EVENTOS", ['except' => 'index']);
	}

    //Test:
    //Fecha:
    public function index(Request $request) {
    	if($request->has('tipos') && $request->has('start') && $request->has('end')) {
            return Evento::
                search()
                ->whereNull('atnc_cat_giras.deleted_at')
                ->whereIn('tipoevento_id', json_decode($request->tipos))
                ->whereBetween('fecha', [$request->start, $request->end])
                ->orderBy('even_eventos.fecha','desc')
                ->get();
        }
        
        return Evento::
        search()
        ->orderBy('even_eventos.fecha','desc')
        ->whereNull('atnc_cat_giras.deleted_at')
        ->get();
    }

    //Test: OK 
    //Fecha: 05/12/2018
    public function create() {
        return response()->json([
            'html' =>  view::make('agendaeventos::eventos.create_edit')->render()
        ], 200);
    }

    //Test: OK 
    //Fecha: 05/12/2018
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $nombre = strtoupper($request['nombre']);
            $nombre = str_replace('"', "", $nombre);
            $nombre = str_replace("'", "", $nombre);

            $request['fecha'] = (\DateTime::createFromFormat('d/m/Y', $request['fecha']))->format('Y-m-d');
            $request['gira_id'] = Gira::firstOrCreate(['nombre' => $nombre])->id;
            $request['usuario_id'] = auth()->user()->id;

            if(Evento::where([
                ['gira_id', $request['gira_id']],
                ['fecha', $request['fecha']],
                ['municipio_id', $request['municipio_id']],
                ['localidad_id', $request['localidad_id']]
            ])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra un evento registrado con la misma fecha y ubicación.'));
            }

            $request['estado_modulo_id'] = 1;

            $evento = Evento::create($request->all());
            //Agregando el area de Atencion Ciudadana en cada evento que se registre por DEFAULT
            //Pudiera estar en un trigger pero bueno...
            $request['area_id'] = 15;
            $evento->eventoareas()->create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'evento' => $evento));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
    
    //Test:
    public function show($id, BeneficiariosgiraDataTable $dt_beneficiarios, SolicitantesGiraDataTable $dt_solicitantes) {
        $evento = Evento::findOrFail($id);
        if (request()->get('tipo') === 'beneficiarios') {                        
            return $dt_beneficiarios->with(['evento_id' => $evento->id])->render('atnciudadana::giras.show', compact('dt_beneficiarios', 'dt_solicitantes', 'evento'));
        }
        return $dt_solicitantes->with(['evento_id' => $evento->id])->render('atnciudadana::giras.show', compact('dt_beneficiarios', 'dt_solicitantes', 'evento'));
    }

    //Test: OK 
    //Fecha: 05/12/2018
    public function edit($id) {
        return response()->json([
            'html' =>  view::make('agendaeventos::eventos.create_edit')
            ->with('evento', Evento::findOrFail($id))
            ->render()
        ], 200);
    }
    
    //Test: OK
    //Fecha: 10/12/2018
    public function update($id, Request $request) {
        $evento = Evento::findOrFail($id);
        if(!$evento){
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }        
        try {
            DB::beginTransaction();
            $nombre = strtoupper($request['nombre']);
            $nombre = str_replace('"', "", $nombre);
            $nombre = str_replace("'", "", $nombre);
            $request['fecha'] = (\DateTime::createFromFormat('d/m/Y', $request['fecha']))->format('Y-m-d');
            $request['gira_id'] = Gira::firstOrCreate(['nombre' => $nombre])->id;
            $request['usuario_id'] = auth()->user()->id;

            if(Evento::where([
                ['gira_id', $request['gira_id']],
                ['fecha', $request['fecha']],
                ['municipio_id', $request['municipio_id']],
                ['localidad_id', $request['localidad_id']],
                ['id', '!=', $id]
            ])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra un evento registrado con la misma fecha y ubicación.'));
            }

            //$request['estado_modulo_id'] = 3;

            $evento->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'evento' => $evento));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
    
    //Test: OK
    //Fecha: 10/12/2018
    public function destroy($id) {
        $evento = Evento::find($id);
        if(!$evento) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $evento->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Evento eliminado exitosamente.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
    
}
