<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Evento extends Model {
    protected $table = 'even_eventos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["gira_id","tipoevento_id","estado_modulo_id","calle","localidad","municipio_id","fecha","hora","observacion","id_evento_google","usuario_id","localidad_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query) {
        $area_id = Auth::user()->persona->empleado->area->id;
        $usuario_id = Auth::id();
        $query  ->join('atnc_cat_giras','atnc_cat_giras.id','even_eventos.gira_id')
                ->join('even_cat_tiposevento','even_cat_tiposevento.id','even_eventos.tipoevento_id')
                ->leftjoin('cat_municipios','cat_municipios.id','even_eventos.municipio_id')
                ->leftjoin('cat_localidades','cat_localidades.id','even_eventos.localidad_id')
                ->leftjoin('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
                ->leftjoin('cat_regiones','cat_regiones.id','cat_distritos.region_id')
                ->join('even_areas_eventos', 'even_areas_eventos.evento_id', 'even_eventos.id')
                    ->join('estados_modulos', 'estados_modulos.id', 'even_eventos.estado_modulo_id')
                    ->join('cat_estados', 'cat_estados.id', 'estados_modulos.estado_id')
                ->select([
                    'atnc_cat_giras.nombre',
                    'even_cat_tiposevento.id_calendario_google as calendario_google',
                    'even_eventos.tipoevento_id',
                    'even_eventos.id as evento_id',
                    'even_eventos.id_evento_google as evento_google',
                    'even_eventos.fecha as fecha',
                    'even_eventos.hora as hora',
                    'cat_estados.nombre as estado',
                    'cat_municipios.id as municipio_id',
                    'cat_municipios.nombre as municipio',
                    'cat_distritos.nombre as distrito',
                    'cat_regiones.nombre as region',
                    'cat_localidades.id as localidad_id',
                    DB::raw('IFNULL(cat_localidades.nombre, even_eventos.localidad) as localidad')
                ])
                ->distinct('even_eventos.id');
                if($area_id) {
                    $query->where(function($query) use ($area_id, $usuario_id) {
                        $areas = array_map(function($o) { return $o->id_hijo; }, DB::select('call getAreasHijas(?)', [$area_id]));
                        $areas[$area_id] = $area_id;
                        if($usuario_id === 137) {
                            $query
                            ->whereIn('even_eventos.tipoevento_id', [6])
                            ;
                        }
                        else {
                            if($area_id !== 1 && $area_id !== 12 && $area_id !== 15 && $area_id !== 22) {
                                $query
                                ->whereIn('area_id', $areas)
                                ->where('even_areas_eventos.genera_ficha', 1)
                                ->whereNull('even_areas_eventos.deleted_at')
                                ;
                            }
                        }
                    });
                }
    }   

    public function beneficiarios() {
        return $this->hasMany('App\Models\Beneficiariogira','evento_id');
    }

    public function eventoareas() {
        return $this->hasMany('Modules\AgendaEventos\Entities\EventoAreas');
    }

    public function cat_localidad() {
        return $this->belongsTo('App\Models\Localidad','localidad_id');
    }

    public function municipio() {
        return $this->belongsTo('App\Models\Municipio');
    }

    public function gira() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Gira');
    }

    public function ficha(){
        return $this->hasOne('Modules\AgendaEventos\Entities\Ficha');
    }

    public function calendario() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\TipoEvento','tipoevento_id');
    }

    public function usuario() {//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario', 'id', 'usuario_id');
    }

    public function get_formato_fecha() {
        return (\DateTime::createFromFormat('Y-m-d', $this->fecha))->format('d/m/Y');
    }

    public function get_fecha_letra() {
        //Si, hay 2 lineas para el setLocale para que se muestre en español
        //Si le quitas uno no funciona y lo muestra en ingles
        //Si lo quieres en español deja las dos lineas papu
        //FIXME:
        setlocale(LC_TIME, "es_MX.UTF-8");
        $fecha = explode(',', date('m,d,Y', strtotime($this->fecha)));
        setlocale(LC_TIME, "es_MX.UTF-8"); //NO ELIMINAR, LEE ARRIBA
        $dfecha = mb_strtoupper(strftime('%A %e de %B de %Y', mktime(0,0,0, $fecha[0], $fecha[1], $fecha[2])));
        return $dfecha;
    }

    public function get_formato_hora() {
        return date("g:i a",strtotime($this->hora));
    }

    public function get_formato_hora_minutos() {
        return date("H:i",strtotime($this->hora));
    }

    public function get_inversion() {
        $inversion = 0;
        foreach($this->beneficiarios->where('folio','!=',null) as $beneficiario){
            $producto_foliado = Productosfoliados::where('folio',$beneficiario->folio)->first();
            $inversion += $producto_foliado->detalleentradasproductos->precio;
        }
        return number_format($inversion, 2, '.', ',');
    }
}
