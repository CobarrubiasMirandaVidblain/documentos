<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Gira extends Model {
    protected $table = 'atnc_cat_giras';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query, $area_id) {
        $usuario_id = Auth::id();
        $query
        ->leftjoin('even_eventos','even_eventos.gira_id','atnc_cat_giras.id')
        ->leftjoin('even_cat_tiposevento', 'even_cat_tiposevento.id', 'even_eventos.tipoevento_id')
                ->leftjoin('cat_municipios','cat_municipios.id','even_eventos.municipio_id')
                ->leftjoin('cat_localidades','cat_localidades.id','even_eventos.localidad_id')
                ->leftjoin('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
                ->leftjoin('cat_regiones','cat_regiones.id','cat_distritos.region_id')
                ->join('even_areas_eventos', 'even_areas_eventos.evento_id', 'even_eventos.id')
                ->leftjoin('estados_modulos', 'estados_modulos.id', 'even_eventos.estado_modulo_id')
                ->leftjoin('cat_estados', 'cat_estados.id', 'estados_modulos.estado_id')
                
                ->leftjoin('even_fichasinfo', 'even_fichasinfo.evento_id', 'even_eventos.id')

                ->whereNull('even_eventos.deleted_at')
                ->whereNull('even_fichasinfo.deleted_at')
                ;
                if($area_id) {
                    $query->where(function($query) use ($area_id, $usuario_id) {
                        $areas = array_map(function($o) { return $o->id_hijo; }, DB::select('call getAreasHijas(?)', [$area_id]));
                        $areas[$area_id] = $area_id;
                        if($usuario_id === 137) {
                            $query
                            ->whereIn('even_eventos.tipoevento_id', [6])
                            ;
                        }
                        else {
                            if($area_id !== 1 && $area_id !== 12 && $area_id !== 15 && $area_id !== 22) {
                                $query
                                ->whereIn('area_id', $areas)
                                ->where('even_areas_eventos.genera_ficha', 1)
                                ->whereNull('even_areas_eventos.deleted_at')
                                ;
                            }
                        }
                    });
                }
        $query->select([
            'atnc_cat_giras.id as gira_id',
            'even_eventos.id as evento_id',
            'even_fichasinfo.id as ficha_id',
            'even_cat_tiposevento.id as even_cat_tiposevento_id',
            'even_eventos.id_evento_google as id_evento_google',
            'even_cat_tiposevento.id_calendario_google as id_calendario_google',
            'even_cat_tiposevento.nombre as tipoevento',
            'atnc_cat_giras.nombre as nombre',
            'even_eventos.fecha as fecha',
            'even_eventos.hora as hora',
            'cat_estados.nombre as estado',
            'cat_municipios.id as municipio_id',
            'cat_municipios.nombre as municipio',
            'cat_distritos.nombre as distrito',
            'cat_regiones.nombre as region',
            'even_fichasinfo.numero_ficha as numero_ficha',
            DB::raw('IFNULL(cat_localidades.nombre, even_eventos.localidad) as localidad')
        ])
        ->distinct('even_eventos.id')
        ;
    }

    public function eventos(){
        return $this->hasMany('Modules\AgendaEventos\Entities\Evento');
    }
}