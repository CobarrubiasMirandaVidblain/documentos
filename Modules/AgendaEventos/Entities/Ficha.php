<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Ficha extends Model {
    protected $table = 'even_fichasinfo';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["evento_id","numero_ficha","vestimenta_id","aforo","tipoevento_id",
                        "temperatura","precipitacion","humedad","viento",
                        "persona_convoca_id","cargo_persona_convoca_id","persona_preside_id","cargo_persona_preside_id",
                        "objetivo","presidenta_participacion_id","director_participacion_id","fila_honor","antecedentes","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $with = [
    	"evento.cat_localidad",
        "evento.municipio",
        "evento.municipio.inversionprograma",
		"evento.gira",
		"vestimenta",
      	"tipoevento",
		"personaconvoca",
		"cargopersonaconvoca",
		"personapreside",
		"cargopersonapreside",
		"participacionpresidenta",
		"participaciondirector",
		"asistentes.persona",
        "asistentes.cargo",
        "autoridadesmunicipales.presidentemunicipal.cargo",
        "autoridadesdiputados.diputado.cargo",
        "autoridadesdiputados.diputado.partidopolitico",
		"actividades.actividadresponsables.actividad",
		"actividades.actividadresponsables.responsable",
		"aportaciones.dependencia.tipo",
		"aportaciones.producto"
		];

		protected $appends = ['count_peticiones'];
		
		public function getCountPeticionesAttribute() {
			$claveMunicipio = $this->evento->municipio->id;

			$peticiones = DB::connection('sqlsrv')
			->select(DB::raw("SELECT
					peticiones.CP_AÑO anio,
					CASE
				WHEN peticiones.cp_st_seguimiento = 'E' THEN
					'EN PROCESO'
				WHEN peticiones.cp_st_seguimiento = 'N' THEN
					'NUEVO'
				WHEN peticiones.cp_st_seguimiento = 'F' THEN
					'FINALIZADO'
				END AS estado,
				COUNT (peticiones.CP_AÑO) subtotal
				FROM
					BdSICOPE.dbo.v_Peticiones AS peticiones
				LEFT JOIN BdSICOPE.dbo.v_Beneficiarios AS beneficiarios ON beneficiarios.cp_id_peticion = peticiones.cp_id
				WHERE
					peticiones.Clave_Municipio = '$claveMunicipio'
					AND peticiones.cp_st_seguimiento IN ('E', 'N', 'F')
					AND peticiones.CP_AÑO IN ('2019', '2018', '2017')
				GROUP BY
					peticiones.CP_AÑO,
					peticiones.cp_st_seguimiento
				ORDER BY
					peticiones.CP_AÑO DESC,
					peticiones.cp_st_seguimiento"));

					return $peticiones;
		}

    public function evento() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Evento', 'evento_id');        
    }
    
    public function vestimenta() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Vestimenta', 'vestimenta_id');        
    }

    public function tipoevento() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\TipoEvento', 'tipoevento_id');        
    }

    public function personaconvoca() {
        return $this->belongsTo('App\Models\Persona','persona_convoca_id');
    }

    public function cargopersonaconvoca() {
        return $this->belongsTo('App\Models\Cargo','cargo_persona_convoca_id');
    }

    public function personapreside() {
        return $this->belongsTo('App\Models\Persona','persona_preside_id');
    }

    public function cargopersonapreside() {
        return $this->belongsTo('App\Models\Cargo','cargo_persona_preside_id');
    }

    public function participacionpresidenta() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Participacion','presidenta_participacion_id');
    }

    public function participaciondirector() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Participacion','director_participacion_id');
    }

    public function asistentes() {
        return $this->hasMany('Modules\AgendaEventos\Entities\Asistente','fichainfo_id');
    }
    
	public function autoridadesmunicipales() {
        return $this->hasMany('Modules\AgendaEventos\Entities\AutoridadMunicipio','fichainfo_id');
    }

	public function autoridadesdiputados() {
        return $this->hasMany('Modules\AgendaEventos\Entities\AutoridadDiputado','fichainfo_id');
    }

    public function actividades() {
        return $this->hasMany('Modules\AgendaEventos\Entities\Actividad','fichainfo_id')->orderBy('hora');
	}
	
	public function aportaciones() {
        return $this->hasMany('Modules\AgendaEventos\Entities\Aportacion','fichainfo_id');
    }
}