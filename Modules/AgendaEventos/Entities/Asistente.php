<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Asistente extends Model {
    protected $table = 'even_asistentes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["fichainfo_id","persona_id","cargo_id","posicion","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query){
        $query->join('personas','personas.id','even_asistentes.persona_id')
				->join('cat_cargos','cat_cargos.id','even_asistentes.cargo_id')
				->select([
					'even_asistentes.id as id',
					'even_asistentes.fichainfo_id as fichainfo_id',
					'even_asistentes.persona_id as persona_id',
					'even_asistentes.posicion as posicion',
					'cat_cargos.nombre as cargo',
	                DB::raw("CONCAT( personas.nombre, ' ', personas.primer_apellido, ' ', personas.segundo_apellido) as nombre")
            	]);
    }

    public function ficha() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Ficha', 'fichainfo_id');        
    }

    public function persona() {
        return $this->belongsTo('App\Models\Persona','persona_id');
    }

    public function cargo() {
        return $this->belongsTo('App\Models\Cargo','cargo_id');
    }
    
}