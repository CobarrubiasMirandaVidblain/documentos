<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aportacion extends Model {
    protected $table = 'even_aportaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["fichainfo_id","dependencia_id","producto_id","cantidad","monto","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query) {
        $query->join('cat_instituciones','cat_instituciones.id','even_aportaciones.dependencia_id')
				->join('even_cat_productos','even_cat_productos.id','even_aportaciones.producto_id')
				->select([
					'even_aportaciones.id as id',
					'even_aportaciones.fichainfo_id as fichainfo_id',
					'even_aportaciones.dependencia_id as dependencia_id',
					'cat_instituciones.nombre as nombre',
					'even_cat_productos.nombre as producto',
					'even_aportaciones.cantidad as cantidad',
					'even_aportaciones.monto as monto'
            	]);
    }

    public function dependencia() {
        return $this->belongsTo('App\Models\Dependencia','dependencia_id');
    }

    public function producto() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Producto', 'producto_id');        
    }
}