<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoInstitucion extends Model {
    use SoftDeletes;

    protected $table = 'cat_tiposinstitucion';

    protected $fillable = [];
}