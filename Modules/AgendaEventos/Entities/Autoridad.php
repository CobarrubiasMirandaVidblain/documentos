<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Autoridad extends Model {
    protected $table = 'even_autoridadesmuni';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["fichainfo_id","nombre","cargo_id","partidopolitico_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query){
        $query->join('cat_partidospoliticos','cat_partidospoliticos.id','even_autoridadesmuni.partidopolitico_id')
				->join('cat_cargos','cat_cargos.id','even_autoridadesmuni.cargo_id')
				->select([
					'even_autoridadesmuni.id as id',
					'even_autoridadesmuni.fichainfo_id as fichainfo_id',
					'even_autoridadesmuni.nombre as nombre',
					'even_autoridadesmuni.partidopolitico_id as partido_id',
					'cat_partidospoliticos.siglas as siglas',
					'cat_partidospoliticos.nombre as partido',
					'cat_cargos.nombre as cargo'
            	]);
    }

    public function ficha() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Ficha', 'fichainfo_id');        
    }
    
    public function cargo() {
      return $this->belongsTo('App\Models\Cargo','cargo_id');
    }
    
    public function partidopolitico() {
        return $this->belongsTo('App\Models\PartidoPolitico','partidopolitico_id');
    }
}