<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vestimenta extends Model {
    protected $table = 'even_vestimenta';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","codigo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}