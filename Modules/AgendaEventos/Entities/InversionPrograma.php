<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\DB;

class InversionPrograma extends Model {
    protected $table = 'inve_inversionesprogramas';

    protected $fillable = [];
}