<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoEvento extends Model {
    protected $table = 'even_cat_tiposevento';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","descripcion","id_calendario_google"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}