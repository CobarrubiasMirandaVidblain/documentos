<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoModulo extends Model {
    use SoftDeletes;

    protected $table = 'estados_modulos';

    protected $fillable = [];
}