<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class AutoridadMunicipio extends Model {
    use SoftDeletes;

    protected $table = 'even_autoridadesmuni';

    protected $fillable = ['fichainfo_id', 'presidentemunicipal_id', 'usuario_id'];
    
    public function presidentemunicipal() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\PresidenteMunicipal','presidentemunicipal_id');
    }
}