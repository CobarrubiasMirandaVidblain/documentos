<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Diputado extends Model {
    use SoftDeletes;

    protected $table = 'diputadoslocales';

    protected $fillable = [];

    public function nombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }
    
    public function cargo() {
        return $this->belongsTo('App\Models\Cargo');
    }

    public function partidopolitico() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\PartidoPolitico');
    }
}