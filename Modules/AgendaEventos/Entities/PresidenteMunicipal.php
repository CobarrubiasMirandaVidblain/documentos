<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class PresidenteMunicipal extends Model {
    use SoftDeletes;

    protected $table = 'presidentesmunicipales';

    protected $fillable = ['nombre', 'primer_apellido', 'segundo_apellido', 'cargo_id', 'telefono_particular', 'telefono_fijo', 'metodo_eleccion_id', 'mayoria_relativa', 'periodo', 'municipio_id', 'usuario_id'];

    public function nombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    public function cargo() {
        return $this->belongsTo('App\Models\Cargo');
    }
}
