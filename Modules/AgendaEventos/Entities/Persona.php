<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\AgendaEventos\Entities\Base;

class Persona extends Base {
    protected $table = 'personas';

    protected $fillable = ['email'];
}