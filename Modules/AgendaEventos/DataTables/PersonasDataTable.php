<?php

namespace Modules\AgendaEventos\DataTables;

use Modules\AgendaEventos\Entities\Persona;

use Yajra\DataTables\Services\DataTable;

class PersonasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        $dataTable = datatables($query);
        
        if($this->tipo === 'modal_personas') {
            $dataTable->addColumn('seleccionar', function($model) {
                return '<a class="btn btn-success btn-xs" onclick="' . "seleccionar_persona('$model->id', '$model->email', '$model->nombre $model->primer_apellido $model->segundo_apellido');" . '"><i class="fa fa-check"></i> SELECCIONAR</a>';
            })
            ->rawColumns(['seleccionar']);
        }
        else {
            $dataTable
            ->addColumn('editar', function($model) {
                return '<a class="btn btn-warning btn-xs" onclick="' . "abrir_modal('$model->id', '$model->email');" . '"><i class="fa fa-pencil"></i> EDITAR</a>';
            })
            ->rawColumns(['editar']);
        }

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = Persona::
        select(
            [
                'personas.id as id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.email as email',
                'cat_tiposempleados.tipo as tipo',
                'cat_areas.nombre as area'
            ]
        )

        ->join('empleados', 'empleados.persona_id', '=', 'personas.id')
        ->whereNull('empleados.deleted_at')

        ->join('cat_tiposempleados', 'cat_tiposempleados.id', '=', 'empleados.tiposempleado_id')
        ->whereNull('cat_tiposempleados.deleted_at')
        ->where('cat_tiposempleados.tipo', '=', 'MANDOS MEDIOS')
        
        ->join('cat_areas', 'cat_areas.id', '=', 'empleados.area_id')
        ->whereNull('cat_areas.deleted_at');
        
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->ajax(['data' => 'function(d) { d.table = "personas"; }'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns = [
            //'id' => ['title' => 'ID', 'data' => 'id', 'name' => 'personas.id'],
            'nombre' => ['title' => 'NOMBRE', 'data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido' => ['title' => 'PRIMER APELLIDO', 'data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['title' => 'SEGUNDO APELLIDO', 'data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'email' => ['title' => 'EMAIL', 'data' => 'email', 'name' => 'personas.email'],
            'tipo' => ['title' => 'RELACION LABORAL', 'data' => 'tipo', 'name' => 'cat_tiposempleados.tipo'],
            'area' => ['title' => 'AREA', 'data' => 'area', 'name' => 'cat_areas.nombre'],
            //'editar' => ['title' => 'EDITAR', 'data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];

        if($this->tipo === 'modal_personas') {
            $columns['seleccionar'] = ['title' => 'SELECCIONAR', 'data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }
        else
        {
            $columns['editar'] = ['title' => 'EDITAR', 'data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> EXCEL',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> REINICIAR',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> RECARGAR',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Persona_' . date('YmdHis');
    }
}