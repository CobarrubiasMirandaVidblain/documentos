<?php

namespace Modules\AgendaEventos\DataTables;

use Modules\AgendaEventos\Entities\PresidenteMunicipal;

use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class PresidenteMunicipalDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        $dataTable = datatables($query)
        ->addColumn('seleccionar', function($query) {
            return '<a class="btn btn-success btn-xs" onclick="seleccionar_autoridad_municipal(' . "'" . $query->id . "'" . ', ' . "'" . $query->nombreCompleto() . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
        })
        ->rawColumns(['seleccionar']);
        
        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = PresidenteMunicipal::
        select(
          [
            'presidentesmunicipales.id as id',
            'presidentesmunicipales.nombre as nombre',
            'presidentesmunicipales.primer_apellido as primer_apellido',
            'presidentesmunicipales.segundo_apellido as segundo_apellido',
            'cat_cargos.nombre as cargo',
            'presidentesmunicipales.mayoria_relativa as partido_politico',
            'cat_regiones.nombre as region',
            'cat_distritos.nombre as distrito',
            'cat_municipios.nombre as municipio'
          ]
        )
        ->join('cat_cargos', 'cat_cargos.id', '=', 'presidentesmunicipales.cargo_id')
        ->leftjoin('cat_municipios', 'cat_municipios.id', '=', 'presidentesmunicipales.municipio_id')
        ->leftjoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftjoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->ajax(['data' => 'function(d) { d.table = "autoridadmunicipio"; }'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns =
        [
          //'id' => ['data' => 'id', 'name' => 'presidentesmunicipales.id'],
          'nombre' => ['data' => 'nombre', 'name' => 'presidentesmunicipales.nombre'],
          'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'presidentesmunicipales.primer_apellido'],
          'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'presidentesmunicipales.segundo_apellido'],
          'cargo' => ['data' => 'cargo', 'name' => 'cat_cargos.nombre'],
          'partido_politico' => ['data' => 'partido_politico', 'name' => 'presidentesmunicipales.mayoria_relativa'],
          'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
          'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
          'municipio' => ['data' => 'municipio', 'name' => 'presidentesmunicipales.nombre'],
          'seleccionar' => ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];

        return $columns;
    }
    
    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'sEmptyTable' => 'No hay autoridades municipales registrados(as).'
            ],
            'dom' => 'fBtipr',
            //'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            //'responsive' => true,
            //'autoWidth' => true,
            'columnDefs' => [
                [
                    'className' => 'text-center', 'targets' => '_all'
                ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'presidentemunicipaldatatable_' . time();
    }
}
