<?php

Route::group(['middleware' => 'web', 'prefix' => 'vale', 'namespace' => 'Modules\Vale\Http\Controllers'], function() {

    Route::resource('peticiones', 'PeticionController',['as'=>'vale','only'=>['index','show','update','destroy']]);   //Peticiones
    Route::resource('programas', 'ProgramaController',['as'=>'vale']);  

    Route::get('/', 'PeticionController@index');

    Route::resource('/personas', 'PersonaController', ['as' => 'vale']);

    Route::resource('/grupos', 'GrupoController', ['as' => 'vale']);

    Route::resource('/alumnos', 'AlumnoController', ['as' => 'vale']);

    Route::get('/etnias/select', 'EtniaController@select')->name('vale.etnias.select');
    Route::resource('/etnias', 'EtniaController', ['as' => 'vale']);
});