<?php

namespace Modules\Vale\Http\Controllers;

//use App\DataTables\Vale\GruposDataTable;
use Modules\Vale\Entities\Persona;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PersonaController extends Controller {
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('vale::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('vale::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $datos = json_decode($request->persona, true);

                $datos['fecha_nacimiento'] = (\DateTime::createFromFormat('d/m/Y', $datos['fecha_nacimiento']))->format('Y-m-d');

                $datos['usuario_id'] = auth()->user()->id;

                $directorio = 'public/fotografia';
                
                if($request->has('fotografia')) {
                    $url = Storage::putFile($directorio, $request->fotografia);
                    $partes = explode('public', $url);
                    $datos['fotografia'] = 'storage' . $partes[1];
                }
                
                $persona = Persona::create($datos);
                
                DB::commit();
                
                auth()->user()->bitacora(request(), [
                    'tabla' => 'personas',
                    'registro' => $persona->id . '',
                    'campos' => json_encode($persona) . '',
                    'metodo' => request()->method()
                ]);
                
                return response()->json(['persona' => $persona], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request, $id) {
        if($request->ajax()) {
            try {
                $persona = Persona::with('etnia', 'localidad', 'municipio.distrito.region')->get()->find($id);
                
                if(!$persona) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                return response()->json(['persona' => $persona], 200);
            }
            catch(\Exception $e) {
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('vale::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $datos = json_decode($request->persona, true);
                
                $datos['fecha_nacimiento'] = (\DateTime::createFromFormat('d/m/Y', $datos['fecha_nacimiento']))->format('Y-m-d');

                $datos['usuario_id'] = auth()->user()->id;
                
                $persona = Persona::find($datos['id']);
                
                if($persona) {
                    $directorio = 'public/fotografia';
                    
                    if($request->has('fotografia')) {
                        $url = Storage::putFile($directorio, $request->fotografia);
                        $partes = explode('public', $url);
                        $datos['fotografia'] = 'storage' . $partes[1];
                    }
                    else {
                        if($datos['imagen'] === 'true' && isset($persona->fotografia)) {
                            $partes = explode('storage', $persona->fotografia);
                            Storage::delete('public' . $partes[1]);
                            $datos['fotografia'] = null;
                        }
                    }

                    $persona->update($datos);
                }
                
                DB::commit();
                
                auth()->user()->bitacora(request(), [
                    'tabla' => 'personas',
                    'registro' => $persona->id . '',
                    'campos' => json_encode($persona) . '',
                    'metodo' => request()->method()
                ]);
                
                return response()->json(['persona' => $persona], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}