<?php

namespace Modules\Vale\Http\Controllers;

use Modules\Vale\Entities\Etnia;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class EtniaController extends Controller {
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('vale::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('vale::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('vale::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('vale::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
    
    public function select(Request $request) {
        if($request->ajax()) {
            $etnias = Etnia::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
            //->take(10)
            ->get()
            ->toArray();
            
            return response()->json($etnias);
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }
}