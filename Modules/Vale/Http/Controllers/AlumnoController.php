<?php

namespace Modules\Vale\Http\Controllers;

use App\DataTables\Vale\AlumnosDataTable;
use App\DataTables\Vale\PersonasDataTable;

use Modules\Vale\Entities\Alumno;
//use Modules\Vale\Entities\Persona;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class AlumnoController extends Controller {
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(AlumnosDataTable $alumnos, PersonasDataTable $personas) {
        if(request()->get('table') === 'alumnos') {
            return $alumnos->render('vale::alumnos.index', compact('alumnos', 'personas'));
        }
        else {
            return $personas->render('vale::alumnos.index', compact('alumnos', 'personas'));
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('vale::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $datos = $request->alumno;
                
                $datos['usuario_id'] = auth()->user()->id;

                $alumno = Alumno::create($datos);
                
                DB::commit();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'vale_alumnos',
                    'registro' => $alumno->id . '',
                    'campos' => json_encode($alumno) . '',
                    'metodo' => request()->method()
                ]);
                
                return response()->json(['alumno' => $alumno], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request, $id) {
        if($request->ajax()) {
            try {
                $alumno = Alumno::with('persona.localidad', 'persona.municipio.distrito.region')->get()->find($id);
                
                if(!$alumno) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                return response()->json(['alumno' => $alumno], 200);
            }
            catch(\Exception $e) {
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('vale::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $alumno = Alumno::find($id);

                if(!$alumno) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                $datos = $request->alumno;
                
                $datos['usuario_id'] = auth()->user()->id;
                
                $alumno->update($request->alumno);
                
                DB::commit();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'vale_alumnos',
                    'registro' => $alumno->id . '',
                    'campos' => json_encode($alumno) . '',
                    'metodo' => request()->method()
                ]);
                
                return response()->json(['alumno' => $alumno], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $alumno = Alumno::find($id);

                if(!$alumno) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                $tmp = $alumno;
                
                $alumno->delete();
                
                DB::commit();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'vale_alumnos',
                    'registro' => $tmp->id . '',
                    'campos' => json_encode($tmp) . '',
                    'metodo' => request()->method()
                ]);
                
                return response()->json(['alumno' => $tmp], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }
}