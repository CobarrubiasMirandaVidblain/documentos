@extends('vale::layouts.master')

@section('content-title', '')

@section('content-subtitle', '')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<!-- <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"> -->
<!-- sweetalert2 -->
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
select[readonly] {
	background: #eee;
	cursor: no-drop;
}

select[readonly] option {
	display: none;
}

.modal-body {
	overflow-y: auto;
	max-height: calc(100vh - 210px);
}
</style>
@endpush

@section('content')
<div id="app">
    <h1> </h1>
    <p>
        <router-link to="/index">Go to Index</router-link>
        <router-link to="/create">Go to Create</router-link>
    </p>
    
    <div v-show="['index'].indexOf($route.name) > -1" class="box box-primary shadow">
        <div class="box-header with-border">
            <h3 class="box-title">Tabla de Grupos:</h3>
        </div>
        
        <div class="box-body">
        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'grupos', 'name' => 'grupos', 'style' => 'width: 100%']) !!}
        </div>
    </div>
    <!-- <keep-alive include="index">
    <router-view></router-view>
    </keep-alive> -->
    <router-view></router-view>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>

<!-- sweetalert2 -->
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>

<script>
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

const CreateEdit = Vue.component('create', {
    name: 'create',
    template: `
    <div class="box box-primary shadow">
        <form id="formGrupo" name="formGrupo">
            <div class="box-header with-border">
                <h3 class="box-title">@{{ title }}</h3>
            </div>
            
            <div class="box-body">
            
                <div v-show="['edit'].indexOf($route.name) > -1" class="form-group">
                    <label for="id">Id:</label>
                    @{{ grupo.id }}
                </div>
            
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" v-model="grupo.nombre" @input="setUpperCase($event, grupo, 'nombre')">
				</div>

                <div class="form-group">
                    <label for="descripcion">Descripción:</label>
                    <input type="text" class="form-control" id="descripcion" name="descripcion" v-model="grupo.descripcion" @input="setUpperCase($event, grupo, 'descripcion')">
				</div>

                <div class="form-group">
                    <label for="capacidad">Capacidad:</label>
                    <input type="text" class="form-control" id="capacidad" name="capacidad" v-model="grupo.capacidad" @input="setUpperCase($event, grupo, 'capacidad')">
				</div>

                <div class="pull-right">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" @click.prevent="validarForm()"><i class="fa fa-database"></i> Guardar</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
    `,
    data() {
        return {
            title: 'Crear Grupo:',
            form: null,
            formValidate: null,
            grupo: {
                id: 0,
                nombre: '',
                descripcion: '',
                capacidad: ''
            }
        }
    },
    watch: {
        '$route' (to, from) {
            if(typeof this.$route.params.editId !== 'undefined') {
                this.title = 'Editar Grupo:';
            }
            else {
                this.title = 'Crear Grupo:';
            }
            this.formValidate.resetForm();
            this.grupo.id = 0;
            this.grupo.nombre = '';
            this.grupo.descripcion = '';
            this.grupo.capacidad = '';
        }
    },
    mounted() {
        if(typeof this.$route.params.editId !== 'undefined') {
            this.title = 'Editar Grupo:';
            this.show(this.$route.params.editId);
        }
        
        this.form = $('#formGrupo');

        this.formValidate = this.form.validate({
            rules: {
                nombre: {
                    required: true
                },
                descripcion: {
                    required: true
                },
                capacidad: {
                    required: true,
                    digits: true,
                    rangelength: [1, 2],
                    min: 1
                }
            },
            messages: {
            }
        });
    },
    methods: {
        setUpperCase(e, o, prop) {
            const start = e.target.selectionStart;
            e.target.value = e.target.value.toUpperCase();
            this.$set(o, prop, e.target.value);
            e.target.setSelectionRange(start, start);
        },
        validarForm(e) {
            if(this.form.valid()) {
                block();
                if(typeof this.$route.params.editId !== 'undefined') {
                    this.update();
                }
                else {
                    this.create();
                }
            }
        },
        create() {
            axios.post('/vale/grupos', {
                grupo: this.grupo
            })
            .then(function(response) {
                unblock();
                console.log(response);
                window.LaravelDataTables['grupos'].ajax.reload(null, false);
                router.push({ name: 'index' });
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            })
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    window.LaravelDataTables['grupos'].ajax.reload(null, false);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                }
            });
        },
        show(id) {
            axios.get('/vale/grupos/' + id, {})
            .then(function(response) {
                unblock();
                console.log(response);
                this.grupo.id = response.data.grupo.id;
                this.grupo.nombre = response.data.grupo.nombre;
                this.grupo.descripcion = response.data.grupo.descripcion;
                this.grupo.capacidad = response.data.grupo.capacidad;
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                    router.push({ name: 'index' });
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                    router.push({ name: 'index' });
                }
            });
        },
        update() {
            axios.put('/vale/grupos/' + this.grupo.id, {
                grupo: this.grupo
            })
            .then(function(response) {
                unblock();
                console.log(response);
                window.LaravelDataTables['grupos'].ajax.reload(null, false);
                router.push({ name: 'index' });
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            })
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    window.LaravelDataTables['grupos'].ajax.reload(null, false);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                }
            });
        }
    }
});

const Destroy = Vue.component('destroy', {
    name: 'destroy',
    template: `
    <div class="box box-primary shadow">
        <form id="formGrupo" name="formGrupo">
            <div class="box-header with-border">
                <h3 class="box-title">@{{ title }}</h3>
            </div>
            
            <div class="box-body">
            
                <div class="form-group">
                    <label>Id:</label>
                    @{{ grupo.id }}
                </div>
                
                <div class="form-group">
                    <label>Nombre:</label>
                    @{{ grupo.nombre }}
                </div>

                <div class="form-group">
                    <label>Descripción:</label>
                    @{{ grupo.descripcion }}
                </div>

                <div class="form-group">
                    <label>Capacidad:</label>
                    @{{ grupo.capacidad }}
                </div>
                
                <div class="pull-right">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-danger" @click.prevent="destroy()"><i class="fa fa-trash"></i> Eliminar</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
    `,
    data() {
        return {
            title: 'Eliminar Grupo:',
            grupo: {
                id: 0,
                nombre: '',
                descripcion: '',
                capacidad: ''
            }
        }
    },
    mounted() {
        this.show(this.$route.params.destroyId);
    },
    methods: {
        show(id) {
            axios.get('/vale/grupos/' + id, {})
            .then(function(response) {
                unblock();
                console.log(response);
                this.grupo.id = response.data.grupo.id;
                this.grupo.nombre = response.data.grupo.nombre;
                this.grupo.descripcion = response.data.grupo.descripcion;
                this.grupo.capacidad = response.data.grupo.capacidad;
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                    router.push({ name: 'index' });
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                    router.push({ name: 'index' });
                }
            });
        },
        destroy() {
            swal({
				title: '',
				text: '',
				type: 'info',
                showConfirmButton: true,
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Eliminar',
				cancelButtonText: 'Cancelar',
				allowEscapeKey: false,
				allowOutsideClick: false
			}).then((result) => {
                if(result.value) {
                    axios.delete('/vale/grupos/' + this.grupo.id, {})
                    .then(function(response) {
                        unblock();
                        console.log(response);
                        window.LaravelDataTables['grupos'].ajax.reload(null, false);
                        router.push({ name: 'index' });
                        Swal({
                            type: 'success',
                            title: response.status + ' ' + response.statusText,
                            html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    }.bind(this))
                    .catch(function(error) {
                        if(error.response) {
                            unblock();
                            console.log(error.response);
                            window.LaravelDataTables['grupos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                            Swal({
                                type: 'error',
                                title: error.response.status + ' ' + error.response.statusText,
                                text: error.response.data.message,
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }
                        else if(error.request) {
                            unblock();
                            console.log(error.request);
                            window.LaravelDataTables['grupos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                        }
                        else {
                            unblock();
                            console.log('Error', error.message);
                            window.LaravelDataTables['grupos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                        }
                    });
                }

				if(result.dismiss) {
                }
                
                if(result.dismiss === swal.DismissReason.timer) {
                }
			});
        }
    }
});

const routes = [
    {
        path: '/',
        redirect: '/index'
    },
    {
        path: '/index',
        name: 'index'
    },
    {
        path: '/create',
        name: 'create',
        component: CreateEdit
    },
    {
        path: '/show/:showId',
        name: 'show',
        component: CreateEdit
    },
    {
        path: '/edit/:editId',
        name: 'edit',
        component: CreateEdit
    },
    {
        path: '/destroy/:destroyId',
        name: 'destroy',
        component: Destroy
    }
]

const router = new VueRouter({
    routes
})

const app = new Vue({
    router
}).$mount('#app')
</script>
{!! $dataTable->scripts() !!}
@endpush