<?php

namespace Modules\Vale\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Etnia extends Model {
    use SoftDeletes;

    protected $table = 'cat_etnias';

    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre', 'clave_inegi'];
}