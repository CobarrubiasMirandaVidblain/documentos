<?php

namespace Modules\Vale\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Grupo extends Model {
    use SoftDeletes;

    protected $table = 'vale_grupos';

    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre', 'descripcion', 'capacidad'];
}