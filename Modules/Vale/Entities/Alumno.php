<?php

namespace Modules\Vale\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Alumno extends Model {
    use SoftDeletes;
    
    protected $table = 'vale_alumnos';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['persona_id', 'matricula', 'usuario_id'];

    public function persona() {
        return $this->belongsTo('Modules\Vale\Entities\Persona');
    }
}