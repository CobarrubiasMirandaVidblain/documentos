<?php

namespace Modules\Vale\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model {
    use SoftDeletes;

    protected $table = 'personas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['titulo', 'nombre', 'primer_apellido', 'segundo_apellido', 'calle', 'numero_exterior', 'numero_interior', 'colonia', 'codigopostal', 'municipio_id', 'localidad_id', 'curp', 'genero', 'fecha_nacimiento', 'clave_electoral', 'numero_celular', 'numero_local', 'email', 'referencia_domicilio', 'curpo', 'fotografia', 'etnia_id', 'usuario_id'];

    public function nombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }
    
    public function etnia() {
        return $this->belongsTo('Modules\Vale\Entities\Etnia');
    }
    
    public function municipio() {
        return $this->belongsTo('App\Models\Municipio');
    }
    
    public function localidad() {
        return $this->belongsTo('App\Models\Localidad');
    }
}