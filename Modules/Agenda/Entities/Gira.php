<?php

namespace Modules\Agenda\Entities;

use Modules\Agenda\Entities\Base;

class Gira extends Base {
		protected $table = 'atnc_cat_giras';
		//protected $table = 'age_giras';

		protected $fillable = [];

		public function eventos() {
				return $this->hasMany('Modules\Agenda\Entities\Evento');
		}
}