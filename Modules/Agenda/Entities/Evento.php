<?php

namespace Modules\Agenda\Entities;

use Modules\Agenda\Entities\Base;

class Evento extends Base {
		protected $table = 'even_eventos';
		//protected $table = 'age_eventos';

		protected $fillable = [];
		
		public function gira() {
				return $this->belongsTo('Modules\Agenda\Entities\Gira', 'id');
    }
}