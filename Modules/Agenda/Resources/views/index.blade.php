@extends('agenda::layouts.main')

@section('content')
<div id="app">
	<button id="googleSignInButton" style="display: none;" onClick="ApisGoogle.signIn();">Sign In</button>
	<button id="googleSignOutButton" style="display: none;" onClick="ApisGoogle.signOut();">Sign Out</button>
	<button id="googleScript" style="display: none;" onClick="ApisGoogle.signOut();">Sign Out</button>
	<router-link to="/home">Go to Index</router-link>
	<router-link to="/create">Go to Create</router-link>
	<router-view></router-view>
</div>
@stop

@push('body')
<script type="text/javascript">
var ComponentBlockUi = function() {
	var _init = function() {
		if(!$().block) {
			console.warn('Warning - blockui.min.js is not loaded.');
			return;
		}
	};

	var _blockPage = function() {
		_init();
		$.blockUI();
	};

	var _unBlockPage = function() {
		_init();
		$.unblockUI();
	};

	return {
		init: function() {
			_init();
		},
		blockPage: function() {
			_blockPage();
		},
		unBlockPage: function() {
			_unBlockPage();
		}
	};
}();

var ApisGoogle = function() {
	var CLIENT_ID = '';
	var API_KEY = '';
	var DISCOVERY_DOCS = [''];
	var SCOPES = '';

	var googleAuth2 = null;
	
	var googleSignInButton = document.getElementById('googleSignInButton');
	var googleSignOutButton = document.getElementById('googleSignOutButton');

	var _init = function() {
		CLIENT_ID = '489363224492-tob8tfhthj23q4vf21fss3gobbmamuig.apps.googleusercontent.com';
		API_KEY = 'AIzaSyC9u0yWqGY5cXp0jqzX_snVN1YiriTHxUg';
		DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
		SCOPES = "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events";

		_handleClientLoad();
	};

	var _statusGapi = function() {
		if(!window.gapi) {
			throw new Error('Warning - https://apis.google.com/js/api.js is not loaded.');
		}
	};
	
	var _statusGapiClient = function() {
		if(!window.gapi.client) {
			throw new Error('Warning - window.gapi.client is not loaded.');
		}
	};

	var _statusGapiAuth2 = function() {
		if(!window.gapi.auth2) {
			throw new Error('Warning - window.gapi.auth2 is not loaded.');
		}
	};
	
	function _handleClientLoad() {
		_statusGapi();
		ComponentBlockUi.blockPage();
		
		var loadGapiClient = new Promise(function(resolve) {
			gapi.load('client:auth2', resolve);
		});
		
		loadGapiClient
		.then(function() {
			googleAuth2 = gapi.auth2.init({
				apiKey: API_KEY,
				clientId: CLIENT_ID,
				discoveryDocs: DISCOVERY_DOCS,
				scope: SCOPES
			});
			
			googleAuth2
			.then(function() {
				_updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
				ComponentBlockUi.unBlockPage();
			});
		});
	}
	
	function _signIn() {
		_statusGapi();
		_statusGapiAuth2();
		ComponentBlockUi.blockPage();
		
		if(gapi.auth2.getAuthInstance().isSignedIn.get()) {
			console.log(gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile());
			ComponentBlockUi.unBlockPage();
		}
		else {
			googleAuth2
			.signIn()
			.then(function() {
				_updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
				ComponentBlockUi.unBlockPage();
			})
			.catch(function(error) {
				_updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
				ComponentBlockUi.unBlockPage();
				console.log(error);
			});
		}
	};
	
	function _updateSignInStatus(isSignedIn) {
		if(isSignedIn) {
			googleSignInButton.style.display = 'none';
			googleSignOutButton.style.display = 'block';
		}
		else {
			googleSignInButton.style.display = 'block';
			googleSignOutButton.style.display = 'none';
		}
	}
	
	function _signOut() {
		ComponentBlockUi.blockPage();
		
		gapi.auth2.getAuthInstance()
		.signOut()
		.then(function() {
			_updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
			ComponentBlockUi.unBlockPage();
		})
		.catch(function(error) {
			_updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
			ComponentBlockUi.unBlockPage();
			console.log(error);
		});
	};

	function _initScript() {
		ComponentBlockUi.blockPage();

		$.getScript("https://apis.google.com/js/api.js", function() {
			_init();
		})
		.done(function(script, textStatus) {
			ComponentBlockUi.unBlockPage();
			console.log(textStatus);
		})
		.fail(function(jqxhr, settings, exception) {
			ComponentBlockUi.unBlockPage();
			console.log(jqxhr);
			console.log(settings);
			console.log(exception);
		});
	};
	
	return {
		init: function() {
			_init();
		},
		signIn: function() {
			_signIn();
		},
		signOut: function() {
			_signOut();
		},
		initScript: function() {
			_initScript();
		}
	};
}();

ComponentBlockUi.blockPage();
</script>

<script async defer src="https://apis.google.com/js/api.js"
onload="ComponentBlockUi.unBlockPage(); ApisGoogle.init();"
onerror="ComponentBlockUi.unBlockPage();"
onreadystatechange="if(this.readyState === 'complete') this.onload();">
</script>

<script type="text/javascript">
</script>

<script type="text/javascript">
</script>
@endpush