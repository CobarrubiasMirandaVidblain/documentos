<?php

Route::group(['prefix' => 'evento', 'namespace' => 'Modules\Agenda\Http\Controllers'], function() {
	Route::post('/login', 'AgendaController@login');
    Route::get('/', 'AgendaController@index');
});

Route::group(['middleware' => 'web', 'prefix' => 'agenda/api/v1', 'namespace' => 'Modules\Agenda\Http\Controllers\Api\V1'], function() {
		Route::resource('/eventos', 'EventoController', ['as' => 'evento']);

		Route::resource('/eventos/{eventoId}/giras', 'GiraController', ['as' => 'gira']);
});