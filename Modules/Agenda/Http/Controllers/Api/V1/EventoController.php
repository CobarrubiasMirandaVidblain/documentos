<?php

namespace Modules\Agenda\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Str;

use Modules\Agenda\Http\Library\Api\V1\EloquentApiBuilder;

class EventoController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        //dd('Hola Mundo.');
        if(!$request->accepts(['application/json'])) {
						return response()->json(['status_code' => 406, 'status_message' => 'Not Acceptable.'], 406);
				}
				
				if(Str::contains($request->header('CONTENT_TYPE'), ['/json', '+json'])) {
            try {
                $builder = new EloquentApiBuilder(new \App\Models\Persona(), $request);
                $builder->setResponse();
            }
            catch(\Exception $e) {
                return response()->json(['status_code' => 500, 'status_message' => 'Internal Server Error.', 'exception_code' => $e->getCode(), 'exception_message' => $e->getMessage()], 500);
            }

            return response()->json(['status_code' => 200, 'status_message' => 'OK.', 'data' => $builder->getResponse()], 200);
        }

        return response()->json(['status_code' => 415, 'status_message' => 'Unsupported Media Type.'], 415);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agenda::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agenda::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agenda::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}