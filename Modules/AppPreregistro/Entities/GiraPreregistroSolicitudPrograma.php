<?php

namespace Modules\AppPreregistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GiraPreregistroSolicitudPrograma extends Model {
	use SoftDeletes;

	protected $table = 'atnc_giraspreregis_beneficiosprog';

    protected $fillable = ['girapreregis_id', 'beneficioprograma_id', 'usuario_id'];

    public function beneficioprograma(){
        return $this->belongsTo('App\Models\Beneficiosprograma','beneficioprograma_id','id');
    }
}