<?php

namespace Modules\AppPreregistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeneficioPrograma extends Model {
	use SoftDeletes;

	protected $table = 'beneficiosprogramas';

    protected $fillable = ['nombre'];
}