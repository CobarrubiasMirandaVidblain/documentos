<?php

namespace Modules\AppPreregistro\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\Persona;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

use \Firebase\JWT\JWT;
use Validator;

use Modules\AppPreregistro\Entities\GiraPreregistro;
use App\Models\Beneficiosprograma;
use App\Models\Solicitud;
use App\Models\EventosSolicitudes;
use App\Models\SolicitudesPersonales;
use App\Models\BeneficiosprogramasSolicitud;
use App\Models\PeticionesPersonas;
use App\Models\BeneficiosPersonas;
use App\Models\EstadosSolicitud;

use Illuminate\Support\Facades\DB;

class AppPreregistroController extends Controller {

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'usuario' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return new JsonResponse(['data' => null, 'message' => $validator->errors()], 480);
        }

        $usuario = [
            'usuario' => $request->usuario,
            'password' => $request->password
        ];

        if(Auth::attempt($usuario)) {
            $datos_token = [
                'id' => Auth::user()->id,
                'nombre' => Auth::user()->persona->nombre,
                'primer_apellido' => Auth::user()->persona->primer_apellido,
                'segundo_apellido' => Auth::user()->persona->segundo_apellido,
                'usuario' => Auth::user()->usuario,
                'datetime' => date('Y-m-d h:i:s')
            ];

            $jwt = JWT::encode($datos_token, config('app.jwt_token'), 'HS256');

            $datos_usuario = [
                'id' => Auth::user()->id,
                'nombre' => Auth::user()->persona->nombre . ' ' . Auth::user()->persona->primer_apellido . ' ' . Auth::user()->persona->segundo_apellido,
                'usuario' => Auth::user()->usuario,
                'token' => $jwt
            ];

            return new JsonResponse($datos_usuario, 200);
        }
        else {
            return new JsonResponse(['data' => null, 'message' => ''], 480);
        }
    }

    public function logout(Request $request) {
        dd(JWT::decode($token, config('app.jwt_token'), ['HS256']));
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        return GiraPreregistro::
        with('municipio')
        ->with('localidad')
        ->find($request->id);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return '';
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        //dd($request->id);
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $alumno = GiraPreregistro::find($request->id);

                if(!$alumno) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                $datos = $request->all();
                
                $datos['usuario_id'] = auth()->user()->id;
                
                $alumno->update($datos);
                
                DB::commit();

                return response()->json(['alumno' => $alumno], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return '';
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return '';
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }


    public function crearSolicitudInterna(Request $request) {
        if($request->ajax()) {
            dd(auth()->user());
            // dd(Auth::user());
            try { 
                $beneficio = Beneficiosprograma::where('id',$request->beneficio_id)->first();
                // dd($beneficio);
                if($beneficio && $request->curp){
                    // return response()->json(['existeBeneficio' => true], 200);
                    $persona = Persona::where('curp',$request->curp)->first();
                    if(!$persona) {
                        $datos_persona = $request->except(['beneficio_id','beneficios','created_at','deleted_at','evento_id','folio','fotografia','id','updated_at','usuario','telefono']);
                        $datos_persona['numero_celular'] = $request->telefono;
                        $persona = Persona::Create($datos_persona);
                    }

                    DB::beginTransaction();

                    //Persona es el solicitante y el beneficiario
                    $datos_solicitud = [];
                    $datos_solicitud['persona_id'] = 37124; //HOLM
                    $datos_solicitud['compromiso'] = false;
                    $datos_solicitud['tiposrecepcion_id'] = 1; // GIRA o EVENTO
                    $datos_solicitud['tiposremitente_id'] = 4; // PERSONAL
                    $datos_solicitud['numoficio'] = 'X';
                    $datos_solicitud['fechaoficio'] = $request->created_at ;
                    $datos_solicitud['fecharecepcion'] = $request->created_at;
                    $datos_solicitud['asunto'] = 'SOLICITUD DE ' . $beneficio->nombre . ' EN EVENTO.';
                    $datos_solicitud['folio'] = "I/" . (Solicitud::where('folio','LIKE',$request->input('tipo').'%')->max('id') + 1) . "/" . date("Y");
                    $datos_solicitud['usuario_id'] = auth()->user()->id;

                    //Creamos la solicitud con los datos
                    $solicitud = Solicitud::create($datos_solicitud);

                    //La creamos como solicitud de evento
                    EventosSolicitudes::create([
                        'evento_id' => $request->input('evento_id'),
                        'solicitud_id' => $solicitud->id,
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    //La creamos como solicitud personal
                    $tipo_solicitud = SolicitudesPersonales::create([
                        'solicitud_id' => $solicitud->id,
                        'persona_id' => $persona->id
                    ]);

                    $postfolio = BeneficiosprogramasSolicitud::where([["beneficioprograma_id", $request['beneficio_id']],["solicitud_id", $solicitud->id]])->count()+1;

                    //Se debe crear un registro BeneficiosprogramasSolicitud
                    $beneficio_solicitud = BeneficiosprogramasSolicitud::create([
                        'solicitud_id' => $solicitud->id,
                        'beneficioprograma_id' => $request['beneficio_id'],
                        'cantidad' => 1,
                        'folio'=> $solicitud->folio.'/'.$request['beneficio_id'].'-'.$postfolio,
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    //Se le asigna el status de SOLICITANTE
                    $status_solicitud = EstadosSolicitud::create([
                        'beneficioprograma_solicitud_id' => $beneficio_solicitud->id,
                        'statusproceso_id' => 2, //Solicitante
                        'usuario_id' => auth()->user()->id
                    ]);

                    //Creamos el Beneficio Persona
                    $beneficiopersona = BeneficiosPersonas::create([
                        'persona_id' => $persona->id,
                        'beneficioprograma_id' => $request['beneficio_id'],
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    $peticion_persona = PeticionesPersonas::create([
                        'beneficiosprogramas_solicitud_id' => $beneficio_solicitud->id,
                        'beneficiopersona_id' => $beneficiopersona->id,
                        'entregado' => false,   
                        'evaluado' => false, 
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    GiraPreregistro::find($request->input('id'))->delete();
                    
                    DB::commit();
                    // Guardamos los datos en la BD y regresamos status 200 y la solicitud
                    return response()->json(['success' => array(['code' => 200, 'solicitud' => $solicitud])], 200);
                } else {
                    return response()->json(['mensaje' => 'Faltan Datos'], 409);
                }
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        } else {
            abort(403, 'Acción no autorizada.');
        }
    }
}