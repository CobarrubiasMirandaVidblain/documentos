<?php

namespace Modules\AppPreregistro\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

//use Modules\AppPreregistro\Entities\ProductoFoliado;

use Modules\AppPreregistro\Entities\BeneficioPrograma;

use Modules\AppPreregistro\Entities\GiraPreregistro;
use Modules\AppPreregistro\Entities\GiraPreregistroSolicitudPrograma;

//use App\Models\Beneficiariogira;
//use App\Models\Programa;
//use App\Models\BeneficiariogiraPrograma;

class SolicitudController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('apppreregistro::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('apppreregistro::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $beneficiarios = [];
        \Log::debug($request);
        

        foreach($request->all() as $beneficiario) {
            try {
                DB::beginTransaction();

                if($beneficiario['fecha_nacimiento']) {
                    $beneficiario['fecha_nacimiento'] = (\DateTime::createFromFormat('d-m-Y', $beneficiario['fecha_nacimiento']))->format('Y-m-d');
                }

                $beneficiario['fecha_registro'] = date('Y-m-d h:i:s', strtotime($beneficiario['fecha_registro']));

                $beneficiario['usuario_id'] = $request->header('usuario_id');
                $beneficiario['observaciones'] = 'SOLICITUD EN GIRA.';

                $beneficiario_gira = GiraPreregistro::create(array_except($beneficiario, ['folio','area','color','textura']));

                foreach($beneficiario['apoyos'] as $apoyo) {
                    //$programa_id = Programa::firstOrCreate(['nombre' => $apoyo['nombre']], ['activo' => 1, 'tipo' => 'PRODUCTO', 'oficial' => 0, 'puede_agregar_benef' => 0, 'usuario_id' => $beneficiario['usuario_id']])->id;

                    $beneficio_id = BeneficioPrograma::where('nombre', $apoyo['nombre'])->first()->id;

                    GiraPreregistroSolicitudPrograma::create([
                        'girapreregis_id' => $beneficiario_gira->id,
                        'beneficioprograma_id' => $beneficio_id,
                        'usuario_id' => $beneficiario['usuario_id']
                    ]);
                }

                DB::commit();

                $beneficiario['estatus'] = 'Sincronizado';

                $beneficiarios[] = $beneficiario;
            }
            catch(\Exception $e) {
                DB::rollBack();

                $beneficiario['estatus'] = $e->getMessage();

                $beneficiarios[] = $beneficiario;
            }
        }

        return response()->json($beneficiarios);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('apppreregistro::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('apppreregistro::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}