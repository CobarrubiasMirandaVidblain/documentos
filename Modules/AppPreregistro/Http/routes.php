<?php

Route::post('apppreregistro/api/v1/login', 'Modules\AppPreregistro\Http\Controllers\AppPreregistroController@login');

Route::post('apppreregistro/api/v1/logout', 'Modules\AppPreregistro\Http\Controllers\AppPreregistroController@logout');

Route::get('apppreregistro/api/v1/preregistro', 'Modules\AppPreregistro\Http\Controllers\AppPreregistroController@index');

Route::post('apppreregistro/api/v1/preregistro', 'Modules\AppPreregistro\Http\Controllers\AppPreregistroController@store');

// Route::post('apppreregistro/api/v1/crear/solicitud/interna', 'Modules\AtnCiudadana\Http\Controllers\AtnCiudadanaController@crearSolicitudInterna');

Route::group(['middleware' => ['cors', '\Modules\AppPreregistro\Http\Middleware\CheckToken::class'], 'prefix' => 'apppreregistro', 'namespace' => 'Modules\AppPreregistro\Http\Controllers'], function() {
	Route::resource('api/v1/beneficarios', 'Api\v1\BeneficiarioController', ['as' => 'api.v1.apppreregistro.beneficarios']);
	Route::resource('api/v1/solicitudes', 'Api\v1\SolicitudController', ['as' => 'api.v1.apppreregistro.solicitudes']);
});

Route::get('tequio','Modules\AppPreregistro\Http\Controllers\presolicitudes@tequio');