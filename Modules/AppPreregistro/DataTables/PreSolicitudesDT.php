<?php

namespace Modules\appPreregistro\DataTables;

use Modules\AppPreregistro\Entities\GiraPreregistro;
use App\Models\Beneficiosprograma;
use Modules\AppPreregistro\Entities\GiraPreregistroSolicitudPrograma;
use Illuminate\Support\Facades\DB;

use Yajra\DataTables\Services\DataTable;

class PreSolicitudesDT extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addIndexColumn()
        ->addColumn('completo', function($query) {
            return "$query->nombre $query->primer_apellido $query->segundo_apellido";
        })
        ->filterColumn('completo', function($query, $keyword){
            $query->where(function ($qry) use ($keyword){
                $qry->orWhere('atnc_giraspreregis.nombre','like',"%$keyword%")
                ->orWhere('atnc_giraspreregis.primer_apellido','like',"%$keyword%")
                ->orWhere('atnc_giraspreregis.segundo_apellido','like',"%$keyword%");
            });
        })
        ->addColumn('folio', function($query) {
            if($query->observaciones)
            return explode('|',$query->observaciones)[0];
            return '';
        })
        ->filterColumn('folio',function ($query,$keyword){
            $query->where('observaciones','like',"keyword|%");
        })
        ->addColumn('area', function($query) {
            if($query->observaciones)
            return explode('|',$query->observaciones)[1];
            return '';
        })
        ->filterColumn('area',function ($query,$keyword){
            $query->where('observaciones','like',"%|keyword|%");
        })
        ->addColumn('color', function($query) {
            if($query->observaciones)
            return explode('|',$query->observaciones)[2];
            return '';
        })
        ->filterColumn('color',function ($query,$keyword){
            $query->where('observaciones','like',"%|keyword|%");
        })
        ->addColumn('textura', function($query) {
            if($query->observaciones)
            return explode('|',$query->observaciones)[3];
            return '';
        })
        ->filterColumn('textura',function ($query,$keyword){
            $query->where('observaciones','like',"%|keyword");
        })
        ->addColumn('consultar', function($query) {
            return '<button style="margin-right: 10px;" class="btn btn-info btn-xs" data-toggle="tooltip" title="Consultar" onclick="mostrarSolicitud('.
            $query->id .',' . $query->beneficio_id . ')"><i class="fa fa-address-card"></i> Consultar</button>';
        })
        ->rawColumns(['consultar'])
        ->with('solicitantes',GiraPreregistro::count())
        ->with('nuevas',GiraPreregistroSolicitudPrograma::count())
        ->with('viejas',GiraPreregistro::onlyTrashed()->count())
        ->with('beneficio',Beneficiosprograma::find(GiraPreregistroSolicitudPrograma::select(DB::raw('count(*) as ct,beneficioprograma_id as bn'))->groupBy('beneficioprograma_id')->orderBy('ct','desc')->first()->bn)->nombre);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = GiraPreregistro::
        select(
            [
                'atnc_giraspreregis.id as id',
                'atnc_giraspreregis.folio as folio',
                'atnc_cat_giras.nombre as gira',
                'cat_municipios.nombre as municipio',
                'atnc_giraspreregis.nombre as nombre',
                'atnc_giraspreregis.primer_apellido as primer_apellido',
                'atnc_giraspreregis.segundo_apellido as segundo_apellido',
                'beneficiosprogramas.nombre as beneficio',
                'beneficiosprogramas.id as beneficio_id',
                'atnc_giraspreregis.colonia',
                'atnc_giraspreregis.calle',
                'atnc_giraspreregis.observaciones',
                'usuarios.token as dependencia'
            ]
        )
        ->leftjoin('even_eventos', 'even_eventos.id', '=', 'atnc_giraspreregis.evento_id')
        ->leftjoin('atnc_cat_giras', 'atnc_cat_giras.id', '=', 'even_eventos.gira_id')

        ->leftjoin('cat_municipios', 'cat_municipios.id', '=', 'even_eventos.municipio_id')

        ->leftjoin('atnc_giraspreregis_beneficiosprog', 'atnc_giraspreregis_beneficiosprog.girapreregis_id', '=', 'atnc_giraspreregis.id')
        ->leftjoin('beneficiosprogramas', 'beneficiosprogramas.id', '=', 'atnc_giraspreregis_beneficiosprog.beneficioprograma_id')
        
        ->join('usuarios','usuarios.id','atnc_giraspreregis.usuario_id');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns =
        [
            //'id' => ['data' => 'id', 'name' => 'atnc_giraspreregis.id'],
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
            // 'folio' => ['data' => 'folio', 'name' => 'atnc_giraspreregis.folio'],
            'gira' => ['data' => 'gira', 'name' => 'atnc_cat_giras.nombre'],
            'completo' => [],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'colonia' => ['data' => 'colonia', 'name' => 'atnc_giraspreregis.colonia','visible'=>false,'exportable'=>true],
            'calle' => ['data' => 'calle', 'name' => 'atnc_giraspreregis.calle','visible'=>false,'exportable'=>true],
            'dependencia' => ['name' => 'usuarios.token'],
            'folio' => [],
            'area' => ['searchable'=>false,'orderable'=>false],
            'color' => ['searchable'=>false,'orderable'=>false],
            'textura' => ['searchable'=>false,'orderable'=>false],
            // 'nombre' => ['data' => 'nombre', 'name' => 'atnc_giraspreregis.nombre', 'orderable' => false, 'searchable' => false],
            // 'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'atnc_giraspreregis.primer_apellido','orderable' => false, 'searchable' => false],
            // 'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'atnc_giraspreregis.segundo_apellido','orderable' => false, 'searchable' => false],
            'beneficio' => ['data' => 'beneficio', 'name' => 'beneficiosprogramas.nombre','orderable' => true, 'searchable' => true],
            'detalle' => ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Bltipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); actualizaNumeros(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10,-1], [10,'all'] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'rowGroup' => [
                'dataSrc' => 'gira'
            ],
            'order' => [
                1,
                'desc'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'atnciudadana/PreSolicitudes_' . date('YmdHis');
    }
}