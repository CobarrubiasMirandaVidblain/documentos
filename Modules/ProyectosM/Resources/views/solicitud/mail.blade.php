
{{-- <h5>Tienes una nueva solicitud de Monte de Piedad</h5>
<h5>Area: DIRECCION DE PLANEACION Y MONITOREO</h5>
<h4>Folio:  DIFO/MP/DPM/1/2019</h4> --}}

<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nueva solicitud</title>
</head>
<body>
    <p>Se genero una nueva solicitud de Monte de Piedad</p>
    <p>Estos son los datos de la solicitud:</p>
    <ul>
        <li>Folio: {{ $datos->folio }}</li>
        <li>Area: {{ $area->nombre}}</li>
        <li>Total: {{ $datos->total }}</li>
        <li>Fecha: {{ $datos->created_at }}</li>
    </ul>
    <a href="http://intradif.developersoax.com.mx:8080/proyectosm/solicitud/lista" type="button" class="button"  style="background-color: #4CAF50; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px;">Revisar</a>
</body>
</html>