@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
<style type="text/css">
	[v-cloak] {display: none;}
</style>
@stop

@section('content-title', 'Solicitud')
@section('content-subtitle', 'Monte de Piedad')

@section('content')
<section>
<div id="app">
    <div v-cloak>	
        <div class="row">
            {{-- @if(auth()->user()->hasRoles(['DIRECTOR','PLANEACION'])) --}}
            @if(auth()->user()->hasRolesStrModulo(['DIRECTOR','PLANEACION'], 'proyectosm'))
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Presupuesto Total:</span>
                            <span class="info-box-number">$ @{{ formatPesos(presupuesto.total)}}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                Presupuesto total
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Comprobado</span>
                            <span class="info-box-number">$ @{{ formatPesos(presupuesto.comprobado)}}</span>
                            <div class="progress">
                                {{-- <div class="progress-bar" :style="'width: '+(((presupuesto.comprobado)/presupuesto.total)*100)+'%'"></div> --}}
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                {{-- @{{(((presupuesto.comprobado)/presupuesto.total)*100)}} % Comprobado --}}
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-hourglass-2"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Sin comprobar</span>
                            <span class="info-box-number">$ @{{formatPesos(presupuesto.sincomprobar)}}</span>
                            <div class="progress">
                                {{-- <div class="progress-bar" :style="'width: '+(((presupuesto.total-presupuesto.comprobado)/presupuesto.total)*100)+'%'"></div> --}}
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                {{-- @{{(((presupuesto.sincomprobar)/presupuesto.total)*100).toFixed(0)}} % Sin Comprobar --}}
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-hand-pointer-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Solicitudes</span>
                            <span class="info-box-number"># @{{numsolicitudes}}</span>
                            {{-- <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                                70% Increase in 30 Days
                            </span> --}}
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"> <i class="fa fa-edit"></i> Nueva solicitud <small>Area: @{{solicitud.area.nombre}}</small></h3>
                        {{-- <h3 class="box-title"> <i class="fa fa-edit"></i> Nueva solicitud <small>Area: @{{solicitud.area.nombreD}}</small></h3> --}}
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-12">
                                <div  :class="{ 'form-group': true, 'has-error': errors.has('justificacion') }">
                                    <label>Justificación</label>
                                    <textarea rows="2" cols="50" ref="editor" id="editor" class="form-control input-sm" v-model="solicitud.justificacion" v-validate="'required:true'" name="justificacion"></textarea>
                                </div>
                            </div>
                            {{-- Agregar Productos --}}
                            <div class="col-md-12">
                                <h4 class="text-green"> <i class="fa fa-asterisk"></i> Agregar Productos</h3>
                                <hr>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Producto</label>
                                    <v-select :options="products" label="productoNombre" v-model="producto" @search="findProduct" :filterable='false'></v-select>
                                    {{-- <input type="text" class="form-control input-sm" v-model="producto" > --}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Cantidad</label>
                                    <input type="text" class="form-control input-sm" v-model="cantidad" >
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Precio</label>
                                    <input type="text" class="form-control input-sm" v-model="precio">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Observación</label>
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control" v-model="observacion">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-info btn-flat" @click.prevent="addProducto()"><i class="fa fa-plus"></i></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h3 class="box-title text-green"> <i class="fa fa-list-alt"></i> Lista de Productos</h3>
                                        <div class="box-tools pull-right">
                                        
                                            {{-- <span data-toggle="tooltip" title="Numero de productos" class="badge" data-original-title="Numero de productos"> # @{{solicitud.listProductos.length}}</span> --}}
                                            <span data-toggle="tooltip" title="Total" class="badge bg-green" data-original-title="Numero de productos"> $  @{{formatPesos(solicitud.sumatoria)}}</span>
                                        </div>
                                    </div>

                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr class="text-muted">
                                                    <th>Cantidad</th>
                                                    <th>Producto</th>
                                                    <th>Presentación</th>
                                                    <th>Descripción</th>
                                                    <th>Precio</th>
                                                    <th>Total</th>
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(p,index) in solicitud.listProductos">
                                                    <th style="width: 5%;">@{{p.cantidad}}</th>
                                                    <th style="width: 10%;">@{{p.producto}}</th>
                                                    <th style="width: 6%;">@{{p.presentacion}}</th>
                                                    <th style="width: 50%;">
                                                        <p class="text-justify">
                                                            @{{p.observacion}}
                                                        </p>
                                                    </th>
                                                    <th style="width: 15%;">$ @{{formatPesos(p.precio)}} </th>
                                                    <th style="width: 15%;">$ @{{formatPesos(p.precio * p.cantidad)}}</th>
                                                    <th>
                                                        <button class="btn btn-primary btn-sm" @click.prevent="deleteProducto(index, p.precio, p.cantidad)"><i class="fa fa-times"></i></button>
                                                    </th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="col-md-12" align ="right">
                                <button type="button" class="btn btn-success btn-sm" @click.prevente="saveSolicitud()"><i class="fa fa-send-o "></i> Solicitar</button>
                            </div>
                        </div>               
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>
@stop

@section('other-scripts')
<script>
Vue.use(VeeValidate);
Vue.component('v-select', VueSelect.VueSelect);

new Vue({
    el: '#app',
    data: {
        content:'',
        solicitud:{
            fecha:'',
            fecha_entrega:'',
            justificacion:'',
            area:{},
            listProductos:[],
            sumatoria:0,
            listcategoria:[]
        },
        presupuesto:{
            total:33600000,
            comprobado:0,
            sincomprobar:0
        },
        numsolicitudes:0,
        producto:'',
        cantidad:'',
        precio:'',
        observacion:'',
        disponible:'',
        comprometido:'',
        products:[],
    },
    computed: {},
    methods: {
        datosInicio(){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/datos";
            axios.get(url).then(function (response) {
                unblock();
                me.presupuesto.sincomprobar = response.data.sincomprobar;
                me.presupuesto.comprobado = response.data.comprobado;
                me.solicitud.fecha = response.data.fecha;
                me.solicitud.area = response.data.empleado.area;
                me.numsolicitudes = response.data.solicitudes;
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        addProducto(){
            let me = this;
            // if (me.producto && me.cantidad && me.precio && me.observacion)
            if (me.producto && me.cantidad && me.precio > 0 && me.observacion)
            {
                if((parseFloat(me.cantidad).toFixed(2)*parseInt(me.precio)) + parseInt(me.comprometido) > parseInt(me.disponible))
                    return Swal.fire({type: 'error', title: 'Recurso insuficiente', text: 'Cumunicate a la DIRECCION DE PLANEACION Y MONITOREO',  footer: 'Ext. 1181'})
                    
                 // sumatoria de precios por subprogramas de los productos agregados

                let listCat = me.solicitud.listcategoria;
                listCat = listCat.filter(p => {
                    return p.nomCategoria == me.producto.categoria.nombre;
                })
                if (listCat.length > 0) {
                    if(parseInt(listCat[0].comprometido) + parseInt(listCat[0].total)  + (parseFloat(me.cantidad).toFixed(2)*parseInt(me.precio)) > parseInt(listCat[0].disponible))
                        return  me.notificacion('error','Credito Insuficiente, Comunicate ah planeacion')
                    listCat[0].total = parseInt(listCat[0].total)  + (parseFloat(me.cantidad).toFixed(2)*parseInt(me.precio))
                }
                else
                    me.solicitud.listcategoria.push({'nomCategoria':me.producto.categoria.nombre,'total':(parseFloat(me.cantidad).toFixed(2)*parseInt(me.precio)), 'idCategoria':me.producto.categoria.id, 'disponible':me.producto.categoria.disponible, 'comprometido':me.producto.categoria.comprometido});

                let productoFind = me.solicitud.listProductos;
                productoFind = productoFind.filter(p => {
                    return p.producto == me.producto.producto.nombre && p.observacion == me.observacion;
                })
                if (productoFind.length > 0) {
                    // if(((parseInt(productoFind[0].cantidad) + parseInt(me.cantidad)) * parseInt(me.precio)) + parseInt(me.comprometido) > parseInt(me.disponible))
                    //     return  me.notificacion('error','Credito Insuficiente, Comunicate ah planeacion')
                    // me.solicitud.sumatoria = parseFloat(me.solicitud.sumatoria.toFixed(2)) + (parseInt(me.cantidad)*parseFloat(me.precio.toFixed(2)))
                    productoFind[0].cantidad = parseFloat(productoFind[0].cantidad).toFixed(2)  + parseFloat(me.cantidad).toFixed(2)
                    productoFind[0].precio = parseFloat(productoFind[0].precio.toFixed(2)) * parseFloat(productoFind[0].cantidad).toFixed(2)
                }
                else
                {
                    me.solicitud.listProductos.push({'cantidad':me.cantidad,'producto':me.producto.producto.nombre,'precio':parseFloat(me.precio).toFixed(2),'observacion':me.observacion,'presentacion_producto_id':me.producto.id,'presentacion':me.producto.presentacion.presentacion, 'nombreCat':me.producto.categoria.nombre});
                    // me.solicitud.sumatoria = parseFloat(me.solicitud.sumatoria) + (parseFloat(me.cantidad).toFixed(2)*parseFloat(me.precio).toFixed(2))
                }

								me.solicitud.sumatoria = me.solicitud.listProductos.map(e => Number.parseFloat(e.cantidad * e.precio)).reduce((valAnt,ValAct,i,v) => valAnt + ValAct);
               

                me.notificacion('success','Se agrego correctamente')
                me.producto = '';
                me.product = [];
                me.cantidad = '';
                me.precio = '';
                me.observacion = '';
                me.comprometido = '';
                me.disponible = '';
            }
            else
                me.notificacion('error','Existen campos vacios')
        },
        deleteProducto(index,precio,cantidad){
            let me = this;
            let listCat = me.solicitud.listcategoria;
            listCat = listCat.filter(p => {
                return p.nomCategoria == me.solicitud.listProductos[index].nombreCat;
            })
            if (listCat.length > 0) {
                listCat[0].total = parseInt(listCat[0].total)  - (parseFloat(cantidad).toFixed(2)*parseInt(precio))
            }

            me.solicitud.listProductos.splice(index,1);
            // me.solicitud.sumatoria = parseFloat(me.solicitud.sumatoria.toFixed(2)) - (parseFloat(precio.toFixed(2))*parseFloat(cantidad).toFixed(2))
						me.solicitud.sumatoria = me.solicitud.listProductos.length ? me.solicitud.listProductos.map(e => Number.parseFloat(e.cantidad * e.precio)).reduce((valAnt,ValAct,i,v) => valAnt + ValAct) :0

            // me.solicitud.listcategoria[index].total =  parseInt(me.solicitud.listcategoria[index].total) - (parseInt(precio) * parseInt(cantidad)) < 0 ? 0 : parseInt(me.solicitud.listcategoria[index].total) - (parseInt(precio) * parseInt(cantidad));
            me.notificacion('success','Se elimino correctamente')
        },
        findProduct(search, loading) {
            loading(true);
            this.searchP(loading, search, this);
        },
        searchP: _.debounce((loading, search, vm) => {
            fetch(`/proyectosm/solicitud/productos/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            var i = 0;
                            vm.products = json.map(item => {
                                item.productoNombre = item.producto.nombre + ' - ' + item.presentacion.presentacion;       
                                return item;
                            });
                        });
                    loading(false);
                });
        }, 350),
        saveSolicitud(){
            let me = this;
            this.$validator.validateAll().then((result) => {
            if(result) {
                if(me.solicitud.listProductos.length == 0)
                return  me.notificacion('error','No se puede crear una solicitud sin productos')
                var url = "/proyectosm/solicitud/save";
                Swal.fire({
                    title: 'Estas seguro?',
                    text: "Se enviara a validacion, no lo podras editar",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, solicitar!'
                    }).then((result) => {
                    if (result.value) {
                        block();
                        axios.post(url, me.solicitud).then(function (response) {
                        unblock();
                        if(response.data.success)
                        {
                            me.solicitud = {
                                fecha:'',
                                fecha_entrega:'',
                                justificacion:'',
                                listProductos:[],
                                sumatoria:0,
                                area:{},
                            },
                            me.products = [];
                            me.producto = '';
                            me.cantidad = '';
                            me.precio = '';
                            me.comprometido = '';
                            me.disponible = '';
                            me.observacion = '';
                            me.notificacion('success','Se guardo correctamente con el folio :'+response.data.folio)
                            me.$validator.reset();
                            me.datosInicio();
                        }
                        else
                            Swal.fire({type: 'error', title: 'Recurso insuficiente', text: 'Cumunicate a la DIRECCION DE PLANEACION Y MONITOREO',  footer: 'Ext. 1181'})
                            // me.notificacion('error', response.data.message);

                        }).catch(function (error){
                            unblock();
                            console.log("Error "+error)
                        })
                    }
                }) 
            }
            else
                me.notificacion('error','Existen campos vacios')
            });  
        },
        notificacion(tipo,texto){
            // Swal({position: 'top-end', type: tipo, title: texto, showConfirmButton: false, timer: 1500})
            const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
            toast({type: tipo, title: texto})                      
        },
			formatPesos(n) {
				return new Intl.NumberFormat("en-ES",{minimumFractionDigits: 2}).format(n);
			},
    },
    mounted: function() { 
        this.datosInicio();
    },
    watch:{
        producto:function(){
            let me = this;
            if(me.producto){
                me.precio = me.producto.precio_estimado;
                me.observacion = me.producto.descripcion; 
                me.disponible = me.producto.categoria.disponible     
                me.comprometido = me.producto.categoria.comprometido     
            }            
        },
        precio:function(){
            let me = this;
            if(me.precio == 0)
                me.precio = ''
        }
    }
});
</script>
@stop
