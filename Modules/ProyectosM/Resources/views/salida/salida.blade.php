@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
@stop

@section('content-title', 'Salida')
@section('content-subtitle', 'Monte de Piedad')
@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>

<div id="salida">
  <div v-cloak>	
    <section class="content">
      <div class="btn-group-vertical" style="padding-bottom: 1px;">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalSalida" @click.prevent="abrirModal()"><i class="fa fa-plus"></i> Nuevo</button>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Lista de salidas</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Folio</th>
                    <th>Folio Solicitud</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="s in listaSalidas">
                    <th>@{{s.folio}}</th>
                    <th>@{{s.solicitud.folio}}</th>
                    <th>
                      <button class="btn btn-success btn-sm" title="Detalle" @click.prevent="showSalida(s.id)"><i class="fa fa-eye"></i></button>
                      <button class="btn btn-info btn-sm" title="Imprimir" @click.prevent="printPDF(s.id)"><i class="fa fa-print"></i></button>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item" v-if="pagination.current_page > 1">
                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
              </li>
              <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
              </li>
              <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-md-12" v-if="detalle">
          <ul class="timeline">
            <li class="time-label">
              <span class="bg-red">
                FOLIO: @{{salida.folio}}
              </span>
            </li>
            <li class="time-label">
              <span class="bg-gray">
                RECIBE : @{{salida.nombre_recibe}} @{{salida.primer_apellido_recibe}} @{{salida.segundo_apellido_recibe}} 
              </span>
              <span class="bg-gray">
                TRANSPORTA : @{{salida.nombre_transporta}} @{{salida.primer_apellido_transporta}} @{{salida.segundo_apellido_transporta}} 
              </span>
            </li>
            <li v-for="sp in salida.productos">
              <i class="fa fa-shopping-cart bg-blue"></i>
              <div class="timeline-item">
                <h3 class="timeline-header"><b>PRODUCTO:  @{{sp.detalle.producto.nombre}}</b> <small>Cantidad:   @{{sp.cantidad}}</small> </h3>
                <div class="timeline-body">
                  {{-- @{{sp.detalle.descripcion}} --}}
                  @{{sp.descProducto}}
                </div>
              </div>
            </li>
            
          </ul>
        </div>
      </div>
    </section>
    {{-- Modal Entrada --}}
    <div class="modal modal-primary fade" id="modalSalida" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Generar Salida</h4>
                </div>
                <div class="modal-body">
                  <div class="col-md-12">
                    <div class="row text-muted">
                      <div class="form-group">
                        <label for="folio">Folio</label>
                        <v-select :options="solicitudFolio" label="folio" v-model="solicitudSelect" @search="findProduct" :filterable='false'></v-select>
                        <br>
                      </div>
                      <div class="col-md-4 text-light-blue text-center">
                        <p><b> Folio:</b> <br> @{{solicitudSelect.folio}}</p>
                      </div>
                      <div class="col-md-4 text-light-blue text-center">
                        <p><b> Fecha de creacion:</b> <br> @{{solicitudSelect.created_at}}</p>
                      </div>
                      <div class="col-md-4 text-light-blue text-center">
                        <p><b> Estatus:</b> <br>  <small :class="solicitudSelect.estatus.color"> @{{solicitudSelect.estatus.estatus}}</small></p>
                      </div>
                      <div class="col-md-12 text-light-blue text-justify">
                        <p><b> Justificación:</b> <br> @{{solicitudSelect.justificacion}}</p>
                      </div>
                      <div class="col-xs-12">
                        <div class="box">
                          <div class="box-body table-responsive no-padding">
                            <table class="table table-hover text-muted">
                              <thead>
                                <tr>
                                  <th class="width: 10%;">Cantidad</th>
                                  <th>Producto</th>
                                  <th>Descripcion</th>
                                  <th>Opciones</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr v-for="(p,index) in productos">
                                  <template v-if="p.estatus_id == 7 || p.estatus_id == 8">
                                      <td width="25%" class="text-center">@{{p.cantidad}}</td>
                                      <td width="30%">@{{p.nombre}} - @{{p.presentacion}}</td>
                                      <td width="45%">@{{p.descripcion}}</td>
                                      <td width="10%" v-if="p.estatus_id == 7 || p.estatus_id == 8">
                                          <button type="button" class="btn btn-box-tool label-warning" @click.prevent="opcionesEdit(1,index)"><i class="fa fa-minus"></i></button>
                                          <button  type="button" class="btn btn-box-tool label-danger" @click.prevent="opcionesEdit(3,index)"><i class="fa fa-trash"></i></button>
                                      </td>
                                      <td v-else-if="p.estatus_id == 1">
                                        <span class="pull-right badge bg-gray">No disponible</span>
                                      </td>
                                      <td v-else>
                                        <span class="pull-right badge bg-green">Entregado</span>
                                      </td>
                                  </template>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <h5>* Datos de quien transporta los productos</h4>
                      </div>
                      <div class="col-md-4 text-muted">
                        <div :class="{ 'form-group': true, 'has-error': errors.has('nombreEnt') }">
                          <label for="segundoAP">Nombre</label>
                          <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="nombreEnt" v-model="solicitudSelect.nombreEnt">
                        </div>
                      </div>
                      <div class="col-md-4 text-muted">
                        <div :class="{ 'form-group': true, 'has-error': errors.has('primerAPEnt') }">
                          <label for="segundoAP">Primer Apellido</label>
                          <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="primerAPEnt" v-model="solicitudSelect.primerAPEnt">
                        </div>
                      </div>
                      <div class="col-md-4 text-muted">
                        <div :class="{ 'form-group': true, 'has-error': errors.has('segundoAPEnt') }">
                          <label for="segundoAP">Segundo Apellido</label>
                          <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="segundoAPEnt" v-model="solicitudSelect.segundoAPEnt">
                        </div>
                      </div>

                      <div class="col-md-12">
                        <h5>* Datos del beneficiario</h4>
                      </div>
                      <div class="col-md-4 text-muted">
                        <div :class="{ 'form-group': true, 'has-error': errors.has('nombreBen') }">
                          <label for="segundoAP">Nombre</label>
                          <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="nombreBen" v-model="solicitudSelect.nombreBen">
                        </div>
                      </div>
                      <div class="col-md-4 text-muted">
                        <div :class="{ 'form-group': true, 'has-error': errors.has('primerAPBen') }">
                          <label for="segundoAP">Primer Apellido</label>
                          <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="primerAPBen" v-model="solicitudSelect.primerAPBen">
                        </div>
                      </div>
                      <div class="col-md-4 text-muted">
                        <div :class="{ 'form-group': true, 'has-error': errors.has('segundoAPBen') }">
                          <label for="segundoAP">Segundo Apellido</label>
                          <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="segundoAPBen" v-model="solicitudSelect.segundoAPBen">
                        </div>
                      </div>
                    </div>
                  </div>                       
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                    <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="savesalida()"><i class="fa fa-save"></i> Guardar Salida</button>
                </div> 
            </div>
        </div>
    </div>
    {{-- Fin modal Entrada --}}
  </div>
</div>
@stop

@section('other-scripts')
<script>
  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);
  new Vue({
    el: '#salida',
    data: {
      offset: 2,
      pagination:{},
      listaSalidas:[],
      solicitudFolio:[],
      solicitudSelect:{
        folio:'',
        productos:{},
        estatus:{
          color:''
        },
        nombreEnt:'',
        primerAPEnt:'',
        segundoAPEnt:'',
        nombreBen:'',
        primerAPBen:'',
        segundoAPBen:'',
      },
      productos:{},
      salida:{},
      detalle:false
    },
    computed: {
      isActive(){
        return this.pagination.current_page;
      },
      pageNumber(){
        if(!this.pagination.to){
          return [];
        }
        var desde = this.pagination.current_page - this.offset;
          if(desde < 1){
            desde = 1;
          }
        var hasta = desde + (this.offset * 2);
          if(hasta >= this.pagination.last_page) {
            hasta = this.pagination.last_page;
          }
        var pagesArray = [];
          while(desde <= hasta) {
            pagesArray.push(desde);
            desde++;
          }
        return pagesArray;
      }
    },
    methods: {
      cambiarPagina(page){
        this.salidasGet(page);
      },
      salidasGet(page){
        block();
        let me = this;
        var url = "/proyectosm/salidas/showList?page="+page;
        axios.get(url).then(function (response) {
          unblock();
          me.listaSalidas = response.data.data
          me.pagination = response.data;
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      },
      notificacion(tipo,texto){
        // Swal({position: 'top-end', type: tipo, title: texto, showConfirmButton: false, timer: 1500})
        const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
        toast({type: tipo, title: texto})                      
      },
      findProduct(search, loading) {
            loading(true);
            this.searchP(loading, search, this);
        },
      searchP: _.debounce((loading, search, vm) => {
          fetch(`/proyectosm/salidas/folio/${escape(search)}`)
              .then(res => {
                  res.json()
                      .then(json => {                        
                          vm.solicitudFolio = json
                      });
                  loading(false);
              });
      }, 350),
      opcionesEdit(tipo,index)
      {
        let me = this;
        if(tipo==1)
            me.productos[index].cantidad = (parseInt(me.productos[index].cantidad) - 1) <= 0? 1:parseInt(me.productos[index].cantidad) - 1;                
        // else if(tipo == 2)
        //     me.productos[index].cantidad = parseInt(me.productos[index].cantidad) + 1;
        else if(tipo == 3)
            me.productos.length == 1?me.notificacion('error','No puede quedar la lista vacia'):me.productos.splice(index,1)
      },
      savesalida()
      {
        let me = this;
        this.$validator.validateAll().then((result) => {
          if(result) {
            block();
            var prodSave = 0;
            for (i = 0; i < me.productos.length; i++) { 
                if(me.productos[i].estatus_id == 7)
                  prodSave++
            }
            if(prodSave == 0)
              return me.notificacion('error','Lista de productos vacia ');

            me.solicitudSelect.productos = me.productos
            var url = "/proyectosm/salidas/save";
            axios.post(url,me.solicitudSelect).then(function (response) {
              unblock();
              if(response.data.success)
              {
                me.salidasGet(1);
                $('#modalSalida').modal('hide');
                me.productos = '';
                me.solicitudSelect = {
                  folio:'',
                  estatus:{ }
                };
                me.notificacion('success','Se guardo correctamente con el folio :'+response.data.folio)
                me.$validator.reset();
              }
              else
                me.notificacion('error','Ocurrio un error durante el guardado')
            }).catch(function (error){
              unblock();
              console.log("Error "+error)
            })
          }
          else
            me.notificacion('error','Existen campos vacios'); 
        });
      },
      showSalida(id){
        block();
        let me = this;
        var url = "/proyectosm/salidas/"+id;
        axios.get(url).then(function (response) {
          unblock();
          me.salida = response.data.salida;
          me.detalle = true;
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      },
      printPDF(id){
        block();
        let me = this;
        var url = "/proyectosm/salidas/"+id;
        axios.get(url).then(function (response) {
          fecha = response.data.salida.created_at.split("-")
          dia = fecha[2].split(" ")
          // unblock();
          jsreport.serverUrl = '{{config('app.reporter_url')}}';
          var request = {
            template:{
              shortid: "BJhh4gFQN" // PMReciboExterno
            },
            header: {
              Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
            },
            data:{
              salida: response.data.salida,
              usuario: response.data.user,
              anioo:fecha[0],
              mess:fecha[1],
              diaa:dia[0],
            }
        };
        // jsreport.render('_blank', request);
        jsreport.renderAsync(request).then(function(res) {
          var url = res.toDataURI();
          var url_with_name = url.replace("data:application/pdf;", "data:application/pdf;name=solicitud.pdf;")
          var html = '<html>' +
              '<style>html, body { padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}  </style>' +
              '<body>' +
              '<iframe type="application/pdf" src="' + url_with_name + '"></iframe>' +
              '</body></html>';
          var a = window.open("about:blank", "solicitud");
          a.document.write(html);
          a.document.close();
          unblock();
        }) 
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      },
      abrirModal(){
        let me = this;
        me.productos = '';
        me.solicitudFolio =[];
        me.solicitudSelect = {
          folio:'',
          estatus:{ }
        };
        me.$validator.reset();
      },
    },
    mounted: function() {
      let me = this;
      me.salidasGet(1);
    },
    watch:{ 
      'solicitudSelect.folio':function(){
        let me = this;
        if(me.solicitudSelect.folio){
          var url = "/proyectosm/solicitud/listaProd/"+me.solicitudSelect.id;
          axios.get(url).then(function (response) {
             me.productos = response.data
          }).catch(function (error){
              console.log("Error "+error)
          })     
        }
      },
    }
  });
</script>
@stop


