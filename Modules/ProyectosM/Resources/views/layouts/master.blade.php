@extends('vendor.admin-lte.layouts.main')

@if (auth()->check())
<!-- @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia())) -->
<!-- @section('user-name', auth()->user()->persona->nombre) -->
@section('user-job')
@section('user-profile', route('users.edit', auth()->user()->id))
<!-- @section('user-log', auth()->user()->created_at) -->
@endif

@push('head')
@yield('mystyles')
@endpush

@section('sidebar-menu')
<ul class="sidebar-menu">
    <li class="header">NAVEGACIÓN</li>
    <li class="active">
        <a href="{{ route('home') }}"><i class="fa fa-home"></i><span>Home</span></a>
    </li>    
    <li>
        <a href="{{ route('inicio') }}"><i class="fa fa-home"></i><span>Asignacion</span></a>
    </li>
    <li class="treeview">
        <a href="#">
          <i class="fa fa-sticky-note-o"></i> <span>Solicitud</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('lista') }}"><i class="fa fa-list-ol"></i> Lista Solicitud</a></li>
            @if(auth()->user()->hasRolesStrModulo(['PLANEACION','USUARIO'], 'proyectosm'))
            {{-- @if(auth()->user()->hasRoles(['PLANEACION','ADMINISTRADOR','USUARIO'])) --}}
                <li><a href="{{ route('nuevo') }}"><i class="fa fa-pencil-square"></i> Nuevo</a></li>
                <li><a href="{{ route('in') }}"><i class="fa fa-sign-in"></i> Nueva entrada</a></li>
                <li><a href="{{ route('exit') }}"><i class="fa fa-sign-out"></i> Salida</a></li>
            @endif
            {{-- <li><a href="/recmat/almacen#/reimpresion"><i class="fa fa-sign-out"></i> Listas Salida</a></li> --}}
        </ul>
    </li>
    {{-- @if(auth()->user()->hasRoles(['ADMINISTRADOR','PLANEACION'])) --}}
    @if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
    <li class="treeview">
        <a href="#">
            <i class="fa fa-th"></i> <span>Productos</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('productos')}}">Agregar Producto</a></li>
            <li><a href="{{ route('listaProductos')}}">Listado</a></li>
        </ul>
    </li>
    @endif
</ul>
@endsection

@section('message-url-all', '#')
@section('message-all', 'Ver todos los mensajes')
@section('message-number')
@section('message-text', 'Tienes 1 mensages')
@section('message-list')
<!-- start message -->
<li>
  <a href="#">
      <div class="pull-left">
          <!-- User Image -->
          <img src="https://www.gravatar.com/avatar/?d=mm" class="img-circle" alt="User Image">
      </div>
      <!-- Message title and timestamp -->
      <h4>
          Support Team
          <small><i class="fa fa-clock-o"></i> 5 mins</small>
      </h4>
      <!-- The message -->
      <p>Why not buy a new awesome theme?</p>
  </a>
</li>
<!-- end message -->
@endsection

@section('notification-id', 'notificaciones')
@section('notification-link', 'notificaciones-link')
@section('notification-dropdown-ul', 'notificaciones-ul')
@section('notification-number')
{{-- @section('notification-text', 'Hola mundo') --}}
@section('notification-dropdown-id', 'notificaciones-titulo')
{{-- @section('notification-list')
<a href="/recmat">
    <i class="fa fa-refresh text-yellow" id="reinicio"></i> Recargue la pagina
</a>
@endsection --}}
@section('notification-dropdown-li-id', 'notificaciones-lista')

@section('task-number')
@section('task-text', 'Tienes 1 tarea')
@section('task-url-all')

@endsection
@section('task-list')
<li>
  <a href="#">
      <!-- Task title and progress text -->
      <h3>
          Design some buttons
          <small class="pull-right">20%</small>
      </h3>
      <!-- The progress bar -->
      <div class="progress xs">
          <!-- Change the css width attribute to simulate progress -->
          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
              <span class="sr-only">20% Complete</span>
          </div>
      </div>
  </a>
</li>
@endsection

@push('body')

<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/vue-router.js') }}"></script>
<script src="{{ asset('js/vue-select.js') }}"></script>
<script src="{{ asset('js/vee-validate.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.all.js') }}"></script>
<script src="{{ asset('js/jsreport.js') }}"></script>
<script src="{{ asset('js/numero_letras.js') }}"></script>
<script src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ asset('plugins/loaders/block.js') }}"></script>

<script type="text/javascript">
	function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff',
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}
	}

	function unblock() {
		$.unblockUI();
	}	
</script>
@yield('cdn-scripts')
@yield('other-scripts')

@endpush