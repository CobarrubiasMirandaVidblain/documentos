@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
@stop

@section('content-title', 'Recursos')
@section('content-subtitle', 'Monte de Piedad')

@section('content')
<div id="app">        
   <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Asignacion</h3>
                    <div class="box-tools pull-right">
                        <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="3 New Messages">$ @{{cantidad}}</span>
                        <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 New Messages">$ @{{sumatoria}}</span>
                        <span data-toggle="tooltip" title="" class="badge bg-light-blue" data-original-title="3 New Messages">$ @{{cantidad - sumatoria}}</span>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="cantidad">Cantidad</label>
                        <input type="number" class="form-control" id="cantidad" autocomplete="off" min="0" v-model="cantidad">
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="categoria">Categoria</label>
                                    <input type="text" class="form-control" id="categoria" autocomplete="off" v-model="categoria">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="descripcion">Descripcion</label>
                                    <input type="text" class="form-control" id="descripcion" autocomplete="off" v-model="descripcion">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label for="cantidadC">Cantidad</label>
                                <div class="input-group input-group-sm">    
                                    <input type="number" class="form-control" autocomplete="off" min="1" v-model="cantidadC">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-info btn-flat" @click.prevent="addList(1)">Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title">Lista de categorias</h3>
                            </div>
                            <div class="box-body">
                                <div class="box-group" id="listPrep">
                                    <div class="panel box box-primary" v-for="(list, index) in listaCat">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">
                                                <a data-toggle="collapse" data-parent="#listPrep" :href="'#list'+list.categoria" aria-expanded="false" class="collapsed">
                                                    @{{list.categoria +' - '+ index}}
                                                </a>
                                            </h4>
                                            <div class="box-tools pull-right">
                                                <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="3 New Messages">Presupuesto: $ @{{list.cantidad}}</span>
                                                <span data-toggle="tooltip" title="" class="badge" data-original-title="3 New Messages" @click.prevent="addSubCategoria(list.categoria)"><i class="fa fa-plus-circle"></i></span>
                                                <span data-toggle="tooltip" title="" class="badge bg-red" data-original-title="3 New Messages" @click.prevent="deleteList(index, list.cantidad,1)"><i class="fa fa-trash"></i></span>
                                            </div>
                                        </div>
                                        <div :id="'list'+list.categoria" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="box-body">
                                               <h4>Descripcion : @{{list.descripcion}}</h4> 
                                                <div class="col-md-12">
                                                    <div class="box box-solid">
                                                        <div class="box-header with-border">
                                                            <h3 class="box-title">Sub categorias</h3>
                                                        </div>
                                                        <div class="box-body" >
                                                            <div class="box-group" :id="list.categoria">
                                                                <template v-for="(sub, indexSub) in listSubCat">
                                                                    <div class="panel box box-primary"  v-if="list.categoria == sub.id">
                                                                        <div class="box-header with-border">
                                                                            <h4 class="box-title">
                                                                                <a data-toggle="collapse" :data-parent="'#'+list.categoria" :href="'#'+sub.nombreSub">
                                                                                    @{{sub.nombreSub }}
                                                                                </a>
                                                                            </h4>
                                                                            <div class="box-tools pull-right">
                                                                                <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="3 New Messages">Presupuesto: $ @{{+sub.cantidad}}</span>
                                                                                <span data-toggle="tooltip" title="" class="badge bg-red" data-original-title="3 New Messages" @click.prevent="deleteList(indexSub, list.cantidad,2)"><i class="fa fa-trash"></i></span>
                                                                            </div>
                                                                        </div>
                                                                        <div :id="sub.nombreSub" class="panel-collapse collapse">
                                                                            <div class="box-body">
                                                                                @{{sub.descripcion}}
                                                                            </div>
                                                                        </div>                                                                    
                                                                    </div>
                                                                </template>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('other-scripts')
<script>
Vue.use(VeeValidate);
Vue.component('v-select', VueSelect.VueSelect);
new Vue({
			el: '#app',
			data: {
				cantidad:1000,
                sumatoria:0,
				categoria:'uno',
				descripcion:'example',
                cantidadC:'100',
                categoriaName:'',
                listaCat:[],
                listSubCat:[],
                saveMode:false
			},
			computed: {},
			methods: {
                addList(tipo){
                    let me = this;
                    if(parseInt(me.cantidadC)+parseInt(me.sumatoria) > parseInt(me.cantidad) || parseInt(me.cantidadC) == 0)
                        return Swal({position: 'top-end', type: 'error',  title: 'Cantidad mayor a la disponible', showConfirmButton: false, timer: 1500});

                    if(me.cantidadC && me.categoria && me.descripcion)
                    {
                        if(tipo == 1)
                            me.listaCat.push({'cantidad':me.cantidadC,'categoria':me.categoria,'descripcion':me.descripcion});
                        else
                            me.listSubCat.push({'cantidad':me.cantidadC,'categoria':me.categoria});

                        me.sumatoria = me.sumatoria? parseInt(me.sumatoria) + parseInt(me.cantidadC):parseInt(me.cantidadC);
                        me.cantidadC = ''
                        me.categoria = ''
                        me.descripcion = ''
                    }
                    else
                        Swal({position: 'top-end', type: 'error',  title: 'Existen campos vacios', showConfirmButton: false, timer: 1500});
                },
                deleteList(index, cantidad, tipo){
                    let me = this;
                    const swalWithBootstrapButtons = Swal.mixin({
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false,
                    });
                   
                    swalWithBootstrapButtons({
                    title: 'Estas seguro?',
                    text: "Se eliminara de la lista!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Si, Eliminar!',
                    cancelButtonText: 'No, cancelar!',
                    reverseButtons: true
                    }).then((result) => {
                    if (result.value) {
                        if(tipo == 1)
                        {
                            me.listaCat.splice(index,1);
                            me.sumatoria = me.sumatoria? parseInt(me.sumatoria) - parseInt(cantidad):'';
                        }
                        else{
                            me.listSubCat.splice(index,1);
                        }
                        
                        swalWithBootstrapButtons('Eliminado!', 'Se elimino correctamente.', 'success')
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        swalWithBootstrapButtons('Cancelado', 'No se realizo ningun cambio :)', 'error')
                    }
                    })
                },
                addSubCategoria(categoria, precio){
                    let me =this;
                    Swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Siguiente &rarr;',
                    // showCancelButton: true,
                    allowEscapeKey:false,
                    allowOutsideClick:false,
                    progressSteps: ['1', '2', '3']
                    }).queue([
                    {title: 'Nombre'},
                    {title: 'Descripción'},
                    {title: 'Presupuesto'}
                    ]).then((result) => {
                    if (result.value[0] && result.value[1] && result.value[2]) {
                        // if(parseInt(cantidad) - parseInt(result.value[2]))
                        me.listSubCat.push({'nombreSub':result.value[0],'descripcion':result.value[1],'cantidad':result.value[2],'id':categoria});
                        Swal({position: 'top-end', type: 'success', title: 'Todo bien', showConfirmButton: false, timer: 1500})
                    }
                    else
                        Swal({position: 'top-end', type: 'error', title: 'Existen campos vacios', showConfirmButton: false, timer: 1500})
                    })
                },
                selectCat(cat){
                    let me = this;
                    me.cantidad = cat.cantidad;
                    me.categoriaName = cat.categoria;
                    me.sumatoria = 0
                    me.cantidadC = ''
                    me.categoria = ''
                }
            },
			mounted: function() { }
		});

</script>
@stop
