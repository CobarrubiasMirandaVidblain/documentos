@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
<style type="text/css">
	[v-cloak] {display: none;}
</style>
@stop

@section('content-title', 'Inicio')
@section('content-subtitle', 'Monte de Piedad')

@section('content')

<div id="app">        
    <div v-cloak>
        <div class="row">
            {{-- @if(auth()->user()->hasRoles(['DIRECTOR','PLANEACION'])) --}}
            @if(auth()->user()->hasRolesStrModulo(['DIRECTOR','PLANEACION'], 'proyectosm'))
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Presupuesto Total:</span>
                            <span class="info-box-number">$ @{{presupuesto.total.toLocaleString("en-US")}}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                Presupuesto total
                            </span>
                        </div>
                    </div>
                </div>
    
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Comprobado</span>
                            <span class="info-box-number">$ @{{ presupuesto.comprobado.toLocaleString("en-US") }}</span>
                            <div class="progress">
                                {{-- <div class="progress-bar" :style="'width: '+(((presupuesto.comprobado)/presupuesto.total)*100)+'%'"></div> --}}
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                {{-- @{{(((presupuesto.comprobado)/presupuesto.total)*100)}} % Comprobado --}}
                            </span>
                        </div>
                    </div>
                </div>
    
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-hourglass-2"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Sin comprobar</span>
                            <span class="info-box-number">$ @{{presupuesto.sincomprobar.toLocaleString("en-US") }}</span>
                            <div class="progress">
                                {{-- <div class="progress-bar" :style="'width: '+(((presupuesto.total-presupuesto.comprobado)/presupuesto.total)*100)+'%'"></div> --}}
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                {{-- @{{(((presupuesto.sincomprobar)/presupuesto.total)*100).toFixed(0)}} % Sin Comprobar --}}
                            </span>
                        </div>
                    </div>
                </div>
                <a href="{{ route('lista') }}">
                    <div class="col-md-3 col-sm-6 col-xs-12" >
                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="fa fa-hand-pointer-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Solicitudes</span>
                                <span class="info-box-number"># @{{numsolicitudes}}</span>
                                {{-- <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                                    70% Increase in 30 Days
                                </span> --}}
                            </div>
                        </div>
                    </div>
                </a>
                @endif
                @if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Sub-Programas</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th COLSPAN=5>Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <template v-for="(p, index) in programasList">
                                        <tr align="center" >
                                            <th>@{{index+1}}</th>
                                            <th COLSPAN=6>@{{p.programa}}</th>
                                        </tr>
                                        <tr>
                                            <th :ROWSPAN="p.categorias.length + 1"></th>
                                            <td><b>#</b></td> 
                                            <td><b>Nombre</b></td> 
                                            <td><b>Presupuesto</b></td>
                                            <td><b>Ejercido</b></td>
                                            <td><b>Tope Candado</b></td>
                                            @if(auth()->user()->hasRolesStrModulo(['PLANEACION','ADMINISTRADOR'], 'proyectosm'))
                                                <td><b>Opciones</b></td>
                                            @endif
                                        </tr>
                                        <tr v-for="(sub, numero) in p.categorias" >
                                            <td>@{{numero + 1}}</td> 
                                            <td>@{{sub.nombre}}</td>
                                            <td :class="sub.presupuesto > sub.comprometido ? 'text-green' : 'text-red'">$ @{{sub.presupuesto.toLocaleString("en-US")}}</td>
                                            <td :class="sub.disponible > sub.comprometido ? 'text-green' : 'text-yellow'">$ @{{sub.comprometido.toLocaleString("en-US") }}</td>
                                            <td>$ @{{sub.disponible.toLocaleString("en-US")}}</td>
                                            @if(auth()->user()->hasRolesStrModulo(['PLANEACION','ADMINISTRADOR'], 'proyectosm'))
                                                <td> <button  type="button" class="btn btn-box-tool label-warning" @click.prevent="edit(sub)"><i class="fa fa-edit"></i></button></td>
                                            @endif
                                        </tr>
                                    </template>
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {{-- Modal --}}
    <div class="modal modal-primary fade" id="modalEdit"  style="posicion:center">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Editar </h4>
                </div>
                <div class="modal-body">
                    <div :class="{ 'form-group': true, 'has-error': errors.has('justificacion') }">
                        <label class="text-muted" for="Nombre">Nombre</label>
                        <input type="text" class="form-control"  placeholder="Nombre" v-model="programa.nombre" v-validate="'required:true'" name="justificacion">
                    </div>    
                    <div :class="{ 'form-group': true, 'has-error': errors.has('presupuesto') }">
                        <label class="text-muted" for="Presupuesto">Presupuesto</label>
                        <input type="text" class="form-control"  placeholder="Presupuesto" v-model="programa.presupuesto" v-validate="{numeric:true,required:true}" name="presupuesto">
                        {{-- <span class="text-red">@{{ errors.first('presupuesto') }}</span> --}}
                    </div>     
                    <div :class="{ 'form-group': true, 'has-error': errors.has('ejercido') }">
                        <label class="text-muted" for="Ejercido">Ejercido</label>
                        <input type="text" class="form-control"  placeholder="Ejercido" v-model="programa.ejercido" v-validate="{numeric:true,required:true}" name="ejercido">
                        {{-- <span class="text-red">@{{ errors.first('ejercido') }}</span> --}}
                    </div>  
                    <div :class="{ 'form-group': true, 'has-error': errors.has('topeCandado') }">
                        <label class="text-muted" for="topeCandado">Tope Candado</label>
                        <input type="text" class="form-control"  placeholder="topeCandado" v-model="programa.topeCandado" v-validate="{numeric:true,required:true}" name="topeCandado">
                        {{-- <span class="text-red">@{{ errors.first('topeCandado') }}</span> --}}
                    </div>                      
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                    <button type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="updatePrograma()"><i class="fa fa-floppy-o"></i> Guardar</button>
                </div> 
            </div>
        </div>
    </div>
    {{-- Fin Modal --}}
</div>
@stop

@section('other-scripts')
<script>
Vue.use(VeeValidate);
new Vue({
    el: '#app',
    data: {
        presupuesto:{
            total:33600000,
            comprobado:0,
            sincomprobar:0
        },
        numsolicitudes:0,
        programasList:{},
        programa:{
            nombre:'',
            presupuesto:0,
            ejercido:0,
            topeCandado:0,
            id:''
        }
    },
    computed: {},
    methods: {
        datosInicio(){
            let me = this;
            var url = "/proyectosm/solicitud/datos";
            axios.get(url).then(function (response) {
                me.presupuesto.sincomprobar = response.data.sincomprobar;
                me.numsolicitudes = response.data.solicitudes;
                me.presupuesto.comprobado = response.data.comprobado;
            }).catch(function (error){
                console.log("Error "+error)
            })
        },
        programas(){
            let me = this;
            var url = "/proyectosm/proyecto/datos";
            axios.get(url).then(function (response) {
                me.programasList = response.data
            }).catch(function (error){
                console.log("Error "+error)
            })
        },
        edit(dato){
            console.log(dato)
            let me = this;
            me.programa.nombre = dato.nombre;
            me.programa.presupuesto = dato.presupuesto;
            me.programa.topeCandado = dato.disponible;
            me.programa.ejercido   = dato.comprometido;
            me.programa.id   = dato.id;
            $('#modalEdit').modal('show');
        },
        updatePrograma(){
            let me = this;
            this.$validator.validateAll().then((result) => {
            if(result) {
                block();
                var url = "/proyectosm/categoria/update";
                axios.post(url,me.programa).then(function (response){
                    unblock();
                    me.datosInicio();
                    me.programas();
                    me.$validator.reset();
                    $('#modalEdit').modal('hide');
                    me.notificacion('success','Se guardo correctamente')
                }).catch(function (error){
                    unblock();
                    console.log("Error "+error.message)
                })
            }
            else
                me.notificacion('error','Existen campos vacios')
            });  
           
        },
        notificacion(tipo,texto){
            // Swal({position: 'top-end', type: tipo, title: texto, showConfirmButton: false, timer: 1500})
            const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
            toast({type: tipo, title: texto})                      
        }
    },
    mounted: function() { 
        this.datosInicio();
        this.programas();
    },
    watch:{ }
});

</script>
@stop
