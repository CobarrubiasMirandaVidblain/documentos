@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
@stop

@section('content-title', 'Productos')
@section('content-subtitle', 'Monte de Piedad')


@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>

<div id="producto">
  <div v-cloak>	
    <section class="content">
      
      <div class="row">
          <form role="form">
            <div class="box-body">
              <div class="form-group" :class="errors.first('nombreproducto') ? 'has-error' : ''" >
                <label for="producto">Nombre Producto</label>
                <input type="text" class="form-control" id="producto" placeholder="Enter nombre" name="nombreproducto" v-model="producto.nombre" v-validate="'required'" data-vv-validate-on="click">
                <span class="text-danger">@{{ errors.first('nombreproducto') }}</span>
              </div>
              <div class="form-group" :class="errors.first('descripcion') ? 'has-error' : ''" >
                <label for="producto">Descripcion</label>
                <input type="text" class="form-control" id="descripcion" placeholder="Enter Descripcion" name="descripcion" v-model="producto.descripcion" v-validate="'required'" data-vv-validate-on="click">
                <span class="text-danger">@{{ errors.first('descripcion') }}</span>
              </div>
              <div class="form-group" :class="errors.first('presentacion') ? 'has-error' : ''">
                <label>Presentación</label>
                <v-select taggable multiple :options="presentaciones" label="presentacion" v-model="presentacion" :filterable="false" @search="buscarPresentacion" name="presentacion" v-validate="'required'"></v-select>
                <span v-show="! errors.first('presentacion')">Seleccione o agregue al menos una presentacion. Puede agregar todas las presentaciones que necesite.</span>
                <span class="text-danger">@{{ errors.first('presentacion') }}</span>
              </div>
              <div class="form-group" :class="errors.first('categoria') ? 'has-error' : ''">
                <label>Categoria</label>
                <v-select :options="categorias" label="nombre" v-model="categoria_select" :filterable="false" @search="buscarCategoria" name="categoria" v-validate="'required'"></v-select>
                <span class="text-danger">@{{ errors.first('categoria') }}</span>
              </div>
  
            </div>
            <!-- /.box-body -->
  
            <div class="box-footer">
              <button type="submit" class="btn btn-primary" @click.prevent="validateBeforeGuardarProducto()">Guardar</button>
            </div>
          </form>
      </div>
    </section>
  </div>
</div>
@stop

@section('other-scripts')
<script>
  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);
  new Vue({
    el: '#producto',
    data: {
      producto: {
        nombre: '',
        descripcion:''
      },
      presentaciones: [],
      presentacion: null,
      precios: [],
      categorias: [],
      categoria_select: null
    },
    computed: { },
    methods: {
      buscarPresentacion(search, loading){
        loading(true);
        this.peticionPersona(loading, search, this);
      },
      peticionPersona: _.debounce((loading, search, vm) => {
        fetch(`/proyectosm/productos/presentaciones/${escape(search)}`)
          .then(res => {
            res.json()
              .then(json => {
                vm.presentaciones = json;
              });
            loading(false);
          });
      }, 350),

      buscarCategoria(search, loading){
        loading(true);
        this.peticionCategoria(loading, search, this);
      },

      peticionCategoria: _.debounce((loading, search, vm) => {
        fetch(`/proyectosm/productos/categoria/${escape(search)}`)
          .then(res => {
            res.json()
              .then(json => {
                vm.categorias = json;
              });
            loading(false);
          });
      }, 350),

      validateBeforeGuardarProducto(e) {
        this.$validator.validate().then(result => {
          if (!result) {
            swal({
              type: 'error',
              title: 'Oops...',
              text: 'Tiene unos detalles !'
            });
          }else{
            this.guardarProducto();
          }
        });
      },

      guardarProducto(){
        Swal.mixin({
          input: 'text',
          confirmButtonText: 'Next &rarr;',
          showCancelButton: true,
          progressSteps: this.presentacion.map( (item, key) => { return key + 1; } )
        }).queue(
          this.presentacion.map(item => { return {title: item.presentacion ? item.presentacion : item} })
        ).then((result) => {
          if (result.value) {
            axios.post('/proyectosm/productos/save', {
              producto: this.producto.nombre.toUpperCase(),
              presentaciones: this.presentacion,
              categoria: this.categoria_select,
              descripcion:this.producto.descripcion,
              precios: result.value.map(item => { return parseInt(item); }),
            })
              .then(response => {
                swal({
                  title: 'Registro agregado!',
                  text: 'ID: ' + response.data.id,
                  timer: 1000,
                  onOpen: () => {
                    swal.showLoading()
                  }
                }).then((result) => {
                  if (result.dismiss === swal.DismissReason.timer) { // Read more about handling dismissals
                  }
                });
                this.producto.nombre = '';
                this.precios = [];
                this.presentacion = null;
              })
              .catch(error => { 
                swal({
                  type: 'error',
                  title: 'Oops...',
                  text: 'Ocurrio algo mal!',
                  footer: '<a href>Why do I have this issue?</a>',
                })
                console.log(error.data); 
              });
          }
        })
      },
      notificacion(tipo,texto){
        // Swal({position: 'top-end', type: tipo, title: texto, showConfirmButton: false, timer: 1500})
        const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
        toast({type: tipo, title: texto})                      
      },
      
    },
    mounted: function() {},
    watch:{}
  });
</script>
@stop


