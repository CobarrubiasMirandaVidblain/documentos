@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
@stop

@section('content-title', 'Lista de Productos')
@section('content-subtitle', 'Monte de Piedad')


@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>

<div id="listaProd">
  <div v-cloak>	
    <section class="content">
        <div>
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Productos</h3>
                <a href="{{ route('productos')}}" class="btn btn-xs btn-outline btn-outline-primary"><i class="fa fa-plus"></i> Agregar Producto</a>
                <button class="btn btn-xs btn-outline btn-outline-primary" @click.prevent="loadProducts(1)"><i class="fa fa-refresh"></i></button>
                <div class="box-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" v-model="search">
  
                    <div class="input-group-btn">
                      <button type="submit" class="btn btn-default" @click.prevent="buscarProd()"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="example" class="table table-hover">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Producto</th>
                      <th>Categoria</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="p in products">
                      <td>@{{p.id}}</td>
                      <td>@{{p.nombre}}</td>
                      <template v-for="cat in p.categoria">
                        <td>@{{cat.nombre}}</td>
                      </template>
                      
                      <td><button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalProducto" @click.prevent="editProductModel(p)"><i class="fa fa-pencil"></i></button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="box-footer">
                <div class="col-md-12">
                  <nav aria-label="Page navigation example">
                      <ul class="pagination">
                          <li class="page-item" v-if="pagination.current_page > 1">
                              <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                          </li>
                          <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                              <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                          </li>
                          <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                              <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                          </li>
                      </ul>
                  </nav>
                </div>
              </div>
            </div>
            {{-- Modal Entrada --}}
            <div class="modal modal-primary fade" id="modalProducto">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Editar Producto</h4>
                  </div>
                  <form class="form-horizontal">
                    <div class="modal-body">
                      
                      <div class="form-group">
                        <label for="id" class="col-sm-3 control-label text-primary">ID</label>
                        <div class="col-sm-9">
                          <input type="number" class="form-control" id="id" v-model="editProduct.id" disabled>
                        </div>
                      </div>
                      <template v-for="cat in editProduct.categoria">
                        <div class="form-group">
                          <label for="id" class="col-sm-3 control-label text-primary">Categoria</label>
                          <div class="col-sm-9">
                            <input type="text" class="form-control" :value="cat.nombre" disabled>
                          </div>
                        </div>
                      </template>
                      
                      <div class="form-group">
                        <label for="producto" class="col-sm-3 control-label text-primary">Producto</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="producto" v-model="editProduct.nombre">
                        </div>
                      </div>
                      <div class="form-group" v-show="editMode">
                        <label for="producto" class="col-sm-3 control-label text-primary">Descripcion</label>
                        <div class="col-sm-9">
                          <textarea type="text" class="form-control" v-model="edicion.descripcion"></textarea>
                        </div>
                      </div>
                      <div class="form-group" v-show="editMode">
                        <label for="precio" class="col-sm-3 control-label text-primary">Precio</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" v-model="edicion.precio">
                        </div>
                      </div>
                      {{--  --}}
                      <div class="col-xs-12">
                          <div class="box">
                            <div class="box-body table-responsive no-padding">
                              <table class="table table-hover">
                                <thead>
                                  <tr class="text-primary">
                                    <th style="width: 20%;">Presentacion</th>
                                    <th style="width: 50%;">Descripción</th>
                                    <th style="width: 20%;">Precio</th>
                                    <th style="width: 10%;">Opciones</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-for="(edit,index) in editProduct.presentaciones" class="text-light-blue">
                                    <th style="width: 20%;">@{{edit.presentacion}}</th>
                                    <th style="width: 50%;">@{{edit.pivot.descripcion}} </th>
                                    <th style="width: 20%;">@{{edit.pivot.precio_estimado}}</th>
                                    <th style="width: 20%;">
                                      <button class="btn btn-primary btn-sm" title="Editar" @click.prevent="editarPre(index,edit)"><i class="fa fa-pencil"></i></button>
                                    </th>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <button type="button" class="btn btn-block btn-primary btn-xs" @click.prevent="savePresentacionProducto()">Guardar presentacion</button>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                        <button type="button" class="btn btn-outline btn-outline-primary"  @click.prevent="guardarProduco()"><i class="fa fa-check"></i> Guardar Producto</button>
                    </div>
                  </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
          </div>
    </section>
        {{-- Fin modal Entrada --}}
  </div>
</div>
@stop

@section('other-scripts')
<script>
  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);
  new Vue({
    el: '#listaProd',
    data: {
      products:{},
      editProduct:'',
      edicion:{
        descripcion:'',
        precio:'',
        index:'',
        id:''
      },
      editMode:false,
      offset: 2,
      pagination:{},
      search:''
    },
    computed: { 
      isActive(){
          return this.pagination.current_page;
      },
      pageNumber(){
          if(!this.pagination.to){
              return [];
          }
          var desde = this.pagination.current_page - this.offset;
          if(desde < 1){
              desde = 1;
          }
          var hasta = desde + (this.offset * 2);
          if(hasta >= this.pagination.last_page) {
              hasta = this.pagination.last_page;
          }
          var pagesArray = [];
          while(desde <= hasta) {
              pagesArray.push(desde);
              desde++;
          }
          return pagesArray;
      }
    },
    methods: {
      loadProducts(page){
        block();
        axios.get('/proyectosm/productos/productosList?page='+page).then(response => { 
          unblock();       
          this.products = response.data.data;
          this.pagination = response.data;
        }).catch(error => {
          console.log(error);
          unblock();
        });
      },
      cambiarPagina(page){
          this.loadProducts(page);
      },
      editProductModel(product){
        this.valor = 1;
        this.editProduct = product;
      },
      notificacion(tipo,texto){
        // Swal({position: 'top-end', type: tipo, title: texto, showConfirmButton: false, timer: 1500})
        const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
        toast({type: tipo, title: texto})                      
      },
      editarPre(index, presentacion){
        let me = this
        me.editMode = true;
        me.edicion.descripcion = presentacion.pivot.descripcion
        me.edicion.precio = presentacion.pivot.precio_estimado
        me.edicion.index =index
        me.edicion.id = presentacion.pivot.id
      },
      savePresentacionProducto(){
        block();
        let me = this;
        var url = '/proyectosm/productos/updatePresentacion'
        axios.post(url,me.edicion).then(function (response){
          unblock();
          me.editProduct.presentaciones[me.edicion.index].pivot.descripcion = me.edicion.descripcion
          me.editProduct.presentaciones[me.edicion.index].pivot.precio_estimado = me.edicion.precio
          me.notificacion('success','Se actualizo producto de la lista')
          me.edicion.descripcion = ''
          me.edicion.precio = ''
          me.edicion.index = ''
          me.edicion.id = ''
          me.editMode = false;
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      },
      buscarProd(){
        let me = this;
        if(me.search)
        {
          var url = `/proyectosm/productos/search/`+me.search;
          axios.get(url).then(function (response) {
            me.products = response.data.data;
            me.pagination = response.data;
          }).catch(function (error){
              console.log("Error "+error)
          })
        }
        else
          me.notificacion('error','El campo de busqueda esta vacio')
      },
      guardarProduco(){
        block();
        let me = this;
        var url = '/proyectosm/productos/updateProducto'
        axios.post(url,me.editProduct).then(function (response){
          unblock();
          me.editMode = false;
          $('#modalProducto').modal('hide');
          me.loadProducts(1);
          me.notificacion('success','Se actualizo el producto')
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      }
    },
    mounted() {
      this.loadProducts();
    },
    watch:{}
  });
</script>
@stop


