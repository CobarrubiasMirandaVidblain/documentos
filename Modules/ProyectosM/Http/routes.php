<?php

Route::group(['middleware' => 'web', 'prefix' => 'proyectosm', 'namespace' => 'Modules\ProyectosM\Http\Controllers'], function()
{
    Route::get('/', 'ProyectosMController@index')->name('inicio');

    Route::group(['middleware' => 'web', 'prefix' => 'proyecto'], function(){
        Route::get('/datos', 'ProyectosMController@show'); 
    });
    
    Route::group(['middleware' => 'web', 'prefix' => 'solicitud'], function(){
        Route::get('/', 'SolicitudController@create')->name('nuevo');
        Route::get('/datos', 'SolicitudController@userDatos');
        Route::get('/productos/{nombre}', 'SolicitudController@findProd'); // quitar de aqui
        Route::get('/listaProd/{id}', 'SolicitudController@listaProd'); // quitar de aqui
        Route::get('/lista', 'SolicitudController@index')->name('lista');
        Route::get('/solicitudesList', 'SolicitudController@show')->name('list');
        Route::get('/{id}', 'SolicitudController@edit');
        Route::get('folio/{folio}', 'SolicitudController@search');
        Route::get('busqueda/{folio}', 'SolicitudController@busqueda');
        Route::post('/save', 'SolicitudController@store');
        Route::post('/update', 'SolicitudController@update');
        Route::post('/changeStatus', 'SolicitudController@changeStatus');
        Route::delete('/eliminar/{id}', 'SolicitudController@destroy');
        Route::get('/list/eliminado', 'SolicitudController@trashed');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'entradas'], function(){
        Route::get('/', 'EntradaController@index')->name('in');
        Route::get('/showList', 'EntradaController@show');
        Route::get('/{id}', 'EntradaController@edit');
        Route::post('/save', 'EntradaController@store');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'salidas'], function(){
        Route::get('/', 'SalidaController@index')->name('exit');
        Route::get('/showList', 'SalidaController@show');
        Route::get('folio/{folio}', 'SalidaController@searchFolio');
        Route::post('/save', 'SalidaController@store');
        Route::get('/{id}', 'SalidaController@edit');
        Route::get('/datos', 'SalidaController@datoSalida'); 
    });

    Route::group(['middleware' => 'web', 'prefix' => 'productos'], function(){
        Route::get('/', 'ProductoController@index')->name('productos');
        Route::get('/lista', 'ProductoController@show')->name('listaProductos');
        Route::get('/presentaciones/{nombre}', 'ProductoController@buscarPresentacion');
        Route::get('/categoria/{nombre}', 'ProductoController@buscarCategoria');
        Route::get('/productosList', 'ProductoController@list');
        Route::get('/search/{nombre}', 'ProductoController@buscarProductos');
        Route::post('/save', 'ProductoController@store');
        Route::post('/updatePresentacion', 'ProductoController@updatePres');
        Route::post('/updateProducto', 'ProductoController@update');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'categoria'], function(){
        Route::post('/update', 'CategoriasController@update');
    });
});
