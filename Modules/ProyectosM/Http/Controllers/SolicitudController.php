<?php

namespace Modules\ProyectosM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

use App\Models\Usuario;
use App\Models\Area;
use App\Models\Empleado;
use App\Models\AreasResponsable;
use App\Models\Persona;
use App\Models\MopiSolicitudes;
use App\Models\MopiSolicitudesProductos;
use App\Models\MopiHistorialSolicitudes;
use App\Models\MopiPresentacionProducto;

use Illuminate\Support\Facades\Mail;
use Modules\ProyectosM\Emails\Solicitud;
// pruebas
use App\Models\MopiCatCategorias;
use App\Models\MopiCatProgamas;
use App\Models\MopiCatProductos;

class SolicitudController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('roles:ADMINISTRADOR,DIRECTOR,PLANEACION,USUARIO,CONSULTOR');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('proyectosm::solicitud.lista');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('proyectosm::solicitud.nuevo');
        // return view('proyectosm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            // validacion para limitar el recurso 
            // si comprometido es 20+20 que es la sumatoria por categoria es menor a los disponible continua
            foreach ($request->listcategoria as $categoria) {
                $categoriaFind = MopiCatCategorias::find($categoria['idCategoria']);
                if(((int)$categoriaFind->comprometido + (int)$categoria['total']) > (int)$categoriaFind->disponible)
                    return new JsonResponse(['success' => false, 'code' => 409, 'message' => 'Error: Sin presupuesto, Cumunicate a la DIRECCION DE PLANEACION Y MONITOREO Ext. 1811 ']);
                
                // $categoriaFind->comprometido = ((int)$categoriaFind->comprometido + (int)$categoria['total']);
                // $categoriaFind->save();
            }

            $area = Empleado::where('persona_id',auth()->user()->persona_id)->select('area_id')->firstOrFail();
            $datosArea = Area::find($request->area['id']);

            $solicitud = new MopiSolicitudes();
            $solicitud->fecha_entrega = date("Y-m-d");
            $solicitud->justificacion = $request->justificacion;
            $solicitud->usuario_id = auth()->user()->id;
            $solicitud->estatus_id = 2;
            $solicitud->area_id = $area->area_id;
            $solicitud->total = $request->sumatoria;
            $solicitud->campos = json_encode($request->listcategoria);
            $solicitud->save();  
            $solicitud->folio = "DIFO-MP-$datosArea->siglas-$solicitud->id-".date("Y");  
            $solicitud->save();  
            $array = array("DIRECCIÓN GENERAL","DIRECCION DE PLANEACION Y MONITOREO");
            $correos=array();

            foreach ($array as &$valor) {
                $area = Area::where('nombre',$valor)->firstOrFail();
                $empleado = Empleado::find($area->areasresponsables->empleado_id);
                array_push($correos,trim($empleado->persona->email));
            }
            Mail::to($correos)->queue(new Solicitud($solicitud, $datosArea));
            
            foreach ($request->listProductos as $value) {
	    		$nuevoProducto = new MopiSolicitudesProductos();
	    		$nuevoProducto->cantidad = $value['cantidad'];
	    		$nuevoProducto->precio_unitario = $value['precio'];
	    		$nuevoProducto->precio_total = ((float)$value['precio'] * (float)$value['cantidad']);
	    		$nuevoProducto->descripcion = $value['observacion'];
	    		$nuevoProducto->estatus_id = 1;
	    		$nuevoProducto->presentacion_producto_id =$value['presentacion_producto_id'];
                $nuevoProducto->solicitud_id = $solicitud->id;
                $nuevoProducto->save();  
            };

            $historial = new MopiHistorialSolicitudes();
            $historial->solicitud_id = $solicitud->id;
            $historial->estatus_id = 1;
            $historial->usuario_id = auth()->user()->id;
            $historial->save();  
            
            DB::commit();
            
            auth()->user()->bitacora(request(), [
                'tabla' => 'mopiSolicitud',
                'registro' => $solicitud->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            // return response()->json(['success' => true, 'folio'=>$solicitud->folio], 200);
            return new JsonResponse(['success' => true, 'folio'=>$solicitud->folio]);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            DB::select('call reset_autoincrement(?)',['mopi_solicitudes']);
            \Log::debug($e->getMessage());
            return new JsonResponse(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        if(auth()->user()->hasRolesStrModulo(['DIRECTOR'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('estatus_id',2)->with('estatus','area')->orderBy('id','DESC')->paginate(20));
        else if(auth()->user()->hasRolesStrModulo(['PLANEACION','CONSULTOR'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('estatus_id','!=',1)->with('estatus','area')->orderBy('id','DESC')->paginate(20));
        else if(auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('usuario_id',auth()->user()->id)->with('estatus','area')->orderBy('id','DESC')->paginate(20));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $productos = DB::table('mopi_solicitudes_productos as msp')
            ->select(['msp.descripcion','msp.presentacion_producto_id','msp.estatus_id','mpp.producto_id','mpp.presentacion_id','mcp.nombre','mcpre.presentacion',DB::raw("SUM(cantidad) as cantidad"),'msp.precio_unitario','msp.id'])
            ->where('solicitud_id', $id)
            // ->whereNull('msp.deleted_at')
            ->join('mopi_presentacion_productos as mpp', 'mpp.id', '=', 'msp.presentacion_producto_id')
            ->join('mopi_cat_productos as mcp', 'mcp.id', '=', 'mpp.producto_id')
            ->join('mopi_cat_presentaciones as mcpre', 'mcpre.id', '=', 'mpp.presentacion_id')
						->groupBy('presentacion_producto_id','msp.descripcion','msp.estatus_id','mpp.producto_id','mpp.presentacion_id','mcp.nombre','mcpre.presentacion','msp.precio_unitario','msp.id')
						->where('msp.deleted_at',null)
            // ->ordeBy('msp.id')
            ->get();
        $solicitud = MopiSolicitudes::withTrashed()->where('id',$id)->with('estatus','area','usuario')->firstOrFail();
        return new JsonResponse(['productos'=>$productos,'solicitud'=>$solicitud, 'area'=>DB::select('call getDireccion(?)',[$solicitud->area_id]),'userPerson'=>Persona::find($solicitud->usuario->persona_id)]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $solicitud = MopiSolicitudes::find($request->id);
            $solicitud->justificacion = $request->justificacion;
            $solicitud->usuario_id = auth()->user()->id;
            $solicitud->estatus_id = 2;
            $solicitud->total = $request->total;
            $solicitud->save();  

            DB::table('mopi_solicitudes_productos')->where('solicitud_id',$solicitud->id)->delete();

            foreach ($request->productos as $value) {
	    		$nuevoProducto = new MopiSolicitudesProductos();
	    		$nuevoProducto->cantidad = $value['cantidad'];
	    		$nuevoProducto->precio_unitario = $value['precio_unitario'];
	    		$nuevoProducto->precio_total = ((float)$value['precio_unitario'] * (float)$value['cantidad']);
	    		$nuevoProducto->descripcion = $value['descripcion'];
	    		$nuevoProducto->estatus_id = 1;
	    		$nuevoProducto->presentacion_producto_id =$value['presentacion_producto_id'];
                $nuevoProducto->solicitud_id = $solicitud->id;
                $nuevoProducto->save();  
            };

            $historial = new MopiHistorialSolicitudes();
            $historial->solicitud_id = $solicitud->id;
            $historial->estatus_id = 2;
            $historial->usuario_id = auth()->user()->id;
            $historial->save();  
            
            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'mopiSolicitud',
                'registro' => $solicitud->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            return new JsonResponse(['success' => true, 'folio'=>$solicitud->folio]);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return new JsonResponse(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        
        $productos = MopiSolicitudesProductos::where('solicitud_id',$id)->delete();

        $solicitud = MopiSolicitudes::find($id);
        $solicitud->estatus_id=11;
        $solicitud->save();
        $solicitud->delete();

        $historial = new MopiHistorialSolicitudes();
        $historial->solicitud_id = $id;
        $historial->estatus_id =  11; // Eliminado
        $historial->usuario_id = auth()->user()->id;
        $historial->save();  

        auth()->user()->bitacora(request(), [
            'tabla' => 'mopiSolicitud',
            'registro' => $id . '',
            'campos' => json_encode($id) . '',
            'metodo' => request()->method(),
            'usuario_id' => auth()->user()->id
        ]);
        
        return new JsonResponse(['success' => true]);
       
    }

    public function userDatos ()
    {
        $empleado = Empleado::where('persona_id',auth()->user()->persona_id)->with('area','nivel')->firstOrFail();
        $autoriza = AreasResponsable::where('area_id',$empleado->area->id)->with('empleado','area')->firstOrFail(); 
        
        return new JsonResponse([
            'solicitante' => Usuario::where('id',auth()->user()->id)->with('persona')->firstOrFail(),
            'empleado' => $empleado,
            'autoriza' => Persona::where('id',$autoriza->empleado->persona_id)->select('nombre')->firstOrFail(),
            'areaResp' => $autoriza,
            'direccion' => DB::select('call getDireccion(?)',[$autoriza->area->id]),
            'solicitudes' => auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm')?MopiSolicitudes::where('usuario_id',auth()->user()->id)->count():MopiSolicitudes::count(),
            'sincomprobar' => auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm')?MopiSolicitudes::where('usuario_id',auth()->user()->id)->whereIn('estatus_id', [5,6,7,8,9])->sum('total'):MopiSolicitudes::whereIn('estatus_id', [5,6,7,8,9])->sum('total'),
            'comprobado' => auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm')?MopiSolicitudes::where('usuario_id',auth()->user()->id)->where('estatus_id',10)->sum('total'):MopiSolicitudes::where('estatus_id',10)->sum('total'),
            // 'fecha' => date("Y-m-d"),
        ]);
    }

    public function findProd ($nombre)
    {
        $productos = MopiPresentacionProducto::with('categoria')->whereHas('producto', function($query) use($nombre){
            $query->where('nombre', 'like', "%$nombre%");
        })->take(10)->get();
        return new JsonResponse($productos);
    }

    public function listaProd ($id)
    {
        $productos = DB::table('mopi_solicitudes_productos as msp')
            ->select(['msp.descripcion','msp.presentacion_producto_id','msp.estatus_id','mpp.producto_id','mpp.presentacion_id','mcp.nombre','mcpre.presentacion',DB::raw("SUM(cantidad) as cantidad"),'msp.precio_unitario','msp.id'])
            ->where('solicitud_id', $id)
            ->whereNull('msp.deleted_at')
            ->join('mopi_presentacion_productos as mpp', 'mpp.id', '=', 'msp.presentacion_producto_id')
            ->join('mopi_cat_productos as mcp', 'mcp.id', '=', 'mpp.producto_id')
            ->join('mopi_cat_presentaciones as mcpre', 'mcpre.id', '=', 'mpp.presentacion_id')
            ->orderBy('id', 'ASC')
            ->groupBy('presentacion_producto_id','msp.descripcion','msp.estatus_id','mpp.producto_id','mpp.presentacion_id','mcp.nombre','mcpre.presentacion','msp.precio_unitario','msp.id')
            // ->orderBy('msp.id','DESC')
            
            ->get();

        return new JsonResponse( $productos);
    }
    
    public function search ($folio)
    {
        if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->where('estatus_id',5)->with('estatus','area','usuario')->take(10)->get());
        else
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->where('estatus_id',5)->where('usuario_id',auth()->user()->id)->with('estatus','area','usuario')->take(10)->get());
    }

    public function busqueda ($folio)
    {
        if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->with('estatus','area','usuario')->take(10)->get());
        else
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->where('estatus_id','!=',11)->where('usuario_id',auth()->user()->id)->with('estatus','area','usuario')->take(10)->get());
    }

    public function changeStatus(Request $request)
    {
        try {
            DB::beginTransaction();
            $solicitud = MopiSolicitudes::find($request->id);
            if($request->estatus == 5)
            {
                $totalCategoria = json_decode($solicitud->campos, true);
                foreach ($totalCategoria as $categoria) {
                     $categoriaFind = MopiCatCategorias::find($categoria['idCategoria']);
                    if(((int)$categoriaFind->comprometido + (int)$categoria['total']) > (int)$categoriaFind->disponible)
                        return new JsonResponse(['success' => false, 'code' => 409, 'message' => "Error: Sin presupuesto, en el sub-Programa: $categoriaFind->nombre"]);
                    
                    $categoriaFind->comprometido = ((int)$categoriaFind->comprometido + (int)$categoria['total']);
                    $categoriaFind->save();
                }   
            }
            
            $solicitud = MopiSolicitudes::find($request->id);
            $solicitud->estatus_id = $request->estatus;
            $solicitud->motivo = $request->estatus == 3? $request->motivo:null;
            $solicitud->save();  
            
            $historial = new MopiHistorialSolicitudes();
            $historial->solicitud_id = $solicitud->id;
            $historial->estatus_id =  $request->estatus;
            $historial->usuario_id = auth()->user()->id;
            $historial->save();  
            
            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'mopiSolicitud',
                'registro' => $solicitud->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return new JsonResponse(['success' => true, 'folio'=>$solicitud->folio]);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return new JsonResponse(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()]);
        }
    }

    public function trashed ()
    {
       return new JsonResponse(MopiSolicitudes::onlyTrashed()->with('estatus','area')->orderBy('id','DESC')->paginate(30)); 
    }
}
