<?php

namespace Modules\ProyectosM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use App\Models\MopiSalidasSolicitud;
use App\Models\MopiSalidasProductos;
use App\Models\MopiSolicitudes;
use App\Models\MopiSolicitudesProductos;
use App\Models\MopiHistorialSolicitudes;
use App\Models\Persona;

class SalidaController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('roles:ADMINISTRADOR,DIRECTOR,PLANEACION,USUARIO');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // return view('proyectosm::index');
        return view('proyectosm::salida.salida');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('proyectosm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            
            $salida = new MopiSalidasSolicitud();
            $salida->solicitud_id = $request->id;
            $salida->estatus_id = 2;
            $salida->subtotal = 1;
            $salida->total = 1;
            $salida->nombre_recibe = $request->nombreBen;
            $salida->primer_apellido_recibe = $request->primerAPBen;
            $salida->segundo_apellido_recibe = $request->segundoAPBen; 
            $salida->nombre_transporta = $request->nombreEnt;
            $salida->primer_apellido_transporta = $request->primerAPEnt;
            $salida->segundo_apellido_transporta = $request->segundoAPEnt; 
            $salida->usuario_id = auth()->user()->id;            
            $salida->save();
            
            $salida->folio = "DOB-RE-$salida->id-".date("Y");  
            $salida->save();  

            //$listaProducto = MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',7)->get(); // lista de productos de la solicitud recibidos
            $productosEntregados = MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',9)->get(); // lista de productos entregados
                        
            foreach ($request->productos as $value) {
                
                // $productoFind = $listaProducto->firstWhere('presentacion_producto_id', $value['presentacion_producto_id']);
                
                if($value['estatus_id'] == 7)
                {
                    $productoFind = MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',7)->where('presentacion_producto_id', $value['presentacion_producto_id'])->where('descripcion',$value['descripcion'])->firstOrFail();
                    $productoEntregado =  $productosEntregados->firstWhere('presentacion_producto_id', $value['presentacion_producto_id']);
                    
                    if($productoFind->cantidad == $value['cantidad'])
                    {
                        if($productoEntregado)
                        {
                            $updateProducto = MopiSolicitudesProductos::find($productoEntregado->id);
                            $updateProducto->cantidad = (int)$updateProducto->cantidad + (int)$value['cantidad'];
                            $updateProducto->precio_total =((float)$productoEntregado->precio_unitario * ((int)$value['cantidad'])+(int)$productoEntregado->cantidad);
                            $updateProducto->save();
                            DB::table('mopi_solicitudes_productos')->where('id',$productoFind->id)->delete();
                        }
                        else
                            DB::table('mopi_solicitudes_productos')->where('id',$productoFind->id)->update(['estatus_id'=>9]);   
                    }
                    else
                    {
                        if($productoEntregado)
                        {
                            $updateProducto = MopiSolicitudesProductos::find($productoEntregado->id);
                            $updateProducto->cantidad = (int)$updateProducto->cantidad + (int)$value['cantidad'];
                            $updateProducto->precio_total =((float)$productoEntregado->precio_unitario * ((int)$value['cantidad'])+(int)$productoEntregado->cantidad);
                            $updateProducto->save();                            
                        }
                        else
                        {
                            $newProdSol = new MopiSolicitudesProductos();
                            $newProdSol->cantidad = (int)$value['cantidad'];
                            $newProdSol->precio_unitario = (float)$value['precio_unitario'];
                            $newProdSol->precio_total = ((float)$value['precio_unitario'] * (int)$value['cantidad']);
                            $newProdSol->descripcion = $value['descripcion'];
                            $newProdSol->estatus_id = 9;
                            $newProdSol->presentacion_producto_id = $value['presentacion_producto_id'];
                            $newProdSol->solicitud_id = $request->id;
                            $newProdSol->save(); 
                        }
                        DB::table('mopi_solicitudes_productos')->where('id',$productoFind->id)->update(['cantidad'=>(((int)$productoFind->cantidad)-((int)$value['cantidad'])),'precio_total'=>((((int)$productoFind->cantidad)-((int)$value['cantidad']))* (float)$productoFind->precio_unitario)]);
                        
                    }                    
                    $nuevoProducto = new MopiSalidasProductos();
                    
                    $nuevoProducto->precio_unitario = 1;
                    $nuevoProducto->subtotal = 2;
                    $nuevoProducto->salida_id = $salida->id;                   
                    $nuevoProducto->presentacion_producto_id = $value['presentacion_producto_id'];
                    $nuevoProducto->cantidad = $value['cantidad'];
                    $nuevoProducto->usuario_id = auth()->user()->id;
                    $nuevoProducto->save();

                    if(MopiSolicitudesProductos::where('solicitud_id',$request->id)->count() == MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',9)->count())
                    {
                        DB::table('mopi_solicitudes')->where('id',$request->id)->update(['estatus_id'=>9]);
                        $historial = new MopiHistorialSolicitudes();
                        $historial->solicitud_id = $request->id;
                        $historial->estatus_id = 9;
                        $historial->usuario_id = auth()->user()->id;
                        $historial->save();  
                    }     
                }           	
            };
            
            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'mopi_salida_solicitudes',
                'registro' => $salida->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            return new JsonResponse(['success' => true, 'folio'=>$salida->folio]);            
        }
        catch(\Exception $e) {
            DB::rollBack();
            DB::select('call reset_autoincrement(?)',['mopi_salidas_solicitudes']);
            \Log::debug($e->getMessage());
            return new JsonResponse(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
            return new JsonResponse(MopiSalidasSolicitud::with('solicitud')->paginate(10));
        else
            return new JsonResponse(MopiSalidasSolicitud::with('solicitud')->where('usuario_id',auth()->user()->id)->paginate(10));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {

        $salida = MopiSalidasSolicitud::with('solicitud','productos','usuario')->where('id',$id)->firstOrFail();
        $prodEntradas = MopiSolicitudesProductos::where('solicitud_id',$salida->solicitud_id)->where('estatus_id',9)->get();  
        
        foreach ($salida->productos as $value) {
            $productoFind = $prodEntradas->firstWhere('presentacion_producto_id', $value['presentacion_producto_id']);
            $value['descProducto'] = $productoFind->descripcion;
            $productoFind->presentacion_producto_id = 0;
            // $descripcion = MopiSolicitudesProductos::select('descripcion')->where('solicitud_id',$salida->solicitud_id)->where('presentacion_producto_id',$value['presentacion_producto_id'])->firstOrFail();
            // $value['descProducto'] = $descripcion->descripcion;
        }
        $user = Persona::find($salida->usuario->persona_id);
        return new JsonResponse(['salida'=>$salida, 'user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function searchFolio ($folio)
    {        
        if(auth()->user()->hasRolesStrModulo(['DIRECTOR'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->where('estatus_id',5)->where('estatus_id',7)->with('estatus','area','usuario')->take(10)->get());
        else if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->whereIn('estatus_id', [5,6,7])->with('estatus','area','usuario')->take(10)->get());
        else
            return new JsonResponse(MopiSolicitudes::where('folio', 'like', "%$folio%")->whereIn('estatus_id', [5,6,7])->where('usuario_id',auth()->user()->id)->with('estatus','area','usuario')->take(10)->get());
    }

}
