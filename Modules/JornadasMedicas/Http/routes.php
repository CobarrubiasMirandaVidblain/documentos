<?php

Route::group(['middleware' => 'web', 'prefix' => 'jornadasmedicas', 'namespace' => 'Modules\JornadasMedicas\Http\Controllers'], function(){  
  Route::get('/', 'PeticionController@index');
  Route::resource('peticiones', 'PeticionController',['as'=>'jornadasmedicas','only'=>['index','show','update','destroy']]);   //Peticiones
  Route::resource('programas', 'ProgramaController',['as'=>'jornadasmedicas']);                                                //Programas
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'jornadasmedicas' ]);                                //Beneficiarios
});
