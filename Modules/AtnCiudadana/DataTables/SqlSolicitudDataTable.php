<?php

namespace Modules\AtnCiudadana\DataTables;

use Modules\AtnCiudadana\Entities\SqlSolicitudes;
use Yajra\DataTables\Services\DataTable;

class SqlSolicitudDataTable extends DataTable {
  
  /**
   * @param mixed $query Results from query() method.
   * @return \Yajra\DataTables\DataTableAbstract
   */
  public function dataTable($query) {
    $dataTable = datatables($query)
			->editColumn('estado', function($model) {
				if($model->estado === 'E') {
					return 'EN PROCESO';
				}
				if($model->estado === 'N') {
					return 'NUEVO';
				}
				if($model->estado === 'F') {
					return 'FINALIZADO';
				}
      });

    return $dataTable;  
  }

  /**
   *
   * @param \App\User $model
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function query() {
    $model = SqlSolicitudes::select(
      [
        'p.folio AS folio',
        'p.CP_AÑO AS anio',
        'p.Region AS region',
        'p.Municipio AS municipio',
        'p.Localidad AS localidad',
        'p.cp_solicitante AS solicitante',
        'p.cp_cargo_sol AS cargo_solicitante',
        'p.cp_asunto AS asunto',
        'p.producto AS producto',
        'p.cp_cantidad AS cantidad',
        'p.recibido_en AS fecha_recibido',
        'b.Cp_Curp AS curp',
        'b.Cp_Rfc AS rfc',
        'b.Cp_Sexo AS genero',
        'p.Direccion AS direccion',
        'p.DepExterna AS dependencia_externa',
        'p.cp_st_seguimiento AS estado',
        'p.Clave_Municipio AS municipio_id'
      ]
    )
    ->LeftJoin('BdSICOPE.dbo.v_Beneficiarios AS b', 'b.cp_id_peticion', '=', 'p.cp_id')
    ->WhereIn('p.CP_AÑO', ['2019', '2018', '2017']);

    return $model;
  }

  /**
   *
   * @return \Yajra\DataTables\Html\Builder
   */
  public function html() {
    return $this
      ->builder()
      ->columns($this->getColumns())
      ->minifiedAjax()
      ->ajax(['data' => 'function(d) { d.table = ""; }'])
      ->parameters($this->getBuilderParameters());
  }

  /**
   *
   * @return array
   */
  protected function getColumns() {
    $columns = [
      'folio' => ['data' => 'folio', 'name' => 'p.folio'],
      'anio' => ['data' => 'anio', 'name' => 'p.CP_AÑO'],
      'region' => ['data' => 'region', 'name' => 'p.Region'],
      'municipio' => ['data' => 'municipio', 'name' => 'p.Municipio'],
      'localidad' => ['data' => 'localidad', 'name' => 'p.Localidad'],
      'solicitante' => ['data' => 'solicitante', 'name' => 'p.cp_solicitante'],
      'cargo_solicitante' => ['data' => 'cargo_solicitante', 'name' => 'p.cp_cargo_sol'],
      'asunto' => ['data' => 'asunto', 'name' => 'p.cp_asunto'],
      'producto' => ['data' => 'producto', 'name' => 'p.producto'],
      'cantidad' => ['data' => 'cantidad', 'name' => 'p.cp_cantidad'],
      'fecha_recibido' => ['data' => 'fecha_recibido', 'name' => 'p.recibido_en'],
      'curp' => ['data' => 'curp', 'name' => 'b.Cp_Curp'],
      'rfc' => ['data' => 'rfc', 'name' => 'b.Cp_Rfc'],
      'genero' => ['data' => 'genero', 'name' => 'b.Cp_Sexo'],
      'direccion' => ['data' => 'direccion', 'name' => 'p.Direccion'],
      'dependencia_externa' => ['data' => 'dependencia_externa', 'name' => 'p.DepExterna'],
      'estado' => ['data' => 'estado', 'name' => 'p.cp_st_seguimiento'],
      'municipio_id' => ['visible' => false, 'data' => 'municipio_id', 'name' => 'p.Clave_Municipio']
    ];
    return $columns;
  }

  /**
   *
   * @return array
   */
  protected function getBuilderParameters() {
    $builderParameters = [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'sEmptyTable' => 'No hay autoridades municipales registrados(as).'
      ],
      'initComplete' => ' function () {
        var api = this.api()
        api.columns().every(function () {
            var column = this;
            if(api.table().init().columns[column[0]].searchable){
                var input = document.createElement("input")
                $(input).addClass("column-filter")
                $(input).addClass("text-center")
                $(input).attr("placeholder","Filtrar "+column.header().textContent.toLowerCase())
                $(input).appendTo($(column.footer()).empty())
                .on("change", function () {
                  column.search($(this).val(), false, false, true).draw()
                });
            }
        });
      }',
      'dom' => 'Btipr',
      'preDrawCallback' => 'function() { block(); }',
      'drawCallback' => 'function() { unblock(); }',
      'buttons' => [
        [
          'extend' => 'excel',
          'text' => '<i class="fa fa-file-excel-o"></i> Excel',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => 'button-dt tool'
        ]
      ],
      'lengthMenu' => [ [10], [10] ],
      'responsive' => true,
      'autoWidth' => true,
      'columnDefs' => [
        [
          'className' => 'text-center', 'targets' => '_all'
        ]
      ]
    ];
    return $builderParameters;
  }

  /**
   *
   * @return string
   */
  protected function filename() {
    return 'SqlPeticion_' . date('YmdHis');
  }

}