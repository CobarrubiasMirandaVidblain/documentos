@extends("atnciudadana::layouts.master")

@section('styles')
  @include('atnciudadana::layouts.links')
  <style type="text/css">
  </style>  
@stop

@section('content-title', auth()->user()->persona->empleado->area->nombre)
@section('content-subtitle', 'Solicitudes Anterior')

@section('li-breadcrumbs')
    <li class="active">Solicitudes Anterior</li>
@endsection

@section('content')
  <section class="content">
      <div class="box box-primary shadow">
        <div class="box-header with-border">
          <h3 class="box-title">Tabla de Solicitudes Anterior</h3>
        </div>
        <div class="box-body">
          {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'peticionesA', 'name' => 'peticionesA', 'style' => 'width: 100%'], true) !!}
        </div>
      </div>
  </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
{!! $dataTable->scripts() !!}

{{--  Configuracion Block y Unblock  --}}
<script type="text/javascript">
	function block() {
        baseZ = 1030;        
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: baseZ,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

        function unblock_error() {
            if($.unblockUI()) {
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
            }
        }

	    setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
</script>
@endpush