@extends('atnciudadana::layouts.master')

@section('content-subtitle', (isset($solicitud) ? 'Editar' : 'Nueva') . ' Solicitud')

@section('myCSS')
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
  <link href="{{asset('plugins/dropzone/dist/dropzone.css')}}" type="text/css" rel="stylesheet">
  <style>
    #btnNext_solicitud,#btnPrevious_peticion,#btnPrevious_beneficiarios {
      transition: all 0.5s;
      font-size: 20px;
      padding: 3px 25px;
      border-radius: 3px;
    }

    #btnNext_solicitud:hover,#btnPrevious_peticion:hover,#btnPrevious_beneficiarios:hover {
      box-shadow: 1px 1px 5px #d12654b3;
    }
    
    i {
      -webkit-transition: all .4s ease-in-out;
    }

    #btnNext_solicitud:hover i.fa-arrow-circle-right,#btnPrevious_peticion:hover i.fa-arrow-circle-left, #btnPrevious_beneficiarios:hover i.fa-arrow-circle-left {
        transform: rotateX(360deg);
    }

    .nav-tabs-custom>.nav-tabs>li.disabled>a {
      pointer-events: none;
      color: #777;
    }

    span.select2-selection__rendered {
        text-transform: uppercase;
    }

    .modal-dialog:not([role="document"]) > .modal-content > .modal-body {
        overflow-y: auto;
        max-height: calc(100vh - 210px);
    }

    #modal-body-beneficiario {
       min-height: 400px;
    }

    .modal-body {
        overflow-y: auto;
        max-height: calc(100vh - 210px);
    }
    .nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
        background: #d46985;
    }
    .noedit{
        padding: 3px 5px;
        font-size: 12px;
        line-height: 1.5;
        font-weight: 100;
        border: 1px solid;
        border-color: #57C7E6;
        border-radius: 3px;
        cursor: no-drop;
        background-color: #74D1EA !important;
        color: #177D99 !important;
    }
  </style>
@endsection

@section('li-breadcrumbs')
  <li class="active"><a href="{{ route('solicitudes.index') }}">Solicitudes</a></li>
<li class="active">{{ isset($solicitud) ? 'Editar' : 'Nueva' }} Solicitud</li>
@endsection

@section('content')
  <div class="row shadow">
    <div class="col-xs-12 no-padding-lr">
      <div class="nav-tabs-custom no-padding-b">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_solicitud" data-toggle="tab">Solicitud</a></li>
          <li class="disabled" name="tabsolicitud"><a href="#tab_peticion" data-toggle="tab">Petición</a></li>
          <li class="disabled" style="display: none;"><a href="#tab_beneficiarios" data-toggle="tab">Beneficiarios</a></li>
          <li class="disabled" name="tabsolicitud"><a href="#tab_anexos" data-toggle="tab">Anexos</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="tab_solicitud">
            <div class="box-body">
              <h3 class="nmt">Datos generales de la solicitud</h3>
              @include('atnciudadana::solicitudes.tab.solicitud')
              <h3>Datos del remitente</h3>
              <form role="form" id="form_remitente" onsubmit="return false;">    
                <div id="body_form_remitente">
                    <div class="row">
                    <div class="box-body">
                        @if(!isset($solicitud))
                        <span class="text-red"><i class="fa fa-exclamation-triangle "></i> Seleccione un tipo de remitente </span>
                        @endif
                    </div>
                    </div>
                </div>    
            </form>
            </div>
            <div class="box-footer">
              <button id="btnNext_solicitud" type="button" class="btn btn-primary pull-right" data-toggle="tooltip" data-placement="left" title="Continuar">
                <i class="fa fa-arrow-circle-right"></i>
              </button>
            </div> 
          </div>
          <div class="tab-pane disabled" id="tab_peticion">
            <h3>Petición</h3>
            @include('atnciudadana::solicitudes.tab.peticion')
          </div>
          <div class="tab-pane disabled" id="tab_beneficiarios">
            <h3>Beneficiarios</h3>
            @include('atnciudadana::solicitudes.tab.beneficiarios')
          </div>
          <div class="tab-pane disabled" id="tab_anexos">
            <h3>Anexos</h3>
            <form method="post" action="{{route('atnciudadana.solicitudes.anexos.store', 54)}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Data Table de Personas -->
  <div class="modal fade" id="modal-personas">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="modal-title-personas">Tabla personas:</h4>
        </div>
        <div class="modal-body" id="modal-body-personas">
          <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
            <input type="text" id="search" name="search" class="form-control">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
            </span>
          </div>
          {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
        </div>
        <div class="modal-footer" id="modal-footer-personas">
          <div class="pull-right">
            <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Data Table de Giras -->
  <div class="modal fade" id="modal-giras">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="Giras.cerrar_sin_seleccionar()" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">Lista de giras y eventos</h4>
        </div>
        <div class="modal-body" id="modal-body-giras">
          <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
            <input type="text" id="search_giras" class="form-control">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_giras">Buscar</button>
            </span>
          </div>
          {{-- {!! $giras->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'giras', 'name' => 'giras', 'style' => 'width: 100%']) !!} --}}
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button type="button" class="btn btn-danger" onclick="Giras.cerrar_sin_seleccionar()"><i class="fa fa-remove"></i> Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Formulario Para Agregar Personas -->
  <div class="modal fade" id="modal-persona">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" onclick="cerrar_modal('modal-persona')">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="modal-title-persona"></h4>
        </div>
        <div class="modal-body" id="modal-body-persona">
        </div>
        <div class="modal-footer" id="modal-footer-persona">
          <div class="pull-right">
            <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
          </div>
        </div>
      </div>
    </div>
  </div> 

  <div class="modal fade" id="modal-beneficiario">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Registro de beneficiario</h4>
            </div>
            <div class="modal-body" id="modal-body-beneficiario"></div>
            <div class="modal-footer" id="modal-footer-beneficiario">
                {{-- <button type="button" class="btn btn-warning pull-left" data-dismiss="modal" id="btn_editar_persona"><i class="fa fa-pencil"></i> Editar</button> --}}
                <button type="button" class="btn btn-danger" onclick="Beneficiarios.cancelar()"><i class="fa fa-ban"></i> Cancelar</button>
                <button type="button" class="btn btn-success" onclick="Beneficiarios.guardar_beneficiario()"><i class="fa fa-database"></i> Aceptar</button>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-dependencia">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-dependencia')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-dependencia">Registrar dependencia o institución</h4>
            </div>
            <div class="modal-body" id="modal-body-dependencia"></div>
            <div class="modal-footer" id="modal-footer-dependencia">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-dependencia')"><i class="fa fa-close"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="dependencia.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
            </div>      
        </div>
    </div>
</div>

@stop

@section('myScripts')
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
  <!-- jQuery Validation -->
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
  <script type="text/javascript" src="{{asset('plugins/dropzone/dist/dropzone.js')}}"></script>
  {{-- {!! $giras->html()->scripts() !!} --}}
  {!! $personas->html()->scripts() !!}
  {!! $beneficios->html()->scripts() !!}
  {!! $solicitantes->html()->scripts() !!}
  @include('personas.js.persona')
  @include('dependencias.js.dependencia')
  @include('atnciudadana::beneficiosExternos.js.beneficio_externo')
  
  <!-- Modulos -->
  <script type="text/javascript">
  
    Dropzone.options.dropzone = {
        maxFilesize: 2,
        addRemoveLinks: true,
        dictRemoveFile: 'Eliminar',
        url: "{{ route('atnciudadana.solicitudes.anexos.store', 54) }}",
        dictDefaultMessage: 'Click aquí o arrastre sus archivos para subirlos',
        headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' },
        init: function() {
            @if(isset($solicitud))
                @foreach($solicitud->anexos as $anexo)
                    var mockFile = { name: '{{ $anexo->anexo }}' };
                    mockFile.id = '{{ $anexo->id }}';
                    this.emit('addedfile', mockFile);            
                    this.createThumbnailFromUrl(mockFile, '{{ asset("$anexo->anexo") }}');            
                    this.emit('complete', mockFile);
                    var link = document.createElement('a');
                    link.setAttribute('href', '{{ asset("$anexo->anexo") }}');
                    link.innerHTML = "<br>Descargar";
                    mockFile.previewTemplate.appendChild(link);
                @endforeach
            @endif

            this.on('addedfile', function(file) {
                if(file.size > Dropzone.options.dropzone.maxFilesize * 1024 * 1024) {
                    this.removeFile(file);               
                    swal(
                        '¡Error!',
                        'Solo se permiten archivos de ' + Dropzone.options.dropzone.maxFilesize + 'MB o menos.',
                        'error'
                    );
                }
            });

            this.on('removedfile', function(file) {
                console.log(file);                
                /*block();            
                $.ajax({
                    url: '{{ route('files.destroy', 0) }}' + '?id=' + file.id,
                    type: 'DELETE',
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        unblock();
                    },
                    error: function(response) {
                        unblock();
                    }
                });*/
            });
        },
        success: function(file, response) {
            swal(
                '¡Correcto!',
                'Archivo subido correctamente.',
                'success'
            );
        }
    };

    var nuevo_beneficiario = false;

    // Modulo para manipular el DOM sin Jquery
    var Domcito = (()=>{

      var _elemento = (e) => {
        var element = document.createElement(e);

        element.setID = (id) => {
          element.setAttribute('id', id);
          return element;
        };

        element.addClass = (...arguments) => {
          for(var i =0;i<arguments.length;i++){
            element.classList.add(arguments[i]);
          }
          return element;
        };

        element.removeClass = (...arguments) => {
          for(var i =0;i<arguments.length;i++){
            element.classList.remove(arguments[i]);
          }
          return element;
        };

        element.addAtribute = (atr,v) => {
          element.setAttribute(atr, v);
          return element;
        }

        element.texto = (text) => {
          element.textContent = text;
          return element;
        }

        return element; 
      }

      return {
        crear_elemento: _elemento
      }

    })();

    // Modulo para crear Inputs usando el Dom
    var Funi = (()=>{

      var _create_input_persona = (id = 'remitente_id',name = 'Remitente') => {
        var div_Col = Domcito.crear_elemento('div').addClass('col-xs-12','col-sm-12','col-md-6','col-lg-6');
        var div_FrmGrp = Domcito.crear_elemento('div').addClass('form-group');
        var label = Domcito.crear_elemento('label').addAtribute('for',id).texto(name+':');
        var div_Input = Domcito.crear_elemento('div').addClass('input-group');
        var select = Domcito.crear_elemento('select').setID(id).addClass('form-control','input-persona').addAtribute('name',id).addAtribute('readonly','readonly');
        var div_input_group_btn = Domcito.crear_elemento('div').addClass('input-group-btn');        
        var boton = Domcito.crear_elemento('button').addClass('btn','btn-info').addAtribute('type','button').addAtribute('onclick','Personas.abrir_modal_personas(\''+id+'\')');
        boton.style.paddingLeft = "25px";
        boton.style.paddingRight  = "25px";
        boton.style.borderRadius  = "0px";
        var icon = Domcito.crear_elemento('i').addClass('fa','fa-search');
        
        boton.appendChild(icon);
        div_input_group_btn.appendChild(boton);

        var boton2 = Domcito.crear_elemento('button').addClass('btn','btn-primary').addAtribute('type','button').addAtribute('onclick','Personas.mostrar_persona()');
        boton2.style.paddingLeft = "25px";
        boton2.style.paddingRight  = "25px";
        boton2.style.borderRadius  = "0px";
        var icon2 = Domcito.crear_elemento('i').addClass('fa','fa-address-card');
        
        boton2.appendChild(icon2);
        div_input_group_btn.appendChild(boton2)


        div_Input.appendChild(select);
        div_Input.appendChild(div_input_group_btn);        
        div_FrmGrp.appendChild(label);
        div_FrmGrp.appendChild(div_Input);
        div_Col.appendChild(div_FrmGrp)

        return div_Col;
      }

      var _create_input_gira = (id = 'remitente_id',name = 'Remitente') => {
        var div_Col = Domcito.crear_elemento('div').addClass('col-xs-12','col-sm-12','col-md-12','col-lg-12');
        var div_FrmGrp = Domcito.crear_elemento('div').addClass('form-group');
        var label = Domcito.crear_elemento('label').addAtribute('for',id).texto(name+':');
        var div_Input = Domcito.crear_elemento('div').addClass('input-group');
        div_Input.style.width = "100%";
        var select = Domcito.crear_elemento('select').setID(id).addClass('form-control','input-persona').addAtribute('name',id).addAtribute('readonly','readonly');
        var div_input_group_btn = Domcito.crear_elemento('div').addClass('input-group-btn');
        var boton = Domcito.crear_elemento('button').addClass('btn','btn-info').addAtribute('type','button').addAtribute('onclick','Giras.abrir_modal_giras();');
        boton.style.paddingLeft = "25px";
        boton.style.paddingRight  = "25px";
        boton.style.borderRadius  = "0px";
        var icon = Domcito.crear_elemento('i').addClass('fa','fa-search');

        boton.appendChild(icon);
        div_input_group_btn.appendChild(boton)
        div_Input.appendChild(select);
        div_Input.appendChild(div_input_group_btn);
        div_FrmGrp.appendChild(label);
        div_FrmGrp.appendChild(div_Input);
        div_Col.appendChild(div_FrmGrp);

        return div_Col;
      }

      var _crear_input_otro = () => {
        var div_Otro = Domcito.crear_elemento('div').setID('otro').addClass('col-xs-12','col-sm-12','col-md-6','col-lg-6');
        var div_Row = Domcito.crear_elemento('div').addClass('row');        
        div_Otro.appendChild(div_Row.appendChild(_create_input_persona('otro_id','Nombre')));
        return div_Otro;
      }

      var _crear_input_gira = () => {
        var div_Gira = Domcito.crear_elemento('div').setID('gira').addClass('col-xs-12','col-sm-12','col-md-6','col-lg-6');
        var div_Row = Domcito.crear_elemento('div').addClass('row');        
        div_Gira.appendChild(div_Row.appendChild(_create_input_gira('gira_id','Gira')));
        return div_Gira;
      }

      var _crear_select2 = (id,name) => {
        var div_Col = Domcito.crear_elemento('div').addClass('col-xs-12','col-sm-12','col-md-12','col-lg-6');
        var div_FrmGrp = Domcito.crear_elemento('div').addClass('form-group');
        var label = Domcito.crear_elemento('label').addAtribute('for',id).texto(name+':');
        var select2 = Domcito.crear_elemento('select').setID(id).addClass('form-control','select2').addAtribute('name',id);

        div_FrmGrp.appendChild(label);
        div_FrmGrp.appendChild(select2);

        div_Col.appendChild(div_FrmGrp);

        return div_Col;
      }

      return {
        create_input_persona: _create_input_persona,
        crear_input_otro:_crear_input_otro,
        crear_input_gira:_crear_input_gira,
        crear_select2:_crear_select2
      }
    })();

    var Giras = ( ()=> {

        var modal_giras = $('#modal-giras');
        var modal_body_giras = $('#modal-body-giras');
      
        var btn_buscar_giras = $('#btn_buscar_giras');
        var search_giras = $('#search_giras');

        var _abrir_modal_giras = () => {
            $(".modal-gira").hide().removeClass("button-dt");
            modal_giras.modal('show');
        }

        var _cerrar_sin_seleccionar = () => {
            $('#tiporecepcion_id').val(null).trigger('change');
            modal_giras.modal('hide');
        }

        var _init_modal_giras = () => {
            $.fn.modal.Constructor.prototype.enforceFocus = () => {};
            var table = $('#giras').dataTable();
            datatable_giras = $(table).DataTable();
            search_giras.keypress(e => {
                if(e.which === 13) {
                    datatable_giras.search(search_giras.val()).draw();
                }
            });
            btn_buscar_giras.on('click', () => {
                datatable_giras.search(search_giras.val()).draw();
            });
            modal_giras.on('shown.bs.modal', evt => {
                _recargar_tabla_giras();
            });
        }

        var _recargar_tabla_giras = () => {
            if(datatable_giras !== undefined) {
                datatable_giras.ajax.reload();
            }
        };

        var _seleccionar_gira = (id, nombre) => {
            if($("#gira").length === 0){
                $('#div-dirigido').append(Funi.crear_input_gira());
            }
            $('#gira_id').html("<option value='" + id + "'selected>" + nombre + "</option>");
            Solicitud.nueva_solicitud.evento_id = id;
            modal_giras.modal('hide');
      };

        return {
            abrir_modal_giras: _abrir_modal_giras,
            seleccionar_gira:_seleccionar_gira,
            init_modal_giras:_init_modal_giras,
            cerrar_sin_seleccionar: _cerrar_sin_seleccionar
        }
    })();

    // Modulo para el control de los Modal Personas
    var Personas = (()=>{
      var modal_personas = $('#modal-personas');
      var modal_persona = $('#modal-persona');
      var modal_title_persona = $('#modal-title-persona');
      var modal_body_persona = $('#modal-body-persona');
      var modal_footer_persona = $('#modal-footer-persona');
      var btn_buscar = $('#btn_buscar');
      var search = $('#search');
      var elemento = undefined;
      var ele = undefined;
      
      let _persona = {};
      _persona.persona_id = undefined;

      var _abrir_modal_personas = (el) => {     
          if( el === "beneficiarios") {
              nuevo_beneficiario = true;

                if( Beneficiarios.programa.cantidad && Beneficiarios.programa.solicitantes && Beneficiarios.programa.cantidad === Beneficiarios.programa.solicitantes){
                    return swal(
                        'Error',
                        'Ya se ha registrado la cantidad de beneficiarios solicitada',
                        'error'
                    )
                }
          }
          
        elemento = $('#' + el);
        ele = el;
        modal_personas.modal('show');
      };
      
      var _seleccionar_persona = (id, nombre) => {
        if(elemento !== undefined) {          
          if(ele === 'remitente_id') {
            elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
            Solicitud.nueva_solicitud.solicitante_id = id;
            Solicitud.nueva_solicitud.nombre_solicitante = nombre;
          }
          else if(ele === 'otro_id') {
            elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
            Solicitud.nueva_solicitud.persona_id = id;
          }
          else if(nuevo_beneficiario) {
            Personas.persona.persona_id = id; //Persona en proceso de ser agregada como solicitante
            _agregar_persona(id);
          }
          modal_personas.modal('hide');
        }
      };

      var _init_modal_persona = () => {
        $.fn.modal.Constructor.prototype.enforceFocus = () => {};

        var table = $('#personas').dataTable();

        datatable_personas = $(table).DataTable();

        search.keypress(e => {
          if(e.which === 13) {
            datatable_personas.search(search.val()).draw();
          }
        });

        btn_buscar.on('click', () => {
          datatable_personas.search(search.val()).draw();
        });

        modal_personas.on('shown.bs.modal', evt => {
          _recargar_tabla_personas();
        });
      }

      var _editar_persona = (id) => {
          block();         
            $.get('/personas/search?tipo=create_edit&id=' + id, function(data) {
                modal_title_persona.text('Actualizar Datos');
                modal_body_persona.html(data.html);
                modal_footer_persona.html(
                    '<button type="button" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="Personas.cancelar_editar();"><i class="fa fa-reply"></i> Regresar</button>'
                );
                app.to_upper_case();
                persona.init();
                persona.editar_fotografia();
			    persona.agregar_fecha_nacimiento();
			    persona.agregar_inputmask();
                persona.agregar_select_create();
                persona.agregar_select_edit();
			    persona.agregar_validacion_atnciudadana();
                modal_persona.modal('show');
                unblock();
            });
      }

      var _cancelar_editar = () => {
          //if(nuevo_beneficiario){
              $('#modal-persona').modal('hide');
              $('#modal-beneficiario').modal('show');
          //}
      }

      var _agregar_modal_static = () => {
        $('.modal').modal({
          backdrop: 'static'
        });

        $('.modal').modal('hide');
      };

      var _recargar_tabla_personas = () => {
        if(datatable_personas !== undefined) {
          datatable_personas.ajax.reload();
        }
      };

      var _persona_create_edit_success = (response) => {
          
        if(elemento !== undefined && ele !== undefined) {
            if(ele === 'remitente_id') {
                Solicitud.nueva_solicitud.solicitante_id = response.id;
                Solicitud.nueva_solicitud.nombre_solicitante = response.nombre;
                elemento.html("<option value='" + response.id + "'selected>" + response.nombre + "</option>");
            }
            if(ele === 'otro_id') {
                Solicitud.nueva_solicitud.persona_id = response.id;
                elemento.html("<option value='" + response.id + "'selected>" + response.nombre + "</option>");
            }
            if(ele === 'beneficiarios'){
                if(nuevo_beneficiario){
                   _agregar_persona(response.id);            
                }else{                
                    Beneficiarios.mostrar_beneficiario(response.id);
                    Beneficiarios.recargar_beneficiarios();
                }
            }
        }    
        app.set_bloqueo(false);
        modal_personas.modal("hide");
        modal_persona.modal("hide");
        unblock();
      }

      var _persona_create_edit_error = (response) => {
        unblock();
        if(response.status === 422) {
          swal({
            title: 'Error al registrar',				
            text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
            type: 'error',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Regresar',
            allowEscapeKey: false,
            allowOutsideClick: false
          });
        }
      }

      var _agregar_persona = (id) => {
          block();

        $.get('/personas/search?tipo=create_edit&id=' + id, data => {})
          .done(data => {
            modal_body_persona.html(data.html);
            persona.init();
            persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
            persona.agregar_select_create();
            persona.agregar_select_edit();
			persona.agregar_validacion_atnciudadana();
            //Para agregar al beneficiario
            Beneficiarios.programa.persona_id = id;
            //Para editar datos de la persona
            Personas.persona_id = id;

            var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.show", [":id",":programa",":persona"] ) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id)
                    .replace(':programa', Beneficiarios.programa.programa_id)
                    .replace(':persona', data.id);

            $.get(url, function(data) {
                //$('#modal-body-beneficiario').html(data.beneficiario);
                $('#modal-beneficiario').html(data.beneficiario);
                $("#modal-footer-beneficiario button").show();

                if(!nuevo_beneficiario){
                    $("#modal-footer-beneficiario button:not(:eq(0))").hide();
                }
                //$('#btn_editar_persona').show();
                $('#btn_editar_persona').off('click');
                $('#btn_editar_persona').on('click',() => {
                    Personas.editar_persona(id);
                });
                Beneficiarios.recargar_beneficiarios();
                $('#modal-beneficiario').modal('show');
                unblock();         
            });  
          })
          .fail(data => {
            modal_title_persona.text('Agregar Persona:');
            modal_body_persona.html(data.responseJSON.html);
            modal_footer_persona.html(
              '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>'+
              '<button type="submit" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'                
            );
            app.to_upper_case();
            persona.init();
            persona.editar_fotografia();
            persona.agregar_fecha_nacimiento();
            persona.agregar_inputmask();
            persona.agregar_select_create();
            persona.agregar_select_edit();
            persona.agregar_validacion_atnciudadana();      
            modal_persona.modal('show');
            unblock();
          });
      }

      var _mostrar_persona = () => {
          if(Solicitud.nueva_solicitud.solicitante_id !== undefined){
            block();
            var url = '{{ route("personas.mostrar", [":id"] ) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitante_id);
            $.get(url, function(data) {
                $('#modal-beneficiario').html(data.persona);
                $('#modal-beneficiario').modal('show');
                $("#modal-footer-beneficiario button:not(:eq(0))").hide();
                unblock();
            });
          }
        }

      return {
        abrir_modal_personas:_abrir_modal_personas,
        seleccionar_persona:_seleccionar_persona,
        agregar_modal_static:_agregar_modal_static,
        persona_create_edit_success:_persona_create_edit_success,
        persona_create_edit_error:_persona_create_edit_error,
        init_modal_persona:_init_modal_persona,
        agregar_persona:_agregar_persona,
        editar_persona: _editar_persona,
        persona: _persona,
        cancelar_editar: _cancelar_editar,
        mostrar_persona: _mostrar_persona
      }
    })();

    // Crear Inputs segun el tipo de remitente
    var Remitente = (()=>{
      
      let tipoSelect = 0;

      // Remitente tipo Regional
      var _tipo1 = () => {
        var _row = Domcito.crear_elemento('div').addClass('row');
        _row.appendChild(Funi.create_input_persona());
        _row.appendChild(Funi.crear_select2('remitente_region_id','Región'));
        _row.appendChild(Funi.crear_select2('cargo','Cargo'));
        return _row;
      }

      // Remitente tipo Municipal
      var _tipo2 = () => {
        var _row = Domcito.crear_elemento('div').addClass('row');
        _row.appendChild(Funi.create_input_persona());
        _row.appendChild(Funi.crear_select2('remitente_municipio_id','Municipio'));
        _row.appendChild(Funi.crear_select2('cargo','Cargo'));
        return _row;
      }
      
      // Remitente tipo Institucional
      var _tipo3 = () => {
        var _row = Domcito.crear_elemento('div').addClass('row');
        _row.appendChild(Funi.crear_select2('dependencia_id','Dependencia'));

        return _row;
      }
      
      // Remitente tipo Personal
      var _tipo4 = () => {
        var _row = Domcito.crear_elemento('div').addClass('row');
        _row.appendChild(Funi.create_input_persona());

        return _row;
      }

      var _setSelect = (t) => {
        tipoSelect = t;
      }

      var _getSelect = () => {
        return tipoSelect;
      }

      //Inicializar Select2 (Usando JQuery)
      var _init_select2  = (t) => {
        switch(t){
          case "1":
          @if(isset($solicitud))
            if(Solicitud.nueva_solicitud.tiposremitente_id == 1 && Solicitud.nueva_solicitud.tiposremitente_id == {{ $solicitud->tiposremitente_id }}){
                var option = new Option(Solicitud.nueva_solicitud.nombre_remitente, Solicitud.nueva_solicitud.remitente, true, true);
                $('#remitente_region_id').append(option).trigger('change');
            }
         @endif
            
            $('#remitente_region_id').select2({
              language: 'es',
              allowClear : true,
              placeholder : 'SELECCIONE...',
              ajax: {
                url: "{{ route('regiones.select') }}",
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: params => {
                  return {
                    search: params.term
                  };
                },
                processResults: (data, params) => {
                  params.page = params.page || 1;
                  return {
                    results: $.map(data, item => {
                      return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                      }
                    })
                  };
                },
                cache: true
              }
            }).change(evt => {
              if($('#remitente_region_id').valid()){
                Solicitud.nueva_solicitud.remitente = $('#remitente_region_id').val();
              }
            });

            $('#cargo').select2({
              language: 'es',
              tags: true,
              //allowClear : true,
              placeholder : 'SELECCIONE...',
              ajax: {
                url: "{{ route('cargos.select') }}",
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: params => {
                  return {
                    search: params.term
                  };
                },
                processResults: (data, params) => {
                  params.page = params.page || 1;
                  return {
                    results: $.map(data, item => {
                      return {
                        id: item.nombre,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                      }
                    })
                  };
                },
                cache: true
              }
            }).change(evt => {
              if($('#cargo').valid()){
                Solicitud.nueva_solicitud.cargo= $('#cargo').val().toUpperCase();
              }
            });
          break;
          case "2":
            @if(isset($solicitud))
                if(Solicitud.nueva_solicitud.tiposremitente_id == 2 && Solicitud.nueva_solicitud.tiposremitente_id == {{ $solicitud->tiposremitente_id }}){
                    var option = new Option(Solicitud.nueva_solicitud.nombre_remitente, Solicitud.nueva_solicitud.remitente, true, true);
                    $('#remitente_municipio_id').append(option).trigger('change');
                }
            @endif
            $('#remitente_municipio_id').select2({
              language: 'es',
              minimumInputLength: 2,
              allowClear : true,
             placeholder : 'SELECCIONE...',
              ajax: {
                url: '{{ route('municipios.select') }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data:params => {
                  return {
                    search: params.term
                  };
                },
                processResults: (data, params) => {
                  params.page = params.page || 1;
                  return {
                    results: $.map(data, item => {
                      return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                      }
                    })
                  };
                },
                cache: true
              }
            }).change(evt => {
              if($('#remitente_municipio_id').valid()){
                Solicitud.nueva_solicitud.remitente = $('#remitente_municipio_id').val();
              }
            });

            $('#cargo').select2({
              language: 'es',
              tags: true,
              //allowClear : true,
              placeholder : 'SELECCIONE...',
              ajax: {
                url: "{{ route('cargos.select') }}",
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: params => {
                  return {
                    search: params.term
                  };
                },
                processResults: (data, params) => {
                  params.page = params.page || 1;
                  return {
                    results: $.map(data, item => {
                      return {
                        id: item.nombre,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                      }
                    })
                  };
                },
                cache: true
              }
            }).change(evt => {
              if($('#cargo').valid()){
                Solicitud.nueva_solicitud.cargo= $('#cargo').val().toUpperCase();
              }
            });
          break;
          case "3":
            $('#dependencia_id').select2({
              language: 'es',
              allowClear : true,
             placeholder : 'SELECCIONE...',
              ajax: {
                url: '{{ route('dependencias.select') }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: params => {
                  return {
                    search: params.term
                  };
                },
                processResults: (data, params) => {
                  params.page = params.page || 1;
                  return {
                    results: $.map(data, item => {
                      return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                      }
                    })
                  };
                },
                cache: true
              }
            }).change(evt => {
              if($('#dependencia_id').valid()){
                Solicitud.nueva_solicitud.remitente = $('#dependencia_id').val();
              }
            });
          break;
        }
      }
 
      return {
        1:_tipo1,
        2:_tipo2,
        3:_tipo3,
        4:_tipo4,
        setSelect:_setSelect,
        getSelect:_getSelect,
        init_select2: _init_select2
      }
    })();

    // Modulo de Solicitudes
    var Solicitud = (()=>{

      let _nueva_solicitud = {};
      _nueva_solicitud.solicitud_id = undefined;
      _nueva_solicitud.solicitante_id = undefined;
      _nueva_solicitud.persona_id = undefined;
      _nueva_solicitud.status = undefined;
      _nueva_solicitud.tipo = undefined;
      _nueva_solicitud.tiposrecepcion_id = undefined;
      _nueva_solicitud.tiposremitente_id = undefined;
      _nueva_solicitud.numoficio = undefined;
      _nueva_solicitud.fechaoficio = undefined;
      _nueva_solicitud.fecharecepcion = undefined;
      _nueva_solicitud.asunto = undefined;
      _nueva_solicitud.cargo = undefined;
      _nueva_solicitud.compromiso = 0;
      _nueva_solicitud.remitente = undefined; //ID de la region, municipio o dependencia

      let table = $('#beneficiosDT').dataTable();
      let datatable_beneficios;
      
      var _valid_fechas = (dI,dF) => {
        if(dF.datepicker('getDate') > dI.datepicker('getDate')){
          return false;
        }
        return true;
      }

      var _guardar_solicitud = () => {
          block();
        $.ajax({
            url: '{{ route('solicitudes.store')}}',
            type: 'POST',
            data: Solicitud.nueva_solicitud,
            success: function(response) {
                Solicitud.nueva_solicitud.solicitud_id = response.success[0].id;
                $("[name='tabsolicitud']").removeClass('disabled');
			    unblock();
                swal({
                    title: 'Solicitud registrada',
                    type: 'success',
                    timer: 3000,
                    toast : true,
                    position : 'top-end',
                    showConfirmButton: false
                });
			    $('.nav-tabs a[href="#tab_peticion"]').tab('show');
            },
            error: function(response) {                
                unblock();
                swal(
                    'Error',
                    'Revise los datos',
                    'error'
                )
            }
        });
      }

      var _guardar_edicion = () => {
        var url = '{{ route("solicitudes.update", ":id") }}';
        url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id);
        $.ajax({
            url: url,
            type: 'PUT',
            data: Solicitud.nueva_solicitud,
            success: function(response) {
			    unblock();
			    $('.nav-tabs a[href="#tab_peticion"]').tab('show');
            },
            error: function(response) {
                unblock();
            }
        });
      }

      var _init_beneficios = () => {        
        datatable_beneficios = $(table).DataTable();
      }

      var _recargar_beneficios = () => {
          datatable_beneficios.ajax.reload(null, false);
      }

      return {
        nueva_solicitud:_nueva_solicitud,
        valid_fechas:_valid_fechas,
        guardar_solicitud:_guardar_solicitud,
        guardar_edicion:_guardar_edicion,
        init_beneficios: _init_beneficios,
        recargar_beneficios: _recargar_beneficios
      }
    })();
    
    // Modulo de Beneficios
    var Beneficios = (()=>{
      let _lista_beneficios = [];

      var _load_beneficios = (t) => {
        $('#beneficios').empty();
        $('#dependencias').empty();
        $('#dependencias').select2({
          language: 'es',
          ajax: {
            delay: 500,
            url: "{{ route('dependencias.select') }}",
            dataType: 'JSON',
            type: 'GET',
            data: params => {
              return {
                search: params.term
              };
            },
            processResults: (data, params) => {
              params.page = params.page || 1;

              return {
                results: $.map(data, item => {
                  return {
                    id: item.id,
                    text: item.nombre,
                    slug: item.nombre,
                    results: item
                  }
                })
              }
            },
            cache: true
          }
        });


        return $('#beneficios').select2({
          language: 'es',
          ajax: {
            delay: 500,
            url: "{{ route('beneficio.select') }}",
            dataType: 'JSON',
            type: 'GET',
            data: params => {
              return {
                search: params.term,
                tipo: '{{ $tipo_solicitud }}'
              };
            },
            processResults: (data, params) => {
              params.page = params.page || 1;

              return {
                results: $.map(data, item => {
                  return {
                    id: item.id,
                    text: item.nombre,
                    slug: item.nombre,
                    results: item
                  }
                }).filter( function(result_el) {
                    return _lista_beneficios.filter(function(beneficio_el) {
                        return beneficio_el.id == result_el.id
                    }).length == 0
                })

              }
            },
            cache: true
          }
        });
      }

      var _clear_responsable = () => {
        $('#areaR').text('---------------------');
        $('#areaT').text('---------------------');
        $('#prog').text('---------------------');
        $('#cantidad').val('');
        $('#beneficios').val(null).trigger('change');
        if($('#dependencias').length){
            $('#dependencias').val(null).trigger('change');
        }
      }

      var _agregar_beneficio = () => {
        block();
        let obj_temp = {          
          cantidad: $('#cantidad').val() == 'X' ? 0 : $('#cantidad').val()          
        }

        if($('#dependencias').length){
            obj_temp.id = $('#dependencias').select2('data')[0].sector;
            obj_temp.dependencia_id = $('#dependencias').val();
            obj_temp.dependencia = $('#dependencias').select2('data')[0].text;
        }else{
            obj_temp.id = $('#beneficios').val(),
            obj_temp.nombre = $('#beneficios').select2('data')[0].text;
        }

        var url = '{{ route("atnciudadana.solicitudes.programas.store", ":id") }}';
        url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id);

        $.ajax({
            url: url,
            type: 'POST',
            data: obj_temp,
            success: function(response) {
                _lista_beneficios.push(obj_temp);
                Solicitud.recargar_beneficios();
                _clear_responsable();
                unblock();
            },
            error: function(response) {
                swal({
                    title: 'Error al registrar',				
                    text: '',
                    type: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Regresar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
                unblock();
            }
        });        
      }

      var _editar_beneficio = (id) => {
        var p = _lista_beneficios.map(item => { return item.id }).indexOf(""+id);
	      var beneficio = new Option(_lista_beneficios[p].nombre, _lista_beneficios[p].id, true, true);
          if($('#dependencias').length){
            var dependencia = new Option(_lista_beneficios[p].dependencia, _lista_beneficios[p].dependencia_id, true, true);
            $('#dependencias').append(dependencia).trigger('change');
          }
        $('#beneficios').append(beneficio).trigger('change');
        $('#beneficios').attr('disabled', 'disabled');

        $('#cantidad').val(_lista_beneficios[p].cantidad);
        
        $('#btnAgregar').off('click');
        $('#btnAgregar').attr('id','btnEditar');
        $('#btnEditar').text("Editar");
        $('#btnEditar').css('background-color','#ec971f');
	      $('#btnEditar').css('border-color', '#e08e0b');
        $('#btnEditar').on('click',() => {
          if($('#form_producto').valid()){
            Beneficios.guardar_edicion(id,p);
          }else{
              var msg = 'Debe seleccionar un beneficio y la cantidad';
              if ('{{ $tipo_solicitud }}' == 'E'){
                msg = 'Debe seleccionar una dependencia, ingresar el tag y la cantidad';
              }
            swal(
  				'¡Advertencia!',
  				msg,
  				'warning'
			);
          }
        });
      }

      var _guardar_edicion = (id,p) => {
        swal({
          title: '¡Advertencia!',
          text: "¿Desea actualizar el beneficio?",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí',
          cancelButtonText: 'Cancelar'
        }).then(r => {
          if(r.value){
            var url = '{{ route("atnciudadana.solicitudes.programas.update", [":id",":programa"]) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id).replace(':programa', id);
            _lista_beneficios[p].cantidad = $('#cantidad').val() == 'X' ? 0 : $('#cantidad').val();
            if($('#dependencias').length){
                _lista_beneficios[p].dependencia_id = $('#dependencias').val();
                _lista_beneficios[p].dependencia = $('#dependencias').select2('data')[0].text;                
            }
            block();

            $.ajax({
                url: url,
                type: 'PUT',
                data: _lista_beneficios[p],
                success: function(response) {
                    Solicitud.recargar_beneficios();
                    _resetBoton();
                    unblock();
                },
                error: function(response) {
                    unblock();                        
                    if(response.status === 422) {
			            swal({
				            title: 'Error al actualizar',
				            text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]].message,
				            type: 'error',
				            confirmButtonColor: '#3085d6',
				            confirmButtonText: 'Regresar',
				            allowEscapeKey: false,
				            allowOutsideClick: false
			            });
		            } else {
                        swal(
                            'Error',
                            '',
                            'error'
                        )
                    }
                }
            });           
          } else {
            _resetBoton();
          }
        });
      }

      var _resetBoton = () => {
        _clear_responsable();
        $('#beneficios').removeAttr('disabled');
        $('#tipo').removeAttr('disabled');
        $("#btnEditar").off('click');
        $('#btnEditar').attr('id','btnAgregar');
        $('#btnAgregar').text("Agregar");
        $('#btnAgregar').css('background-color','#00a65a');
        $('#btnAgregar').css('border-color', '#008d4c');
        $("#btnAgregar").off('click');
        $('#btnAgregar').on('click',() => {
          if($('#form_producto').valid()){
            _agregar_beneficio();
          }else{              
              var msg = 'Debe seleccionar un beneficio y la cantidad';
              if ('{{ $tipo_solicitud }}' == 'E'){
                msg = 'Debe seleccionar una dependencia, ingresar el tag y la cantidad';
              }
            swal(
              '¡Advertencia!',
              msg,
              'warning'
            );
          }
        });
      }

      var _borrar_beneficio = (id) => {
        swal({
          title: '¡Advertencia!',
          text: '¿Está seguro de eliminar este beneficio?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#28a745',
          cancelButtonColor: '#dc3545',
          cancelButtonText: 'No',
          confirmButtonText: 'Sí'
        }).then((result) => {
          if(result.value) {
            var url = '{{ route("atnciudadana.solicitudes.programas.destroy", [":id",":programa"]) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id).replace(':programa', id);          
            block();

            $.ajax({
                url: url,
                type: 'DELETE',
                success: function(response) {
                    var p = _lista_beneficios.map(item => { return item.id }).indexOf(""+id);
                    _lista_beneficios.splice(p, 1);
                    Solicitud.recargar_beneficios();
                    _resetBoton();
                    unblock();
                },
                error: function(response) {
                    swal({
                        title: 'Error al eliminar',				
                        text: '',
                        type: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Regresar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    unblock();
                }
            });
          }
        });
      }

      var _turnar_beneficio = (id) => {
        swal({
          title: '¡Advertencia!',
          text: "No podrá editar el beneficio ni agregar beneficiarios. ¿Desea turnar la petición?",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí',
          cancelButtonText: 'Cancelar'
        }).then(r => {
          if(r.value){
            var url = '{{ route("atnciudadana.solicitudes.programas.estatus", [":id",":programa"]) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id).replace(':programa', id);
            block();

            $.ajax({
                url: url,
                data: 'estatus_id=3',
                type: 'POST',
                success: function(response) {
                    Beneficios.resetBoton();
                    Solicitud.recargar_beneficios();
                    unblock();
                },
                error: function(response) {
                    swal({
                        title: 'Error al turnar',				
                        text: '',
                        type: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Regresar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    unblock();
                }
            });           
          }
        });
      }

      return {
        lista_beneficios: _lista_beneficios,
        load_beneficios:_load_beneficios,
        agregar_beneficio:_agregar_beneficio,
        editar_beneficio:_editar_beneficio,
        borrar_beneficio:_borrar_beneficio,
        guardar_edicion:_guardar_edicion,
        turnar_beneficio: _turnar_beneficio,
        resetBoton: _resetBoton
      }
    })();

    // Modulo de Beneficiarios
    var Beneficiarios = (()=>{

      let _programa = {};
      _programa.programa_id = undefined;
      _programa.persona_id = undefined;
      _programa.cantidad = undefined;
      _programa.estatus = undefined;
      _programa.solicitantes = undefined;
      let table = $('#solicitantesDT').dataTable();
      let datatable_beneficiarios;

      var _guardar_beneficiario = () => {
          if($('#curpBeneficiario').text()){
            block();
            var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.store", [":id",":programa"] ) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id).replace(':programa', _programa.programa_id);
            $.ajax({
			    url: url,
			    type: 'POST',
			    data: "persona_id="+Personas.persona.persona_id,
			    success: function(response) {
                    $('#modal-beneficiario').modal('hide');      
                    _recargar_beneficiarios();
                    Solicitud.recargar_beneficios();
                    unblock();
                    swal(                            
                        '¡Correcto!',
                        'Beneficiario registrado',
                        'success'
                    )                                      
			    },
                error: function(response){
                    unblock();
                    swal(
                        'Error',
                        response.responseJSON.errors[0].message,
                        'error'
                    )               
                }
		    });                
          }else {
            swal(
                'Error',
                'Debe ingresar la CURP',
                'error'
            );
          }
      }

        var _mostrar_beneficiario = (persona_id) => {
            nuevo_beneficiario = false;
            block();
            var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.show", [":id",":programa",":persona"] ) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id)
                    .replace(':programa', Beneficiarios.programa.programa_id)
                    .replace(':persona', persona_id);
            $.get(url, function(data) {
                //$('#modal-body-beneficiario').html(data.beneficiario);
                $('#modal-beneficiario').html(data.beneficiario);
                $('#modal-beneficiario').modal('show');
                //$('#btn_editar_persona').show();
                $("#modal-footer-beneficiario button:not(:eq(0))").hide();
                $('#btn_editar_persona').off('click');
                $('#btn_editar_persona').on('click',() => {
                    Personas.editar_persona(persona_id);
                });

                if(Beneficiarios.programa.estatus === 'VINCULADO' || data.estatus !== null){
                    $('#btn_editar_persona').hide();
                }    
                unblock();
            });
        }

      var _agregar_beneficiarios = (id, folio) => {
          Beneficios.resetBoton();
          _programa.programa_id = id;
          $("#programa_folio").text(folio);
          _recargar_beneficiarios();
          $("[href='#tab_beneficiarios']").parent().show();
          $('.nav-tabs a[href="#tab_beneficiarios"]').tab('show');
      }

      var _abrir_eliminar_beneficiario = (id, nombre) => {
        $('#btn_eliminar_beneficiario').off('click');
        $('#btn_eliminar_beneficiario').on('click',() => {
            _eliminar_beneficiario(id);
        });
        $("#lblSolicitante").text(nombre);
        $('#motivo').val(null).trigger('change');
        $("#modal-cancelar").modal("show");
      }

      var _eliminar_beneficiario = (id) => {
          if($('#form_cancelar').valid()) {
            block();
            $("#modal-cancelar").modal("hide");
            var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.destroy", [":id",":programa",":persona"]) }}';
            url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id)
                    .replace(':programa', Beneficiarios.programa.programa_id)
                    .replace(':persona', id);
            $.ajax({
                type: "DELETE",
                url: url,
                data: "motivo="+$("#motivo").find(":selected").text(),
                success: function(response){
                    Solicitud.recargar_beneficios();
                    Beneficiarios.recargar_beneficiarios();
                    unblock();
                    swal(                                
                        '¡Correcto!',
                        'Beneficiario cancelado',
                        'success'
                    )
                },
                error: function(response){
                    if(response.status === 409){
                        unblock();
                        swal(                                
                            '¡Ocurrió un error inesperado!',
                            response.responseJSON.message,
                            'error'
                        )
                    }                    
                }
            });
        }          
      }

      var _init_beneficiarios = () => {
          datatable_beneficiarios = $(table).DataTable();
          datatable_beneficiarios.on( 'xhr', function () {
            var json = datatable_beneficiarios.ajax.json();
            Beneficiarios.programa.cantidad = (json.programa_cantidad === 0) ? 'X' : json.programa_cantidad;
            Beneficiarios.programa.solicitantes = json.programa_solicitantes;
            Beneficiarios.programa.estatus = json.programa_estatus;
            $("#programa_nombre").text(json.programa_nombre);
            $("#programa_cantidad").text(Beneficiarios.programa.cantidad);
            console.log(json.programa_estatus);
            $("#programa_estatus").text(json.programa_estatus);            

            if(Beneficiarios.programa.estatus === 'VINCULADO'){
                $(".nuevo-solicitante").hide().removeClass("button-dt");
            }else{
                $(".nuevo-solicitante").show().addClass("button-dt");
            }

        } );
        $('#searchBeneficiario').keypress(function(e) {
			if(e.which === 13) {
				datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
			}
		});

		$('#btn_buscarBeneficiario').on('click', function() {
			datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
		});
      }

      var _recargar_beneficiarios = () => {
          datatable_beneficiarios.ajax.reload(null, false);
      }

      var _cancelar = () => {
          if(nuevo_beneficiario){
              swal({
                title: '¿Desea cancelar el registro del beneficiario?',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, cancelar',
                cancelButtonText: 'No, regresar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $('#modal-beneficiario').modal('hide');
                }
            });
          }else {
              $('#modal-beneficiario').modal('hide');
          }
        
      }

      var _reactivar_beneficiario = (id, nombre) => {
        swal({
            title: '¿Está seguro de reactivar al beneficiario?',
            text: nombre,
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'No',
            confirmButtonText: 'Sí, reactivar'
        }).then((result) => {
            if(result.value) {
                block();
                var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.update", [":id",":programa",":persona"] ) }}';
                url = url.replace(':id', Solicitud.nueva_solicitud.solicitud_id)
                    .replace(':programa', Beneficiarios.programa.programa_id)
                    .replace(':persona', id);
                $.ajax({
				    url: url,
				    type: "PUT",
				    success: function(response) {
                        app.set_bloqueo(false);
                        Solicitud.recargar_beneficios();
                        _recargar_beneficiarios();
                        swal({
                            title: '¡Correcto!',
                            text: 'Solicitante reactivado',
                            type: 'success',
                            timer: 2500,
                            showConfirmButton: false
                        });                        
                    },
                    error: function(response) {
                        unblock();                        
                        if(response.status === 422) {
			                swal({
				                title: 'Error al reactivar',
				                text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]].message,
				                type: 'error',
				                confirmButtonColor: '#3085d6',
				                confirmButtonText: 'Regresar',
				                allowEscapeKey: false,
				                allowOutsideClick: false
			                });
		                } else {
                            swal(
                                'Error',
                                '',
                                'error'
                            )
                        }                        
                    }
			    });                
            }
        });
    }

    var _agregar_tabla_apoyos = () => {
		datatable_apoyos = $("#apoyos-otorgados").DataTable({
			language: {
				url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
			},
			dom: 'itp',
			lengthMenu: [ [5], [5] ],
			responsive: true,
			autoWidth: true,
			processing: true,
			columnDefs: [
				{ className: 'text-center', 'targets': '_all' }
			]
		});            
	}

      return {        
        agregar_beneficiarios: _agregar_beneficiarios,
        recargar_beneficiarios: _recargar_beneficiarios,
        cancelar: _cancelar,
        programa: _programa,
        init_beneficiarios: _init_beneficiarios,
        guardar_beneficiario: _guardar_beneficiario,
        mostrar_beneficiario: _mostrar_beneficiario,
        abrir_eliminar_beneficiario: _abrir_eliminar_beneficiario,
        reactivar_beneficiario: _reactivar_beneficiario,
        agregar_tabla_apoyos: _agregar_tabla_apoyos
      }
    })();
  </script>

  <!-- Scripts Datos Generales y Remitente -->
  <script type="text/javascript">
    // Inicializar el Select del Titular
    $('#titular_id').select2({
      language: 'es',
      allowClear : true,
      placeholder : 'SELECCIONE...',
      ajax: {
        url: "{{ route('titular.select') }}",
        dataType: 'JSON',
        type: 'GET',
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          // Agregamos la opcion OTRO al select
          data.push({
            id:'0',
            titulo:'',
				    nombre: 'OTRO',
			      primer_apellido: '',
			      segundo_apellido: ''
          });

          var r = $.map(data,item => {
            return {
              id: item.id,
              text: ((item.titulo == null ? 'C.':item.titulo )+' '+item.nombre+' '+item.primer_apellido+' '+item.segundo_apellido).toUpperCase(),
              slug: item.titulo+' '+ item.nombre+' '+item.primer_apellido+' '+item.segundo_apellido,
              results: item
            }
          });

          params.page = params.page || 1;

          return {
            results: r
          };
        },
        cache: true
      }
    }).change(evt => {
      $('#titular_id').valid();
      $('#otro').remove();
      if($('#titular_id').val() == '0'){
        //Agregar el input-persona OTRO
        if($("#gira").length > 0){
            $("#gira").before(Funi.crear_input_otro());
        }else{
            $('#div-dirigido').append(Funi.crear_input_otro());
        }
        Personas.abrir_modal_personas('otro_id');
      }else{
        Solicitud.nueva_solicitud.persona_id = $('#titular_id').val();
      }
    });

    // Inicializar el Select de Recepciones
    $('#tiporecepcion_id').select2({
      language: 'es',
      allowClear : true,
      placeholder : 'SELECCIONE...',
      ajax: {
        url: "{{ route('recepcion.select') }}",
        dataType: 'JSON',
        type: 'GET',
        data: params => {
          return {
            search: params.term
          };
        },
        processResults:(data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.tipo,
                slug: item.tipo,
                results: item
              }
            })
          };
        },
        cache: true
      }
    }).change(evt => {
      if ($('#tiporecepcion_id').valid()){
        Solicitud.nueva_solicitud.tiposrecepcion_id = $('#tiporecepcion_id').val();
        $('#gira').remove();
        if($('#tiporecepcion_id').find("option:selected").text() == "GIRA"){
            Giras.abrir_modal_giras();
        }
      }else{
          $('#gira').remove();
      }
	});

    // Inicializar el Select de Tipo Remitente
    $('#tiporemitente_id').select2({
      language: 'es',
      allowClear : true,
      placeholder : 'SELECCIONE...',
      ajax: {
        type: "GET",
        url: "{{ route('remitente.select') }}",
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.tipo,
                slug: item.tipo,
                results: item
              }
            })
          };
        },
        cache: true
      }
    }).change(evt => {
      if ($('#tiporemitente_id').valid()) { 
        Solicitud.nueva_solicitud.tiposremitente_id = $('#tiporemitente_id').val();               
        // Agregar los input necesarios por opcion
        let tipo = $('#tiporemitente_id').val();
        if(Remitente.getSelect() != tipo){
          Remitente.setSelect(tipo);
          $('#body_form_remitente').empty();
          $('#rowDatosRemitente').empty();
          $('#body_form_remitente').append(Remitente[tipo]);
          if(Solicitud.nueva_solicitud.solicitante_id !== undefined){
              $("#remitente_id").html("<option value='"+Solicitud.nueva_solicitud.solicitante_id+"'selected> "+Solicitud.nueva_solicitud.nombre_solicitante+" </option>");
          }          
          Remitente.init_select2(tipo);
        }
      }
      if(Solicitud.nueva_solicitud.cargo !== undefined){            
            console.log(Solicitud.nueva_solicitud.cargo);
            
            var option = new Option(Solicitud.nueva_solicitud.cargo, Solicitud.nueva_solicitud.cargo, true, true);
            $('#cargo').append(option).trigger('change');
        }
		});

    // Inicializar los DatePicker
    $('#fechaoficio, #fecharecepcion').datepicker({
      autoclose: true,
      language: 'es',
      endDate: '0d',
      orientation: 'bottom'
    });

    $('#form_cancelar').validate({
        rules:  {
            motivo: {
                required: true
            }
        }
    });

    // Inicializar Select de Beneficios
    Beneficios.load_beneficios($('#tipo').val()).change(evt => {
      $.ajax({
        url: '{{ route('area.find')}}',
        dataType: 'JSON',
        type: 'GET',
        data: {
          id: $('#beneficios').val()
        },
        success: r => {
          $('#areaR').text(r.area);
          $('#areaT').text(r.responsable);
          $('#prog').text(r.programa);
        }
      })
    });

    //Inicializar funciones para los Modals Personas
    $.fn.modal.Constructor.prototype.enforceFocus = () => {};
    app.to_upper_case();
	  app.agregar_bloqueo_pagina();    
    Personas.agregar_modal_static();
    Personas.init_modal_persona();
    //Giras.init_modal_giras();
      
    var seleccionar_persona = (id, nombre) => {
      Personas.seleccionar_persona(id, nombre);
    }

    var agregar_persona = () => {
      Personas.agregar_persona();
    }

    var persona_create_edit_error = (response) => {
      Personas.persona_create_edit_error(response);
    }

    var persona_create_edit_success = (response) => {
      Personas.persona_create_edit_success(response)
    }

    var dependencia_create_edit_success = (response) => {
		app.set_bloqueo(false);
		unblock();
        cerrar_modal('modal-dependencia');
        var option = new Option(response.nombre, response.id, true, true);
        $("#dependencias").append(option).trigger('change');        
	};

	var dependencia_create_edit_error = (response) => {
		unblock();
    };

    var abrir_modal = (modal) => {
        if(modal === "modal-dependencia"){
            $.get('/dependencias/create', function(data) {            
                $('#modal-body-dependencia').html(data.html);                            
                dependencia.init();                
                $('#modal-dependencia').modal('show');            
            });
        }
	};

    function cerrar_modal(modal){
        $("#"+modal).modal("hide");
    }

    $(document).ready(() => {       

        Solicitud.init_beneficios();
        Beneficiarios.init_beneficiarios();

      //Set headers|
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      // Activar los tooltip
      $('[data-toggle="tooltip"]').tooltip();

      // Desactivar click en los tabs
      $('a[data-toggle="tab"]').on('click', () => {
        if ($(this).parent('li').hasClass('disabled')) {
          return false;
        };
      });

      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
     });

    $('#compromiso').on('ifChecked', function(event){
        Solicitud.nueva_solicitud.compromiso = 1;        
    });

    $('#compromiso').on('ifUnchecked', function(event){
        Solicitud.nueva_solicitud.compromiso = 0;
    });


      $('#modal-beneficiario').on('shown.bs.modal', function (event) {
            if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
                Beneficiarios.agregar_tabla_apoyos();
		    }
	    });

      @if(isset($solicitud))

      Solicitud.nueva_solicitud.solicitud_id = {{ $solicitud->id }};
      $("[name='tabsolicitud']").removeClass('disabled');
      Solicitud.nueva_solicitud.persona_id = {{ $solicitud->persona_id }};
      Solicitud.nueva_solicitud.tiposrecepcion_id = {{ $solicitud->tiposrecepcion_id }};
      Solicitud.nueva_solicitud.tiposremitente_id = {{ $solicitud->tiposremitente_id }};
      Solicitud.nueva_solicitud.numoficio = '{{ $solicitud->numoficio }}';
      Solicitud.nueva_solicitud.asunto = '{{ json_encode( $solicitud->asunto )}}';

        @if($solicitud->compromiso)
            Solicitud.nueva_solicitud.compromiso = 1;
        @endif

        @if($solicitud->solicitudpersonales)
            Solicitud.nueva_solicitud.solicitante_id = {{ $solicitud->solicitudpersonales->persona_id }};
            Solicitud.nueva_solicitud.nombre_solicitante = '{{ $solicitud->solicitudpersonales->persona->get_nombre_completo() }}';

        @elseif($solicitud->solicitudmunicipales)
            @if($solicitud->solicitudmunicipales->cargo)            
                Solicitud.nueva_solicitud.cargo = '{{ $solicitud->solicitudmunicipales->cargo->nombre }}';                
            @endif
            Solicitud.nueva_solicitud.remitente = {{ $solicitud->solicitudmunicipales->municipio_id }};
            Solicitud.nueva_solicitud.nombre_remitente = '{{ $solicitud->solicitudmunicipales->municipio->nombre }}';
            Solicitud.nueva_solicitud.solicitante_id = {{ $solicitud->solicitudmunicipales->persona_id }};
            Solicitud.nueva_solicitud.nombre_solicitante = '{{ $solicitud->solicitudmunicipales->persona->get_nombre_completo() }}';
            
        @elseif($solicitud->solicitudregionales)
            @if($solicitud->solicitudregionales->cargo)
                Solicitud.nueva_solicitud.cargo = '{{ $solicitud->solicitudregionales->cargo->nombre }}';                
            @endif
            Solicitud.nueva_solicitud.remitente = {{ $solicitud->solicitudregionales->region_id }};
            Solicitud.nueva_solicitud.nombre_remitente = '{{ $solicitud->solicitudregionales->region->nombre }}';
            Solicitud.nueva_solicitud.solicitante_id = {{ $solicitud->solicitudregionales->persona_id }};   
            Solicitud.nueva_solicitud.nombre_solicitante =  '{{ $solicitud->solicitudregionales->persona->get_nombre_completo() }} ';

        @elseif($solicitud->solicituddependencias)
            Solicitud.nueva_solicitud.remitente = '{{ $solicitud->solicituddependencias->dependencia->nombre }}';
            Solicitud.nueva_solicitud.nombre_remitente = {{ $solicitud->solicituddependencias->dependencia_id }};
        @endif      

        @foreach( $solicitud->programassolicitudes as $programa_solicitud )
            if( '{{$programa_solicitud->statusActual()}}' !== 'CANCELADO') {
                Beneficios.lista_beneficios.push({
                    id: '{{ $programa_solicitud->programa_id }}',
                    nombre: '{{ $programa_solicitud->programa->nombre }}',
                    cantidad: '{{ $programa_solicitud->cantidad }}'
                });
            }
        @endforeach
        
        var option = new Option('{{ $solicitud->tiposremitente->tipo }}',Solicitud.nueva_solicitud.tiposremitente_id, true, true);
        $('#tiporemitente_id').append(option).trigger('change');        

      @else
        //Init DatePicker con Fecha Actual
        $('#fecharecepcion').datepicker('setDate', new Date().toDateString());
        $('#fechaoficio').datepicker('setDate', new Date().toDateString());
      @endif

      Solicitud.nueva_solicitud.fechaoficio = $('#fechaoficio').val();
      Solicitud.nueva_solicitud.fecharecepcion = $('#fecharecepcion').val();
        
      //Change-Valid Fechas
      $('#fechaoficio,#fecharecepcion').datepicker().on('changeDate', evt => {
        if(!Solicitud.valid_fechas($('#fecharecepcion'),$('#fechaoficio'))){
          swal(
            '¡Advertencia!',
            'La fecha de oficio debe ser anterior a la de recepción',
            'warning'
          );
        }else{
          //$('#fecharecepcion').datepicker('setDate', $('#fechaoficio').datepicker('getDate'));
          Solicitud.nueva_solicitud.fechaoficio = $('#fechaoficio').val();
          Solicitud.nueva_solicitud.fecharecepcion = $('#fecharecepcion').val();
        }
      });

      //Init Validaciones Form Solicitud (Datos Generales)
      $('#form_solicitud').validate({
        rules: {
            titular_id: {
				required: true
			},
			tiporecepcion_id: {
				required: true
			},
			tiporemitente_id: {
				required: true
            },
            numoficio: {
                required: true,
                minlength: 1,
                maxlength: 30                
          },
          fechaoficio: {
			required: true
		    },
          fecharecepcion: {
            required: true
          },
          asunto: {
            required: true,
            minlength: 10,
          },
          otro_id: {
			required: true
		}
        },
        messages: {
            numoficio: { 
                required: "Ingrese X si no dispone de uno.",
                maxlength: "Por favor, no escribas más de 30 caracteres."
            }
        }
      });

      //Init Validaciones Form Remitente
      $('#form_remitente').validate({
        rules: 	{
          remitente_municipio_id: {
            required: true
          },
          remitente_region_id: {
            required: true
          },
          cargo: {
            required: true
          },
          dependencia_id: {
            required: true
          },
          remitente_id: {
            required: true
          }
        }
      });

      $('#form_producto').validate({
		rules: 	{
			dependencias:{
				required: true
            },
            tag:{
				required: true
			},
			beneficios: {
				required: true
			},
			cantidad: {
				required: true,
				minlength: 1,
				pattern_folios: 'solo numeros, X si no dispone de uno.'
			}
		}
	});

    $('#btnFinalizar').on('click', () => {
        //Nose si turnar todas las peticiones
        //O si decirle que se turnaran todas las peticiones posibles
        //O si decirle en que si esta de acuerdo en la linea de arriba
        //Mientras lo redirijo al index de solicitudes LOL
        app.set_bloqueo(false);
        location.href = "{{ route('solicitudes.index', ['tipo_sol' => $tipo_solicitud ]) }}";
    });

      // Guardar Datos Generales y Remitente
      $('#btnNext_solicitud').on('click', () => {
          $('#tiporemitente_id').rules('add', { required: true });
        @if(isset($solicitud))
            if($("#tiporemitente_id").select2('data').length === 0){
                $('#tiporemitente_id').rules('remove', 'required');
            }
        @endif

        // Validar ambos formularios (Datos Generales y Remitente)
        if($('#form_solicitud').valid() && $('#form_remitente').valid()){
          // Validar Fechas
          if(!Solicitud.valid_fechas($('#fecharecepcion'),$('#fechaoficio'))){
            swal(
              '¡Advertencia!',
              'La fecha de oficio debe ser anterior a la de recepción',
              'warning'
            );
          } else {

            Solicitud.nueva_solicitud.numoficio = ($('#numoficio').val() == 'X') ? 0 : $('#numoficio').val();
            Solicitud.nueva_solicitud.asunto = $('#asunto').val();

            //Update
            if(Solicitud.nueva_solicitud.solicitud_id){
                Solicitud.guardar_edicion();
            }
            //Store
            else{
                // Asignar el tipo de Solicitud, Status , numOficio Y Asunto
                Solicitud.nueva_solicitud.tipo = '{{ $tipo_solicitud }}';
                Solicitud.nueva_solicitud.status = "pendiente";
                Solicitud.guardar_solicitud();
            }            
          }
        }
      });

      $('#btnPrevious_peticion').on('click', () => {          
        $('a[href="#tab_solicitud').tab('show');
      });

      $('#btnPrevious_beneficiarios').on('click', () => {
          $("[href='#tab_beneficiarios']").parent().hide();
          $('a[href="#tab_peticion').tab('show');
      });

      $("#motivo").select2({
		language: 'es',
		placeholder: 'SELECCIONE UN MOTIVO',
        minimumResultsForSearch: Infinity			
	 }).change(function(event) {
		$("#motivo").valid();				
      });
    });
  </script>

  <!-- Scripts Peticion (Beneficios) -->
  <script type="text/javascript">
    // Inicializar el select TIPO 
    $('#tipo').select2({
      language: 'es'
    }).change(evt => {
      // Cargar Beneficios segun el tipo
      Beneficios.load_beneficios($('#tipo').val());
      $('#areaR').text('---------------------');
      $('#areaT').text('---------------------');
      $('#prog').text('---------------------');
    });

    // Asignarle una accion al boton agregar beneficio
    $('#btnAgregar').on('click',() => {
      if($('#form_producto').valid()){
        Beneficios.agregar_beneficio();
      }else{
        var msg = 'Debe seleccionar un beneficio y la cantidad';
        if ('{{ $tipo_solicitud }}' == 'E'){
            msg = 'Debe seleccionar una dependencia, ingresar el tag y la cantidad';
        }
        swal(
          '¡Advertencia!',
          msg,
          'warning'
        )
      }
    });

  </script>
@endsection