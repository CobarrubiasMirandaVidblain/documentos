<div class="box-body">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:30%">
                            <i class="fa fa-id-card margin-r-5"></i>
                            Folio:
                        </th>
                        <td id="programa_folio"></td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-cubes margin-r-5"></i>
                            Apoyo Solicitado:
                        </th>
                        <td id="programa_nombre"></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:40%">
                            <i class="fa fa-signal margin-r-5"></i>
                            Estatus
                        </th>
                        <td id="programa_estatus"></td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-list-ol margin-r-5"></i>
                            Cantidad
                        </th>
                        <td id="programa_cantidad"></td>
                    </tr>                                    
                </table>
            </div>
        </div>
    </div>

    <div class="row">        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">Lista de beneficiarios</h4>
                </div>
                <div class="box-body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="searchBeneficiario" name="searchBeneficiario" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-info btn-flat" id="btn_buscarBeneficiario" name="btn_buscarBeneficiario">Buscar</button>
                        </span>
                    </div>
                    {!! $solicitantes->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'solicitantesDT', 'name' => 'solicitantesDT', 'style' => 'width: 100%']) !!}
                </div>
            </div>
        </div>
    </div>      
</div>

<form id="form_cancelar" method="POST" action="#"> 
    <div class="modal fade" id="modal-cancelar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">¿Está seguro de cancelar al solicitante?</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">                    
                            <label>Solicitante: </label>
                            <span id="lblSolicitante"></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="motivo">Motivo de cancelación:</label>
                            <select id="motivo" name="motivo" class="form-control select2" style="width: 100%;">
                                <option value="" disabled selected>SELECCIONE UN MOTIVO...</option>
                                @foreach($motivos as $motivo)
                                    <option value="{{ $motivo->id }}">{{ $motivo->motivo }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>
                    <button type="button" class="btn btn-success" id="btn_eliminar_beneficiario"><i class="fa fa-check"></i> Aceptar</button>            
                </div>
            </div>
        </div>
    </div>
</form>