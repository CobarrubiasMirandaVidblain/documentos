<script>
    var Beneficiarios = (()=>{
        
        var datatable_beneficiarios, programa_id, beneficioprograma_solicitud_id;
        var nuevo_beneficiario = false;

        var inicializar = () => {
            datatable_beneficiarios = $('#solicitantesDT').DataTable();
            datatable_beneficiarios.on( 'xhr', function () {
                var json = datatable_beneficiarios.ajax.json();
                $("#programa_nombre").text(json.programa_nombre);
                $("#programa_cantidad").text((json.programa_cantidad === 0) ? 'X' : json.programa_cantidad);
                $("#programa_estatus").text(json.programa_estatus);  

                if(json.programa_estatus === 'VINCULADO'){
                    $(".nuevo-solicitante").hide().removeClass("button-dt");
                }else{
                    $(".nuevo-solicitante").show().addClass("button-dt");
                }
            });
            $('#searchBeneficiario').keypress(function(e) {
			    if(e.which === 13) {
				    datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
			    }
		    });

		    $('#btn_buscarBeneficiario').on('click', function() {
			    datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
		    });
        }

        var agregar = (id, beneficio_solicitud_id, folio) => {
            Solicitud.resetBoton();
            programa_id = id;
            beneficioprograma_solicitud_id = beneficio_solicitud_id;
            $("#programa_folio").text(folio);
            recargar();
            $("[href='#tab_beneficiarios']").parent().show();
            $( "#tabSolicitud" ).tabs( { active: 2 } );
        }

        var getProgramaId = () => {
            return programa_id;
        }

        var getBeneficioProgramaSolicitudID = () => {
            return beneficioprograma_solicitud_id;
        }

        var recargar = () => {
            datatable_beneficiarios.ajax.reload(null, false);
        }

        var cancelar = () => {
            if(nuevo_beneficiario){                
                swal({
                    title: '¿Desea cancelar el registro del beneficiario?',
                    text: '',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sí, cancelar',
                    cancelButtonText: 'No, regresar',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        nuevo_beneficiario = false;
                        $('#modal-beneficiario').modal('hide');
                    }
                });
            } else {
                $('#modal-beneficiario').modal('hide');
            }
        }

        var mostrar = (id) => {
            block();
            var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.show", [":id",":programa",":persona"] ) }}';
            url = url.replace(':id', Solicitud.getId())
                    .replace(':programa', getProgramaId())
                    .replace(':persona', id);
            $.get(url, function(data) {
                $('#modal-beneficiario').html(data.beneficiario);
                if(nuevo_beneficiario){
                    $("#modal-footer-beneficiario button").show();
                }else{
                    $("#modal-footer-beneficiario button").hide();
                }
                $('#modal-beneficiario').modal('show');
                //$("#modal-footer-beneficiario button:not(:eq(0))").hide();
                /*$('#btn_editar_persona').off('click');
                $('#btn_editar_persona').on('click',() => {
                    Personas.editar_persona(persona_id);
                });*/

                /*if(Beneficiarios.programa.estatus === 'VINCULADO' || data.estatus !== null){
                    $('#btn_editar_persona').hide();
                }*/   
                unblock();
            });
        }

        var guardar = (id) => {
            if($('#curpBeneficiario').text()){
                block();
                var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.store", [":id",":programa"] ) }}';
                url = url.replace(':id', Solicitud.getId()).replace(':programa', getProgramaId());
                $.ajax({
			        url: url,
			        type: 'POST',
			        data: "persona_id=" + id,
			        data: {
                        persona_id: id,
                        beneficioprograma_solicitud_id: getBeneficioProgramaSolicitudID()
                    },
			        success: function(response) {
                        $('#modal-beneficiario').modal('hide');      
                        recargar();
                        Solicitud.recargar_peticiones();
                        unblock();
                        swal(                            
                            '¡Correcto!',
                            'Beneficiario registrado',
                            'success'
                        )                                      
			        },
                    error: function(response){
                        unblock();
                        swal(
                            'Error',
                            response.responseJSON.errors[0].message,
                            'error'
                        )               
                    }
		        });                
            }else {
                swal(
                    'Error',
                    'Debe ingresar la CURP',
                    'error'
                );
            }
        }

        var abrir_modal_eliminar = (id, nombre) => {
            $('#btn_eliminar_beneficiario').off('click');
            $('#btn_eliminar_beneficiario').on('click',() => {
                eliminar(id);
            });
            $("#lblSolicitante").text(nombre);
            $('#motivo').val(null).trigger('change');
            $("#modal-cancelar").modal("show");
        }

        var eliminar = (id) => {
            if($('#form_cancelar').valid()) {
                block();
                $("#modal-cancelar").modal("hide");
                var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.destroy", [":id",":programa",":persona"]) }}';
                url = url.replace(':id', Solicitud.getId())
                    .replace(':programa', getProgramaId())
                    .replace(':persona', id);
                $.ajax({
                    type: "DELETE",
                    url: url,
                    data: "motivo="+$("#motivo").find(":selected").text(),
                    success: function(response){
                        Solicitud.recargar_peticiones();
                        recargar();
                        unblock();
                        swal(                                
                            '¡Correcto!',
                            'Beneficiario cancelado',
                            'success'
                        )
                    },
                    error: function(response){
                        if(response.status === 409){
                            unblock();
                            swal(                                
                                '¡Ocurrió un error inesperado!',
                                response.responseJSON.message,
                                'error'
                            )
                        }                    
                    }
                });
            }          
        }

        var reactivar = (id, nombre) => {
            swal({
                title: '¿Está seguro de reactivar al beneficiario?',
                text: nombre,
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                cancelButtonText: 'No',
                confirmButtonText: 'Sí, reactivar'
            }).then((result) => {
                if(result.value) {
                    block();
                    var url = '{{ route("atnciudadana.solicitudes.programas.beneficiarios.update", [":id",":programa",":persona"] ) }}';
                    url = url.replace(':id', Solicitud.getId())
                        .replace(':programa', getProgramaId())
                        .replace(':persona', id);
                    $.ajax({
				        url: url,
				        type: "PUT",
				        success: function(response) {
                            app.set_bloqueo(false);
                            Solicitud.recargar_peticiones();
                            recargar();
                            swal({
                                title: '¡Correcto!',
                                text: 'Solicitante reactivado',
                                type: 'success',
                                timer: 2500,
                                showConfirmButton: false
                            });                        
                        },
                        error: function(response) {
                            unblock();                        
                            if(response.status === 422) {
			                    swal({
				                    title: 'Error al reactivar',
				                    text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]].message,
				                    type: 'error',
				                    confirmButtonColor: '#3085d6',
				                    confirmButtonText: 'Regresar',
				                    allowEscapeKey: false,
				                    allowOutsideClick: false
			                    });
		                    } else {
                                swal(
                                    'Error',
                                    '',
                                    'error'
                                )
                            }                        
                        }
			        });                
                }
            });
        }

        return {
            inicializar,
            agregar,
            guardar,
            mostrar,
            getProgramaId,
            cancelar,
            abrir_modal_eliminar,
            eliminar,
            reactivar,
            getBeneficioProgramaSolicitudID
        }
    })();
</script>