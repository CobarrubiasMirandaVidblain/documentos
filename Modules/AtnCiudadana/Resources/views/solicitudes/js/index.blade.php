<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script>

var table = $('#solicitudes').dataTable();

datatable_solicitudes = $(table).DataTable();

datatable_solicitudes.on( 'xhr', function () {
    var data = datatable_solicitudes.ajax.json();
    $("#peticiones_nuevas").text( data.peticiones_nuevas );
    $("#peticiones_proceso").text( data.peticiones_proceso );
    $("#peticiones_finalizadas").text( data.peticiones_finalizadas );
    $("#peticiones_canceladas").text( data.peticiones_canceladas );
});

$('#btn_buscar').keypress(function(e) {
  if(e.which === 13) {
    datatable_solicitudes.search($('#btn_buscar').val()).draw();
  }
});

$('#btn_buscar').on('click', function() {
  datatable_solicitudes.search($('#search').val()).draw();
});          

$('#tiporemitente_id').select2({
    language: 'es',
    ajax: {
        type: "GET",
        url: "{{ route('remitente.select') }}", 
        dataType: "json",
        data: params => {
            return {
                search: params.term
            };
        },
        processResults: (data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.tipo,
                        slug: item.tipo,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
});
$("#rows").select2({
    minimumResultsForSearch: Infinity
});

    
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

var turnar = (solicitud_id, programa_id) => {    
    swal({
          title: '¡Advertencia!',
          text: "No podrá editar el beneficio ni agregar beneficiarios. ¿Desea turnar la petición?",
          type: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí',
          cancelButtonText: 'Cancelar'
        }).then(r => {
          if(r.value){
            var url = '{{ route("atnciudadana.solicitudes.programas.estatus", [":id",":programa"]) }}';
            url = url.replace(':id', solicitud_id).replace(':programa', programa_id);
            block();

            $.ajax({
                url: url,
                data: 'estatus_id=3',
                type: 'POST',
                success: function(response) {
                    datatable_solicitudes.ajax.reload(null, false);
                    swal(                            
                        '¡Correcto!',
                        'Petición turnada',
                        'success'
                    )
                    unblock();
                },
                error: function(response) {
                    swal({
                        title: 'Error al turnar',				
                        text: '',
                        type: 'error',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Regresar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                    unblock();
                }
            });           
          }
        });
}

var Peticion = (() => {

        //DataTable de peticiones
        var peticiones;
        var ejercicio;

        var filtrar_peticiones = (columna, valor) => {
            datatable_solicitudes.columns(columna).search(valor).draw();
        }

        return {            
            filtrar_peticiones
        }

    })();

 
</script>