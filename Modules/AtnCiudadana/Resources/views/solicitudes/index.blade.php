@extends('atnciudadana::layouts.master')

@section('myCSS')
	@include('atnciudadana::solicitudes.css.index')
@endsection

@section('content-subtitle', $titulo)

@section('li-breadcrumbs')
  <li class="active">{{ $titulo }}</li>
@endsection

@section('content')
<section class="content">

    @if($titulo == 'Peticiones')
    <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-green" onclick="Peticion.filtrar_peticiones(8, 'Vo. Bo.')">
                    <div class="inner">
                        <h3 id="peticiones_nuevas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones nuevas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-edit"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-yellow" onclick="Peticion.filtrar_peticiones(8, 'VALIDANDO')">
                    <div class="inner">
                        <h3 id="peticiones_proceso"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones en proceso</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-aqua" onclick="Peticion.filtrar_peticiones(8, 'FINALIZADO')">
                    <div class="inner">
                        <h3 id="peticiones_finalizadas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones finalizadas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-red" onclick="Peticion.filtrar_peticiones(8, 'CANCELADO')">
                    <div class="inner">
                        <h3 id="peticiones_canceladas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                        <p>Peticiones canceladas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-minus-square"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif

    <div class="row">
    	<div class="col-xs-12 no-padding-lr">
    	    <div class="box box-primary shadow">
                <div class="box-header with-border">
                        <h3 class="box-title">Lista de {{ $titulo }}</h3>
                </div>
				<div class="box-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
              <input type="text" id="search" name="search" class="form-control">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
              </span>
            </div>
            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'solicitudes', 'name' => 'solicitudes', 'style' => 'width: 100%']) !!}              
				  </div>
    		</div>
    	</div>
    </div>
  </section>
@stop

@section('myScripts')
    @include('atnciudadana::solicitudes.js.index')
@endsection