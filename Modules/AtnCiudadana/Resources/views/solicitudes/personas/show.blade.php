<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_Datos" data-toggle="tab">Datos Generales</a></li>
        <li><a href="#tab_Historial" data-toggle="tab">Historial de Apoyos</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_Datos">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form class="form-horizontal">
                        <div class="row">
                            <label class="col-sm-2 control-label">Nombre:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static">{{ $persona->nombreCompleto() }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">Género:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->genero == 'F' ? 'FEMENINO' : 'MASCULINO' }}</p>
                            </div>
                            <label class="col-sm-4 control-label">Fecha de nacimiento:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ isset($persona->fecha_nacimiento) ? $persona->get_formato_fecha_nacimiento() : '' }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">CURP:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static" id="curpBeneficiario" data-persona-id="{{ $persona->id }}">{{ $persona->curp }}</p>
                            </div>
                            <label class="col-sm-4 control-label">Clave electoral:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->clave_electoral }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-label">Teléfono:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->numero_local }}</p>
                            </div>
                            <label class="col-sm-4 control-label">Celular:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->numero_celular }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">Correo electrónico:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static">{{ (isset($persona->email) ? $persona->email : '') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">Domicilio:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static">{{ $persona->calle . ' ' . $persona->numero_exterior . (isset($persona->numero_interior) ? '-'.$persona->numero_interior : '') . ' ' . $persona->colonia }}
                                    <br> {{ (isset($persona->localidad) ? $persona->localidad->nombre.', ' : '') . ' ' . $persona->municipio->nombre . (isset($persona->codigopostal) ? ', C.P. ' . $persona->codigopostal : '') }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">Referencias del domicilio:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static">{{ (isset($persona->referencia_domicilio) ? $persona->referencia_domicilio : '') }}</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>            
        </div>
        <div class="tab-pane disabled" id="tab_Historial">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table id="apoyos-otorgados" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center">PETICIÓN</th>
                                <th class="text-center">FECHA</th>
                                <th class="text-center">PROGRAMA</th>
                                <th class="text-center">PRODUCTO</th>
                            </tr>   
                        </thead>
                        <tbody>
                            @foreach($persona->solicitudespersonas->where("entregado",1)->take(5) as $beneficioOtorgado)
                                <tr>
                                    <td class="text-center">{{ $beneficioOtorgado->programas_solicitud->solicitud->folio }}</td>
                                    <td class="text-center">{{ $beneficioOtorgado->salida_producto->get_formato_fecha() }}</td>
                                    <td class="text-center">{{ $beneficioOtorgado->programas_solicitud->programa->nombre }}</td>
                                    <td class="text-center">{{ $beneficioOtorgado->salida_producto->detallesalidasproductos->first()->areasproducto->producto->producto }}</td>
                                </tr>
                            @endforeach
                            {{-- @foreach($historial as $apoyo_en_bd_fea)
                                <tr>
                                    <td class="text-center">{{ $apoyo_en_bd_fea->folio }}</td>
                                    <td class="text-center">{{ Carbon\Carbon::parse($apoyo_en_bd_fea->cp_fec_atendida)->format('d-m-Y') }}</td>
                                    <td class="text-center">{{ strtoupper($apoyo_en_bd_fea->producto) }}</td>
                                    <td class="text-center">{{ strtoupper($apoyo_en_bd_fea->cp_unimed) }}</td>
                                </tr>
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>