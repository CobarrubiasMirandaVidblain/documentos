<script type="text/javascript">        
    var app = (function() {

		var bloqueo = false;

        $("form").submit(function(e){
            e.preventDefault();
        });
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

		var to_upper_case = () => {
			$(':input').on('propertychange input', (e) => {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		var set_bloqueo = (valor) => {
			bloqueo = valor;
		};

		var get_bloqueo = () => {
			return bloqueo;
		};

		var agregar_bloqueo_pagina = () => {
			$('form :input').change(() => {
				bloqueo = true;
			});

			window.onbeforeunload = (e) => {
				if(bloqueo) {
					return '¿Está seguro de salir?';
				}
			};
        };

        var cerrar_modal = (modal) => {
            $("#"+modal).modal("hide");
        }

        var ajaxHelper = (url, method, data) => {
            return $.ajax({
                type: method,
                url: url,
                data: data ? data : null,
                beforeSend: () => {
                    block();
                },
                complete: () => {
                    unblock();
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                swal(
                    '¡Error!',
                    jqXHR.responseJSON.errors[0],
                    'error'
                )
            });
        }

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
            agregar_bloqueo_pagina: agregar_bloqueo_pagina,
            ajaxHelper,
            cerrar_modal
		};
	})();
</script>