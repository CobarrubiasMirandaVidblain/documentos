<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte</title>
    <link rel="stylesheet" href="{{ asset('reportes/bootstrap.min.css') }}" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .container {
            margin-left: 0px;
            margin-right: 15px;
            width: 100% !important;
        }
        
        .row {
            margin-right: 0px; 
            margin-left: 0px; 
        }

        .center {
            text-align: center;
        }
        
        body {
            margin-top: 20px;
            repeat: no-repeat;
            opacity: 50;
            position: relative;
            bottom: 0;
            float: left;
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container">
    
        {{-- <div class="row">
            <label>Evento: {{ $gira->nombre }}</label>
            <label>Fecha: {{ $gira->get_formato_fecha() }}</label>
            <label>Municipio: {{ $gira->municipio->nombre }}</label>
            <label>Localidad: @if($gira->localidad_id)
                                {{ $gira->localidad->nombre }}
                            @elseif($gira->localidad)
                                {{ $gira->localidad }}
                            @endif</label>
        </div> --}}

        <div class="row">
            <table class="table table-bordered table-condensed table-striped" style="width: 100%;">
                @foreach($data as $row)
                    @if ($row == reset($data)) 
                        <tr>
                            @foreach($row as $key => $value)
                                <th class="center">{!! $key !!}</th>
                            @endforeach
                        </tr>
                    @endif
                    <tr>
                        @foreach($row as $key => $value)
                            @if(is_string($value) || is_numeric($value))
                                <td>{!! $value !!}</td>
                            @else
                                <td></td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    
    <script src="{{ asset('reportes/jquery.min.js') }}"></script>
    <script src="{{ asset('reportes/bootstrap.min.js') }}" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>