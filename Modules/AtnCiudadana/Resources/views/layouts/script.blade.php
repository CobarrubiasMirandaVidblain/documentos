<!-- jQuery 3 -->
<script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- date-range-picker -->
<script type="text/javascript" src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap datepicker locales -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<!-- bootstrap color picker -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script type="text/javascript" src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- Select2 -->
<script type="text/javascript" src="{{ asset('bower_components/select2/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<!-- InputMask -->
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- blockui -->
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<!-- sweetalert2 -->
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- Configuracion BlockUI -->
<script type="text/javascript">
  function block() {
      baseZ = 1030;        
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

    $.blockUI({
      css: {
        border: 'none',
        padding: '5px',
        opacity: .9,
        backgroundColor: 'none',
      },
      baseZ: baseZ,
        message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#fff; margin-top:5px;font-family:arial; font-size: larger;">Procesando...</p></div>',
    });

    function unblock_error() {
      if($.unblockUI())
        alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
    }
    setTimeout(unblock_error, 120000);
  }

  function unblock() {
    $.unblockUI();
  }
</script>