<!-- Bootstrap 3.3.7 -->
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<!-- Ionicons -->
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<!-- daterange picker -->
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<!-- bootstrap datepicker -->
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- iCheck for checkboxes and radio inputs -->
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<!-- Bootstrap Color Picker -->
<link href="{{ asset('bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- Bootstrap time Picker -->
<link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- Select2 -->
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<!-- Theme style -->
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">
<!-- Estilos Modal Persona -->
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<!-- Estilos para la Data Table Responsive -->
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<!-- Estilos de los SweetAlert -->
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
