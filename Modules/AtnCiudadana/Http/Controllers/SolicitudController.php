<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use App\Models\Cargo;
use App\Models\EventosSolicitudes;
use App\Models\Modulo;
use App\Models\Motivo;
use App\Models\BeneficiosprogramasSolicitud;
use App\Models\Solicitud;
use App\Models\Area;
use App\Models\SolicitudesRegiones;
use App\Models\SolicitudesMunicipales;
use App\Models\SolicitudesDependencias;
use App\Models\SolicitudesPersonales;
use App\Models\PeticionesPersonas;
use App\Models\EstadosSolicitud;
use App\Models\Tiposrecepcion;
use App\Models\Tiposremitente;
use App\Models\ProgramasSolicitud;
use App\Models\AreasResponsable;
use App\Models\Usuario;
use App\Models\AreasPrograma;
use App\DataTables\BeneficiariosDataTable;
use App\DataTables\PersonasDataTable;
use App\DataTables\DependenciasDataTable;
use App\DataTables\PeticionesDataTable;
use App\DataTables\AtnCiudadana\BeneficiosDataTable;
use App\DataTables\AgendaEventos\GirasDataTable;
use App\DataTables\AtnCiudadana\SolicitantesDataTable;
use App\DataTables\AtnCiudadana\SolicitudesDataTable;
use App\DataTables\AtnCiudadana\BeneficiosExternosDataTable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

class SolicitudController extends Controller
{

  public function __construct()
  {
    $this->middleware(['auth', 'roles:CAPTURISTA,ADMINISTRADOR,ATNC-AREA', 'authorized']);
    $this->middleware(function (Request $request, $next) {
      $modulito = Modulo::where('nombre', 'ATENCION CIUDADANA')->first();
      $request['modulo_id'] = $modulito ? $modulito->id : '';
      return $next($request);
    });
  }

  public function index(Request $request, SolicitudesDataTable $dataTable)
  {
    if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'atnciudadana')) {
      if (auth()->user()->hasRolesStrModulo(['ATNC-AREA'], 'atnciudadana')) {
        return view('atnciudadana::solicitudes.index_area');
      }
      return view('atnciudadana::solicitudes.direccion');
    } else {
      $tipo_solicitud = ($request['tipo_solicitud'] && ($request['tipo_solicitud'] === 'i' || $request['tipo_solicitud'] === 'e')) ? strtoupper($request['tipo_solicitud']) : 'I';
      return $dataTable->with('tipo_solicitud', $tipo_solicitud)->render('atnciudadana::solicitudes.index', ['titulo' => 'Solicitudes']);
    }
  }

  public function indexSolicitudes(Request $request, SolicitudesDataTable $dataTable)
  {
    $tipo_solicitud = ($request['tipo_solicitud'] && ($request['tipo_solicitud'] === 'i' || $request['tipo_solicitud'] === 'e')) ? strtoupper($request['tipo_solicitud']) : 'I';
    return $dataTable->with('tipo_solicitud', $tipo_solicitud)->render('atnciudadana::solicitudes.index', ['titulo' => 'Solicitudes']);
  }



  public function peticiones(Request $request, PeticionesDataTable $dataTable)
  {
    $tipo_solicitud = ($request['tipo_solicitud'] && ($request['tipo_solicitud'] === 'i' || $request['tipo_solicitud'] === 'e')) ? strtoupper($request['tipo_solicitud']) : 'I';
    return $dataTable->with(['atnCiudadana' => true, 'modulo' => 'atnciudadana', 'permiso_para_turnar' => true, 'tipo_solicitud' => $tipo_solicitud])
      ->render('atnciudadana::solicitudes.index', ['titulo' => 'Peticiones']);
  }

  public function beneficiarios(Request $request, BeneficiariosDataTable $dataTable)
  {
    $tipo_solicitud = ($request['tipo_solicitud'] && ($request['tipo_solicitud'] === 'i' || $request['tipo_solicitud'] === 'e')) ? strtoupper($request['tipo_solicitud']) : 'I';
    return $dataTable->with(['programa_padre' => 'TODOS', 'modulo' => 'atnciudadana', 'permiso_para_turnar' => true, 'tipo_solicitud' => $tipo_solicitud])
      ->render('atnciudadana::solicitudes.index', ['titulo' => 'Beneficiarios']);
  }

  public function store(Request $request)
  {
    if ($request->ajax()) {
      try {
        DB::beginTransaction();

        // En datos solicitud excluimos lo que NO es necesario para una solicitud
        $datos_solicitud = $request->except(['beneficios', 'tipo', 'solicitante_id', 'remitente']);
        $datos_solicitud['fecharecepcion'] = (\DateTime::createFromFormat('d/m/Y', $datos_solicitud['fecharecepcion']))->format('Y-m-d');
        $datos_solicitud['fechaoficio'] = (\DateTime::createFromFormat('d/m/Y', $datos_solicitud['fechaoficio']))->format('Y-m-d');

        //Creamos el folio de la solicitud dependiendo el tipo (I o E + el id + el año)
        $datos_solicitud['folio'] = $request->input('tipo') . "/" . (Solicitud::where('folio', 'LIKE', $request->input('tipo') . '%')->max('id') + 1) . "/" . date("Y");

        //Usuarios que esta guardando la solicitud
        $datos_solicitud['usuario_id'] = Auth::user()->id;

        //Verificando si es un compromiso
        $datos_solicitud['compromiso'] = $request->has('compromiso') ? true : false;

        //Creamos la solicitud con los datos
        $solicitud = Solicitud::create($datos_solicitud);
        auth()->user()->bitacora(request(), [
          'tabla' => 'solicitudes',
          'registro' => $solicitud->id . '',
          'campos' => json_encode($solicitud) . '',
          'metodo' => 'POST'
        ]);

        if ($request->input('tiposrecepcion_id') == 1) {
          EventosSolicitudes::create([
            'evento_id' => $request->input('evento_id'),
            'solicitud_id' => $solicitud->id,
            'usuario_id' => Auth::user()->id
          ]);
        }

        $tipo_solicitud = ''; //Para la bitácora
        $tabla = ''; //Para la bitácora

        //Guardamos al remitente dependiedo el tipo (OJO depende del id que tengan en el catalogo)
        switch ($request->input('tiposremitente_id')) {
          case 1: //Solicitud Regional
            $tabla = 'solicitudes_regiones';
            $tipo_solicitud = SolicitudesRegiones::create([
              'solicitud_id' => $solicitud->id,
              'cargo_id' => Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo'))])->id,
              'region_id' => $request->input('region_id'),
              'persona_id' => $request->input('solicitante_id')
            ]);
            break;
          case 2: //Solicitud Municipal
            $tabla = 'solicitudes_municipios';
            $tipo_solicitud = SolicitudesMunicipales::create([
              'solicitud_id' => $solicitud->id,
              'cargo_id' => Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo'))])->id,
              'municipio_id' => $request->input('solicitud_municipio_id'),
              'persona_id' => $request->input('solicitante_id')
            ]);
            break;
          case 3: //Solicitud Institucional
            $tabla = 'solicitudes_dependencias';
            $tipo_solicitud = SolicitudesDependencias::create([
              'solicitud_id' => $solicitud->id,
              'dependencia_id' => $request->input('dependencia_id')
            ]);
            break;
          case 4: //Solicitud Personal
            $tabla = 'solicitudes_personales';
            $tipo_solicitud = SolicitudesPersonales::create([
              'solicitud_id' => $solicitud->id,
              'persona_id' => $request->input('solicitante_id')
            ]);
            break;
        }
        auth()->user()->bitacora(request(), [
          'tabla' => $tabla,
          'registro' => $tipo_solicitud->id . '',
          'campos' => json_encode($tipo_solicitud) . '',
          'metodo' => 'POST'
        ]);
        DB::commit();
        // Guardamos los datos en la BD y regresamos status 200 y el id de la solicitud creada
        return response()->json(['success' => array(['code' => 200, 'id' => $solicitud->id])], 200);
      } catch (Exeption $e) {
        DB::rollBack();
        return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
      }
    }
  }

  public function show($id_solicitud)
  {
    // dd($id_solicitud);
    $datos = Solicitud::where('id', $id_solicitud)->with(['tiposrecepcion', 'tiposremitente'])->get();
    $datos_generales = Solicitud::allSolicitudes($id_solicitud)->get();
    $timeline = array();
    // $programas = ProgramasSolicitud::join('programas as programas','programas.id','programas_solicitudes.programa_id')
    // ->join('programas as padre','padre.id','programas.padre_id')
    // ->where('solicitud_id',$id_solicitud)->groupby('programa_id','padre.nombre','programas.padre_id')->get(['padre.nombre','programas.padre_id']);

    $programas = BeneficiosprogramasSolicitud::join('beneficiosprogramas', 'beneficiosprogramas.id', 'beneficiosprogramas_solicitudes.beneficioprograma_id')
      ->join('anios_programas as ap', 'ap.id', 'beneficiosprogramas.anio_programa_id')
      ->join('programas as p', 'p.id', 'ap.programa_id')
      ->join('programas as padre', 'padre.id', 'p.padre_id')
      ->join('programas as abuelo', 'abuelo.id', 'padre.padre_id')
      ->join('areas_programas as arp', 'arp.programa_id', 'abuelo.id')
      ->join('cat_areas as ar', 'ar.id', 'arp.area_id')
      ->where('beneficiosprogramas_solicitudes.solicitud_id', $id_solicitud)
      ->get(['beneficiosprogramas.id', 'beneficiosprogramas.nombre', 'beneficiosprogramas_solicitudes.id as bpsid', 'ar.nombre as area_responsable', 'beneficiosprogramas_solicitudes.folio']);
    // dd(EstadosSolicitud::timelinesolicitud($id_solicitud)->get()->toarray());
    // foreach ($programas as $programa) {
    // 		array_push($timeline, ['nombre'=> $programa->nombre,'lineas'=>EstadosSolicitud::timelinesolicitud($id_solicitud)->get()->toarray()]);
    // }
    // dd($timeline);

    foreach ($programas as $programa) {
      array_push($timeline, ['folio' => $programa->folio, 'area_responsable' => $programa->area_responsable, 'nombre' => $programa->nombre, 'lineas' => EstadosSolicitud::timeline($programa->bpsid, $programa->id)->get()->toarray()]);
    }

    return view('atnciudadana::solicitudes.show', array('datos' => $datos, 'generales' => $datos_generales, 'timeline' => $timeline));
  }

  public function edit(Request $request, $solicitud_id, PersonasDataTable $personas, BeneficiosDataTable $beneficios, SolicitantesDataTable $solicitantes, GirasDataTable $giras, DependenciasDataTable $dependencias, BeneficiosExternosDataTable $beneficiosExt)
  {
    $tipo_solicitud = ($request['tipo_solicitud'] && (strtolower($request['tipo_solicitud']) === 'i' || strtolower($request['tipo_solicitud']) === 'e')) ? strtoupper($request['tipo_solicitud']) : 'I';
    $solicitud = Solicitud::find($solicitud_id);
    $motivos = DB::table('motivos_programas')
      ->select(['motivos_programas.id', 'cat_motivos.motivo'])
      ->where('programas.nombre', '=', 'ATENCIÓN CIUDADANA')
      ->join('cat_motivos', 'cat_motivos.id', '=', 'motivos_programas.motivo_id')
      ->join('programas', 'programas.id', '=', 'motivos_programas.programa_id')
      ->get();

    $recepciones = Tiposrecepcion::orderBy('tipo', 'asc')->get();
    $remitentes = Tiposremitente::all();
    $titulares = DB::table('titulares')
      ->join('personas', 'titulares.persona_id', '=', 'personas.id')
      ->selectRaw("personas.id, CONCAT(personas.nombre, ' ', personas.primer_apellido, ' ', personas.segundo_apellido) as nombre")->get();
    //$beneficio;
    if (request()->get('beneficioprograma_solicitud_id')) {
      $beneficio = BeneficiosprogramasSolicitud::where('id', request()->get('beneficioprograma_solicitud_id'))->firstOrFail();
    }

    if (request()->get('table') == 'beneficios') {
      return $beneficios->with(['solicitud_id' => $solicitud->id, 'tipo_solicitud' => $tipo_solicitud])->render('atnciudadana::solicitudes.create');
    }

    if (request()->get('table') == 'giras') {
      return $giras->with('tipo', 'modal')->render('atnciudadana::solicitudes.create');
    }

    //Si existe el parámetro tables y su valor es dependencias entonces se está manipulando el DataTables de Dependencias
    if (request()->get('table') == 'dependencias') {
      return $dependencias->with('tipo', 'modal')->render('atnciudadana::solicitudes.create');
    }

    //Ni idea solo lo copie por si jala lo de beneficios Externos
    if (request()->get('table') == 'beneficiosExt') {
      return $beneficiosExt->render('atnciudadana::solicitudes.create');
    }

    if (request()->get('table') == 'solicitantes') {
      if (isset($beneficio)) {
        return $solicitantes->with(['estatus' => $beneficio->estadoActual(), 'programa_nombre' => $beneficio->beneficio->nombre, 'programa_solicitantes' => PeticionesPersonas::where('beneficiosprogramas_solicitud_id', $beneficio->id)->whereNull('observacion_evaluado')->count(), 'programa_cantidad' => $beneficio->cantidad, 'programas_solicitud_id' => $beneficio->id])->render('atnciudadana::solicitudes.create');
      } else {
        return $solicitantes->render('atnciudadana::solicitudes.create');
      }
    }
    $beneficios->with('tipo_solicitud', $tipo_solicitud);
    $personas->with('tipo', 'afuncionalessolicitantes');
    $giras->with('tipo', 'modal');
    $dependencias->with('tipo', 'modal');
    return $personas->render('atnciudadana::solicitudes.create', compact('personas', 'beneficios', 'solicitantes', 'solicitud', 'giras', 'dependencias', 'motivos', 'tipo_solicitud', 'titulares', 'remitentes', 'recepciones', 'beneficiosExt'));
  }

  public function update($solicitud_id, Request $request)
  {
    if ($request->ajax()) {
      try {
        $solicitud = Solicitud::findOrFail($solicitud_id);
        DB::beginTransaction();

        // En datos solicitud excluimos lo que NO es necesario para una solicitud
        $datos_solicitud = $request->except(['beneficios', 'tipo', 'solicitante_id', 'remitente']);
        $solicitud->fecharecepcion = (\DateTime::createFromFormat('d/m/Y', $datos_solicitud['fecharecepcion']))->format('Y-m-d');
        $solicitud->fechaoficio = (\DateTime::createFromFormat('d/m/Y', $datos_solicitud['fechaoficio']))->format('Y-m-d');
        $solicitud->usuario_id = Auth::user()->id;
        $solicitud->numoficio = $datos_solicitud['numoficio'];
        $solicitud->asunto = $datos_solicitud['asunto'];
        $solicitud->tiposrecepcion_id = $datos_solicitud['tiposrecepcion_id'];
        $solicitud->tiposremitente_id = $datos_solicitud['tiposremitente_id'];
        $solicitud->persona_id = $datos_solicitud['persona_id'];
        $solicitud->compromiso = $request->has('compromiso') ? true : false;
        $solicitud->save();

        auth()->user()->bitacora(request(), [
          'tabla' => 'solicitudes',
          'registro' => $solicitud->id . '',
          'campos' => json_encode($solicitud) . '',
          'metodo' => 'PUT'
        ]);

        if ($request->input('tiposrecepcion_id') == 1) {
          $evento_solicitud = EventosSolicitudes::where('solicitud_id', $solicitud_id)->first();
          if (($evento_solicitud && $evento_solicitud->evento_id != $request->input('evento_id')) || !$evento_solicitud) {
            if ($evento_solicitud) {
              $evento_solicitud->delete();
            }
            EventosSolicitudes::create([
              'evento_id' => $request->input('evento_id'),
              'solicitud_id' => $solicitud->id,
              'usuario_id' => Auth::user()->id
            ]);
          }
        } else {
          $evento_solicitud = EventosSolicitudes::where('solicitud_id', $solicitud_id)->first();
          if ($evento_solicitud) {
            $evento_solicitud->delete();
          }
        }

        $solicitud_regional = SolicitudesRegiones::where('solicitud_id', $solicitud->id)->first();
        $solicitud_municipal = SolicitudesMunicipales::where('solicitud_id', $solicitud->id)->first();
        $solicitud_institucional = SolicitudesDependencias::where('solicitud_id', $solicitud->id)->first();
        $solicitud_personal = SolicitudesPersonales::where('solicitud_id', $solicitud->id)->first();

        //Guardamos al remitente dependiedo el tipo (OJO depende del id que tengan en el catalogo)
        switch ($request->input('tiposremitente_id')) {
          case 1: //Solicitud Regional
            if ($solicitud_municipal) {
              $solicitud_municipal->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_municipios',
                'registro' => $solicitud_municipal->id . '',
                'campos' => json_encode($solicitud_municipal) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_institucional) {
              $solicitud_institucional->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_dependencias',
                'registro' => $solicitud_institucional->id . '',
                'campos' => json_encode($solicitud_institucional) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_personal) {
              $solicitud_personal->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_personales',
                'registro' => $solicitud_personal->id . '',
                'campos' => json_encode($solicitud_personal) . '',
                'metodo' => 'DELETE'
              ]);
            }
            $solicitud_regional = SolicitudesRegiones::updateOrCreate([
              'solicitud_id' => $solicitud->id
            ], [
              'cargo_id' => Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo'))])->id,
              'region_id' => $request->input('region_id'),
              'persona_id' => $request->input('solicitante_id')
            ]);
            auth()->user()->bitacora(request(), [
              'tabla' => 'solicitudes_regiones',
              'registro' => $solicitud_regional->id . '',
              'campos' => json_encode($solicitud_regional) . '',
              'metodo' => 'PUT'
            ]);
            break;
          case 2: //Solicitud Municipal
            if ($solicitud_regional) {
              $solicitud_regional->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_regiones',
                'registro' => $solicitud_regional->id . '',
                'campos' => json_encode($solicitud_regional) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_institucional) {
              $solicitud_institucional->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_dependencias',
                'registro' => $solicitud_institucional->id . '',
                'campos' => json_encode($solicitud_institucional) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_personal) {
              $solicitud_personal->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_personales',
                'registro' => $solicitud_personal->id . '',
                'campos' => json_encode($solicitud_personal) . '',
                'metodo' => 'DELETE'
              ]);
            }
            $solicitud_municipal = SolicitudesMunicipales::updateOrCreate([
              'solicitud_id' => $solicitud->id
            ], [
              'cargo_id' => Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo'))])->id,
              'municipio_id' => $request->input('solicitud_municipio_id'),
              'persona_id' => $request->input('solicitante_id')
            ]);
            auth()->user()->bitacora(request(), [
              'tabla' => 'solicitudes_municipios',
              'registro' => $solicitud_municipal->id . '',
              'campos' => json_encode($solicitud_municipal) . '',
              'metodo' => 'PUT'
            ]);
            break;
          case 3: //Solicitud Institucional
            if ($solicitud_municipal) {
              $solicitud_municipal->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_municipios',
                'registro' => $solicitud_municipal->id . '',
                'campos' => json_encode($solicitud_municipal) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_regional) {
              $solicitud_regional->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_regiones',
                'registro' => $solicitud_regional->id . '',
                'campos' => json_encode($solicitud_regional) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_personal) {
              $solicitud_personal->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_personales',
                'registro' => $solicitud_personal->id . '',
                'campos' => json_encode($solicitud_personal) . '',
                'metodo' => 'DELETE'
              ]);
            }
            $solicitud_institucional = SolicitudesDependencias::updateOrCreate([
              'solicitud_id' => $solicitud->id
            ], [
              'dependencia_id' => $request->input('dependencia_id')
            ]);
            auth()->user()->bitacora(request(), [
              'tabla' => 'solicitudes_dependencias',
              'registro' => $solicitud_institucional->id . '',
              'campos' => json_encode($solicitud_institucional) . '',
              'metodo' => 'PUT'
            ]);
            break;
          case 4: //Solicitud Personal
            if ($solicitud_municipal) {
              $solicitud_municipal->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_municipios',
                'registro' => $solicitud_municipal->id . '',
                'campos' => json_encode($solicitud_municipal) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_regional) {
              $solicitud_regional->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_regiones',
                'registro' => $solicitud_regional->id . '',
                'campos' => json_encode($solicitud_regional) . '',
                'metodo' => 'DELETE'
              ]);
            } else if ($solicitud_institucional) {
              $solicitud_institucional->delete();
              auth()->user()->bitacora(request(), [
                'tabla' => 'solicitudes_dependencias',
                'registro' => $solicitud_institucional->id . '',
                'campos' => json_encode($solicitud_institucional) . '',
                'metodo' => 'DELETE'
              ]);
            }
            $solicitud_personal = SolicitudesPersonales::updateOrCreate([
              'solicitud_id' => $solicitud->id
            ], [
              'persona_id' => $request->input('solicitante_id')
            ]);
            auth()->user()->bitacora(request(), [
              'tabla' => 'solicitudes_personales',
              'registro' => $solicitud_personal->id . '',
              'campos' => json_encode($solicitud_personal) . '',
              'metodo' => 'PUT'
            ]);
            break;
        }
        DB::commit();
        // Guardamos los datos en la BD y regresamos status 200 y el id de la solicitud creada
        return response()->json(['success' => array(['code' => 200, 'id' => $solicitud->id])], 200);
      } catch (Exeption $e) {
        DB::rollBack();
        return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
      }
    }
  }

  public function create(Request $request, PersonasDataTable $personas, BeneficiosDataTable $beneficios, SolicitantesDataTable $solicitantes, GirasDataTable $giras, DependenciasDataTable $dependencias, BeneficiosExternosDataTable $beneficiosExt)
  {
    $tipo_solicitud = ($request['tipo_solicitud'] && (strtolower($request['tipo_solicitud']) === 'i' || strtolower($request['tipo_solicitud']) === 'e')) ? strtoupper($request['tipo_solicitud']) : 'I';

    //Lista de motivos por los que se puede CANCELAR una SOLICITUD desde ATN CIUDADANA
    $motivos = DB::table('motivos_programas')
      ->select(['motivos_programas.id', 'cat_motivos.motivo'])
      ->where('programas.nombre', '=', 'ATENCIÓN CIUDADANA')
      ->join('cat_motivos', 'cat_motivos.id', '=', 'motivos_programas.motivo_id')
      ->join('programas', 'programas.id', '=', 'motivos_programas.programa_id')
      ->get();

    $recepciones = Tiposrecepcion::orderBy('tipo', 'asc')->get();
    $remitentes = Tiposremitente::all();
    $titulares = DB::table('titulares')
      ->join('personas', 'titulares.persona_id', '=', 'personas.id')
      ->selectRaw("personas.id, CONCAT(personas.nombre, ' ', personas.primer_apellido, ' ', personas.segundo_apellido) as nombre")->get();

    //La siguiente variable sirve para filtrar a los beneficiarios de una peticion
    //$beneficio;
    //Si existe el parámetro programa_id quiere decir que va a consultar a los solicitantes/beneficiarios de tal programa
    if (request()->get('programa_id')) {
      $beneficio = ProgramasSolicitud::where('solicitud_id', request()->get('solicitud_id'))
        ->where('programa_id', request()->get('programa_id'))
        ->firstOrFail();
    }

    //Si existe el parámetro tables y su valor es giras entonces se está manipulando el DataTables de Giras
    if (request()->get('table') == 'giras') {
      return $giras->with(['tipo' => 'modal', 'request' => $request])->render('atnciudadana::solicitudes.create');
    }

    //Si existe el parámetro tables y su valor es dependencias entonces se está manipulando el DataTables de Dependencias
    if (request()->get('table') == 'dependencias') {
      return $dependencias->with('tipo', 'modal')->render('atnciudadana::solicitudes.create');
    }

    //Ni idea solo lo copie por si jala lo de beneficios Externos
    if (request()->get('table') == 'beneficiosExt') {
      return $beneficiosExt->render('atnciudadana::solicitudes.create');
    }

    //Si existe el parámetro tables y su valor es beneficios entonces se está manipulando el DataTables de Beneficios (Peticiones o Programas) de una solicitud
    if (request()->get('table') == 'beneficios') {
      return $beneficios->with(['solicitud_id' => request()->get('solicitud_id'), 'tipo_solicitud' => $tipo_solicitud])->render('atnciudadana::solicitudes.create');
    }

    //Si existe el parámetro tables y su valor es solicitantes entonces se está manipulando el DataTables de Solicitantes o Beneficiarios
    if (request()->get('table') == 'solicitantes') {
      if (isset($beneficio)) {
        return $solicitantes->with([
          'estatus' => $beneficio->statusActual(),
          'programa_nombre' => $beneficio->programa->nombre,
          'programa_solicitantes' => SolicitudesPersona::where('programas_solicitud_id', $beneficio->id)->whereNull('observacionevaluado')->count(),
          'programa_cantidad' => $beneficio->cantidad, 'programas_solicitud_id' => $beneficio->id
        ])->render('atnciudadana::solicitudes.create');
      } else {
        return $solicitantes->with(['estatus' => 'SOLICITANTE'])->render('atnciudadana::solicitudes.create');
      }
    }

    //Las siguientes 4 líneas tienen que estar, si no, no funca nada jajaja no sé pero asi funca
    //Ahh no es cierto, es que los DataTable esperan esos valores como default
    $personas->with('tipo', 'afuncionalessolicitantes');
    $giras->with(['tipo' => 'modal', 'request' => $request]);
    $dependencias->with('tipo', 'modal');
    $beneficios->with('tipo_solicitud', $tipo_solicitud);

    //Regresamos la vista como tal
    return $personas->render(
      'atnciudadana::solicitudes.create',
      [
        'personas' => $personas,
        'beneficios' => $beneficios,
        'solicitantes' => $solicitantes,
        'giras' => $giras,
        'dependencias' => $dependencias,
        'beneficiosExt' => $beneficiosExt,
        'motivos' => $motivos,
        'recepciones' => $recepciones,
        'tipo_solicitud' => $tipo_solicitud,
        'titulares' => $titulares,
        'remitentes' => $remitentes,
        'tipo_solicitud' => $tipo_solicitud
      ]
    );
  }

  public function PeticionesDireccion($direccion_id = NULL)
  {
    //return $solicitud = Solicitud::with('solicitud.solicituddependencias','solicitud.solicitudpersonales','solicitud.solicitudregionales','solicitud.solicitudmunicipales','solicitud.tiposremitente','tiposrecepcion')->get();
    //return $datos = BeneficiosprogramasSolicitud::with('petcionespersonas.beneficiopersona.persona','solicitud.solicituddependencias.dependencia.municipio','solicitud.solicitudpersonales.persona.municipio','solicitud.solicitudregionales.persona.municipio','solicitud.solicitudmunicipales.municipio','solicitud.tiposremitente','solicitud.tiposrecepcion','beneficio.anioprograma.programa.lastPadre.AreasPrograma.area.areasresponsables.empleado.persona','estadosSolicitud.statusproceso:status,id','statusActual.statusproceso:status,id')->first();
    if (auth()->user()->hasRolesStrModulo(['ATNC-AREA'], 'atnciudadana')) {
      $area_id = auth()->user()->persona->empleado->area_id;
      $direccion_id = DB::select('call getPadreSiaf(?)', [$area_id])[0]->direccion_id;
    }

    $list = collect([]);
    $listD = collect([]);


    $datos = BeneficiosprogramasSolicitud::with('petcionespersonas.beneficiopersona.persona', 'solicitud.solicituddependencias.dependencia.municipio', 'solicitud.solicitudpersonales.persona.municipio', 'solicitud.solicitudregionales.persona.municipio', 'solicitud.solicitudmunicipales.persona', 'solicitud.tiposremitente', 'solicitud.tiposrecepcion', 'beneficio.anioprograma.programa.lastPadre.AreasPrograma.area.areasresponsables.empleado.persona', 'estadosSolicitud.statusproceso:status,id', 'statusActual.statusproceso:status,id')->get();
    foreach ($datos as $d) {

      foreach ($d->estadosSolicitud as $f) {
        if ($f->statusproceso->status == 'VINCULADO') {
          $fecha_turnado = $f->created_at;
          $vinculado = $f->statusproceso->status;
        }
      }

      if ($d->solicitud->solicituddependencias != NULL) {
        $solicitante = $d->solicitud->solicituddependencias->dependencia->nombre;
        $cve_municipio = $d->solicitud->solicituddependencias->dependencia->municipio->id;
        $municipio = $d->solicitud->solicituddependencias->dependencia->municipio->nombre;
        $cve_distrito = $d->solicitud->solicituddependencias->dependencia->municipio->distrito->id;
        $distrito = $d->solicitud->solicituddependencias->dependencia->municipio->distrito->nombre;
        $cve_region = $d->solicitud->solicituddependencias->dependencia->municipio->distrito->region->id;
        $region = $d->solicitud->solicituddependencias->dependencia->municipio->distrito->region->nombre;
        $cve_localidad = '00';
        $localidad = '...';
      }

      if ($d->solicitud->solicitudmunicipales != NULL) {
        $solicitante = $d->solicitud->solicitudmunicipales->persona->nombre . ' ' . $d->solicitud->solicitudmunicipales->persona->primer_apellido . ' ' . $d->solicitud->solicitudmunicipales->persona->segundo_apellido; //$d->solicitud->solicitudmunicipales->municipio->nombre;
        $cve_municipio = $d->solicitud->solicitudmunicipales->municipio->id;
        $municipio = $d->solicitud->solicitudmunicipales->municipio->nombre;
        $cve_distrito = $d->solicitud->solicitudmunicipales->municipio->distrito->id;
        $distrito = $d->solicitud->solicitudmunicipales->municipio->distrito->nombre;
        $cve_region = $d->solicitud->solicitudmunicipales->municipio->distrito->region->id;
        $region = $d->solicitud->solicitudmunicipales->municipio->distrito->region->nombre;
        $cve_localidad = '00';
        $localidad = '...';
      }

      if ($d->solicitud->solicitudregionales != NULL) {
        $solicitante = $d->solicitud->solicitudregionales->persona->nombre . ' ' . $d->solicitud->solicitudregionales->persona->primer_apellido . ' ' . $d->solicitud->solicitudregionales->persona->segundo_apellido;
        //echo $d->solicitud->solicitudregionales->persona;
        $cve_municipio = $d->solicitud->solicitudregionales->persona->municipio->id;
        $municipio = $d->solicitud->solicitudregionales->persona->municipio->nombre;
        $cve_distrito = $d->solicitud->solicitudregionales->persona->municipio->distrito->id;
        $distrito = $d->solicitud->solicitudregionales->persona->municipio->distrito->nombre;
        $cve_region = $d->solicitud->solicitudregionales->persona->municipio->distrito->id;
        $region = $d->solicitud->solicitudregionales->persona->municipio->distrito->nombre;
        if ($cve_localidad = $d->solicitud->solicitudregionales->persona->localidad != NULL) {
          $cve_localidad = $d->solicitud->solicitudregionales->persona->localidad->id;
          $localidad = $d->solicitud->solicitudregionales->persona->localidad->nombre;
        } else {
          $cve_localidad = '00';
          $localidad = '...';
        }
      }

      if ($d->solicitud->solicitudpersonales != NULL) {
        $solicitante = $d->solicitud->solicitudpersonales->persona->nombre . ' ' . $d->solicitud->solicitudpersonales->persona->primer_apellido . ' ' . $d->solicitud->solicitudpersonales->persona->segundo_apellido;
        $cve_municipio = $d->solicitud->solicitudpersonales->persona->municipio->id;
        $municipio = $d->solicitud->solicitudpersonales->persona->municipio->nombre;
        $cve_distrito = $d->solicitud->solicitudpersonales->persona->municipio->distrito->id;
        $distrito = $d->solicitud->solicitudpersonales->persona->municipio->distrito->nombre;
        $cve_region = $d->solicitud->solicitudpersonales->persona->municipio->distrito->id;
        $region = $d->solicitud->solicitudpersonales->persona->municipio->distrito->nombre;
        if ($cve_localidad = $d->solicitud->solicitudpersonales->persona->localidad != NULL) {
          $cve_localidad = $d->solicitud->solicitudpersonales->persona->localidad->id;
          $localidad = $d->solicitud->solicitudpersonales->persona->localidad->nombre;
        } else {
          $cve_localidad = '00';
          $localidad = '...';
        }
      }

      if ($d->statusActual->statusproceso->status == "SOLICITANTE" || $d->statusActual->statusproceso->status == "PRE-SOLICITUD") {
        $statusG = "NUEVO";
      }

      if ($d->statusActual->statusproceso->status == "VINCULADO") {
        $statusG = "VINCULADO";
      }

      if ($d->statusActual->statusproceso->status == "FINALIZADO" || $d->statusActual->statusproceso->status == "RECHAZADO" || $d->statusActual->statusproceso->status == "CANCELADO") {
        $statusG = "CONCLUIDO";
      }

      if ($d->statusActual->statusproceso->status == "Vo. Bo." || $d->statusActual->statusproceso->status == "VALIDANDO" || $d->statusActual->statusproceso->status == "LISTA DE ESPERA") {
        $statusG = "PROCESO";
      }

      //return $d;
      //return $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->id;

      //echo $d->beneficio->anioprograma->programa;
      //echo $d->solicitud;

      if ($d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->id == $direccion_id) {

        $listD->prepend([
          'folio' => $d->folio,
          'fecha_turnado' => $fecha_turnado,
          'tipo' => $d->solicitud->tiposremitente->tipo,
          'municipio' => $municipio,
          'cve_municipio' => $cve_municipio,
          'cve_distrito' => $cve_distrito,
          'distrito' => $distrito,
          'cve_region' => $cve_region,
          'region' => $region,
          'cve_localidad' => $cve_localidad,
          'localidad' => $localidad,
          'solicitante' =>  $solicitante,
          'producto' => $d->beneficio->nombre,
          'cantidad' => $d->cantidad,
          'estatus' => $d->statusActual->statusproceso->status,
          'estatusG' => $statusG,
          'departamento' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->nombre : 'SIN DEPARTAMENTO CAPTURADO',
          'jefe' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN NOMBRE CAPTURADO',
          'direccion' => $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre : 'SIN DIRECCION',
          'director' =>  $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN DIRECTOR'
        ]);
      } else {
        $list->prepend([
          'beneficiario' => isset($d->petcionespersonas[0]) ? $d->petcionespersonas[0]->beneficiopersona->persona->nombre . ' ' . $d->petcionespersonas[0]->beneficiopersona->persona->primer_apellido . ' ' . $d->petcionespersonas[0]->beneficiopersona->persona->segundo_apellido : "SIN BENEFICIARIO",
          'curp' => isset($d->petcionespersonas[0]) ? $d->petcionespersonas[0]->beneficiopersona->persona->curp : "SIN CURP",
          'folio' => $d->folio,
          'fecha_turnado' => $fecha_turnado,
          'tipo' => $d->solicitud->tiposremitente->tipo,
          'municipio' => $municipio,
          'cve_municipio' => $cve_municipio,
          'cve_distrito' => $cve_distrito,
          'distrito' => $distrito,
          'cve_region' => $cve_region,
          'region' => $region,
          'cve_localidad' => $cve_localidad,
          'localidad' => $localidad,
          'solicitante' =>  $solicitante,
          'producto' => $d->beneficio->nombre,
          'cantidad' => $d->cantidad,
          'estatus' => $d->statusActual->statusproceso->status,
          'estatusG' => $statusG,
          'departamento' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->nombre : 'SIN DEPARTAMENTO CAPTURADO',
          'direccion' => $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre : 'SIN DIRECCION',
          'director' =>  $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN DIRECTOR'

        ]);
      }
    }
    if ($direccion_id == NULL) {
      $response = new JsonResponse(['peticiones' => $list, 'message' => "Se generó correctamente la información"], 200);
    } else {
      $response = new JsonResponse(['peticiones' => $listD, 'message' => "Se generó correctamente la información"], 200);
    }

    return $response;
  }

  public function peticionesAgrupadas($direccion_id = NULL)
  {
    //return $datos = BeneficiosprogramasSolicitud::with('solicitud.solicituddependencias.dependencia.municipio','solicitud.solicitudpersonales.persona.municipio','solicitud.solicitudregionales.persona.municipio','solicitud.solicitudmunicipales.municipio','solicitud.tiposremitente','solicitud.tiposrecepcion','beneficio.anioprograma.programa.lastPadre.AreasPrograma.area.areasresponsables.empleado.persona','estadosSolicitud.statusproceso:status,id','statusActual.statusproceso:status,id')->first();
    //return $datos = Area::with('hijas','hijas.programas.programa.aniosprogramas.beneficioprograma','programas.programa.aniosprogramas.beneficioprograma')->where('id',$area_id)->get();

    if (auth()->user()->hasRolesStrModulo(['ATNC-AREA'], 'atnciudadana')) {
      $area_id = auth()->user()->persona->empleado->area_id;
      $direccion_id = DB::select('call getPadreSiaf(?)', [$area_id])[0]->direccion_id;
    }
    $list = collect([]);
    $listD = collect([]);

    $datos = BeneficiosprogramasSolicitud::with(
      'solicitud.solicituddependencias.dependencia.municipio',
      'solicitud.solicitudpersonales.persona.municipio',
      'solicitud.solicitudregionales.persona.municipio',
      'solicitud.solicitudmunicipales.persona',
      'solicitud.tiposremitente',
      'solicitud.tiposrecepcion',
      'beneficio.anioprograma.programa.lastPadre.AreasPrograma.area.areasresponsables.empleado.persona',
      'estadosSolicitud.statusproceso:status,id',
      'statusActual.statusproceso:status,id'
    )->orderBy('solicitud_id', 'DESC')->orderBy('folio', 'DESC')->get();

    foreach ($datos as $d) {
      $fecha_turnado = null;
      $statusG = null;
      foreach ($d->estadosSolicitud as $f) {
        if ($f->statusproceso->status == 'VINCULADO') {
          $fecha_turnado = $f->created_at;
          $vinculado = $f->statusproceso->status;
        }
      }

      if ($d->solicitud->solicituddependencias != NULL) {
        $solicitante = $d->solicitud->solicituddependencias->dependencia->nombre;
        $municipio = $d->solicitud->solicituddependencias->dependencia->municipio->nombre;
      }

      if ($d->solicitud->solicitudmunicipales != NULL) {
        $solicitante = $d->solicitud->solicitudmunicipales->persona->nombre . ' ' . $d->solicitud->solicitudmunicipales->persona->primer_apellido . ' ' . $d->solicitud->solicitudmunicipales->persona->segundo_apellido; //$d->solicitud->solicitudmunicipales->municipio->nombre;
        $municipio = $d->solicitud->solicitudmunicipales->municipio->nombre;
      }

      if ($d->solicitud->solicitudregionales != NULL) {
        $solicitante = $d->solicitud->solicitudregionales->persona->nombre . ' ' . $d->solicitud->solicitudregionales->persona->primer_apellido . ' ' . $d->solicitud->solicitudregionales->persona->segundo_apellido;;
        $municipio = $d->solicitud->solicitudregionales->persona->municipio->nombre;
      }

      if ($d->solicitud->solicitudpersonales != NULL) {
        $solicitante = $d->solicitud->solicitudpersonales->persona->nombre . ' ' . $d->solicitud->solicitudpersonales->persona->primer_apellido . ' ' . $d->solicitud->solicitudpersonales->persona->segundo_apellido;
        $municipio = $d->solicitud->solicitudpersonales->persona->municipio->nombre;
      }

      if ($d->statusActual->statusproceso->status == "SOLICITANTE" || $d->statusActual->statusproceso->status == "PRE-SOLICITUD") {
        $statusG = "NUEVO";
      }

      if ($d->statusActual->statusproceso->status == "VINCULADO") {
        $statusG = "VINCULADO";
      }

      if ($d->statusActual->statusproceso->status == "FINALIZADO" || $d->statusActual->statusproceso->status == "RECHAZADO" || $d->statusActual->statusproceso->status == "CANCELADO") {
        $statusG = "CONCLUIDO";
      }

      if ($d->statusActual->statusproceso->status == "Vo. Bo." || $d->statusActual->statusproceso->status == "VALIDANDO" || $d->statusActual->statusproceso->status == "LISTA DE ESPERA") {
        $statusG = "PROCESO";
      }


      if (auth()->user()->hasRolesStrModulo(['ATNC-AREA'], 'atnciudadana') && $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->id == $direccion_id) {
        $listD->prepend([
          'folio' => $d->folio,
          'fecha_turnado' => $fecha_turnado,
          'tipo' => $d->solicitud->tiposremitente->tipo,
          'municipio' => $municipio,
          'solicitante' =>  $solicitante,
          'programa' => $d->beneficio->anioprograma->programa->lastPadre->padre->nombre,
          'producto' => $d->beneficio->nombre,
          'cantidad' => $d->cantidad,
          'estatus' => $d->statusActual->statusproceso->status,
          'estatusG' => $statusG,
          'departamento' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->nombre : $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre,
          'jefe' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN NOMBRE CAPTURADO',
          'direccion' => $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre : 'SIN DIRECCION',
          'director' =>  $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN DIRECTOR'
        ]);
      } else if ($d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->id == $direccion_id && $fecha_turnado != NULL) {
        $listD->prepend([
          'folio' => $d->folio,
          'fecha_turnado' => $fecha_turnado,
          'tipo' => $d->solicitud->tiposremitente->tipo,
          'municipio' => $municipio,
          'solicitante' =>  $solicitante,
          'programa' => $d->beneficio->anioprograma->programa->lastPadre->padre->nombre,
          'producto' => $d->beneficio->nombre,
          'cantidad' => $d->cantidad,
          'estatus' => $d->statusActual->statusproceso->status,
          'estatusG' => $statusG,
          'departamento' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->nombre : $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre,
          'jefe' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN NOMBRE CAPTURADO',
          'direccion' => $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre : 'SIN DIRECCION',
          'director' =>  $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN DIRECTOR'
        ]);
      } else {
        $list->prepend([
          'folio' => $d->folio,
          'fecha_turnado' => $fecha_turnado,
          'tipo' => $d->solicitud->tiposremitente->tipo,
          'municipio' => $municipio,
          'solicitante' =>  $solicitante,
          'programa' => $d->beneficio->anioprograma->programa->lastPadre->padre->nombre,
          'producto' => $d->beneficio->nombre,
          'cantidad' => $d->cantidad,
          'estatus' => $d->statusActual->statusproceso->status,
          'estatusG' => $statusG,
          'departamento' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->nombre : $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre,
          'jefe' => isset($d->beneficio->anioprograma->programa->lastPadre->areasPrograma) ? $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN NOMBRE CAPTURADO',
          'direccion' => $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->nombre : 'SIN DIRECCION',
          'director' =>  $d->beneficio->anioprograma->programa->lastPadre ? $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->nombre . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->primer_apellido . ' ' . $d->beneficio->anioprograma->programa->lastPadre->padre->areasPrograma->area->areasresponsables->empleado->persona->segundo_apellido : 'SIN DIRECTOR'

        ]);
      }
    }

    if ($direccion_id != NULL) {
      $direcciones = array();
      foreach ($listD as $value) {
        $direcciones[$value['direccion']][] = $value;
      }
      if (auth()->user()->hasRolesStrModulo(['ATNC-AREA'], 'atnciudadana')) {
        $programas = array_values($direcciones);
        if (sizeof($programas) > 0) {
          unset($direcciones);
          $programas = $programas[0];
          foreach ($programas as $value) {
            $direcciones[$value['programa']][] = $value;
          }
        }
      }
    } else {
      $direcciones = array();
      foreach ($list as $value) {
        $direcciones[$value['direccion']][] = $value;
      }
    }

    return $direcciones;
  }

  public function direcciones()
  {
    return view('atnciudadana::solicitudes.direccion');
  }

  public function peticionesPersona()
  {
    return view('atnciudadana::solicitudes.peticionespersona');
  }

  public function getDirecciones(Request $request)
  {
    $direcciones = Area::where('tipoarea_id', 1)->orWhere('nombre', 'COORDINACIÓN DE CAPACITACIÓN')->orWhere('nombre', 'PROCURADURÍA ESTATAL DE PROTECCIÓN DE LOS DERECHOS DE LAS NIÑAS, NIÑOS Y ADOLESCENTES DEL ESTADO DE OAXACA')->where('nombre', 'like', '%' . $request->term . '%')->get();
    $respuesta = collect([]);
    foreach ($direcciones as $direccion) {
      $respuesta->prepend([
        'id' => $direccion->id,
        'nombre' => $direccion->nombre
      ]);
    }
    return $respuesta;
  }

  public function getDireccion($direccion_id = null)
  {
    $direccion = Area::where('id', $direccion_id)->with('areasresponsables.empleado.persona')->first();
    $respuesta = [
      'id' => $direccion->id,
      'nombre' => $direccion->nombre,
      'responsable' => $direccion->areasresponsables->empleado->persona->nombreCompleto(),
      'genero' => $direccion->areasresponsables->empleado->persona->genero
    ];
    return $respuesta;
  }

  public function getPeticionesPersona($persona_nombre = NULL)
  {
    $peticiones = $this->PeticionesDireccion();
  }

  public function getProgramasDireccion()
  {
    $area_id = auth()->user()->persona->empleado->area_id;
    $direccion_id = DB::select('call getPadreSiaf(?)', [$area_id])[0]->direccion_id;
    $programas = AreasPrograma::join('programas', 'areas_programas.programa_id', 'programas.id')->where('areas_programas.area_id', $direccion_id)->where('programas.tipo', 'PROGRAMA')->select('programas.nombre')->distinct()->get();
    return $programas;
  }
}