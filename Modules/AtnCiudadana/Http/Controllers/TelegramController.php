<?php


namespace Modules\AtnCiudadana\Http\Controllers;


use App\Models\BeneficiosprogramasSolicitud;
use App\Models\Area;
use Illuminate\Routing\Controller;

use Carbon\Carbon;
use Exception;

class TelegramController extends Controller
{
  public function DatosGeneralesDireccionGeneral()
  {
    $solicitudes_beneficios = BeneficiosprogramasSolicitud::with(
      'statusActual.statusproceso',
      'beneficio.anioprograma.programa.lastPadre.AreasPrograma.area'
    )->get();

    $resultados_direccion_general = collect();
    $total_nuevos_proceso_finalizados = collect();
    $direcciones = collect();
    $resultados_estadisticos = collect();

    $direcciones_lista = [2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15];

    $nuevos_general = $solicitudes_beneficios->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "VINCULADO" ? 1 : 0);
    }, 0);

    $proceso_general = $solicitudes_beneficios->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $concluidos_general = $solicitudes_beneficios->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);


    $solicitudes_ayer = $solicitudes_beneficios->filter(function ($value) {
      return Carbon::parse($value->estadosSolicitud->last()->created_at)
        ->lessThan(Carbon::yesterday());
    });

    $solicitudes_cinco_dias = $solicitudes_beneficios->filter(function ($value) {
      return Carbon::parse($value->estadosSolicitud->last()->created_at)
        ->lessThan(Carbon::now()->subDays(5));
    });


    foreach ($direcciones_lista as $value) {
      $area = Area::find( $value );

      $resultados_por_area = collect();
      $resultados_numericos = collect();

      $solicitudes_una_direccion = $solicitudes_beneficios->filter(function ($solicitud) use ($area) {
        return $solicitud->beneficio->anioprograma->programa
          ->lastPadre->padre->areasPrograma->area->nombre == $area->nombre;
      });

      $nuevos_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "VINCULADO" ? 1 : 0);
      }, 0);

      $proceso_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
          || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
          || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
      }, 0);

      $concluidos_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
          || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
          || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
      }, 0);

      $resultados_numericos->put("nuevos", $nuevos_por_area);
      $resultados_numericos->put("proceso", $proceso_por_area);
      $resultados_numericos->put("concluidos", $concluidos_por_area);
      $resultados_numericos->put("total", $nuevos_por_area + $proceso_por_area + $concluidos_por_area);

      $resultados_por_area->put("nombre_direccion", $area->nombre);
      $resultados_por_area->put("nombre_direccion_siglas", $area->siglas);
      $resultados_por_area->put("totales", $resultados_numericos);
      $direcciones->push($resultados_por_area);
    }


    $finalizados_cinco_dias = $solicitudes_cinco_dias->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);

    $proceso_cinco_dias = $solicitudes_cinco_dias->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $finalizados_ayer = $solicitudes_ayer->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);

    $proceso_ayer = $solicitudes_ayer->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $total_nuevos_proceso_finalizados->put("nuevos", $nuevos_general);
    $total_nuevos_proceso_finalizados->put("proceso", $proceso_general);
    $total_nuevos_proceso_finalizados->put("concluidos", $concluidos_general);
    $total_nuevos_proceso_finalizados->put("total", $nuevos_general + $proceso_general + $concluidos_general);

    $resultados_estadisticos->put("proceso", $proceso_ayer);
    $resultados_estadisticos->put("finalizados", $finalizados_ayer);

    $resultados_estadisticos->put("inicio_tasa_semanal_concluidos", $finalizados_cinco_dias);
    $resultados_estadisticos->put("inicio_tasa_semanal_proceso", $proceso_cinco_dias);

    $resultados_direccion_general->put("nombre_direccion", "DIRECCÓN GENERAL");
    $resultados_direccion_general->put("totales", $total_nuevos_proceso_finalizados);
    $resultados_direccion_general->put("direcciones", $direcciones);
    $resultados_direccion_general->put("resultados_estadisticos", $resultados_estadisticos);

    return $resultados_direccion_general;
  }

  public function DatosGeneralesPorArea(Area $area)
  {
    $solicitudes_beneficios = BeneficiosprogramasSolicitud::with(
      'statusActual.statusproceso',
      'beneficio.anioprograma.programa.lastPadre.AreasPrograma.area'
    )->get();

    $programas_por_area = Area::with(
      'programas.programa'
    )->where('id', $area->id)->get()
      ->pluck('programas')
      ->first()
      ->filter(function ($value) {
        return $value->programa->tipo == "PROGRAMA" && $value->programa->oficial == 1;
      })->map(function ($item) {
        return $item->programa->nombre;
      });

    $resultados_por_area = collect();
    $resultados_numericos = collect();
    $resultados_detalle_programas = collect();
    $resultados_total_programas = collect();
    $resultados_numericos_ayer = collect();

    $solicitudes_una_direccion = $solicitudes_beneficios->filter(function ($solicitud) use ($area) {
      return $solicitud->beneficio->anioprograma->programa
        ->lastPadre->padre->areasPrograma->area->nombre == $area->nombre;
    });


    $nuevos_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "VINCULADO" ? 1 : 0);
    }, 0);

    $proceso_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $concluidos_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);


    foreach ($programas_por_area as $programa) {

      $resultados_numericos_por_programa = collect();
      $solicitudes_de_un_programa = $solicitudes_una_direccion
        ->filter(function ($value) use ($programa) {
          return $value->beneficio->anioprograma->programa
            ->lastPadre->padre->nombre == $programa;
        });

      $nuevos_por_programa = $solicitudes_de_un_programa->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "VINCULADO" ? 1 : 0);
      }, 0);

      $concluidos_por_programa = $solicitudes_de_un_programa->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
          || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
          || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
      }, 0);

      $proceso_por_programa = $solicitudes_de_un_programa->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
          || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
          || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
      }, 0);

      $resultados_numericos_por_programa->put("nuevos", $nuevos_por_programa);
      $resultados_numericos_por_programa->put("proceso", $proceso_por_programa);
      $resultados_numericos_por_programa->put("concluidos", $concluidos_por_programa);

      $resultados_detalle_programas->put($programa, $resultados_numericos_por_programa);
      $resultados_total_programas->put(
        $programa,
        $nuevos_por_programa +
          $concluidos_por_programa +
          $proceso_por_programa

      );
    }

    $solicitudes_ayer = $solicitudes_una_direccion->filter(function ($value) {
      return Carbon::parse($value->estadosSolicitud->last()->created_at)
        ->lessThan(Carbon::yesterday());
    });

    $solicitudes_cinco_dias = $solicitudes_una_direccion->filter(function ($value) {
      return Carbon::parse($value->estadosSolicitud->last()->created_at)
        ->lessThan(Carbon::now()->subDays(5));
    });

    $finalizados_cinco_dias = $solicitudes_cinco_dias->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);

    $proceso_cinco_dias = $solicitudes_cinco_dias->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $finalizados_ayer = $solicitudes_ayer->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);

    $proceso_ayer = $solicitudes_ayer->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $resultados_numericos->put("nuevos", $nuevos_por_area);
    $resultados_numericos->put("proceso", $proceso_por_area);
    $resultados_numericos->put("concluidos", $concluidos_por_area);
    $resultados_numericos->put("total", $nuevos_por_area + $proceso_por_area + $concluidos_por_area);

    $resultados_numericos_ayer->put("proceso", $proceso_ayer);
    $resultados_numericos_ayer->put("finalizados", $finalizados_ayer);
    $resultados_numericos_ayer->put("inicio_tasa_semanal_concluidos", $finalizados_cinco_dias);
    $resultados_numericos_ayer->put("inicio_tasa_semanal_proceso", $proceso_cinco_dias);

    $resultados_por_area->put("nombre_direccion", $area->nombre);
    $resultados_por_area->put("totales", $resultados_numericos);
    $resultados_por_area->put("detalles_programas", $resultados_detalle_programas);
    $resultados_por_area->put("total_programas", $resultados_total_programas);
    $resultados_por_area->put("resultados_estadisticos", $resultados_numericos_ayer);

    return $resultados_por_area;
  }
}
