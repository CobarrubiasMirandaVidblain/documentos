<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\DataTables\DependenciasDataTable;

use App\Models\Persona;
use App\Models\Beneficiosprograma;
use App\Models\Solicitud;
use App\Models\EventosSolicitudes;
use App\Models\SolicitudesPersonales;
use App\Models\BeneficiosprogramasSolicitud;
use App\Models\PeticionesPersonas;
use App\Models\BeneficiosPersonas;
use App\Models\EstadosSolicitud;
use App\Models\Area;
use Modules\AppPreregistro\Entities\GiraPreregistro;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AtnCiudadanaController extends Controller {

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('authorized');
        $this->middleware('roles:SUPERADMIN,ADMINISTRADOR', ['except' => 'edit']);
        $this->middleware('saveaction', ['except' => ['index', 'create', 'show', 'edit']]);
    }

    public function dependencias(DependenciasDataTable $dataTable) {
      return $dataTable->render('atnciudadana::dependencias.index');
    }

    public function crearSolicitudInterna(Request $request) {
        if($request->ajax()) {
            // dd(auth()->user());
            // dd(Auth::user());
            try { 
                $beneficio = Beneficiosprograma::where('id',$request->beneficio_id)->first();
                // dd($beneficio);
                if($beneficio && $request->curp){
                    // return response()->json(['existeBeneficio' => true], 200);
                    $persona = Persona::where('curp',$request->curp)->first();
                    if(!$persona) {
                        $datos_persona = $request->except(['beneficio_id','beneficios','created_at','deleted_at','evento_id','folio','fotografia','id','updated_at','usuario','telefono']);
                        $datos_persona['numero_celular'] = $request->telefono;
                        $persona = Persona::Create($datos_persona);
                    }

                    DB::beginTransaction();

                    //Persona es el solicitante y el beneficiario
                    $datos_solicitud = [];
                    $datos_solicitud['persona_id'] = 37124; //HOLM
                    $datos_solicitud['compromiso'] = false;
                    $datos_solicitud['tiposrecepcion_id'] = 1; // GIRA o EVENTO
                    $datos_solicitud['tiposremitente_id'] = 4; // PERSONAL
                    $datos_solicitud['numoficio'] = 'X';
                    $datos_solicitud['fechaoficio'] = $request->created_at ;
                    $datos_solicitud['fecharecepcion'] = $request->created_at;
                    $datos_solicitud['asunto'] = 'SOLICITUD DE ' . $beneficio->nombre . ' EN EVENTO.';
                    $datos_solicitud['folio'] = "I/" . (Solicitud::where('folio','LIKE',$request->input('tipo').'%')->max('id') + 1) . "/" . date("Y");
                    $datos_solicitud['usuario_id'] = auth()->user()->id;

                    //Creamos la solicitud con los datos
                    $solicitud = Solicitud::create($datos_solicitud);

                    //La creamos como solicitud de evento
                    EventosSolicitudes::create([
                        'evento_id' => $request->input('evento_id'),
                        'solicitud_id' => $solicitud->id,
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    //La creamos como solicitud personal
                    $tipo_solicitud = SolicitudesPersonales::create([
                        'solicitud_id' => $solicitud->id,
                        'persona_id' => $persona->id
                    ]);

                    $postfolio = BeneficiosprogramasSolicitud::where([["beneficioprograma_id", $request['beneficio_id']],["solicitud_id", $solicitud->id]])->count()+1;

                    //Se debe crear un registro BeneficiosprogramasSolicitud
                    $beneficio_solicitud = BeneficiosprogramasSolicitud::create([
                        'solicitud_id' => $solicitud->id,
                        'beneficioprograma_id' => $request['beneficio_id'],
                        'cantidad' => 1,
                        'folio'=> $solicitud->folio.'/'.$request['beneficio_id'].'-'.$postfolio,
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    //Se le asigna el status de SOLICITANTE
                    $status_solicitud = EstadosSolicitud::create([
                        'beneficioprograma_solicitud_id' => $beneficio_solicitud->id,
                        'statusproceso_id' => 2, //Solicitante
                        'usuario_id' => auth()->user()->id
                    ]);

                    //Creamos el Beneficio Persona
                    $beneficiopersona = BeneficiosPersonas::create([
                        'persona_id' => $persona->id,
                        'beneficioprograma_id' => $request['beneficio_id'],
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    $peticion_persona = PeticionesPersonas::create([
                        'beneficiosprogramas_solicitud_id' => $beneficio_solicitud->id,
                        'beneficiopersona_id' => $beneficiopersona->id,
                        'entregado' => false,   
                        'evaluado' => false, 
                        'usuario_id' =>  auth()->user()->id
                    ]);

                    GiraPreregistro::find($request->input('id'))->delete();
                    
                    DB::commit();
                    // Guardamos los datos en la BD y regresamos status 200 y la solicitud
                    return response()->json(['success' => array(['code' => 200, 'solicitud' => $solicitud])], 200);
                } else {
                    return response()->json(['mensaje' => 'Faltan Datos'], 409);
                }
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        } else {
            abort(403, 'Acción no autorizada.');
        }
    }
}
