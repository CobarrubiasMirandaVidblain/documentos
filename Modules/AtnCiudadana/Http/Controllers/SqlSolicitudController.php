<?php

namespace Modules\AtnCiudadana\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Modules\AtnCiudadana\Entities\SqlSolicitudes;
use Modules\AtnCiudadana\DataTables\SqlSolicitudDataTable;

class SqlSolicitudController extends Controller {
  /**
  * @return Response
  */
  public function index(SqlSolicitudDataTable $dataTable) {
    return $dataTable->render('atnciudadana::solicitudes.peticiones_anterior', compact('dataTable'));
  }
}
