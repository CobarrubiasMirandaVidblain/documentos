<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AtnCiudadana\DataTables\PreSolicitudesDataTable;

class PreSolicitudesController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(PreSolicitudesDataTable $dataTable) {
        return $dataTable->render('atnciudadana::preSolicitudes.index', compact('dataTable'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('atnciudadana::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('atnciudadana::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('atnciudadana::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}