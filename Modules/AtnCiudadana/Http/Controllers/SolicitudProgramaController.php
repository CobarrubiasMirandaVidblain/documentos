<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Solicitud;
use App\Models\SolicitudExterna;
use App\Models\BeneficiosprogramasSolicitud;
use App\Models\PeticionesPersonas;
use App\Models\EstadosSolicitud;

class SolicitudProgramaController extends Controller {
    
    public function index() {
        return view('atnciudadana::index');
    }

    public function store($solicitud_id, Request $request) {
        if($request->ajax()) {
            try {       
                $solicitud = Solicitud::findOrFail($solicitud_id);

                //Comprobando que el programa no se repita
                /* if( ProgramasSolicitud::where("programa_id", $request['programa_id'])->where("solicitud_id", $solicitud_id)->count() > 0 ){
                    return response()->json(['errors' => array(['code' => 422, 'message' => 'El beneficio ya se encuentra registrado.'])], 422);
                } */
                $postfolio = BeneficiosprogramasSolicitud::where([["beneficioprograma_id", $request['programa_id']],["solicitud_id", $solicitud_id]])->count()+1;
                DB::beginTransaction();
                    //Se debe crear un registro programa_solicitud
                    $programa_solicitud = BeneficiosprogramasSolicitud::create([
                        'solicitud_id' => $solicitud_id,
                        'beneficioprograma_id' => $request['programa_id'],
                        'cantidad' => $request['cantidad'] == "X" ? 0 : $request['cantidad'],
                        'folio'=> $solicitud->folio.'/'.$request['programa_id'].'-'.$postfolio,
                        'usuario_id' => Auth::user()->id
                    ]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'programas_solicitudes',
                        'registro' => $programa_solicitud->id . '',
                        'campos' => json_encode($programa_solicitud) . '',
                        'metodo' => 'POST'
                    ]);

                    if($request->has('dependencia_externa')){
                        SolicitudExterna::create([
                            'solicitud_id' => $solicitud_id,
                            'dependencia_id' => $request['dependencia_externa'],
                            'usuario_id' => Auth::user()->id
                        ]);
                    }

                    //Se le asigna el status de SOLICITANTE
                    $status_solicitud = EstadosSolicitud::create([
                        'beneficioprograma_solicitud_id' => $programa_solicitud->id,
                        'statusproceso_id' => 2, //Solicitante
                        'usuario_id' => Auth::user()->id
                    ]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'status_solicitudes',
                        'registro' => $status_solicitud->id . '',
                        'campos' => json_encode($status_solicitud) . '',
                        'metodo' => 'POST'
                    ]);
                
                DB::commit();
                // Guardamos los datos en la BD y regresamos status 200
                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('atnciudadana::show');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($solicitud_id, $programa_id, Request $request) {
        if($request->ajax()) {
            try {       
                $beneficio = BeneficiosprogramasSolicitud::where([
                    ['solicitud_id',$solicitud_id],
                    ['beneficioprograma_id', $programa_id],
                    ['folio', $request->input('folio')]
                    ])->firstOrFail();

                //Comprobando que la cantidad solicitada sea mayor al numero de beneficiarios
                if( $request['cantidad'] < PeticionesPersonas::where("beneficiosprogramas_solicitud_id",$beneficio->id)->whereNull('observacion_evaluado')->count() ){
                    return response()->json(['errors' => array(['code' => 422, 'message' => 'La cantidad solicitada es menor que el número de beneficiarios registrados.'])], 422);
                }

                DB::beginTransaction();                    
                $beneficio->cantidad = $request['cantidad'] == "X" ? 0 : $request['cantidad'];
                $beneficio->usuario_id = Auth::user()->id;
                $beneficio->save();
                
                auth()->user()->bitacora(request(), [
                    'tabla' => 'programas_solicitudes',
                    'registro' => $beneficio->id . '',
                    'campos' => json_encode($beneficio) . '',
                    'metodo' => 'PUT'
                ]);

                /*if($request->has('dependencia_id')){
                    SolicitudExterna::updateOrCreate([
                        'programas_solicitud_id' => $programa_solicitud->id
                    ],[
                        'dependencia_id' => $request['dependencia_id']
                    ]);
                }*/

                if($request->has('dependencia_externa')){
                    SolicitudExterna::updateOrCreate([
                        'solicitud_id' => $solicitud_id,
                    ],['dependencia_id' => $request['dependencia_externa']]);
                }


                DB::commit();
                // Guardamos los datos en la BD y regresamos status 200
                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($solicitud_id, $programa_id, Request $request) {
        if($request->ajax()) {
            try {       
                $beneficio = BeneficiosprogramasSolicitud::where([
                    ['solicitud_id',$solicitud_id],
                    ['beneficioprograma_id', $programa_id],
                    ['folio', $request->input('folio')]
                    ])->firstOrFail();
                    
                DB::beginTransaction();
                //Se le asigna el status de CANCELADO
                // $status_solicitud = StatusSolicitud::create([
                //     'programas_solicitud_id' => $beneficio->id,
                //     'statusproceso_id' => 7, //Cancelado
                //     'usuario_id' => Auth::user()->id
                // ]);
                $status_solicitud = EstadosSolicitud::create([
                    'beneficioprograma_solicitud_id' => $beneficio->id,
                    'statusproceso_id' => 7, //Cancelado
                    'usuario_id' => Auth::user()->id
                ]);


                
                auth()->user()->bitacora(request(), [
                    'tabla' => 'status_solicitudes',
                    'registro' => $status_solicitud->id . '',
                    'campos' => json_encode($status_solicitud) . '',
                    'metodo' => 'POST'
                ]);

                //Y elimino el ProgramaSolicitud
                $beneficio->delete();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'programas_solicitudes',
                    'registro' => $beneficio->id . '',
                    'campos' => json_encode($beneficio) . '',
                    'metodo' => 'DELETE'
                ]);
                DB::commit();
                // Guardamos los datos en la BD y regresamos status 200
                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }

    public function estatus($solicitud_id, $programa_id, Request $request) {
        if($request->ajax()) {
            DB::beginTransaction();                    
            try {
                $beneficio = BeneficiosprogramasSolicitud::where([
                    ['solicitud_id',$solicitud_id],
                    ['beneficioprograma_id', $programa_id],
                    ['folio', $request->input('folio')]
                    ])->firstOrFail();

                $request["usuario_id"] = $request->user()->id;
                if(!$beneficio->estadosSolicitud->contains('statusproceso_id', $request->estatus_id)){            
                    $request["statusproceso_id"] = $request->estatus_id;
                    $beneficio->estadosSolicitud()->create( $request->all() );
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'status_solicitudes',
                        'registro' => $beneficio->estadosSolicitud()->latest()->first()->id . '',
                        'campos' => json_encode($beneficio->estadosSolicitud()->latest()->first()) . '',
                        'metodo' => request()->method()
                    ]);
                }
                DB::commit();
                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }
}