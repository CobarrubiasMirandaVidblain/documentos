<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use App\Http\Controllers\PeticionBaseController;
use App\Models\ProgramasSolicitud;
use App\Models\StatusSolicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeticionController extends PeticionBaseController {

    public function __construct(){
        //parent::__construct('ATENCIÓN CIUDADANA', 'atnciudadana');
    }   
    
    public function update($peticion_id, Request $request){
        $peticion = ProgramasSolicitud::findOrFail($peticion_id);
        try {
            DB::beginTransaction();
            //Si si registro beneficiarios les asigno la entrega
            if($peticion->solicitudespersonas()->count() > 0) {
                foreach($peticion->solicitudespersonas as $beneficiario){
                    $beneficiario->entregado = 1;
                    $beneficiario->save();
                }
            }else {
                //No se si obligar a capturar por lo menos a un beneficiario o 
                //en dado caso que no tenga beneficiarios agrego al solicitante como uno
                //o que hago ueies?
                //-- No se we, creo que desde la captura se puede preguntar si el solicitante es el beneficiario :D 
            }

            $status_solicitud = StatusSolicitud::updateOrCreate([
                'programas_solicitud_id' => $peticion_id,
                'statusproceso_id' => $request['estatus'],
                'motivoprograma_id' => $request['motivo_bitacora'],
                'observacion' => $request['observacion']            
            ],[
                'usuario_id' => auth()->user()->id
            ]);

            //Agregando a la bitácora
            auth()->user()->bitacora(request(), [
            'tabla' => 'status_solicitudes',
            'registro' => $status_solicitud->id . '',
            'campos' => json_encode($status_solicitud) . '',
            'metodo' => 'POST'
            ]);

            DB::commit();

        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
        }    
    }    
}