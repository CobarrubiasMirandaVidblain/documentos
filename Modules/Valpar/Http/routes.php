<?php

Route::group(['middleware' => 'web', 'prefix' => 'valpar', 'namespace' => 'Modules\Valpar\Http\Controllers'], function(){
  Route::get('/', 'PeticionController@index');
  Route::resource('peticiones', 'PeticionController',['as'=>'valpar','only'=>['index','show','update','destroy']]);   //Peticiones
  Route::resource('programas', 'ProgramaController',['as'=>'valpar']);                                                //Programas
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'unidaddeportiva' ]);                                //Beneficiarios
});
