<?php

Route::group(['middleware' => 'web', 'prefix' => 'gestionproyectos', 'namespace' => 'Modules\GestionProyectos\Http\Controllers'], function()
{
    Route::get('/', 'GestionProyectosController@index');
});
