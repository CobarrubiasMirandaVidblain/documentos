<?php

Route::group(['middleware' => 'web', 'prefix' => 'unidaddeportiva', 'namespace' => 'Modules\UnidadDeportiva\Http\Controllers'], function(){
  Route::get('/', 'PeticionController@index');
  Route::resource('peticiones', 'PeticionController',['as'=>'unidaddeportiva','only'=>['index','show','update','destroy']]);                                                         //Peticiones
  Route::resource('programas', 'ProgramaController',['as'=>'unidaddeportiva']);                                                                                                      //Programas
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'unidaddeportiva' ]);                               //Beneficiarios
});
