$(document).ready(function () {

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        //errorElement: 'span',
        //errorClass: 'label label-danger',
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    $("#form_persona").validate({
        //errorLabelContainer: '#form_errors',
        rules: {
            nombre: {
                required: true,
                minlength: 3,
                pattern_nombre: 'El nombre solo puede contener letras.'
            },
            primer_apellido: {
                required: true,
                minlength: 1,
                pattern_apellidos: 'El apellido paterno solo puede contener letras.'
            },
            segundo_apellido: {
                required: true,
                minlength: 1,
                pattern_apellidos: 'El apellido materno solo puede contener letras.'
            },
            calle: {
                required: true,
                minlength: 3
            },
            numero_exterior: {
                required: true,
                minlength: 1,
                pattern_numeros: 'Solo numeros, S/N si no dispone de uno.'
            },
            numero_interior: {
                required: false,
                pattern_numeros: 'Solo numeros, S/N si no dispone de uno.'
            },
            colonia: {
                required: true,
                minlength: 3
            },
            fecha_nacimiento: {
                required: true
            },
            referencia_domicilio: {
                required: true,
                minlength: 3
            },
            municipio_id: {
                required: true
            },
            localidad_id: {
                required: true
            },
            discapacidad_id: {
                required: true
            },
            numero_celular: {
                required: true,
                pattern_telefonos: 'Complete el formato.'
            },
            numero_local: {
                required: true,
                pattern_telefonos: 'Complete el formato.'
            },
            curp: {
                required: true,
                minlength: 18,
                maxlength: 19,
                pattern_curp: '18 caracteres de la CURP'
            },
            codigopostal: {
                required: true,
                minlength: 5,
                maxlength: 5,
                pattern_numeros: 'Código postal solo puede contener números'
            },
            clave_electoral: {
                required: false,
                minlength: 13,
                pattern_numeros: 'solo numeros'
            }
        },
        messages: {
            nombre: {
                required: 'Por favor introduzca un nombre',
                minlength: 'El nombre debe tener al menos 3 caracteres.'
            },
            primer_apellido: {
                required: 'Por favor introduzca un apellido o "X"',
                minlength: 'El nombre debe tener al menos 3 caracteres.'
            },
            segundo_apellido: {
                required: 'Por favor introduzca un apellido o "X"',
                minlength: 'El nombre debe tener al menos 3 caracteres.'
            },
            fecha_nacimiento: {
                required: 'Por favor seleccione'
            },
            calle: {
                required: 'Por favor ingrese la calle'
            },
            numero_exterior: {
                required: 'Por favor ingrese el numero ext, o S/N'
            },
            colonia: {
                required: 'Por favor ingrese la colonia'
            },
            codigopostal: {
                required: 'Por favor ingrese la codigo postal'
            },
            municipio_id: {
                required: 'Por favor seleccione'
            },
            localidad_id: {
                required: 'Por favor seleccione'
            },
            discapacidad_id: {
                required: 'Por favor seleccione'
            },
            curp: {
                required: 'Por favor ingrece la curp',
                minlength: 'La CURP consta de 18 carcateres',
                maxlength: 'La CURP consta de 18 carcateres'
            },
            referencia_domicilio: {
                required: 'Por favor rellene'
            },
            numero_celular: {
                required: 'Introduca un numero celular de contacto',
                minlength: 'Introduca celular a 10 digitor',
                maxlength: 'Introduca celular a 10 digitor'
            },
            numero_local: {
                required: 'introduzca un numero fijo de contacto',
                minlength: 'introduzca numero local a 7 digitos',
                maxlength: 'introduzca numero local a 7 digitos'
            },
            clave_electoral: {
                minlength: '13 digitos de la clave electoral',
                pattern_numeros: 'La clave electoral son solo numeros'
            }
        },
        submitHandler: function (form) {
            candidato_create_edit();
        }
    });

    $.validator.addMethod('pattern_nombre', function (value, element) {
        return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
    }, 'Solo letras y espacios entre palabras');
    $.validator.addMethod('pattern_apellidos', function (value, element) {
        return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{3,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
    }, 'Solo letras y espacios entre palabras, si no dispone de uno coloque X');
    $.validator.addMethod('pattern_curp', function (value, element) {
        return this.optional(element) || /^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/.test(value);
    }, 'Revise los caracteres y vuelve a intentarlo');
    $.validator.addMethod('pattern_numeros', function (value, element) {
        return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
    }, 'Solo numeros, S/N si no dispone de uno.');
    $.validator.addMethod('pattern_telefonos', function (value, element) {
        return this.optional(element) || /^\(\d{2,3}\)\s\d+(?:-\d+)+$/.test(value);
    }, 'Siga el formato');
});