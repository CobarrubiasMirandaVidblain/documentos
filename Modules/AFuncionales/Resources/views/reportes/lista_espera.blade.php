@extends('afuncionales::layouts.master')

@section('content-subtitle', 'Solicitantes')

@section('li-breadcrumbs')
    <li class="active">Solicitantes</li>
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')

    <div class="box box-primary shadow">
            <div class="box-header">
              <h3 class="box-title">Solicitantes en lista de espera</h3>
            </div>
            <div class="box-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search" name="search" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </span>
                </div>

                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'solicitantes', 'name' => 'solicitantes', 'style' => 'width: 100%']) !!}
          </div>
    </div>

    @include('afuncionales::peticiones.modals_entrega')
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#solicitantes').dataTable();
		datatable_solicitantes = $(table).DataTable();

        $('#search').keypress(function(e) {
			if(e.which === 13) {
				datatable_solicitantes.search($('#search').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_solicitantes.search($('#search').val()).draw();
		});

        $("#empleado_id").select2({
		    language: 'es',
		    minimumInputLength: 2,
		    ajax: {
                delay: 500,
			    url: '{{ route('empleados.select') }}',
			    dataType: 'JSON',
			    type: 'GET',
			    data: function(params) {
				    return {
					    search: params.term
				    };
			    },
			    processResults: function(data, params) {
				    params.page = params.page || 1;
				    return {
					    results: $.map(data, function(item) {
						    return {
							    id: item.id,
							    text: item.nombre,
							    slug: item.nombre,
							    results: item
						    }
					    })
				    };
			    },
			    cache: true
		    }
	    }).change(function(event) {
		    $("#empleado_id").valid();	
        });

        var motivos = $("#motivo");
        $.ajax({
            type: "GET",
            url: "/afuncionales/motivos",
            success: function(res){
                $(res).each(function(key,value) {
                    motivos.append("<option id='" + value.id + "' value=" + value.id + ">" + value.motivo +"</option>");                        
                });
                $('#motivo').select2({
                    language: 'es',
                    placeholder: 'SELECCIONE UNA OPCIÓN',
                    minimumResultsForSearch: Infinity
                }).change(function(event){
                    $(this).valid();
                });
            }
        });

        $('#form_f').validate({
            rules:  {
                apoyo: {
                    required: true
                },
                fecha: {
                    required: true
                },
                recibo: {
                    required: true
                },
                empleado_id: {
                    required: true
                }
            }
        });

        $('#form_r').validate({
            rules:  {
                motivo: {
                    required: true
                }
            }
        });

        var date = $('#fecha_entrega').datepicker({
            autoclose: true,
            language: 'es',
            format: 'dd-mm-yyyy',
            startDate: '01-01-1900',
		    endDate: '0d'
        }).change(function(event) {
            $('#fecha_entrega').valid();
        });
	});

    function abrir_modal(ruta, programa_id, nombre, modal) {
        $('#apoyo').select2({
            language: 'es',
            ajax: {
                delay: 500,
                url: '{{ route('afuncionales.apoyos.select') }}',
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term,
                        programa: programa_id
                    };
                },
                processResults: function(data, params) {                    
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.folio,
                                text: item.producto + ' ' + item.folio,
                                slug: item.producto,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $("#form_f, #form_r")[0].reset();
        $('#apoyo, #empleado_id, #motivo').val(null).trigger('change');
        var validator = $( "#form_f" ).validate();
        validator.resetForm();
        validator = $( "#form_r" ).validate();
        validator.resetForm();
        $("#form_f").attr("action", ruta);
        $("#form_r").attr("action", ruta);
        $("#entregaSolicitante, #rechazoSolicitante").html('<label>Solicitante:</label> '+ nombre);
        $("#"+modal).modal("show");
    }

    function entregar() {
        if($('#form_f').valid()) {
            block();            
            var form = $('#form_f');
            var formData = new FormData(form[0]);
            formData.append("type","Entregado");
            formData.append("folio",$("#apoyo").select2('data')[0].results.folio);
            formData.append("areas_producto_id",$("#apoyo").select2('data')[0].results.areas_producto_id);
            $.ajax({
                url: form.attr('action'),
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,           
                success:  function (response) {
                    unblock();
                    datatable_solicitantes.ajax.reload(null, false);
                    $(".modal").modal("hide");
                    $('#arc2').val('');
                    $('#apoyo').val(null).trigger('change');
                    swal({
                        title: 'Entrega registrada',
                        text: "",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Aceptar'
                    });
                },
                error: function(response){
                    swal({
						title: 'Error al registrar la entrega.',				
						text: 'Código de error: ' + response.status,
						type: 'error',
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'Regresar',
						allowEscapeKey: false,
						allowOutsideClick: false
					});
                }
            }); 
        }
    }

    function rechazar(){
        if($('#form_r').valid()) {
            block();
            var form = $('#form_r');
            var formData = new FormData(form[0]);
            formData.append("type","Cancelado");                    
            $.ajax({
                url: $('#form_r').attr('action'),               
                type: "POST",
                data: formData,
                processData: false, 
                contentType: false,
                success:  function (response) {
                    unblock();
                    datatable_solicitantes.ajax.reload(null, false);
                    $(".modal").modal("hide");
                    swal(        
                        'Entrega cancelada',
                        '',
                        'success'
                    )                    
                }
            });
        }
    }


</script>
@endpush


