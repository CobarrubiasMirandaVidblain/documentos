@extends('afuncionales::layouts.master')

@section('content-subtitle', 'Grafiquitas')

@section('li-breadcrumbs')
    <li class="active">Grafiquitas</li>
@endsection

@push('head')

<style type="text/css">
</style>
@endpush

@section('content')

    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Grafiquitas</h3>
            </div>
            <div class="box-body" style="width:75%;">
                {!! $chartjs->render() !!}
          </div>
        </div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components\chart.js\Chart.js') }}"></script>
@endpush


