@extends('afuncionales::layouts.master')

@section('content-subtitle', 'Beneficiarios')

@section('li-breadcrumbs')
    <li class="active">Beneficiarios</li>
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
    .small-box {
        cursor: pointer;
    }
</style>
@endpush

@section('content')
<section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-green" onclick="Beneficiario.filtrar_beneficiarios('NUEVO')">
                    <div class="inner">
                        <h3 id="beneficiarios_nuevos">...</h3>
                        <p>Solicitantes nuevos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-edit"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-yellow" onclick="Beneficiario.filtrar_beneficiarios('VALIDANDO')">
                    <div class="inner">
                        <h3 id="beneficiarios_proceso">...</h3>
                        <p>Solicitantes en proceso</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-aqua" onclick="Beneficiario.filtrar_beneficiarios('FINALIZADO')">
                    <div class="inner">
                        <h3 id="beneficiarios_finalizados">...</h3>
                        <p>Beneficiarios</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box bg-red" onclick="Beneficiario.filtrar_beneficiarios('CANCELADO')">
                    <div class="inner">
                        <h3 id="beneficiarios_cancelados">...</h3>
                        <p>Solicitantes cancelados</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-minus-square"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
    	    <div class="col-xs-12">
    		    <div class="box box-primary shadow">
                    <div class="box-header with-border">
		                <h3 class="box-title">Lista de beneficiarios</h3>
                    </div>
				    <div class="box-body">
                        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                            <input type="text" id="buscar" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                            </span>
                        </div>
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiarios', 'name' => 'beneficiarios', 'style' => 'width: 100%']) !!}
				    </div>
    		    </div>
    	    </div>
        </div>
    </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">

    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    
    var Beneficiario = (() => {

        //DataTable de beneficiarios
        var beneficiarios;

        //Inicializa DataTable
        var inicializar = () => {
            beneficiarios = $('#beneficiarios').DataTable();
            beneficiarios.on( 'xhr', function () {
                var data = beneficiarios.ajax.json();
                $("#beneficiarios_nuevos").text( data.beneficiarios_nuevos );
                $("#beneficiarios_proceso").text( data.beneficiarios_proceso );
                $("#beneficiarios_finalizados").text( data.beneficiarios_finalizados );
                $("#beneficiarios_cancelados").text( data.beneficiarios_cancelados );
            });

            $('#buscar').keypress(function(e) {
			    if(e.which === 13) {
				    beneficiarios.search($('#buscar').val()).draw();
			    }
		    });

		    $('#btn_buscar').on('click', function() {
			    beneficiarios.search($('#buscar').val()).draw();
            });           
            
            filtrar_beneficiarios('NUEVO');
        }

        var filtrar_beneficiarios = (estatus) => {
            $('#beneficiarios').DataTable().columns(17).search(estatus).draw();
        }

        return {
            inicializar,
            filtrar_beneficiarios
        }

    })();

    $(document).ready(function() {
        Beneficiario.inicializar();
    });
    
</script>
@endpush