@extends('afuncionales::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css"
    rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css"
    rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css"
    rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css"
    rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css"
    rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle', 'Peticiones')

@if(isset($notificaciones) && $notificaciones->count() > 0)
@section('notification-text', 'Tienes ' . $notificaciones->count() . ($notificaciones->count() == 1 ? ' petición nueva'
: ' peticiones nuevas'))
@section('notification-number', $notificaciones->count() )
@section('notification-all', 'Ver todas las peticiones')
@section('notification-url-all', '/afuncionales/peticiones/')
@section('notification-list')
@foreach($notificaciones as $notificacion)
<a
    href="{{ route('afuncionales.peticiones.programas.show', ['peticion_id' => $notificacion->solicitud_id, 'programa_id' => $notificacion->programa_id]) }}">
    <i class="fa fa-list"></i> {{ $notificacion->apoyo }}
</a>
@endforeach
@endsection
@else
@section('notification-text', 'No tienes notificaciones')
@endif

@section('li-breadcrumbs')
<li class="active">Peticiones</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-green" onclick="Peticion.filtrar_peticiones(8,'{{ auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES']) ? 'NUEVO' : 'Vo. Bo.' }}' )">
                <div class="inner">
                    <h3 id="peticiones_nuevas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Peticiones nuevas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-edit"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-yellow" onclick="Peticion.filtrar_peticiones(8, 'VALIDANDO')">
                <div class="inner">
                    <h3 id="peticiones_proceso"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Peticiones en proceso</p>
                </div>
                <div class="icon">
                    <i class="fa fa-caret-square-o-right"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-aqua" onclick="Peticion.filtrar_peticiones(8, 'FINALIZADO')">
                <div class="inner">
                    <h3 id="peticiones_finalizadas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Peticiones finalizadas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-red" onclick="Peticion.filtrar_peticiones(8, 'CANCELADO')">
                <div class="inner">
                    <h3 id="peticiones_canceladas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Peticiones canceladas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-minus-square"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 class="box-title">Lista de peticiones</h3>
                </div>
                <div class="box-body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="search" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-info btn-flat" id="btn_buscar"
                                name="btn_buscar">Buscar</button>
                        </span>
                    </div>
                    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable
                    dtr-inline', 'id' => 'peticiones', 'name' => 'peticiones', 'style' => 'width: 100%'],true) !!}
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-cancelar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" onclick="cerrar_modal('modal-cancelar')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-cancelar-title">Cancelar petición</h4>
            </div>
            <div class="modal-body" id="modal-body-cancelar">
                <form data-toggle="validator" role="form" id="form_cancelar">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Folio:</label>
                                <span id="folio" name="folio"></span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Apoyo:</label>
                                <span id="apoyo" name="apoyo"></span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Remitente:</label>
                                <span id="remitente" name="remitente"></span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="motivo_id">Motivo:</label>
                                <select class="form-control select2" id="motivo_id" name="motivo_id"
                                    style="width: 100%;">
                                    <option selected value="">SELECCIONE UNA OPCIÓN</option>
                                    @foreach($motivos as $motivo)
                                    <option value="{{ $motivo->id }}">{{ $motivo->motivo->motivo }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="observacion">Observación:</label>
                                <textarea id="observacion" name="observacion" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" id="modal-footer-inactivar">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-cancelar')">
                    <i class="fa fa-reply"></i>
                    Regresar</button>
                <button type="button" class="btn btn-success" onclick="cancelar_peticion()">
                    <i class="fa fa-check"></i>
                    Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-reactivar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header btn-warning">
                <button type="button" class="close" onclick="cerrar_modal('modal-rectivar')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-reactivar-title">Reactivar petición</h4>
            </div>
            <div class="modal-body" id="modal-body-cancelar">
                <form data-toggle="validator" role="form" id="form_reactivar">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Folio:</label>
                                <span id="folio" name="folio"></span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Apoyo:</label>
                                <span id="apoyo" name="apoyo"></span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label>Remitente:</label>
                                <span id="remitente" name="remitente"></span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="observacion">Observación:</label>
                                <textarea id="observacion" name="observacion" class="form-control" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" id="modal-footer-inactivar">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-reactivar')">
                    <i class="fa fa-reply"></i>
                    Regresar</button>
                <button type="button" class="btn btn-success" onclick="reactivar_peticion()">
                    <i class="fa fa-check"></i>
                    Aceptar</button>
            </div>
        </div>
    </div>
</div>
@stop

@push('body')
<script type="text/javascript"
    src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript"
    src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript"
    src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript"
    src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript"
    src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript"
    src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">
    $.ajaxSetup({
	    headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
    });

    $(document).ready(function(){
        $(':input').on('propertychange input', function(e) {
		    var ss = e.target.selectionStart;
		    var se = e.target.selectionEnd;
		    e.target.value = e.target.value.toUpperCase();
		    e.target.selectionStart = ss;
		    e.target.selectionEnd = se;
	    });

        var table = $('#peticiones').dataTable();
		    datatable_peticiones = $(table).DataTable();

        datatable_peticiones.on( 'xhr', function () {
            var json = datatable_peticiones.ajax.json();
            $("#solicitudes_nuevas").text( json.solicitudes_nuevas );
            $("#solicitudes_espera").text( json.solicitudes_espera );
            $("#solicitudes_finalizadas").text( json.solicitudes_finalizadas );
            $("#solicitudes_canceladas").text( json.solicitudes_canceladas );
        } );

        $('#search').keypress(function(e) {
			if(e.which === 13) {
				datatable_peticiones.search($('#search').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_peticiones.search($('#search').val()).draw();
		});          

        $('#form_cancelar').validate({
		    rules: {
	  		    motivo_id: {
  				    required: true
			    }				
		    },
		    messages: {}
	    });

        $('#form_reactivar').validate({
		    rules: {
	  		    observacion: {
  				    required: true
			    }				
		    },
		    messages: {}
	    });

        $("#motivo_id").select2({
		    language: 'es',
		    placeholder: 'SELECCIONE UN MOTIVO',
            minimumResultsForSearch: Infinity			
	    }).change(function(event) {
		    $("#motivo_id").valid();				
	    });

        Peticion.inicializar();
	});

    var Peticion = (() => {
        //DataTable de peticiones
        var peticiones;
        var ejercicio;

        //Inicializa DataTable
        var inicializar = () => {
            peticiones = $('#peticiones').DataTable();
            peticiones.on( 'xhr', function () {
                var data = peticiones.ajax.json();
                if(data != undefined){
                    $("#peticiones_nuevas").text( data.peticiones_nuevas );
                    $("#peticiones_proceso").text( data.peticiones_proceso );
                    $("#peticiones_finalizadas").text( data.peticiones_finalizadas );
                    $("#peticiones_canceladas").text( data.peticiones_canceladas );
                }                
            });

            $('#buscar').keypress(function(e) {
                if(e.which === 13) {
                    peticiones.search($('#buscar').val()).draw();
                }
            });

            $('#btn_buscar').on('click', function() {
                peticiones.search($('#buscar').val()).draw();
            });

            // filtrar_peticiones(8, 'Vo.');
        }


        var cerrar_modal = (modal) => {
            $("#"+modal).modal('hide');
        }

        //La columna del DataTable, recuerden que empieza con el indice 0 y se tambien cuentan las columnas no visibles
        //El valor a ser buscado en esa columna
        var filtrar_peticiones = (columna, valor) => {
            $('#peticiones').DataTable().columns(columna).search(valor).draw();
        }

        var autorizar = (peticion_id) => {
            block();
            $.ajax({
                url: window.location.href + '/' + peticion_id,				
                type: "PUT",
                data: {estatus: 9},
                success: function(response) {                  
                    $.get("{{ route('peticiones.cantidad') }}",(data) => {
                        if(data > 0){
                            $('#lbl_Peticiones_Nuevas_Director').text(data);
                            $('#lbl_Peticiones_Nuevas_Director').parent().removeClass('hidden');
                        }else{
                            $('#lbl_Peticiones_Nuevas_Director').parent().addClass('hidden');
                        }
                        peticiones.ajax.reload(null, true);
                        unblock();
                    });
                },
                error: function(response){
                    unblock();              
                }
            });
        }

        return {
            inicializar,
            filtrar_peticiones,
            autorizar
        }

    })();

    function cerrar_modal(modal){
        $("#"+modal).modal('hide');
    }  

    function cancelar(url, folio, apoyo, remitente) {
        $('#form_cancelar').attr('action', url);
        $('#motivo_id').val(null).trigger('change');
		$("[name='observacion']").val('');
        $("[name='folio']").text(folio);
        $("[name='apoyo']").text(apoyo);
        $("[name='remitente']").text(remitente);
        $("#modal-cancelar").modal("show");
    }

    function cancelar_peticion(){
        if(!$('#form_cancelar').valid()){
			return;
		}
        swal({
			title: '¿Está seguro de cancelar la petición?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sí, cancelar',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.value) {
				$.ajax({
                    url: $('#form_cancelar').attr('action'),       
                    data: $('#form_cancelar').serialize()+"&estatus_id=7",
                    type: "PUT",
                    success: function(response) {  
                        datatable_peticiones.ajax.reload(null, false);                  
                        cerrar_modal('modal-cancelar');
                        swal(        
                            'Petición cancelada',
                            '',
                            'success'
                        )   
                    }
                });
			}
		});     
    }

    function reactivar(url, folio, apoyo, remitente){
        $('#form_reactivar').attr('action', url);
    	$("[name='observacion']").val('');
        $("[name='folio']").text(folio);
        $("[name='apoyo']").text(apoyo);
        $("[name='remitente']").text(remitente);
        $("#modal-reactivar").modal("show");
    }

    function reactivar_peticion(){
        if(!$('#form_reactivar').valid()){
			return;
		}
        swal({
			title: '¿Está seguro de reactivar la petición?',
			text: "",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sí, reactivar',
			cancelButtonText: 'No'
		}).then((result) => {
			if (result.value) {
				$.ajax({
                    url: $('#form_reactivar').attr('action'),       
                    data: $('#form_reactivar').serialize()+"&estatus_id=4",
                    type: "PUT",
                    success: function(response) {  
                        datatable_peticiones.ajax.reload(null, false);                  
                        cerrar_modal('modal-reactivar');
                        swal(        
                            'Petición reactivada',
                            '',
                            'success'
                        )          
                    }
                });
			}
		});     
    }
</script>
@endpush