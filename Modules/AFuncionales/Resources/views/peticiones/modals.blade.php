<div class="modal fade" id="modal-discapacidad">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-discapacidad"></h4>
            </div>
            <div class="modal-body" id="modal-body-discapacidad"></div>
            <div class="modal-footer" id="modal-footer-discapacidad"></div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-expediente">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-expediente"></h4>
                <h4>Expediente</h4>
            </div>
            <div class="modal-body" id="modal-body-expediente"></div>
            <div class="modal-footer" id="modal-footer-expediente"></div>      
        </div>
    </div>
</div>

<form id="form_cancelar" method="POST" action="#"> 
      <div class="modal fade" id="modal-cancelar">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header btn-danger">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">¿Está seguro de cancelar al solicitante?</h4>
            </div>
            <div class="modal-body">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">                    
                    <label>Solicitante: </label>
                    <span id="lblSolicitante"></span>
                </div>
                    </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                  <label for="motivo">Motivo de cancelación:</label>
                    <select id="motivo" name="motivo" class="form-control select2" style="width: 100%;">
                      <option value="" disabled selected>SELECCIONE UN MOTIVO...</option>
                    </select>
                </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>
              <button type="button" class="btn btn-success" onclick="cancelarSolicitante()"><i class="fa fa-check"></i> Aceptar</button>            
            </div>
          </div>
        </div>
      </div>
    </form>