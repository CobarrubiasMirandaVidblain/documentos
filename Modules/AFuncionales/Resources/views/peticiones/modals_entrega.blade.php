<!--Formulario para entrega de apoyo-->
<form id="form_f" method="post" enctype="multipart/form-data" action="#">
    <!-- Modal para entregar apoyo-->
    <div class="modal fade" id="entregar">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header btn-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">Datos de la entrega</h4></center>
          </div>
          <div class="modal-body">
            <div>
                <div class="form-group" id="entregaSolicitante"></div>
    
              <div class="form-group">                
                <label for="apoyo">Apoyo a entregar:</label>
                  <select id="apoyo" name="apoyo" class="form-control select2" style="width: 100%;">
                  </select>
    
                </div>
                  <div class="form-group">
    
                    <label>Fecha de entrega:</label>
    
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="fecha_entrega" autocomplete="off" name="fecha">
                    </div>
                    <!-- /.input group -->
                  </div>
    
                  
                      <div class="form-group">
                          <label for="empleado_id">Empleado que autoriza la entrega</label>  
                          <select id="empleado_id" class="form-control select2"  name="empleado_id" style="width: 100%;"></select>   
                      </div>
                  
    
                  <!-- /.form group -->
                  <div class="form-group">
                      <label>Subir Recibo de Apoyo Funcional</label>
                    <input id="arc2" type="file" name="recibo">
                  </div>
                  
            </div>
            
            <input id="arc1" name="_token" type="hidden" value="{{ csrf_token() }}" />
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><I class="fa fa-close"></I> Cerrar</button>
            <button type="button" class="btn btn-primary pull-rigth" onclick="Beneficiario.entregar();"><i class="fa fa-database"></i> Guardar</button>
            
          </div>
        </div>
      </div>
    </div>
    
    </form>
    
    <!--Formulario para rechazar de apoyo-->
    <form id="form_r" method="POST" action="#">
      <input name="_token" type="hidden" value="{{ csrf_token() }}" />
      <!-- Modal para entregar apoyo-->  
    <div class="modal fade" id="rechazar">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header btn-danger">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">¿Está seguro de cancelar la entrega?</h4></center>
          </div>
          <div class="modal-body">
              <div class="form-group" id="rechazoSolicitante"></div>
            <div>
              <div class="form-group">
                <label for="motivo">Motivo de cancelación de entrega</label>
                  <select id="motivo" name="motivo" placeholder="Motivo" class="form-control select2" style="width: 100%;">
                    <option value="" disabled selected>Seleccione un motivo...</option>
                  </select>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><I class="fa fa-close"></I> Cerrar</button>
            <button type="button" class="btn btn-primary pull-rigth" onclick="rechazar()"><I class="fa fa-check"></I> Aceptar</button>
          </div>
        </div>
      </div>
    </div>
    </form>