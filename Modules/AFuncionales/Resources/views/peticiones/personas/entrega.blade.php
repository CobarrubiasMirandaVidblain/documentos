
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <img src="{{ asset($solicitud->salida_producto->get_url_comprobante()) }}" class="img-thumbnail center-block">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="form-group">
                <label>Beneficiario:</label>
                {{ $solicitud->persona->get_nombre_completo() }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="form-group">
                <label>Apoyo entregado:</label>
                {{ $solicitud->salida_producto->detallesalidasproductos->first()->areasproducto->producto->producto .' '. $solicitud->salida_producto->detallesalidasproductos->first()->productosfoliados->first()->folio }}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="form-group">
                <label>Fecha de entrega:</label>
                {{ $solicitud->salida_producto->get_formato_fecha() }}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="form-group">
                <label>Autorizado por:</label>
                {{ $solicitud->salida_producto->empleado->persona->get_nombre_completo() }}
            </div>
        </div>        
    </div>