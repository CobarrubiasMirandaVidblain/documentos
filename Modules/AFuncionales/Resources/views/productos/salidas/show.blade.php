<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    
        {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <img src="{{ asset($salida->get_url_comprobante()) }}" class="img-thumbnail center-block">
        </div> --}}

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Fecha:</label>
                {{ $salida->get_formato_fecha() }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Tipo:</label>
                {{ $salida->tipossalida->tipo }}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Solicitante:</label>
                {{ $salida->persona->get_nombre_completo() }}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Autorizado por:</label>
                {{ $salida->empleado->persona->get_nombre_completo() }}
            </div>
        </div>        
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <input id="recibo" name="recibo[]" type="file" class="file" url="{{ $salida->recibo ? asset($salida->get_url_comprobante()) : '' }}">
    </div>
</div>

<br>
    
<div class="box box-primary">
    <div class="box-header with-border">
        <h5 class="box-title">Lista de productos</h5>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table id="productos" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">PRODUCTO</th>
                            <th class="text-center">FOLIO</th>                    
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($salida->detallesalidasproductos as $detalle)
                            @foreach($detalle->productosfoliados as $productofoliado)
                                <tr>                        
                                    <td>{{$detalle->areasproducto->producto->producto}}</td>
                                    <td class="text-center">{{$productofoliado->folio}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center">FOLIO</th>
                            <th class="text-center">PRODUCTO</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>     