@extends('afuncionales::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<style>
    .file-drop-disabled {
        width: 50%;
        margin: 0 auto;
    }
</style>
@endpush

@section('content-subtitle', 'Salidas')

@section('li-breadcrumbs')
    <li class="active">Salidas</li>
@endsection

@section('content')

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-primary shadow">
                    <div class="box-header with-border">
                        <h3 class="box-title">Salidas de productos</h3>
                    </div>
				    <div class="box-body">
                        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                            <input type="text" id="search" name="search" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                            </span>
                        </div>
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'salidas', 'name' => 'salidas', 'style' => 'width: 100%']) !!}
				    </div>
    			</div>
    		</div>
    	</div>
	</section>
	
<div class="modal fade" id="modal-salida">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Datos de la salida</h4>
			</div>
			<div class="modal-body" id="modal-body-salida"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>	
			</div>      
		</div>
	</div>
</div>
            
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/js/locales/es.min.js"></script>
{!! $dataTable->scripts() !!}
<script type="text/javascript">    

    $(document).ready(function() {
		var salidas = $('#salidas');  
		datatable_salidas = $(salidas).DataTable();

    	$('#search').keypress(function(e) {
			if(e.which === 13) {
				datatable_salidas.search($('#search').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_salidas.search($('#search').val()).draw();
		});         
	});    

	function consultar(url){
      	$.get(url, function(data) {
        	$('#modal-body-salida').html(data);
			crear_datatable_productos();
			var type = $("#recibo").attr('url').split('.')[$("#recibo").attr('url').split('.').length-1].toLowerCase();

			if(type == 'pdf'){
				type = 'pdf';
			}else{
				type = 'image';
			}		
            
			$("#recibo").fileinput({
            	initialPreview: [ $("#recibo").attr('url') ],
            	initialPreviewAsData: true,
            	initialPreviewFileType: type,
                initialPreviewDownloadUrl: $("#recibo").attr('url'),
            	initialPreviewConfig: [
                	{type: type, caption: "Comprobante", url: $("#recibo").attr('url'), downloadUrl: $("#recibo").attr('url'), key: 1}
            	],
            	initialPreviewShowDelete: false,
            	fileActionSettings: {
                	showDrag: false
            	},
                overwriteInitial: false,
            	language: "es",
            	showUpload: false,
            	showCancel: false,
				showRemove: false,
				showBrowse: false,
				showCaption: false,
				showClose: false,
            	dropZoneEnabled: false
        	});
        	$('#modal-salida').modal('show');
      	});
	}

	function crear_datatable_productos(){
		datatable_productos = $("#productos").DataTable({
			language: {
				url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
			},
			dom: 'itp',
			lengthMenu: [ [10], [10] ],
			responsive: true,
			autoWidth: true,
			processing: true			
		});
	}

	function cancelar(salida_id){
		swal({
			title: '¿Está seguro de cancelar la salida?',
			text: "Los productos relacionados volverán a estar disponibles",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'SI, cancelar',
			cancelButtonText: 'No'
			}).then((result) => {
			if (result.value) {
                $.ajaxSetup({
          			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        		});
        		$.ajax({
          			url: "{{ URL::to('afuncionales/productos/salidas/destroy') }}" + '/' + salida_id,
          			type: 'POST',
          			data: {_method: 'delete'},
          			success: function(response) {
            			datatable_salidas.ajax.reload(null, false);
            			swal({
              			title: '¡Correcto!',
              			text: 'La salida ha sido cancelada.',
              			type: 'success',
              			timer: 3000
            			});
          			},
          			error: function(response) {
          			}
				});
			}
		});	
	}

    function registrar_salida(){
        location.href = "{{route('afuncionales.productos.salidas.create')}}";
	}
</script>
@endpush