<?php

Route::group(['middleware' => 'web', 'prefix' => 'afuncionales', 'namespace' => 'Modules\AFuncionales\Http\Controllers'], function()
{
    Route::get('/', 'PeticionController@index');
    Route::get('/graficas', 'AFuncionalesController@graficas'); 
    
    //Route::get('/peticiones/{peticion}/programas/{programa}/beneficiarios/{beneficiario}/entrega', 'PeticionProgramaBeneficiarioController@showEntrega')->name('afuncionales.peticiones.programas.beneficiarios.entrega.show');
    //Route::get('beneficiarios', 'AFuncionalesController@beneficiarios')->name('afuncionales.beneficiarios');
    Route::get('beneficiarios', 'AFuncionalesController@beneficiarios')->name('afuncionales.beneficiarios');
    Route::get('solicitantes', 'AFuncionalesController@solicitantes')->name('afuncionales.solicitantes');
    Route::get('cancelados', 'AFuncionalesController@cancelados')->name('afuncionales.cancelados');
    Route::get('/productos/inventario', 'ProductosController@inventario')->name('afuncionales.productos.inventario');
    Route::get('/productos/salidas', 'SalidaController@index')->name('afuncionales.productos.salidas');
    Route::get('/productos/select', 'ProductosController@productosSelect')->name('afuncionales.productos.select');
    
    Route::resource('/productos/entradas','EntradaController',['as'=>'afuncionales.productos','parameters'=>['entrada'=>'id']]);

    //Route::delete('/productos/entradas/destroy/{id}', 'EntradaController@destroy')->name('afuncionales.productos.entradas.destroy');

    Route::get('/productos/salidas/create', 'SalidaController@create')->name('afuncionales.productos.salidas.create');
    Route::get('/productos/salidas/edit/{id}', 'SalidaController@edit')->name('afuncionales.productos.salidas.edit');
    Route::get('/productos/salidas/show/{id}', 'SalidaController@show')->name('afuncionales.productos.salidas.show');
    Route::post('/productos/salidas/update', 'SalidaController@update')->name('afuncionales.productos.salidas.update');
    Route::delete('/productos/salidas/destroy/{id}', 'SalidaController@destroy')->name('afuncionales.productos.salidas.destroy');
    Route::delete('/productos/salidas/detalle/destroy', 'SalidaController@destroyDetalle')->name('afuncionales.productos.salidas.detalle.destroy');
    Route::post('/productos/salidas', 'SalidaController@store')->name('afuncionales.productos.salidas.store');
    Route::resource('productos', 'ProductosController', ['as' => 'afuncionales']);    
    Route::get('/apoyos_funcionales', 'AfuncionalesController@apoyos')->name('afuncionales.apoyos.select');
    Route::get('/motivos', 'AfuncionalesController@motivos');
    Route::get('/dependencias', 'AfuncionalesController@dependencias')->name('afuncionales.dependencias.index');
    //Route::get('/programas', 'AfuncionalesController@programas')->name('afuncionales.programas.index');
    Route::get('/notificaciones', 'PeticionController@notificaciones')->name('afuncionales.peticiones.notificaciones');

    //Route::resource('/programas','ProgramaController',['as'=>'afuncionales']);

    //Peticiones genérico
    Route::post('/peticiones/{peticion}/beneficiarios/{beneficiario}/entrega', 'PeticionBeneficiarioController@entrega')->name('afuncionales.peticiones.programas.beneficiarios.entrega');
    Route::resource('peticiones', 'PeticionController', ['only' => ['index','show', 'update'], 'as' => 'afuncionales' ]);
    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'afuncionales' ]);
    Route::resource('peticiones.beneficiarios.discapacidad', 'PeticionBeneficiarioDiscapacidadController', ['only' => ['create', 'store', 'edit', 'update'], 'as' => 'afuncionales' ]);
    //Route::resource('peticiones.beneficiarios.expediente', 'PeticionBeneficiarioExpedienteController', ['only' => ['create', 'store', 'edit', 'update'], 'as' => 'afuncionales' ]);

    //Programas genérico
    Route::resource('programas','ProgramaController',['as'=>'afuncionales']);
});