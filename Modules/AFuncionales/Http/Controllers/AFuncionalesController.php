<?php

namespace Modules\AFuncionales\Http\Controllers;

use App\Models\Motivo;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\EntradasProducto;
use Illuminate\Routing\Controller;
use App\DataTables\DependenciasDataTable;
use App\DataTables\Afuncionales\BeneficiariosDataTable;

class AFuncionalesController extends Controller {

    public function __construct(){
        $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
    }
    
    public function dependencias(DependenciasDataTable $dataTable) {
        return $dataTable->render('afuncionales::dependencias.index');
    }

    public function beneficiarios(BeneficiariosDataTable $dataTable) {
        return $dataTable->render('afuncionales::beneficiarios.index');      
    }

    public function apoyos(Request $request) {
        if ($request->ajax()){
            $apoyos = EntradasProducto::join('detallesentradas_productos', 'detallesentradas_productos.entradas_producto_id','entradas_productos.id')
            ->join('beneficiosprogramas','beneficiosprogramas.id','detallesentradas_productos.beneficioprograma_id')
            ->join('anios_programas','anios_programas.id','beneficiosprogramas.anio_programa_id')
            ->join('programas','programas.id','anios_programas.programa_id')
            ->join('programas as padre','padre.id','programas.padre_id')
            ->join('productosfoliados','productosfoliados.detallesentradas_producto_id','detallesentradas_productos.id')
            ->where(function ($query) use ($request) {
              $query->where('beneficiosprogramas.nombre', 'LIKE', '%' . $request->input('search') . '%')
                    ->orWhere('folio', 'LIKE', '%' . $request->input('search') . '%');
              })
            ->where(function($query){
              $query->where('padre.nombre', 'like', 'apoyos funcionales')
              ->orwhere('padre.nombre', 'like', 'trabajo social');
            })
            ->whereNull('productosfoliados.detallessalidas_producto_id')
            ->whereNull('productosfoliados.deleted_at');
            if($request->has('programa')){
              $apoyos = $apoyos->where('programas.id',$request->input('programa'));//('areas_productos.programa_id', $request->input('programa'));
            }
            $apoyos = $apoyos->select(['beneficiosprogramas.id','beneficiosprogramas.nombre','folio'])->paginate();
            return response()->json($apoyos);
        }
    }

    public function motivos() {
        $motivos = Motivo::where('tipo','AFUNCIONALES')->get();
        return response()->json(
            $motivos->toArray()
        );
    }

    public function graficas() {
        $chartjs = app()->chartjs
        ->name('Apoyos')
        ->type('bar')
        ->labels(['Auxiliares Auditivos', 'Sillas De Ruedas', 'Otros Apoyos Funcionales'])
        ->datasets([
            [
                "label" => "Peticiones",
                'backgroundColor' => ['rgba(255, 99, 132, 0.2)','rgba(255, 99, 132, 0.2)','rgba(255, 99, 132, 0.2)'],
                'data' => [69, 52, 30]
            ],
            [
                "label" => "Beneficiarios",
                'backgroundColor' => [],
                'data' => [12, 15, 52]
            ]
        ])
        ->options([]);

        return view('afuncionales::reportes.graficas', compact('chartjs'));
    }
}
