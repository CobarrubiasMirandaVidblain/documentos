<?php

namespace Modules\AFuncionales\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Producto;
use App\Models\AreasProducto;
use App\Models\Unidadmedida;
use App\Models\Beneficiosprograma;
use App\DataTables\PersonasDataTable;
use App\DataTables\Afuncionales\InventarioDataTable;
use App\DataTables\Afuncionales\ProductosDataTable;
use App\Models\Programa;

class ProductosController extends Controller {

  public function __construct(){
    $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProductosDataTable $dataTable) {      
      return $dataTable->render('afuncionales::productos.index', ['modulo' => 'afuncionales']);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
      return response()->json([
        'html' =>  view::make('productos.iframe.create_edit')
        ->with('modulo','afuncionales')
        ->with('unidades', Unidadmedida::get())
        ->render() ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
      if($request->ajax()) {
        try{
          DB::beginTransaction();
          $request['usuario_id'] = $request->User()->id;
          $producto = Producto::create($request->all());          
          auth()->user()->bitacora(request(), [
            'tabla' => 'cat_productos',
            'registro' => $producto->id . '',
            'campos' => json_encode($producto) . '',
            'metodo' => request()->method()
          ]);
          $area_producto = AreasProducto::create([
            'producto_id' => $producto->id,
            'area_id' => Programa::find($request['programa_id'])->programasresponsable->areasresponsable->area_id,
            'programa_id' => $request['programa_id'],
            'usuario_id' => $request['usuario_id']
          ]);
          auth()->user()->bitacora(request(), [
            'tabla' => 'areas_productos',
            'registro' => $area_producto->id . '',
            'campos' => json_encode($area_producto) . '',
            'metodo' => request()->method()
          ]);
          DB::commit();
          return response()->json(array('success' => true, 'producto' => $producto, 'areaproducto_id' => $area_producto->id));
        }catch(\Exception $e){
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
      return view('afuncionales::productos.show')
      ->with('producto', Producto::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $programa = Programa::where('nombre', 'APOYOS FUNCIONALES')->first();
        $area_id = $programa->programasresponsable->areasresponsable->area_id;
        $areas = array_map( function($o) { return $o->id_hijo; }, DB::select('call getAreasHijas(?)',[$area_id]));
        $areas[$area_id] = $area_id;        
        $producto = Producto::findOrFail($id);
        $programa = $producto->areasproductos->whereIn('area_id', $areas)->first()->programa;
        return response()->json([
            'html' =>  view::make('productos.iframe.create_edit')
            ->with('producto',$producto)
            ->with('programa', $programa)
            ->with('modulo','afuncionales')
            ->with('unidades', Unidadmedida::get())->render()
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $request['usuario_id'] = $request->User()->id;
                $producto = Producto::findOrFail($id);
                $producto->update($request->all());
                auth()->user()->bitacora(request(), [
                    'tabla' => 'cat_productos',
                    'registro' => $producto->id . '',
                    'campos' => json_encode($producto) . '',
                    'metodo' => request()->method()
                ]);
                $area_producto = AreasProducto::updateOrCreate([
                    'producto_id' => $producto->id,
                    'area_id' => Programa::find($request['programa_id'])->programasresponsable->areasresponsable->area_id
                ],[
                    'programa_id' => $request['programa_id'],
                    'usuario_id' => $request['usuario_id']
                ]);
                auth()->user()->bitacora(request(), [
                    'tabla' => 'areas_productos',
                    'registro' => $area_producto->id . '',
                    'campos' => json_encode($area_producto) . '',
                    'metodo' => request()->method()
                ]);
                DB::commit();
                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id) {
      if($request->ajax()) {
          try {
              DB::beginTransaction();
              $producto = Producto::findOrFail($id);
              $producto->delete();              
              auth()->user()->bitacora(request(), [
                'tabla' => 'cat_productos',
                'registro' => $producto->id . '',
                'campos' => json_encode($producto) . '',
                'metodo' => request()->method()
              ]);
              DB::commit();
              return response()->json(array('success' => true, 'id' => $id));
          }
          catch(\Exception $e) {
              DB::rollBack();
              return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
          }
      }
    }

    public function inventario(InventarioDataTable $dataTable) {
      return $dataTable->render('afuncionales::productos.inventario');      
    }   

    public function productosSelect(Request $request) {
      if ($request->ajax()) {
        // $productos = Producto::join('areas_productos','areas_productos.producto_id','cat_productos.id')
        // ->join('programas','programas.id','areas_productos.programa_id')
        // ->where('producto', 'LIKE', '%' . $request->input('search') . '%')
        // ->whereRaw('(select nombre from programas as programas_padres where programas_padres.id = programas.padre_id ) = "APOYOS FUNCIONALES"')
        // ->take(10)
        // ->select('areas_productos.id','cat_productos.producto')
        // ->get()
        // ->toArray();

        $productos = Beneficiosprograma::whereHas('anioprograma',function($q){
          $q->whereHas('programa',function($qu){
            $qu->whereHas('padre',function($que){
              $que->where('nombre','like','%trabajo social%')
                  ->orWhere('nombre','like','%apoyos funcionales%');
            });
          });
        })
        ->where('nombre', 'LIKE', '%' . $request->input('search') . '%')
        ->select(['beneficiosprogramas.id','beneficiosprogramas.nombre'])
        ->paginate(10);


        return response()->json($productos);
      }
    }
}