<?php

namespace Modules\AFuncionales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\DataTables\PersonasDataTable;
use App\DataTables\EmpleadosDataTable;
use App\DataTables\Afuncionales\EntradasDataTable;
use App\DataTables\Afuncionales\DetalleEntradaDataTable;
use App\Models\EntradasProducto;
use App\Models\EntradasproductosDependencia;
use App\Models\EntradasproductosPersona;
use App\Models\DetalleEntradasproductos;
use App\Models\Productosfoliados;
use App\Models\Tiposentrada;
use View;

class EntradaController extends Controller {

    public function __construct(){
        $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(EntradasDataTable $dt_Entradas, DetalleEntradaDataTable $dt_Detalle) {
        if( request()->get('table') != 'detalle'){
            return $dt_Entradas->render('afuncionales::productos.entradas.index', ['modulo' => 'afuncionales', 'dt_Entradas' => $dt_Entradas, 'dt_Detalle' => $dt_Detalle]);
        }
        $fecha = '';
        $tipo = '';
        $remitente = '';
        $receptor = '';
        $entrada = EntradasProducto::find( request()->get('entrada_id') );
        if($entrada){
            setlocale(LC_ALL, "es_ES");
            $fecha = explode(',', date('m,d,Y', strtotime($entrada->fechaentrada)));
            $fecha = mb_strtoupper(strftime('%A %e de %B de %Y', mktime(0,0,0, $fecha[0], $fecha[1], $fecha[2])));
            $tipo = $entrada->tiposentrada->tipo;
            $remitente = ($entrada->entradasproductosdependencia) ? $entrada->entradasproductosdependencia->dependencia->nombre : $entrada->entradasproductospersona->persona->get_nombre_completo();
            $receptor = $entrada->empleado->persona->get_nombre_completo();
        }        
        return $dt_Detalle->with(['fecha' => $fecha, 'tipo' => $tipo, 'remitente' => $remitente, 'receptor' => $receptor])->render('afuncionales::productos.entradas.index', ['modulo' => 'afuncionales', 'dt_Entradas' => $dt_Entradas, 'dt_Detalle' => $dt_Detalle]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(PersonasDataTable $pdt, EmpleadosDataTable $edt) {      
        $tipos_entrada = Tiposentrada::get();
        $modulo = 'afuncionales';     
        if (request()->get('table') == 'empleados') {
          return $edt->render('afuncionales::productos.entradas.create_edit', compact('pdt', 'edt', 'tipos_entrada', 'modulo'));
        }
        return $pdt->with('tipo', 'afuncionalessolicitantes')->render('afuncionales::productos.entradas.create_edit', compact('pdt', 'edt', 'tipos_entrada', 'modulo'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        if($request->ajax()){
            try {
                DB::beginTransaction();
                $request['usuario_id'] = Auth::user()->id;
                $request['fechaentrada'] = (\DateTime::createFromFormat('d-m-Y', $request['fechaentrada']))->format('Y-m-d');          
                $entrada_producto = EntradasProducto::create($request->all());
                $request['entradas_producto_id'] = $entrada_producto->id;

                auth()->user()->bitacora(request(), [
                    'tabla' => 'entradas_productos',
                    'registro' => $entrada_producto->id . '',
                    'campos' => json_encode($entrada_producto) . '',
                    'metodo' => request()->method()
                ]);
  
                if($request->has('dependencia_id')){
                    $entrada_dependencia = EntradasproductosDependencia::create($request->all());
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'entradasproductos_dependencias',
                        'registro' => $entrada_dependencia->id . '',
                        'campos' => json_encode($entrada_dependencia) . '',
                        'metodo' => request()->method()
                    ]);
                } else {
                    $entrada_persona = EntradasproductosPersona::create($request->all());
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'entradasproductos_personas',
                        'registro' => $entrada_persona->id . '',
                        'campos' => json_encode($entrada_persona) . '',
                        'metodo' => request()->method()
                    ]);
                }
  
                foreach ($request->input('productos') as $producto) {
                    $detalle = DetalleEntradasproductos::create([
                        'entradas_producto_id' => $request['entradas_producto_id'],
                        'cantidad' => $producto['cantidad'],
                        'precio' => $producto['precio'],
                        'beneficioprograma_id' => $producto['area']  
                    ]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'detallesentradas_productos',
                        'registro' => $detalle->id . '',
                        'campos' => json_encode($detalle) . '',
                        'metodo' => request()->method()
                    ]);
  
                    for($i = 0; $i < $detalle->cantidad; $i++){
                        $folio = $this->getFolio($producto['area']);                       

                        $producto_foliado = Productosfoliados::create([
                            'detallesentradas_producto_id' => $detalle->id,
                            'folio' => $folio
                        ]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'productosfoliados',
                            'registro' => $producto_foliado->id . '',
                            'campos' => json_encode($producto_foliado) . '',
                            'metodo' => request()->method()
                        ]);
                    }
                }         
                DB::commit();
            } catch(\Exception $e){
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }      
    }

    private function getFolio($area){
        $nueva_area = '';
        if($area < 10){
            $nueva_area = '00' . $area;
        }else if($area < 100){
            $nueva_area = '0' . $area;
        }else{
            $nueva_area = $area;
        }
        return date('y') . '/' . $nueva_area . '/' . (Productosfoliados::withTrashed()
                            ->join('detallesentradas_productos','detallesentradas_productos.id','productosfoliados.detallesentradas_producto_id')
                            ->where('detallesentradas_productos.beneficioprograma_id',$area)
                            ->where('folio','like', date('y').'%')->count()+1);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        return View::make("productos.entradas.show")->with('entrada', EntradasProducto::withTrashed()->findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(PersonasDataTable $pdt, EmpleadosDataTable $edt, $id) {      
        $tipos_entrada = Tiposentrada::get();
        $entrada = EntradasProducto::findOrFail($id);
        if (request()->get('table') == 'empleados') {
            return $edt->render('afuncionales::productos.entradas.create_edit', compact('pdt', 'edt', 'tipos_entrada', 'entrada'));
        }
        return $pdt->with('tipo', 'afuncionalessolicitantes')->render('afuncionales::productos.entradas.create_edit', compact('pdt', 'edt', 'tipos_entrada','entrada'));
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        return $request;
        if($request->ajax()){
            try {
                DB::beginTransaction();
                $request['usuario_id'] = Auth::user()->id;
                $request['fechaentrada'] = (\DateTime::createFromFormat('d-m-Y', $request['fechaentrada']))->format('Y-m-d');          
                $entrada_producto = EntradasProducto::create($request->all());
                $request['entradas_producto_id'] = $entrada_producto->id;

                auth()->user()->bitacora(request(), [
                    'tabla' => 'entradas_productos',
                    'registro' => $entrada_producto->id . '',
                    'campos' => json_encode($entrada_producto) . '',
                    'metodo' => request()->method()
                ]);
  
                if($request->has('dependencia_id')){
                    $entrada_dependencia = EntradasproductosDependencia::create($request->all());
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'entradasproductos_dependencias',
                        'registro' => $entrada_dependencia->id . '',
                        'campos' => json_encode($entrada_dependencia) . '',
                        'metodo' => request()->method()
                    ]);
                } else {
                    $entrada_persona = EntradasproductosPersona::create($request->all());
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'entradasproductos_personas',
                        'registro' => $entrada_persona->id . '',
                        'campos' => json_encode($entrada_persona) . '',
                        'metodo' => request()->method()
                    ]);
                }
  
                foreach ($request->input('productos') as $producto) {
                    $detalle = DetalleEntradasproductos::create([
                        'entradas_producto_id' => $request['entradas_producto_id'],
                        'cantidad' => $producto['cantidad'],
                        'beneficioprograma_id' => $producto['area']  
                    ]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'detallesentradas_productos',
                        'registro' => $detalle->id . '',
                        'campos' => json_encode($detalle) . '',
                        'metodo' => request()->method()
                    ]);
  
                    for($i = 0; $i < $detalle->cantidad; $i++){
                        $folio = date('Y') . '/' . $producto['area'] . '/' .
                        (Productosfoliados::withTrashed()
                            ->join('detallesentradas_productos','detallesentradas_productos.id','productosfoliados.detallesentradas_producto_id')
                            ->where('detallesentradas_productos.beneficioprograma_id',$producto['area'])
                            ->where('folio','like', date('Y').'%')->count()+1);

                        $producto_foliado = Productosfoliados::create([
                            'detallesentradas_producto_id' => $detalle->id,
                            'folio' => $folio
                        ]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'productosfoliados',
                            'registro' => $producto_foliado->id . '',
                            'campos' => json_encode($producto_foliado) . '',
                            'metodo' => request()->method()
                        ]);
                    }
                }         
                DB::commit();
            } catch(\Exception $e){
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $entrada = EntradasProducto::findOrFail($id);
                foreach($entrada->detalleentradasproductos as $detalle_entrada){
                    foreach($detalle_entrada->productosfoliados as $producto_foliado){
                        //Si el producto foliado ya es parte de una salida entonces no lo dejo eliminar nada pero nada
                        if($producto_foliado->detallessalidas_producto_id) {
                            return response()->json(['errors' => array(['code' => 422, 'message' => 'El producto con folio '.$producto_foliado->folio.' forma parte de una salida.'])], 422);
                        }
                        $producto_foliado->delete();
                        auth()->user()->bitacora(request(), [
                            'tabla' => 'productosfoliados',
                            'registro' => $producto_foliado->id . '',
                            'campos' => json_encode($producto_foliado) . '',
                            'metodo' => request()->method()
                        ]);
                    }
                    $detalle_entrada->delete();
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'detallesentradas_productos',
                        'registro' => $detalle_entrada->id . '',
                        'campos' => json_encode($detalle_entrada) . '',
                        'metodo' => request()->method()
                    ]);
                }
                $entrada->delete();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'entradas_productos',
                    'registro' => $entrada->id . '',
                    'campos' => json_encode($entrada) . '',
                    'metodo' => request()->method()
                ]);
                
                DB::commit();
                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}