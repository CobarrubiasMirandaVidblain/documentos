<?php

namespace Modules\AFuncionales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Models\Persona;
use App\Models\Solicitud;
use App\Models\SolicitudesPersona;
use App\Models\ProgramasSolicitud;
use App\Models\DocumentosPersona;
use App\Models\Limitacion;
use App\Models\Motivo;
use App\Models\SalidasProducto;
use App\Models\DetalleSalidasproductos;
use App\Models\PersonasTipo;
use App\Models\Tipospersona;
use App\Models\Productosfoliados;
use App\Models\Tipossalida;
use App\Models\Tabla;
use App\Models\Modulo;
use View;

class PeticionProgramaBeneficiarioController extends Controller {    

    public function __construct(){
        $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($peticion_id, $programa_id, $persona_id){
        $persona = Persona::find($persona_id);
        
        $beneficio = ProgramasSolicitud::where('solicitud_id',$peticion_id)
            ->where('programa_id', $programa_id)
            ->first();
            
        /*$db_ext = \DB::connection('sqlsrv');
        $historial = $db_ext->table('beneficiarios_funcionales')
                    ->where(function ($query) use ($persona) {
                        $query->where('curp', $persona->curp)
                            ->whereNotNull('curp')
                            ->where('curp', '!=', '');
                    })                    
                    ->orWhere(function ($query) use ($persona) {
                        $query->where('nombre', $persona->nombre)
                            ->where('primer_apellido', $persona->primer_apellido)
                            ->where('segundo_apellido', $persona->segundo_apellido)
                            ->where('fecha_nacimiento', $persona->get_formato_fecha_nacimiento());
                    })->get();*/

        $beneficiario = View::make('afuncionales::peticiones.personas.show')
            ->with("persona", $persona)
            ->with("beneficio", $beneficio);
            //->with("historial", $historial);
        
        $pichon = SolicitudesPersona::where('programas_solicitud_id', $beneficio->id)
                    ->where('persona_id', $persona_id)->first();
        $estatus = null;
        if($pichon){
            $estatus = $pichon->evaluado;
        }
        
        return response()->json([
            'beneficiario' =>  $beneficiario->render(),            
            'estatus' => $estatus //Para saber si puede editar info del solicitante
        ], 200);
    }

    public function showEntrega($peticion_id, $programa_id, $persona_id){
        $persona = Persona::find($persona_id);

        $beneficio = ProgramasSolicitud::where('solicitud_id',$peticion_id)
            ->where('programa_id', $programa_id)
            ->first();

        $solicitud_persona = SolicitudesPersona::where('programas_solicitud_id', $beneficio->id)
                    ->where('persona_id', $persona_id)->first();

        $entrega = View::make('afuncionales::peticiones.personas.entrega')
            ->with("solicitud", $solicitud_persona);
        
        return response()->json([
            'entrega' =>  $entrega->render()
        ], 200);
    }


    //Reactivar solicitante
    public function update($peticion_id, $programa_id, $persona_id, Request $request){        
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $programaSolicitud = ProgramasSolicitud::where("solicitud_id",$peticion_id)
                                                        ->where("programa_id",$programa_id)
                                                        ->first();
                $solicitudPersona = SolicitudesPersona::where("programas_solicitud_id",$programaSolicitud->id)
                                                        ->where("persona_id",$persona_id)
                                                        ->first();                
                $solicitudPersona->entregado = null;
                if($request->tipo == "solicitante"){
                    $this->validarExpediente($programaSolicitud, Persona::find($persona_id));
                    $solicitudPersona->observacionevaluado = null;
                }else{                    
                    $solicitudPersona->observacionentrega = null;
                }
                
                $solicitudPersona->save();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'solicitudes_personas',
                    'registro' => $solicitudPersona->id . '',
                    'campos' => json_encode($solicitudPersona) . '',
                    'metodo' => 'PUT'
                ]);                
                DB::commit();
                return response()->json(['status' => '', 'persona_id' => $persona_id], 200);                               
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    public function entrega($peticion_id, $programa_id, $persona_id, Request $request){
        $programaSolicitud = ProgramasSolicitud::where("solicitud_id",$peticion_id)
                                                ->where("programa_id",$programa_id)
                                                ->first();    
        $solicitudPersona = SolicitudesPersona::where("programas_solicitud_id",$programaSolicitud->id)
                                                ->where("persona_id",$persona_id)
                                                ->first();
            $status = "";
            try{
                DB::beginTransaction();
                if($request->type == "Cancelado") {                    
                    $motivo = Motivo::findOrFail($request->motivo);
                    $solicitudPersona->entregado = false;
                    $solicitudPersona->observacionentrega = $motivo->motivo;
                } elseif($request->type == "Entregado") {
                    $ruta = $request->file('recibo');
                    if($ruta != null) {            
                        $directorio = 'public/comprobantes';
                        $url = Storage::putFile($directorio, $request->file('recibo'));
                        $partes = explode('public', $url);
                        $a = 'storage' . $partes[1];
                        
                        $salida_producto = new SalidasProducto;
                        $salida_producto->fechasalida = (\DateTime::createFromFormat('d-m-Y', $request['fecha']))->format('Y-m-d');
                        $salida_producto->recibo = $a;
                        $salida_producto->tipossalida_id = Tipossalida::where('tipo','DONATIVO')->first()->id;
                        $salida_producto->usuario_id = $request->user()->id;
                        $salida_producto->persona_id = $persona_id;
                        $salida_producto->empleado_id = $request['empleado_id'];                        
                        $salida_producto->save();

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'salidas_productos',
                            'registro' => $salida_producto->id . '',
                            'campos' => json_encode($salida_producto) . '',
                            'metodo' => 'POST'
                        ]);

                        $detalle_salida = DetalleSalidasproductos::create([
                            'salidas_producto_id' => $salida_producto->id,
                            'cantidad' => 1,
                            'areas_producto_id' => $request['areas_producto_id']
                        ]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'detallesalidas_productos',
                            'registro' => $detalle_salida->id . '',
                            'campos' => json_encode($detalle_salida) . '',
                            'metodo' => 'POST'
                        ]);
                        
                        $solicitudPersona->entregado = 1;
                        $solicitudPersona->salidas_producto_id = $salida_producto->id;

                        $productofoliado = Productosfoliados::where('folio', $request['folio'])->first();
                        $productofoliado->update(['detallessalidas_producto_id' => $detalle_salida->id]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'productosfoliados',
                            'registro' => $productofoliado->id . '',
                            'campos' => json_encode($productofoliado) . '',
                            'metodo' => 'PUT'
                        ]);
                        
                        $persona_tipo = PersonasTipo::create([
                            'tipospersona_id' => Tipospersona::firstOrCreate(['tipo' => 'APOYOS FUNCIONALES'])->id,
                            'persona_id' => $persona_id,
                            'tablabd_id' => Tabla::firstOrCreate(['nombre' => 'afuncionalespersonas'])->id,
                            'modulo_id' => Modulo::where('nombre','APOYOS FUNCIONALES')->first()->id
                        ]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'personas_tipos',
                            'registro' => $persona_tipo->id . '',
                            'campos' => json_encode($persona_tipo) . '',
                            'metodo' => 'POST'
                        ]);
                        
                    } else {
                        return response()->json(['status' => 'ARCHIVO', 'persona_id' => $persona_id], 200);
                    }
                }
                $solicitudPersona->save();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'solicitudes_personas',
                    'registro' => $solicitudPersona->id . '',
                    'campos' => json_encode($solicitudPersona) . '',
                    'metodo' => 'PUT'
                ]);                

                //Si todos los beneficiarios ya cuentan con un status entrega
                //Entonces el programa-solicitud pasa a estatus Finalizado                      
                if(DB::table('solicitudes_personas')->where('programas_solicitud_id',$programaSolicitud->id)
                        ->whereNull("entregado")->whereNull("deleted_at")->count() == 0){
                    $this->cambiarEstatus($programaSolicitud, 8, $request); 
                    $status = "FINALIZADO";
                }        
                DB::commit();
            } catch(QueryException $e) {
                DB::rollBack();
              return redirect()->back()->with('status', 'no');
            }
            return response()->json(['status' => $status, 'persona_id' => $persona_id], 200);
        }

    public function comprobante(Request $request){
        return Storage::response("public/".$request->nombre);
    }
    
}