<?php

namespace Modules\AFuncionales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\DataTables\EmpleadosDataTable;
use App\DataTables\Afuncionales\SalidasDataTable;
use App\Models\SalidasProducto;
use App\Models\DetalleSalidasproductos;
use App\Models\Productosfoliados;
use App\Models\Tipossalida;
use App\Models\Empleado;
use View;

class SalidaController extends Controller {

    public function __construct(){
        $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SalidasDataTable $dataTable) {
        return $dataTable->render('afuncionales::productos.salidas.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(EmpleadosDataTable $dataTable) {
        return $dataTable
        ->with('tipo', 'normal')
        ->render('afuncionales::productos.salidas.create_edit', ['tipos_salida' => Tipossalida::get()]);      
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){
        $salida = (array) json_decode($request->salida);
        if($request->ajax()){
          // dd($salida);
            try {
                DB::beginTransaction();
                if($request->has('recibo')) {
                    $directorio = 'public/comprobantes';
                    $url = Storage::putFile($directorio, $request->recibo);
                    $partes = explode('public', $url);
                    $salida["recibo"] = 'storage' . $partes[1];
                }
  
                $salida['persona_id'] = Empleado::find($salida['persona_id'])->persona->id;
                $salida['usuario_id'] = Auth::user()->id;
                $salida['fechasalida'] = (\DateTime::createFromFormat('d-m-Y', $salida['fechasalida']))->format('Y-m-d');          
                $salida_producto = SalidasProducto::create($salida);
                $salida['salidas_producto_id'] = $salida_producto->id;               

                auth()->user()->bitacora(request(), [
                    'tabla' => 'salidas_productos',
                    'registro' => $salida_producto->id . '',
                    'campos' => json_encode($salida_producto) . '',
                    'metodo' => request()->method()
                ]);
            
                foreach (array_keys( (array) $salida['detalle']) as $detalle) {
                    $detalle_salida = DetalleSalidasproductos::create([
                        'salidas_producto_id' => $salida['salidas_producto_id'],
                        'cantidad' => count($salida['detalle']->$detalle),
                        'beneficioprograma_id' => $detalle  
                    ]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'detallessalidas_productos',
                        'registro' => $detalle_salida->id . '',
                        'campos' => json_encode($detalle_salida) . '',
                        'metodo' => request()->method()
                    ]);
  
                    foreach($salida['detalle']->$detalle as $folio){
                        $producto = Productosfoliados::where("folio",$folio)->first();
                        $producto->detallessalidas_producto_id = $detalle_salida->id;
                        $producto->save();
                        
                        auth()->user()->bitacora(request(), [
                            'tabla' => 'productosfoliados',
                            'registro' => $producto->id . '',
                            'campos' => json_encode($producto) . '',
                            'metodo' => 'PUT'
                        ]);
                    }
                }                
                DB::commit();
            } catch(\Exception $e){
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }      
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        return View::make("afuncionales::productos.salidas.show")->with('salida', SalidasProducto::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(EmpleadosDataTable $dataTable, $id) {
        return $dataTable
        ->with('tipo', 'normal')
        ->render('afuncionales::productos.salidas.create_edit', 
        ['tipos_salida' => Tipossalida::get(), 'salida' => SalidasProducto::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request){ 
        $salida = (array) json_decode($request->salida);
        $salida_update = SalidasProducto::find($salida['id']);
  
        foreach (array_keys( (array) $salida['detalle']) as $detalle) {
          foreach($salida['detalle']->$detalle as $key => $folio) {     
            $producto = Productosfoliados::where("folio",$folio)->first();     
            if(DetalleSalidasproductos::find($producto->detallessalidas_producto_id) != null) {
              if(DetalleSalidasproductos::find($producto->detallessalidas_producto_id)->salidas_producto_id == $salida_update->id){            
                unset($salida['detalle']->$detalle[$key]);
              }
            }
          }
        }
        
        if($request->ajax()){
          try {
            DB::beginTransaction();
            if($request->has('recibo')) {
              $url = Storage::putFile('public/recibo', $request->recibo);
              $partes = explode('public', $url);
              $salida_update->recibo = 'storage' . $partes[1];
            }else{
                $salida_update->recibo = '';
            }
            
            $salida_update->empleado_id = $salida['empleado_id'];
            $salida_update->persona_id = Empleado::find($salida['persona_id'])->persona->id;
            $salida_update->usuario_id = Auth::user()->id;
            $salida_update->fechasalida = (\DateTime::createFromFormat('d-m-Y', $salida['fechasalida']))->format('Y-m-d');          
            $salida_update->save();

            auth()->user()->bitacora(request(), [
                'tabla' => 'salidas_productos',
                'registro' => $salida_update->id . '',
                'campos' => json_encode($salida_update) . '',
                'metodo' => request()->method()
            ]);
            
            foreach (array_keys( (array) $salida['detalle']) as $detalle) {
              if(count($salida['detalle']->$detalle) > 0){
                $detalle_salida = DetalleSalidasproductos::create([
                  'salidas_producto_id' => $salida_update->id,
                  'cantidad' => count($salida['detalle']->$detalle),
                  'beneficioprograma_id' => $detalle  
                ]);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'detallessalidas_productos',
                    'registro' => $detalle_salida->id . '',
                    'campos' => json_encode($detalle_salida) . '',
                    'metodo' => 'POST'
                ]);
    
                foreach($salida['detalle']->$detalle as $folio){
                  $producto = Productosfoliados::where("folio",$folio)->first();
                  $producto->detallessalidas_producto_id = $detalle_salida->id;
                  $producto->save();
                  auth()->user()->bitacora(request(), [
                    'tabla' => 'productosfoliados',
                    'registro' => $producto->id . '',
                    'campos' => json_encode($producto) . '',
                    'metodo' => 'PUT'
                ]);
                }
              }            
            }        
  
            DB::commit();
          }catch(\Exception $e){
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
          }
        }      
      }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $salida = SalidasProducto::find($id);
                foreach($salida->detallesalidasproductos as $detalle_salida){
                  foreach($detalle_salida->productosfoliados as $producto_foliado){
                    $producto_foliado->update(['detallessalidas_producto_id' => null]);
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'productosfoliados',
                        'registro' => $producto_foliado->id . '',
                        'campos' => json_encode($producto_foliado) . '',
                        'metodo' => 'POST'
                    ]);
                  }
                  DetalleSalidasproductos::destroy($detalle_salida->id);
                  auth()->user()->bitacora(request(), [
                    'tabla' => 'detallessalidas_productos',
                    'registro' => $detalle_salida->id . '',
                    'campos' => json_encode($detalle_salida) . '',
                    'metodo' => request()->method()
                ]);
                }
                SalidasProducto::destroy($id);
                auth()->user()->bitacora(request(), [
                    'tabla' => 'salidas_productos',
                    'registro' => $salida->id . '',
                    'campos' => json_encode($salida) . '',
                    'metodo' => request()->method()
                ]);
                DB::commit();
                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
  
    public function destroyDetalle(Request $request){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $producto_foliado = Productosfoliados::where('folio', $request->folio)->first();
                $detalle_salida = DetalleSalidasproductos::find($producto_foliado->detallessalidas_producto_id);
                if($detalle_salida->cantidad-1 == 0){
                  DetalleSalidasproductos::destroy($detalle_salida->id);
                  auth()->user()->bitacora(request(), [
                    'tabla' => 'detallessalidas_productos',
                    'registro' => $detalle_salida->id . '',
                    'campos' => json_encode($detalle_salida) . '',
                    'metodo' => request()->method()
                  ]);
                }else{
                  $detalle_salida->update(['cantidad' => $detalle_salida->cantidad-1]);        
                  auth()->user()->bitacora(request(), [
                    'tabla' => 'detallessalidas_productos',
                    'registro' => $detalle_salida->id . '',
                    'campos' => json_encode($detalle_salida) . '',
                    'metodo' => 'POST'
                  ]);                        
                }                
                $producto_foliado->update(['detallessalidas_producto_id' => null]);
                auth()->user()->bitacora(request(), [
                    'tabla' => 'productosfoliados',
                    'registro' => $producto_foliado->id . '',
                    'campos' => json_encode($producto_foliado) . '',
                    'metodo' => 'POST'
                ]);
                DB::commit();
                return response()->json(array('success' => true));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}