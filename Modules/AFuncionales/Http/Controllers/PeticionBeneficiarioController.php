<?php

namespace Modules\AFuncionales\Http\Controllers;

use App\Models\Tabla;

use App\Models\Modulo;
use App\Models\Tipossalida;
use App\Models\PersonasTipo;
use App\Models\Tipospersona;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\SalidasProducto;
use App\Models\Productosfoliados;
use App\Models\PeticionesPersonas;
use App\Models\ProgramasSolicitud;
use App\Models\SolicitudesPersona;
use Illuminate\Support\Facades\DB;
use App\Models\DetalleSalidasproductos;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;
use App\Models\BeneficiosprogramasSolicitud;
use App\Http\Controllers\PeticionBeneficiarioBaseController;

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController
{

  public function __construct()
  {
    parent::__construct('APOYOS FUNCIONALES');
  }

  public function entrega($peticion_id, $persona_id, Request $request)
  {
    // dd($request->all());
    $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
    $persona_peticion = PeticionesPersonas::where('beneficiosprogramas_solicitud_id', $peticion_id)->wherehas('beneficiopersona', function ($q) use ($persona_id) {
      $q->where('persona_id', $persona_id);
    })->first();
    $status = "";
    try {
      DB::beginTransaction();
      if ($request->type == "Cancelado") {
        $motivo = Motivo::findOrFail($request->motivo);
        $persona_peticion->entregado = false;
        $persona_peticion->observacionentrega = $motivo->motivo;
      } elseif ($request->type == "Entregado") {
        $ruta = $request->file('recibo');
        if ($ruta != null) {
          $directorio = 'public/comprobantes';
          $url = Storage::putFile($directorio, $request->file('recibo'));
          $partes = explode('public', $url);
          $a = 'storage' . $partes[1];

          $salida_producto = new SalidasProducto;
          $salida_producto->fechasalida = (\DateTime::createFromFormat('d-m-Y', $request['fecha']))->format('Y-m-d');
          $salida_producto->recibo = $a;
          $salida_producto->tipossalida_id = Tipossalida::where('tipo', 'DONATIVO')->first()->id;
          $salida_producto->usuario_id = $request->user()->id;
          $salida_producto->persona_id = $persona_id;
          $salida_producto->empleado_id = $request['empleado_id'];
          $salida_producto->save();

          auth()->user()->bitacora(request(), [
            'tabla' => 'salidas_productos',
            'registro' => $salida_producto->id . '',
            'campos' => json_encode($salida_producto) . '',
            'metodo' => 'POST'
          ]);

          $detalle_salida = DetalleSalidasproductos::create([
            'salidas_producto_id' => $salida_producto->id,
            'beneficioprograma_id' => $request['apoyo'],
            'cantidad' => 1
          ]);

          auth()->user()->bitacora(request(), [
            'tabla' => 'detallesalidas_productos',
            'registro' => $detalle_salida->id . '',
            'campos' => json_encode($detalle_salida) . '',
            'metodo' => 'POST'
          ]);

          $persona_peticion->entregado = 2;
          $persona_peticion->salidas_producto_id = $salida_producto->id;

          $productofoliado = Productosfoliados::where('folio', $request['folio'])->first();
          $productofoliado->update(['detallessalidas_producto_id' => $detalle_salida->id]);

          auth()->user()->bitacora(request(), [
            'tabla' => 'productosfoliados',
            'registro' => $productofoliado->id . '',
            'campos' => json_encode($productofoliado) . '',
            'metodo' => 'PUT'
          ]);

          $persona_tipo = PersonasTipo::create([
            'tipospersona_id' => Tipospersona::firstOrCreate(['tipo' => 'APOYOS FUNCIONALES'])->id,
            'persona_id' => $persona_id,
            'tablabd_id' => Tabla::firstOrCreate(['nombre' => 'afuncionalespersonas'])->id,
            'modulo_id' => Modulo::where('nombre', 'APOYOS FUNCIONALES')->first()->id
          ]);

          auth()->user()->bitacora(request(), [
            'tabla' => 'personas_tipos',
            'registro' => $persona_tipo->id . '',
            'campos' => json_encode($persona_tipo) . '',
            'metodo' => 'POST'
          ]);
        } else {
          return response()->json(['status' => 'ARCHIVO', 'persona_id' => $persona_id], 200);
        }
      }
      $persona_peticion->save();
      auth()->user()->bitacora(request(), [
        'tabla' => 'solicitudes_personas',
        'registro' => $persona_peticion->id . '',
        'campos' => json_encode($persona_peticion) . '',
        'metodo' => 'PUT'
      ]);

      //Si todos los beneficiarios ya cuentan con un status entrega
      //Entonces el programa-solicitud pasa a estatus Finalizado                      
      if (DB::table('solicitudes_personas')->where('programas_solicitud_id', $peticion->id)
        ->whereNull("entregado")->whereNull("deleted_at")->count() == 0
      ) {
        $this->cambiar_estatus($peticion, 8, $request);
        $status = "FINALIZADO";
      }
      DB::commit();
    } catch (QueryException $e) {
      DB::rollBack();
      return response()->json(['status' => $status, 'message' => $e->getMessage(), 'persona_id' => $persona_id], 500);//redirect()->back()->with('status', 'no');
    }
    return response()->json(['status' => $status, 'persona_id' => $persona_id], 200);
  }

  private function cambiar_estatus($peticion, $estatus_id, Request $request)
  {
    if (!$peticion->estadosSolicitud->contains('statusproceso_id', $estatus_id)) {
      $peticion->estadosSolicitud()->create([
        'statusproceso_id' => $estatus_id,
        'usuario_id' => $request->user()->id
      ]);

      auth()->user()->bitacora(request(), [
        'tabla' => 'status_solicitudes',
        'registro' => $peticion->estadosSolicitud()->latest()->first()->id . '',
        'campos' => json_encode($peticion->estadosSolicitud()->latest()->first()) . '',
        'metodo' => 'POST'
      ]);
    }
  }

}
