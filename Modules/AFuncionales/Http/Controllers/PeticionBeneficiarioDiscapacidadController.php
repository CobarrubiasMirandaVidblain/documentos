<?php

namespace Modules\AFuncionales\Http\Controllers;

use View;
use App\Models\Persona;
use App\Models\Limitacion;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Afuncionalessilla;
use App\Models\Beneficiosprograma;
use App\Models\ProgramasSolicitud;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Afuncionalespersona;
use App\Models\BeneficiosprogramasSolicitud;

class PeticionBeneficiarioDiscapacidadController extends Controller {

    public function store($peticion_id, $persona_id, Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $persona = Persona::findOrFail($persona_id);
                $request['usuario_id'] = $request->user()->id;
                $request['persona_id'] = $persona->id;
                $persona_funcional = Afuncionalespersona::create($request->all());
                auth()->user()->bitacora(request(), [
                    'tabla' => 'afuncionalespersonas',
                    'registro' => $persona_funcional->id . '',
                    'campos' => json_encode($persona_funcional) . '',
                    'metodo' => request()->method()
                ]);
                if($request->has('tiposilla')) {
                    $request['usuario_id'] = $request->user()->id;
                    $request['afuncionalespersona_id'] = $persona_funcional->id;
                    $persona_silla = Afuncionalessilla::create($request->all());
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'afuncionales_sillas',
                        'registro' => $persona_silla->id . '',
                        'campos' => json_encode($persona_silla) . '',
                        'metodo' => request()->method()
                    ]);
                }
                DB::commit();
                return response()->json(['status' => 'ok', 'persona_id' => $persona->id], 200);
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function edit($peticion_id, $persona_id) {
        $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
/*         if($peticion->programa->padre->nombre != 'APOYOS FUNCIONALES'){
            abort(403,'Permisos insuficientes');
        } */
        $persona = Persona::findOrFail($persona_id);
        $sillas = Beneficiosprograma::whereHas('anioprograma',function($q){
          $q->whereHas('programa',function($qu){
            $qu->where('nombre','like','%silla%')->whereHas('padre',function($que){
              $que->where('nombre','like','%trabajo social%');
            });
          });
        })->get(['id',DB::raw('REPLACE(beneficiosprogramas.nombre,"ENTREGA DE ","") as nombre')]);
        // dd($sillas);
        $discapacidad = View::make('afuncionales::peticiones.personas.discapacidad')
            ->with("peticion", $peticion)
            ->with("persona", $persona)
            ->with("sillas", $sillas);

        return response()->json([
            'discapacidad' => $discapacidad->render()
        ], 200);
    }

    public function update($peticion_id, $persona_id, Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $persona_funcional = Afuncionalespersona::where('persona_id', $persona_id)->first();
                $persona_funcional->limitacion_id = $request->limitacion_id;
                $persona_funcional->discapacidad_id = $request->discapacidad_id;
                $persona_funcional->tiempodiscapacidad = $request->tiempodiscapacidad;
                $persona_funcional->usuario_id = $request->user()->id;
                $persona_funcional->save();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'afuncionalespersonas',
                    'registro' => $persona_funcional->id . '',
                    'campos' => json_encode($persona_funcional) . '',
                    'metodo' => request()->method()
                ]);
                
                if($request->has('tiposilla')) {
                    $request['usuario_id'] = $request->user()->id;
                    Afuncionalessilla::updateOrCreate(
                        ['afuncionalespersona_id' => $persona_funcional->id], $request->all()
                    );                    
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'afuncionales_sillas',
                        'registro' => $persona_funcional->afuncionalessilla->id . '',
                        'campos' => json_encode($persona_funcional->afuncionalessilla) . '',
                        'metodo' => request()->method()
                    ]);
                }               
            
                DB::commit();
                return response()->json(['status' => 'ok', 'persona_id' => $persona_funcional->persona->id], 200);
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}
