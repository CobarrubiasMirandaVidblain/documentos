<?php

Route::group(['middleware' => 'web', 'prefix' => 'ubr', 'namespace' => 'Modules\UBR\Http\Controllers'], function() {
  Route::get('/', 'PeticionController@index');
  Route::resource('peticiones', 'PeticionController',['as'=>'ubr','only'=>['index','show','update','destroy']]);   //Peticiones
  Route::resource('programas', 'ProgramaController',['as'=>'ubr']);                                                //Programas
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'ubr' ]);                                //Beneficiarios
});