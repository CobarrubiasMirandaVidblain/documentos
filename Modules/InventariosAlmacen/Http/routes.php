<?php

Route::group(['middleware' => 'web', 'prefix' => 'inventariosalmacen', 'namespace' => 'Modules\InventariosAlmacen\Http\Controllers'], function()
{
    Route::get('/', 'InventariosAlmacenController@index');
});
