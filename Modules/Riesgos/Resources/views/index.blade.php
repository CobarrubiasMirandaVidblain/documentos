@extends('riesgos::layouts.master')
@php
    $regiones = (array)json_decode($regiones);
    $regiones["CAÑADA"]->color           = '#57AA65';//'#F00000';
    $regiones["COSTA"]->color            = '#693C6B';//'#FF35E2';
    $regiones["ISTMO"]->color            = '#2EBAB0';//'#2900FF';
    $regiones["MIXTECA"]->color          = '#AD8E05';//'#0096A3';
    $regiones["PAPALOAPAN"]->color       = '#B46F14';//'#0A9D18';
    $regiones["SIERRA NORTE"]->color     = '#EF0008';//'#ADA904';
    $regiones["SIERRA SUR"]->color       = '#D0316E';//'#024705';
    $regiones["VALLES CENTRALES"]->color = '#5A118F';//'#5A118F';
    $riesgos = (array)json_decode($riesgos);
    $riesgosNombres = array_keys($riesgos);
    $coloresRiesgos = array('#D0316E','#693C6B','#2EBAB0','#AD8E05','#B46F14','#EF0008','#57AA65');
    //$coloresRiesgos = array('&00FFCD','&D6CC00','&0036FF','&0065ED','&B37F00','&Ff0000','&1CF500');
    /* for ($i=0; $i < count($riesgosNombres); $i++) {
      $riesgos[$riesgosNombres[$i]]->color = $coloresRiesgos[$i];
    } */
    $riesgos["SISMOS"]->estado->todo = false;
    //dd($regiones);
@endphp
@push('head')
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <style type="text/css">
    .center-block {
      display: block;
      margin-right: auto;
      margin-left: auto;
      margin-top: auto;
    }
    .map-control{
      background: transparent;
      border: 0px;
    }
    .checkbox{
      margin-top: 5px;
      margin-bottom: 5px;
    }
    .btn{
      margin-bottom: 3px;
    }
    .text-white{
      color : white;
    }
    .dropdown-submenu {
      position: relative;
    }

    .dropdown-submenu .dropdown-menu {
      top: 0;
      left: 100%;
      margin-top: -1px;
    }
    .dt-button-collection{
      background: transparent !important;
      border: 0px !important;
      border-top-style: none !important;
      box-shadow: none !important;
    }
    .acotacion{
      color: white;
      margin-left: 20px;
      margin-bottom: 10px;
      font-size: larger;
      border-radius: 4px;
    }
  </style>
@endpush
@section('content-subtitle','MAPA DE MUNICIPIOS')
@section('content')
  <div id="app" class="box box-primary">
    <div class="box-header with-border">
      {{-- <h3 class="box-title">{{ }}</h3> --}}
    </div>
    <div class="box-body shadow" style="WIDTH:100%">
      <div id="map" class="center-block" style="height: 830px;"></div>
      <hr>
      <div>
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
          <input type="text" id="search" name="search" class="form-control">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
          </span>
        </div>
        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable', 'id' => 'riesgos']) !!}
      </div>
    </div>
    <div style="display:none">
      <div id="controles" class="row" style="margin: 10px 0 0 0 ;width:30%">
        <div class="col-xs-6">
          <button class="btn btn-default dropdown-toggle" style="height:40px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Ver">
            <i class="fa fa-eye"></i><span class="caret"></span>
          </button>
          <ul class="dropdown-menu map-control">
            <template v-for ="(riesgo, key) in riesgos">
              <template v-if="key == 'SISMOS'">
                <li class="dropdown-submenu">
                  <button class="btn btn-xs text-white test" style="background:#AAAAAA80;"> Corredores sísmicos <span class="caret"></span></button>
                  <ul class="dropdown-menu map-control" style="minWidth:250px">
                    <li><button @click.prevent="municipiosRiesgos(key+'.todo')" class="btn btn-xs text-white" :style="`font-size:16px; background : ${riesgo.estado.todo ? 'green' : '#AAAAAA80'}`">Todos</button></li>
                    <li v-for = "(corredor, ky) in riesgo.municipios">
                      <button @click.prevent="municipiosRiesgos(key+'.'+ky)" class="btn btn-xs text-white" :style="`font-size:16px; background : ${riesgo.estado[ky] ? 'green' : '#AAAAAA80'}`">@{{ ky }}</button>
                    </li>
                  </ul>
                </li>
              </template>
              <template v-else>
                <li>
                  <button v-if="key != 'SISMOS'" @click.prevent="municipiosRiesgos(key+'.'+key)" class="btn btn-xs text-white" :style="`font-size:16px; background : ${riesgo.estado ? 'green' : '#AAA'}`">@{{ key }}</button>
                </li>
              </template>
            </template>
          </ul>
        </div>
        <div v-show="riesgos.HELADAS.estado" class="col-xs-6" style="display:flex;justify-content:space-between;align-items:center;font-size:15px;background:rgba(255, 255, 255, 0.75);color:black;">
          <input v-model="mostrarDelegaciones" type="checkbox" name="" id="calidez">
          <label for="calidez" >Mostrar calidez En Familia</label>
        </div>
      </div>
      <div id="etiquetas" style="padding: 7px">
        <button class="dropdown-toggle" style="font-size:16px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Detalles"> @{{ mostrando }}</button>
        <ul class="dropdown-menu map-control">
          <li>
            <template v-for="(region, key) in control">
              <label class="btn btn-xs acotacion" :style="`background:${region.color}80;font-size:16px;`">&nbsp;@{{ key }}&nbsp; @{{ conta[key] > 0 ? '[M:'+Object.keys(region.municipios).length+', R:'+conta[key]+', C:'+conve[key]+']' : '' }}</label>
              <br>
            </template>
          </li>
          <label class="acotacion" style="background:#FFF80;"> M: MUNICIPIOS TOTALES</label><br>
          <label class="acotacion" style="background:#FFF80;"> R: MUNICIPIOS EN RIESGO</label><br>
          <label class="acotacion" style="background:#FFF80;"> C: MUNICIPIOS CON SUBCOMITE APCE</label>
        </ul>
      </div>
      {{-- <div id="acotaciones">
        <template v-for="(region, key) in control">
          <label class="acotacion" :style="`background:${region.color}80;font-size:16px;`">&nbsp;@{{ key }}&nbsp; @{{ conta[key] > 0 ? '[M:'+Object.keys(region.municipios).length+', R:'+conta[key]+', C:'+conve[key]+']' : '' }}</label>
          <br>
        </template>
        <label class="acotacion" style="background:#FFF80;"> M: MUNICIPIOS TOTALES</label><br>
        <label class="acotacion" style="background:#FFF80;"> R: MUNICIPIOS EN RIESGO</label><br>
        <label class="acotacion" style="background:#FFF80;"> C: MUNICIPIOS CON SUBCOMITE APCE</label>
      </div> --}}
    </div>
  </div>
@stop
@push('body')
{{-- <script type="text/javascript" src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDpYJe13cGrxH5Hk-9u5fu4PFQI-YMD5M&libraries=visualization"></script>

<script type="text/javascript" src="{{ asset('js/oax_map.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/coordMunicipios.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vue.js') }}"></script>

<script type="text/javascript">

  var app = new Vue({
    el   : "#app",
    data : {
      control     : @json($regiones),
      map         : null,
      oaxaca      : null,
      municipios  : [],
      riesgos     : @json($riesgos),
      mostrando   : "",
      conta       : {},
      conve       : {},
      munsDelegaciones : [19,220,318,306,151,256,283,464,488,548,242,93,106,121,221,321,332,339,341,346,479,518,521,536,540,547,556,54,96,140,147,215,224,250,264,270,281,304,331,395,404,463,492,493,523,562,26,110,133,172,210,239,240,297,317,370,371,372,383,397,408,480,500,173,262,458,496,504,377,535,95,148,211,236,347,391,424,512,74,428,487,494,108,273,388],
      mostrarDelegaciones : false
    },
    watch:{
      mostrarDelegaciones(val){
        this.munsDelegaciones.forEach(m=>m.setMap(val?this.map:null))
      }
    },
    methods :{
      initMap (){
        this.map = new google.maps.Map(document.getElementById('map'), {
          zoom   : 8.33,
          center : {lat : 17.15727971021128, lng: -96.23866181640627 },
          minZoom: 8.33,
          styles : [
            {
              "featureType": "administrative.country",
              "elementType": "geometry.fill",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.country",
              "elementType": "geometry.stroke",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.country",
              "elementType": "labels.text",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.province",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.province",
              "elementType": "geometry.fill",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.locality",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.locality",
              "elementType": "labels",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "administrative.neighborhood",
              "elementType": "labels.text",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "landscape",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "landscape",
              "elementType": "geometry.stroke",
              "stylers": [
              {
                "visibility": "off"
              },
              {
                "saturation": "1"
              }
              ]
            },
            {
              "featureType": "landscape.man_made",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "landscape.man_made",
              "elementType": "labels",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "landscape.natural",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "landscape.natural",
              "elementType": "geometry.fill",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "landscape.natural",
              "elementType": "labels",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "road",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "transit",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "off"
              }
              ]
            },
            {
              "featureType": "water",
              "elementType": "all",
              "stylers": [
              {
                "visibility": "on"
              }
              ]
            }
          ],
          disableDefaultUI: true,
          mapTypeControl: false,
          zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
          },
          fullscreenControlOptions:{
            position: google.maps.ControlPosition.TOP_RIGHT
          },
          scaleControl: false,
          streetViewControl: false,
          rotateControl: false,
          fullscreenControl: true,
          zoomControl: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
      },
      limitarOax (){
        let coordmapini=[
          {lat: this.map.getBounds().getSouthWest().lat() ,lng: this.map.getBounds().getSouthWest().lng() },//{lat:15.258438722900348,lng:-99.75428681640625},
          {lat: this.map.getBounds().getSouthWest().lat() ,lng: this.map.getBounds().getNorthEast().lng() },//{lat:15.258438722900348,lng:-92.72303681640625},
          {lat: this.map.getBounds().getNorthEast().lat() ,lng: this.map.getBounds().getNorthEast().lng() },//{lat:19.03689065576206,lng:-92.72303681640625},
          {lat: this.map.getBounds().getNorthEast().lat() ,lng: this.map.getBounds().getSouthWest().lng() },//{lat:19.03689065576206,lng:-99.75428681640625}
        ];
        //dibujamos oaxaca
        this.oaxaca = new google.maps.Polygon({
          paths: [coordmapini,oaxCoord],
          strokeColor: '#FFF',
          strokeOpacity: .5,
          strokeWeight: 3,
          fillColor: '#000000',
          fillOpacity: 1,
          map : this.map
        });
        //configurando lis limites solo 1 vez cuadndo se termina de dibujar
        google.maps.event.addListenerOnce(this.map,'idle', () => {
          limit = this.borders();
        });
        //configurar el zoom minimo cada vez que se entra o sale de pantalla comlpeta
        document.addEventListener("fullscreenchange", this.recentrar);       //Estandar (jajajajajajaja)
        document.addEventListener("webkitfullscreenchange", this.recentrar); //Webkit (Safari, Chrome y Opera 15+)
        document.addEventListener("mozfullscreenchange", this.recentrar);    //Firefox
        document.addEventListener("MSFullscreenChange", this.recentrar);     //Internet Explorer 11+
        //al alejar el mapa y ver todo oaxaca, centrar solo oaxaca
        google.maps.event.addListener(this.map,'zoom_changed', () => {
          if(this.map.getZoom() == 9){
            this.map.setCenter({lat : 17.15727971021128, lng: -96.22866181640627 });
            this.map.setZoom(8.5);
          }
        });
        //revisar que no se salga
        google.maps.event.addListener(this.map,'drag',() => {
          if(this.map.getZoom() < 9)
            this.map.setCenter(limit.center);
          else{
            current = this.borders();
            if( current.maxLng <= limit.maxLng && current.minLng >= limit.minLng )
              activeCenterLng = current.center.lng();
            else
              this.map.setCenter(
                new google.maps.LatLng(
                  activeCenterLat,
                  activeCenterLng
                )
              );
            if( current.maxLat <= limit.maxLat && current.minLat >= limit.minLat )
              activeCenterLat = current.center.lat();
            else
            this.map.setCenter(
                new google.maps.LatLng(
                  activeCenterLat,
                  activeCenterLng
                )
              );
          }
        });
      },
      pintarRegiones (){
        for(var regionNombre in this.control) {
          var region = this.control[regionNombre];
          for (var munId in region.municipios) {
            //generando un poligono con el contorno del municipio}
            var aux = new google.maps.Polygon({
              paths: coordMunicipios[munId],
              strokeColor: region.color,
              strokeOpacity: .5,
              strokeWeight: .5,
              fillColor: region.color,
              fillOpacity: .15,
              map : this.map,
              id : parseInt(munId)
            });
            //asignando region a poligono
            aux.region = regionNombre;
            //colocando nombre y centro a poligonos
            this.asignaInfoWindowToPoligon(aux,region.municipios[munId]);
            //relacionando poligonos a regiones
            this.municipios.push(aux)//[munId] = aux;
            this.conta[regionNombre]=0;
            this.conve[regionNombre]=0;
          }
        }
      },
      borders(){
        return {
          maxLat : this.map.getBounds().getNorthEast().lat(),
          maxLng : this.map.getBounds().getNorthEast().lng(),
          minLat : this.map.getBounds().getSouthWest().lat(),
          minLng : this.map.getBounds().getSouthWest().lng(),
          center : this.map.getCenter()
        }
      },
      recentrar(){
        if ( $(this.map.getDiv()).children().eq(0).height() == window.innerHeight && $(this.map.getDiv()).children().eq(0).width()  == window.innerWidth )
          this.map.setOptions({minZoom : 8.8})
        else
          this.map.setOptions({minZoom : 8.33})
      },
      initControls() {
        this.map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.querySelector('#controles'));
        this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.querySelector('#etiquetas'));
        this.map.controls[google.maps.ControlPosition.LEFT_CENTER].push(document.querySelector('#acotaciones'));
      },
      reiniciaConta(){
        for (const region in this.conta) {
          this.conta[region] = 0;
          this.conve[region] = 0;
        }
      },
      pintaMunicipios(muns,bandera,color){
        for (var i = 0; i < muns.length; i++) {
          if(bandera){
            this.conta[this.municipios[muns[i]].region]++;
          }
          let colorRegion = this.control[this.municipios[muns[i]].region].color;
          this.municipios[muns[i]].obj.setOptions({
            fillColor   : bandera ? color : colorRegion,
            fillOpacity : bandera ? .75 : .15
          });
          this.municipios[muns[i]].obj.showInfoWindow = bandera;
        }
      },
      toggleMunicipios(etiqueta){
        let riesgo = etiqueta.split('.');
        this.mostrando = riesgo[1];
        if(riesgo[0]=='SISMOS'){
          this.reiniciaConta();
          for (const nombre in this.riesgos) {
            if(this.riesgos[nombre].estado){
              /* if (nombre == 'SISMOS'){
                for (const corredor in this.riesgos[nombre].estado) {
                  this.riesgos[nombre].estado[corredor]=false;
                  if(corredor!='todo'){
                    this.riesgos[nombre].estado[corredor] = this.riesgos[riesgo[0]].estado[riesgo[1]];
                    this.pintaMunicipios(this.riesgos[nombre].municipios[corredor],this.riesgos[nombre].estado[corredor],this.riesgos[nombre].color);
                  }
                }
              }else{ */
                if ( nombre != riesgo[0])
                  this.riesgos[nombre].estado = false;
                this.pintaMunicipios(this.riesgos[nombre].municipios,this.riesgos[nombre].estado,this.riesgos[nombre].color);
              //}
            }
          }
          this.riesgos[riesgo[0]].estado[riesgo[1]]=!this.riesgos[riesgo[0]].estado[riesgo[1]];
          if(riesgo[1]=='todo'){
            for (const corredor in this.riesgos[riesgo[0]].estado) {
              if(corredor!='todo'){
                this.riesgos[riesgo[0]].estado[corredor] = this.riesgos[riesgo[0]].estado[riesgo[1]];
                this.pintaMunicipios(this.riesgos[riesgo[0]].municipios[corredor],this.riesgos[riesgo[0]].estado[riesgo[1]],this.riesgos[riesgo[0]].color);
              }
            }
          }else{
            this.pintaMunicipios(this.riesgos[riesgo[0]].municipios[riesgo[1]],this.riesgos[riesgo[0]].estado[riesgo[1]],this.riesgos[riesgo[0]].color);
          }
        }else if (riesgo[0].indexOf('APCE') < 0 ) {
          this.reiniciaConta();
          this.riesgos[riesgo[0]].estado = !this.riesgos[riesgo[0]].estado;
          if(!this.riesgos[riesgo[0]].estado)
            this.pintaMunicipios(this.riesgos[riesgo[0]].municipios,this.riesgos[riesgo[0]].estado,this.riesgos[riesgo[0]].color);
          for (const nombre in this.riesgos) {
            if(this.riesgos[nombre].estado){
              if (nombre == 'SISMOS'){
                for (const corredor in this.riesgos[nombre].estado) {
                  this.riesgos[nombre].estado[corredor]=false;
                  if(corredor!='todo')
                    this.pintaMunicipios(this.riesgos[nombre].municipios[corredor],this.riesgos[nombre].estado[corredor],this.riesgos[nombre].color);
                }
              }else{
                if ( nombre != riesgo[0])
                  this.riesgos[nombre].estado = false;
                this.pintaMunicipios(this.riesgos[nombre].municipios,this.riesgos[nombre].estado,this.riesgos[nombre].color);
              }
            }
          }
          //this.pintaMunicipios(this.riesgos[riesgo[0]].municipios,true,this.riesgos[riesgo[0]].color);
        } else {
          this.riesgos[riesgo[0]].estado = !this.riesgos[riesgo[0]].estado;
          this.pintaMunicipios(this.riesgos[riesgo[0]].municipios,this.riesgos[riesgo[0]].estado,this.riesgos[riesgo[0]].color);
          this.reiniciaConta();
        }
      },
      findMunicipioEstado(munId){
        for (const nomRiesgo in this.riesgos) {
          const riesgo = this.riesgos[nomRiesgo];
          if(Array.isArray(riesgo.municipios) && riesgo.estado)
            return riesgo.municipios.filter(m=>m==munId).length>0//return riesgo.municipios.indexOf(`${munId}`)>=0;
          else
            for (const nomCorredor in riesgo.municipios) {
              if (riesgo.municipios.hasOwnProperty(nomCorredor) && riesgo.estado[nomCorredor]) {
                if (riesgo.municipios[nomCorredor].indexOf(`${munId}`)>=0)
                  return true;
              }
            }
        }
        return false;
      },
      municipiosRiesgos(etiqueta){
        this.reiniciaConta();
        nomRiesgoAct = etiqueta.split('.');
        $('#riesgos').DataTable().search(nomRiesgoAct[0]).draw();
        //poner todo en false, menos lo mandado
        for (const riesgo in this.riesgos) { //recorrer los riesgos
            console.log('vuelta:',riesgo)
          if (this.riesgos.hasOwnProperty(riesgo) && (riesgo != nomRiesgoAct[0] || nomRiesgoAct[0]=='SISMOS' )) { //descartar el riesgo actual
            const element = this.riesgos[riesgo];
            console.log(element)
            if(!Array.isArray(element.municipios)){//para el caso de sismos
              for (const corredor in element.estado) {//recorremos los corredores
                if (element.estado.hasOwnProperty(corredor) && corredor!=nomRiesgoAct[1] && nomRiesgoAct[1]!='todo') {
                  element.estado[corredor] = false//colocamos en false todo
                }
              }
            }else
              element.estado = false;            //colocamos false en todo
          }
        }
        //cambiar lo mandado a true o false, segun corresponda
        var riesgoActual = this.riesgos[nomRiesgoAct[0]]

        if (!Array.isArray(riesgoActual.municipios)) {
          if(nomRiesgoAct[1]=="todo"){
            for (const nc in riesgoActual.estado) {
              if (riesgoActual.estado.hasOwnProperty(nc)) {
                riesgoActual.estado[nc] = !riesgoActual.estado[nc];
              }
            }
          }else
            riesgoActual.estado[nomRiesgoAct[1]]=!riesgoActual.estado[nomRiesgoAct[1]];
        }else{
          riesgoActual.estado=!riesgoActual.estado;
        }
        //pintar
        this.municipios.forEach(mun => {
          bandera = this.findMunicipioEstado(mun.id);
          if (bandera) this.conta[mun.region]++;
          mun.showInfoWindow = bandera;
          mun.setOptions({
            fillOpacity : bandera ? .5 : .13
          });
        });
        //remarcar apce
        this.riesgos['CONVENIOS APCE'].municipios.forEach(munId => {
          var poligonAc = this.municipios.filter(m=>m.id==munId)[0]//[element];
          if (poligonAc.showInfoWindow) this.conve[poligonAc.region]++;
          poligonAc.setOptions({
            fillOpacity : poligonAc.showInfoWindow ? 1 : poligonAc.fillOpacity,
            strokeColor : poligonAc.showInfoWindow ? '#FFF' : this.control[poligonAc.region].color,
            strokeWeight : poligonAc.showInfoWindow ? 1 : .5
            //strokeOpacity : poligonAc.showInfoWindow ? .1 : .5,
          });
        });
        var conve = 0, riesgo = 0;
        for (const num in this.conta) {
          if (this.conta.hasOwnProperty(num)) {
            riesgo += this.conta[num];
          }
        }
        for (const num in this.conve) {
          if (this.conve.hasOwnProperty(num)) {
            conve += this.conve[num];
          }
        }
        this.mostrando = `MUNICIPIOS CON RIESGO: ${riesgo}, MUNICIPIOS CON SUBCOMITE APCE: ${conve}`;
      },
      asignaInfoWindowToPoligon(polygon, data){
        polygon.infoWindow = new google.maps.InfoWindow({
          content: data.nombre,
        });
        polygon.centro = {lat:parseFloat(data.lat),lng:parseFloat(data.lng)};
        polygon.showInfoWindow = false;
        google.maps.event.addListener(polygon, 'mouseover', function(e) {
          if (this.showInfoWindow) {
            polygon.infoWindow.setPosition(this.centro);
            /* var bounds = new google.maps.LatLngBounds()
            this.getPath().forEach(function(element,index){bounds.extend(element)})
            polygon.infoWindow.setPosition(bounds.getCenter()); */
            polygon.infoWindow.open(this.map);
          }
        });
        google.maps.event.addListener(polygon, 'mouseout', function() {
          polygon.infoWindow.close();
        });
      },
      tabla () {
        var tabla = $('#riesgos'),
        btn_buscar = $('#btn_buscar'),
        search = $('#search');

        search.keypress(function(e) {
          if(e.which === 13) {
            tabla.DataTable().search(search.val()).draw();
          }
        });

        btn_buscar.on('click', function() {
          tabla.DataTable().search(search.val()).draw();
        });
      },
      calidezEnFamilia(){
        var tamañoMarkers = 24;
        var icon = new google.maps.MarkerImage(
          "{{ asset('images/R001.png') }}",
          //new google.maps.Size(tamañoMarkers,tamañoMarkers*2),
          //new google.maps.Point(0,0),
          //new google.maps.Point((tamañoMarkers/2),(tamañoMarkers)),
          //new google.maps.Size(tamañoMarkers,tamañoMarkers*2)
        );
        this.munsDelegaciones = this.munsDelegaciones.map(item=>
          new google.maps.Marker({
            position: this.municipios.filter(m=>m.id==item)[0].centro,
            icon: icon,
            //map:this.map,//this.adapterDelegaciones,
            //title: 'Calidez en familia',
            zindex: 11101
          })
        )
      }
    },
    mounted (){
      this.initMap();
      google.maps.event.addListenerOnce(this.map, 'bounds_changed', () => {
        this.limitarOax();
        this.pintarRegiones();
        this.initControls();
        this.calidezEnFamilia()
      });
      this.tabla();
      $('.dropdown-menu button').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
      });
    }
  });

</script>
{!! $dataTable->scripts() !!}
@endpush
