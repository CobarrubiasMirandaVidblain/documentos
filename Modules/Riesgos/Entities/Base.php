<?php

namespace Modules\Riesgos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class base extends Model{
  use SoftDeletes;

  protected $dates = ['deleted_at'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
}
