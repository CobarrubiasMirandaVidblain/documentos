<?php

namespace Modules\Riesgos\Entities;

use Modules\Riesgos\Entities\Base;

class CorredorSismico extends Base{

  protected $table = 'ries_cat_corredoressismicos';

  protected $fillable = ['nombre'];

  public function municipiosriesgocorredor()
  {
    return $this->hasMany('Modules\Riesgos\Entities\MunicipioCorredorSismico','corredor_id','id');
  }
}
