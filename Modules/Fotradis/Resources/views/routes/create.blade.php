@extends('fotradis::layouts.master')

<style type="text/css">
    #parada_map {
         height: 100%;
    }
</style>
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ isset($ruta) ? 'EDITAR' : 'AGREGAR' }} RUTA <strong>{{ isset($ruta) ? ' | ' . $ruta->nombre : '' }}</strong></h3>
  </div>
  <div class="box-body">
    @include('fotradis::routes.partials.visualizar_ruta')
    @include('fotradis::routes.partials.adm_paradas')
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <div class="pull-right">
      <a href="{{ route('rutas.index') }}" class="btn btn-warning"><i class="fa fa-reply"></i> Listo</a>
    </div>
  </div>
</div>

<form id="form_agregar_parada" method="POST" action="#">
    <!-- Modal para entregar apoyo-->
    <div class="modal fade" id="modal_agregar_parada" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header btn-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">Actualizando paradas de ruta {{ isset($ruta->nombre) ? $ruta->nombre : '' }}</h4></center>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="ubicar_parada">Dirección (Ubicacion):</label>
              <div class="input-group input-group-sm">
                <input type="text" class="form-control" id="ubicar_parada" name="ubicar_parada" value="" />
                <span class="input-group-btn">
                    <button type="button" class="btn btn-info btn-flat" onclick="localizar_parada('add');"><i class="fa fa-fw fa-map-marker"></i></button>
                    <button type="button" class="btn btn-warning btn-flat" onclick="localizar_parada('edit');"><i class="fa fa-fw fa-map-marker"></i></button>
                </span>
              </div>
            </div>

            <div class="form-group">
              <label for="nombre_parada">Nombre:</label>
              <input type="text" class="form-control" id="nombre_parada" name="nombre_parada" value=""/>
            </div>

            {{-- <div class="form-group">
              <label for="referencia_parada">Referencia de ubicación:</label>
              <input type="text" class="form-control" id="referencia_parada" name="referencia_parada" value=""/>
            </div> --}}

            <div class="form-group">
              <label for="numeroestacion_parada">Número de estación:</label>
              <input type="text" class="form-control" id="numeroestacion_parada" name="numeroestacion_parada" value=""/>
            </div>

            <div class="form-group">
              <label for="numeroestacion_parada">Tipo:</label>
              <select type="text" class="form-control" id="tipo" name="tipo" value="">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </select>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="inicializar_form_parada();">Cerrar</button>
            <button type="button" id="boton_enviar_parada" class="btn btn-primary pull-rigth" data-toggle="modal">Listo</button>
          </div>
        </div>
      </div>
    </div>
</form>

<div class="modal fade" id="modal_ver_parada">
    <div class="modal-header btn-default">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">Ubicación parada</h4></center>
          </div>
      <div id="body_modal_ver_parada" class="modal-body">
        <div id="parada_map"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

@include('fotradis::routes.partials.modal_nombre_ruta')
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

@include('fotradis::routes.js.rutas')
<script>
console.log("...........");
console.log("{{$ruta}}");
</script>

@endpush