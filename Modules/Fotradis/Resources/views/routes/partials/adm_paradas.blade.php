<div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-body">
        <h3 class="box-title">Paradas de ruta</h3>
        <div class="box-header with-border">
          <button type="button" class="btn btn-success pull-right" onclick="lanzar.mensaje_de_alerta(0,'agregar_parada')"><i class="fa fa-plus"></i> Agregar parada</button>
          <button type="button" class="btn btn-info pull-left" onclick="nombre_ruta({{ isset($ruta->id) ? $ruta->id : '' }}, 'edit')" {{ isset($en_asignacion) ? 'disabled' : '' }}><i class="fa fa-plus"></i> Cambiar nombre de ruta</button>
        </div>

        <div class="box-body">
          <!-- /.box-header -->
          <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
            <input type="text" id="search" name="search" class="form-control">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
            </span>
          </div>
          {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'paradas']) !!}
        </div>
      </div>
    </div>
  </div>
</div>