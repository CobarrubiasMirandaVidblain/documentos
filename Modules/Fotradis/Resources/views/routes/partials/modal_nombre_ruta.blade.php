<div class="modal fade" id="modal_nombre_ruta">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <center><h4 class="modal-title">Nombre ruta</h4></center>
      </div>
      <div class="modal-body">
        <form id="form_nombre_ruta">
          <div class="form-group">
            <label for="nombre_ruta">Nombrar ruta:</label>
            <input type="text" class="form-control" id="nombre_ruta" name="nombre_ruta"/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-info pull-rigth" id="button_modal_nombre_ruta">Listo</button>
      </div>
    </div>
  </div>
</div>