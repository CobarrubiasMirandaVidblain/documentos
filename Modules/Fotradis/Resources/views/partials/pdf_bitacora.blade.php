<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bitácora unidad</title>
    <link rel="stylesheet" href="{{ asset('reportes/bootstrap.min.css') }}" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .container {
            margin-left: 0px;
            margin-right: 15px;
            width: 100% !important;
        }
        
        .row {
            margin-right: 0px; 
            margin-left: 0px; 
        }

        .logo_header {
            height: 100px;
            width: 120px;
        }
        
        #foto {
            height: 500px;
            width: 600px;
        }

        table {
            text-align:center;
            width: 100%;
        }
    </style>
</head>
<body>
    @include('fotradis::partials.tablas_bitacora')
</body>