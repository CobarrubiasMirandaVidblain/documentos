<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bitácora unidad</title>
    <link rel="stylesheet" href="{{ asset('reportes/bootstrap.min.css') }}" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <style>
        .container {
            margin-left: 0px;
            margin-right: 15px;
            width: 100% !important;
        }
        
        .row {
            margin-right: 0px; 
            margin-left: 0px; 
        }

        .logo_header {
            height: 100px;
            width: 120px;
        }
        
        #foto {
            height: 500px;
            width: 600px;
        }

        table {
            text-align:center;
            width: 100%;
        }
    </style>
</head>
<body>
    <button class="btn btn-danger" id="pdf" onclick="getPDF();"><i>GENERAR PDF</i></button>
    <div id="html_pdf">
        @include('fotradis::partials.tablas_bitacora')
    </div>
    {{-- </div> --}}
    <div class="modal fade" id="modal_foto">
    <img id="foto" src="">
    <div class="modal-footer">
        <button class="btn btn-success" id="rotar"><i>ROTAR IMÁGEN</i></button>
    </div>
</div><!-- /.modal -->
</body>
</html>

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{asset('js/firebase-app.js')}}"></script>
<script src="{{asset('js/firebase-auth.js')}}"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-storage.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

var config = {
    apiKey: "AIzaSyCeNLW0QsPvJZAzeZ7rIPJPOGH2wW1cqbw",
    authDomain: "fotradis.firebaseapp.com",
    databaseURL: "https://fotradis.firebaseio.com",
    projectId: "fotradis",
    storageBucket: "fotradis.appspot.com",
    messagingSenderId: "1095529979890"
  };

  firebase.initializeApp(config);
  var storage = firebase.storage();
  var storageRef = storage.ref();

  var angle = 0;

  jQuery.fn.rotate = function(degrees) {
    $(this).css({'transform' : 'rotate('+ degrees +'deg)'});
    return $(this);
    };

  $('#rotar').on('click', function() {
    angle += 90;
    $("#foto").rotate(angle);
  });

  function fotoOdometro(name) {
    var img = document.getElementById('foto');
    img.src = "";
    storageRef.child('odometro/'+name).getDownloadURL().then(function(url) {
      // `url` is the download URL for 'images/stars.jpg'

      // This can be downloaded directly:
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function(event) {
        var blob = xhr.response;
      };
      xhr.open('GET', url);
      xhr.send();

      img.src = url;
      // Or inserted into an <img> element:
      $("#modal_foto").modal("show");
    }).catch(function(error) {
      // Handle any errors
    });
  }    

  $('#pdf').on('click', function() {
    $.ajax({
        // la URL para la petición
        url : "{{route('bitacora.store')}}",
     
        // la información a enviar
        // (también es posible utilizar una cadena de datos)
        data : { html : $("#html_pdf").html() },
     
        // especifica si será ua petición POST o GET
        type : 'POST',
     
        // el tipo de información que se espera de respuesta
        dataType : 'html',

        contentType: 'text/plain',
     
        // código a ejecutar si la petición es satisfactoria;
        // la respuesta es pasada como argumento a la función
        success : function(json) {
            // $('<h1/>').text(json.title).appendTo('body');
            // $('<div class="content"/>')
            //     .html(json.html).appendTo('body');
                alert('se hizo');
        },
     
        // código a ejecutar si la petición falla;
        // son pasados como argumentos a la función
        // el objeto de la petición en crudo y código de estatus de la petición
        error : function(xhr, status) {
            alert('Disculpe, existió un problema');
        }//,
     
        // // código a ejecutar sin importar si la petición falló o no
        // complete : function(xhr, status) {
        //     alert('Petición realizada');
        // }
    });    
  });
</script>