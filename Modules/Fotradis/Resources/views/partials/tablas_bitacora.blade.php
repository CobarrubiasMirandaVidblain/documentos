<style>
.container {
    margin-left: 0px;
    margin-right: 15px;
    width: 100% !important;
}

.row {
    margin-right: 0px; 
    margin-left: 0px; 
}

.logo_header {
    height: 100px;
    width: 120px;
}

#foto {
    height: 500px;
    width: 600px;
}

.extends {
    text-align:center;
    width: 100%;
}
</style>
<div class="container">
{{-- <div class="row"> --}}
    <table class="table table-condensed table-striped extends">
        <tr></tr>
        <td><img class="logo_header" src="{{asset('images/logo_header.png')}}"><img class="logo_header" src="{{asset('images/diftellavalogo.png')}}"></td>
        <td><h1>BITÁCORA UNIDAD {{$unidad}}</h1></td>
    </table>
    
    <h5>PERIODO DE INFORME: {{$inicio}} - {{$fin}}</h5>
    <br>
    <h5>DONATARIO: SISTEMA PARA EL DESA-RROLLO INTEGRAL DE LA FAMILIA DEL ESTADO OAX.</h5>
    <br>
    {{-- <h5>DEPARTAMENTO: DIRECCION DE DISCAPACIDAD PLACAS: {{}}</h5>
    <br>
    <h5>PRIMER KILOMETRAJE: {{}} CAPACIDAD EN LITROS: {{}} TOTAL KMS RECORRIDOS: {{}}</h5>
    <br>
    <h5>RENDIMIENTO MENSUAL: {{}}</h5>
    <br> --}}

    <h3>Tabla de kilometrajes</h3>            
    <table class="table table-bordered table-condensed table-striped table-center extends">
        @if(isset($dataKm))
            @if(count($dataKm) == 0)
            <i>No hay datos disponibles</i>
            @else
                @foreach($dataKm as $row)
                    @if ($row == reset($dataKm)) 
                        <tr>
                            @foreach($row as $key => $value)
                                <th>{!! $key !!}</th>
                            @endforeach
                        </tr>
                    @endif
                    <tr>
                        @foreach($row as $key => $value)
                            {{-- @if(is_string($value) || is_numeric($value)) --}}
                                <td>{!! $value !!}</td>
                            {{-- @else
                                <td></td>
                            @endif --}}
                        @endforeach
                    </tr>
                @endforeach
            @endif
        @endif
    </table>

    <h3>Tabla de cargas de gasolina</h3>            
    <table class="table table-bordered table-condensed table-striped table-center">
        @if(isset($dataKm))
            @if(count($dataGas) == 0)
            <i>No hay datos disponibles</i>
            @else
                @foreach($dataGas as $row)
                    @if ($row == reset($dataGas)) 
                        <tr>
                            @foreach($row as $key => $value)
                                <th>{!! $key !!}</th>
                            @endforeach
                        </tr>
                    @endif
                    <tr>
                        @foreach($row as $key => $value)
                            {{-- @if(is_string($value) || is_numeric($value)) --}}
                                <td>{!! $value !!}</td>
                            {{-- @else
                                <td></td>
                            @endif --}}
                        @endforeach
                    </tr>
                @endforeach
            @endif
        @endif
    </table>
    <table class="table table-bordered table-condensed table-striped">

    </table>
</div>