<script>
	/* $.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

	var controlrutas = (function(){
		var form_control_rutas = $("#form_control_rutas");
		function agregar_editar(controlrutas, accion) {
			form_control_rutas.validate({
				rules: {
					conductor_id: {
						required: true
					},
					ruta_id: {
						required: true
					},
					unidad_id: {
						required: true
					}
				}
			});
			
			if(accion == "change") {
				transaccionAgregarEditar(controlrutas, accion)
			} else {
				if(form_control_rutas.valid()) {
					swal({
					title: '¿Estas seguro?',
					text: 'Se modificara esta asignación',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#28a745',
					cancelButtonColor: '#dc3545',
					confirmButtonText: 'SI',
					cancelButtonText: 'NO'
					}).then((result) => {
						if(result.value) {
							transaccionAgregarEditar(controlrutas, accion)
						} else {
							console.log("result.value false")
						}
					});
				}
			}
		};
		
		function transaccionAgregarEditar(controlrutas, accion) {
			var formData = new FormData(form_control_rutas[0]);
			formData.append('accion', accion);
			formData.append('controlruta', controlrutas);

			$.ajax({
				url:  "{{ route('controlrutas.store') }}",
				type: 'POST',
				data: formData,
				dataType: 'JSON',
				processData: false, 
				contentType: false,
				success: function(response) {
					swal({
							title: '¡Transacción exitosa!',
							text: 'Se ha almacenado...',
							type: 'success'
						});
					var ruta = response.ruta;
					if (response.tipo_servicio == 'taxi')
						ruta = 'TAXI';
					actualizarConductoresF(response.unidad, ruta, response.conductor, response.nombre_conductor, response.tipo_servicio);
					$("#modal_control_rutas").modal("hide");
					$('#conductor_id').empty();
					$('#ruta_id').empty();
					$('#unidad_id').empty();
					window.LaravelDataTables['rutas_del_dia'].ajax.reload(null, false);
				},
				error: function(response) {
					swal({
						title: '¡Registro rechazado!',
						text: 'No almacenado...',
						type: 'error'
					});
					window.LaravelDataTables['rutas_del_dia'].ajax.reload(null, false);
				}
			});
		};

		function eliminar(controlrutas, accion) {
			swal({
				title: '¿Estas seguro?',
				text: 'Se actualizarán elementos de la tabla de asignación',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						console.log("result.value true");
						$.ajax({
							url:  "{{ URL::to('fotradis/controlrutas') }}" + "/" + controlrutas,
							type: 'DELETE',
							success: function(response) {
								console.log(response.conductor)
									eliminarConductorF(response.conductor)

									if(accion == "change") 
										agregar_editar(controlrutas, "change")
									else {
										swal({
											title: '¡Transacción exitosa!',
											text: 'Se ha almacenado...',
											type: 'success'
										})

										window.LaravelDataTables['rutas_del_dia'].ajax.reload(null, false)
									}
							},
							error: function(response) {
								swal({
									title: '¡Registro rechazado!',
									text: 'No almacenado...',
									type: 'error'
								});
							}
						});				        
					} else {
						console.log("result.value false");
					}
				});
		};

		function boton(controlrutas, accion, data) {
			title_asignacion = $("#title_asignacion")
			header_modal_asignacion = $("#header_modal_asignacion")

			if(accion == "change") {
				if(servicioCambio) {
					title_asignacion.text("Cambiando urban " + data.val.unidad + " a servicio de taxi")
					header_modal_asignacion.attr("class", "modal-header btn-warning")
				} else {
					title_asignacion.text("Cambiando taxi " + data.val.unidad + " a servicio de ruta")
					header_modal_asignacion.attr("class", "modal-header btn-primary")
				}
				$("#button_modal_control_rutas").attr('onclick', 'controlrutas.eliminar('+ data.val.asignacionKey +',"'+ accion +'")')
			} else {
					title_asignacion.text("Asignación de rutas")
					header_modal_asignacion.attr("class", "modal-header btn-success")
					var option = new Option("","",true,true)

					$('#conductor_id').append(option).trigger('change')
					$('#unidad_id').append(option).trigger('change')
					$('#recorrido_id').append(option).trigger('change')

					$("#button_modal_control_rutas").attr('onclick', 'controlrutas.agregar_editar('+ controlrutas +',"'+ accion +'")')
			}
			$("#modal_control_rutas").modal("show");
		};

		function select2_conductores() {
			$('#conductor_id').select2({
		      language: 'es',
		      ajax: {
		        url: '{{ route('controlconductores.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.id,
		                text: item.nombre,
		                slug: item.nombre,
		                value: item.id,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		function select2_unidades() {
			$('#unidad_id').select2({
		      language: 'es',
		      ajax: {
		        url: '{{ route('controlunidades.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.id,
		                text: item.numero_unidad + " " + item.tipo_servicio,
		                slug: item.numero_unidad,
		                value: item.id,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		function select2_rutas() {
			$('#recorrido_id').select2({
		      language: 'es',
		      ajax: {
		        url: '{{ route('controlrutas.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.id,
		                text: item.nombre,
		                slug: item.nombre,
		                value: item.id,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		return {
			agregar_editar: agregar_editar,
			eliminar: eliminar,
			boton: boton,
			select2_rutas: select2_rutas,
			select2_unidades: select2_unidades,
			select2_conductores: select2_conductores
		};
	})();

	function reiniciarTablaMonitoreo() {
		window.LaravelDataTables['rutas_del_dia'].ajax.reload(null, false)
	} */
</script>