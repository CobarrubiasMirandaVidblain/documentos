<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
        
        <div class="hovereffect">
            <img src="{{ asset(isset($persona) ? $persona->get_url_fotografia() : 'images/no-image.png') }}" class="img-thumbnail center-block" id="imagen" name="imagen">
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

        <div class="row" style="padding-left: 0; padding-right: 0;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3>Nombre: </h3>
                <h4>{{ isset($persona) ? $persona->obtenerNombreCompleto() : '' }}</h4>
            </div>
        </div>

        <div class="row" style="padding-left: 0; padding-right: 0;">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <h3>Fecha de nacimiento: </h3>
                <h4>{{ isset($persona) ? $persona->get_formato_fecha_nacimiento() : '' }}</h4>
            </div>
        </div>

        <div class="row" style="padding-left: 0; padding-right: 0;">
            {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <h3>CURP: </h3>
                <h4>{{ isset($persona) ? $persona->curp : '' }}</h4>
            </div> --}}

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <h3>RFC: </h3>
                <h4>{{ isset($persona) ? $persona->empleado->rfc : '' }}</h4>
            </div>
        </div>

        <div class="row" style="padding-left: 0; padding-right: 0;">        
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <h3>Número seguro social: </h3>
                <h4>{{ isset($persona) ? $persona->empleado->segurosocial : '' }}</h4>
            </div>
        </div>

    </div>
</div>

    <form data-toggle="validator" role="form" id="form_conductor" action="{{ isset($conductor) ? route('conductor.update', $conductor->id) : route('conductor.store') }}" method="POST">

    <h4>Datos conductor:</h4>

    <hr>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label for="licencia_id">Licencia:</label>
            <div class="input-group form-group has-warning" id="asignar_licencia">                
                <input type="text" class="form-control" id="licencia_id" name="licencia_id" value="{{ $conductor->licencia->numero_licencia or '' }}" onclick="validar_modal_nueva_licencia()"/>
                @if(isset($conductor->licencia->numero_licencia))
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-detalles_licencia"><i class="fa fa-fw fa-eye"></i></button>
                    </span>
                @endif
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="donador_organos">Donador de organos:</label>
                <select id="donador_organos" name="donador_organos" class="form-control">
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="restricciones">Restricciones:</label>
                <textarea class="form-control" rows="3" id="restricciones" name="restricciones">{{ $conductor->restricciones or '' }}</textarea>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="padecimientos_medicos">Padecimientos médicos:</label>
                <textarea class="form-control" rows="3" id="padecimientos_medicos" name="padecimientos_medicos">{{ $conductor->padecimientos_medicos or '' }}</textarea>
            </div>
        </div>
    </div>

    {{--Emergencia--}}

    <h4>Contacto en caso de emergencia:</h4>

    <hr>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="persona_emergencia">Llamar a (Nombre de la persona):</label>
                <input type="text" class="form-control" id="persona_emergencia" name="persona_emergencia" value="{{ $persona->empleado->persona_emergencia or '' }}" />
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="telefono_emergencia">Teléfono:</label>
                <input type="text" class="form-control mask" id="telefono_emergencia" name="telefono_emergencia" data-inputmask='"mask": "(999) 999-9999"' data-mask value="{{ $persona->empleado->telefono_emergencia or '' }}" placeholder="NÚMERO DE TUTOR">
            </div>
        </div>
    </div>
</form>

<form id="form_modal_licencia" method="POST" action="#">
    <div class="modal fade" id="modal_elegir_licencia" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header btn-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">Licencias sin conductor asignado</h4></center>
          </div>
          <div class="modal-body" id="modal_body_elegir_licencia">
            <div class="form-group">
              <label for="tipo">Tipo de licencia:</label>
              <select id="tipo" name="tipo" class="form-control">
                  <option>A</option>
                  <option>B</option>
                  <option>C</option>
                  <option>D</option>
              </select>
            </div>

            <div class="form-group">
              <label for="vigencia">Fecha de vigencia:</label>
              <input type="text" class="form-control datepicker" id="vigencia" name="vigencia" value="" />
            </div>

            <div class="form-group">
              <label for="numero_licencia">Número de licencia:</label>
              <input type="text" class="form-control" id="numero_licencia" name="numero_licencia" value="" />
            </div>

            <div class="form-group">
              <label for="fecha_expedicion">Fecha de expedición:</label>
              <input type="text" class="form-control datepicker" id="fecha_expedicion" name="fecha_expedicion" value="" />
            </div>
          
            <div class="form-group">
              <label for="oficina_expedidora">Oficina de expedición:</label>
              <input type="text" class="form-control" id="oficina_expedidora" name="oficina_expedidora" value="" />
            </div>
            
            <div class="form-group">
              <label for="entidad_id">Entidad federativa:</label>
              <select id="entidad_id" name="entidad_id" class="form-control select2">
              </select>
            </div>
          </div>
          <div class="modal-footer" id="modal_footer_elegir_licencia">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
            <button id="confirmar_licencia" type="button" class="btn btn-primary pull-rigth" onclick="licencias.crear_licencia()">Confirmar</button>
          </div>
        </div>
      </div>
    </div>
</form>

<div class="modal fade" id="modal-detalles_licencia">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Datos de licencia de {{ isset($persona) ? $persona->obtenerNombreCompleto() : '' }}</h4>
        </div>
        <div class="modal-body">
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li><a href="#">Tipo <span class="badge bg-aqua">{{ $conductor->licencia->tipo or '' }}</span></a></li>
                    <li><a href="#">Vigencia <span class="badge bg-blue">{{ $conductor->licencia->vigencia or '' }}</span></a></li>
                    <li><a href="#">Número de licencia <span class="badge bg-green">{{ $conductor->licencia->numero_licencia or '' }}</span></a></li>
                    <li><a href="#">Fecha de expedición <span class="badge bg-yellow">{{ $conductor->licencia->fecha_expedicion or '' }}</span></a></li>
                    <li><a href="#">Oficina expedidora <span class="badge bg-red">{{ $conductor->licencia->oficina_expedidora or '' }}</span></a></li>
                </ul>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->