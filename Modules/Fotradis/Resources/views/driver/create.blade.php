@extends('fotradis::layouts.master')

@push('head')
@endpush

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{ isset($conductor) ? 'EDITAR' : 'AGREGAR' }} CONDUCTOR</h3>
  </div>
  <div class="box-body">
    @include('fotradis::driver.partials.create_edit_conductor')
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
    <div class="pull-right">
      <button type="button" class="btn btn-success" onclick='{{ isset($conductor) ? "crear_editar_conductor(" . $persona->empleado->id . ", 1)" : "crear_editar_conductor(" . $persona->empleado->id . ", 0)" }}'><i class="fa fa-database"></i> Guardar</button>
      <a href="{{ route('conductor.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
    </div>
  </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>

@include('fotradis::driver.js.conductor')

<script type="text/javascript">
  licencias.select2_licencias();
  modal_nueva_licencia = $("#modal_elegir_licencia");
  modal_body_nueva_licencia = $("#modal_body_elegir_licencia");
  modal_footer_nueva_licencia = $("#modal_footer_elegir_licencia");
  button_confirmar_licencia = $("#confirmar_licencia");
  button_nueva_licencia = $("#nueva_licencia");

  $(document).ready(function() {
    if({{ isset($conductor) ? 1 : 0}} ) {
        $("#donador_organos option[value="+ {{ $conductor->donador_organos or '' }} +"]").attr("selected",true);
    }

    $('.mask').inputmask();
  });
</script>
@endpush