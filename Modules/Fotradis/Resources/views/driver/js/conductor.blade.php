<script type="text/javascript">
	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Estás seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	var licencias = (function() {

		function select2_licencias() {
			$('#clave_licencia').select2({
		      language: 'es',
		      ajax: {
		        url: '{{ route('licencias.fotradis.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.numero_licencia,
		                text: item.numero_licencia,
		                slug: item.numero_licencia,
		                value: item.id,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		function crear_licencia() {
			var form_modal_licencia = $("#form_modal_licencia");
			if(form_modal_licencia.valid()) {
				swal({
				title: '¿Estas seguro?',
				text: '¡Nueva licencia!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						console.log("true");
						$.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});

						var formData = new FormData(form_modal_licencia[0]);
						
 						$.ajax({
							url:  "{{ route('fotradis.crear.licencia') }}",
							type: 'POST',
							data: formData,
							dataType: 'JSON',
							processData: false, 
							contentType: false,
							success: function(response) {
								if(response.estatus == 'existente') {
									swal({
										title: '¡No se almaceno licencia!',
										text: 'Esta repetida...',
										type: 'error'
									});
								} else if(response.estatus == 'nuevo') {
									swal({
										title: '¡Transacción exitosa!',
										text: 'Se ha almacenado...',
										type: 'success'
									});
									$("#licencia_id").val($("#numero_licencia").val());
									$("#modal_elegir_licencia").modal("hide");
								}
							},
							error: function(response) {
								swal({
									title: '¡Registro rechazado!',
									text: 'No almacenado...',
									type: 'error'
								});
							}
						});
					} else {
						console.log("false");
					}
				});
			}
		};

		function ver_licencia(licencia_id) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
				$.ajax({
				url:  "{{ URL::to('fotradis/licencia/search') }}" + "/" + licencia_id,
				type: 'GET',
				success: function(response) {
					$("#ver_licencia").html(response.html);
					$(".input_licencia").prop( "disabled", true );
					$("#modal_ver_conductor").modal("hide");
					$("#modal_ver_licencia").modal("show");
				},
				error: function(response) {
					$("#ver_licencia").html(response.html);	
					$("#modal_ver_licencia").modal("show");
				}
			});
		};

		function eliminar_licencia(licencia_id) {
			swal({
				title: '¿Estas seguro?',
				text:  '¡Se eliminara el registro!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						console.log("true");
						$.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});
							$.ajax({
							url:  "{{ URL::to('fotradis/licencia') }}" + "/" + licencia_id,
							type: 'DELETE',
							success: function(response) {
								swal({
									title: '¡Transacción exitosa!',
									text: 'Registro almacenado...',
									type: 'success'
								});
							},
							error: function(response) {
								swal({
									title: '¡Transacción rechazada!',
									text: 'Registro no almacenado...',
									type: 'error'
								});
							}
						});
					} else {
						console.log("false");
					}
				});
		};

		return {
			select2_licencias: select2_licencias,
			crear_licencia: crear_licencia,
			ver_licencia: ver_licencia,
			eliminar_licencia: eliminar_licencia
		};

	})();

	var empleados = (function() {
		var form = $("#form_modal_agregar_conductor");
		function select2_empleados() {
			$('#conductor').select2({
		      language: 'es',
		      ajax: {
		        url: '{{ route('empleados.fotradis.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.rfc,
		                text: item.nombre,
		                slug: item.rfc,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					conductor: {
						required: true
					}
				}
			});
		};

		function seleccionar_empleado(empleado) {
			if(form.valid()) {
				swal({
				title: '¿Estas seguro?',
				text: '¡Empleado seleccionado!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						$.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});

						var rfc;
						if(empleado == '')
							rfc = $("#conductor").select2('data')[0].results.rfc;
						else
							rfc = empleado;

 						$.ajax({
							url:  "{{ URL::to('fotradis/conductor/create') }}" + "?rfc=" + rfc,
							type: 'GET',
							success: function(response) {
								swal({
									title: '¡Selección exitosa!',
									text: 'Registro encontrado...',
									type: 'success'
								});

								window.location.href = "{{ URL::to('fotradis/conductor/show') }}" + "?empleado=" + response.id;
							},
							error: function(response) {
								swal({
									title: '¡Selección cancelada!',
									text: 'Registro no encontrado...',
									type: 'error'
								});
							}
						});

					}
				});
			}
		};

		return {
			select2_empleados: select2_empleados,
			agregar_validacion: agregar_validacion,
			seleccionar_empleado: seleccionar_empleado
		};
	})();

	function elegir_licencia() {
		//$("#licencia_id").val($("#clave_licencia").select2('data')[0].results.numero_licencia);
		$("#licencia_id").val($("#numero_licencia").val());
	}

	function mostrar_modal_nueva_licencia() {
		$("#modal_nueva_licencia").modal("show");
	}

	function validar_modal_nueva_licencia() {
	    $('.datepicker').datepicker({
	        autoclose: true,
	        language: 'es',
	        format: 'yyyy-mm-dd'
	    }).change(function(event) {
	      //$(".datepicker").valid();  
	    });

	    select2_entidades();

	    $("#form_modal_licencia").validate({
			rules: {
				tipo: {
					required:true
				},
				numero_licencia: {
					required:true,
					pattern_numeros: true
				},
				fecha_expedicion: {
					required:true
				},
				vigencia: {
					required:true
				},
				oficina_expedidora: {
					required:true
				},
				entidad_id: {
					required:true
				}
			}
		});

	    $.validator.addMethod('pattern_numeros', function (value, element) {
	        return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
	    }, 'Solo números');
	    
	    $("#modal_elegir_licencia").modal("show");
  }

function select2_entidades() {
	$('#entidad_id').select2({
	  language: 'es',
	  ajax: {
	    url: '{{ route('entidades.select') }}',
	    dataType: 'JSON',
	    type: 'GET',
	    data: function(params) {
	      return {
	        search: params.term
	      };
	    },
	    processResults: function(data, params) {
	      params.page = params.page || 1;
	      return {
	        results: $.map(data, function(item) {
	          return {
	            id: item.id,
	            text: item.entidad,
	            slug: item.entidad,
	            results: item
	          }
	        })
	      };
	    },
	    cache: true
	  }
	});
};

function crear_editar_conductor(empleado_id, conductor) {
	var form_conductor = $("#form_conductor");
	$.validator.addMethod('pattern_numeros', function (value, element) {
		        return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
		    }, 'Ingresar 10 dígitos');

	form_conductor.validate({
			rules: {
				licencia_id: {
					required:true
				},
				donador_organos: {
					required:true,
				},
				restricciones: {
					required:true
				},
				padecimientos_medicos: {
					required:true
				},
				telefono_emergencia: {
					required:true,					
				},
				persona_emergencia: {
					required:true,
				}
			}
		});

		if(form_conductor.valid()) {
			swal({
			title: '¿Estas seguro?',
			text:  '¡Actualizando conductores!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'SI',
			cancelButtonText: 'NO'
			}).then((result) => {
				if(result.value) {
					console.log("true");
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});

					var formData = new FormData(form_conductor[0]);
					formData.append('empleado_id', empleado_id);
					formData.append('clave_conductor', 'n/e');

						$.ajax({
						url:  form_conductor.attr('action'),
						type: form_conductor.attr('method'),
						data: formData,
						dataType: 'JSON',
						processData: false, 
						contentType: false,
						success: function(response) {
							swal({
									title: '¡Transacción exitosa!',
									text: 'Se ha modificado los conductores...',
									type: 'success'
								});
							//reiniciar_modal_nueva_licencia();
							window.location.href = "{{ route("conductor.index") }}";
						},
						error: function(response) {
							swal({
								title: '¡Transacción rechazada!',
								text: 'Registro no almacenado...',
								type: 'error'
							});
						}
					});
				} else {
					console.log("false");
				}
			});
		}
	}

	function eliminar_conductor(conductor) {
		swal({
			title: '¿Estas seguro?',
			text:  '¡Se eliminara el registro!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'SI',
			cancelButtonText: 'NO'
			}).then((result) => {
				if(result.value) {
					console.log("true");
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
						$.ajax({
						url:  "{{ URL::to('fotradis/conductor') }}" + "/" + conductor,
						type: 'DELETE',
						success: function(response) {
							swal({
								title: '¡Transacción exitosa!',
								text: 'Conductor eliminado...',
								type: 'success'
							});
              window.LaravelDataTables['conductores_actuales'].ajax.reload(null, false);
						},
						error: function(response) {
							swal({
								title: '¡Transacción rechazada!',
								text: 'Registro no almacenado...',
								type: 'error'
							});
						}
					});
				} else {
					console.log("false");
				}
			});
	}

	function ver_conductor(persona, conductor) {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
			$.ajax({
			url:  "{{ URL::to('fotradis/conductor/search') }}" + "/" + persona + "/" + conductor,
			type: 'GET',
			success: function(response) {
				$("#body_modal_ver_conductor").html(response.html);
				$("#modal_ver_conductor").modal("show");
				$("#licencia_id").prop( "disabled", true );
				$("#donador_organos").prop( "disabled", true );
				$("#restricciones").prop( "disabled", true );
				$("#padecimientos_medicos").prop( "disabled", true );
				$("#persona_emergencia").prop( "disabled", true );
				$("#telefono_emergencia").prop( "disabled", true );
				$("#clave_conductor").prop( "disabled", true );

				$("#asignar_licencia").html(
					response.html2
                );
			},
			error: function(response) {
				$("#body_modal_ver_conductor").html(response.html);	
				$("#modal_ver_conductor").modal("show");
			}
		});
	}
</script>