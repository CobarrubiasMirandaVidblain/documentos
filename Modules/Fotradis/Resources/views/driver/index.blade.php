
@extends('fotradis::layouts.master')

@push('head')
	<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('li-breadcrumbs')
    <li class="active">Conductores actuales</li>
@endsection

@section('content')

    <section class="content">
    	<div class="box box-primar shadow">
			<div class="box-header with-border">
				<h3 class="box-title">Conductores Actuales</h3>
				<!--<a href="{{ route('conductor.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Agregar</a>-->
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-toggle="modal" title="Elegir" data-target="#modal_modo_agregar_conductor" style="color: #d12654;"><i class="fa fa-fw fa-plus-square"></i> Agregar Conductor</button>
        </div>
      </div>

			<div class="box-body">
				<!-- /.box-header -->
				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
					<input type="text" id="search" name="search" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
					</span>
				</div>
				<!-- table start -->
				{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'conductores_actuales', 'name' => 'conductores_actuales', 'style' => 'width: 100%']) !!}
			</div>
		</div>
    </section>

    @include('fotradis::driver.partials.modo_agregar_conductor')
  	@include('fotradis::driver.partials.modal_licencia')

@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

@include('fotradis::driver.js.conductor')

<script type="text/javascript">
	empleados.select2_empleados();
	empleados.agregar_validacion();
	
	var modal_paradas = $("#modal_nueva_unidad");

	swal({
			title: 'Conductores asignados a una ruta y una unidad',
			text: '¡No se pueden eliminar!',
			type: 'warning',
			timer: 8000,
			toast : true,
			position : 'top-end',
			showConfirmButton: false
		});

	$(document).ready(function() {
		block();
		var table = (function() {
			var tabla = undefined,
			btn_buscar = $('#btn_buscar'),
			search = $('#search');

			function init() {			
				search.keypress(function(e) {
					if(e.which === 13) {
						tabla.DataTable().search(search.val()).draw();
					}
				});

				btn_buscar.on('click', function() {
					tabla.DataTable().search(search.val()).draw();
				});
			};

			function set_table(valor) {
				tabla = $(valor);
			};

			function get_table() {
				return tabla;
			};

			return {
				init: init,
				set_table: set_table,
				get_table: get_table
			};
		})();

		table.set_table($('#conductores_actuales').dataTable());
		table.init();
	    unblock();
	});
</script>
@endpush