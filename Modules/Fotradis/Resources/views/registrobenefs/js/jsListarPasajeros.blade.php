{!! $dataTable->scripts() !!}

<script type="text/javascript">


$(document).ready(() =>
{
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
	});

  table.set_table($('#tblPasajeros').dataTable());
  table.init();

});


var table = (function() 
{
  var tabla = undefined,
  btn_buscar = $('#btn_buscar'),
  search = $('#search');

  function init() {
    search.keypress(function(e) {
      if(e.which === 13) {
        tabla.DataTable().search(search.val()).draw();
      }
    });

    btn_buscar.on('click', function() {
      tabla.DataTable().search(search.val()).draw();
    });
  };

  function set_table(valor) {
    tabla = $(valor);
  };

  function get_table() {
    return tabla;
  };

  return {
    init: init,
    set_table: set_table,
    get_table: get_table
  };
})();

/* ---------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------- PARA LAS VENTANAS MODALES -------------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------------------------------------------------------------------------- */

function abrir_modal(modal, id, e, dt) 
{
  if (modal === 'modal-verPasajero')
  {
    if (id)
    {
      block();
      $("#modal-verPasajero-title").text('Visualizando pasajero');
      $.get('/fotradis/registrobenef/getwatchpasajero/'+id, function(data) {            
        $('#modal-body-verPasajero').html(data.html);
        VerPasajero.init();
        unblock();
        $('#modal-verPasajero').modal('show');
      });
    }
	} else if (modal === 'modal-editarPasajero'){
    if (id)
    {
      block();
      $("#modal-editarPasajero-title").text('Editando datos de pasajero');
      $.get('/fotradis/registrobenef/geteditpasajero/'+id, function(data) { 
        $('#modal-body-editarPasajero').html(data.html);
        EditarPasajero.init();
        unblock();
        $('#modal-editarPasajero').modal('show');
      });
    }
  } else if (modal === 'modal-pagarServicio'){
    if (id)
    {
      block();
      $("#modal-pagarServicio-title").text('Pago de servicio');
      $.get('/fotradis/registrobenef/getpagarservicio/'+id, function(data) {            
        $('#modal-body-pagarServicio').html(data.html);
        PagarServicio.init();
        //$("#form_documento")[0].reset();
        unblock();
        $('#modal-pagarServicio').modal('show');
      });
    }
  } else if(modal === "modal-EliminarServicio")
  { 
    block();
    var url = '/fotradis/registrobenef/getdeleteservicio/'+id;
    
    var title = 'Eliminar servicio';
    $("#modal-eliminarServicio-title").text(title);
    $.get(url, function(data) {            
      $('#modal-body-eliminarServicio').html(data.html);
      EliminarServicio.init(data.numServicios);
      unblock();
      $('#modal-eliminarServicio').modal('show');
    });
  }else if (modal === 'modal-repInscritos'){
		block();
		$("#modal-repInscritos-title").text('Reporte trimestral');
		$.get('/fotradis/registrobenef/draftrepinscritos', function(data) {            
			$('#modal-body-repInscritos').html(data.html);
			RepInscritos.init(e, dt);
			unblock();
			$('#modal-repInscritos').modal('show');
		});
  } else
    $("#"+modal).modal('show');
}

function cerrar_modal(modal)
{
  $("#"+modal).modal("hide");
}

/* ---------------------------------------------------------------------------------------------------------------------------------------------------------- */
/* --------------------------------------------------------- PARA LAS FUNCIONES DEL MÓDULO ------------------------------------------------------------------ */
/* ---------------------------------------------------------------------------------------------------------------------------------------------------------- */



</script>