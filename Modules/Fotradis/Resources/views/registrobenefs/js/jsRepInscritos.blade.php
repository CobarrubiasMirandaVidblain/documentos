<script type="text/javascript">

var RepInscritos = (function()
{
	var erFechaDiaMesAnio = Globaljs.erFechaDiaMesAnio;
  var fechaIniServ=null, fechaFinServ=null;
	var form_repInscritos;
	var e, d;

	function init(e, dt) 
  {
		form_repInscritos = $("#form_repInscritos");
    fechaIniServ = $("#modal-repInscritos #fechaIniServ");
    fechaFinServ = $("#modal-repInscritos #fechaFinServ");
		e = e;
		d = dt;

		fechaIniServ.datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: '01/01/2019'
        // endDate: '0d'
    }).change(function(event) {
        //fechaIniServ.valid();
    });

		fechaFinServ.datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: '01/01/2019'
        // endDate: '0d'
    }).change(function(event) {
        //fechaIniServ.valid();
    });
    
		setFormValidations();
  }

	function reporteInscritos ()
	{
		if(form_repInscritos.valid() && validarEntradas ()) 
    {
			$.ajax({
				url: "{{ route('registrobenef.repinscritos') }}", // dt.ajax.url() || window.location.href,
				data: { draw: 2, length: -1, action:'customExcel', fechaIniServ:Globaljs.reverseDate (fechaIniServ.val(), "/", "-"), fechaFinServ:Globaljs.reverseDate (fechaFinServ.val(), "/", "-") },
				success: function (res) {
					var datos = {};
					datos.titulo = document.title;
					datos.cabeceras = res.cabeceras
					datos.datos = res.data.map(d => Object.values(d)).map(e => e.map(f => f != null ? f : ''));
					jsreport.serverUrl = 'http://187.157.97.110:3001';
					var request = {
						template: {
							shortid: 'HJ5VttAyS'
						},
						data: datos
					};
					jsreport.renderAsync(request).then(function (res2) {
						res2.download(res.name+'.xlsx');
					}); 
				}
			});
		}
	}

	function setFormValidations() 
  {
    $.validator.addMethod('pattern_fecha', function (value, element) {
		        return this.optional(element) || erFechaDiaMesAnio.test(value);
    }, 'Formato de fecha incorrecta');

    return form_repInscritos.validate({
      rules: {
        fechaIniServ: {
          required:true,
          pattern_fecha: true
        },
				fechaFinServ: {
          required:true,
          pattern_fecha: true
        }
      }
    });
  }

	function validarEntradas ()
  {
    return true;
  }

	return {
    init: init,
		reporteInscritos: reporteInscritos
  };
})();

</script>