<script type="text/javascript">

var EditarPasajero = (function()
{
	var form_editarPasajero;

	function init() 
  {
		form_editarPasajero = $("#form_editarPasajero");
    // $("#modal-body-editarPasajero input").attr("disabled", "disabled");  //$("input").prop( "disabled", false );
    // $('#imgen').rotate(90);

		$('#pais').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getPaises') }}", 
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    }).change(function(event){
      if ($('#pais').select2('data')[0].text !== "MÉXICO"){
        $('#select2-estado-container').html("").trigger('change.select2');
        Globaljs.object_setEnabled(false,'estado');
      }else
        Globaljs.object_setEnabled(true,'estado');
    });

    $('#estado').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getEntidades') }}", //entidades.select
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.entidad,
                slug: item.entidad,
                results: item
              }
            })
          };
        },
        cache: true
      },
       /*placeholder: {
        id: '20', // the value of the option
        text: 'OAXACA'
      }*/
    });

    $('#municipio').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getMunicipios') }}", //municipios.select
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    });

		// setFormValidations();
  }

	function guardarCambiosPasajero ()
	{
		if(form_editarPasajero.valid() && validarEntradas ()) 
    {
      swal({ title: '¿Está seguro?', text:'¿Confirma que desea guardar las modificaciones al registro?', type: 'warning', showCancelButton: true, 
          confirmButtonColor: '#28a745', cancelButtonColor: '#dc3545', confirmButtonText: 'SI', cancelButtonText: 'NO'
      }).then((result) => 
      {
        if(result.value) 
        {
          block();
          /*$.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });*/
          var formEditarServicio = new FormData(form_editarPasajero[0]);
          // formEditarServicio.append('fechaFinServ', 'hola' /*fechaFinServ.val()*/);
          $.ajax({
            url: form_editarPasajero.attr('action'),
            type: form_editarPasajero.attr('method'),
            data: formEditarServicio,  //form.serialize(),
            dataType: 'JSON',
            processData: false, 
            contentType: false,
            success: function(response) {
              unblock();
              if (response.success){
                swal({ title:'¡Transacción exitosa!', text: 'Registro almacenado...', type: 'success', timer: 2600});
                window.location.href = "{{ route('registrobenef.index') }}"; //"{{ URL::to('atnciudadana/solicitudes') }}"
              }else
                swal({ title: '¡Transacción rechazada!', html:'Registro no almacenado.<br><br>'+response.mensaje, type:'error'});
              //pasajero_create_edit_success(response, method);
            },
            error: function(response) {
              unblock();
              swal({ title: '¡Transacción rechazada!', text:'Registro no almacenado...', type:'error'});
              //pasajero_create_edit_error(response);
            }
          });
        }
      });
    }
	}

	function setFormValidations() 
  {
    $.validator.addMethod('pattern_fecha', function (value, element) {
		        return this.optional(element) || erFechaDiaMesAnio.test(value);
    }, 'Formato de fecha incorrecta');

    return form_editarPasajero.validate({
      rules: {
        fechaIniServ: {
          required:true,
          pattern_fecha: true
        },
        folioPago: {
          required:true
        },
      }
    });
  }

	function validarEntradas ()
  {
    /*if (Globaljs.compareDate(fechaIniServ.val(),"<=",backupFechaFinServ.val()))
      return mostrarWarning("La fecha inicial ("+fechaIniServ.val()+") tiene que ser posterior a la fecha de término del servicio anterior ("+backupFechaFinServ.val()+").");
    if (Globaljs.compareDate(fechaIniServ.val(),"<",Globaljs.getFechaHoy(false)))
      return mostrarWarning("La fecha inicial tiene que ser igual o posterior al día de hoy.");

    if (Globaljs.compareDate(fechaFinServ.val(),"<=",backupFechaFinServ.val()))
            return mostrarWarning("El periodo de pago que intenta establecer ya está cubierto.");*/
    
    return true;
  }

  return {
    init: init,
		guardarCambiosPasajero: guardarCambiosPasajero,
		validarEntradas: validarEntradas
  };
	
})();
</script>