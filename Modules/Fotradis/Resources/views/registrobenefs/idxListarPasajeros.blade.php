@extends('fotradis::layouts.master')

@section('localCSS')
	@include('fotradis::registrobenefs.css.cssListarPasajeros')
@endsection

@section('content-title','DIF TE LLEVA')
@section('content-subtitle', 'Fondo para la Accesibilidad en el Transporte Público para las Personas con Discapacidad')

@section('li-breadcrumbs')
    <li class="active">Registro de pasajeros</li>
@endsection

@section('content')	
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box  box-primary shadow">
        <div class="box-body">
          <div class="box-header with-border">
            <h3 class="box-title">Listado de pasajeros</h3>
            <div class="box-tools pull-right">
              <!--a class="btn btn-success pull-right" onclick="abrir_modal('modal-regispasajero')"><i class="fa fa-plus"></i> Agregar</a-->
              <a href="{{ route('registrobenef.getaddpasajero') }}" class="btn btn-box-tool" style="color: #d12654;"><i class="fa fa-fw fa-plus-square"></i> Agregar pasajero</a>
            </div>
          </div>
        </div>
        <div class="box-body">
          <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
            <input type="text" id="search" name="search" class="form-control">
            <span class="input-group-btn">
              <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
            </span>
          </div>

          {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'tblPasajeros', 'name' => 'tblPasajeros', 'style' => 'width: 100%']) !!}
          
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="modal-verPasajero">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" onclick="cerrar_modal('modal-verPasajero')" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-verPasajero-title"></h4>
      </div>
      <div class="modal-body" id="modal-body-verPasajero"></div>
      <div class="modal-footer" id="modal-footer-verPasajero">
        <div class="pull-right">
          <button type="button" class="btn btn-success" onclick="cerrar_modal('modal-verPasajero')">Aceptar</button>    
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-editarPasajero">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" onclick="cerrar_modal('modal-editarPasajero')" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-editarPasajero-title"></h4>
      </div>
      <div class="modal-body" id="modal-body-editarPasajero"></div>
      <div class="modal-footer" id="modal-footer-editarPasajero">
        <div class="pull-right">
					<button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-editarPasajero')">Cancelar</button>
          <button type="button" class="btn btn-success" onclick="EditarPasajero.guardarCambiosPasajero()">Guardar</button>    
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-pagarServicio">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" onclick="cerrar_modal('modal-pagarServicio')" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-pagarServicio-title"></h4>
      </div>
      <div class="modal-body" id="modal-body-pagarServicio" class="modal-body"></div>
      <div class="modal-footer" id="modal-footer-pagarServicio">
        <div class="pull-right">
          <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-pagarServicio')">Cancelar</button>
          <button type="button" class="btn btn-success" onclick="PagarServicio.guardarPago()">Guardar</button>    
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-eliminarServicio">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" onclick="cerrar_modal('modal-eliminarServicio')" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-eliminarServicio-title"></h4>
      </div>
      <div class="modal-body" id="modal-body-eliminarServicio" class="modal-body"></div>
      <div class="modal-footer" id="modal-footer-eliminarServicio">
        <div class="pull-right">
          <button type="button" class="btn btn-success" onclick="cerrar_modal('modal-eliminarServicio')">Cancelar</button>
          <button type="button" class="btn btn-danger" onclick="EliminarServicio.eliminarServicio()">Eliminar</button>    
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-repInscritos">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" onclick="cerrar_modal('modal-repInscritos')" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-repInscritos-title"></h4>
      </div>
      <div class="modal-body" id="modal-body-repInscritos" class="modal-body"></div>
      <div class="modal-footer" id="modal-footer-repInscritos">
        <div class="pull-right">
          <button type="button" class="btn btn-success" onclick="RepInscritos.reporteInscritos()">Ver reporte</button>
          <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-repInscritos')">Cancelar</button>    
        </div>
      </div>
    </div>
  </div>
</div>
@stop

<!--@section('localScripts')
@endsection-->

@push('body')
  @include('fotradis::registrobenefs.js.jsLinks');
  @include('fotradis::registrobenefs.js.jsGlobalJs');
  @include('fotradis::registrobenefs.js.jsPagarServicio');
  @include('fotradis::registrobenefs.js.jsEliminarServicio');
  @include('fotradis::registrobenefs.js.jsVerPasajero');
	@include('fotradis::registrobenefs.js.jsEditarPasajero');
  @include('fotradis::registrobenefs.js.jsListarPasajeros');
  @include('fotradis::registrobenefs.js.jsRepInscritos');
@endpush