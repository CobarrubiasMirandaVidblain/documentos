@extends('fotradis::layouts.master')

@section('content-subtitle')
  {{isset($pasajero) ? 'Editar pasajero' : 'Nuevo pasajero' }}
@endsection
@section('li-breadcrumbs')
    <li><a href="{{route('registrobenef.index')}}">Registro de beneficiarios</a></li>
    <li class="active">{{ isset($pasajero) ? 'Editar' : 'Nuevo'}} pasajero</li>
@endsection

@section('content')
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ isset($pasajero) ? 'EDITAR' : 'AGREGAR' }} PASAJERO</h3>
          </div>
          <div class="box-body">
              @include('fotradis::registrobenefs.partials.ptlCrearEditarPasajero')
          </div>
          <div class="box-footer">
            <div class="pull-right">
                <button type="button" class="btn btn-primary btn-sm" onclick="CrearEditarPasajero.guardarPasajero()">Guardar</button>
                <a href="{{ route('registrobenef.index') }}" class="btn btn-danger btn-sm">Cancelar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  @include('fotradis::registrobenefs.partials.ptlVerFotoPasajero')

@stop

@push('body')
  @include('fotradis::registrobenefs.js.jsLinks');
  @include('fotradis::registrobenefs.js.jsGlobalJs');
  @include('fotradis::registrobenefs.js.jsCrearEditarPasajero');
@endpush


