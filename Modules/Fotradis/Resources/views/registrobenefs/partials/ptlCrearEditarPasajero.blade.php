{{-- Formulario pasajeros --}}

<form id="form_pasajero" enctype="multipart/form-data" action="{{ isset($pasajero) ? route('registrobenef.setupdatepasajero', $pasajero->pasajero_id) : route('registrobenef.setstorepasajero') }}" method="POST">

	<div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
      <div class="form-group">
        <label for="foto">Fotografía:</label>
        <div class="form-group">
          <div class="hovereffect" style="width:240px; height:240px;">
            <img src="{{ isset($pasajero) && $pasajero->foto!='' ? asset($pasajero->foto) : asset('images/no-image.png')}}" class="center-block" id="imagen" name="imagen" style="height:240px;"> <!-- src="{{ isset($pasajero) ? asset($pasajero->foto) : asset('images/no-image.png')}}"-->
            <div class="overlay">
              <h2>Acciones</h2>
              <button type="button" class="btn btn-success" onclick="fotoPasajero.agregar();"><i class="fa fa-database"></i> Agregar</button>
              <button type="button" class="btn btn-danger" onclick="fotoPasajero.eliminar()"><i class="fa fa-trash"></i> Eliminar</button>
              <button type="button" class="btn btn-info" onclick="fotoPasajero.ver()"><i class="fa fa-eye"></i> Ver</button>
              <input id="foto" name="archivo" style="display: none;" type="file">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

	@if(!isset($pasajero))
	<div id="datosPasajero">
		<br>
		<h4>Datos de pasajero:</h4>
		<hr>

		<div  class="row" style="padding-left: 0; padding-right: 0;">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label for="tipoPasajero">Tipo de pasajero*:</label>
					<select id="tipoPasajero" name="tipoPasajero" class="form-control select2">
						<@if(isset($pasajero))
							<option value="{{ $pasajero->tipopasajero_id }}" selected>{{ $pasajero->tipoPasajero }}</option>
						@endif>
					</select>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label>Fecha inicio*:</label>
					<div class="input-group date">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" id="fechaIniServ" name="fechaIniServ" class="form-control pull-right" value="{{ $pasajero->fechaIniServ or '' }}" placeholder="dd/MM/aaaa" >
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div id="pnlTipoServicio" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label for="tipoServicio">Tipo de servicio*:</label>
					<select id="tipoServicio" name="tipoServicio" class="form-control select2">
						<@if(isset($pasajero))
							<option value="{{ $pasajero->tiposervicio_id }}" selected>{{ $pasajero->tipoServicio }}</option>
						@endif>
					</select>
				</div>
			</div>
			<div id="pnlNumPasajero" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="form-group">
					<label>Nº de pasajero*:</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-file-text-o"></i>
						</div>
						<input type="text" id="numPasajero" name="numPasajero" class="form-control" placeholder="Número de pulsera" value="{{ $pasajero->numPasajero or '' }}">
					</div>
				</div>
			</div>

			<div id="pnlNumPasajeroAmbos" class="urban taxi col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="form-group">
					<label>Nº 2 pasajero*:</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-file-text-o"></i>
						</div>
						<input type="text" id="numPasajeroAmbos" name="numPasajeroAmbos" class="form-control" placeholder="Número de pulsera" value="{{ $pasajero->numPasajeroAmbos or '' }}">
					</div>
				</div>
			</div>

			<div class="urban col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="form-group">
					<label for="numMesesServ">Cantidad de meses a pagar*:</label>
					<select id="numMesesServ" name="numMesesServ" class="form-control select2">
						@for ($i = 1; $i < 12; $i++)
								@if(isset($pasajero->numMesesServ))
									@if($pasajero->numMesesServ == $i)
										<option value="{{ $i }}" selected="true" title="Cantidad de meses a pagar">{{ $i }}</option>
									@else
										<option value="{{ $i }}" title="Cantidad de meses a pagar">{{ $i }}</option>
									@endif
								@else
									<option value="{{ $i }}" title="Cantidad de meses a pagar">{{ $i }}</option>
								@endif
							@endfor
					</select>
				</div>
			</div>

			<div class="urban col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="form-group">
					<label for="folioPago">Folio/No. pago*:</label>
					<input type="text" id="folioPago" name="folioPago" class="form-control" placeholder="Folio o número de pago" value="{{ $pasajero->folioPago or '' }}">
				</div>
			</div>

			<div id="pnlLugarPago" class="urban col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<div class="form-group">
					<label for="lugarPago">Pagó en:</label>
					<select id="lugarPago" name="lugarPago" class="form-control select2">
						<@if(isset($pasajero))
							<option value="{{ $pasajero->lugarPago }}" selected>{{ $pasajero->lugarPago }}</option>
						@endif>
					</select>
				</div>
			</div>

		</div>
	</div>
	@else
		<input type="hidden" id="numPasajero" name="numPasajero" value="{{ $pasajero->numPasajero or '' }}">
	@endif

  <br>
  <h4>Datos de la persona:</h4>
  <hr>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        <label for="nombre">Nombre(s)*:</label>
        <input type="text" id="nombre" name="nombre" class="form-control" value="{{isset($pasajero) ? $pasajero->nombre : ''}}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        <label for="primerApe">Primer apellido*:</label>
        <input type="text" id="primerApe" name="primerApe" class="form-control" value="{{isset($pasajero) ? $pasajero->primerApe : ''}}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        <label for="segundoApe">Segundo apellido:</label>
        <input type="text" id="segundoApe" name="segundoApe" class="form-control" value="{{isset($pasajero) ? $pasajero->segundoApe : ''}}">
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        <label for="curp">CURP*:</label>
        <input type="text" id="curp" name="curp" class="form-control" value="{{isset($pasajero) ? $pasajero->curp : ''}}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        <label for="telefono">Teléfono*:</label>
        <input type="text" id="telefono" name="telefono" class="form-control" value="{{isset($pasajero) ? $pasajero->telefono : ''}}">
      </div>
    </div>

		<div id="pnlEtnia" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="form-group">
				<label for="etnia">Etnia*:</label>
				<select id="etnia" name="etnia" class="form-control select2">
					<@if(isset($pasajero))
						<option value="{{ $pasajero->etnia_id }}" selected>{{ $pasajero->etnia }}</option>
					@endif>
				</select>
			</div>
		</div>

  </div>

  <br>
  <h4>Domicilio de la persona:</h4>
  <hr>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        <label for="pais">País*:</label>
        <select id="pais" name="pais" class="form-control select2">
          <@if(isset($pasajero))
            <option value="{{ $pasajero->pais_id }}" selected>{{ $pasajero->pais }}</option>
          @endif>
        </select>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        <label for="entidad">Entidad*:</label>
        <select id="entidad" name="entidad" class="form-control select2">
          <@if(isset($pasajero))
            <option value="{{ $pasajero->entidad_id }}" selected>{{ $pasajero->entidad }}</option>
          @endif>
        </select>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <div class="form-group">
        <label for="calle">Calle:</label>
        <input type="text" id="calle" name="calle" class="form-control" value="{{isset($pasajero) ? $pasajero->calle : ''}}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <div class="form-group">
        <label for="numero">N°:</label>
        <input type="text" id="numero" name="numero" class="form-control" value="{{isset($pasajero) ? $pasajero->numero : ''}}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
      <div class="form-group">
        <label for="colonia">Colonia:</label>
        <input type="text" id="colonia" name="colonia" class="form-control" value="{{isset($pasajero) ? $pasajero->colonia : ''}}">
      </div>
    </div>
  </div>


  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="municipio_id">*Municipio:</label>
        <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
          @if(isset($pasajero) && isset($pasajero->municipio))
          <option value="{{ $pasajero->municipio_id }}" selected>({{ $pasajero->municipio }})</option>
          @endif
        </select>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="localidad_id">Localidad:</label>
        <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;">
          @if(isset($pasajero) && isset($pasajero->localidad))
          <option value="{{ $pasajero->localidad }}" selected>  </option>
          @endif
        </select>
      </div>
    </div>
  </div>



  <br>
  <h4>Contacto en caso de emergencia:</h4>
  <hr>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        <label for="persona_emergencias">Persona para emergencias:</label>
        <input type="text" id="persona_emergencias" name="persona_emergencias" class="form-control" value="{{isset($pasajero) ? $pasajero->persona_emergencias : ''}}">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
        <label for="telefono_emergencias">Teléfono para emergencias:</label>
        <input type="text" id="telefono_emergencias" name="telefono_emergencias" class="form-control" value="{{isset($pasajero) ? $pasajero->telefono_emergencias : ''}}">
      </div>
    </div>
  </div>
</form>




@push('body')
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/jquery-ui/jquery-ui.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>


  @include('personas.js.persona');

  <script type="text/javascript">

    $(document).ready(function() {

      $('#municipio_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('municipios.select') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#municipio_id').valid();
        $('#localidad_id').empty();
        $('#localidad_id').select2({
          language: 'es',
          placeholder : 'SELECCIONE ...',
          ajax: {
            url: '{{ route('localidades.select') }}',
            delay: 500,
            dataType: 'JSON',
            type: 'GET',
            data: function(params) {
              return {
                search: params.term,
                municipio_id: $('#municipio_id').val()
              };
            },
            processResults: function(data, params) {
              params.page = params.page || 1;
              return {
                results: $.map(data, function(item) {
                  return {
                    id: item.id,
                    text: item.nombre,
                    slug: item.nombre,
                    results: item
                  }
                })
              };
            },
            cache: true
          }
        }).change(function(event) {
          $('#localidad_id').valid();
        });
      });
      $('.mask').inputmask();


    });



  </script>
@endpush
