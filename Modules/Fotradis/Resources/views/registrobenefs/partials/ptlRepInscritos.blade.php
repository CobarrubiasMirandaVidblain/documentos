<div class="row">
	<form id="form_repInscritos" enctype="multipart/form-data" action="{{ route('registrobenef.repinscritos') }}" method="GET">

		<input type="hidden" id="servicioadquirido_id" name="servicioadquirido_id" value="{{ $pago->servicioadquirido_id or '' }}">
			
		<h4>Perído a visualizar de inscritos al servicio:</h4>
		<hr>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="form-group">
					<label>Fecha inicial:</label>
					<div class="input-group date">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" id="fechaIniServ" name="fechaIniServ" class="datepicker form-control pull-right" value="" placeholder="dd/MM/aaaa" >
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<div class="form-group">
					<label for="fechaFinServ">Fecha final:</label>
					<div class="input-group date">
						<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
						<input type="text" id="fechaFinServ" name="fechaFinServ" class="form-control" placeholder="dd/MM/aaaa" value="">
					</div>
				</div>
			</div>
		</div>
  </form>
</div>