<div class="row">

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
    <div class="hovereffect">
      <img src="{{ asset(isset($pasajero) ? $pasajero->foto : 'images/no-image.png') }}" class="img-thumbnail center-block" id="imagen" name="imagen" style="transform: rotate(90deg);">
    </div>
  </div>

  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

    <div class="row" style="padding-left: 0; padding-right: 0;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3>Nombre: </h3>
        <h4>{{ isset($pasajero) ? $pasajero->nombre.' '.$pasajero->primerApe.' '.$pasajero->segundoApe : '' }}</h4>
      </div>
    </div>

    <div class="row" style="padding-left: 0; padding-right: 0;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <h3>CURP: </h3>
        <h4>{{ isset($pasajero) ? $pasajero->curp: '' }}</h4>
      </div>
    </div>

    <div class="row" style="padding-left: 0; padding-right: 0;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <h3>Teléfono: </h3>
        <h4>{{ isset($pasajero) ? $pasajero->telefono : '' }}</h4>
      </div>
    </div>

  </div>
</div>

<div class="row">

  <h4>Datos de pasajero:</h4>
  <hr>
  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="tipoServicio">Tipo de servicio:</label>
        <input type="text" class="form-control" id="tipoServicio" name="tipoServicio" value="{{ $pasajero->tipoServicio or '' }}" />
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="tipoPasajero">Tipo de pasajero:</label>
        <input type="text" class="form-control" id="tipoPasajero" name="tipoPasajero" value="{{ $pasajero->tipoPasajero or '' }}" />
      </div>
    </div>

    @if(isset($pasajero->fechaIniServ))
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
          <label for="fechaIniServ">Fecha de Inicio del servicio:</label>
          <input type="text" class="form-control" id="fechaIniServ" name="fechaIniServ" value="{{ $pasajero->fechaIniServ or '' }}" />
        </div>
      </div>
    @endif

    @if(isset($pasajero->fechaFinServ))
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
          <label for="fechaFinServ">Fecha final de servicio:</label>
          <input type="text" class="form-control" id="fechaFinServ" name="fechaFinServ" value="{{ $pasajero->fechaFinServ or '' }}" />
        </div>
      </div>
    @endif

    @if(isset($pasajero->numMesesServ))
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="numMesesServ">Número de meses:</label>
        <input type="text" class="form-control" id="numMesesServ" name="numMesesServ" value="{{ $pasajero->numMesesServ or '' }}" />
      </div>
    </div>
    @endif

    @if(isset($pasajero->folioPago))
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="folioPago">Folio de pago:</label>
        <input type="text" class="form-control" id="folioPago" name="folioPago" value="{{ $pasajero->folioPago or '' }}" />
      </div>
    </div>
    @endif

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="numPasajero">Num pasajero:</label>
        <input type="text" class="form-control" id="numPasajero" name="numPasajero" value="{{ $pasajero->numPasajero or '' }}" />
      </div>
    </div>

    @if(isset($pasajero->numPasajeroAmbos) && $pasajero->numPasajeroAmbos!=='')
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
          <label for="numPasajeroAmbos">2do No. de pasajero:</label>
          <input type="text" class="form-control" id="numPasajeroAmbos" name="numPasajeroAmbos" value="{{ $pasajero->numPasajeroAmbos or '' }}" />
        </div>
      </div>
    @endif

  </div>
  
  <h4>Domicilio de la persona:</h4>
  <hr>
  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="pais">Pais:</label>
        <input type="text" class="form-control" id="pais" name="pais" value="{{ $pasajero->pais or '' }}" />
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="estado">Estado:</label>
        <input type="text" class="form-control" id="estado" name="estado" value="{{ $pasajero->estado or '' }}" />
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="calle">Calle:</label>
        <input type="text" class="form-control" id="calle" name="calle" value="{{ $pasajero->calle or '' }}" />
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="numero">Número:</label>
        <input type="text" class="form-control" id="numero" name="numero" value="{{ $pasajero->numero or '' }}" />
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="colonia">Colonia:</label>
        <input type="text" class="form-control" id="colonia" name="colonia" value="{{ $pasajero->colonia or '' }}" />
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="localidad">Localidad:</label>
        <input type="text" class="form-control" id="localidad" name="localidad" value="{{ $pasajero->localidad or '' }}" />
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="municipio">Municipio:</label>
        <input type="text" class="form-control" id="municipio" name="municipio" value="{{ $pasajero->municipio or '' }}" />
      </div>
    </div>

  </div>

  <h4>Contacto en caso de emergencia:</h4>
  <hr>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="persona_emergencias">Llamar a (Nombre de la persona):</label>
        <input type="text" class="form-control" id="persona_emergencias" name="persona_emergencias" value="{{ $pasajero->persona_emergencias or '' }}" />
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="telefono_emergencias">Teléfono:</label>
        <input type="text" class="form-control mask" id="telefono_emergencias" name="telefono_emergencias" data-inputmask='"mask": "(999) 999-9999"' data-mask value="{{ $pasajero->telefono_emergencias or '' }}">
      </div>
    </div>
  </div>
</div>
