@extends('fotradis::layouts.master')

@section('content-subtitle')
  {{isset($unidad) ? 'Editar unidad' : 'Nueva unidad' }}
@endsection
@section('li-breadcrumbs')
    <li><a href="{{route('unidad.index')}}">Unidades</a></li>
    <li class="active">{{ isset($unidad) ? 'Editar' : 'Nueva'}} unidad</li>
@endsection


@section('content')
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ isset($unidad) ? 'EDITAR' : 'AGREGAR' }} UNIDAD</h3>
          </div>
          <div class="box-body">
              @include('fotradis::unity.partials.crear_editar_unidad')
          </div>         
          <div class="box-footer">
            <div class="pull-right">
                <button type="button" class="btn btn-primary btn-sm" onclick="agregar_editar.unidad()">Guardar</button>
                <a href="{{ route('unidad.index') }}" class="btn btn-danger btn-sm">Cancelar</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    @include('fotradis::unity.partials.ver_unidad')
  </section>
@stop

@push('body')
@include('fotradis::unity.js.unidades');
<script type="text/javascript">
  @if(isset($unidad->tipo_servicio))
    var servicio = "{{ $unidad->tipo_servicio }}";
    $("#tipo_servicio option[value=" + servicio + "]").attr("selected",true)      
  @endif
  
  $('.datepicker').datepicker({
          autoclose: true,
          language: 'es',
          format: 'yyyy-mm-dd'
      }).change(function(event) {
        //$(".datepicker").valid();  
      });
</script>
@endpush