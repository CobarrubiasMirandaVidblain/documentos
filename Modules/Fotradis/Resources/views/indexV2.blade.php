@extends('fotradis::layouts.master')

@push('head')
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}">
  <link type="text/css" rel="stylesheet" href="{{ asset('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
  <style type="text/css">
    #monitoreo_map {
      background-color: lightblue;
      height: calc(100vh - 211px);
    }
    .products-list .product-img img {
      width: 17px;
      height: 30px;
    }
    .cards {
      margin-right: -15px !important;
      margin-left: -15px !important;
    }
    #audio {
      display: none
    }
    .nameMax {
      font-size: 22px !important;
      padding-bottom: 14px !important;
    }
    #ruta_mas_transitada{
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    #loaderTable{
      display: flex;
      justify-content: center;
    }
  </style>
@endpush

@section('content')
  {{-- tarjetas informativas --}}
  <div class="row cards">
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-aqua shadow">
        <div class="inner">
          <h3 id="total_pasajeros"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
          <p>Beneficiarios activos</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-green shadow">
        <div class="inner">
          <h3 id="pasajeros_hoy"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>{{-- <sup style="font-size: 20px">% --}}</sup></h3>
          <p>Registros del día</p>
        </div>
        <div class="icon">
          <i class="fa fa-user"></i>              
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <div class="small-box bg-yellow shadow">
        <div class="inner">
          <h3 id="pasajeros_mes"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
          <p>Registros del mes</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6"> {{--style="width:100%; word-wrap: break-word;">--}}
      <div class="small-box bg-red shadow">
        <div class="inner">
          <h3 id="ruta_mas_transitada"><i class="fa fa-spinner fa-pulse fa-fw"></i></h3>
          <p>Ruta más transitada este mes</p>
        </div>
        <div class="icon">
          <i class="fa fa-map-marker"></i>
        </div>
      </div>
    </div>
  </div>
  {{-- tabla de asignaciones --}}
  <div class="row cards">
    <div class="col-xs-12">
      <div class="box box-primary shadow">
        <div class="box-header with-border">
          <h3 class="box-title">Rutas Asignadas</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            {{-- <div class="checkbox"> --}}<input type="checkbox" name="showRecord" id="showRecord"><span> Mostrar historial</span>{{-- </div> --}}
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
              <input type="text" id="search" name="search" class="form-control">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
              </span>
            </div>
          </div>
          {{-- <div id="loaderTable">
            <i  class="fa fa-refresh fa-spin fa-2x fa-fw" style="color: #c54e77;"></i>
          </div> --}}
          <div id="content-table" class="">
            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'sesiones', 'name' => 'sesiones', 'style' => 'width: 100%']) !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- mapa de monitoreo --}}
  <div class="box box-primary shadow">
    <div class="box-header with-border">
      <h3 class="box-title">Monitoreo de unidades</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
      </div>
    </div>
    <div class="box-body">
      <div id="monitoreo_map"></div>
    </div>
  </div>
  {{-- modal informativo --}}
  <div class="modal fade" id="create_edit">
		<div class="modal-dialog">
			<div class="modal-content">

			</div>
		</div>
	</div>
@stop

@push('body')
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
  <script src="{{ asset('js/firebase-app.js') }}"></script>
  <script src="{{ asset('js/firebase-auth.js') }}"></script>
  <script src="{{ asset('js/firebase-database.js') }}"></script>
  <!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGJY1fM_1PhRMuqn6eu9-2Cra1LRdnoqg&libraries=geometry"></script-->
  <script type="text/javascript" src="{{ asset('js/fotradis/poligono_puerto.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/fotradis/poligono_oax.js') }}"></script>
  {!! $dataTable->scripts() !!}
  <script>
    /* var config = {
      apiKey: "AIzaSyB7jM0WfuaZdwqfRlE5t-CJ3zfDz11xSUw",
      authDomain: "reginet-7c5b2.firebaseapp.com",
      databaseURL: "https://reginet-7c5b2.firebaseio.com",
      projectId: "reginet-7c5b2",
      storageBucket: "reginet-7c5b2.appspot.com",
      messagingSenderId: "770654201194"
    }; */
    var config = {
      apiKey: "AIzaSyAsQUbiwTC1JrapsfBHClpHKo94tuyKkow",
      authDomain: "reginet-tests.firebaseapp.com",
      databaseURL: "https://reginet-tests.firebaseio.com",
      projectId: "reginet-tests",
      storageBucket: "reginet-tests.appspot.com",
      messagingSenderId: "661942876658"
    };

    var map;
    var waypts = [];
    var markers = [];
    var paradas = [];
    var pasajeros;
    var mini =  true;
    var lineas = [];
    var poligonos = [];
    
    //INICIAR TODO Xd
    $(document).ready(function(){
      table.set_table($('#rutas_del_dia').dataTable());
      initMap();
      actualizarEstadisticos();
    });
    //iniciar firebase
    firebase.initializeApp(config);
    /* var mAuth = firebase.auth().signInWithEmailAndPassword('diftelleva@gmail.com', 'sicarucheelu').catch(function(error) {
      // Encargarse de errores aquí
      var errorCode = error.code;
      var errorMessage = error.message;
    }); */

    //iniciar mapa
    function initMap() {
      map = new google.maps.Map(document.getElementById('monitoreo_map'), {
        center: {lat: 17.027557, lng: -96.710725},
        zoom: 12,
        mapTypeControl: false,
        styles:  [
          {
              featureType: 'poi.business',
              stylers: [{visibility: 'off'}]
          },
          {
              featureType: 'transit',
              elementType: 'labels.icon',
              stylers: [{visibility: 'off'}]
          }
        ]
      });

      //generando un poligono cuandrado con el contorno de oaxaca en el centro
      oaxaca = new google.maps.Polygon({
        paths: [puertoCoord,oaxCoord],
          strokeColor: '#000000',
          strokeOpacity: 0.35,
          strokeWeight: 3,
          fillColor: '#000000',
          fillOpacity: 0.0
      });
      
      //y asignado al mapa
      oaxaca.setMap(map);

      var points = [];
      var marcadores = [];
      var obj = {};
      var audio = {};

      @foreach($paradas as $parada)
        @if($parada->tipoparada === 'R')
          marcadores.push({LatLng:{ lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },tipo:{{$parada->tipo}},ref:"{{$parada->referencia}}",icono:"{{$parada->icono}}"});                
        @endif
        @if($parada->tipo == 3)
            points.push({location: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} }, stopover: false});
        @endif
        @if($parada->tipo == 1)
            obj.inicio = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };
        @endif
        @if($parada->tipo == 2)
            obj.fin = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };                
        @endif
      @endforeach

      @if(isset(($rutas)))
        @foreach($rutas as $ruta)
          @if(isset($ruta->polyline))
              var lineaRuta = new google.maps.Polyline({
                  path: google.maps.geometry.encoding.decodePath("{{$ruta->polyline}}"),
                  geodesic: true,
                  strokeColor: "{{$ruta->color}}",
                  strokeOpacity: 0.8,
                  strokeWeight: 5
              });
              lineaRuta.setMap(map)
              lineas.push(lineaRuta);
          @endif
        @endforeach
      @endif

      @if(isset($paradas))
        @foreach($paradas as $parada)
          @if($parada->tipoparada === 'R' && $parada->tipo != 3)
            new google.maps.Marker({
              map: map,
              draggable: false,
              animation: google.maps.Animation.DROP,
              position: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },
              title: "{{$parada->referencia}}",
              icon: {{$parada->tipo}} !=3 ?
              {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 35),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 35)} :
              {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 22),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 11)}
            });
          @endif
          @if($parada->tipo == 3)
            var p = new google.maps.Marker({
              draggable: false,
              animation: {{$parada->tipo}} !=3 ? google.maps.Animation.BOUNCE : google.maps.Animation.DROP,
              position: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },
              title: "{{$parada->referencia}}",
              icon: {{$parada->tipo}} !=3 ?
              {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 35),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 35)} :
              {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 22),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 11)}
            });
            paradas.push({marcador: p, ruta:{{$parada->ruta}} });
          @endif
        @endforeach
      @endif

      map.addListener('zoom_changed', function() {
        if(map.getZoom() == 14){
          for(var i=0;i<paradas.length;i++){
            paradas[i].marcador.setMap(map);
          }
        }else if (map.getZoom() < 14){
          for(var i=0;i<paradas.length;i++){
            paradas[i].marcador.setMap(null);
          }
        }
      });

      var rutaspanel = '<div class="row" style="min-width: 130px;">' +
      '<div class="col-xs-12" style="padding-right: 0px;padding-left: 0px;">' +
      '<div class="box box-info shadow">' +
      '<div class="box-header with-border">' +
        '<h3 class="box-title" onclick="min()">RUTAS</h3>' +
        '<div class="box-tools pull-right">' +
                  '<button type="button" class="btn btn-box-tool" onclick="min()" ><i id="min-plus" class="fa fa-plus"></i>' +
                  '</button>' +
      '</div>' +
      '<div id="allRutas" class="box-body collapse" style="padding: 0px 10px;">' +
        '<ul class="products-list product-list-in-box">' +
          '<li class="item">' +
            '<div class="product-img">' +
              '<img src="/images/R001.png" alt="Product Image">' +
            '</div>' +
            '<div class="product-info">' +
              '<a href="javascript:void(0)" onclick="selectRuta(1)" class="product-title">DIF - CREE</a>' +
                '<span class="product-description">' +
                '</span>' +
            '</div>' +
          '</li>' +
          '<li class="item">' +
            '<div class="product-img">' +
              '<img src="/images/R002.png" alt="Product Image">' +
            '</div>' +
            '<div class="product-info">' +
              '<a href="javascript:void(0)" onclick="selectRuta(3)" class="product-title">VIGUERA - CD. ADMINISTRATIVA</a>' +
              '<span class="product-description">' +
                  '</span>' +
            '</div>' +
          '</li>' +
          '<li class="item">' +
            '<div class="product-img">' +
              '<img src="/images/R003.png" alt="Product Image">' +
            '</div>' +
            '<div class="product-info">' +
              '<a href="javascript:void(0)" onclick="selectRuta(6)" class="product-title">LLANO - MONTE ALBAN</a>' +
              '<span class="product-description">' +
                  '</span>' +
            '</div>' +
          '</li>' +
          '<li class="item">' +
            '<div class="product-img">' +
              '<img src="/images/R004.png" alt="Product Image">' +
            '</div>' +
            '<div class="product-info">' +
              '<a href="javascript:void(0)" onclick="selectRuta(7)" class="product-title">PARQUE EL LLANO</a>' +
              '<span class="product-description">' +
                  '</span>' +
            '</div>' +
          '</li>' +
          '<li class="item">' +
            '<div class="product-img">' +
              '<img src="/images/R005.png" alt="Product Image">' +
            '</div>' +
            '<div class="product-info">' +
              '<a href="javascript:void(0)" onclick="selectRuta(8)" class="product-title">SN. LUIS BELTRAN - CD. JUDICIAL</a>' +
              '<span class="product-description">' +
                  '</span>' +
            '</div>' +
          '</li>' +
          '<li class="item">' +
            '<div class="product-img">' +
              '<img src="/images/R006.png" alt="Product Image">' +
            '</div>' +
            '<div class="product-info">' +
              '<a href="javascript:void(0)" onclick="selectRuta(10)" class="product-title">ATZOMPA - CONGRESO</a>' +
              '<span class="product-description">' +
              '</span>' +
            '</div>' +
          '</li>' +
        '</ul>' +
      '</div>' +
      '  </div>' +
      '</div>' +
      '  </div>' ;
      var todo = document.createElement('button');
      todo.textContent = "Oaxaca";
      todo.setAttribute('class', 'btn btn-block btn-info');
      todo.setAttribute('type', 'button');
      todo.addEventListener('click', function() {
        map.setCenter({lat: 17.027557, lng: -96.710725});
        map.setZoom(12);
        for(var i=0;i<paradas.length;i++){
          paradas[i].marcador.setMap(null);
        }
      });
      
      var puerto = document.createElement('button');
      puerto.textContent = "Puerto";
      puerto.setAttribute('class', 'btn btn-block btn-info');
      puerto.setAttribute('type', 'button');
      puerto.addEventListener('click', function() {
        map.setCenter({lat: 15.8749265, lng: -97.1120579});
      });

      var rutas_ = document.createElement('button');
      rutas_.textContent = "Ocultar rutas";
      rutas_.setAttribute('class', 'btn btn-block btn-success');
      rutas_.setAttribute('type', 'button');
      rutas_.addEventListener('click', function() {
        let m = null;
        if(rutas_.textContent == "Ocultar rutas") {
          rutas_.textContent = "Mostrar rutas";
          rutas_.setAttribute('class', 'btn btn-block btn-danger');
        } else {
          rutas_.textContent = "Ocultar rutas";
          rutas_.setAttribute('class', 'btn btn-block btn-success');
          m = map;
        }
        lineas.forEach(element => {
          element.setMap(m);
        });
      });

      var limites_ = document.createElement('button');
      limites_.textContent = "Ocultar limites";
      limites_.setAttribute('class', 'btn btn-block btn-success');
      limites_.setAttribute('type', 'button');
      limites_.addEventListener('click', function() {
        let m = null;
        if(limites_.textContent == "Ocultar limites") {
          limites_.textContent = "Mostrar limites";
          limites_.setAttribute('class', 'btn btn-block btn-danger');
        } else {
          limites_.textContent = "Ocultar limites";
          limites_.setAttribute('class', 'btn btn-block btn-success');
          m = map;
        }
        oaxaca.setMap(m);
      });

      var ControlDivUBI = document.createElement('div');
      ControlDivUBI.setAttribute('style','margin-left: 10px;');
      ControlDivUBI.appendChild(todo);
      ControlDivUBI.appendChild(puerto);
      ControlDivUBI.appendChild(rutas_);
      ControlDivUBI.appendChild(limites_);
      map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(ControlDivUBI);

      var PanelRutas= document.createElement('div');
      PanelRutas.setAttribute('style','margin-top: 10px;margin-left: 10px;');
      PanelRutas.innerHTML=rutaspanel;
      PanelRutas.index = 1;
      map.controls[google.maps.ControlPosition.LEFT_TOP].push(PanelRutas);
      
      var monitoreo_unidades = firebase.database().ref('ubication').on('value', function(snapshot) {
        deleteMarkers();        
        if (snapshot != null) {
        snapshot.forEach(function(childSnapshot) {
          waypts.push({
              key: childSnapshot.key,
              position: new google.maps.LatLng(childSnapshot.val().latitude, childSnapshot.val().longitude),
              text: "Unidad: " + childSnapshot.key + "\n\nConductor: " + childSnapshot.val().driver + "\n\nEsta en: " +childSnapshot.val().address+ "\n\nActualizado: " +childSnapshot.val().datetime
            });
        });

        waypts.forEach(function(feature) {
            var marker = new google.maps.Marker({
              id: feature.key,
              position: feature.position,
              title: feature.text,
              map: map
            });
            
            var alerta_unidad = firebase.database().ref('alarm/' + feature.key).on('value',function(snapshot){            
              if (snapshot.val() != null) {
                if(snapshot.val() == 1) {
                  if(feature.key.search('TX') == 0)
                    marker.setIcon("{{ asset('images/diftelleva/rutataxi-17.png') }}");
                  else
                    marker.setIcon("/images/diftelleva/rutaurvan-"+feature.key.replace('UV-','')+"-alerta.png");
                  
                  if(audio[feature.key] === undefined){
                    audio[feature.key] = document.createElement('audio');
                    var tempSource = document.createElement('source');
                    tempSource.src = "/alerta.mp3";
                    tempSource.type = "audio/mp3";
                    audio[feature.key].appendChild(tempSource);
                    audio[feature.key].loop = true;
                  }
                  if(audio[feature.key] != undefined && audio[feature.key].paused ){
                    audio[feature.key].play();
                  }
                  marker.setAnimation(google.maps.Animation.BOUNCE);
                  
                } else if(snapshot.val() == 0) {
                  if(feature.key.search('TX') == 0)
                    marker.setIcon("{{ asset('images/diftelleva/rutataxi-22.png') }}");
                  else
                    marker.setIcon("/images/diftelleva/rutaurvan-"+feature.key.replace('UV-','')+".png");
                  
                  if(audio[feature.key] != undefined){
                    audio[feature.key].pause();
                    audio[feature.key].currentTime = 0;
                  }
                  marker.setAnimation(null);
                  marker.setZIndex(1);
                
                }
              } else {
                if(feature.key.search('TX') == 0) {
                  marker.setIcon("{{ asset('images/diftelleva/rutataxi-22.png') }}");
                } else {
                  marker.setIcon("/images/diftelleva/rutaurvan-"+feature.key.replace('UV-','')+".png");
                }
              }
            });

            marker.addListener('click', function() {
              info(map, marker,marker.id);
            });
            markers.push(marker);
          });
        }
      });

      var alerta_unidades = firebase.database().ref('alarm').on('value', function(snapshot) {        
        snapshot.forEach(function(childSnapshot) {
          if (childSnapshot != null) {
            markers.forEach(function(feature) {
              if(feature.id == childSnapshot.key && childSnapshot.val() == 1) {
                if(childSnapshot.key.search('TX') == 0)
                  feature.setIcon("{{ asset('images/diftelleva/rutataxi-17.png') }}");
                else
                  feature.setIcon("/images/diftelleva/rutaurvan-"+childSnapshot.key.replace('UV-','')+"-alerta.png");
              
                feature.setAnimation(google.maps.Animation.BOUNCE);
              } else if(feature.id == childSnapshot.key && childSnapshot.val() == 0) {
                if(childSnapshot.key.search('TX') == 0)
                  feature.setIcon("{{ asset('images/diftelleva/rutataxi-22.png') }}");
                else
                  feature.setIcon("/images/diftelleva/rutaurvan-"+childSnapshot.key.replace('UV-','')+".png");
              
                feature.setAnimation(null);
              }
            });
          }
        });
      });
    }

    function info(map,marker,unidad){      
      $.get("{{ URL::to('fotradis/pasajeros/') }}"+"/"+unidad, function(data) {
        var infowindow = new google.maps.InfoWindow({
          content: contentInfo(data)
        });

        infowindow.open(map);
        infowindow.setPosition(marker.getPosition());
      })      
    }

    function contentInfo(data) {
      if(data.success == false) {
        return contentString(data.unidad,'<p><b>Sin pasajeros a bordo.</b></p>');
      } else {
        var lista = '';
        data.abordo.forEach(element => {
          var emerg = 'no disponible.';
          if(element.nump != null)
            emerg = element.nump;

          lista = lista + '<p><b>'+element.nombre+' '+element.pa+' '+element.sa+'</b>, En caso de emergencias: '+emerg+'.</p>';
        });
        return contentString(data.unidad,lista);
      }
    }

    function contentString(unidad,lista) {
      return '<div id="content">'+
              '<div id="siteNotice">'+
              '</div>'+
              '<h1 id="firstHeading" class="firstHeading">'+unidad+'</h1>'+
              '<div id="bodyContent">'+
              lista
              '</div>'+
              '</div>';
    }

    var min = () => {
      $("#allRutas").collapse('toggle');
      $("#min-plus").attr("class",mini ? "fa fa-minus" :"fa fa-plus" );
      mini = !mini;
    }

    var selectRuta = (id) => {
      for(var i=0;i<paradas.length;i++){
                  paradas[i].marcador.setMap(null);
                  paradas[i].marcador.setAnimation(null);
              }
      for(var i=0;i<paradas.length;i++){
        if(paradas[i].ruta == id){
          paradas[i].marcador.setMap(map);
          paradas[i].marcador.setAnimation(google.maps.Animation.BOUNCE);
        }
      }
    }
    
    var updateRegistro = firebase.database().ref('register_passenger').on('value', function(snapshot) {
      actualizarEstadisticos()
      console.log('pasajero');
    })

    var updateSesiones = firebase.database().ref('socket_new_sesion').on('value', function(snapshot) {
      window.LaravelDataTables['sesiones'].ajax.reload(null, false)
      console.log('sesion');
    }) 

    function deleteMarkers() {
      clearMarkers();
      markers = [];
      waypts = [];
    }
    function clearMarkers() {
      setMapOnAll(null);
    }
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }
    function actualizarEstadisticos(params) {
      $.get("{{ route('fotradis.stats') }}", function(data) {
        $('#total_pasajeros').html(data.benAct);
        $('#pasajeros_hoy').html(data.hoy);
        $('#pasajeros_mes').html(data.mes);
        $('#ruta_mas_transitada').html(data.ruta);
      })
    }
    
    //Datatable busqueda
    var table = (function() {
      var tabla = undefined,
      btn_buscar = $('#btn_buscar'),
      search = $('#search');

      function init() {     
        search.keypress(function(e) {
          if(e.which === 13) {
            tabla.DataTable().search(search.val()).draw();
          }
        });

        btn_buscar.on('click', function() {
          tabla.DataTable().search(search.val()).draw();
        });

        tabla.on('draw.dt', function () {
          $(".myToggle").each(function(){
              $(this).bootstrapToggle();
              $(this).change(function() {
                cambiarServicio($(this).attr('id'),{estado : $(this).prop('checked') });
            })
          });
        });
      };

      function set_table(valor) {
        tabla = $(valor);
      };

      function get_table() {
        return tabla;
      };

      return {
        init: init,
        set_table: set_table,
        get_table: get_table
      };
    })();
    //mostrar los detalles de una sesion
    function cargarModal(id) {
      $.get("{{ route('fotradis.index') }}"+'sesion/'+id, function(data) {
        $('.modal-content').html(data.body);
        $('#create_edit').modal("show");
      });
    }
  </script>
@endpush