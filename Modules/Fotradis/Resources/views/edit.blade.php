@extends('fotradis::layouts.master')

@push('head')
	<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('li-breadcrumbs')
    <li class="active">Historial Conductores Asignados</li>
@endsection

@section('content')

    <section class="content">
    	<div class="box box-primary shadow">
			<div class="box-header with-border">
				<h3 class="box-title">Historial Conductores Asignados</h3>				
			</div>

			<div class="box-body">
				<!-- /.box-header -->
				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
					<input type="text" id="search" name="search" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
					</span>
				</div>
				<!-- table start -->
				{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'historial', 'name' => 'historial', 'style' => 'width: 100%']) !!}
			</div>
		</div>
    </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">
	$(document).ready(function() {
		block();
		var table = (function() {
			var tabla = undefined,
			btn_buscar = $('#btn_buscar'),
			search = $('#search');

			function init() {			
				search.keypress(function(e) {
					if(e.which === 13) {
						tabla.DataTable().search(search.val()).draw();
					}
				});

				btn_buscar.on('click', function() {
					tabla.DataTable().search(search.val()).draw();
				});
			};

			function set_table(valor) {
				tabla = $(valor);
			};

			function get_table() {
				return tabla;
			};

			return {
				init: init,
				set_table: set_table,
				get_table: get_table
			};
		})();

		table.set_table($('#historial').dataTable());
		table.init();
	    unblock();
	});
</script>
@endpush