@extends('fotradis::layouts.master')

@section('content')
	<div class="modal fade" id="modal_bitacora" data-keyboard="false" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header btn-info">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        <span aria-hidden="true">&times;</span></button>
	        <center><h4 class="modal-title">Generar bitácora</h4></center>
	      </div>
	      <div class="modal-body">
	        <form id="form_bitacora">
	          <div class="form-group">
				  <label for="unidad">Unidad:</label>
				  <select id="unidad" name="unidad" class="form-control">
				  </select>
			  </div>


	          <div class="form-group">
	            <label for="inicio">Inicio:</label>
	            <input type="text" class="form-control datepicker" id="inicio" name="inicio"/>	            
	          </div>
				
			  <div class="form-group">
	          	<label for="fin">Fin:</label>
	            <input type="text" class="form-control datepicker" id="fin" name="fin"/>
	          </div>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
	        <button type="button" class="btn btn-info pull-rigth" id="button_modal_bitacora" data-dismiss="modal" onclick="formBitacora()">Listo</button>
	      </div>
	    </div>
	  </div>
	</div>

	<button class="btn btn-danger" id="pdf" onclick="getPDF();"><i>GENERAR PDF</i></button>
    <div id="documento">
		
	</div>
    {{-- </div> --}}
    <div class="modal fade" id="modal_foto">
    <img id="foto" src="">
    <div class="modal-footer">
        <button class="btn btn-success" id="rotar"><i>ROTAR IMÁGEN</i></button>
    </div>
@endsection

@push('body')
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-storage.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.4/jspdf.plugin.autotable.js"></script>

<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script type="text/javascript">
	Date.prototype.toString = function() { return this.getDate()+"/"+(this.getMonth()+1)+"/"+this.getFullYear(); } 
	$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

 var config = {
    apiKey: "AIzaSyB7jM0WfuaZdwqfRlE5t-CJ3zfDz11xSUw",
    authDomain: "reginet-7c5b2.firebaseapp.com",
    databaseURL: "https://reginet-7c5b2.firebaseio.com",
    projectId: "reginet-7c5b2",
    storageBucket: "reginet-7c5b2.appspot.com",
    messagingSenderId: "770654201194"
  };


  firebase.initializeApp(config);

  var mAuth = firebase.auth().signInWithEmailAndPassword('angelbrian.inc@gmail.com', 'scodelario1').catch(function(error) {
    // Encargarse de errores aquí
    var errorCode = error.code;
    var errorMessage = error.message;
  });

	$(document).ready(function() {
		$('.datepicker').datepicker({
  			autoclose: true,
        language: 'es',
        format: 'yyyy-mm-dd',

  	}).change(function(event) {

  	});

    $("#fin").datepicker({

    }).change(function(event) {
      fin = moment($("#fin").val()).format('YYYY/MM/DD');
      inicio = moment($("#inicio").val()).format('YYYY/MM/DD');
      if(fin < inicio){
        $("#fin").val('');
        swal({
          title: '¡No se puede seleccionar una fecha anterior al inicio',
          type: 'error',
          timer: 3000,
          toast : true,
          position : 'top-end',
          showConfirmButton: false
        });
      }
    });
  	select2_todas_unidades();
		$("#modal_bitacora").modal("show");
	});

	function select2_todas_unidades() {
		$('#unidad').select2({
	      language: 'es',
	      ajax: {
	        url: "{{ route('controlunidades.select', ['todas' => true]) }}",
	        dataType: 'JSON',
	        type: 'GET',
	        data: function(params) {
	          return {
	            search: params.term
	          };
	        },
	        processResults: function(data, params) {
	          params.page = params.page || 1;
	          return {
	            results: $.map(data, function(item) {
	              return {
	                id: item.id,
	                text: item.numero_unidad,
	                slug: item.numero_unidad,
	                value: item.id,
	                results: item
	              }
	            })
	          };
	        },
	        cache: true
	      }
  		});
	}

	function formBitacora() {
		var form = $("#form_bitacora");
		form.validate({
			rules: {
				unidad: {
					required: true
				},
				inicio: {
					required: true
				},
				fin: {
					required: true
				}
			}
		});

		if(form.valid()) {
			$.ajax({
				url:  "{{ URL::to('fotradis/fotradis') }}" + "/" + $("#unidad").select2('data')[0].results.id,
				type: 'GET',
				success: function(response) {
					swal({
							title: '¡Espere un momento!',
							type: 'success'
						});
					var inicio = $("#inicio").datepicker('getDate');
					var fin = $("#fin").datepicker('getDate');
					// alert(inicio.getTime() + " " + fin.getTime());
					generarBitacora(response.numero_unidad, response.matricula, "" + inicio.getTime(), "" + fin.getTime());
				},
				error: function(response) {
					swal({
						title: '¡Error!',
						text: 'No se encontró esta unidad...',
						type: 'error'
					});
				}
			});
		}
	}
 	
 	var jsonGas;
	var jsonKm;
	var sumatoriaGas = 0;
	var kmsRecorridos = 0;
	var rendimientoMensual = 0;
	var sumatoriaKm1 = 0;
	var sumatoriaKm2 = 0;
	var dataGas = [];
	var dataOdometro = [];
	var dGas = [];
	var dOdometro = [];
	var uni;

	function generarBitacora(unidad,matricula,f_inicio,f_fin) {
		uni = unidad;
		let promesaTablaGas = new Promise((resolve, reject) => {
			var gas = firebase.database().ref('cargas_de_gas').orderByKey().startAt(f_inicio).endAt(f_fin).once('value', function(snapshot) {
				console.log(snapshot);
				var i = 1;
				if (snapshot != null) {					
					snapshot.forEach(function(childSnapshot) {
						if (unidad == childSnapshot.val().unidad) {
							var date = new Date();
							date.setTime(childSnapshot.key);
							dataGas.push({iterador: i, fecha: date, numero_comprobante: childSnapshot.val().num_comprobante, modo_pago: childSnapshot.val().modo_pago, litros: childSnapshot.val().litros, conductor: childSnapshot.val().conductor, comprobante: '<button onclick="fotoGas('+"'"+ childSnapshot.val().comprobante_pago +"'"+');">Comprobante de pago</button>'});
							dGas.push({iterador: i, fecha: date, numero_comprobante: childSnapshot.val().num_comprobante, modo_pago: childSnapshot.val().modo_pago, litros: childSnapshot.val().litros, conductor: childSnapshot.val().conductor});
							sumatoriaGas = sumatoriaGas + parseInt(childSnapshot.val().litros);
							i++;
						}
					});
				}
				jsonGas = JSON.stringify(dataGas);
				// console.log(jsonGas);
				resolve();
			});			  
		});

		promesaTablaGas.then(
			function() {
				var km = firebase.database().ref('odometro').orderByKey().startAt(f_inicio).endAt(f_fin).once('value', function(snapshot) {
					var i = 1;
					unidad = 1;
					if (snapshot != null) {
						snapshot.forEach(function(childSnapshot) {
							if (unidad == childSnapshot.val().unidad) {
								if(i == 1)
									sumatoriaKm1 = parseInt(childSnapshot.val().km);
								var date = new Date();
								date.setTime(childSnapshot.key);
								// dataOdometro.push({iterador: i, fecha: date, km: childSnapshot.val().km, conductor: childSnapshot.val().conductor, odometro: '<button onclick="fotoOdometro('+"'"+ childSnapshot.val().odometro +"'"+');">Odómetro</button>'});
								dataOdometro.push({fecha: date, km: childSnapshot.val().km, unidad: childSnapshot.val().unidad});
								dOdometro.push({iterador: i, fecha: date, km: childSnapshot.val().km, conductor: childSnapshot.val().conductor});
								sumatoriaKm2 = parseInt(childSnapshot.val().km);
								i++;
							}
						});
					}
					jsonKm = JSON.stringify(dataOdometro);
					kmsRecorridos = sumatoriaKm2 - sumatoriaKm1;
					rendimientoMensual = sumatoriaGas/kmsRecorridos;
					console.log(jsonKm);
					getHTML();
			})
			.catch(
			function() {
				swal({
					title: '¡Error!',
					text: 'Fallo en reecolección de datos...',
					type: 'error'
				});
			});
			});
		}

	function getHTML() {
		$.ajax({
			url:  "{{ route('bitacora.index') }}",
			type: 'GET',
			data: {dataGas: jsonGas, dataKm: jsonKm, sumatoriaGas: sumatoriaGas, kmsRecorridos: kmsRecorridos, rendimientoMensual: rendimientoMensual, inicio: inicio, fin: fin, unidad: uni},
			dataType: 'JSON',
			contentType: 'json',
			success: function(response) {
				// $("#modal_bitacora").modal("hide");
				$("#documento").html(response.html);
				swal({
					title: '¡Transacción exitosa!',
					text: 'Se ha almacenado...',
					type: 'success'
				});
			},
			error: function(response) {
				swal({
					title: '¡Registro rechazado!',
					text: 'No almacenado...',
					type: 'error'
				});
			}
		});
		// window.location.href = "URL::to('fotradis/bitacora/create') " + "?dataGas=" + jsonGas + "&dataKm=" + jsonKm;
	}
	
	function getPDF() {
		jKm = JSON.stringify(dOdometro);
		jGas = JSON.stringify(dGas);
		window.location.href = "{{URL::to('fotradis/bitacora/create')}}" + "?dataGas=" + jGas + "&dataKm=" + jKm + 
		"&sumatoriaGas" + sumatoriaGas +  "&kmsRecorridos" + kmsRecorridos + "&rendimientoMensual" + rendimientoMensual + "&inicio" + inicio + "&fin" + fin + "&unidad" + uni;
	}

  var storage = firebase.storage();
  var storageRef = storage.ref();

  var angle = 0;

  function fotoOdometro(name) {
    var img = document.getElementById('foto');
    img.src = "";
    storageRef.child('odometro/'+name).getDownloadURL().then(function(url) {
      // `url` is the download URL for 'images/stars.jpg'

      // This can be downloaded directly:
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function(event) {
        var blob = xhr.response;
      };
      xhr.open('GET', url);
      xhr.send();

      img.src = url;
      // Or inserted into an <img> element:
      $("#modal_foto").modal("show");
    }).catch(function(error) {
      // Handle any errors
    });
  }

  function fotoGas(name) {
    var img = document.getElementById('foto');
    img.src = "";
    storageRef.child('gas/'+name).getDownloadURL().then(function(url) {
      // `url` is the download URL for 'images/stars.jpg'

      // This can be downloaded directly:
      var xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function(event) {
        var blob = xhr.response;
      };
      xhr.open('GET', url);
      xhr.send();

      img.src = url;
      // Or inserted into an <img> element:
      $("#modal_foto").modal("show");
    }).catch(function(error) {
      // Handle any errors
    });
  }

  jQuery.fn.rotate = function(degrees) {
    $(this).css({'transform' : 'rotate('+ degrees +'deg)'});
    return $(this);
    };

  $('#rotar').on('click', function() {
    angle += 90;
    $("#foto").rotate(angle);
  });

</script>
@endpush
