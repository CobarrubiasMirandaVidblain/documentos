<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Storage;

use App\Models\Unidad;
use App\Models\TarjetaCirculacion;
use App\Models\ControlRutas;
use App\Models\MantenimientoUnidad;
use App\DataTables\Fotradis\UnidadesActuales;

// use Illuminate\Support\Facades\View;
use View;

class UnityController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR', ['except' => ['show', 'index']]);
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

    public function index(UnidadesActuales $dataTables)
    {
        return $dataTables->render('fotradis::unity.index');
    }

    public function listaMantenimiento($unidad_id)
    {

        $unidad = Unidad::findOrFail($unidad_id);
        $mantenimientos = MantenimientoUnidad::join('unidades', 'dtll_mantenimientosunidad.unidad_id', 'unidades.id')
            ->join('dtll_cat_mantenimientos', 'dtll_cat_mantenimientos.id', 'dtll_mantenimientosunidad.mantenimiento_id')
            ->where('unidades.id', $unidad_id)
            ->get(['dtll_mantenimientosunidad.fecha_mantenimiento as fecha', 'dtll_mantenimientosunidad.costo as costo', 'dtll_cat_mantenimientos.nombre as tipo', 'dtll_mantenimientosunidad.descripcion as descripcion', 'unidades.numero_unidad as numero_unidad']);
        $html = View::make(
            'fotradis::unity.partials.lista_mantenimiento',
            ['numero_unidad' => $unidad->numero_unidad, 'mantenimientos' => $mantenimientos]
        )
            ->render();
        return response()->json(array('dt' => $html), 200);
    }

    public function agregarMantenimiento(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $datos = $request->all();
                $mantenimiento_unidad = MantenimientoUnidad::create($datos);
                DB::commit();
                return response()->json(array('success' => true, 'estatus' => 'insertado', 'unidad' => $mantenimiento_unidad->unidad_id));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }


    public function store(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $datos = $request->except(['oficina_expedidora', 'fecha_expedicion', 'vigencia', 'clave_repuve', 'reg_ent', 'verificacion_vehicular']);
                $tarj_cir = $request->only(['oficina_expedidora', 'fecha_expedicion', 'vigencia', 'clave_repuve', 'reg_ent', 'verificacion_vehicular']);

                if ($request->file('archivo')) {
                    $url = Storage::putFile('public/fotradis/poliza', $request->file('archivo'));
                    $sub_carpeta = explode('public', $url);
                    $datos['poliza_garantia'] = 'storage' . $sub_carpeta[1];
                }

                $unidad_create = Unidad::create($datos);

                $update_ct = TarjetaCirculacion::where('unidad_id', '=', $unidad_create->id)
                    ->whereNull('deleted_at')->first();

                $tarj_cir['unidad_id'] = $unidad_create->id;

                if (isset($update_ct)) {
                    $update_ct->update($tarj_cir);
                } else {
                    $update_ct = TarjetaCirculacion::create($tarj_cir);
                }

                DB::commit();
                return response()->json(array('success' => true, 'estatus' => 'insertado', 'unidad' => $unidad_create->id));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function show(Request $request)
    {
        $unidad = Unidad::where('matricula', '=', $request["unidad"])->first();
        $anio = getdate()["year"];
        $tarjeta_circulacion;
        if ($unidad) {
            $tarjeta_circulacion = TarjetaCirculacion::where('unidad_id', '=', $unidad->id)->first();
        }
        if ($request->accion == "render") {
            $view = View::make('fotradis::unity.partials.crear_editar_unidad')->with('unidad', $unidad)->with('anio_actual', $anio)->with('tarjeta_circulacion', $tarjeta_circulacion)->with('tipo_servicio', true)->with('ver', true);
            $html = $view->render();

            return response()->json(['status' => 'ok', 'html' => $html], 200);
        } elseif ($request->accion == "add") {
            return view('fotradis::unity.create')->with('anio_actual', $anio);
        }
        $asignacion = Unidad::join('controlrutas', 'controlrutas.unidad_id', 'unidades.id')
            ->where('controlrutas.unidad_id', '=', $unidad->id)
            ->whereNull('controlrutas.deleted_at')
            ->count();

        if ($asignacion > 0)
            return view('fotradis::unity.create')->with('unidad', $unidad)->with('anio_actual', $anio)->with('tarjeta_circulacion', $tarjeta_circulacion)->with('editar', true)->with('en_asignacion', true);

        return view('fotradis::unity.create')->with('unidad', $unidad)->with('anio_actual', $anio)->with('tarjeta_circulacion', $tarjeta_circulacion)->with('editar', true);
    }

    public function edit()
    {
        return view('fotradis::edit');
    }

    public function update($unidad, Request $request)
    {
        if ($request->ajax()) {
            try {
                $datos = $request->except(['oficina_expedidora', 'fecha_expedicion', 'vigencia', 'clave_repuve', 'reg_ent', 'verificacion_vehicular']);
                $tarj_cir = $request->only(['oficina_expedidora', 'fecha_expedicion', 'vigencia', 'clave_repuve', 'reg_ent', 'verificacion_vehicular']);

                $unidad_update = Unidad::findOrFail($unidad);

                if ($unidad_update) {
                    DB::beginTransaction();
                    if ($request->file('archivo')) {
                        $url = Storage::putFile('public/fotradis/poliza', $request->file('archivo'));
                        $sub_carpeta = explode('public', $url);
                        $datos['poliza_garantia'] = 'storage' . $sub_carpeta[1];
                    }

                    $unidad_update->update($datos);

                    $update_ct = TarjetaCirculacion::where('unidad_id', '=', $unidad)
                        ->whereNull('deleted_at')->first();

                    $tarj_cir['unidad_id'] = $unidad;

                    if (isset($update_ct)) {
                        $update_ct->update($tarj_cir);
                    } else {
                        $update_ct = TarjetaCirculacion::create($tarj_cir);
                    }

                    DB::commit();
                    return response()->json(array('success' => true, 'estatus' => 'actualizado', 'unidad' => $unidad_update->id));
                }
                return response()->json(array('success' => false, 'estatus' => 'unidad', 'unidad' => $unidad_update->id));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $unidad = Unidad::destroy($request->unidad);
                TarjetaCirculacion::where('unidad_id', $request->unidad)->delete();

                DB::commit();

                return response()->json(array('success' => true, 'unidad' => $request->unidad));
            } catch (Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}