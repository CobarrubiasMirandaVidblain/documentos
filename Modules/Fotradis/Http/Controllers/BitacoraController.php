<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\View;

class BitacoraController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR', ['except' => ['show', 'index']]);
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

    public function index(Request $request)
    {
        $json_arrayKm = json_decode($request->dataKm, true);
        $json_arrayGas = json_decode($request->dataGas, true);
        $view = View::make('fotradis::partials.tablas_bitacora')->with('dataKm', $json_arrayKm)
            ->with('dataGas', $json_arrayGas)
            ->with('sumatoriaGas', $request->sumatoriaGas)
            ->with('kmsRecorridos', $request->kmsRecorridos)
            ->with('rendimientoMensual', $request->rendimientoMensual)
            ->with('inicio', $request->inicio)
            ->with('fin', $request->fin)
            ->with('unidad', $request->unidad);
        $html = $view->render();

        return response()->json(array('success' => true, 'estatus' => 'realizado', 'html' => $html));
    }

    public function create(Request $request)
    {
        $json_arrayKm = json_decode($request->dataKm, true);
        $json_arrayGas = json_decode($request->dataGas, true);
        $view = View::make('fotradis::partials.pdf_bitacora')->with('dataKm', $json_arrayKm)
            ->with('dataGas', $json_arrayGas)
            ->with('sumatoriaGas', $request->sumatoriaGas)
            ->with('kmsRecorridos', $request->kmsRecorridos)
            ->with('rendimientoMensual', $request->rendimientoMensual)
            ->with('inicio', $request->inicio)
            ->with('fin', $request->fin)
            ->with('unidad', $request->unidad);
        $html = $view->render();

        $pdf = app('snappy.pdf.wrapper');
        $pdf->loadHTML($html);

        return $pdf->download('cristal.pdf');
    }

    public function show()
    {
        return view('fotradis::show');
    }

    public function edit()
    {
        return view('fotradis::edit');
    }
}