<?php

namespace Modules\Fotradis\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use \Firebase\JWT\JWT;
use App\Models\Conductor;
use App\Models\ControlRutas;
use App\Models\Unidad;

use App\Models\DTLL\Servicio;
use App\Models\DTLL\BajaServicio;

class ApiController extends Controller{
  /**
  * Display a listing of the resource.
  * @return Response
  */
  public function login(Request $request){
    $clave = $request->input('key');
    $imei = $request->input('device');
    $driver = Conductor::findorfail(intval(substr($clave,-2)));
    if( $driver && $this->checkDevice($imei) ){
      $token = array(
        'user' => intval(substr($clave,-2)),
        'tablet' => $imei,
        'datetime' => Date('Y-M-d h:m:s')
      );
      
      return JWT::encode($token,config('app.jwt_token'),'HS256');
    }
    abort(401,'datos no validos');
  }
  
  private function checkDevice($imei){
    return true;
  }
  
  public function appLogIn(Request $request){
    $unidad = Unidad::where('numero_unidad',$request->input('unidad'))->firstOrFail()->id;
    try {
      $pre = ControlRutas::where(function($query) use ($request,$unidad){
        $query->orWhere('conductor_id',$request->conductor_id)
        ->orWhere('unidad_id',$unidad);
      })->whereNull('fechahora_sesion_fin')->count();
      if($pre==0){
        DB::beginTransaction();
        $session = ControlRutas::create([
          "recorrido_id"          => $request->input('ruta'),
          "unidad_id"             => $unidad,
          "conductor_id"          => $request->conductor_id,
          "fechahora_sesion_ini"  => date("Y-m-d H:i:s")
        ]);
        DB::commit();
        return response()->json($session->id);
      }
      else
        abort(409,"conductor o unidad en uso");
    }catch(Exception $e) {
      DB::rollBack();
      abort(500,'Error al registrar en la BD: ' . $e->getMessage());
    }
  }

  public function appLogOut(Request $request, $id){
    $session = ControlRutas::findorfail($id);
    try{
      DB::beginTransaction();
      
      if($session->conductor_id == $request->conductor_id){
        $session->update(['fechahora_sesion_fin'=>date("Y-m-d H:i:s")]);
        
        $pasajeros_sin_bajar = Servicio::leftjoin('dtll_serviciosbajadas','dtll_serviciosbajadas.servicio_id','dtll_servicios.id')
        ->where('dtll_servicios.controlruta_id', $id)->whereNotNull('subida')->whereNull('bajada')
        ->get(['dtll_servicios.id as id','dtll_servicios.controlruta_id as controlruta_id','dtll_servicios.pasajero_id as pasajero_id']);
        
        if($pasajeros_sin_bajar) {
          foreach ($pasajeros_sin_bajar as $value) {
            BajaServicio::create([
              'servicio_id' => $value->id,
              'bajada'      => date("Y-m-d H:i:s"),
              'latitud'     => null,
              'longitud'    => null
            ]);
          }
        }
        DB::commit();
        return response()->json(['result'=>true]);
      }else
        abort(403,'usuario incorrecto');
    }catch(Exception $e){
      DB::rollBack();
      abort(500,'Error al registrar en la BD: ' . $e->getMessage());
    }
  }
}
