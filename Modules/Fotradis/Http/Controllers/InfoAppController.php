<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

use App\Models\DTLL\RegistroKm;
use App\Models\DTLL\Pasajero;
use App\Models\DTLL\TipoServicio;
use App\Models\DTLL\ServiciosAdquiridos;
use App\Models\DTLL\Servicio;
use App\Models\Unidad;
use App\Models\Conductor;
use App\Models\Recorridos;
use App\Models\Persona;
use App\Models\MantenimientoUnidad;

class InfoAppController extends Controller
{
    public function servicios(Request $request)
    {
        $fechaIni = Carbon::createFromFormat('d/m/Y', $request->input('fechaIni'))->format('Y-m-d');
        $fechaFin = Carbon::createFromFormat('d/m/Y', $request->input('fechaFin'))->format('Y-m-d');

        //Servicios registrados por conductor
        $servicios_por_conductor = Conductor::select(DB::raw(
            'conductores.id as conductor,conductores.clave_conductor as clave, upper(concat(nombre," ",primer_apellido," ",segundo_apellido)) as nombreConductor,' .
                '(select count(*) as taxi from controlrutas inner join dtll_servicios on dtll_servicios.controlruta_id = controlrutas.id inner join recorridos on recorridos.id = controlrutas.recorrido_id where controlrutas.conductor_id = conductor and recorridos.nombre not like "%TAXI%" and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") <= "' . $fechaFin . '" limit 1) as servRuta,' .
                '(select count(*) taxi from controlrutas inner join dtll_servicios on dtll_servicios.controlruta_id = controlrutas.id inner join recorridos on recorridos.id = controlrutas.recorrido_id where controlrutas.conductor_id = conductor and recorridos.nombre like "%TAXI%" and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") <= "' . $fechaFin . '" limit 1) as servTaxi,' .
                '(select count(*) taxi from controlrutas inner join dtll_servicios on dtll_servicios.controlruta_id = controlrutas.id where controlrutas.conductor_id = conductor and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") <= "' . $fechaFin . '" limit 1) as totalServ'
        ))
            ->leftJoin('controlrutas', 'controlrutas.conductor_id', 'conductores.id')
            ->join('empleados', 'empleados.id', 'conductores.empleado_id')
            ->join('personas', 'personas.id', 'empleados.persona_id')
            ->groupBy(DB::raw('1,2,3,4,5,6'))
            ->orderBy('totalServ', 'desc')
            ->get();
        //Servicios por pasajero
        $servicios_por_pasajero = Pasajero::select(DB::raw(
            'dtll_pasajeros.id serv,' .
                'dtll_pasajeros.fechaInsert,' .
                'substr(curp,11,1) as generoCurp,' .
                'CONCAT(personas.nombre," ",personas.primer_apellido," ",personas.segundo_apellido) nombreCompleto,' .

                '(select numPasajero from dtll_serviciosadquiridos where pasajero_id = serv and tiposervicio_id = 1 and dtll_serviciosadquiridos.numPasajero not like "%G%" order by created_at desc limit 1) numRuta, ' .
                '(select numPasajero from dtll_serviciosadquiridos where pasajero_id = serv and tiposervicio_id = 2 and dtll_serviciosadquiridos.numPasajero not like "%G%" order by created_at desc limit 1) numTaxi, ' .

                '(select count(*) ' .
                'from dtll_pasajeros left join dtll_servicios on dtll_servicios.pasajero_id = dtll_pasajeros.id ' .
                'inner join controlrutas on controlrutas.id = dtll_servicios.controlruta_id ' .
                'inner join recorridos on recorridos.id = controlrutas.recorrido_id ' .
                'where dtll_pasajeros.id = serv and recorridos.nombre not like "%TAXI%"  and date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '") ruta, ' .

                '(select count(*) ' .
                'from dtll_pasajeros left join dtll_servicios on dtll_servicios.pasajero_id = dtll_pasajeros.id ' .
                'inner join controlrutas on controlrutas.id = dtll_servicios.controlruta_id ' .
                'inner join recorridos on recorridos.id = controlrutas.recorrido_id ' .
                'where dtll_pasajeros.id = serv and recorridos.nombre like "%TAXI%" and date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '") taxi, ' .

                '(select count(*) ' .
                'from dtll_pasajeros left join dtll_servicios on dtll_servicios.pasajero_id = dtll_pasajeros.id ' .
                'inner join controlrutas on controlrutas.id = dtll_servicios.controlruta_id ' .
                'inner join recorridos on recorridos.id = controlrutas.recorrido_id ' .
                'where dtll_pasajeros.id = serv and date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '") totalServ ' .

                ',if( ' .
                '(select dtll_cat_tipospasajero.nombre ' .
                'from dtll_pasajeros ' .
                'inner join dtll_serviciosadquiridos on dtll_serviciosadquiridos.pasajero_id = dtll_pasajeros.id ' .
                'inner join dtll_cat_tipospasajero on dtll_cat_tipospasajero.id = dtll_serviciosadquiridos.tipopasajero_id ' .
                'where dtll_cat_tipospasajero.nombre like "%BENEFICIARIO%" and dtll_pasajeros.id = serv ' .
                'limit 1) = "BENEFICIARIO", ' .
                '"beneficiario", ' .
                '"acompañante" ' .
                ') as tipopasajero ' .

                ',if( ' .
                '(select dtll_cat_tipospasajero.nombre ' .
                'from dtll_pasajeros ' .
                'inner join dtll_serviciosadquiridos on dtll_serviciosadquiridos.pasajero_id = dtll_pasajeros.id ' .
                'inner join dtll_cat_tipospasajero on dtll_cat_tipospasajero.id = dtll_serviciosadquiridos.tipopasajero_id ' .
                'where dtll_cat_tipospasajero.nombre like "%BENEFICIARIO%" and dtll_pasajeros.id = serv ' .
                'limit 1) = "BENEFICIARIO", ' .
                '"#008B8B", ' .
                '"#DC143C" ' .
                ') as colortipopasajero'
        ))
            ->leftJoin('dtll_servicios', 'dtll_servicios.pasajero_id', 'dtll_pasajeros.id')
            ->join('personas', 'personas.id', 'dtll_pasajeros.persona_id')
            ->whereNull('dtll_pasajeros.deleted_at')
            ->groupBy(DB::raw('1,2,3,4,5,6,7,8,9,10,11'))
            ->orderBy('totalServ', 'desc')
            ->get();

        if ($request->input("tipo-servicio") == "general") {
            //Recorridos que han registrado servicios
            $servicios_por_recorrido = Recorridos::select(DB::raw(
                'nombre,count(*) as servicios'
            ))
                ->join('controlrutas', 'controlrutas.recorrido_id', 'recorridos.id')
                ->join('dtll_servicios', 'dtll_servicios.controlruta_id', 'controlrutas.id')
                ->whereRaw('date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(controlrutas.fechahora_sesion_ini,"%Y-%m-%d") <= "' . $fechaFin . '"')
                ->groupBy(DB::raw('nombre'))
                ->orderBy('servicios', 'desc')
                ->get();
            //Servicios por género
            $servicios_por_genero = Pasajero::select(DB::raw(
                'substr(curp,11,1) as generoCurp, if(recorridos.nombre like "%TAXI%","taxi","ruta") as tipo, count(*) as servicios'
            ))
                ->join('dtll_servicios', 'dtll_servicios.pasajero_id', 'dtll_pasajeros.id')
                ->join('controlrutas', 'controlrutas.id', 'dtll_servicios.controlruta_id')
                ->join('recorridos', 'recorridos.id', 'controlrutas.recorrido_id')
                ->join('personas', 'personas.id', 'dtll_pasajeros.persona_id')
                ->whereRaw('date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '"')
                ->groupBy(DB::raw('1,2'))
                ->get();
            //Pasajeros inscritos por mes
            $pasajeros_inscritos_por_mes = Pasajero::select(
                DB::raw('date_format(fechaInsert,"%b %Y") as mesAnio,count(*) as inscritos')
            )
                ->whereNull('deleted_at')
                ->groupBy('mesAnio')
                ->orderBy(DB::raw('date_format(fechaInsert,"%Y%m")'))
                ->get();
            //Inscritos por mes
            $inscritos_por_mes_servicio = Pasajero::select(DB::raw(
                'if(servicios = 1' .
                    ',lower((select nombre from dtll_serviciosadquiridos inner join dtll_cat_tiposservicio on dtll_cat_tiposservicio.id = dtll_serviciosadquiridos.tiposervicio_id where dtll_serviciosadquiridos.pasajero_id = pj_id limit 1))' .
                    ',"ambos") as servicio' .
                    ',mesAnio' .
                    ',mesAnioN' .
                    ',count(*) as rep'
            ))
                ->leftJoin(DB::raw(
                    '(select dtll_pasajeros.id as pj_id, date_format(fechaInsert,"%b %Y") as mesAnio,date_format(fechaInsert,"%Y%m") as mesAnioN, count(*) as servicios ' .
                        'from dtll_pasajeros ' .
                        'join dtll_serviciosadquiridos on dtll_serviciosadquiridos.pasajero_id = dtll_pasajeros.id ' .
                        'group by 1,2,3) as filtro '
                ), 'filtro.pj_id', 'dtll_pasajeros.id')
                ->groupBy(DB::raw('1,2,3'))
                ->orderBy('mesAnioN')
                ->get();
            //Meses años
            $meses_anios = Pasajero::select(DB::raw(
                'date_format(fechaInsert,"%b %Y") as mesAnio,date_format(fechaInsert,"%Y%m") as mesAnioN'
            ))
                ->groupBy(DB::raw('1,2'))
                ->orderBy(DB::raw('date_format(fechaInsert,"%Y%m")'))
                ->get();
            //Días de servicio por recorrido
            $dias_de_servicio_por_recorrido = Recorridos::select(DB::raw(
                'nombre as iskey,count(*) as isvalue'
            ))
                ->join(DB::raw(
                    '(select recorridos.id,date_format(fechahora_sesion_ini,"%Y-%m-%d") ' .
                        'from recorridos left join controlrutas on controlrutas.recorrido_id = recorridos.id ' .
                        'where date_format(fechahora_sesion_ini,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(fechahora_sesion_ini,"%Y-%m-%d") <= "' . $fechaFin . '" ' .
                        'group by 1,2) as agrupar'
                ), 'recorridos.id', 'agrupar.id')
                ->groupBy('nombre')
                ->get();
            //Servicios días semanas
            $servicios_por_dia_semana = Servicio::select(DB::raw(
                'weekday(subida) as iskey,' .
                    '(CASE lower(dayname(subida))' .
                    'WHEN "monday" THEN "lunes"' .
                    'WHEN "tuesday" THEN "martes"' .
                    'WHEN "wednesday" THEN "miércoles"' .
                    'WHEN "thursday" THEN "jueves"' .
                    'WHEN "friday" THEN "viernes"' .
                    'WHEN "saturday" THEN "sábado"' .
                    'WHEN "sunday" THEN "domingo"' .
                    'ELSE "no exist" END) as format,' .
                    'concat("Semana ",week(subida)) as islegend,' .
                    'count(*) as isvalue'
            ))
                ->whereRaw('date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '"')
                ->groupBy(DB::raw('1,2,3'))
                ->orderBy(DB::raw('week(subida)'))
                ->get();
            //Servicios hrs semanas
            $servicios_por_hora_semana = Servicio::select(DB::raw(
                'date_format(subida,"%H") as iskey,' .
                    'concat(date_format(subida,"%H")," hrs") as format,' .
                    'concat("Semana ",week(subida)) as islegend,' .
                    'count(*) as isvalue'
            ))
                ->whereRaw('date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '"')
                ->groupBy(DB::raw('1,2,3'))
                ->orderBy(DB::raw('week(subida)'))
                ->get();
            //Servicios por tipo de pasajero
            $servicios_por_tipopasajero = Pasajero::select(DB::raw(
                'tipo,sum(servicios) as cont'
            ))
                ->join(DB::raw(
                    '(select dtll_pasajeros.id as p,' .

                        'if( ' .
                        '(select dtll_cat_tipospasajero.nombre ' .
                        'from dtll_pasajeros ' .
                        'inner join dtll_serviciosadquiridos on dtll_serviciosadquiridos.pasajero_id = dtll_pasajeros.id ' .
                        'inner join dtll_cat_tipospasajero on dtll_cat_tipospasajero.id = dtll_serviciosadquiridos.tipopasajero_id ' .
                        'where dtll_cat_tipospasajero.nombre like "%BENEFICIARIO%" and dtll_pasajeros.id = p ' .
                        'limit 1) = "BENEFICIARIO", ' .
                        '"beneficiaro", ' .
                        '"acompañante" ' .
                        ') as tipo ' .

                        ',count(*) as servicios from dtll_servicios ' .
                        'inner join dtll_pasajeros on dtll_pasajeros.id = dtll_servicios.pasajero_id ' .
                        'where date_format(dtll_servicios.subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_servicios.subida,"%Y-%m-%d") <= "' . $fechaFin . '" ' .
                        'group by 1 ' .
                        'order by 1) as t'
                ), 't.p', 'dtll_pasajeros.id')
                ->groupBy(DB::raw('1'))
                ->get();
            //Inscritos por tipo de pasajero
            $inscritos_por_tipopasajero = Pasajero::select(DB::raw(
                'tipo,count(*) as cont '
            ))
                ->join(DB::raw(
                    '(select dtll_pasajeros.id as p, ' .

                        'if( ' .
                        '(select dtll_cat_tipospasajero.nombre ' .
                        'from dtll_pasajeros ' .
                        'inner join dtll_serviciosadquiridos on dtll_serviciosadquiridos.pasajero_id = dtll_pasajeros.id ' .
                        'inner join dtll_cat_tipospasajero on dtll_cat_tipospasajero.id = dtll_serviciosadquiridos.tipopasajero_id ' .
                        'where dtll_cat_tipospasajero.nombre like "%BENEFICIARIO%" and dtll_pasajeros.id = p ' .
                        'limit 1) = "BENEFICIARIO", ' .
                        '"beneficiaro", ' .
                        '"acompañante" ' .
                        ') as tipo ' .
                        'from dtll_serviciosadquiridos ' .
                        'inner join dtll_pasajeros on dtll_pasajeros.id = dtll_serviciosadquiridos.pasajero_id ' .
                        'where date_format(fechaInsert,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(fechaInsert,"%Y-%m-%d") <= "' . $fechaFin . '" ' .
                        'group by 1 ' .
                        'order by 1 ' .

                        ') as t'
                ), 't.p', 'dtll_pasajeros.id')
                ->groupBy(DB::raw('1'))
                ->get();

            return response()->json(array(
                'fechaIni' => $request->input('fechaIni'),
                'fechaFin' => $request->input('fechaFin'),
                'meses_anios' => $meses_anios,
                'inscritos_por_tipopasajero' => $inscritos_por_tipopasajero,
                'servicios_por_tipopasajero' => $servicios_por_tipopasajero,
                'servicios_por_hora_semana' => $servicios_por_hora_semana,
                'servicios_por_dia_semana' => $servicios_por_dia_semana,
                'dias_de_servicio_por_recorrido' => $dias_de_servicio_por_recorrido,
                'inscritos_por_mes_servicio' => $inscritos_por_mes_servicio,
                'servicios_por_genero' => $servicios_por_genero,
                'pasajeros_inscritos_por_mes' => $pasajeros_inscritos_por_mes,
                'servicios_por_recorrido' => $servicios_por_recorrido,
                'servicios_por_conductor' => $servicios_por_conductor,
                'servicios_por_pasajero' => $servicios_por_pasajero
            ));
        }
        return response()->json(array(
            'fechaIni' => $request->input('fechaIni'),
            'fechaFin' => $request->input('fechaFin'),
            'servicios_por_conductor' => $servicios_por_conductor,
            'servicios_por_pasajero' => $servicios_por_pasajero
        ));
    }

    public function bitacora(Request $request)
    {
        $fechaIni = Carbon::createFromFormat('d/m/Y', $request->input('fechaIni'))->format('Y-m-d');
        $fechaFin = Carbon::createFromFormat('d/m/Y', $request->input('fechaFin'))->format('Y-m-d');


        //Kilometrajes por unidad
        $kms_por_unidad = Unidad::select(DB::raw(
            'id as nu,' .
                'upper(matricula) as matricula,' .
                'numero_unidad,' .
                '(select km from dtll_registrokm inner join controlrutas on controlrutas.id = dtll_registrokm.controlruta_id where unidad_id = nu and date_format(dtll_registrokm.fecha,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_registrokm.fecha,"%Y-%m-%d") <= "' . $fechaFin . '" order by dtll_registrokm.fecha asc limit 1) as kmIni,' .
                '(select km from dtll_registrokm inner join controlrutas on controlrutas.id = dtll_registrokm.controlruta_id where unidad_id = nu and date_format(dtll_registrokm.fecha,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_registrokm.fecha,"%Y-%m-%d") <= "' . $fechaFin . '" order by dtll_registrokm.fecha desc limit 1) as kmFin'
        ))
            ->groupBy(DB::raw("1,2,3,4"))
            ->orderBy('numero_unidad', 'desc')
            ->whereNull('deleted_at')
            ->get();
        //Cargas de gas acumuladas por unidad
        $cargas_de_gas_acumulada = Unidad::select(DB::raw(
            'upper(matricula) as matricula,numero_unidad,sum(dtll_registrogasolina.importe) as importe,sum(dtll_registrogasolina.litros) as litros'
        ))
            ->leftJoin('controlrutas', 'controlrutas.unidad_id', 'unidades.id')
            ->join('dtll_registrogasolina', 'dtll_registrogasolina.controlruta_id', 'controlrutas.id')
            ->leftJoin('dtll_cat_tiposcarga', 'dtll_cat_tiposcarga.id', 'dtll_registrogasolina.tipocarga_id')
            ->whereNull('unidades.deleted_at')
            ->whereRaw('date_format(dtll_registrogasolina.fecha,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_registrogasolina.fecha,"%Y-%m-%d") <= "' . $fechaFin . '"')
            ->groupBy(DB::raw('1,2'))
            ->orderBy('numero_unidad', 'desc')
            ->get();
        //Registros de gasolina
        $registros_de_carga_de_gas = Unidad::select(DB::raw(
            'concat(personas.nombre," ",primer_apellido) registro,upper(matricula) as matricula,numero_unidad,dtll_registrogasolina.fecha,dtll_registrogasolina.folio,dtll_cat_tiposcarga.nombre as tipocarga,dtll_registrogasolina.importe,dtll_registrogasolina.litros'
        ))
            ->leftJoin('controlrutas', 'controlrutas.unidad_id', 'unidades.id')
            ->join('dtll_registrogasolina', 'dtll_registrogasolina.controlruta_id', 'controlrutas.id')
            ->leftJoin('dtll_cat_tiposcarga', 'dtll_cat_tiposcarga.id', 'dtll_registrogasolina.tipocarga_id')
            ->join('conductores', 'conductores.id', 'controlrutas.conductor_id')
            ->join('empleados', 'empleados.id', 'conductores.empleado_id')
            ->join('personas', 'personas.id', 'empleados.persona_id')
            ->whereNull('unidades.deleted_at')
            ->whereRaw('date_format(dtll_registrogasolina.fecha,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(dtll_registrogasolina.fecha,"%Y-%m-%d") <= "' . $fechaFin . '"')
            ->orderBy('numero_unidad', 'desc')
            ->get();

        return response()->json(array(
            'fechaIni' => $request->input('fechaIni'),
            'fechaFin' => $request->input('fechaFin'),
            'kms_por_unidad' => $kms_por_unidad,
            'cargas_de_gas_acumulada' => $cargas_de_gas_acumulada,
            'registros_de_carga_de_gas' => $registros_de_carga_de_gas
        ));
    }

    public function coordenadas(Request $request)
    {
        $fechaIni = Carbon::createFromFormat('d/m/Y', $request->input('fechaIni'))->format('Y-m-d');
        $fechaFin = Carbon::createFromFormat('d/m/Y', $request->input('fechaFin'))->format('Y-m-d');

        if ($request->input('tipo-mapa-calor') == "subidas") {
            $coordenadas = Servicio::select(DB::raw(
                'latitud as longitud,longitud as latitud'
            ))
                ->whereRaw('date_format(subida,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(subida,"%Y-%m-%d") <= "' . $fechaFin . '"')
                ->get();
        } else {
            $coordenadas = Servicio::select(DB::raw(
                'dtll_serviciosbajadas.latitud as longitud,dtll_serviciosbajadas.longitud as latitud'
            ))
                ->join('dtll_serviciosbajadas', 'dtll_serviciosbajadas.servicio_id', 'dtll_servicios.id')
                ->whereRaw('date_format(bajada,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(bajada,"%Y-%m-%d") <= "' . $fechaFin . '"')
                ->whereNotNull('dtll_serviciosbajadas.latitud')
                ->whereNotNull('dtll_serviciosbajadas.longitud')
                ->get();
        }

        return response()->json(array('coordenadas' => $coordenadas));
    }

    public function mantenimiento(Request $request)
    {
        $fechaIni = Carbon::createFromFormat('d/m/Y', $request->input('fechaIni'))->format('Y-m-d');
        $fechaFin = Carbon::createFromFormat('d/m/Y', $request->input('fechaFin'))->format('Y-m-d');

        $m = MantenimientoUnidad::select(DB::raw(
            'costo,dtll_mantenimientosunidad.descripcion,matricula,nombre,date_format(dtll_cat_mantenimientos.created_at,"%d/%m/%Y") as fecharegistro,date_format(fecha_mantenimiento,"%d/%m/%Y") as fechamantenimiento,numero_unidad'
        ))
            ->join('dtll_cat_mantenimientos', 'dtll_cat_mantenimientos.id', 'dtll_mantenimientosunidad.mantenimiento_id')
            ->join('unidades', 'unidades.id', 'dtll_mantenimientosunidad.unidad_id')
            ->whereRaw('date_format(fecha_mantenimiento,"%Y-%m-%d") >= "' . $fechaIni . '" and date_format(fecha_mantenimiento,"%Y-%m-%d") <= "' . $fechaFin . '"')
            ->get();

        return response()->json(array(
            'fechaIni' => $request->input('fechaIni'),
            'fechaFin' => $request->input('fechaFin'), 'mantenimientos' => $m
        ));
    }
}