<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\View;

use App\Models\TarjetaCirculacion;

class TarjetaCirculacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR');
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

    public function index()
    {
        return view('fotradis::index');
    }

    public function create()
    {
        return view('fotradis::create');
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $datos = $request->all();

                $existe = TarjetaCirculacion::where('unidad_id', '=', $datos['unidad_id'])->first();

                if ($existe) {
                    $tarjeta_update = $existe;
                    $existe->update($datos);
                } else {
                    $tarjeta_update = TarjetaCirculacion::create($datos);
                }

                DB::commit();
                return response()->json(array('success' => true, 'estatus' => 'insertado', 'tarjetacirculacion' => $tarjeta_update->id));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function show()
    {
        return view('fotradis::show');
    }

    public function edit()
    {
        return view('fotradis::edit');
    }

    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $tarjeta_delete = TarjetaCirculacion::where('unidad_id', '=', $request->tarjetacirculacion)->first();
                TarjetaCirculacion::destroy($tarjeta_delete->id);

                DB::commit();

                return response()->json(array('success' => true, 'id' => $tarjeta_delete->id));
            } catch (Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}