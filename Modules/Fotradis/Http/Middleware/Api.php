<?php

namespace Modules\Fotradis\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use \Firebase\JWT\JWT;

class api{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next){
      $jwt = $request->header('jwt');
      if($jwt){
        try{
          $inf=JWT::decode($jwt, config('app.jwt_token'), array('HS256'));
          $request['conductor_id']=$inf->user;
          return $next($request);
        } catch(Exception $e){
          abort(401,'Sin acceso');
        }
      }
      abort(401,'sin token');
    }
}
