import Vue from 'vue'
import VueRouter from 'vue-router';
import vue2Dropzone from 'vue2-dropzone'
import BootstrapVue from 'bootstrap-vue';
import CxltToastr from 'cxlt-vue2-toastr'
import Multiselect from 'vue-multiselect'
import VeeValidate from 'vee-validate';
import validationMessages from 'vee-validate/dist/locale/es';
import datePicker from 'vue-bootstrap-datetimepicker';
import store from './store';
import Home from './components/Home.vue';
import ExcelValidator from './components/ExcelValidator.vue';

import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css'
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
require('@coreui/coreui/dist/js/coreui.min.js');

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(CxltToastr)
Vue.use(VeeValidate, {
   dictionary: {
    en: validationMessages
   }
});
Vue.use(datePicker);
Vue.use(ExcelValidator);
Vue.component('dropzone',vue2Dropzone)
Vue.component('multiselect', Multiselect)
Vue.config.productionTip = false;

const router = new VueRouter({
  routes: [
		{
      path: '/',
      name: 'index',
      component: Home,
    },
		{
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/archivos',
      name: 'archivos',
      component: ExcelValidator
    }
  ]
});

const app = new Vue({
  el: '#app',
  store,
  router,
  created(){
    store.dispatch('fetchRoles');
  },
});
