<?php

namespace Modules\ServiciosItinerantes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Filesystem\Filesystem;
use App\Models\Municipio;
use App\Models\Programa;
use Modules\ServiciosItinerantes\Classes\ExcelValidator;
use Modules\AgendaEventos\Entities\Gira;

class ServiciosItinerantesController extends Controller
{
		private $storageDir;
		private $storageSubDir;

		public function __construct() {
			$this->middleware(['auth', 'authorized']);
			//$this->middleware('rolModuleV2:orgeventos,ADMINISTRADOR,ADMIN,CAPTURISTA', ['only' => ['show', 'index','store','edit','update','destroy','listEventos','tipoEventoList','quitarParticipante','repParticipantes','search']]);
			$this->storageDir = '/public/excelval_files';
			$this->storageDirFullPath = storage_path('app') . $this->storageDir;
		}
		
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('serviciositinerantes::index');
    }
    /**
     * Almacena en disco los chunks de archivos de Excel válidos.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
			$validatedData = $request->validate([
				'file'  => 'required|file',
				'fileMimeType' => [
						'required',
						Rule::in(['xls', 'xlsx']),
				],
			]);

			//Variables del chunk y del archivo original
			$fileId = $request->dzuuid;
			$fileChunk = $request->file('file');
			$fileName = str_replace(' ', '_', $fileChunk->getClientOriginalName());
			$fileChunkType = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
			$chunkIndex = $request->dzchunkindex + 1;
			$fileChunkName = "{$fileId}-{$chunkIndex}.{$fileChunkType}";
			$fileChunkTempPath = "{$this->storageDir}/{$fileName}.{$fileId}";

			//store chunk file
			try{
			
					$fileChunkDirPath = $fileChunk->storeAs($fileChunkTempPath, $fileChunkName);
					$fileChunkDirPath = str_replace('public', 'storage', $fileChunkDirPath);
							
					return new JsonResponse(['data' => null, 'message'=>"Archivo por partes enviado correctamente al servidor."],200);
			}catch(Exception $e){
					return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
			}
    }
		
		/**
     * Busca municipios por nombre
     * @param  Request $request
     * @return Response
     */
    public function searchMunicipio($nombre = ''){
			$municipios = Municipio::select('id', 'nombre', 'distrito_id')
			->with([
					'localidades' => function ($query) {
							$query->select('id', 'nombre', 'municipio_id');
					},
					'distrito' => function ($query) {
							$query->select('id', 'nombre', 'region_id');
					},
					'distrito.region' => function ($query) {
							$query->select('id', 'nombre');
					}
			])->where('nombre', 'like', "%$nombre%")->get();
			return [
					'municipios' => $municipios
			];
	}

		/**
		 * Recupera los subprogramas asociados
		 * @param  Request $request
		 * @return Response
		 */
		public function listUnidades(){
				$unidades = Programa::select('id', 'nombre', 'tipo')
				->with([
						'hijos' => function ($query) {
								$query->select('id', 'nombre','tipo', 'padre_id')->whereNotIn('nombre', ['SOLICITUD DE SERVICIOS DE UNIDADES MÓVILES', 'CARABANA DE LA SALUD']);
						}
				])->where('nombre', '=', 'PROGRAMA INTEGRAL DE UNIDADES MÓVILES')->first();
				
				return [
						'unidades' => $unidades->hijos
				];
		}

		/**
		 * Lista de caravanas existentes
		 * @param  Request $request
		 * @return Response
		 */
		public function listCaravanas(){
			$eventosCaravana = Gira::where('nombre', '=', 'CARAVANA DE LA SALUD DIF')->with('eventos')->first();
			
			return [
					'eventos' => $eventosCaravana->eventos
			];
	}


		/**
		 * Une los chunks para reconstruir el archivo original
		 * @param  Request $request
		 * @return Response
		 */
		public function mergeChunks(Request $request){
				// Get variables
				$ds = DIRECTORY_SEPARATOR;
				$fileId = $request->dzuuid;
				$fileName = str_replace(' ', '_', $request->fileName);
				$fileType = $request->fileMimeType;
				$fileSize = $request->dztotalfilesize;
				$chunkTotal = $request->dztotalchunkcount;

				// Temp files path variable
				$targetPath = "{$this->storageDirFullPath}{$ds}{$fileName}.{$fileId}{$ds}";

				// Main file path variable
				$mainFilePath = "{$targetPath}{$fileName}";
				
				// Loop through temp files and grab the content
				try{
						for ($i = 1; $i <= $chunkTotal; $i++) {
								// Target temp file
								$tempFilePath = "{$targetPath}{$fileId}-{$i}.{$fileType}";
								if(!file_exists($tempFilePath)) return abort(422, 'Archivo temporal no encontrado..');
						
								// Copy chunk
								$chunk = file_get_contents($tempFilePath);
								if ( empty($chunk) ) return abort(422, 'Archivo temporal subido incorrectamente.');
						
								// Add chunk to main file
								file_put_contents($mainFilePath, $chunk, FILE_APPEND | LOCK_EX);
						
								// Delete chunk
								unlink($tempFilePath);
								if ( file_exists($tempFilePath) ) return abort(422, 'Los archivos temporales no se pudieron eliminar.');
						}
				}catch(\Exception $e){
						File::deleteDirectory($targetPath);
						return abort(422, $e->getMessage());
				}

				try{
						$mainFileSize = filesize($mainFilePath);
				}catch(\Exception $e){
						File::deleteDirectory($targetPath);
						return abort(422, $e->getMessage());
				}

				if ($mainFileSize !== $fileSize){
						return new JsonResponse(['data' => null, 'message'=>"El archivo original no se pudo reconstruir correctamente..."], 422);
				}

				return new JsonResponse(['data' => null, 'message'=>"Archivo original reconstruido correctamente..."],200);
		}

		/**
		 * Verifica los datos que contiene el archivo de Excel
		 * @param  Request $request
		 * @return Response
		 */
		public function processFile(Request $request){

				//Server response
				$response = [];

				// Get default limit
				$normalTimeLimit = ini_get('max_execution_time');

				// Set new limit
				ini_set('max_execution_time', 600);


				// Get variables
				$fileId = $request->dzuuid;
				$fileName = str_replace(' ', '_', $request->fileName);
				
				// Excel validator object
				$excelValidator = new ExcelValidator($this->storageDirFullPath, $fileId, $fileName, $request->usuario, [
					'evento' => $request->evento,
					'localidad' => $request->localidad,
					'municipio' => $request->municipio,
					'fecha' => $request->fecha,
					'unidad' => $request->unidad
				]);

				// Check file
				if(!$excelValidator->fileExists()) return new JsonResponse(['data' => null, 'message'=>'No se encontró el archivo. Operación cancelada'],419);

				// Create temporal table
				$excelValidator->createTempTables();

				//Insert dataset from file to temporal table
				$loadResponse = $excelValidator->loadTempData();

				if(!$loadResponse['status']) return new JsonResponse(['data' => null, 'message'=> strpos($loadResponse['message'], 'Undefined index:') === false ? $loadResponse['message'] : 'El archivo no contiene el formato de columnas homologado. Verifique.'],419);
				
				// Validate data in temporal table
				$validateResponse = $excelValidator->validateTempTableBeneficiarios();

				if(!$validateResponse['status']) return new JsonResponse(['data' => null, 'message'=>$validateResponse['message']],419);

				// Validate tutores columns in temporal table
				$validateResponse = $excelValidator->validateTempTableTutores();

				if(!$validateResponse['status']) return new JsonResponse(['data' => null, 'message'=>$validateResponse['message']],419);

				// Validate services columns in temporal table
				$validateResponse = $excelValidator->validateTempTableServicios();

				if(!$validateResponse['status']) return new JsonResponse(['data' => null, 'message'=>$validateResponse['message']],419);

				// Store info in real database
				$insertResponse = $excelValidator->insertDataBD();

				if(!$insertResponse['status']) return new JsonResponse(['data' => null, 'message'=> $insertResponse['message']],419);

				// Store response file in server storage folder
				$storeResponse = $excelValidator->storeResponse();

				// Restore default limit
				ini_set('max_execution_time', $normalTimeLimit);

				return [
					'inserted' => $insertResponse['totalRegistrosInsertados'],
					'duplicates' => $insertResponse['totalRegistrosRepetidos'],
					'invalid' => $insertResponse['totalRegistrosIncorrectos'] + $insertResponse['totalTutoresIncorrectos']
				];
		}

		public function downloadExcel(Request $request){
				$fileId = $request->fileId;
				$fileName = 'Respuesta_' . str_replace(' ', '_', $request->fileName);
				$pathToFile = "{$this->storageDirFullPath}/{$fileName}.{$fileId}.xlsx";
				$headers = array(
					'Content-Type: application/xlsx'
				);
						
				return response()->download($pathToFile, $fileName, $headers)->deleteFileAfterSend(true);
		}
}
