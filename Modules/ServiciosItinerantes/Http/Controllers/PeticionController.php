<?php

namespace Modules\ServiciosItinerantes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

use App\Models\Modulo;
use App\Models\Usuario;
use Modules\ServiciosItinerantes\Traits\Util;


use App\Http\Controllers\PeticionBaseController;

class PeticionController extends PeticionBaseController{
  use Util;

  function __construct(){
    //parent::__construct('ServiciosItinerantes','serviciositinerantes');
  }

  public function permissions(){
  $usuario = \App\Models\Usuario::find(73/*request()->usuario_id*/);
      $modulo = Modulo::where('nombre', 'like', 'INTRANET')->first();
      $roles = \App\Models\UsuariosRol::select('id', 'modulo_id', 'rol_id', 'usuario_id')->where('modulo_id', $modulo->id)->where('usuario_id', $usuario->id)->with('rol:id,usuario_id,nombre')->get();

      if(count($roles) <= 0) return new JsonResponse([['id' => 0, 'name' => 'VISITANTE']]);

      foreach($roles as $role){
          $role->name = $role->rol->nombre;
      }
      return new JsonResponse($roles);
		}
	
		public function roles(){
			$usuario = auth()->user();
			$user = $this->consultarUsuario($usuario->id);
			$token = [
					'usuario_id' => $usuario->id,
					'user' => $user
			];
			return $token;
		}
}
