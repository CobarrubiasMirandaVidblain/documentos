<?php

namespace Modules\ServiciosItinerantes\Entities;

use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
	use SoftDeletes;
	protected $table = 'itn_mascotas';
	protected $dates = ['deleted_at'];
	protected $fillable=["beneficiopersona_caravana_id","nombre","edad","especie_id","usuario_id"];
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');

	public function especie(){
		return $this->belongsTo('App\Models\EspecieMascota', 'especie_id');
	}

	public function caravanaBeneficio(){
		return $this->belongsTo('App\Models\BeneficiospersonaCaravana', 'beneficiopersona_caravana_id');
	}
}
