<?php

namespace Modules\ServiciosItinerantes\Classes;

use Illuminate\Support\Facades\Schema;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Excel;
use App\Models\BeneficiosPersonas;
use App\Models\Persona;
use Modules\ServiciosItinerantes\Entities\BeneficiopersonaCaravana;
use Modules\ServiciosItinerantes\Entities\Caravana;
use Modules\ServiciosItinerantes\Entities\Tutor;

class ExcelValidator
{
		private $tempDB = 'temp_work';
		private $realDB = 'difoaxaca_prueba';
		private $storageDir; // Directorio raiz de almacenamiento
		private $dirPath;  // Directorio del achivo
		private $filePath;  // Ruta al archivo
		private $fileId;  // Identificador único del archivo
		private $fileName;  // Nombre del archivo
		private $tempTable;  // Tabla temporal para datos de beneficiarios
		private $tempServicesTable;  // Tabla temporal para servicios de beneficiarios
		private $datosCaravana;  //Informacion de la caravana
		private $usuario;
		private $currentDate;
		private $responseData;
		private $listaOmisiones;

		function __construct($storageDir, $fileId, $fileName, $usuario, $datosCaravana){
			$this->storageDir = $storageDir;
			$this->fileId = $fileId;
			$this->fileName = $fileName;
			$this->usuario = $usuario;
			$this->datosCaravana = $datosCaravana;
			$this->currentDate = Carbon::now()->format('dmY');
			$this->dirPath = "{$storageDir}/{$fileName}.{$fileId}/";
			$this->filePath = $this->dirPath . "{$fileName}";
			$this->responseData = [
				'datosInvalidos' => [],
				'datosDuplicados' => [],
				'tutoresInvalidos' => []
			];
			$this->listaOmisiones = ['N/A', 'NA', 'S/N', 'SN', 'NP', 'NO', ''];
		}

		public function fileExists(){
			return file_exists($this->filePath);
		}

		public function createTempTables(){
			$this->tempTable = "{$this->fileId}" . '-' . "{$this->currentDate}";
			$this->tempServicesTable = 'serv_' . "{$this->tempTable}";

			// Create temporal table in database temp_work
			Schema::connection('mysql_server_temp')->create($this->tempTable, function($table)
				{
						$table->increments('id');
						$table->string('persona_tmp_id',255)->nullable();
						$table->string('nombre',255)->nullable();
						$table->string('primer_apellido',255)->nullable();
						$table->string('segundo_apellido',255)->nullable();
						$table->string('fecha_nacimiento',255)->nullable();
						$table->string('curp',255)->nullable();
						$table->string('genero',255)->nullable();
						$table->string('colonia',255)->nullable();
						$table->string('calle',255)->nullable();
						$table->string('numero_exterior',255)->nullable();
						$table->string('numero_celular',255)->nullable();
						$table->string('numero_local',255)->nullable();
						$table->string('clave_electoral',255)->nullable();
						$table->string('localidad_id',255)->nullable();
						$table->string('municipio_id',255)->nullable();
						$table->string('etnia_id',255)->nullable();
						$table->string('tipobeneficiario_id',255)->nullable();
						$table->string('seccion',255)->nullable();
						$table->string('observaciones',255)->nullable();
						$table->string('persona_id', 255)->nullable();
						$table->string('bienestarpersona_id', 255)->nullable();
						$table->string('observaciones_error', 255)->nullable();
				});

				// Create temporal services table in database temp_work
				Schema::connection('mysql_server_temp')->create($this->tempServicesTable, function($table)
				{
						$table->increments('id');
						$table->string('persona_tmp_id',255)->nullable();
						$table->string('servicio_id',255)->nullable();
						$table->string('beneficio_id',255)->nullable();
				});

				// Columnas responsables 687: SERVICIOS DE OFTALMOLOGÍA, 785: SUPLEMENTOS VITAMÍNICOS, 790: SERVICIOS DE PEDIATRÍA
				if (in_array($this->datosCaravana['unidad'], array(687, 785, 790))) {
					Schema::connection('mysql_server_temp')->table($this->tempTable, function($table){
						$table->string('nombre_responsable',255)->nullable();
						$table->string('primer_apellido_responsable',255)->nullable();
						$table->string('segundo_apellido_responsable',255)->nullable();
						$table->string('fecha_nacimiento_responsable',255)->nullable();
						$table->string('curp_responsable',255)->nullable();
						$table->string('genero_responsable',255)->nullable();
						$table->string('clave_electoral_responsable',255)->nullable();
						$table->string('parentesco',255)->nullable();
						$table->string('tutor_persona_id',255)->nullable();
						$table->string('tutor_bienestarpersona_id',255)->nullable();
						$table->string('observaciones_tutor_error', 255)->nullable();
					});
				}

				// Columnas mascotas 806: UNIDAD MÉDICA INFECCIOSA
				if ($this->datosCaravana['unidad'] === 806) {
					Schema::connection('mysql_server_temp')->table($this->tempTable, function($table){
						$table->string('mascota',255)->nullable();
						$table->string('especie',255)->nullable();
						$table->string('edad_mascota',255)->nullable();
					});
				}
		}

		public function loadTempData(){
			$response = ['status' => true, 'message' => ''];
			$dataSet = []; // Conjunto de datos de los beneficiarios
			$servicesDataSet = []; // Conjunto de datos de servicios proporcionados a los beneficiarios
			$chunkSize = 1000;
			$dataSetSize = 0;
			$dataSetRemainder = 0;
			$chunks = 0;
			$tempPersonaId = 1;
			
			try{
				// Insert data from Excel file to temporal table in database temp_work
				Excel::load($this->filePath, function($reader) use (&$dataSet, &$servicesDataSet, &$tempPersonaId ) {
					// Loop through all sheets
					$reader->each(function($sheet) use (&$dataSet, &$servicesDataSet, &$tempPersonaId ) {
							// Loop through all rows
							$sheet->each(function($row) use (&$dataSet, &$servicesDataSet, &$tempPersonaId ) {

									// Avoid empty rows
									$isRowEmpty = true;
									foreach ($row as $cell) {
											if ($cell !== '' && $cell !== NULL) {
													$isRowEmpty = false;
													break;
											}
									}

									if (!$isRowEmpty) {
										// Load row
										$tempDataSet = array(
											'persona_tmp_id'  => $tempPersonaId,
											'nombre'  => mb_strtoupper($row['nombre']),
											'primer_apellido'  => mb_strtoupper($row['apellido_paterno']),
											'segundo_apellido'  => mb_strtoupper($row['apellido_materno']),
											'fecha_nacimiento'  => $row['fecha_de_nacimiento'],
											'curp'  => mb_strtoupper($row['curp']),
											'genero'  => mb_strtoupper($row['genero']),
											'colonia'  => mb_strtoupper($row['colonia']),
											'calle'  => mb_strtoupper($row['calle']),
											'numero_exterior' => mb_strtoupper($row['numero_exterior']),
											'numero_celular' => mb_strtoupper($row['telefono']),
											'clave_electoral' => mb_strtoupper($row['clave_de_elector']),
											'localidad_id'  => mb_strtoupper($row['localidad']),
											'municipio_id'  => mb_strtoupper($row['municipio']),
											'etnia_id'  => mb_strtoupper($row['lengua_indigena']),
											'tipobeneficiario_id'  => mb_strtoupper($row['tipo_de_beneficiario']),
											'seccion'  => mb_strtoupper($row['seccion']),
											'observaciones'  => mb_strtoupper($row['observaciones'])
										);

										// Columnas responsables 687: SERVICIOS DE OFTALMOLOGÍA, 785: SUPLEMENTOS VITAMÍNICOS, 790: SERVICIOS DE PEDIATRÍA
										if (in_array($this->datosCaravana['unidad'], array(687, 785, 790))) {
											$tempDataSet = array_merge($tempDataSet, array(
												'nombre_responsable' => mb_strtoupper($row['nombre_responsable']),
												'primer_apellido_responsable' => mb_strtoupper($row['apellido_paterno_responsable']),
												'segundo_apellido_responsable' => mb_strtoupper($row['apellido_materno_responsable']),
												'fecha_nacimiento_responsable' => mb_strtoupper($row['fecha_de_nacimiento_responsable']),
												'curp_responsable' => mb_strtoupper($row['curp_responsable']),
												'genero_responsable' => mb_strtoupper($row['genero_responsable']),
												'clave_electoral_responsable' => mb_strtoupper($row['clave_de_elector_responsable']),
												'parentesco' => mb_strtoupper($row['parentesco'])
											));
										}

										// Columnas mascotas 806: UNIDAD MÉDICA INFECCIOSA
										if ($this->datosCaravana['unidad'] === 806) {
											$tempDataSet = array_merge($tempDataSet, array(
												'mascota' => mb_strtoupper($row['mascota']),
												'especie' => mb_strtoupper($row['especie']),
												'edad_mascota' => mb_strtoupper($row['edad_de_mascota'])
											));
										}

										// Columnas de servicios y beneficios
										switch ($this->datosCaravana['unidad']) {
											case 687: // SERVICIOS DE OFTALMOLOGÍA
												if (mb_strtoupper($row['cirugia_y_trasplante']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CIRUGÍA Y TRASPLANTE', 'beneficio_id' => null);
												if (mb_strtoupper($row['deteccion_de_patologias_retinoplastia_diabetica_estrabismo_glaucoma_cataratas_pterigion_y_carnosidad)']) === 'X') $servicesDataSet[] = array('persona_tmp_id'  => $tempPersonaId, 'servicio_id' => 'DETECCION DE PATOLOGÍAS (RETINOPLASTIA DIABETICA, ESTRABISMO, GLAUCOMA, CATARATAS, PTERIGIÓN Y CARNOSIDAD)', 'beneficio_id' => null);
												if (mb_strtoupper($row['donacion_de_lentes_graduados_y_solares']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'DONACION DE LENTES (GRADUADOS Y SOLARES)', 'beneficio_id' => null);
												if (mb_strtoupper($row['examen_de_la_vista_por_computadora']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'EXAMEN DE LA VISTA POR COMPUTADORA', 'beneficio_id' => null);

												break;
						
											case 688: // SERVICIOS DE MEDICINA GENERAL
												if (mb_strtoupper($row['chequeo_de_glucosa']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CHEQUEO DE GLUCOSA', 'beneficio_id' => null);
												if (mb_strtoupper($row['consulta_medica_general']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CONSULTA MÉDICA GENERAL', 'beneficio_id' => null);
												if (mb_strtoupper($row['cuidado_de_heridas']) === 'X') $servicesDataSet[] =  array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CUIDADO DE HERIDAS', 'beneficio_id' => null);
												if (mb_strtoupper($row['toma_de_signos_vitales_y_somatometria_presion_arterial_peso_y_talla_temperatura']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'TOMA DE SIGNOS VITALES Y SOMATOMETRÍA (PRESIÓN ARTERIAL, PESO Y TALLA, TEMPERATURA)', 'beneficio_id' => null);

												break;
						
											case 689: // SERVICIOS DE GINECOLOGÍA
												if (mb_strtoupper($row['colocacion_y_retiro_del_diu_dispositivo_intra_uterino']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'COLOCACIÓN Y RETIRO DEL DIU (DISPOSITIVO INTRA-UTERINO)', 'beneficio_id' => null);
												if (mb_strtoupper($row['diagnostico_y_tratamiento_de_padecimientos_ginecologicos']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'DIAGNOSTICO Y TRATAMIENTO DE PADECIMIENTOS GINECOLÓGICOS', 'beneficio_id' => null);
												if (mb_strtoupper($row['exploracion_de_mama']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'EXPLORACIÓN DE MAMA', 'beneficio_id' => null);
												if (mb_strtoupper($row['retiro_de_implante_subdermico']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'RETIRO DE IMPLANTE SUBDERMICO', 'beneficio_id' => null);
												if (mb_strtoupper($row['toma_de_papanicolau']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'TOMA DE PAPANICOLAU', 'beneficio_id' => null);
												if (mb_strtoupper($row['ultrasonidos_obstetricos_a_embarazadas']) === 'X') $servicesDataSet[] =array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'ULTRASONIDOS OBSTETRICOS A EMBARAZADAS', 'beneficio_id' => null);
											
												break;
						
											case 785: // SUPLEMENTOS VITAMÍNICOS
												if (mb_strtoupper($row['acido_folico_a_embarazadas']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'ÁCIDO FÓLICO A EMBARAZADAS', 'beneficio_id' => null);
												if (mb_strtoupper($row['desparasitacion_a_la_familia']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'DESPARASITACIÓN A LA FAMILIA', 'beneficio_id' => null);
												if (mb_strtoupper($row['vitamina_a_a_pediatricos_de_6_meses_a_59_meses']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'VITAMINA A, A PEDIÁTRICOS DE 6 MESES A 59 MESES', 'beneficio_id' => null);
												
												break;
						
											case 790: // SERVICIOS DE PEDIATRÍA
												if (mb_strtoupper($row['capacitacion_sobre_ablactacion_alimentacion_despues_del_seno_materno']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CAPACITACIÓN SOBRE ABLACTACIÓN (ALIMENTACIÓN DESPUÉS DEL SENO MATERNO)', 'beneficio_id' => null);
												if (mb_strtoupper($row['consulta_pediatrica']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CONSULTA PEDIÁTRICA', 'beneficio_id' => null);
												if (mb_strtoupper($row['control_del_nino_y_nina_sano']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CONTROL DEL NIÑO Y NIÑA SANO', 'beneficio_id' => null);
												if (mb_strtoupper($row['cuidados_del_recien_nacido']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CUIDADOS DEL RECIÉN NACIDO', 'beneficio_id' => null);
												if (mb_strtoupper($row['curacion_de_heridas_en_ninos_y_ninas']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CURACIÓN DE HERIDAS EN NIÑOS Y NIÑAS', 'beneficio_id' => null);
												if (mb_strtoupper($row['promocion_a_la_salud_de_preescolares_y_escolares']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'PROMOCIÓN A LA SALUD DE PREESCOLARES Y ESCOLARES', 'beneficio_id' => null);
												if (mb_strtoupper($row['valoracion_de_peso_y_talla']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'VALORACIÓN DE PESO Y TALLA', 'beneficio_id' => null);
												if (mb_strtoupper($row['valoracion_de_recien_nacido_y_lactantes']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'VALORACIÓN DE RECIÉN NACIDO Y LACTANTES', 'beneficio_id' => null);

												break;
						
											case 799: // SERVICIOS DE ODONTOLOGÍA
												if (mb_strtoupper($row['amalgamas']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'AMALGAMAS', 'beneficio_id' => null);
												if (mb_strtoupper($row['aplicaciones_de_fluor']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'APLICACIONES DE FLÚOR', 'beneficio_id' => null);
												if (mb_strtoupper($row['consulta_general']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CONSULTA GENERAL', 'beneficio_id' => null);
												if (mb_strtoupper($row['extracciones']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'EXTRACCIONES', 'beneficio_id' => null);
												if (mb_strtoupper($row['profilaxis']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'PROFILAXIS', 'beneficio_id' => null);
												if (mb_strtoupper($row['resina']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'RESINA', 'beneficio_id' => null);
												
												break;
						
											case 806: // UNIDAD MÉDICA INFECCIOSA
												if (mb_strtoupper($row['campanas_de_concientizacion_animal']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CAMPAÑAS DE CONCIENTIZACIÓN ANIMAL', 'beneficio_id' => null);
												if (mb_strtoupper($row['consulta_general']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'CONSULTA GENERAL', 'beneficio_id' => null);
												if (mb_strtoupper($row['esterilizacion_caninos_y_felinos']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'ESTERILIZACIÓN CANINOS Y FELINOS', 'beneficio_id' => null);
												if (mb_strtoupper($row['reanimacion']) === 'X') $servicesDataSet[] = array('persona_tmp_id' => $tempPersonaId, 'servicio_id' => 'REANIMACIÓN', 'beneficio_id' => null);

												break;
										}
										
										$dataSet[] = $tempDataSet;
										$tempPersonaId++;
									}
							});
					});
				});

				// Inserta beneficiarios
				$dataSetSize = count($dataSet);
				$dataSetRemainder = $dataSetSize % $chunkSize;
				$chunks = ($dataSetSize - $dataSetRemainder)  / $chunkSize;

				$i = 0;
				while ($i < $chunks) {
						DB::connection('mysql_server_temp')->table($this->tempTable)->insert(array_slice($dataSet, $i, $chunkSize));
						$i++;
				}
				if ($dataSetRemainder > 0)
						DB::connection('mysql_server_temp')->table($this->tempTable)->insert(array_slice($dataSet, $i, $dataSetRemainder));
				
				// Inserta servicios de los beneficiarios
				$dataSetSize = count($servicesDataSet);
				$dataSetRemainder = $dataSetSize % $chunkSize;
				$chunks = ($dataSetSize - $dataSetRemainder)  / $chunkSize;

				$i = 0;
				while ($i < $chunks) {
						DB::connection('mysql_server_temp')->table($this->tempServicesTable)->insert(array_slice($servicesDataSet, $i, $chunkSize));
						$i++;
				}
				if ($dataSetRemainder > 0)
						DB::connection('mysql_server_temp')->table($this->tempServicesTable)->insert(array_slice($servicesDataSet, $i, $dataSetRemainder));

			}catch(\Exception $e){

				$response = ['status' => false, 'message' => $e->getMessage()];

				// Drop temporal tables
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempTable);
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempServicesTable);

			} finally {
				// Delete temporal folder
				File::deleteDirectory($this->dirPath);
			}
			
			return $response;
		}

		public function validateTempTableBeneficiarios(){
			$response = ['status' => true, 'message' => ''];
			try{
				/**########################################### QUERYS PARA VALIDAR EL ARCHIVO ###########################################*/

				// =============== Eliminamos espacios en blanco antes y después de los datos en cada celda ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
				->update([
					'nombre' => DB::raw('TRIM(nombre)'),
					'primer_apellido' => DB::raw('TRIM(primer_apellido)'),
					'segundo_apellido' => DB::raw('TRIM(segundo_apellido)'),
					'fecha_nacimiento' => DB::raw('TRIM(fecha_nacimiento)'),
					'curp' => DB::raw('TRIM(curp)'),
					'genero' => DB::raw('TRIM(genero)'),
					'colonia' => DB::raw('TRIM(colonia)'),
					'calle' => DB::raw('TRIM(calle)'),
					'numero_exterior' => DB::raw('TRIM(numero_exterior)'),
					'numero_celular' => DB::raw('TRIM(numero_celular)'),
					'clave_electoral' => DB::raw('TRIM(clave_electoral)'),
					'localidad_id' => DB::raw('TRIM(localidad_id)'),
					'municipio_id' => DB::raw('TRIM(municipio_id)'),
					'etnia_id' => DB::raw('TRIM(etnia_id)'),
					'tipobeneficiario_id' => DB::raw('TRIM(tipobeneficiario_id)'),
					'seccion' => DB::raw('TRIM(seccion)'),
					'observaciones' => DB::raw('TRIM(observaciones)')
				]);
				
				// =============== Corregimos formatos de columna telefono ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->update([
						'numero_celular' => DB::raw("REPLACE(numero_celular,' ','')")
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereIn('numero_celular', $this->listaOmisiones)
						->update([
							'numero_celular' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw('LENGTH(numero_celular) = 7')
						->update([
						'numero_local' => DB::raw("numero_celular"),
						'numero_celular' => NULL
				]);

				// =============== Corregimos apellidos si tiene solo segundo apellido ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereNull('persona_id')
						->whereNotNull('segundo_apellido')
						->whereNull('primer_apellido')
						->orWhere('primer_apellido', '=', '')
						->orWhere('primer_apellido', 'LIKE', '%N/A%')
						->orWhereRaw("primer_apellido REGEXP '^[xX]+$'")
						->update([
								'primer_apellido' => DB::raw("segundo_apellido"),
								'segundo_apellido' => ''
				]);

				// =============== Corregimos apellidos si tiene solo primer apellido ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereNull('persona_id')
						->whereNotNull('primer_apellido')
						->where('primer_apellido', '<>', '')
						->whereRaw("primer_apellido NOT REGEXP '^[xX]+$'")
						->where( function ( $query )
						{
								$query->whereRaw("segundo_apellido REGEXP '^[xX]+$'")
										->orWhere('segundo_apellido', '=', '')
										->orWhere('segundo_apellido', 'LIKE', '%N/A%');
						})
						->update([
								'segundo_apellido' => ''
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereNull('segundo_apellido')
						->orWhereRaw("segundo_apellido REGEXP '^[xX]+$'")
						->orWhere('segundo_apellido', 'LIKE', '%N/A%')
						->update([
								'segundo_apellido' => ''
				]);

				// =============== Corregimos fechas de nacimiento ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->update([
								'fecha_nacimiento' => DB::raw('CONCAT(SUBSTR(fecha_nacimiento, 1, 10))')
				]);
				
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("fecha_nacimiento not REGEXP '^([0-9]{4})-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-1])$'")
						->update([
								'fecha_nacimiento' => DB::raw("(CASE WHEN SUBSTRING(curp, 5, 6) REGEXP '[0-9]{6}' THEN
								CASE 
										WHEN CONVERT(SUBSTRING(curp,7,2), UNSIGNED)<=12 THEN 
												CASE 
														WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=31 THEN
														DATE(CONCAT( (CASE WHEN CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=20 THEN '20' ELSE '19' END), SUBSTRING(curp,5,2), '-', SUBSTRING(curp,7,2), '-', SUBSTRING(curp,9,2)))
												END
										END
								ELSE null
						END)")
				]);
				
				// =============== Corregimos formato de la curp ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereIn('curp', $this->listaOmisiones)
						->update([
								'curp' => NULL
				]);

				// =============== Corregimos formato del género ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereIn('genero', ['MUJER', 'FEMENINO', 'M'])
						->update([
								'genero' => 'F'
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereIn('genero', ['HOMBRE', 'MASCULINO', 'H'])
						->update([
								'genero' => 'M'
				]);

				// =============== Corregimos formato de sección ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereIn('seccion', $this->listaOmisiones)
						->update([
								'seccion' => NULL
				]);
				
				// =============== Corregimos formato de clave electoral ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw('LENGTH(clave_electoral) < 18')
						->update([
								'clave_electoral' => NULL
				]);
				
				// =============== Corregimos formato de calle y numero exterior ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP 'DOMICILIO CONOCIDO$'")
						->update([
								'calle' => NULL,
								'numero_exterior' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP 'S-N$'")
						->update([
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("S-N", calle) - 1 ))'),
								'numero_exterior' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP 'S/N$'")
						->update([
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("S/N", calle) - 1 ))'),
								'numero_exterior' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP 'S7N$'")
						->update([
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("S7N", calle) - 1 ))'),
								'numero_exterior' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '# ?S(/|-)N$'")
						->update([
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("#", calle) - 1 ))'),
								'numero_exterior' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP ' ?SIN NUMERO$'")
						->update([
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("SIN NUMERO", calle) - 1 ))'),
								'numero_exterior' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '# ?[0-9]+ ?[A-Z]?$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, (LOCATE("#", calle) + 1) ))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("#", calle) - 1 ))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '# ?[0-9]+ [A-Z]+ ?[0-9]$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, (LOCATE("#", calle) + 1) ))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("#", calle) - 1 ))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '# ?[0-9]+ [A-Z]+ ?[0-9]$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, (LOCATE("·", calle) + 1) ))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("·", calle) - 1 ))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP 'NO. ?[0-9]+ ?[A-Z]?$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, (LOCATE("NO.", calle) + 3)))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle FROM 1 FOR LOCATE("NO.", calle) - 1))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '[0-9]{3} ?[A-Z]$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, - 5))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle,1, LENGTH(calle) - 5))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '[0-9]{2} ?[A-Z]$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, - 4))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle,1, LENGTH(calle) - 4))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '[0-9]{1} ?[A-Z]$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, - 3))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle,1, LENGTH(calle) - 3))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '[0-9]{3}$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, - 3))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle,1, LENGTH(calle) - 3))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '[0-9]{2}$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, - 2))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle,1, LENGTH(calle) - 2))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("calle REGEXP '[0-9]{1}$'")
						->update([
								'numero_exterior' => DB::raw('TRIM(SUBSTRING(calle, - 1))'),
								'calle' => DB::raw('TRIM(SUBSTRING(calle,1, LENGTH(calle) - 1))')
				]);

				// =============== Cambiamos municipios por id ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("municipio_id REGEXP ', OAX.$'")
						->update([
								'municipio_id' => DB::raw('TRIM(SUBSTRING(municipio_id, 1, LENGTH(municipio_id) - 6))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("municipio_id IN (SELECT {$this->realDB}.cat_municipios.nombre FROM {$this->realDB}.cat_municipios)")
						->update([
								'municipio_id' => DB::raw("(SELECT id FROM {$this->realDB}.cat_municipios x WHERE x.nombre = municipio_id)")
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("municipio_id NOT IN (SELECT id FROM {$this->realDB}.cat_municipios)")
						->update([
								'municipio_id' => NULL
				]);

				// =============== Cambiamos localidades por id ===============
				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereNull("municipio_id")
						->update([
								'localidad_id' => NULL
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("localidad_id REGEXP '^LOC '")
						->update([
								'localidad_id' => DB::raw('TRIM(SUBSTRING(localidad_id, 4))')
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw("localidad_id REGEXP '^LOC. '")
						->update([
								'localidad_id' => DB::raw('TRIM(SUBSTRING(localidad_id, 5))')
				]);

				DB::connection('mysql_server_temp')
						->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.cat_localidades l 
								set x.localidad_id=l.id 
								where x.localidad_id=l.nombre and x.municipio_id=l.municipio_id"
							);

				DB::connection('mysql_server_temp')
					->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.cat_localidades l 
						set x.localidad_id=l.id 
						where x.localidad_id like CONCAT('%', l.nombre, '%') and x.municipio_id=l.municipio_id"
					);

				DB::connection('mysql_server_temp')
					->statement("update {$this->tempDB}.`{$this->tempTable}` x
						set x.localidad_id=NULL 
						where x.localidad_id NOT IN (SELECT id FROM {$this->realDB}.cat_localidades l WHERE l.municipio_id = x.municipio_id)"
					);
				
				// =============== Cambiamos etnias por id ===============
				DB::connection('mysql_server_temp')
						->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.cat_etnias e 
								set x.etnia_id=e.id 
								where x.etnia_id=e.nombre"
							);

				DB::connection('mysql_server_temp')
						->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.cat_etnias e 
								set x.etnia_id=1 
								where x.etnia_id NOT IN ( SELECT id FROM {$this->realDB}.cat_etnias )"
							);
				
				// =============== Obtenemos los persona_id que coincide en curp y nombre completo ===============
				// Verificamos los que ya están en la tabla personas (asigna id en la columna persona_id)
				DB::connection('mysql_server_temp')
						->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.personas p 
								set 
										x.persona_id = p.id 
								where 
								x.persona_id is null AND x.curp IS NOT NULL AND x.curp != '' 
								AND x.curp = p.curp"
				);//AND x.curp = p.curp and p.nombre = x.nombre and p.primer_apellido=x.primer_apellido and p.segundo_apellido=x.segundo_apellido"

				// =============== Obtenemos los persona_id que existen en la tabla bienestarpersonas ===============
				// Verificamos los que ya están en la tabla bienestarpersonas (asigna id en la columna bienestarpersona_id)
				DB::connection('mysql_server_temp')->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.bienestarpersonas b 
						set 
								x.bienestarpersona_id = b.id 
						where 
						x.persona_id is not null AND 
						x.persona_id = b.persona_id"
				);

				DB::connection('mysql_server_temp')->table($this->tempTable)
				->whereRaw(" curp is not null 
						and (curp not REGEXP '^[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$' 
						or (CASE WHEN SUBSTRING(curp, 5, 6) REGEXP '^[0-9]{6}$' THEN 
								CASE 
										WHEN SUBSTRING(curp,7,2) REGEXP '^(01|03|05|07|08|10|12)$' THEN 
												CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=31 THEN '' ELSE 'MES 31, DIA>31' END 
										WHEN SUBSTRING(curp,7,2) REGEXP '^(04|06|09|11)$' THEN 
												CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=30 THEN '' ELSE 'MES 30, DIA>30' END 
										WHEN SUBSTRING(curp, 7, 2)='02' THEN 
												CASE 
														WHEN (IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%4=0 
																		AND ((IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%100!=0 
																												OR (IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%400=0 
																						) 
																THEN CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=29 THEN '' ELSE 'BISIESTO > 29' END 
														ELSE 
																CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=28 THEN '' ELSE 'NO BISIESTO > 28' END 
												END 
										ELSE 
												'MES INCORRECTO' 
								END 
						ELSE 'NO NUMEROS' 
						END 
						) != '')")
					->update([
							'observaciones_error' => DB::raw("CASE WHEN SUBSTRING(curp, 5, 6) REGEXP '^[0-9]{6}$' THEN					  
									CASE 
											WHEN SUBSTRING(curp,7,2) REGEXP '^(01|03|05|07|08|10|12)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=31 THEN 'ERROR DE FORMATO' ELSE 'MES 31, DIA>31' END 
											WHEN SUBSTRING(curp,7,2) REGEXP '^(04|06|09|11)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=30 THEN 'ERROR DE FORMATO' ELSE 'MES 30, DIA>30' END 
											WHEN SUBSTRING(curp, 7, 2)='02' THEN 
													CASE 
															WHEN (IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%4=0 
																			AND ((IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%100!=0 
																													OR (IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%400=0 
																							) 
																	THEN CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=29 THEN 'ERROR DE FORMATO' ELSE 'BISIESTO > 29' END 
															ELSE 
																	CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=28 THEN 'ERROR DE FORMATO' ELSE 'NO BISIESTO > 28' END 
													END 
											ELSE 
													'MES INCORRECTO' 
											END 
									ELSE 'NO NUMEROS' 
									END")
				]);

				DB::connection('mysql_server_temp')->table($this->tempTable)
				->whereNull('curp')
				->update([
							'observaciones_error' => 'CURP NO ENCONTRADA'
				]);

			}catch(\Exception $e){

				$response = ['status' => false, 'message' => $e->getMessage()];

				// Drop temporal tables
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempTable);
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempServicesTable);

			} finally {
				// Delete temporal folder
				File::deleteDirectory($this->dirPath);
			}
			
			return $response;
		}

		public function validateTempTableTutores() {
			$response = ['status' => true, 'message' => ''];
			try{
				// ===================================== NUEVAS COLUMNAS DE RESPONSABLE ===================================== 
				// =============== Corregimos apellidos del responsable si tiene solo segundo apellido ===============
				// Columnas responsables 687: SERVICIOS DE OFTALMOLOGÍA, 785: SUPLEMENTOS VITAMÍNICOS, 790: SERVICIOS DE PEDIATRÍA
				if (in_array($this->datosCaravana['unidad'], array(687, 785, 790))) {
					DB::connection('mysql_server_temp')->table($this->tempTable)
					->update([
						'nombre_responsable' => DB::raw('TRIM(nombre_responsable)'),
						'primer_apellido_responsable' => DB::raw('TRIM(primer_apellido_responsable)'),
						'segundo_apellido_responsable' => DB::raw('TRIM(segundo_apellido_responsable)'),
						'fecha_nacimiento_responsable' => DB::raw('TRIM(fecha_nacimiento_responsable)'),
						'curp_responsable' => DB::raw('TRIM(curp_responsable)'),
						'genero_responsable' => DB::raw('TRIM(genero_responsable)'),
						'clave_electoral_responsable' => DB::raw('TRIM(clave_electoral_responsable)'),
						'parentesco' => DB::raw('TRIM(parentesco)')
					]);

					// =============== Corregimos apellidos si tiene solo segundo apellido ===============
					DB::connection('mysql_server_temp')->table($this->tempTable)
							->whereNotNull('segundo_apellido_responsable')
							->whereNull('primer_apellido_responsable')
							->orWhere('primer_apellido_responsable', '=', '')
							->orWhere('primer_apellido_responsable', 'LIKE', '%N/A%')
							->orWhereRaw("primer_apellido_responsable REGEXP '^[xX]+$'")
							->update([
									'primer_apellido_responsable' => DB::raw("segundo_apellido_responsable"),
									'segundo_apellido_responsable' => ''
					]);

					// =============== Corregimos apellidos del responsable si tiene solo primer apellido ===============
					DB::connection('mysql_server_temp')->table($this->tempTable)
							->whereNotNull('primer_apellido_responsable')
							->where('primer_apellido_responsable', '<>', '')
							->whereRaw("primer_apellido_responsable NOT REGEXP '^[xX]+$'")
							->where( function ( $query )
							{
									$query->whereRaw("segundo_apellido_responsable REGEXP '^[xX]+$'")
											->orWhere('segundo_apellido_responsable', '=', '')
											->orWhere('segundo_apellido_responsable', 'LIKE', '%N/A%');
							})
							->update([
									'segundo_apellido_responsable' => ''
					]);

					// =============== Corregimos formato de la curp de responsables ===============
					DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereIn('curp_responsable', $this->listaOmisiones)
						->update([
								'curp_responsable' => NULL
					]);

					DB::connection('mysql_server_temp')->table($this->tempTable)
							->whereNull('segundo_apellido_responsable')
							->orWhereRaw("segundo_apellido_responsable REGEXP '^[xX]+$'")
							->orWhere('segundo_apellido_responsable', 'LIKE', '%N/A%')
							->update([
									'segundo_apellido_responsable' => ''
					]);

					// =============== Corregimos fechas de nacimiento de responsables ===============
					DB::connection('mysql_server_temp')->table($this->tempTable)
							->update([
									'fecha_nacimiento_responsable' => DB::raw('CONCAT(SUBSTR(fecha_nacimiento_responsable, 1, 10))')
					]);
					
					DB::connection('mysql_server_temp')->table($this->tempTable)
							->whereRaw("fecha_nacimiento_responsable not REGEXP '^([0-9]{4})-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-1])$'")
							->update([
									'fecha_nacimiento_responsable' => DB::raw("(CASE WHEN SUBSTRING(curp_responsable, 5, 6) REGEXP '[0-9]{6}' THEN
									CASE 
											WHEN CONVERT(SUBSTRING(curp_responsable,7,2), UNSIGNED)<=12 THEN 
													CASE 
															WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=31 THEN
															DATE(CONCAT( (CASE WHEN CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=20 THEN '20' ELSE '19' END), SUBSTRING(curp_responsable,5,2), '-', SUBSTRING(curp_responsable,7,2), '-', SUBSTRING(curp_responsable,9,2)))
													END
											END
									ELSE null
								END)")
							]);

					// =============== Corregimos formato del género de responsables ===============
					DB::connection('mysql_server_temp')->table($this->tempTable)
							->whereIn('genero_responsable', ['MUJER', 'FEMENINO', 'M'])
							->update([
									'genero_responsable' => 'F'
					]);
					DB::connection('mysql_server_temp')->table($this->tempTable)
							->whereIn('genero_responsable', ['HOMBRE', 'MASCULINO', 'H'])
							->update([
									'genero_responsable' => 'M'
					]);
					
					// =============== Corregimos formato de clave electoral de responsables ===============
					DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw('LENGTH(clave_electoral_responsable) < 18')
						->update([
								'clave_electoral_responsable' => NULL
					]);

					// =============== Obtenemos los persona_id que coincide en curp y nombre completo ===============
					// Verificamos los que ya están en la tabla personas (asigna id en la columna persona_id)
					DB::connection('mysql_server_temp')
							->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.personas p 
									set 
											x.tutor_persona_id = p.id 
									where 
									x.tutor_persona_id is null AND x.curp_responsable IS NOT NULL AND x.curp_responsable != '' 
									AND x.curp_responsable = p.curp"
					);//AND x.curp = p.curp and p.nombre = x.nombre and p.primer_apellido=x.primer_apellido and p.segundo_apellido=x.segundo_apellido"

					// =============== Obtenemos los persona_id que existen en la tabla bienestarpersonas ===============
					// Verificamos los que ya están en la tabla bienestarpersonas (asigna id en la columna bienestarpersona_id)
					DB::connection('mysql_server_temp')->statement("update {$this->tempDB}.`{$this->tempTable}` x, {$this->realDB}.bienestarpersonas b 
							set 
									x.tutor_bienestarpersona_id = b.id 
							where 
							x.tutor_persona_id is not null AND 
							x.tutor_persona_id = b.persona_id"
					);

					// Recuperamos curps inválidas de los tutores
					DB::connection('mysql_server_temp')->table($this->tempTable)
						->whereRaw(" curp_responsable is not null 
							and (curp_responsable not REGEXP '^[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$' 
							or (CASE WHEN SUBSTRING(curp_responsable, 5, 6) REGEXP '^[0-9]{6}$' THEN 
									CASE 
											WHEN SUBSTRING(curp_responsable,7,2) REGEXP '^(01|03|05|07|08|10|12)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=31 THEN '' ELSE 'MES 31, DIA>31' END 
											WHEN SUBSTRING(curp_responsable,7,2) REGEXP '^(04|06|09|11)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=30 THEN '' ELSE 'MES 30, DIA>30' END 
											WHEN SUBSTRING(curp_responsable, 7, 2)='02' THEN 
													CASE 
															WHEN (IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%4=0 
																			AND ((IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%100!=0 
																													OR (IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%400=0 
																							) 
																	THEN CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=29 THEN '' ELSE 'BISIESTO > 29' END 
															ELSE 
																	CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=28 THEN '' ELSE 'NO BISIESTO > 28' END 
													END 
											ELSE 
													'MES INCORRECTO' 
									END 
							ELSE 'NO NUMEROS' 
							END 
							) != '')")
						->update([
							'observaciones_tutor_error' => DB::raw("CASE WHEN SUBSTRING(curp_responsable, 5, 6) REGEXP '^[0-9]{6}$' THEN					  
									CASE 
											WHEN SUBSTRING(curp_responsable,7,2) REGEXP '^(01|03|05|07|08|10|12)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=31 THEN 'ERROR DE FORMATO' ELSE 'MES 31, DIA>31' END 
											WHEN SUBSTRING(curp_responsable,7,2) REGEXP '^(04|06|09|11)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=30 THEN 'ERROR DE FORMATO' ELSE 'MES 30, DIA>30' END 
											WHEN SUBSTRING(curp_responsable, 7, 2)='02' THEN 
													CASE 
															WHEN (IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%4=0 
																			AND ((IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%100!=0 
																													OR (IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%400=0 
																							) 
																	THEN CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=29 THEN 'ERROR DE FORMATO' ELSE 'BISIESTO > 29' END 
															ELSE 
																	CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=28 THEN 'ERROR DE FORMATO' ELSE 'NO BISIESTO > 28' END 
													END 
											ELSE 
													'MES INCORRECTO' 
											END 
									ELSE 'NO NUMEROS' 
									END")
					]);
				}

				// Columnas mascotas 806: UNIDAD MÉDICA INFECCIOSA
				if ($this->datosCaravana['unidad'] === 806) {
					DB::connection('mysql_server_temp')->table($this->tempTable)
					->update([
						'mascota' => DB::raw('TRIM(mascota)'),
						'especie' => DB::raw('TRIM(especie)'),
						'edad_mascota' => DB::raw('TRIM(edad_mascota)')
					]);
				}
			
			}catch(\Exception $e){

				$response = ['status' => false, 'message' => $e->getMessage()];

				// Drop temporal tables
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempTable);
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempServicesTable);

			} finally {
				// Delete temporal folder
				File::deleteDirectory($this->dirPath);
			}
			
			return $response;
		}

		public function validateTempTableServicios() {
			$response = ['status' => true, 'message' => ''];
			try{
					// =============== Cambiamos nombre de servicios por sus id ===============
					DB::connection('mysql_server_temp')
						->statement("update {$this->tempDB}.`{$this->tempServicesTable}` x, {$this->realDB}.programas p 
								set x.servicio_id=p.id 
								where x.servicio_id=p.nombre 
								and p.tipo='SERVICIO'"
							);

						// =============== Recuperamos beneficios id relacionados a los servicios ===============
					DB::connection('mysql_server_temp')
					->statement("update 
								{$this->realDB}.beneficiosprogramas bp 
								inner join {$this->realDB}.anios_programas ap on bp.anio_programa_id=ap.id 
								inner join {$this->realDB}.cat_ejercicios e on ap.ejercicio_id=e.id 
								inner join {$this->realDB}.programas p on ap.programa_id=p.id 
								inner join `{$this->tempServicesTable}` x on x.servicio_id=p.id 
						set 
							x.beneficio_id = bp.id 
						where 
							e.anio=2019 
								and bp.deleted_at is null AND ap.deleted_at is null and p.deleted_at is null 
								AND p.oficial=1" 
						);
			
			}catch(\Exception $e){

				$response = ['status' => false, 'message' => $e->getMessage()];

				// Drop temporal tables
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempTable);
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempServicesTable);

			} finally {
				// Delete temporal folder
				File::deleteDirectory($this->dirPath);
			}
			
			return $response;
		}

		public function insertDataBD(){
			$response = ['status' => true, 'message' => '', 'totalRegistrosRepetidos' => 0, 'totalRegistrosIncorrectos' => 0, 'totalTutoresIncorrectos' => 0, 'totalRegistrosInsertados' => 0];

			try{
				// =============== Recuperamos las personas que tienen registro en la tabla bienestarpersonas ===============
				$personasBienestar = DB::connection('mysql_server_temp')->table($this->tempTable)
				->whereNotNull('bienestarpersona_id')
				->select('id')
				->get();

				$listaPersonasBienestar = [];
				foreach ($personasBienestar as $beneficiario) {
						$listaPersonasBienestar[] = $beneficiario->id;
				}
				
				// =============== Revisamos curps con formato inválido ===============
				$curpsIncorrectas = DB::connection('mysql_server_temp')->table($this->tempTable)
				->select('id')
				->whereRaw(" curp is not null 
						and (curp not REGEXP '^[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$' 
						or (CASE WHEN SUBSTRING(curp, 5, 6) REGEXP '^[0-9]{6}$' THEN 
								CASE 
										WHEN SUBSTRING(curp,7,2) REGEXP '^(01|03|05|07|08|10|12)$' THEN 
												CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=31 THEN '' ELSE 'MES 31, DIA>31' END 
										WHEN SUBSTRING(curp,7,2) REGEXP '^(04|06|09|11)$' THEN 
												CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=30 THEN '' ELSE 'MES 30, DIA>30' END 
										WHEN SUBSTRING(curp, 7, 2)='02' THEN 
												CASE 
														WHEN (IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%4=0 
																		AND ((IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%100!=0 
																												OR (IF(CONVERT(SUBSTRING(curp,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp,5,2), UNSIGNED))%400=0 
																						) 
																THEN CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=29 THEN '' ELSE 'BISIESTO > 29' END 
														ELSE 
																CASE WHEN CONVERT(SUBSTRING(curp,9,2), UNSIGNED)<=28 THEN '' ELSE 'NO BISIESTO > 28' END 
												END 
										ELSE 
												'MES INCORRECTO' 
								END 
						ELSE 'NO NUMEROS' 
						END 
						) != '')")
				->get();

				$listaCurpsInvalidas = [];
				foreach ($curpsIncorrectas as $registro) {
					$listaCurpsInvalidas[] = $registro->id;
				}

				$registrosIncorrectos = DB::connection('mysql_server_temp')->table($this->tempTable)
				->select("id as ID", "nombre as NOMBRE", "primer_apellido as APELLIDO PATERNO", "segundo_apellido as APELLIDO MATERNO", "fecha_nacimiento as FECHA DE NACIMIENTO", "curp as CURP", "genero as GENERO", "observaciones_error as 'ERRORES ENCONTRADOS EN LA CURP'")
				->whereIn('id', $listaCurpsInvalidas)
				->orWhereNull('curp')
				->get();

				// =============== Revisamos curps repetidas en los registros ===============
				$curpsRepetidas = DB::connection('mysql_server_temp')->table($this->tempTable)
				->select('curp')
				->whereNotNull('curp')
				->whereNotIn('id', $listaCurpsInvalidas)
				->groupBy('curp')
				->havingRaw('count(*)>1')
				->get();

				$listaCurpsRepetidas = [];
				foreach ($curpsRepetidas as $registro) {
						$listaCurpsRepetidas[] = $registro->curp;
				}

				$registrosRepetidos = DB::connection('mysql_server_temp')->table($this->tempTable)
				->select("id as ID", "nombre as NOMBRE", "primer_apellido as APELLIDO PATERNO", "segundo_apellido as APELLIDO MATERNO", "fecha_nacimiento as FECHA DE NACIMIENTO", "curp as CURP", "genero as GENERO", DB::raw("'CURP REPETIDA' as 'ERRORES ENCONTRADOS EN LA CURP'"))
				->whereIn('curp', $listaCurpsRepetidas)
				->get();

				// =============== Guarda los datos que requieren correcciones por el usuario en una colección ===============
				$this->responseData['datosInvalidos'] = $registrosIncorrectos;
				$this->responseData['datosDuplicados'] = $registrosRepetidos;

				
				$listaTutoresIncorrectos = [0];
				// Columnas responsables 687: SERVICIOS DE OFTALMOLOGÍA, 785: SUPLEMENTOS VITAMÍNICOS, 790: SERVICIOS DE PEDIATRÍA
				if (in_array($this->datosCaravana['unidad'], array(687, 785, 790))) {
					$registrosTutoresIncorrectos = DB::connection('mysql_server_temp')->table($this->tempTable)
					->select("id as ID", "nombre_responsable as NOMBRE RESPONSABLE", "primer_apellido_responsable as APELLIDO PATERNO RESPONSABLE", "segundo_apellido_responsable as APELLIDO MATERNO RESPONSABLE", "fecha_nacimiento_responsable as FECHA DE NACIMIENTO DE RESPONSABLE", "curp_responsable as CURP RESPONSABLE", "genero_responsable as GENERO RESPONSABLE", "observaciones_tutor_error as 'ERRORES ENCONTRADOS EN LA CURP DE RESPONSABLE'")
					->whereRaw(" curp_responsable is not null 
							and (curp_responsable not REGEXP '^[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$' 
							or (CASE WHEN SUBSTRING(curp_responsable, 5, 6) REGEXP '^[0-9]{6}$' THEN 
									CASE 
											WHEN SUBSTRING(curp_responsable,7,2) REGEXP '^(01|03|05|07|08|10|12)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=31 THEN '' ELSE 'MES 31, DIA>31' END 
											WHEN SUBSTRING(curp_responsable,7,2) REGEXP '^(04|06|09|11)$' THEN 
													CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=30 THEN '' ELSE 'MES 30, DIA>30' END 
											WHEN SUBSTRING(curp_responsable, 7, 2)='02' THEN 
													CASE 
															WHEN (IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%4=0 
																			AND ((IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%100!=0 
																													OR (IF(CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)>=0 AND CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED)<=19,2000,1900)+CONVERT(SUBSTRING(curp_responsable,5,2), UNSIGNED))%400=0 
																							) 
																	THEN CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=29 THEN '' ELSE 'BISIESTO > 29' END 
															ELSE 
																	CASE WHEN CONVERT(SUBSTRING(curp_responsable,9,2), UNSIGNED)<=28 THEN '' ELSE 'NO BISIESTO > 28' END 
													END 
											ELSE 
													'MES INCORRECTO' 
									END 
							ELSE 'NO NUMEROS' 
							END 
							) != '')")
					->orWhereNull('curp')
					->get();

					$tutoresBienestar = DB::connection('mysql_server_temp')->table($this->tempTable)
					->whereNotNull('tutor_bienestarpersona_id')
					->select('curp_responsable')
					->get();

					$listaTutoresBienestar = [];
					foreach ($tutoresBienestar as $tutor) {
						$listaTutoresBienestar[] = $tutor->curp_responsable;
					}

					foreach ($registrosTutoresIncorrectos as $registro) {
						$listaTutoresIncorrectos[] = $registro->ID;
					}

					$this->responseData['tutoresInvalidos'] = $registrosTutoresIncorrectos;
				}

				// =============== Recuperamos los nuevos registros con formato correcto y que no se repiten ===============
				$registrosCorrectos = DB::connection('mysql_server_temp')->table($this->tempTable)
				->whereNotIn('curp', $listaCurpsRepetidas)
				->whereNotIn('id', $listaCurpsInvalidas)
				->whereNotIn('id', $listaTutoresIncorrectos)
				->whereNotNull('curp')
				->get();

				// Inicio de la transacción
				DB::beginTransaction();
				$beneficiario = [];
				$tutor = [];

				for ($i=0; $i < count($registrosCorrectos) ; $i++) {
					if (!in_array($registrosCorrectos[$i]->bienestarpersona_id, $listaPersonasBienestar)){ // Si no está en bienestar, se actualiza
						$beneficiario = Persona::updateOrCreate(
							['curp' => $registrosCorrectos[$i]->curp],
							[
								'nombre'              => $registrosCorrectos[$i]->nombre,
								'primer_apellido'     => $registrosCorrectos[$i]->primer_apellido,
								'segundo_apellido'    => $registrosCorrectos[$i]->segundo_apellido,
								'calle'               => $registrosCorrectos[$i]->calle,
								'numero_exterior'     => $registrosCorrectos[$i]->numero_exterior,
								'localidad_id'        => $registrosCorrectos[$i]->localidad_id,
								'curp'                => $registrosCorrectos[$i]->curp,
								'genero'              => $registrosCorrectos[$i]->genero,
								'fecha_nacimiento'    => $registrosCorrectos[$i]->fecha_nacimiento,
								'numero_celular'      => $registrosCorrectos[$i]->numero_celular,
								'municipio_id'        => $registrosCorrectos[$i]->municipio_id,
								'colonia'							=> $registrosCorrectos[$i]->colonia,
								'numero_local'				=> $registrosCorrectos[$i]->numero_local,
								'clave_electoral'			=> $registrosCorrectos[$i]->clave_electoral,
								'etnia_id'						=> $registrosCorrectos[$i]->etnia_id,
								'usuario_id'          => $this->usuario
						]);
					}else{ // Si está en bienestar, no se actualiza
						$beneficiario = Persona::findOrFail($registrosCorrectos[$i]->persona_id);
					}

					// Registra al tutor si el beneficiario se presentó con un tutor
					// Columnas responsables 687: SERVICIOS DE OFTALMOLOGÍA, 785: SUPLEMENTOS VITAMÍNICOS, 790: SERVICIOS DE PEDIATRÍA
					if (in_array($this->datosCaravana['unidad'], array(687, 785, 790)) && $registrosCorrectos[$i]->curp_responsable !== NULL && $registrosCorrectos[$i]->nombre_responsable !== NULL) {
						if (!in_array($registrosCorrectos[$i]->tutor_bienestarpersona_id, $listaTutoresBienestar)){ // Si el tutor no está en bienestar, se actualiza
							$tutor = Persona::updateOrCreate(
								['curp' => $registrosCorrectos[$i]->curp_responsable],
								[
									'nombre'              => $registrosCorrectos[$i]->nombre_responsable,
									'primer_apellido'     => $registrosCorrectos[$i]->primer_apellido_responsable,
									'segundo_apellido'    => $registrosCorrectos[$i]->segundo_apellido_responsable,
									'calle'               => $registrosCorrectos[$i]->calle,
									'numero_exterior'     => $registrosCorrectos[$i]->numero_exterior,
									'localidad_id'        => $registrosCorrectos[$i]->localidad_id,
									'curp'                => $registrosCorrectos[$i]->curp_responsable,
									'genero'              => $registrosCorrectos[$i]->genero_responsable,
									'fecha_nacimiento'    => $registrosCorrectos[$i]->fecha_nacimiento_responsable,
									'numero_celular'      => $registrosCorrectos[$i]->numero_celular,
									'municipio_id'        => $registrosCorrectos[$i]->municipio_id,
									'colonia'							=> $registrosCorrectos[$i]->colonia,
									'numero_local'				=> $registrosCorrectos[$i]->numero_local,
									'clave_electoral'			=> $registrosCorrectos[$i]->clave_electoral_responsable,
									'etnia_id'						=> $registrosCorrectos[$i]->etnia_id,
									'usuario_id'          => $this->usuario
							]);
						}else{ // Si está en bienestar, no se actualiza
							$tutor = Persona::findOrFail($registrosCorrectos[$i]->tutor_persona_id);
						}
					}

					$caravana = Caravana::firstOrCreate(
						['evento_id' => $this->datosCaravana['evento']],
						[
							'fecha'           => $this->datosCaravana['fecha'],
							'localidad_id'    => $this->datosCaravana['localidad'],
							'municipio_id'    => $this->datosCaravana['municipio'],
							'evento_id'    		=> $this->datosCaravana['evento'],
							'usuario_id'      => $this->usuario
					]);

					$beneficiosPersona = DB::connection('mysql_server_temp')->table($this->tempServicesTable)
						->where('persona_tmp_id', '=', $registrosCorrectos[$i]->persona_tmp_id)
						->get();

					foreach ($beneficiosPersona as $beneficio) {
						$beneficioPersona = BeneficiosPersonas::create([
							'beneficioprograma_id' => $beneficio->beneficio_id,
							'persona_id'    			 => $beneficiario->id,
							'usuario_id'         	 => $this->usuario
						]);

						$beneficioPersonaCaravana = BeneficiopersonaCaravana::create([
							'caravana_id' 						 => $caravana->id,
							'beneficioprog_persona_id' => $beneficioPersona->id,
							'seccion'         	 			 => $registrosCorrectos[$i]->seccion,
							'observaciones'         	 => $registrosCorrectos[$i]->observaciones,
							'usuario_id'         	 		 => $this->usuario
						]);

						// Registra al tutor si el beneficiario se presentó con un tutor
						// Columnas responsables 687: SERVICIOS DE OFTALMOLOGÍA, 785: SUPLEMENTOS VITAMÍNICOS, 790: SERVICIOS DE PEDIATRÍA
						if (in_array($this->datosCaravana['unidad'], array(687, 785, 790)) && $registrosCorrectos[$i]->curp_responsable !== NULL && $registrosCorrectos[$i]->nombre_responsable !== NULL) {
							Tutor::create([
								'tutor_persona_id'									 => $tutor->id,
								'tutorado_beneficioprog_persona_id'	 => $beneficioPersona->id,
								'usuario_id'         	 							 => $this->usuario
							]);
						}
					}
				}

				DB::commit();
				// Fin de la transacción

				$response = [ 
					'status' => true, 
					'message' => '', 
					'totalRegistrosRepetidos' => count($this->responseData['datosDuplicados']), 
					'totalRegistrosIncorrectos' => count($this->responseData['datosInvalidos']), 
					'totalTutoresIncorrectos' => count($this->responseData['tutoresInvalidos']), 
					'totalRegistrosInsertados' => count($registrosCorrectos)
				];

			}catch(\Exception $e){

				DB::rollBack();
				$response = ['status' => false, 'message' => $e->getMessage()];

			} finally {
				// Drop temporal tables
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempTable);
				Schema::connection('mysql_server_temp')->dropIfExists($this->tempServicesTable);
			}
			
			return $response;
		}

		public function storeResponse(){
			// Create & save file response in server
			Excel::create("Respuesta_{$this->fileName}.{$this->fileId}", function($excel) {
		
					$excel->sheet('Errores', function($sheet) {

							$registrosInvalidos = json_decode(json_encode($this->responseData['datosInvalidos']), true);
							$sheet->fromArray($registrosInvalidos);

					});
					$excel->sheet('Repetidos', function($sheet) {

							$registrosRepetidos = json_decode(json_encode($this->responseData['datosDuplicados']), true);
							$sheet->fromArray($registrosRepetidos);

					});
					$excel->sheet('ErrorTutores', function($sheet) {

						$registrosTutoresInvalidos = json_decode(json_encode($this->responseData['tutoresInvalidos']), true);
						$sheet->fromArray($registrosTutoresInvalidos);

					});
			})->store('xlsx', "{$this->storageDir}");
		}
}
