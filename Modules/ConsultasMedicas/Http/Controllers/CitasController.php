<?php
/**
* Controlador de citas.
* Gestiona los recursos de las citas
*/
namespace Modules\ConsultasMedicas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use View;

use App\Models\Programa;
use App\Models\Cita;

class CitasController extends Controller{

  /**
   * Crea una nueva instancia del controlador
   * Definiendo que el usuario debe estar logueado y autorizado
   * Ademas de los roles que debe tener
   */
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,MEDICO,ODONTOLOGO,PSICOLOGO');
  }

  /**
  * Vista que muestra los registros existentes en el calendario
  * @return View  Contiene el html con los datos
  */
  public function index(){
    return view('consultasmedicas::Citas.index');
  }

  /**
  * Vista para crear un nuevo registro de cita
  * @return Response contiene el html del formulario necesario.
  */
  public function create(){
    return response()->json([
      'body' => view::make('consultasmedicas::Citas.modal')
      ->render()
    ], 200);
  }

  /**
  * Guarda un nueva cita
  * @param  Request $request el objeto con los datos de la solicitud
  * @return Response arreglo con el estado resultante de la solicitud
  */
  public function store(Request $request){
    if ($request->ajax()) {
      try {
        DB::beginTransaction();
        $aux = $request->except(['fecha', 'hora_inicio', 'hora_fin']);
        $aux['hora_inicio'] = $request->hora_inicio.':00';
        $aux['hora_fin'] = $request->hora_fin.':00';
        $aux['fecha'] = (\DateTime::createFromFormat('Y/m/d', $request->fecha))->format('Y-m-d');
        $aux['empleado_id'] = $request->User()->persona->empleado->id;
        $aux['asistencia'] = 0;
        $aux['programa_id'] = Programa::where('nombre','like', '%area blanca%')->first()->id;
        $aux['color'] = '#00968c';

        $comprobacion = Cita::where([
          ['fecha', '=', $aux['fecha']]
        ])->get();


        foreach($comprobacion as $c){

          if($aux['hora_inicio'] < $c->hora_fin && (($aux['hora_fin'] > $c->hora_inicio && $aux['hora_fin'] <= $c->hora_fin) || $aux['hora_fin'] > $c->hora_fin)){
            if($c->persona_id == $aux['persona_id']){
              $programa = $c->programa->nombre;
              return response()->json(array(['success' => false, 'programa' => $programa, 'code' => 411], 411));
            }
            if($c->programa_id == $aux['programa_id']){
              return response()->json(array(['success' => false, 'code' => 410], 410));
            }
          }
        }

        $cita = Cita::create($aux);

        auth()->user()->bitacora(request(), [
            'tabla' => 'citas',
            'registro' => $cita->id . '',
            'campos' => json_encode($cita) . '',
            'metodo' => request()->method()
        ]);

        DB::commit();
        return response()->json(array(['success' => true, 'code' => 200], 200));
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(array(['success' => false, 'code' => 409, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()], 409));
      }
    }
  }

  /**
  * La vista para ver una cita especifica
  * @param  integer $id identificador de la cita, enviado en la ruta
  * @param  Request $request  el objeto con los datos de la solicitud
  * @return Response contiene el html con la información de la cita
  */
  public function show($id,Request $request){
    $cita = Cita::find($id);
    $fecha = (\DateTime::createFromFormat('Y-m-d', $cita->fecha))->format('d/m/Y');
    $fecha_actual = Date('Y-m-d');
    $mayor=0;
    if(strtotime($cita->fecha) >= strtotime($fecha_actual)){
      $mayor=1;
    }
    $title = $cita->persona->get_nombre_completo();
    $persona_id = $cita->persona_id;
    $start = date("g:i a",strtotime($cita->hora_inicio));
    $end = date("g:i a",strtotime($cita->hora_fin));
    $color = $cita->color;
    $observaciones = $cita->observaciones;
    $asistencia = $cita->asistencia;
    $aux = (object) array('id' => $cita->id, 'title' => $title, 'persona_id' =>$persona_id, 'date' => $fecha, 'start' => $start, 'end' => $end, 'color' => $color, 'observaciones' => $observaciones, 'asistencia' => $asistencia);
    return response()->json([
      'body' => view::make('consultasmedicas::Citas.modal')
      ->with('cita',$aux)->with('mayor', $mayor)
      ->render()
    ], 200);
  }
  /**
  * Muestra la vista para editar un registro especifico
  * @param  integer $id identificador del registro a modificar, enviado en la ruta
  * @return Response contiene el html con el formulario necesario
  */
  public function edit($id){
    return response()->json([
      'body' => view::make('consultasmedicas::Citas.modal',['cita'=>Cita::find($id)])
      ->render()
    ], 200);
  }

  /**
  * Actualiza el registro especificado
  * @param  integer $id identificador del registro, enviando en la ruta
  * @param  Request $request contiene los datos para actualizar el registro
  * @return Response contiene un arreglo con el estado de la solicitud
  */
  public function update($id, Request $request){
    if ($request->ajax()) {
      try {
        DB::beginTransaction();
        if($request->input('tipo','none')=='asistencia'){
          Cita::find($id)->update(['asistencia'=>1]);
          DB::commit();
          return response()->json(array(['success' => true, 'code' => 200], 200));
        }
        $aux = $request->except(['fecha', 'observaciones', 'asistencia', 'hora_inicio', 'hora_fin']);
        $aux['hora_inicio'] = $request->hora_inicio.':00';
        $aux['hora_fin'] = $request->hora_fin.':00';
        $aux['asistencia'] = 0;
        if($request->asistencia == 'true'){
          $aux['asistencia'] = 1;
        }
        if(null !== $request->fecha){
          $aux['fecha'] = (\DateTime::createFromFormat('d/m/Y', $request->fecha))->format('Y-m-d');
        }
        if(null !== $request->observaciones){
          $aux['observaciones'] = $request->observaciones;
        }

        $comprobacion = Cita::where([
          ['fecha', $aux['fecha']],
          ['id', '!=', $id]
        ])->get();

        foreach($comprobacion as $c){
          //dd($aux);
          if($aux['hora_inicio'] < $c->hora_fin && (($aux['hora_fin'] > $c->hora_inicio && $aux['hora_fin'] <= $c->hora_fin) || $aux['hora_fin'] > $c->hora_fin)){
            if($c->persona_id == $aux['persona_id']){
              $programa = $c->programa->nombre;
              return response()->json(array(['success' => false, 'programa' => $programa, 'code' => 411], 411));
            }
            if($c->programa_id == $aux['programa_id']){
              return response()->json(array(['success' => false, 'code' => 410], 410));
            }
          }
        }

        Cita::find($id)->update($aux);
        $cita = Cita::find($id);

        auth()->user()->bitacora(request(), [
            'tabla' => 'citas',
            'registro' => $cita->id . '',
            'campos' => json_encode($cita) . '',
            'metodo' => request()->method()
        ]);

        DB::commit();
        return response()->json(array(['success' => true, 'cita' => $aux, 'code' => 200], 200));
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(array(['success' => false, 'code' => 409, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()], 409));
      }
    }
  }

  /**
  * Eliminar un registro en especifico
  * @param  integer $id identificador del registro, enviado en la ruta
  * @return Response contiene un arreglo con el estado de la solicitud
  */
  public function destroy(){
    if ($request->ajax()) {
      try {
        DB::beginTransaction();
        $cita = Cita::find($id);
        auth()->user()->bitacora(request(), [
            'tabla' => 'citas',
            'registro' => $cita->id . '',
            'campos' => json_encode($cita) . '',
            'metodo' => request()->method()
        ]);
        $cita->delete();

        DB::commit();
        return response()->json(array('success' => true));
      } catch (\Exception $e) {
        DB::rollBack();
        return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
      }
    }
  }
  /**
   * Obtiene las citas almacenados en un programa en especial y entre las fechas dadas pertenecientes
   *
   * @param  Request $request datos de la solicitud
   * @return Array  contiene los datos de las citas
   */
  public function muestraCitas(Request $request){
    $eventos = array();
    if($request->has('start')){
      //dd($request->all());
      $programa_id = Programa::where('nombre','like', '%area blanca%')->first()->id;
      $citas = Cita::whereBetween('fecha', [$request->start, $request->end])->
      where('programa_id',$programa_id)
      ->where('empleado_id',$request->user()->persona->empleado->id)->get();
      foreach ($citas as $cita) {
        $fecha = (\DateTime::createFromFormat('Y-m-d', $cita->fecha))->format('Y-m-d');
        $id = $cita->id;
        $title = $cita->persona->get_nombre_completo();
        $start = $fecha . ' ' . $cita->hora_inicio;
        $end = $fecha . ' ' . $cita->hora_fin;
        $backgoundColor = $cita->color;
        $borderColor = $cita->color;
        if($cita->asistencia == 1){
          $backgoundColor = '#909089';
          $borderColor = '#909089';
        }

        array_push($eventos, array('id' => $id, 'title' => $title, 'start' => $start, 'end' => $end, 'backgroundColor' => $backgoundColor, 'borderColor' => $borderColor));
      }
    }
    return $eventos;

  }
}
