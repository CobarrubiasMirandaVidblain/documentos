<?php

namespace Modules\ConsultasMedicas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\DataTables\ConsultasMedicas\PacientesTabla;
use App\DataTables\ConsultasMedicas\PersonasTabla;
use App\DataTables\ConsultasMedicas\Historial;

use App\Models\ConsultasMedicas\Seguros;
use App\Models\ConsultasMedicas\Pacientes;
use App\Models\ConsultasMedicas\Etnia;

use App\Models\Entidad;
use App\Models\persona;
use App\Models\Pais;

class PacienteController extends Controller{
 /**
  * Crea una nueva instancia del controlador
  * Definiendo que el usuario debe estar logueado y autorizado
  * Ademas de los roles que debe tener
  */
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,MEDICO,ODONTOLOGO,PSICOLOGO');

  }
  /**
  * Vista que muestra los registros existentes
  * @param  PacientesTabla $tabla Servicio de DataTable que contiene los datos a mostrar
  * @return View              Contiene el html y la tabla con los datos
  */
  public function index(PacientesTabla $tabla){
    return $tabla->render('consultasmedicas::Pacientes.index');
  }

  /**
  * Vista para crear un nuevo registro
  * @return Response contiene el html del formulario necesario.
  */
  public function create(){
    return view('consultasmedicas::Pacientes.create_edit',
    [
      'derechohabiecias' => Seguros::all(),
      'entidades'        => Entidad::all(),
      'paises'           => Pais::all(),
      'etnias'           => Etnia::all()
    ]);
  }

  /**
  * Almacena un nuevo registro de la vista
  * @param  Request $request el objeto con los datos de la solicitud
  * @return Response arreglo con el estado resultante de la solicitud
  */
  public function store(Request $request){
    try{
      DB::beginTransaction();
      $paciente['persona_id']=$request->input('persona_id');
      if($request->has('persona')){
        $datos                  = ((array)$request->persona);
        $datos['usuario_id']    = $request->user()->id;
        // dd($datos);
        $paciente['persona_id'] = Persona::firstOrCreate(['curp'=>$datos['curp']],$datos)->id;
      }
      $paciente['seguro_id']   = $request->input('seguro_id');
      $paciente['folioseguro'] = $request->input('folioseguro');
      $paciente['indigena']    = $request->input('indigena');
      $paciente['entidad_id']  = $request->input('estado_id',20);
      $paciente['pais_id']     = $request->input('pais_id',156);
      $paciente['usuario_id']  = $request->user()->id;
      $paciente['rfc']         = $request->input('rfc');
      $new = Pacientes::firstOrCreate(['persona_id'=>$paciente['persona_id']],$paciente);
      DB::commit();
      return response()->json(array('success' => true, 'id' => $new->id));
    } catch (Exception $e) {
      DB::rollBack();
      return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
    }
  }

  /**
  * Muestra la información de un registro especifico
  * @param  integer     $id     Identificador del registro, enviado en la ruta
  * @return Response            Retorna la vista con los datos requeridos
  */
  public function show($id,Historial $tabla){
    $paciente = Pacientes::find($id);
    $consultas = $paciente->consultas;
    $labels = Array();
    $pesos = Array();
    $tallas = Array();
    foreach ($consultas as $consulta) {
      array_push($labels,$consulta->created_at->format('d/m/Y'));//Carbon\Carbon::parse('11/06/1990')->format('d/m/Y')
      array_push($pesos, $consulta->peso);
      array_push($tallas, $consulta->talla*100);
    }
    //dd($labels);
    $avance = app()->chartjs
        ->name('lineChartTest')
        ->type('line')
        ->size(['width' => 1200, 'height' => 300])
        ->labels($labels)
        ->datasets([
            [
                "label" => "Peso",
                'backgroundColor' => "rgba(171, 103, 27, 0.31)",
                'borderColor' => "rgba(213, 63, 15, 0.7)",
                "pointBorderColor" => "rgba(171, 103, 27, 0.31)",
                "pointBackgroundColor" => "rgba(213, 63, 15, 0.7)",
                "pointHoverBackgroundColor" => "#676767",
                "pointHoverBorderColor" => "rgb(83, 83, 83)",
                'data' => $pesos,
            ],
            [
                "label" => "Altura",
                'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                'borderColor' => "rgba(38, 185, 154, 0.7)",
                "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                "pointHoverBackgroundColor" => "#676767",
                "pointHoverBorderColor" => "rgba(83,83,83,83)",
                'data' => $tallas,
            ]
        ])
        ->options([]);

    return $tabla->with('paciente',$paciente->id)->render('consultasmedicas::Pacientes.show',['paciente'=>$paciente,'graf'=>$avance]);
  }

  /**
  * Muestra la vista para editar un registro especifico
  * @param  integer $id identificador del registro a modificar, enviado en la ruta
  * @return Response contiene el html con el formulario necesario
  */
  public function edit($id){
    return view('consultasmedicas::edit');
  }

  /**
  * Actualiza el registro especificado
  * @param  integer $id identificador del registro, enviando en la ruta
  * @param  Request $request contiene los datos para actualizar el registro
  * @return Response contiene un arreglo con el estado de la solicitud
  */
  public function update(Request $request){
  }

}