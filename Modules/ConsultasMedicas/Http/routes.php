<?php

Route::group(['middleware' => 'web', 'prefix' => 'consultasmedicas', 'namespace' => 'Modules\ConsultasMedicas\Http\Controllers'], function(){
    Route::get('/', 'ConsultasMedicasController@index')->name('consultasmedicas.home');
    Route::get('/dashboard', 'ConsultasMedicasController@dashboard')->name('consultasmedicas.dash');
    Route::resource('/pacientes','PacienteController',['as' => 'consultasmedicas', 'parameters' => ['pacientes' => 'id']]);
    Route::get('/citas/eventos','CitasController@muestraCitas')->name('consultasmedicas.citas');
    Route::resource('/citas','CitasController',['as' => 'consultasmedicas', 'parameters' => ['citas' => 'id']]);
    Route::resource('pacientes/{paciente}/consultas', 'ConsultaController', ['as' => 'consultasmedicas.pacientes', 'parameters' => ['pacientes' => 'id', 'consultas' =>'id'], 'only' => ['create', 'store', 'show']]);
    Route::get('/cie10','ConsultaController@cie10List')->name('consultasmedicas.cie10');
    Route::get('/medicamentos','ConsultaController@medicamentosList')->name('consultasmedicas.medicamentos');

    Route::resource('/odontologia/servicios','ServiciosController',['as'=>'consultasmedicas.odontologia','parameters'=>['servicios'=>'id'],'except'=>['show']]);
    Route::resource('/odontologia/piezasdentales','DientesController',['as'=>'consultasmedicas.odontologia','parameters'=>['piezasdentales'=>'id'],'except'=>['show']]);
    Route::resource('/diagnosticos','Cie10Controller',['as'=>'consultasmedicas','parameters'=>['diagnosticos'=>'id'],'except'=>['show']]);
    Route::resource('/clasificacion','ProgramasController',['as'=>'consultasmedicas','parameters'=>['clasificacion'=>'id'],'except'=>['show']]);
    Route::resource('/peticiones','PeticionController',['as'=>'consultasmedicas','parameters'=>['peticiones'=>'id'],'only'=>['index','show','update','destroy']]);
    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'consultasmedicas' ]);
    Route::resource('/programas','ProgramaController',['as'=>'consultasmedicas','parameters'=>['programas'=>'id']]);
});