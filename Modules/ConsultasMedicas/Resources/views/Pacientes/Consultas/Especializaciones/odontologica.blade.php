<row>
  <div class="nav-tabs-custom">

    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">Atención preventiva</a></li>
      <li><a href="#tab_2" data-toggle="tab">Atención curativa</a></li>
      <li><a href="#tab_3" data-toggle="tab">Atención otorgada</a></li>
    </ul>

    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <h4>Atención preventiva</h4>
        <div class="row">
          <form id="form-preventiva" data-toggle="validator" role="form">
            <div class="col-xs-12 col-md-6 col-lg-6">
              <div class="form-group">
                <label for="preventiva">Seleccione :</label>
                <select id="preventiva" name="preventiva" class="form-control select2">
                  <option value=""></option>
                  @foreach ($servicios['preventivos'] as $preventivo)
                    <option value="{{ $preventivo->id }}">{{ $preventivo->nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-xs-2 pull-right" style="padding-bottom: 0">
              <div class="form-group">
                <label for="duracion"></label>
                <button type="button" class="btn btn-info form-control" onclick="addAtencionPreventiva()">
                  <i class="fa fa-plus"></i> Agregar
                </button>
              </div>
            </div>
            
          </form>
        </div>
      </div>
      <div id="tab_2" class="tab-pane">
        <h4>Atencion curativa</h4>
        <div class="row">
          <form id="form-curativa" data-toggle="validator" role="form">
            <div class="col-xs-12 col-md-6 col-lg-4">
              <div class="form-group">
                <label for="pieza_dental">Pieza dental:</label>
                <select id="pieza_dental" name="pieza_dental" class="form-control select2" style="width:100%">
                  <option value=""></option>
                  @foreach ($piezas as $pieza)
                    <option value="{{ $pieza->id }}">{{ $pieza->nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-3">
              <div class="form-group">
                <label for="tratamiento_dental">Tratamiento:</label>
                <select id="tratamiento_dental" name="tratamiento_dental" class="form-control select2"  style="width:100%">
                  <option value=""></option>
                  @foreach ($servicios['curativos'] as $curativo)
                    <option value="{{ $curativo->id }}">{{ $curativo->nombre }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
              <div class="form-group">
                <label for="cantidad_t_d">Cantidad:</label>
                <input type="text" class="form-control" id="cantidad_t_d" name="cantidad_t_d"  placeholder="CANTIDAD">
              </div>
            </div>
          </form>
          <div class="col-xs-2 pull-right" style="padding-bottom: 0">
            <div class="form-group">
              <label for="duracion"></label>
              <button type="button" class="btn btn-info form-control" onclick="addAtencionCurativa()">
                  <i class="fa fa-plus"></i> Agregar
              </button>
            </div>
          </div>

        </div>
      </div>
      <div id="tab_3" class="tab-pane">
        <div class="col-xs-12">
          <table id="tabla-actividad" name="actividades_odontologicas" class="datatable table" style="width:100%;">
            <thead>
              <tr>
                <td>Atención</td>
                <td>Actividad</td>
                <td>Pieza dental</td>
                <td>Cantidad</td>
                <td>Eliminar</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</row>
<hr>
@include('consultasmedicas::pacientes.consultas.especializaciones.general')
