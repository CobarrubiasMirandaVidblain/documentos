<row>
  <div class="row">

    <div class="col-xs-12 col-md-6 col-lg-4">
      <div class="form-group">
        <label for="adiccion">Adiccion:</label>
        <select id="adiccion" name="adiccion" class="form-control select2">
          <option value="Alcohol">Alcohol</option>
          <option value="Tabaco">Tabaco</option>
          <option value="Cannabis">Cannabis</option>
          <option value="Otra">Otra</option>
        </select>
      </div>
    </div>
    <div class="col-xs-4">
      <div class="form-group">
        <label for="entrevista">Entrevista</label>
        <textarea id="entrevista" name="entrevista" class="form-control"></textarea>
      </div>
    </div>
    <div class="col-xs-4">
      <div class="form-group">
        <label for="e_psicometrico">Estudio psicometrico</label>
        <textarea id="e_psicometrico" name="e_psicometrico" class="form-control"></textarea>
      </div>
    </div>

  </div>

  <div class="row">

    <div class="col-xs-4">
      <div class="form-group">
        <label for="p_individual">Psicoterapia individal</label>
        <textarea id="p_individual" name="p_individual" class="form-control"></textarea>
      </div>
    </div>
    <div class="col-xs-4">
      <div class="form-group">
        <label for="p_grupo">Psicoterapia de grupo</label>
        <textarea id="p_grupo" name="p_grupo" class="form-control"></textarea>
      </div>
    </div>
    <div class="col-xs-4">
      <div class="form-group">
        <label for="p_pareja">Psicoterapia de pareja</label>
        <textarea id="p_pareja" name="p_pareja" class="form-control"></textarea>
      </div>
    </div>

  </div>

  {{-- <div class="row">
    <div class="col-xs-2 pull-right" style="padding-bottom: 0">
      <div class="form-group">
        <label for="duracion"></label>
        <button type="button" class="btn btn-info form-control" onclick="">
          agregar <i class="fa fa-plus"></i>
        </button>
      </div>
    </div>
  </div> --}}

</row>
