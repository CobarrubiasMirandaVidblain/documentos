<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">CONSULTA</h3>
  </div>
  <div class="box-body">
    
      <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-5">
          <label>Paciente:</label>
          <input class="form-control" id="paciente_id" value="{{ $consulta->paciente->persona->NombreCompleto() }}" readonly>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-7">
            <div class="form-group">
              <label for="discapacidad">Discapacidad:</label>
              <select id="discapacidad" name="discapacidad" class="form-control select2" multiple="multiple" style="width: 100%;" disabled>
              <option value=""></option>
              @foreach ($consulta->discapacidades as $discapacidad)
                <option value="{{ $discapacidad->discapacidad->id }}" selected>{{ $discapacidad->discapacidad->nombre }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-xs-7" >
          <div class="col-xs-4">
            <div class="form-group">
              <label for="peso">Peso (Kg):</label>
              <input type="text" class="form-control" id="peso" value="{{ $consulta->peso or 'No registrado' }}" readonly>
            </div>
          </div>
          <div class="col-xs-4">
            <div class="form-group">
              <label for="talla">Talla (m):</label>
              <input type="text" class="form-control" id="talla" value="{{ $consulta->talla or 'No registrado' }}" readonly>
            </div>
          </div>
          <div class="col-xs-3" >
            <div class="form-group" style="text-align: center;">
              <label for="imc">Imc</label>
              <strong><input type="text" class="form-control texto-plano" id="imc" data-toggle="tooltip" data-placement="bottom" title="imc" name="imc" value="0" readonly style="text-align: center; background-color : #d6d6d6;"></strong>
            </div>
          </div>
        </div>

        <div class="col-xs-5" style="border-left: 1px solid #d6d6d6; border-right: 1px solid #d6d6d6;">
          <div class="col-xs-6">
            <div class="form-group" style="text-align: center;">
              <label for="referido" class="center-block">Referido:</label>
              <input type="checkbox" id="referido" name="referido" {{ ($consulta->referido)? 'checked':'' }} disabled>
            </div>
          </div>
  
          <div class="col-xs-6">
            <div class="form-group" style="text-align: center;">
              <label for="contrareferido" class="center-block">Contrareferido:</label>
              <input type="checkbox" id="contrareferido" name="contrareferido" {{ ($consulta->contrareferido)? 'checked':'' }} disabled>
            </div>
          </div>
        </div>
        
        {{-- @if ($area == 'general' && $paciente->persona->genero === 'F')
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 pull-right">
            <div class="form-group" style="text-align: center;">
              <label for="embarazo" class="center-block">Embarazo:</label>
              <input type="checkbox" id="embarazo" name="embarazo">
            </div>
          </div>
        @endif --}}
      </div>
      {{-- @if ($area == 'general' && $paciente->persona->get_edad() < 5)
        @include('consultasmedicas::pacientes.consultas.especializaciones.menor')]
      @endif
      @if ($area == 'general' && $paciente->persona->genero === 'F')
        @include('consultasmedicas::pacientes.consultas.especializaciones.embarazo')
      @endif --}}
      <h4>Diagnostico(s)</h4>
      <hr>
      <div id="diagnosticos">
        <div>
          <table id="tabla-diagnostico" name="diagnosticos" class="table table-striped table-bordered table-hover table-condensed table-responsive">
            <thead>
              <tr>
                <td>Diagnostico Textual</td>
                <td>Cie10</td>
                <td>Primera vez</td>
                <td>Primera vez anual</td>
                <td>Programa</td>
              </tr>
            </thead>
            @foreach ($consulta->diagnosticos as $diagnostico)
                <tr>
                  <td>{{ $diagnostico->diagnostico }}</td>
                  <td>{{ $diagnostico->cie10->nombre }}</td>
                  <td>{{ ($diagnostico->primeravezpadecimiento == 0)?'No':'Si' }}</td>
                  <td>{{ ($diagnostico->primeravezanual == 0)?'No':'Si' }}</td>
                  <td>{{ $diagnostico->clasificacion->nombre }}</td>
                </tr>
            @endforeach
          </table>
        </div>
      </div>
      <h4>Tratamiento(s)</h4>
      <div id="tratamiento">
        <div>
          <table id="tabla-tratamiento" name="tratamientos" class="table table-striped table-bordered table-hover table-condensed table-responsive">
            <thead>
              <tr>
                <td>Recomedacion</td>
                <td>Dosis</td>
                <td>Cada</td>
                <td>Durante</td>
              </tr>
            </thead>
            @foreach ($consulta->recetas as $receta)
                <tr>
                  <td>{{ $receta->medicamento->nombre_generico }}</td>
                  <td>{{ $receta->dosis.' '.$receta->medicamento->forma_farmaceutica }}</td>
                  <td>{{ $receta->intervalo.' horas' }}</td>
                  <td>{{ $receta->duracion.' dias' }}</td>
                </tr>
            @endforeach
          </table>
        </div>
      </div>
      {{-- <div class="row">
        <div class="col-xs-12">
          <button type="button" class="btn btn-info pull-right btn-md" data-toggle="modal" data-target="#modal-diagnostico"><i class="fa fa-plus"></i> Agregar diagnositco</button>
        </div>
      </div> --}}
      {{-- <hr> --}}
    {{-- </form> --}}
      {{-- @switch($area)
          @case('odontologia')
              @include('consultasmedicas::pacientes.consultas.especializaciones.odontologica')
              @break

          @case('psicologia')
              @include('consultasmedicas::pacientes.consultas.especializaciones.psicologica')
              @break

          @default
              @include('consultasmedicas::pacientes.consultas.especializaciones.general')
      @endswitch --}}

  </div>
{{-- <div class="box-footer">
  <button type="button" class="btn btn-success pull-right" onclick="procesarConsulta();"><i class="fa fa-save"> </i> Guardar consulta</button>
</div> --}}
</div>