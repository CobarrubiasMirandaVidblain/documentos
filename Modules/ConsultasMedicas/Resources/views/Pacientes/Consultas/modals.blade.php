<div class="modal fade" id="modal-diagnostico" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Agregar diagnostico</h4>
      </div>
      <div class="modal-body">
        <form id="form-diagnostico" data-toggle="validator" role="form">
          <div class="row">
              <div class="col-xs-12">
                <div class="form-group">
                  <label for="cie10">Seleccione diagnostico</label>
                  <select id="cie10" name="cie10" class="form-control select2" style="width: 100%;">
                    <option></option>
                  </select>
                </div>
              </div>
              <div class="col-xs-3">
                <div class="form-group" style="text-align: center;">
                  <label for="diag_1ra_vez" class="center-block">Primera vez</label>
                  <input type="checkbox" id="diag_1ra_vez" name="diag_1ra_vez">
                </div>
              </div>
              <div class="col-xs-3">
                <div class="form-group" style="text-align: center;">
                  <label for="diag_1ra_vez_anio" class="center-block">Primera vez(año)</label>
                  <input type="checkbox" id="diag_1ra_vez_anio" name="diag_1ra_vez_anio">
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="programaXmotivo">Seleccione programa</label>
                  <select id="programaXmotivo" name="programaXmotivo" class="form-control select2" style="width: 100%;">
                    <option value=""></option>
                    @foreach ($clasificaciones as $clasificacion)
                    <option value="{{ $clasificacion->id }}">{{ $clasificacion->nombre }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
          </div>
          <div class="row">
              <div class="col-xs-12">
                <div class="form-group">
                  <label for="diag_textual">Diagnostico textual</label>
                  <textarea id="diag_textual" name="diag_textual" class="form-control"></textarea>
                </div>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onClick="addDiagnostico()">Agregar diagnostico</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-tratamiento" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Agregar tratamiento</h4>
      </div>
      <div class="modal-body">
        <form id="form-tratamiento" data-toggle="validator" role="form">
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                {{-- <label for="tratamiento">Medicamento:</label> --}}
                <div class="input-group">
                  <span class="input-group-addon">De:</span>
                  <select type="text" class="form-control" name="tratamiento" id="tratamiento" style="width:100%;">
                    <option value=""></option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                {{-- <label for="intervalo">Tomar:</label> --}}
                <div class="input-group input-group-sm">
                  <span class="input-group-addon">Tomar:</span>
                  <input type="text" class="form-control" name="cantidad" id="cantidad" style="width: 75%">
                  <select class="select2 form-control" style="width:25%">
                    <option value="capsula(s)">CAPSULAS</option>
                    <option value="cucharada(s)">CUCHARADAS</option>
                    <option value="inyeccion(es)">INYECCIONES</option>
                    <option value="supusitorio(s)">SUPOSITORIOS</option>
                    <option value="ml">ML</option>
                    <option value="mg">MG</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                {{-- <label for="intervalo">Cada:</label> --}}
                <div class="input-group">
                  <span class="input-group-addon">Cada: </span>
                  <input type="text" class="form-control" name="intervalo" id="intervalo">
                  <span class="input-group-addon">Horas</span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="form-group">
                {{-- <label for="duracion">Durante:</label>] --}}
                <div class="input-group">
                  <span class="input-group-addon">Durante: </span>
                  <input type="text" class="form-control" name="duracion" id="duracion">
                  <span class="input-group-addon"> Dias &nbsp; </span>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onClick="addTratamiento()">Agregar tratamiento</button>
      </div>
    </div>
  </div>
</div>