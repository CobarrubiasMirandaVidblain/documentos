@extends('consultasmedicas::layouts.master')

@push('head')
<link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
.texto-plano{
  border: 0;
  width: 100%;
  padding: 5px;
  /* text-decoration: underline; */
}
</style>
@endpush

@section('content-subtitle', 'Nueva consulta')

@section('li-breadcrumbs')
<li><a href="{{route('consultasmedicas.pacientes.index')}}">pacientes</a></li>
<li class="active">paciente</li>

@endsection

@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">CONSULTA</h3>
  </div>
  <div class="box-body">
    <form id="form-consulta" action="{{ route('consultasmedicas.pacientes.consultas.store',$paciente->id) }}" method="post">

      <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-5">
          <label>Paciente:</label>
          <input id="paciente_id" name="paciente_id" class="texto-plano" type="text" value="{{ $paciente->persona->NombreCompleto() }}" readonly>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-7">
          <div class="form-group">
            <label for="discapacidad">Discapacidad:</label>
            <select id="discapacidad" name="discapacidad" class="form-control select2" multiple="multiple">
              <option value=""></option>
              @foreach ($discapacidades as $discapacidad)
              <option value="{{ $discapacidad->id }}">{{ $discapacidad->nombre }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-xs-5" style="padding-left:0; padding-right:0;">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="form-group">
              <label for="peso">Peso (Kg):</label>
              <input type="text" class="form-control" id="peso" name="peso"  placeholder="PESO">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="form-group">
              <label for="talla">Talla (m):</label>
              <input type="text" class="form-control" id="talla" name="talla"  placeholder="TALLA">
            </div>
          </div>
          <div class="col-xs-3" style="padding-left:0; padding-right:0;">
            <div class="form-group" style="text-align: center;">
              <label for="imc">Imc</label>
              <strong><input type="text" class="texto-plano" id="imc" data-toggle="tooltip" data-placement="bottom" title="imc" name="imc" value="0" readonly style="text-align: center; background-color : #d6d6d6;"></strong>
            </div>
          </div>
        </div>

        <div class="col-xs-5" style="border-left: 1px solid #d6d6d6; border-right: 1px solid #d6d6d6;">
          <div class="col-xs-6">
            <div class="form-group" style="text-align: center;">
              <label for="referido" class="center-block">Referido:</label>
              <input type="checkbox" id="referido" name="referido">
            </div>
          </div>
  
          <div class="col-xs-6">
            <div class="form-group" style="text-align: center;">
              <label for="contrareferido" class="center-block">Contrareferido:</label>
              <input type="checkbox" id="contrareferido" name="contrareferido">
            </div>
          </div>
        </div>
        
        @if ($area == 'general' && $paciente->persona->genero === 'F')
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 pull-right">
            <div class="form-group" style="text-align: center;">
              <label for="embarazo" class="center-block">Embarazo:</label>
              <input type="checkbox" id="embarazo" name="embarazo">
            </div>
          </div>
        @endif
      </div>
      @if ($area == 'general' && $paciente->persona->get_edad() < 5)
        @include('consultasmedicas::Pacientes.Consultas.Especializaciones.menor')
      @endif
      @if ($area == 'general' && $paciente->persona->genero === 'F')
        @include('consultasmedicas::Pacientes.Consultas.Especializaciones.embarazo')
      @endif
      <hr>
      <div id="diagnosticos">
        <div>
          <table id="tabla-diagnostico" name="diagnosticos" class="datatable table">
            <thead>
              <tr>
                <td>Diagnostico Textual</td>
                <td>Cie10</td>
                <td>Primera vez</td>
                <td>Primera vez anual</td>
                <td>Programa</td>
                <td>Acciones</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="button" class="btn btn-info pull-right btn-md" data-toggle="modal" data-target="#modal-diagnostico"><i class="fa fa-plus"></i> Agregar diagnositco</button>
        </div>
      </div>
      <hr>
    </form>
      @switch($area)
          @case('odontologia')
              @include('consultasmedicas::Pacientes.Consultas.Especializaciones.odontologica')
              @break

          @case('psicologia')
              @include('consultasmedicas::Pacientes.Consultas.Especializaciones.psicologica')
              @break

          @default
              @include('consultasmedicas::Pacientes.Consultas.Especializaciones.general')
      @endswitch

  </div>
  <div class="box-footer">
    <button type="button" class="btn btn-success pull-right" onclick="procesarConsulta();"><i class="fa fa-save"> </i> Guardar consulta</button>
  </div>
</div>

@include('consultasmedicas::Pacientes.Consultas.modals')

@stop

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript">
    var app = (function() {

      var bloqueo = false;

      function to_upper_case() {
        $(':input').on('propertychange input', function(e) {
          var ss = e.target.selectionStart;
          var se = e.target.selectionEnd;
          e.target.value = e.target.value.toUpperCase();
          e.target.selectionStart = ss;
          e.target.selectionEnd = se;
        });
      };

      function set_bloqueo(valor) {
        bloqueo = valor;
      };

      function get_bloqueo() {
        return bloqueo;
      };

      function agregar_bloqueo_pagina() {
        $('form :input').change(function() {
          bloqueo = true;
        });

        window.onbeforeunload = function(e) {
          if(bloqueo)
          {
            return '¿Estás seguro de salir?';
          }
        };
      };

      return {
        to_upper_case: to_upper_case,
        set_bloqueo: set_bloqueo,
        agregar_bloqueo_pagina: agregar_bloqueo_pagina
      };
    })();
    
    app.agregar_bloqueo_pagina();

  //TODO: se puede hacer todo los de los metodos en uno solo usando jQuery y sus metodos en especial find
  $.fn.modal.Constructor.prototype.enforceFocus = function() {};
  var area = "{{ $area }}";
  var menor = ({{ $paciente->persona->get_edad()}}) < 5 ? true : false;
  $(document).ready(function () {
    $('input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_square',
      increaseArea: '20%'
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('#peso').keyup(function(){
      getImc();
    });
    $('#talla').keyup(function(){
      getImc();
    });
    $('#referido').on('ifChecked',function () {
      $('#contrareferido').iCheck('uncheck');
    })
    $('#contrareferido').on('ifChecked',function () {
      $('#referido').iCheck('uncheck');
    })
    $('#discapacidad').select2({
      placeholder : "SELECCIONE DE SER NECESARIO",
      //allowClear: true,
      language : 'es'
    })
    $('#riesgo_de').select2({
      placeholder : "SELECCIONE DE SER NECESARIO",
      allowClear: true,
      language : 'es'
    })
    $('#programaXmotivo').select2({
      placeholder : "POR FAVOR SELECCIONE",
      language : 'es'
    })
    $('#preventiva').select2({
      placeholder : "POR FAVOR SELECCIONE",
      language : 'es'
    })
    $('#pieza_dental').select2({
      placeholder : "POR FAVOR SELECCIONE",
      language : 'es'
    })
    $('#adiccion').select2({
      placeholder : "POR FAVOR SELECCIONE",
      language : 'es'
    })
    $('#tratamiento_dental').select2({
      placeholder : "POR FAVOR SELECCIONE",
      language : 'es'
    })
    $('#cie10').select2({
      language: 'es',
      placeholder : 'CATALOGO CIE10, BUSQUE Y SELECCIONE',
      minimumInputLength: 3,
      ajax: {
        url: '{{ route('consultasmedicas.cie10') }}',
        delay: 500,
        dataType: 'JSON',
        type: 'GET',
        data: function(params) {
          return {
            search: params.term
          };
        },
        processResults: function(data, params) {
          params.page = params.page || 1;
          return {
            results: $.map(data, function(item) {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    })
    $('.collapse').collapse({
      toggle : false
    });
    $('#embarazo').on('ifToggled',function () {
      $('#extraembarazo').collapse('toggle');
    })
    $('#riesgo').on('ifToggled',function () {
      $('#riesgoembarazo').collapse('toggle');
    })
    $('#tabla-tratamiento').DataTable({
      language : {
        url : '{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}',
        sEmptyTable: 'No se han agregado medicamentos'
      },
      paging : false,
      info   : false,
      dom   : 'btip',
      columnDefs: [{
        orderable: false, targets: '_all'
      }]
    });
    $('#tabla-diagnostico').DataTable({
      language : {
        url : '{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}',
        sEmptyTable: 'No se han agregado diagnósticos'
      },
      paging : false,
      info   : false,
      dom   : 'btip',
      columnDefs: [{
        orderable: false, targets: '_all'
      }]
    });
    
    $('#tratamiento').select2({
      language: 'es',
      placeholder : 'BUSCAR MEDICAMENTO',
      minimumInputLength: 3,
      //dropdownAutoWidth: true,
      dropdownParent : $('#modal-tratamiento'),
      ajax: {
        url: '{{ route('consultasmedicas.medicamentos') }}',
        delay: 500,
        dataType: 'JSON',
        type: 'GET',
        data: function(params) {
          return {
            search: params.term
          };
        },
        processResults: function(data, params) {
          params.page = params.page || 1;
          return {
            results: $.map(data, function(item) {
              return {
                id: item.id,
                text: item.nombre_generico+'['+item.forma_farmaceutica+']'+item.concentracion,
                slug: item.nombre_generico,
                results: item
              }
            })
          }
        },
        cache: true
      }
    });
  })
  function getImc() {
    var peso  = $('#peso').val();
    var talla = $('#talla').val();
    var elemento = $('#imc');
    var imc = (parseFloat(peso) / Math.pow(parseFloat(talla),2)).toFixed(2);
    elemento.val(imc);
    if(imc<16){
      elemento.css('color','red');
      elemento.attr('title','Delgadez severa').tooltip('fixTitle').tooltip('show');
    } else if (imc < 17 && imc >= 16) {
      elemento.css('color','orange');
      elemento.attr('title','Delgadez moderada').tooltip('fixTitle').tooltip('show');
    } else if (imc < 18.5 && imc >= 17) {
      elemento.css('color','yellow');
      elemento.attr('title','Delgadez aceptable').tooltip('fixTitle').tooltip('show');
    } else if (imc < 25 && imc >= 18.5) {
      elemento.css('color','green');
      elemento.attr('title','Normal').tooltip('fixTitle').tooltip('show');
    } else if (imc < 30 && imc >= 25) {
      elemento.css('color','yellow');
      elemento.attr('title','sobrepeso').tooltip('fixTitle').tooltip('show');
    } else if (imc < 35 && imc >= 30) {
      elemento.css('color','orange');
      elemento.attr('title','Obeso tipo I').tooltip('fixTitle').tooltip('show');
    } else if (imc < 40 && imc >= 35 ) {
      elemento.css('color','red');
      elemento.attr('title','Obeso tipo II').tooltip('fixTitle').tooltip('show');
    } else if (imc >= 40) {
      elemento.css('color','red');
      elemento.attr('title','Obeso tipo III').tooltip('fixTitle').tooltip('show');
    }
  }
  function addDiagnostico() {
    if ($('#form-diagnostico').valid()) {
      $('#tabla-diagnostico').DataTable().row.add([
      ($('#diag_textual').val() == "") ? '-------' : $("#diag_textual").val() ,
      $('#cie10').val()+'-'+$('#cie10 option:selected').text(),
      $('#diag_1ra_vez').prop('checked') ? 'SI' : 'NO',
      $('#diag_1ra_vez_anio').prop('checked') ? 'SI' : 'NO',
      $('#programaXmotivo').val()+'-'+$('#programaXmotivo option:selected').text(),
        '<button type="button" class="btn btn-danger btn-xs" onclick="removerFila('+"'diagnostico'"+',this)"><i class="fa fa-remove"></i> Eliminar</button>'
      ]).draw();
      $('#form-diagnostico')[0].reset();
      $('#modal-diagnostico').modal('hide');
      confirmacion(true,'diagnostico');
    }
  }
  function addTratamiento() {
    if ($('#form-tratamiento').valid()) {
      $('#tabla-tratamiento').DataTable().row.add([
        $('#tratamiento').val()+"-"+$('#tratamiento').text(),
        $('#cantidad').val()+" "+$('#cantidad').next().val(),
        $('#intervalo').val()+" horas",
        $('#duracion').val()+" dias",
        '<button type="button" class="btn btn-danger btn-xs" onclick="removerFila('+"'tratamiento'"+',this)"><i class="fa fa-remove"></i> Eliminar</button>'
      ]).draw()
      $('#form-tratamiento')[0].reset();
      $('#modal-tratamiento').modal('hide');
      confirmacion(true,'tratamiento');
    }
  }
  function addAtencionPreventiva() {
    if ($('#form-preventiva').valid()) {
      $("#tabla-actividad").DataTable().row.add([
        'PREVENTIVA',
        $("#preventiva").val(),
        '-------',
        '-------',
        '<button type="button" class="btn btn-danger btn-xs" onclick="removerFila('+"'actividad'"+', this)"><i class="fa fa-remove"></i> Eliminar</button>'
      ]).draw();
      confirmacion(true, 'servicio');
      $('#form-preventiva')[0].reset();
    }
  }
  function addAtencionCurativa() {
    if ($('#form-curativa').valid()) {
      $("#tabla-actividad").DataTable().row.add([
        'CURATIVA',
        $("#tratamiento_dental").val(),
        $("#pieza_dental").val(),
        $("#cantidad_t_d").val(),
        '<button type="button" class="btn btn-danger btn-xs" onclick="removerFila('+"'actividad'"+', this)"><i class="fa fa-remove"></i> Eliminar</button>'
      ]).draw();
      confirmacion(true, 'servicio');
      $('#form-curativa')[0].reset();
    }
  }
  function removerFila(tabla,elemento) {
    swal({
      title              : '¿Está seguro?',
      text               : '¡Usted va a eliminar un '+tabla+'!',
      type               : 'warning',
      focusCancel        : true,
      showCancelButton   : true,
      confirmButtonColor : '#C51414',
      cancelButtonColor  : '#554747',
      confirmButtonText  : 'Si',
      cancelButtonText   : 'No',
      allowEscapeKey     : true,
      allowOutsideClick  : true
    }).then((result)=>{
      if(result.value){
        $('#tabla-'+tabla).DataTable().row( $(elemento).parents('tr') ).remove().draw();
        confirmacion(false,tabla);
      }
    });
  }
  function confirmacion(bandera,keyword) {
    swal({
      title: bandera ? 'Agregado':'¡Elimado!',
      text : 'El '+keyword+ (bandera ? ' se ha agregado' : ' se ha eliminado'),
      type : bandera ? 'success' : 'info',
      timer: 3350,
      toast: true,
      showConfirmButton : false
    })
  }
  function getDatos() {
    var datos = {
      discapacidad_id : $('#discapacidad').val(),
      peso : $('#peso').val(),
      talla : $('#talla').val(),
      referido : $('#referido').prop('checked'),
      contrareferido : $('#contrareferido').prop('checked')
    }
    $('.datatable').each(function () {
      var nombre = $(this).attr('name');
      var tabla = $(this).DataTable().rows().data();
      if(tabla.length > 0){
        datos[nombre] = []
        for (var i = 0; i < tabla.length; i++) {
          datos[nombre][i] = tabla[i];
        }
      }
    });
    if ($('#embarazo').prop('checked'))
      datos['embarazo'] = getDatosEmbarazo();
    if (area == 'psicologia')
      datos['psicologia'] = getDatosPsicologia();
    if (area == 'psocologia' && menor)
      datos['menor'] = getDatosMenor();
    
    return datos;
  }
  function getDatosEmbarazo() {
    data = {
      trimestre_gestacional : $('#trimestre_gestacional').val(),
      primer_embarazo : $('#primer_embarazo').prop('checked'),
      acido_folico : $('#acido_folico').prop('checked'),
      anticonceptivo : $('#anticonceptivo').prop('checked'),
      terapia : $('#terapia').val(),
      descanso : $('#descanso').val()
    };
    if ($('#riesgo').prop('checked')) {
      data['riesgo'] = {
        riesgo_de : $('#riesgo_de').val(),
        infeccion : $('#infeccion').prop('checked'),
        hemorragia : $('#hemorragia').prop('checked'),
        apoyo : $('#apoyo').prop('checked'),
        analisis : $('#analisis').val()
      };
    }
    return data;
  }
  function getDatosMenor(){
    return {
      ninio_sano : $('#ninio_sano').prop('checked'),
      cancer : $('#cancer').prop('checked'),
      tipo_edi : $('#tipo_edi').val(),
      res_edi : $('#res_edi').val(),
      res_battelle : $('#res_battelle').val()
    }
  }
  function getDatosPsicologia() {
    return {
      adiccion : $("#adiccion").val(),
      entrevista : $("#entrevista").val(),
      e_psicometrico : $("#e_psicometrico").val(),
      p_individual : $("#p_individual").val(),
      p_grupo : $("#p_grupo").val(),
      p_pareja : $("#p_pareja").val()
    };
  }
  function procesarConsulta() {
    if($('#form-consulta').valid()){
      if ($('#tabla-diagnostico').length && !$('#tabla-diagnostico').DataTable().rows().data().length) {
        oglibaTabla('diagnosticos');//console.log('no hay diagnosticos');
        return;
      }
      if ($('#tabla-tratamiento').length && !$('#tabla-tratamiento').DataTable().rows().data().length) {
        oglibaTabla('tratamientos');//console.log('no hay tratamientos');
        return;
      }
      if ($('#tabla-actividad').length && !$('#tabla-actividad').DataTable().rows().data().length) {
        oglibaTabla('actividades');//console.log('no hay actividades');
        return;
      }
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      var form = $('#form-consulta')
      if(form.valid()){
        block();
        $.ajax({
          url          : form.attr('action'),
          type         : form.attr('method'),
          data         : getDatos(),
          dataType     : 'JSON',
          success      : function(response) {
            app.set_bloqueo(false);
            unblock();
            swal({
              title: '¡Guardado!',
              text : 'La consulta se ha agregado correctamente',
              type : 'success',
              allowOutsideClick : false,
              allowEscapeKey: false
            }).then((x)=>{
              window.location.href = "{{ route('consultasmedicas.pacientes.index') }}/"+window.location.pathname.split('/')[3];
            });
          },
          error        : function(response){
            unblock();
            swal({
              title             : '¡Error!',
              text              : 'La consulta no se ha guardado',
              type              : 'error',
              timer             : 2500,
              showConfirmButton : false
            })
          }
        });
      }
    }
  }
  function oglibaTabla(name){
    swal({
      title : 'Por favor ingrece datos',
      text  : 'la tabla '+name+' no tiene datos',
      type  : 'warning',
      allowOutsideClick : false,
      allowEscapeKey: false
    });
  }
  //inicio de validaciones
  function getRulesConsulta() {
    if (area=='general') {
      return {
        peso : {
          required : true
        },
        talla : {
          required : true
        }
      }
    }
  }
  $('#form-consulta').validate({
    rules: getRulesConsulta(),
    messages: {
    }
  });
  $('#form-tratamiento').validate({
    rules: {
      tratamiento: {
        required: true
      },
      intervalo: {
        required: true
      },
      cantidad: {
        required: true
      },
      duracion: {
        required: true
      }
    },
    messages: {
    }
  });
  $('#form-diagnostico').validate({
    rules: {
      cie10: {
        required: true
      },
      programaXmotivo: {
        required: true
      },
      diag_textual: {
        required: true
      }
    },
    messages: {
    }
  });
  $('#form-preventiva').validate({
    rules: {
      preventiva : {
        required: true
      }
    },
    messages: {
    }
  });
  $('#form-curativa').validate({
    rules: {
      pieza_dental: {
        required: true
      },
      tratamiento_dental: {
        required: true
      },
      cantidad_t_d: {
        required: true
      }
    },
    messages: {
    }
  });

  $.extend($.validator.messages, {
    required: 'Este campo es obligatorio.',
    remote: 'Por favor, rellena este campo.',
    email: 'Por favor, escribe una dirección de correo válida.',
    url: 'Por favor, escribe una URL válida.',
    date: 'Por favor, escribe una fecha válida.',
    dateISO: 'Por favor, escribe una fecha (ISO) válida.',
    number: 'Por favor, escribe un número válido.',
    digits: 'Por favor, escribe sólo dígitos.',
    creditcard: 'Por favor, escribe un número de tarjeta válido.',
    equalTo: 'Por favor, escribe el mismo valor de nuevo.',
    extension: 'Por favor, escribe un valor con una extensión aceptada.',
    maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
    minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
    rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
    range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
    max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
    min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
    nifES: 'Por favor, escribe un NIF válido.',
    nieES: 'Por favor, escribe un NIE válido.',
    cifES: 'Por favor, escribe un CIF válido.'
  });

  $.validator.setDefaults({
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorPlacement: function(error, element) {
      $(element).parents('.form-group').append(error);
    }
  });
  </script>
@endpush
