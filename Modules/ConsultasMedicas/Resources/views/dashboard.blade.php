@extends('consultasmedicas::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
</style>
@endpush

@section('content-subtitle', 'Dashboard')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Medicos</h3>
	</div>
	<div class="box-body">
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-medical-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-number">Diagnostico mas común</span>
          <span class="">{{ $diag->nombre }} <small>({{ $diag->top }})</small></span>
        </div>
      </div>
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-primary"><i class="ion ion-ios-medical-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-number">Tratamiento mas comun</span>
          <span class="">{{ $trat->nombre_generico }} <small>({{ $trat->forma_farmaceutica }})</small></span>
        </div>
      </div>
    </div>
    
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-blue"><i class="ion ion-ios-medical-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-number">Servicio odontologico mas frecuente</span>
          <span class="">{{ $serv->nombre }} <small>({{ $serv->tipo }})</small></span>
        </div>
      </div>
    </div>
	</div>
</div>

<div class="col-xs-6">
  <div class="box box-info" >
    <div class="box-header with-border">
      <h3 class="box-title">Distribución por área </h3>
    </div>
    
    <div class="box-body">    
      <div style="width:100%;">
        {!! $pastel->render() !!}
      </div>  
    </div>
  </div>
</div>
<div class="col-xs-6">
  <div class="box box-info" >
    <div class="box-header with-border">
      <h3 class="box-title">Total de consultas</h3>
    </div>
    
    <div class="box-body">    
      <div style="width:100%;">
        {!! $barras->render() !!}
      </div>  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-cita" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
    </div>
  </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components\chart.js\Chart.js') }}"></script>
@endpush