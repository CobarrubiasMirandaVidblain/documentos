<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="nav-icon fa fa-home"></i> Home
          {{-- <span class="badge badge-primary">NEW</span> --}}
        </a>
      </li>
      <li class="nav-title">Viaticos</li>
      <li class="nav-item">
        <a class="nav-link" href="colors.html">
          <i class="nav-icon fa fa-user"></i> Nuevo</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="typography.html">
          <i class="nav-icon icon-pencil"></i> Consultar</a>
      </li>
      <li class="nav-title">Vales</li>
      {{-- <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon icon-puzzle"></i> Base</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="base/breadcrumb.html">
              <i class="nav-icon icon-puzzle"></i> Breadcrumb</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="base/cards.html">
              <i class="nav-icon icon-puzzle"></i> Cards</a>
          </li>
        </ul>
      </li> --}}
      
      <li class="divider"></li>
      
      {{-- <li class="nav-item mt-auto">
        <a class="nav-link nav-link-success" href="https://coreui.io" target="_top">
          <i class="nav-icon icon-cloud-download"></i> Download CoreUI</a>
      </li> --}}
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>