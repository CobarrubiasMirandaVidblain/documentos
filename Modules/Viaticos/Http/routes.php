<?php

Route::group(['middleware' => 'web', 'prefix' => 'viaticos', 'namespace' => 'Modules\Viaticos\Http\Controllers'], function()
{
  Route::get('/', 'ViaticosController@index');
});