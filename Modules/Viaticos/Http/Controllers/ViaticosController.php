<?php

namespace Modules\Viaticos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Firebase\JWT\JWT;
use Carbon\Carbon;

class ViaticosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $usuario = auth()->user();
        $token = [
            'usuario_id' => $usuario->id,
            'modulo_id' => 1,
            'rol_id' => 1
        ];
        $encoded = JWT::encode($token, 'mitoken', 'HS256');
        $usuario->token = $encoded;
        $usuario->vida_token = Carbon::now()->addMinutes(40); 
        $usuario->save();
        return view('viaticos::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('viaticos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('viaticos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('viaticos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function probarmitoken()
    {
        return new JsonResponse(['message' => 'Ok!']);
    }
}
