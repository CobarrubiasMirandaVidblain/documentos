let mix = require('laravel-mix');

mix.js('Resources/assets/js/app.js', './../../public/viaticosm')
  .sass('Resources/assets/sass/app.scss', './../../public/viaticosm')
  // .options({
  //     processCssUrls: false
  // })
  .setPublicPath('./../../public/viaticosm');