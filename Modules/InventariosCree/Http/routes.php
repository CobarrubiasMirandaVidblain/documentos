<?php

Route::group(['middleware' => 'web', 'prefix' => 'inventarios-cree', 'namespace' => 'Modules\InventariosCree\Http\Controllers', 'as' => 'inventarios-cree.'], function()
{
    Route::get('/', 'HomeController@index')->name('index');

    Route::prefix('categorias')->name('categorias.')->group(function() {
	    Route::get('/', 'CategoriaController@index')->name('index');
	    Route::get('/getCategorias', 'CategoriaController@getCategorias')->name('getCategorias');
		Route::post('/registrar', 'CategoriaController@store')->name('registrar');
		Route::put('/actualizar', 'CategoriaController@update')->name('actualizar');
		Route::put('/setStatus', 'CategoriaController@setStatus')->name('setStatus');
	});

	Route::prefix('articulos')->name('articulos.')->group(function() {
	    Route::get('/', 'ArticuloController@index')->name('index');
	    Route::get('/getArticulos', 'ArticuloController@getArticulos')->name('getArticulos');
		Route::post('/registrar', 'ArticuloController@store')->name('registrar');
		Route::put('/actualizar', 'ArticuloController@update')->name('actualizar');
		Route::put('/setStatus', 'ArticuloController@setStatus')->name('setStatus');
	});
});