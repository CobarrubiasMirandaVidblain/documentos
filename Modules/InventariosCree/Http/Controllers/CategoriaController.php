<?php

namespace Modules\InventariosCree\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\InventariosCree\Categoria;
use Modules\InventariosCree\Http\Requests\CategoriaRequest;

class CategoriaController extends Controller
{
    public function index(Request $request)
    {
        return view('inventarioscree::categorias');
    }

    public function getCategorias(Request $request)
    {
        if(!$request->ajax()) return response()->json(['msg' => 'No esta permitido'], 422);

        return Categoria::all();
    }

    public function store(CategoriaRequest $request)
    {
        if(!$request->ajax()) return response()->json(['msg' => 'No esta permitido'], 422);

        DB::transaction(function() use($request) {
            $categoria = new Categoria();
            $categoria->nombre = $request->nombre;
            $categoria->descripcion = $request->descripcion;
            $categoria->usuario_id = auth()->user()->id;
            // $categoria->area()->attach(1); // Pendiente de saber que área debe ir aquí
            $categoria->save();
        });
    }

    public function update(CategoriaRequest $request)
    {
        if(!$request->ajax()) return response()->json(['msg' => 'No esta permitido'], 422);

        $categoria = Categoria::findOrFail($request->id);
        $categoria->nombre = $request->nombre;
        $categoria->descripcion = $request->descripcion;
        // $categoria->active = 1;
        $categoria->save();
        return response()->json(['msg' => 'Eliminada']);
    }

    public function setStatus(Request $request)
    {
        if(!$request->ajax()) return response()->json(['msg' => 'No esta permitido'], 422);
        
        $categoria = Categoria::findOrFail($request->id);
        // $categoria->active = $request->status;
        // $categoria->save();
        $categoria->delete();
        return response()->json(['msg' => 'Eliminada']);
    }
}