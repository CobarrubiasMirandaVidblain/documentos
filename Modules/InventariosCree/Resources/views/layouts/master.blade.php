<!-- Extender del layout principal -->
@extends('vendor.admin-lte.layouts.main')

<!-- Nombre de la aplicación -->
@section('title', 'Inventarios CREE')

<!-- Styles -->
@section('styles')
    @parent

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert2/sweetalert2.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/timer.css') }}">
    <style>
        #loader-container {
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
            position: fixed;
            top: 0;
            left: 0;
            z-index: 10000; /* Just to keep it at the very top */
        }

        #loader-image {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        #loader-text {
            color: white;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-40%, 125%);
        }

        .fade-enter-active, .fade-leave-active {
            transition: opacity .5s;
        }

        .fade-enter, .fade-leave-to {
            opacity: 0;
        }
    </style>
@endsection

<!-- Opciones del navbar (número de notificaciones, nombre del dropdown y lista de opciones) para mensajes, notificaciones y tareas -->
<!-- Mensajes -->
@section('message-text', 'No hay mensajes')

<!-- Notificaciones -->
@section('notification-text', 'No hay notificaciones')

<!-- Tareas -->
@section('task-text', 'No hay tareas')

<!-- Nombre de usuario -->
@if (auth()->check())
    @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
    @section('user-name', auth()->user()->persona->nombre)
@endif

<?php $ruta = Request::route()->getName(); ?>

<!-- Sidebar -->
@section('sidebar-menu')
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVEGACIÓN</li>
        <li class="{{ $ruta == 'inventarios-cree.index' ? 'active' : '' }}">
            <a href="{{ route('inventarios-cree.index') }}">
                <i class="fa fa-home"></i>
                <span>Inicio</span>
            </a>
        </li>
        <li class="{{ $ruta == 'inventarios-cree.categorias.index' ? 'active' : '' }}">
            <a href="{{ route('inventarios-cree.categorias.index') }}">
                <i class="fa fa-tags"></i>
                <span>Categorías</span>
            </a>
        </li>
        <li class="{{ $ruta == 'inventarios-cree.articulos.index' ? 'active' : '' }}">
            <a href="{{ route('inventarios-cree.articulos.index') }}">
                <i class="fa fa-shopping-cart"></i>
                <span>Artículos</span>
            </a>
        </li>
        <li class="{{ $ruta == 'inventarios-cree.bodegas.index' ? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-building"></i>
                <span>Bodegas</span>
            </a>
        </li>
        <li class="{{ $ruta == 'inventarios-cree.entradas.index' ? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-sign-in"></i>
                <span>Entradas</span>
            </a>
        </li>
        <li class="{{ $ruta == 'inventarios-cree.salidas.index' ? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-sign-out"></i>
                <span>Salidas</span>
            </a>
        </li>
        <li class="{{ $ruta == 'inventarios-cree.inventario.index' ? 'active' : '' }}">
            <a href="#">
                <i class="fa fa-list"></i>
                <span>Inventario</span>
            </a>
        </li>
    </ul>
@endsection

<!-- Scripts -->
@section('scripts')
    @parent

    <script type="text/javascript" src="{{ asset('bower_components/moment/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vue.js') }}" charset="utf-8"></script>
    <script type="text/javascript" src="{{ asset('js/vee-validate.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        Vue.use(VeeValidate);
    </script>
@endsection