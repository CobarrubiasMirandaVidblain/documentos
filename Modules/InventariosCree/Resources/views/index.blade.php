<!-- Extender el layout del módulo -->
@extends('inventarioscree::layouts.master')

<!-- Nombre de la página actual -->
@section('title')
	@parent - Inicio
@endsection

<!-- Título y subtítulo -->
@section('content-title', '')
@section('content-subtitle', '')

<!-- Breadcrumbs -->
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li>
			<i class="fa fa-home"></i>
			Inicio
		</li>
	</ol>
@endsection

@section('content')
    <h1 class="text-center">
        Bienvenido a Inventarios CREE
    </h1>
@stop