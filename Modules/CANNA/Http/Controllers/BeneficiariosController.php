<?php

namespace Modules\CANNA\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\DataTables\CANNA\CandidatosDataTable;

use App\DataTables\CANNA\BeneficiariosDataTable;

use App\Models\PersonasPrograma;
use App\Models\Tutor;
use App\Models\PersonasTipo;
use App\Models\AlumnosCanna;

use App\Models\Encuesta;
use App\Models\Pregunta;
use App\Models\PreguntasPersona;

use App\Models\AniosPrograma;
use App\Models\Programa;

use App\Models\Tipospersona;
use App\Models\Modulo;
use App\Models\Tabla;

class BeneficiariosController extends Controller
{
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:ADMINISTRADOR,TERAPEUTA');
  }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(BeneficiariosDataTable $dataTable)
    {
      return $dataTable
      ->render('canna::beneficiarios.index');
      //return view('canna::beneficiarios.index');
    }

    public function create(CandidatosDataTable $dataTable){
      return $dataTable->render('canna::beneficiarios.create');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */


    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){
      if ($request->ajax()) {
        try {
          DB::beginTransaction();

          $datos = $request;

          $validacion = AlumnosCanna::where('persona_id', $datos->beneficiario_id)->first();
          if($validacion !== null) {
            abort(409,'niño ya registrado');//return response()->json(array('existente' => true));
          }

          $aux['persona_id'] = $datos->beneficiario_id;
          $programa = Programa::where('nombre', 'CANNA')->first();
          $aux['anios_programa_id'] = AniosPrograma::where('programa_id', $programa->id)->first()->id;
          $aux['baja'] = 0;
          $aux['usuario_id'] = $request->User()->id;
          $pp1 = PersonasPrograma::create($aux);
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas_programas',
              'registro' => $pp1->id . '',
              'campos' => json_encode($pp1) . '',
              'metodo' => request()->method()
          ]);

          $tipo['tipospersona_id'] = Tipospersona::where('tipo', 'BENEFICIARIO')->first()->id;
          $tipo['persona_id'] = $datos->beneficiario_id;
          $tipo['modulo_id'] = Modulo::where('nombre','like','CANNA')->first()->id;
          $tipo['tablabd_id'] = Tabla::where('nombre','like', 'alumnoscanna')->first()->id;
          $pt1 = PersonasTipo::create($tipo);
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas_tipos',
              'registro' => $pt1->id . '',
              'campos' => json_encode($pt1) . '',
              'metodo' => request()->method()
          ]);

          $alumno['persona_id'] = $datos->beneficiario_id;
          $folio =  '1' . '/' . date('Y');
          if(AlumnosCanna::count() > 0){
            $folio = (AlumnosCanna::get()->last()->id) + 1 . '/' . date('Y');
          }
          $alumno['folio'] = $folio;
          //$alumno['gruponivel_id'] = 7; por que existia esta lina!? .---.
          if(null !== $datos->diagnostico)
            $alumno['diagnostico'] = $datos->diagnostico;
          if(null !== $datos->medico)
            $alumno['medico'] = $datos->medico;
          if(null !== $datos->medicamento)
            $alumno['medicamento'] = $datos->medicamento;
          if(null !== $datos->observaciones)
            $alumno['observaciones'] = $datos->observaciones;
          $alumno = AlumnosCanna::create($alumno);
          auth()->user()->bitacora(request(), [
              'tabla' => 'alumnoscanna',
              'registro' => $alumno->id . '',
              'campos' => json_encode($alumno) . '',
              'metodo' => request()->method()
          ]);

          $aux['persona_id'] = $datos->tutor_id;
          $pp2 = PersonasPrograma::create($aux);
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas_programas',
              'registro' => $pp2->id . '',
              'campos' => json_encode($pp2) . '',
              'metodo' => request()->method()
          ]);

          $tutor['persona_id'] = $datos->tutor_id;
          $tutor['tutorado_id'] = $datos->beneficiario_id;
          $programa = Programa::where('nombre', 'CANNA')->first();
          $tutor['programa_id'] = $programa->id;
          $tutor['activo'] = 1;
          $tutor['usuario_id'] = $request->User()->id;
          $t1 = Tutor::create($tutor);
          auth()->user()->bitacora(request(), [
              'tabla' => 'tutores',
              'registro' => $t1->id . '',
              'campos' => json_encode($t1) . '',
              'metodo' => request()->method()
          ]);

          $tipo['tipospersona_id'] = 1;
          $tipo['persona_id'] = $datos->tutor_id;
          $tipo['tabla'] = 'tutor';

          $tipo['tipospersona_id'] = Tipospersona::where('tipo', 'TUTOR')->first()->id;
          $tipo['persona_id'] = $datos->tutor_id;
          $tipo['modulo_id'] = Modulo::where('nombre','CANNA')->first()->id;
          $tipo['tablabd_id'] = Tabla::where('nombre', 'tutores')->first()->id;
          $pt2 = PersonasTipo::create($tipo);
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas_tipos',
              'registro' => $pt2->id . '',
              'campos' => json_encode($pt2) . '',
              'metodo' => request()->method()
          ]);
          //Guardando tutor 2 si hay
          if(isset($request->tutor2_id)){
            $aux['persona_id'] = $datos->tutor2_id;
            $pp3 = PersonasPrograma::create($aux);
            auth()->user()->bitacora(request(), [
                'tabla' => 'personas_programas',
                'registro' => $pp3->id . '',
                'campos' => json_encode($pp3) . '',
                'metodo' => request()->method()
            ]);

            $tutor['persona_id'] = $datos->tutor2_id;
            $t2 = Tutor::create($tutor);
            auth()->user()->bitacora(request(), [
                'tabla' => 'tutores',
                'registro' => $t2->id . '',
                'campos' => json_encode($t2) . '',
                'metodo' => request()->method()
            ]);

            $tipo['persona_id'] = $datos->tutor2_id;
            $pt3 = PersonasTipo::create($tipo);
            auth()->user()->bitacora(request(), [
                'tabla' => 'personas_tipos',
                'registro' => $pt3->id . '',
                'campos' => json_encode($pt3) . '',
                'metodo' => request()->method()
            ]);
          }

          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          abort(500,'Error al registrar en la BD: ' . $e->getMessage());
          //return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
      $alumno = AlumnosCanna::find($id);

      $fdiagnosticas = PreguntasPersona::where([
        ['persona_id', '=', $alumno->persona->id],
        ['pregunta_id', '=', Pregunta::where('pregunta', '=', 'LENGUAJE Y COMUNICACIÓN')->first()->id]
      ])->get();

      $ptrabajo = PreguntasPersona::where([
        ['persona_id', '=', $alumno->persona->id],
        ['pregunta_id', '=', Pregunta::where('pregunta', '=', 'VESTIDO')->first()->id]
      ])->get();
      $programa = Programa::where('nombre', 'CANNA')->first();
      $tutores = Tutor::where([
        ['tutorado_id', '=', $alumno->persona->id],
        ['programa_id', '=', $programa->id]
        ])->get();

      //return $ptrabajo;
      return view('canna::beneficiarios.show')
      ->with('alumno', $alumno)
      ->with('tutores', $tutores)
      ->with('fdiagnosticas', $fdiagnosticas)
      ->with('ptrabajo', $ptrabajo);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('canna::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function cambia_estado($id, Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();
          $alumno = AlumnosCanna::find($id);
          
          if($request->estado == 'true'){
            AlumnosCanna::withTrashed()->where('id', $id)->restore();
          } else {
            AlumnosCanna::destroy($id);
          }
          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    public function select(Request $request) {
      if($request->ajax()) {
          $alumnos = AlumnosCanna::join('personas', 'personas.id', '=', 'alumnoscanna.persona_id')
          ->where('personas.nombre', 'LIKE', '%' . $request->input('search') . '%')
          ->orWhere('personas.primer_apellido', 'LIKE', '%' . $request->input('search') . '%')
          ->orWhere('personas.segundo_apellido', 'LIKE', '%' . $request->input('search') . '%')
          ->take(10)
          ->get()
          ->toArray();
          return response()->json($alumnos);
      }
    }
}
