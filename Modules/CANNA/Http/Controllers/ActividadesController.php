<?php

namespace Modules\CANNA\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\DataTables\CANNA\ActividadesDataTable;

use App\Models\Programa;
use App\Models\EscuelaPrograma;

class ActividadesController extends Controller
{
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:ADMINISTRADOR,TERAPEUTA');

  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ActividadesDataTable $dataTable)
    {
      return $dataTable
      ->render('canna::actividades.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('canna::actividades.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();

          $aux = $request->all();
          $aux['activo'] = 1;
          $aux['tipo'] = 'SERVICIO';
          $aux['oficial'] = 1;
          $aux['usuario_id'] = $request->User()->id;
          $aux['padre_id'] = Programa::where('nombre','CANNA')->first()->id;

          $programa = Programa::create($aux);

          auth()->user()->bitacora(request(), [
              'tabla' => 'programas',
              'registro' => $programa->id . '',
              'campos' => json_encode($programa) . '',
              'metodo' => request()->method()
          ]);

          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('canna::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
      $actividad = Programa::find($id);
      return view('canna::actividades.create')
      ->with('actividad', $actividad);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();
          Programa::find($id)->update($request->all());
          $programa = Programa::find($id);
          auth()->user()->bitacora(request(), [
              'tabla' => 'programas',
              'registro' => $programa->id . '',
              'campos' => json_encode($programa) . '',
              'metodo' => request()->method()
          ]);
          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();

          $programa ::find($id);
          auth()->user()->bitacora(request(), [
              'tabla' => 'programas',
              'registro' => $programa->id . '',
              'campos' => json_encode($programa) . '',
              'metodo' => request()->method()
          ]);
          $programa->delete();

          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    public function select(Request $request) {
        if($request->ajax()) {
            $programas = Programa::where([['padre_id',7],['nombre', 'LIKE', '%' . $request->input('search') . '%']])// Programa::where('nombre','CANNA')->first())
                ->take(5)
                ->get()
                ->toArray();
            return response()->json($programas);
        }
    }

}
