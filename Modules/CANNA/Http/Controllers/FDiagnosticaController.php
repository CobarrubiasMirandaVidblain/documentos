<?php

namespace Modules\CANNA\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Models\PreguntasPersona;
use App\Models\RespuestasAbiertas;
use App\Models\Pregunta;
use App\Models\Encuesta;
use App\Models\AlumnosCanna;

class FDiagnosticaController extends Controller
{

  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:ADMINISTRADOR,TERAPEUTA');
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('canna::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        return view('canna::beneficiarios.fichasDiagnosticas.create')
        ->with('alumno_id', $id)
        ->with('opcion', 'create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request, $id)
    {
      if($request->ajax()){
        try {
          DB::beginTransaction();

          $encuesta = Encuesta::where('encuesta', '=', 'FICHA DIAGNOSTICA')->first();
          $alumno = AlumnosCanna::where('id', $id)->first();
          $empleado = $request->User()->persona->empleado;
          $fecha = Carbon::now();
          $fecha = $fecha->toDateString();

          $preguntas_array = array('LENGUAJE Y COMUNICACIÓN', 'SOCIAL', 'CONDUCTA', 'SENSORIAL',
                                    'AUTONOMIA', 'COGNITIVO', 'MOTRICIDAD');
          $respuestas_llaves = array('lenguaje_comunicacion', 'social', 'conducta', 'sensorial',
                                    'autonomia', 'cognitivo', 'motricidad');
          $pp['persona_id'] = $alumno->persona_id;
          $pp['empleado_id'] = $empleado->id;
          $pp['fecha_encuesta'] = $fecha;


          for ($i=0 ; $i<count($preguntas_array) ; $i++) {
            $pregunta = Pregunta::where([
              ['pregunta', '=', $preguntas_array[$i]],
              ['encuesta_id', '=', $encuesta->id]
            ])->first();
            $pp['pregunta_id'] = $pregunta->id;

            $pp_bd = PreguntasPersona::create($pp);
            auth()->user()->bitacora(request(), [
                'tabla' => 'preguntas_personas',
                'registro' => $pp_bd->id . '',
                'campos' => json_encode($pp_bd) . '',
                'metodo' => request()->method()
            ]);
            $respuesta['preguntas_persona_id'] = $pp_bd->id;

            $respuesta['respuesta'] = $request->input($respuestas_llaves[$i]);
            $ra_bd = RespuestasAbiertas::create($respuesta);

            auth()->user()->bitacora(request(), [
                'tabla' => 'respuestasabiertas',
                'registro' => $ra_bd->id . '',
                'campos' => json_encode($ra_bd) . '',
                'metodo' => request()->method()
            ]);
          }


          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, $num)
    {
      $respuestas = RespuestasAbiertas::whereBetween('preguntas_persona_id', [$num, $num+6])->get();
      //return $respuestas;
        return view('canna::beneficiarios.fichasDiagnosticas.create')
        ->with('alumno_id', $id)
        ->with('respuestas', $respuestas)
        ->with('opcion', 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('canna::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
      if($request->ajax()){
        try {
          DB::beginTransaction();

          $req = $request;

          for ($i=0; $i < count($req->ids); $i++) {
            RespuestasAbiertas::where('id', $req->ids[$i])->update(['respuesta' => $req->respuestas[$i]["value"]]);
            $ra_bd = RespuestasAbiertas::find($req->ids[$i]);
            auth()->user()->bitacora(request(), [
                'tabla' => 'respuestasabiertas',
                'registro' => $ra_bd->id . '',
                'campos' => json_encode($ra_bd) . '',
                'metodo' => request()->method()
            ]);
          }
          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
