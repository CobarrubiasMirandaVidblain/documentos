@extends('canna::layouts.master')

@push('head')
  <style type="text/css">
  </style>
  <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
  <li class="active">Solicitudes</li>
@endsection

@section('content')
  <div class="box box-primary shadow">
    <div class="box-header with-border">
      <h3 class="box-title">solicitud</h3>
    </div>

    <div class="box-body">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:30%">
                <i class="fa fa-id-card margin-r-5"></i>
                Folio:
              </th>
              <td>{{ $beneficio->solicitud->folio }}</td>
            </tr>
            <tr>
              <th>
                <i class="fa fa-user margin-r-5"></i>
                Remitente
              </th>
              <td>
                @if($beneficio->solicitud->solicitudpersonales)
                  {{ $beneficio->solicitud->solicitudpersonales->persona->nombreCompleto() }}
                @elseif($beneficio->solicitud->solicitudmunicipales)
                  {{ $beneficio->solicitud->solicitudmunicipales->municipio->nombre }}
                @elseif($beneficio->solicitud->solicitudregionales)
                  {{ $beneficio->solicitud->solicitudregionales->region->nombre }}
                @elseif($beneficio->solicitud->solicituddependencias)
                  {{ $beneficio->solicitud->solicituddependencias->dependencia->nombre }}
                @endif
              </td>
            </tr>
            <tr>
              <th>
                <i class="fa fa-file-text-o margin-r-5"></i>
                Asunto:
              </th>
              <td>{{ $beneficio->solicitud->asunto }}</td>
            </tr>
          </table>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:40%">
                <i class="fa fa-calendar margin-r-5"></i>
                Fecha de vinculación:
              </th>
              <td>{{ $beneficio->statussolicitudes->where("statusproceso_id",3)->first()->created_at }}</td><!--format('d/m/Y')-->
            </tr>
            <tr>
              <th>
                <i class="fa fa-wheelchair margin-r-5"></i>
                Apoyo Solicitado:
              </th>
              <td id="programa">{{ $beneficio->programa->nombre }}</td>
            </tr>
            <tr>
              <th>
                <i class="fa fa-list-ol margin-r-5"></i>
                Cantidad
              </th>
              <td>{{ $beneficio->cantidad }}</td>
            </tr>
            <tr>
              <th>
                <i class="fa fa-signal margin-r-5"></i>
                Estatus:</th>
                <td id="lblStatus">
                  @if($beneficio->statusActual() == "VINCULADO" )
                    NUEVO
                  @else
                    {{ $beneficio->statusActual() }}
                  @endif
                  @if ($beneficio->statusActual() != 'FINALIZADO')
                    <button type="button" class="btn btn-success pull-right btn-sm" onclick="cambiaStatus('{{$beneficio->statusActual()}}')">
                      <i class="fa fa-mail-forward"></i>
                      @switch ($beneficio->statusActual())
                        @case ("VINCULADO")
                        CAMBIAR A VALIDANDO
                        @break
                        @case ("VALIDANDO")
                        CAMBIAR A FINALIZADO
                        @break
                      @endswitch
                    </button>
                  @endif
                </td>
              </th>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>

  @if ($beneficio->statusActual() == "FINALIZADO" || $beneficio->statusActual() == 'VALIDANDO')
    <div class="box box-primary shadow">
      <div class="box-header with-border">
        <h3 class="box-title">Lista de asistencia</h3>
      </div>
      <div class="box-body">
        @if ($beneficio->statusActual() == 'VALIDANDO')
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <button type="button" class="btn btn-success btn-flat" onclick="abrir_modal_personas()">
                  <i class="fa fa-plus"></i>
                  Agregar persona
                </button>
              </div>
            </div>
          </div>
        @endif
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
          <input type="text" id="searchBeneficiario" name="searchBeneficiario" class="form-control">
          <span class="input-group-btn">
              <button type="submit" class="btn btn-info btn-flat" id="btn_buscarBeneficiario" name="btn_buscarBeneficiario">Buscar</button>
          </span>
        </div>
        {!! $bdt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'solicitantes', 'name' => 'solicitantes', 'style' => 'width: 100%']) !!}
      </div>
    </div>

    @if ($beneficio->statusActual() == 'VALIDANDO')
      <div class="modal fade" id="modal-personas">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="modal-title-personas">Tabla Personas:</h4>
            </div>
            <div class="modal-body" id="modal-body-personas">

              <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                <input type="text" id="search" name="search" class="form-control">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                </span>
              </div>

              {!! $pdt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}

            </div>
            <div class="modal-footer" id="modal-footer-personas">
              <div class="pull-right">
                <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-persona">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title" id="modal-title-persona"></h4>
            </div>
            <div class="modal-body" id="modal-body-persona"></div>
            <div class="modal-footer" id="modal-footer-persona"></div>
          </div>
        </div>
      </div>
    @endif
  @endif
@stop

@push('body')
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
  <script src="/vendor/datatables/buttons.server-side.js"></script>

  {!! $pdt->html()->scripts() !!}
  {!! $bdt->html()->scripts() !!}

@include('personas.js.persona')
  <script type="text/javascript">

  $.fn.modal.Constructor.prototype.enforceFocus = function() {};
  app.to_upper_case();
  app.agregar_bloqueo_pagina();

  var modal_persona = $('#modal-persona');
  var modal_personas = $('#modal-personas');
  var modal_title_persona = $('#modal-title-persona');
  var modal_body_persona = $('#modal-body-persona');
  var modal_footer_persona = $('#modal-footer-persona');

  function abrir_modal_personas() {
    init_modal_persona();
    modal_personas.modal('show');
  }

  function cambiaStatus(statusActual) {
    var satatusDestino = {};
    switch (statusActual) {
      case 'VINCULADO':
      satatusDestino = {status : 'VALIDANDO'};
      break;
      case 'VALIDANDO':
      satatusDestino = {status : 'FINALIZADO'};
      break;
    }

    if(satatusDestino.status == 'FINALIZADO'){
      swal({
        title: '¿Está seguro de pasar a finalizado?',
        text: '¡No podra agregar a mas personas!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'No',
        reverseButtons: true,
        allowEscapeKey: false,
        allowOutsideClick: false
      }).then((result) => {
        if (result.value) {
          enviaDatos(satatusDestino);
        }
      });
    }
    else {
      enviaDatos(satatusDestino);
    }
  }

  function enviaDatos(datos){
    block();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: "POST",
      url: "{{route('canna.peticiones.cambiaestado', $beneficio->id)}}",
      data: datos,
      dataType: 'json',
      success: function(data) {
        //unblock();
        console.log(data);
        location.reload();
      },
      error : function(data){
        unblock();
        console.log(data);
      }
    });
  }

  function agregar_persona() {
    $.get('/personas/search?tipo=create_edit', function(data) {
    })
    .done(function(data) {
    })
    .fail(function(data) {
      modal_title_persona.text('Agregar Persona:');
      modal_body_persona.html(data.responseJSON.html);
      modal_footer_persona.html(
        '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>'+
        '<button type="submit" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'
      );
      app.to_upper_case();
      persona.init();
      persona.editar_fotografia();
      persona.agregar_fecha_nacimiento();
      persona.agregar_inputmask();
      persona.agregar_select_create();
      persona.agregar_validacion();
      modal_persona.modal('show');
    });
  }

  function eliminarBeneficiario(id, nombre) {
    console.log(id);

    swal({
      title: '¿Está seguro de cancelar la persona?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, cancelar',
      cancelButtonText: 'No, regresar',
      reverseButtons: true,
      allowEscapeKey: false,
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type: "DELETE",
          url: "{{ URL::to('canna/solicitudes').'/'.$beneficio->id.'/personas' }}" + '/' + id,
          success: function(data) {
            console.log(data);
            swal({
              title: '¡Persona cancelada!',
              type: 'success',
              showConfirmButton: false,
              timer: 3000
            });
            $("#solicitantes").DataTable().ajax.reload();
          },
          error : function(data){
            console.log(data);
          }
        });
      }
    });

  }

  function seleccionar_persona(id, nombre) {
    console.log(id);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: "POST",
      url: "{{URL::to('canna/solicitudes').'/'.$beneficio->id.'/personas'}}",
      data: {'persona_id' : id},
      dataType: 'json',
      success: function(data) {
        console.log(data);
        swal({
          title: 'Persona registrada',
          text: '',
          type: 'success',
          showConfirmButton: false,
          timer: 2500
        });
        //actualizar data table
        $("#solicitantes").DataTable().ajax.reload();
        modal_personas.modal('hide');
      },
      error : function(data){
        console.log(data);
      }
    });
  }

  function init_modal_persona() {
    var table = $('#personas').dataTable();
    datatable_personas = $(table).DataTable();
    $("#search").keypress(function(e) {
      if(e.which === 13) {
        datatable_personas.search($("#search").val()).draw();
      }
    });
    $("#btn_buscar").on('click', function() {
      datatable_personas.search($("#search").val()).draw();
    });
    modal_personas.on('shown.bs.modal', function(event) {
      datatable_personas.ajax.reload();
    });
  }

  var table = (function() {
    var tabla = undefined,
    btn_buscar = $('#btn_buscar'),
    search = $('#search');

    function init() {
      search.keypress(function(e) {
        if(e.which === 13) {
          tabla.DataTable().search(search.val()).draw();
        }
      });

      btn_buscar.on('click', function() {
        tabla.DataTable().search(search.val()).draw();
      });
    };

    function set_table(valor) {
      tabla = $(valor);
    };

    function get_table() {
      return tabla;
    };

    return {
      init: init,
      set_table: set_table,
      get_table: get_table
    };
  })();

  table.set_table($('#solicitantes').dataTable());
  table.init();

</script>

@endpush
