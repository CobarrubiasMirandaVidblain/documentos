@extends('canna::layouts.master')

@push('head')
<style type="text/css">
select[readonly] {
  background: #eee;
  cursor: no-drop;
}

select[readonly] option {
  display: none;
}

input {
	text-transform: uppercase;
}
</style>
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
  <li><a href="{{route('actividades.index')}}">Actividades</a></li>
  <li class="active">{{isset($actividad) ? 'Editar actividad' : 'Nueva actividad'}}</li>
@endsection

@section('content')
	<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">{{isset($actividad) ? 'Editar actividad' : 'Nueva actividad'}}</h3>
          </div>

          <form id="form_actividad" data-toggle="validator" role="form"
          action="{{isset($actividad) ?
          route('actividades.update', $actividad->id) : route('actividades.store')}}"
          method="{{ isset($actividad) ? 'PUT' : 'POST'}}">
            <div class="box-body">

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
                <div class="form-group">
                  <label for="nombre">NOMBRE</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" placeholder="NOMBRE" value="{{isset($actividad) ? $actividad->nombre : ''}}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
                <div class="form-group">
                  <label for="descripcion">DESCRIPCION</label>
                  <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="DESCRIPCION" value="{{isset($actividad) ? $actividad->descripcion : ''}}">
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="form-group">
                  <label for="codigo">CODIGO</label>
                  <input type="text" class="form-control" id="codigo" name="codigo" placeholder="CODIGO" value="{{isset($actividad) ? $actividad->codigo : ''}}">
                </div>
              </div>

            </div>

            <div class="box-footer">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 pull-right">
                <div class="col-md-6 col-sm-6 col-md-6 col-lg-6">
                  <button id="btn_guardar" type="button" class="btn btn-block btn-primary"  onclick="guardarPrograma()">GUARDAR</button>
                </div>
                <div class="col-md-6 col-sm-6 col-md-6 col-lg-6">
                  <a href="{{route('actividades.index')}}" id="btn_cancelar" class="btn btn-block btn-danger">CANCELAR</a>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
@stop

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

  <script type="text/javascript">

  $(document).ready(function (){
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
  })

  function guardarPrograma() {
    enviarDatosServer();
  }

  function enviarDatosServer() {
    if($("#form_actividad").valid()){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      var form = $("#form_actividad");
      $.ajax({
        type: $('#form_actividad').attr('method'),
        url: $('#form_actividad').attr('action'),
        data: $("#form_actividad").serialize(),
        dataType: 'json',
        success: function(response) {
          console.log(response);
          swal({
            title: 'Actividad registrada',
            text: '',
            type: 'success',
            showConfirmButton: false,
            timer: 2000
          });
          window.location.href = "{{route('actividades.index')}}";
        },
        error: function(response){
          console.log("error");
          console.log(response);
        }
      });
    }
  }

  $("#form_actividad").validate({
    rules : {
      nombre : {
        required : true,
        minlength : 3
      },
      descripcion : {
        required : true,
        minlength : 5
      },
      codigo : {
        required : true,
        minlength : 2
      }
    }
  })
  </script>
@endpush
