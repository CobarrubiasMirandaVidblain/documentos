@extends('canna::layouts.master')

@push('head')
<style type="text/css">
  select[readonly] {
    background: #eee;
    cursor: no-drop;
  }
  .select2-selection__rendered {
        text-transform: uppercase
    }
  select[readonly] option {
    display: none;
  }
</style>

<link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
<li><a href="{{route('canna.beneficiarios.index')}}">Beneficiarios</a></li>
<li class="active">Nuevo</li>
@endsection

@section('content')
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Nuevo beneficiario</h3>
        </div>
        
        <form id="form_beneficiario">
          <div class="box-body">
            
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>BENEFICIARIO</label>
                    <div class="input-group">
                      <select id="beneficiario_id" name="beneficiario_id" class="form-control select2" style="width: 100%;" readonly="readonly"></select>
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-personas', 'beneficiario_id');">
                          <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>DOCUMENTOS DEL BENEFICIARIO</label>
                    <br>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="b_acta" name="b_acta" value=""> ACTA DE NACIMIENTO <br>
                      </label>
                    </div>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="b_curp" name="b_curp" value=""> CURP <br>
                      </label>
                    </div>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="b_diagnostico" name="b_diagnostico" value=""> DIAGNÓSTICO MÉDICO <br>
                      </label>
                    </div>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="diagnostico">DIAGNOSTICO</label>
                    <input type="text" class="form-control" id="diagnostico" name="diagnostico" placeholder="DIAGNOSTICO">
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="medico">MEDICO</label>
                    <input type="text" class="form-control" id="medico" name="medico" placeholder="MEDICO">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="form-group">
                    <label for="medicamento">MEDICAMENTO</label>
                    <select class="form-control select2" id="medicamento" name="medicamento" placeholder="MEDICAMENTO(S)"> {{-- <input type="text" class="form-control" id="medicamento" name="medicamento" placeholder="MEDICAMENTO"> --}}
                    </select>
                  </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label for="observaciones">OBSERVACIONES</label>
                    <textarea name="observaciones" id="observaciones" cols="30" rows="5" class="form-control"></textarea>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9"></div>
              <button type="button" id="btn_tutor2" class="btn btn-primary" onclick="segundoTutor()">AGREGAR SEGUNDO TUTOR</button>
            </div>
            
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>TUTOR</label>
                    <div class="input-group">
                      <select id="tutor_id" name="tutor_id" class="form-control select2" style="width: 100%;" readonly="readonly"></select>
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-personas', 'tutor_id');">
                          <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>DOCUMENTOS DEL TUTOR</label>
                    <br>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="t_ine" name="t_ine" value=""> CREDENCIAL (INE) <br>
                      </label>
                    </div>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="t_domicilio" name="t_domicilio" value=""> COMPROBANTE DE DOMICILIO <br>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div id="seg_tutor" class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="display:none">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>TUTOR</label>
                    <div class="input-group">
                      <select id="tutor2_id" name="tutor2_id" class="form-control select2" style="width: 100%;" readonly="readonly"></select>
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-personas', 'tutor2_id');">
                          <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-12">
                  <div class="form-group">
                    <label>DOCUMENTOS DEL TUTOR</label>
                    <br>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="t2_ine" name="t2_ine" value=""> CREDENCIAL (INE) <br>
                      </label>
                    </div>
                    <div class="form-group">
                      <label>
                        <input type="checkbox" class="flat-red" id="t2_domicilio" name="t2_domicilio" value=""> COMPROBANTE DE DOMICILIO <br>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="box-footer">
              <div class="pull-right">
                <button type="button" class="btn btn-success" onclick="guardarBeneficiario()">GUARDAR</button>
                <a href="{{route('canna.beneficiarios.index')}}" class="btn  btn-danger">CANCELAR</a>
              </div>
            </div>
            
          </form>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="modal-personas">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="modal-title-personas">Tabla Personas:</h4>
          </div>
          <div class="modal-body" id="modal-body-personas">
            
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
              <input type="text" id="search" name="search" class="form-control">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
              </span>
            </div>
            
            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
            
          </div>
          <div class="modal-footer" id="modal-footer-personas">
            <div class="pull-right">
              <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="modal-persona">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="modal-title-persona"></h4>
          </div>
          <div class="modal-body" id="modal-body-persona">
            
          </div>
          <div class="modal-footer" id="modal-footer-persona">
            <div class="pull-right">
              <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </section>
  @stop
  
  @push('body')
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
  
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
  
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
  <script src="/vendor/datatables/buttons.server-side.js"></script>
  {!! $dataTable->scripts() !!}
  
  @include('personas.js.persona');
  
  
  <script type="text/javascript">
    
    $('#medicamento').select2({
      language : 'es',
      multiple : true,
      tags : true
    });
    
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
    
    $(':text').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
    
    btn = document.getElementById('btn_tutor2');
    dv = document.getElementById('seg_tutor');
    
    var elemento = undefined;
    
    var modal_persona = $('#modal-persona');
    var modal_personas = $('#modal-personas');
    var modal_title_persona = $('#modal-title-persona');
    var modal_body_persona = $('#modal-body-persona');
    var modal_footer_persona = $('#modal-footer-persona');
    
    
    $("#form_beneficiario").validate({
      rules : {
        beneficiario_id : {
          required : true
        },
        b_acta : {
          required : false
        },
        b_curp : {
          required : false
        },
        b_diagnostico : {
          required : false
        },
        tutor_id : {
          required : true
        },
        t_ine : {
          required : false
        },
        t_domicilio : {
          required : false
        }
      }
    });
    
    function segundoTutor() {
      if(btn.textContent == 'AGREGAR SEGUNDO TUTOR'){
        dv.style.display = 'inherit';
        btn.innerHTML = 'ELIMINAR SEGUNDO TUTOR';
      }else{
        dv.style.display = 'none';
        btn.innerHTML = 'AGREGAR SEGUNDO TUTOR';
      }
    }
    
    function persona_create_edit_success(response) {
      unblock();
      swal({
        title: 'Persona registrada',
        text: '',
        type: 'success',
        timer: 2000
      });
      modal_persona.modal('hide');
    };
    
    function persona_create_edit_error(response) {
      	unblock();
        if(response.status === 422) {
			swal({
				title: 'Error al registrar',				
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Regresar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
    };
    
    function obtenDatos() {
      datos = {
        beneficiario_id : $("#beneficiario_id").val(),
        b_acta : document.getElementById('b_acta').checked,
        b_curp : document.getElementById('b_curp').checked,
        b_diagnostico : document.getElementById('b_diagnostico').checked,
        diagnostico : $("#diagnostico").val(),
        medico : $("#medico").val(),
        medicamento : JSON.stringify($("#medicamento").val()),
        observaciones : $("#observaciones").val(),
        tutor_id : $("#tutor_id").val(),
        t_ine : document.getElementById('t_ine').checked,
        t_domicilio : document.getElementById('t_domicilio').checked,
        tutor2_id : '',
        t2_ine : false,
        t2t_domicilio : false
      };
      if (btn.textContent == 'ELIMINAR SEGUNDO TUTOR') {
        datos['tutor2_id'] = $("#tutor2_id").val();
        datos['t2_ine'] = document.getElementById('t2_ine').checked;
        datos['t2_domicilio'] = document.getElementById('t2_domicilio').checked;
      }
      return datos;
    }
    
    function guardarBeneficiario() {
      if($("#form_beneficiario").valid()){
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        block();
        $.ajax({
          type: "POST",
          url: "{{route('canna.beneficiarios.store')}}",
          data: obtenDatos(),
          dataType: 'json',
          success: function(data) {
            unblock();
            swal({
              title: 'Beneficiario registrado',
              text: '',
              type: 'success',
              showConfirmButton: false,
              timer: 2500
            });
            window.location.href = "{{route('canna.beneficiarios.index')}}";
          },
          error : function(data){
            unblock();
            swal({
              title: 'Beneficiario no registrado',
              text: 'Por favor reintente',
              type: 'error',
              showConfirmButton: true
            });
            //window.location.href = "{{route('canna.beneficiarios.index')}}";
          }
        });
      }
    }
    
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    
    function agregar_persona() {
      $.get('/personas/search?tipo=create_edit', function(data) {
      })
      .done(function(data) {
      })
      .fail(function(data) {
        modal_title_persona.text('Agregar Persona:');
        modal_body_persona.html(data.responseJSON.html);
        modal_footer_persona.html(
        '<button type="submit" class="btn btn-success" onclick="persona.create_edit();">GUARDAR</button>' +
        '<button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>'
        );
        app.to_upper_case();
        persona.init();
        persona.editar_fotografia();
        persona.agregar_fecha_nacimiento();
        persona.agregar_inputmask();
        persona.agregar_select_create();
        persona.agregar_validacion();
        modal_persona.modal('show');
      });
    }
    
    
    function abrir_modal(modal, el) {
      if(modal == 'modal-personas')
      elemento = $('#' + el);
      $("#"+modal).modal('show');
      $('#search').val('');
      $("#personas").DataTable().search("").draw();
    };
    
    function cerrar_modal(modal){
      $("#"+modal).modal("hide");
    }
    
    function seleccionar_persona(id, nombre){
      if(elemento !== undefined) {
        elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
        modal_personas.modal('hide');
      }
    }
    
    $.extend($.validator.messages, {
      required: 'Este campo es obligatorio.',
      remote: 'Por favor, rellena este campo.',
      email: 'Por favor, escribe una dirección de correo válida.',
      url: 'Por favor, escribe una URL válida.',
      date: 'Por favor, escribe una fecha válida.',
      dateISO: 'Por favor, escribe una fecha (ISO) válida.',
      number: 'Por favor, escribe un número válido.',
      digits: 'Por favor, escribe sólo dígitos.',
      creditcard: 'Por favor, escribe un número de tarjeta válido.',
      equalTo: 'Por favor, escribe el mismo valor de nuevo.',
      extension: 'Por favor, escribe un valor con una extensión aceptada.',
      maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
      minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
      rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
      range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
      max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
      min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
      nifES: 'Por favor, escribe un NIF válido.',
      nieES: 'Por favor, escribe un NIE válido.',
      cifES: 'Por favor, escribe un CIF válido.'
    });
    
    $.validator.setDefaults({
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorPlacement: function(error, element) {
        $(element).parents('.form-group').append(error);
      }
    });
    
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
    
    
    var table = (function() {
      var tabla = undefined,
      btn_buscar = $('#btn_buscar'),
      search = $('#search');
      
      function init() {
        search.keypress(function(e) {
          if(e.which === 13) {
            tabla.DataTable().search(search.val()).draw();
          }
        });
        
        btn_buscar.on('click', function() {
          tabla.DataTable().search(search.val()).draw();
        });
      };
      
      function set_table(valor) {
        tabla = $(valor);
      };
      
      function get_table() {
        return tabla;
      };
      
      return {
        init: init,
        set_table: set_table,
        get_table: get_table
      };
    })();
    
    table.set_table($('#personas').dataTable());
    table.init();
    
    function editar_persona(id) {
      var modal_persona = $('#modal-persona');

      var modal_title_persona = $('#modal-title-persona');

      var modal_body_persona = $('#modal-body-persona');

      var modal_footer_persona = $('#modal-footer-persona');

      var tipo_persona = "'" + tipo_persona + "'";

      $.get('/personas/search?tipo=create_edit&id=' + id, function(data) {
      })
      .done(function(data) {

        modal_title_persona.text('Editar Persona:');
        modal_body_persona.html(data.html);

        app.to_upper_case();

        persona.init();
        persona.editar_fotografia();
        persona.agregar_fecha_nacimiento();
        persona.agregar_inputmask();
        persona.agregar_select_create();
        persona.agregar_select_edit();
        persona.agregar_validacion();

        modal_persona.modal('show');

        var btn_create_edit = $('#btn_create_edit');
        btn_create_edit.attr('class', 'btn btn-warning');
        btn_create_edit.html('<i class="fa fa-pencil"></i> Editar');

      })
      .fail(function(data) {
      });
    }
    
  </script>
  @endpush
  