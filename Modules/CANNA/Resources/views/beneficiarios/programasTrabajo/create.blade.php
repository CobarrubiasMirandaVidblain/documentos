@extends('canna::layouts.master')

@push('head')
<style type="text/css">
textarea {
  resize: none;
  text-transform: uppercase;
}
</style>
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
  <li><a href="{{route('canna.beneficiarios.index')}}">beneficiarios</a></li>
  <li><a href="/canna/beneficiarios/{{$alumno_id}}">Seleccionado</a></li>
  <li class="active">Programa de trabajo</li>
@endsection

@section('content')

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">{{isset($respuestas) ? 'Programa de trabajo' : 'Nuevo programa de trabajo'}}</h3>
        </div>
        <div class="box-body">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="nav-tabs-custom">

              <ul class="nav nav-tabs">
                <li class="active disabled"><a href="#tab_1" data-toggle="tab">Cuidado personal</a></li>
                <li class="disabled"><a href="#tab_2" data-toggle="tab">Habilidades motoras y sensoriales</a></li>
                <li class="disabled"><a href="#tab_3" data-toggle="tab">Habilidades sociales</a></li>
                <li class="disabled"><a href="#tab_4" data-toggle="tab">Habilidades de comunicacion</a></li>
                <li class="disabled"><a href="#tab_5" data-toggle="tab">Habilidades de proceso</a></li>
                <li class="disabled"><a href="#tab_6" data-toggle="tab">Notas</a></li>
              </ul>

              <div class="tab-content">

                <div class="tab-pane active" id="tab_1">

                  <div class="content">
                    <form id="form_c_personal" data-toggle="validator" role="form"
                    action="{{isset($respuestas) ?
                       URL::to('canna/beneficiarios') .'/'. $alumno_id .'/'. 'programatrabajo/' . 1 : route('beneficiarios.programatrabajo.store', $alumno_id)}}"
                    method="{{ isset($respuestas) ? 'PUT' : 'POST' }}">
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="vestido">VESTIDO</label>
                          <textarea type="text" class="form-control" id="vestido" name="vestido" cols="30" rows="3" placeholder="VESTIDO">{{isset($respuestas) ? $respuestas[0]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="alimentacion">ALIMENTACIÓN</label>
                          <textarea type="text" class="form-control" id="alimentacion" name="alimentacion" cols="30" rows="3" placeholder="ALIMENTACIÓN">{{isset($respuestas) ? $respuestas[1]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="banio">BAÑO</label>
                          <textarea type="text" class="form-control" id="banio" name="banio" cols="30" rows="3" placeholder="VESTIDO">{{isset($respuestas) ? $respuestas[2]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="higiene">HIGIENE</label>
                          <textarea type="text" class="form-control" id="higiene" name="higiene" cols="30" rows="3" placeholder="VESTIDO">{{isset($respuestas) ? $respuestas[3]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div>
                    <button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar('tab_1')" style="visibility: hidden"><i class="fa fa-arrow-left"></i> </button>
                    <button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar('tab_2', '#form_c_personal')"><i class="fa fa-arrow-right"></i> </button>
                  </div>


                </div>

                <div class="tab-pane disabled" id="tab_2">

                  <div class="content">
                    <form  id="form_hab_m_s" data-toggle="validator" role="form">
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="mot_gruesa">MOTRICIDAD GRUESA</label>
                          <textarea type="text" class="form-control" id="mot_gruesa" name="mot_gruesa" cols="30" rows="3" placeholder="MOTRICIDAD GRUESA">{{isset($respuestas) ? $respuestas[4]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="mot_fina">MOTRICIDAD FINA</label>
                          <textarea type="text" class="form-control" id="mot_fina" name="mot_fina" cols="30" rows="3" placeholder="MOTRICIDAD FINA">{{isset($respuestas) ? $respuestas[5]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="imitacion">IMITACIÓN</label>
                          <textarea type="text" class="form-control" id="imitacion" name="imitacion" cols="30" rows="3" placeholder="IMITACIÓN">{{isset($respuestas) ? $respuestas[6]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="sensorial">SENSORIAL</label>
                          <textarea type="text" class="form-control" id="sensorial" name="sensorial" cols="30" rows="3" placeholder="SENSORIAL">{{isset($respuestas) ? $respuestas[7]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div>
                    <button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar('tab_1')"><i class="fa fa-arrow-left"></i> </button>
                    <button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar('tab_3', '#form_hab_m_s')"><i class="fa fa-arrow-right"></i> </button>
                  </div>
                </div>

                <div class="tab-pane disabled" id="tab_3">

                  <div class="content">
                    <form  id="form_hab_s" data-toggle="validator" role="form">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="interaccion">INTERACCIÓN SOCIAL</label>
                            <textarea type="text" class="form-control" id="interaccion" name="interaccion" cols="30" rows="3" placeholder="INTERACCIÓN SOCIAL">{{isset($respuestas) ? $respuestas[8]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="participacion">PARTICIPACIÓN SOCIAL</label>
                            <textarea type="text" class="form-control" id="participacion" name="participacion" cols="30" rows="3" placeholder="PARTICIPACIÓN SOCIAL">{{isset($respuestas) ? $respuestas[9]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="juego">JUEGO</label>
                            <textarea type="text" class="form-control" id="juego" name="juego" cols="30" rows="3" placeholder="JUEGO">{{isset($respuestas) ? $respuestas[10]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="conducta">CONDUCTA</label>
                            <textarea type="text" class="form-control" id="conducta" name="conducta" cols="30" rows="3" placeholder="CONDUCTA">{{isset($respuestas) ? $respuestas[11]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="intereses">INTERESES</label>
                            <textarea type="text" class="form-control" id="intereses" name="intereses" cols="30" rows="3" placeholder="INTERESES">{{isset($respuestas) ? $respuestas[12]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div>
                    <button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar('tab_2')"><i class="fa fa-arrow-left"></i> </button>
                    <button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar('tab_4', '#form_hab_s')"><i class="fa fa-arrow-right"></i> </button>
                  </div>
                </div>

                <div class="tab-pane disabled" id="tab_4">

                  <div class="content">
                    <form  id="form_hab_c" data-toggle="validator" role="form">
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="leng_receptivo">LENGUAJE RECEPTIVO</label>
                          <textarea type="text" class="form-control" id="leng_receptivo" name="leng_receptivo" cols="30" rows="3" placeholder="LENGUAJE RECEPTIVO">{{isset($respuestas) ? $respuestas[13]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="leng_expresivo">LENGUAJE EXPRESIVO</label>
                          <textarea type="text" class="form-control" id="leng_expresivo" name="leng_expresivo" cols="30" rows="3" placeholder="LENGUAJE EXPRESIVO">{{isset($respuestas) ? $respuestas[14]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for="leng_corporal">LENGUAJE CORPORAL</label>
                          <textarea type="text" class="form-control" id="leng_corporal" name="leng_corporal" cols="30" rows="3" placeholder="LENGUAJE CORPORAL">{{isset($respuestas) ? $respuestas[15]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar('tab_3')"><i class="fa fa-arrow-left"></i> </button>
                  <button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar('tab_5', '#form_hab_c')"><i class="fa fa-arrow-right"></i> </button>
                </div>

                <div class="tab-pane disabled" id="tab_5">

                  <div class="content">
                    <form  id="form_hab_p" data-toggle="validator" role="form">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="atencion">ATENCIÓN</label>
                            <textarea type="text" class="form-control" id="atencion" name="atencion" cols="30" rows="3" placeholder="ATENCIÓN">{{isset($respuestas) ? $respuestas[16]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="cognicion">COGNICIÓN</label>
                            <textarea type="text" class="form-control" id="cognicion" name="cognicion" cols="30" rows="3" placeholder="COGNICIÓN">{{isset($respuestas) ? $respuestas[17]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="esq_corporal">ESQUEMA CORPORAL</label>
                            <textarea type="text" class="form-control" id="esq_corporal" name="esq_corporal" cols="30" rows="3" placeholder="ESQUEMA CORPORAL">{{isset($respuestas) ? $respuestas[18]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="temp_espacial">OR. TEMPORO-ESPACIAL</label>
                            <textarea type="text" class="form-control" id="temp_espacial" name="temp_espacial" cols="30" rows="3" placeholder="OR. TEMPORO-ESPACIAL">{{isset($respuestas) ? $respuestas[19]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="percepcion">PERCEPCIÓN</label>
                            <textarea type="text" class="form-control" id="percepcion" name="percepcion" cols="30" rows="3" placeholder="PERCEPCIÓN">{{isset($respuestas) ? $respuestas[20]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                          <div class="form-group">
                            <label for="atn_conjunta">ATENCIÓN CONJUNTA</label>
                            <textarea type="text" class="form-control" id="atn_conjunta" name="atn_conjunta" cols="30" rows="3" placeholder="ATENCIÓN CONJUNTA">{{isset($respuestas) ? $respuestas[21]->respuesta : ''}}</textarea>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar('tab_4')"><i class="fa fa-arrow-left"></i> </button>
                  <button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar('tab_6', '#form_hab_p')"><i class="fa fa-arrow-right"></i> </button>
                </div>

                <div class="tab-pane disabled" id="tab_6">

                  <div class="content">
                    <form  id="form_notas" data-toggle="validator" role="form">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                          <label for="notas">NOTAS</label>
                          <textarea type="text" class="form-control" id="notas" name="notas" cols="30" rows="3" placeholder="NOTAS">{{isset($respuestas) ? $respuestas[22]->respuesta : ''}}</textarea>
                        </div>
                      </div>
                    </form>
                  </div>
                  <button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar('tab_5')"><i class="fa fa-arrow-left"></i> </button>
                  <button type="button" class="btn btn-primary" id="btn_guardar" onclick="guardarProgramaTrabajo()">GUARDAR</button>
                </div>
              </div>

            </div>
          </div>

        </div>
        <div class="box-footer">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 pull-right">
            <div class="col-md-6 col-sm-6 col-md-6 col-lg-6">

              {{-- <button id="btn_editar" type="button" class="btn btn-block btn-primary">EDITAR</button> --}}
            </div>
            <div class="col-md-6 col-sm-6 col-md-6 col-lg-6">
              <a href="/canna/beneficiarios/{{$alumno_id}}" class="btn btn-block btn-danger">CANCELAR</a>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

@stop

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

  <script type="text/javascript">
  respuestas = {};
  ids = [];

  $(':text').on('propertychange input', function(e) {
    var ss = e.target.selectionStart;
    var se = e.target.selectionEnd;
    e.target.value = e.target.value.toUpperCase();
    e.target.selectionStart = ss;
    e.target.selectionEnd = se;
  });

  $('a[data-toggle="tab"]').on('click', function(){
      if ($(this).parent('li').hasClass('disabled')) {
          return false;
      }
  });
  function avanzar(tab, formulario) {
    if (formulario) {
      if($(formulario).valid()){
        respuestas[tab.substr(-1)-2] = $(formulario).serializeArray();
        $('.nav-tabs a[href="#' + tab +'"]').tab('show');
      }
    } else {
      $('.nav-tabs a[href="#' + tab +'"]').tab('show');
    }
  }

  function obtenerIdsRespuestas() {
    '{{isset($respuestas) ? $respuestas = $respuestas : $respuestas = 0}}'
    resp = <?php echo $respuestas; ?>;
    if(resp.length > 0){
      for (var i = 0; i < resp.length; i++) {
        ids.push(resp[i]['id']);
      }
    }
  }

  function guardarProgramaTrabajo() {
    respuestas['5'] = $("#form_notas").serializeArray();
    if('{{isset($respuestas)}}'){
      obtenerIdsRespuestas();
      respuestas['6'] = ids;
    }
    enviarDatosServer();
  }

  function enviarDatosServer(){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      block();
      var form = $("#form_programa_trabajo");
      $.ajax({
        type: $('#form_c_personal').attr('method'),
        url: $('#form_c_personal').attr('action'),
        data: respuestas,
        dataType: 'json',
        success: function(response) {
          unblock();
          swal({
            title: 'Programa de trabajo registrado',
            text: '',
            type: 'success',
            showConfirmButton: false,
            timer: 2000
          });
          window.location.href = "/canna/beneficiarios/{{$alumno_id}}";
        },
        error: function(response){
          unblock();
          swal({
            title: 'Programa de trabajo no registrado',
            text: 'Por favor reintente',
            type: 'error',
            showConfirmButton: true
          });
        }
      });
  }

  $("#form_c_personal").validate({
    rules : {
      vestido : {
        required : true
      },
      alimentacion : {
        required : true
      },
      banio : {
        required : true
      },
      higiene : {
        required : true
      }
    }
  });
  $("#form_hab_m_s").validate({
    rules : {
      mot_gruesa : {
        required : true
      },
      mot_fina : {
        required : true
      },
      imitacion : {
        required : true
      },
      sensorial : {
        required : true
      }
    }
  });
  $("#form_hab_s").validate({
    rules : {
      interaccion : {
        required : true
      },
      participacion : {
        required : true
      },
      juego : {
        required : true
      },
      conducta : {
        required : true
      },
      intereses : {
        required : true
      }
    }
  });
  $("#form_hab_c").validate({
    rules : {
      leng_receptivo : {
        required : true
      },
      leng_expresivo : {
        required : true
      },
      leng_corporal : {
        required : true
      }
    }
  });
  $("#form_hab_p").validate({
    rules : {
      atencion : {
        required : true
      },
      cognicion : {
        required : true
      },
      esq_corporal : {
        required : true
      },
      temp_espacial : {
        required : true
      },
      percepcion : {
        required : true
      },
      atn_conjunta : {
        required : true
      }
    }
  });
  </script>
@endpush
