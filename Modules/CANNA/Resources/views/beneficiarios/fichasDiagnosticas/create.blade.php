@extends('canna::layouts.master')

@push('head')
<style type="text/css">
textarea {
  resize: none;
  text-transform: uppercase;
}
</style>
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
  <li><a href="{{route('canna.beneficiarios.index')}}">beneficiarios</a></li>
  <li><a href="/canna/beneficiarios/{{$alumno_id}}">Seleccionado</a></li>
  <li class="active">Ficha diagnostica</li>
@endsection

@section('content')

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">
                @if($opcion == 'show')
                  {{'Ficha diagnostica'}}
                @elseif ($opcion == 'create')
                  {{'Nueva ficha diagnóstica'}}
                @endif
            </h3>
          </div>

          <form id="form_ficha_diagnostica" data-toggle="validator" role="form"
          action="{{isset($respuestas) ?
             URL::to('canna/beneficiarios') .'/'. $alumno_id .'/'. 'fichadiagnostica/' . 1 : route('beneficiarios.fichadiagnostica.store', $alumno_id)}}"
          method="{{ isset($respuestas) ? 'PUT' : 'POST' }}">
            <div class="box-body">

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                  <label for="lenguaje_comunicacion">LENGUAJE Y COMUNICACIÓN</label>
                  <textarea name="lenguaje_comunicacion" id="lenguaje_comunicacion" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[0]->respuesta : ''}}</textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                  <label for="social">SOCIAL</label>
                  <textarea name="social" id="social" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[1]->respuesta : ''}}</textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                    <label for="conducta">CONDUCTA</label>
                  <textarea name="conducta" id="conducta" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[2]->respuesta : ''}}</textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                  <label for="sensorial">SENSORIAL</label>
                  <textarea name="sensorial" id="sensorial" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[3]->respuesta : ''}}</textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                  <label for="autonomia">AUTONOMIA</label>
                  <textarea name="autonomia" id="autonomia" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[4]->respuesta : ''}}</textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                  <label for="cognitivo">COGNITIVO</label>
                  <textarea name="cognitivo" id="cognitivo" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[5]->respuesta : ''}}</textarea>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                <div class="form-group">
                  <label for="motricidad">MOTRICIDAD</label>
                  <textarea name="motricidad" id="motricidad" cols="30" rows="5" class="form-control">{{isset($respuestas) ? $respuestas[6]->respuesta : ''}}</textarea>
                </div>
              </div>

            </div>

            <div class="box-footer">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-3 pull-right">
                <div class="col-md-6 col-sm-6 col-md-6 col-lg-6">
                  <button id="btn_guardar" type="button" class="btn btn-block btn-primary"  onclick="guardarFichaDiagnostica()">GUARDAR</button>
                  <button id="btn_editar" type="button" class="btn btn-block btn-primary" onclick="editarFichaDiagnostica()">EDITAR</button>
                </div>
                <div class="col-md-6 col-sm-6 col-md-6 col-lg-6">
                  <a href="/canna/beneficiarios/{{$alumno_id}}" id="btn_cancelar" class="btn btn-block btn-danger">CANCELAR</a>
                </div>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </section>

@stop

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>


  <script type="text/javascript">

  $(document).ready(function () {

    $(':text').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });

    if('{{isset($respuestas)}}'){
      console.log('hay respuestas');
      textareas = document.getElementsByTagName('textarea');
      for (var i = 0; i < textareas.length; i++) {
        textareas[i].disabled = true;
      }
      $("#btn_editar").attr('style','display:inline');
      $("#btn_guardar").attr('style','display:none');
    }
    else {
      $("#btn_editar").attr('style','display:none');
      $("#btn_guardar").attr('style','display:inline');
    }
  })

  function guardarFichaDiagnostica(){
    enviarDatosServer();
  }

  function obtenerDatos() {
    '{{isset($respuestas) ? $respuestas = $respuestas : $respuestas = 0}}'
    resp = <?php echo $respuestas; ?>;
    if(resp.length > 0){
      ids = [];
      for (var i = 0; i < resp.length; i++) {
        ids.push(resp[i]['id']);
      }
      respuesta = {
        ids : ids,
        respuestas : $("#form_ficha_diagnostica").serializeArray()
      };
      return respuesta;
    }
    else {
      return $("#form_ficha_diagnostica").serialize();
    }
  }

  function editarFichaDiagnostica() {
    if($("#btn_editar").text() == 'EDITAR'){
      textareas = document.getElementsByTagName('textarea');
      for (var i = 0; i < textareas.length; i++) {
        textareas[i].disabled = false;
      }
      $("#btn_editar").text('ACEPTAR');
    }
    else {
      enviarDatosServer();
    }
  }

  function enviarDatosServer(){
    if($("#form_ficha_diagnostica").valid()){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      block();
      var form = $("#form_ficha_diagnostica");
      $.ajax({
        type: $('#form_ficha_diagnostica').attr('method'),
        url: $('#form_ficha_diagnostica').attr('action'),
        data: obtenerDatos(),
        dataType: 'json',
        success: function(response) {
          unblock();
          swal({
            title: 'Ficha diagnóstica registrada',
            text: '',
            type: 'success',
            showConfirmButton: false,
            timer: 2000
          });
          window.location.href = "/canna/beneficiarios/{{$alumno_id}}";
        },
        error: function(response){
          unblock();
          swal({
            title: 'Ficha diagnóstica no registrada',
            text: 'Por favor reintente',
            type: 'error',
            showConfirmButton: true
          });
        }
      });
    }
  }

  $("#form_ficha_diagnostica").validate({
    rules : {
      lenguaje_comunicacion : {
        required : true
      },
      social : {
        required : true
      },
      conducta : {
        required : true
      },
      sensorial : {
        required : true
      },
      autonomia : {
        required : true
      },
      cognitivo : {
        required : true
      },
      motricidad : {
        required : true
      }
    }
  });

  </script>
@endpush
