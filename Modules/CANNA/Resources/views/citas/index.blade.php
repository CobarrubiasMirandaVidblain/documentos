@extends('canna::layouts.master')

@push('head')
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
  <link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
  <link href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" type="text/css" rel="stylesheet">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" rel="stylesheet">

  <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
  <style type="text/css">
  textarea {
    resize: none;
    text-transform: uppercase;
  }

  option{
    color: white;
  }



</style>
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
  <li class="active">Calendario</li>
@endsection

@section('content')
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Calendario</h3>
        </div>
        <div class="box-body">
          <div id="calendar"></div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal-cita" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

      </div>
    </div>
  </div>

</section>

@stop

@push('body')
  <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
  integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
  crossorigin="anonymous"></script>

  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<!-- fullCalendar -->
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

  <script src="../bower_components/moment/moment.js"></script>
  <script src="../bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
  <script src='../bower_components/fullcalendar/dist/locale-all.js'></script>

  <script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>

  <script src="/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>

  <script type="text/javascript" src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>

  <script type="text/javascript" src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
var dia='';
eventos = [];
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
$(document).ready( function () {

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  /* initialize the calendar
   -----------------------------------------------------------------*/
  $('#calendar').fullCalendar({
    header    : {
      left  : 'prev,next today',
      center: 'title',
      right : 'month,agendaWeek,agendaDay'
    },
    buttonText: {
      today: 'actual',
      month: 'mes',
      week : 'semana',
      day  : 'dia'
    },

    //funciones agregadas por mi

    events: {
      url: "{{route('citas.muestraCitasMes', 1)}}",
      cache:true,
      type:'get',
      data: getInicioFinMes(),
      error: function(){
        swal({
          title: '¡No se cargo la agenda!',
          text: 'Se genero un problema',
          type: 'error'
        });
      }
    },
    eventDragStop: function () {
      $('#calendar').fullCalendar('refetchEvents');
    },

    locale : 'es',
    weekends : false,
    navLinks : true,
    selectable : false,
    selectHelper : false,
    select : function(start, end){
      var url = "{{route('canna.citas.create')}}";
      cargarModal(url);
    },
    eventClick : function (event, element) {
      var url = "{{URL::to('canna/citas')}}" + "/" +  event.id;
      cargarModal(url);
    },
    eventLimit : true,
    dayClick : function (date, jsEvent, view) {
      var actual = new Date();
      actual = moment(actual).format('YYYY/MM/DD');
      dia = date.format();
      dia = moment(dia).format('YYYY/MM/DD');
      if(dia >= actual){
        var url = "{{route('canna.citas.create')}}";
        cargarModal(url);
      }
      else {
        swal({
          title: '¡No se puede generar citas en dias pasados',
          type: 'error',
          timer: 3000,
          toast : true,
          position : 'top-end',
          showConfirmButton: false
        });
      }
    }
  });
});

function guardaCita() {
  if ($("#form_cita").valid()) {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    block();
    $.ajax({
      type: $('#form_cita').attr('method'),
      url: $('#form_cita').attr('action'),
      data: obtenDatos(),
      dataType: 'json',
      success: function(data) {
        unblock();
        if(data[0].code === 409){
          swal({
            title: '¡No se registro la cita!',
            text: 'por favor revice los datos',
            type: 'error'
          });
        }else {
          swal({
            type: 'success',
            title: 'Cita registrada',
            text: '',
            showConfirmButton: false,
            timer: 2000
          });
          $("#beneficiario").select2("val", " ");
          $("#observaciones").val('');
          $("#modal-cita").modal('hide');
          dia='';
          $('#calendar').fullCalendar('refetchEvents');
        }
      },
      error : function(data){
        unblock();
        swal({
          title: '¡No se registro la cita!',
          text: 'Se genero un problema',
          type: 'error'
        });
      }
    });
  }
}

function eliminaCita(id) {
  swal({
    title: '¿Está seguro de eliminar la cita?',
    text: '¡La cita se eliminará de forma permanente!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, eliminar',
    cancelButtonText: 'No, regresar',
    reverseButtons: true,
    allowEscapeKey: false,
    allowOutsideClick: false
  }).then((result) => {
    if (result.value) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      block();
      $.ajax({
        type: "DELETE",
        url: "{{ URL::to('canna/citas') }}" + '/' + id,
        success: function(data) {
          unblock();
          $("#beneficiario").select2("val", " ");
          $("#observaciones").val('');
          $("#modal-cita").modal('hide');
          dia='';
          swal({
            title: '¡Eliminada!',
            text: 'La cita ha sido eliminada.',
            type: 'success',
            showConfirmButton: false,
            timer: 3000
          });
          $('#calendar').fullCalendar('refetchEvents');

        },
        error : function(data){
          swal({
            title: '¡Cita no eliminada!',
            text: 'por favor reintente',
            type: 'error',
            showConfirmButton: true
          });
          unblock();
        }
      });
    }
  });

}

function getInicioFinMes() {
  if($("#calendar").fullCalendar('getCalendar') === undefined){
    return {};
  }
  var calendar = $('#calendar').fullCalendar('getCalendar');
  var view = calendar.view;
  Date.prototype.toString = function() { return this.getFullYear() + "-" + (this.getMonth()+1) + "-" + this.getDate(); }
  dias = {
    inicio : view.start._d,
    fin : view.end._d
  };
  return dias;
}


function obtenCita(id) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    type: "GET",
    url: "{{ URL::to('canna/citas') }}" + '/' + id,
    success: function(data) {
      console.log(data);
    },
    error : function(data){
      console.log(data);
    }
  });
}

function cargarModal(url) {
  $.get(url,function (data){
    $('.modal-content').html(data.body);
    $("#modal-cita").modal("show");
    $('.timepicker').timepicker({
      showInputs: false
    });
    $(':text').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
    $('#fecha').datepicker({
      beforeShowDay: $.datepicker.noWeekends,
      autoclose: true,
      language: 'es',
      startDate: '0d'
    });

    $('#hora_fin').change(function(event) {
      hora_start = convertTime12to24($("#hora_inicio").val());
      hora_end = convertTime12to24($("#hora_fin").val());
      console.log($("#hora_fin").val() === '' ||hora_start >= hora_end);
      if ($("#hora_fin").val() === '' ||hora_start >= hora_end) {
        $("#hora_fin").val('');
        swal({
          title: '¡No se puede asignar una hora fin anterior a la inicial',
          type: 'error',
          timer: 3000,
          toast : true,
          position : 'top-end',
          showConfirmButton: false
        });
      }
    });
    $("#fecha").inputmask();
    $('#beneficiario').select2({
      language: 'es',
      ajax: {
        url: '{{ route('beneficiarioscanna.select') }}',
        dataType: 'JSON',
        type: 'GET',
        data: function(params) {
          return {
            search: params.term
          };
        },
        processResults: function(data, params) {
          params.page = params.page || 1;
          return {
            results: $.map(data, function(item) {
              return {
                id: item.id,
                text: item.nombre + ' ' + item.primer_apellido + ' ' + item.segundo_apellido,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    });
    $("#form_cita").validate({
      rules : {
        hora_inicio : {
          required : true
        },
        hora_fin : {
          required : true
        },
        beneficiario : {
          required : true
        },
        observaciones : {
          required : false,
          minlength : 3
        }
      }
    });
    $(".myToggle").each(function(){
      $(this).bootstrapToggle();
    })
  })
}

function obtenDatos() {
  cita = {
    fecha : dia,
    hora_inicio : convertTime12to24($("#hora_inicio").val()),
    hora_fin : convertTime12to24($("#hora_fin").val()),
    persona_id : $("#beneficiario").val(),
    color : $("#color").val(),
    observaciones : $("#observaciones").val().toUpperCase()
  };
  if($('#form_cita').attr('method') == 'PUT'){
    cita["asistencia"] = document.getElementById('asistencia').checked
    cita["fecha"] = $("#fecha").val();
  }
  return cita;
}

function convertTime12to24(time12h) {
  const [time, modifier] = time12h.split(' ');
  let [hours, minutes] = time.split(':');
  if (hours === '12') {
    hours = '00';
  }
  if (modifier === 'PM') {
    hours = parseInt(hours, 10) + 12;
  }
  return hours + ':' + minutes;
}

</script>
@endpush
