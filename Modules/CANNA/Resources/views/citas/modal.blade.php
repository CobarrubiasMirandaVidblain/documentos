<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">{{isset($cita) ? 'Cita' : 'Nueva cita'}}</h4>
</div>
<form id="form_cita" data-toggle="validator" role="form"
action="{{isset($cita) ? route('canna.citas.update',$cita->id) : route('canna.citas.store')}}"
method="{{isset($cita) ? 'PUT' : 'POST'}}">
  <div class="modal-body">
    <div class="box-body">

      @isset($cita)
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
              <label>FECHA :</label>
              <div class="input-group date">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" @if ($mayor == 0)
                    disabled
                @endif class="form-control pull-right" name="fecha" id="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{$cita->date}}">
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-9 col-md-12 col-lg-6 pull-right">
            <div class="form-group pull-right">
              <label for="color">ASISTENCIA</label>
              <input id="asistencia" @if ($cita->asistencia == 1)
                checked
              @endif @if ($mayor == 0)
                  disabled
              @endif name="asistencia" data-toggle="toggle" data-on="Si" data-off="No" data-onstyle="success" data-offstyle="danger" data-size="small" data-width="84" type="checkbox" class="myToggle">
            </div>
          </div>
        </div>

      @endisset

      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-6">
          <div class="bootstrap-timepicker">
            <div class="form-group">
              <label>INICIO</label>
              <div class="input-group">
                <input type="text" @if (isset($mayor) && $mayor == 0)
                    disabled
                @endif class="form-control timepicker" id="hora_inicio" name="hora_inicio" value="{{$cita->start or ''}}" autocomplete="off">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-6">
          <div class="bootstrap-timepicker">
            <div class="form-group">
              <label>FIN</label>
              <div class="input-group">
                <input type="text" @if (isset($mayor) && $mayor == 0)
                    disabled
                @endif class="form-control timepicker" id="hora_fin" name="hora_fin" value="{{$cita->end or ''}}" autocomplete="off">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="beneficiario">BENEFICIARIO</label>
            <select id="beneficiario" @if (isset($mayor) && $mayor == 0)
                disabled
            @endif name="beneficiario" class="form-control select2" style="width: 100%;">
              @isset($cita)
                <option value="{{ $cita->persona_id }}" selected>{{$cita->title}}</option>
              @endisset
            </select>
          </div>
        </div>
      </div>

      {{-- <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-4">
          <ul class="select">
            <li>
        			<input class="select_close" type="radio" name="awesomeness" id="awesomeness-close" value=""/>
        			<span class="select_label select_label-placeholder">SELECCIONE UNO</span>
        		</li>

            <li class="select_items">
        			<input class="select_expand" type="radio" name="awesomeness" id="awesomeness-opener"/>
        			<label class="select_closeLabel" for="awesomeness-close"></label>

              <ul class="select_options">

                <li class="select_option">
        					<input class="select_input" type="radio" name="awesomeness" id="awesomeness-azul"/>
        					<label class="select_label" style="background-color : rgba(#3671be, 1);" for="awesomeness-ridiculous">AZUL</label>
        				</li>

                <li class="select_option">
        					<input class="select_input" type="radio" name="awesomeness" id="awesomeness-verde"/>
        					<label class="select_label" style="background-color : rgba(#6abf10, 1);" for="awesomeness-ridiculous">VERDE</label>
        				</li>

                <li class="select_option">
        					<input class="select_input" type="radio" name="awesomeness" id="awesomeness-amarillo"/>
        					<label class="select_label" style="background-color : rgba(#e0de19, 1);" for="awesomeness-ridiculous">AMARILLO</label>
        				</li>

                <li class="select_option">
        					<input class="select_input" type="radio" name="awesomeness" id="awesomeness-rosa"/>
        					<label class="select_label" style="background-color : rgba(#ec5394, 1);" for="awesomeness-ridiculous">ROSA</label>
        				</li>

                <li class="select_option">
        					<input class="select_input" type="radio" name="awesomeness" id="awesomeness-morado"/>
        					<label class="select_label" style="background-color : rgba(#8d239e, 1);" for="awesomeness-ridiculous">MORADO</label>
        				</li>

              </ul>

              <label class="select_expandLabel" for="awesomeness-opener"></label>
            </li>
          </ul>
        </div>
      </div> --}}

      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-4">
          <div class="form-group">
            <label for="color">COLOR</label>
            <div class="select-style">
              <select class="form-control" @if (isset($mayor) && $mayor == 0)
                disabled
              @endif id="color" name="color">
              <option  @if (isset($cita) && $cita->color == '#3671be')
                selected
              @endif value="#3671be" style="background-color : #3671be;" selected>AZUL</option>
              <option @if (isset($cita) && $cita->color == '#6abf10')
                selected
              @endif value="#6abf10" style="background-color : #6abf10;">VERDE</option>
              <option @if (isset($cita) && $cita->color == '#e0de19')
                selected
              @endif  value="#e0de19" style="background-color : #e0de19;">AMARILLO</option>
              <option @if (isset($cita) && $cita->color == '#ec5394')
                selected
              @endif value="#ec5394" style="background-color : #ec5394;">ROSA</option>
              <option @if (isset($cita) && $cita->color == '#ff0000')
                selected
              @endif value="#ff0000" style="background-color : #ff0000;">ROJO</option>
              <option @if (isset($cita) && $cita->color == '#8d239e')
                selected
              @endif value="#8d239e" style="background-color : #8d239e;">MORADO</option>
            </select>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-12">
          <div class="form-group">
            <label>OBSERVACIONES</label>
            <textarea class="form-control" @if (isset($mayor) && $mayor == 0)
                disabled
            @endif id="observaciones" name="observaciones" rows="3" placeholder="OBSERVACIONES" >{{$cita->observaciones or ''}}</textarea>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
    @isset($cita)
      <button type="button" @if (isset($mayor) && $mayor == 0)
          style="display: none;"
      @endif class="btn btn-danger" onclick="eliminaCita('{{$cita->id}}')">Cancelar</button>
    @endisset
    <button type="button" @if (isset($mayor) && $mayor == 0)
        style="display: none;"
    @endif class="btn btn-primary" onclick="guardaCita()">Guardar</button>
  </div>
</form>

{{-- 0 : fecha anterior al dia $fecha_actual
     1 : fecha atual o futura--}}
