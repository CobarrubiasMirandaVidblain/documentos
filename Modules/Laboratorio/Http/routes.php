<?php

Route::group(['middleware' => 'web', 'prefix' => 'laboratorio', 'namespace' => 'Modules\Laboratorio\Http\Controllers'], function() {
    Route::get('/', 'PeticionController@index');
  	Route::resource('peticiones', 'PeticionController',['as'=>'laboratorio','only'=>['index','show','update','destroy']]);   //Peticiones
  	Route::resource('programas', 'ProgramaController',['as'=>'laboratorio']);                                                //Programas
  	Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'laboratorio' ]);                                //Beneficiarios
});
