@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@if(auth()->check())
  @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('content-title', 'Control Interno')

@section('breadcrumbs')
  <ol class="breadcrumb">
    {{-- <li><a href="{{ route('atnciudadana.home') }}"><i class="fa fa-dashboard"></i>AtnCiudadana</a></li> --}}
    @yield('li-breadcrumbs')
  </ol>
@endsection

@section('sidebar-menu')


<ul class="sidebar-menu" data-widget="tree">
	<li><a href="{{url('/')}}">Intranet DIF</a></li>

	<li class="header active">Control Interno</li>

  <li><a href="{{url('controlinterno')}}">Home</a></li>

	<li class="header active">Área</li>

  @foreach ($menus as $key => $item)
		@include('controlinterno::layouts.menu-item', ['item' => $item])
	@endforeach

  @if(auth()->user()->hasRolesModulo(['ADMINISTRADOR'], 8))
	<li class="header">Catalogos</li>
    <li {{ ($ruta === 'atnciudadana.programas.index') ? 'class=active' : '' }}>
        <a href="{{url('controlinterno/docs')}}">
            <i class="fa fa-file-text-o"></i> <span>Documentos</span>
            <span class="pull-right-container"></span>
        </a>
    </li>
    <li {{ ($ruta === 'atnciudadana.programas.index') ? 'class=active' : '' }}>
        <a href="{{url('controlinterno/areausuario')}}">
            <i class="fa fa-file-text-o"></i> <span>Area - Usuario</span>
            <span class="pull-right-container"></span>
        </a>
    </li>
  @endif
  </ul>
@endsection

@section('styles')
  <!-- CSS Librerias -->
  @include('atnciudadana::layouts.links')
  <!-- CSS Propios -->
  <style type="text/css">
    .row {
      margin-right: 0px;
      margin-left: 0px;
    }

    input {
      text-transform: uppercase;
    }

    .skin-blue .sidebar-menu > li.active > a.home {
      border-left-color: transparent !important;
      color: #b8c7ce !important;
    }
    .text-dots{
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  </style>
@stop

@section('scripts')
@include('atnciudadana::layouts.script')
<script src="{{asset('js/vue.js')}}"></script>
<script src="{{asset('js/vee-validate.js')}}"></script>
<script src="{{asset('js/axios.min.js')}}"></script>
<script src="{{asset('reportes/jquery.min.js')}}"></script>
<script src="{{asset('reportes/bootstrap.min.js')}}"></script>
<script src="{{asset('bower_components/select2/select2/select2.min.js')}}"></script>
<script src="{{asset('bower_components/select2/select2/bootstrap_select.min.js')}}"></script>
<script src="{{asset('js/sweetalert2.all.js')}}"></script>
@stop

@push('head')
  @yield('myCss')
@endpush

@push('body')
  @yield('myScripts')
@endpush