@if ($item['submenu'] == [])
    <li>
        <a href="{{ url('/controlinterno/docs/' . $item['id']) }}">{{ $item['alias'] }} </a>
    </li>
@else
    <li class="treeview">
        <a href="#" class="text-dots">{{ $item['alias'] }} <span class="caret"></span></a>
        <ul class="treeview-menu">
            <li>
                <a href="{{url('/controlinterno/docs/' . $item['id'])}}"><i class="fa fa-home"></i>Home</a>
            </li>
            @foreach ($item['submenu'] as $submenu)
                @if ($submenu['submenu'] == [])
                    <!-- <li><a href="{{ url('menu',['id' => $submenu['id'], 'slug' => $submenu['slug']]) }}">{{ $submenu['alias'] }} </a></li> -->
                    <li><a href="{{ url('/controlinterno/docs/' . $submenu['id']) }}" class="text-dots">{{ $submenu['alias'] }} </a></li>
                @else
                    @include('controlinterno::layouts.menu-item', [ 'item' => $submenu ])
                @endif
            @endforeach
        </ul>
    </li>
@endif