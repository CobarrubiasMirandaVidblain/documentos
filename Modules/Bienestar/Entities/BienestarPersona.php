<?php

namespace Modules\Bienestar\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Bienestar\Entities\Base;

class BienestarPersona extends Base {
    protected $table = 'bienestarpersonas';

    protected $fillable = [];
}