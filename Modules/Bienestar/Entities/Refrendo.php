<?php

namespace Modules\Bienestar\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Bienestar\Entities\Base;

class Refrendo extends Base {
    protected $table = 'bien_refrendos';
    
    protected $fillable = ['anio_programa_id', 'estado_programa_id', 'usuario_id'];

    protected $hidden = [];
}