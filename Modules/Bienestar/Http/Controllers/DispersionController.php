<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Archivo;

use App\Models\BienestarDispersion;
use App\Models\ListaDispersion;
use App\Models\PersonasPrograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use App\Models\Persona;
use Illuminate\Support\Facades\Storage;
use Modules\Bienestar\Http\Requests\DispersionRequest;

use App\DataTables\Bienestar\Dispersiones;
use App\DataTables\Bienestar\DispersionesDataTable;
use App\DataTables\Bienestar\BeneficiariosDataTable;

class DispersionController extends Controller {
    use Bienestar;

    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(DispersionesDataTable $datatables) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);

        return $datatables
        ->render('bienestar::dispersiones.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO']);
        
        $ejercicios = Ejercicio::all();
        return view('bienestar::dispersiones.create_edit', ['ejercicios' => $ejercicios]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(DispersionRequest $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $datos = $request->all();

                $datos['importe'] = 1000;

                $datos['fecha'] = date('Y-m-d h:i:s');

                $datos['status'] = 'CREADA';

                $datos['usuario_id'] = auth()->user()->id;

                $dispersion = ListaDispersion::create($datos);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'listas_dispersiones',
                    'registro' => $dispersion->id . '',
                    'campos' => json_encode($dispersion) . '',
                    'metodo' => request()->method()
                ]);

                DB::commit();
                return response()->json(['success' => true, 'code' => 200, 'id' => $dispersion->id, 'estatus' => 'nuevo'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();

                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, Dispersiones $detalles, BeneficiariosDataTable $listado) {
        //$this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);
        /*$dispersion = ListaDispersion::findOrFail($id);

        $bimestre = $dispersion->bimestre;

        $lista_id = $dispersion->id;
        
        $bimestre_personas = PersonasPrograma::
        
        leftJoin('bienestardispersiones', 'bienestardispersiones.personas_programa_id', '=', 'personas_programas.id')
        
        ->leftJoin('listas_dispersiones','listas_dispersiones.id','=','bienestardispersiones.lista_id')
        
        ->leftJoin('cat_ejercicios', 'listas_dispersiones.ejercicio_id', '=', 'cat_ejercicios.id')
        
        ->where([
            [
                'anios_programa_id', 2
            ],
            [
                'anio', 2018
            ],
            [
                'bimestre', $bimestre
            ]
        ])
        ->select(['personas_programas.id']);

        return ListaDispersion::

        join('bienestardispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->leftJoin('personas_programas', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')

        ->where('anios_programa_id', 2)

        //->whereNull('personas_programas.deleted_at')

        ->where(function($query) use ($bimestre, $lista_id, $bimestre_personas) {
            $query
            ->whereNull('bienestardispersiones.lista_id')
            ->orWhere('bienestardispersiones.lista_id', $lista_id)
            ->orWhere(function($query) use ($bimestre, $bimestre_personas) {
                $query
                ->where('listas_dispersiones.bimestre', '!=', $bimestre)
                ->whereNotIn('personas_programas.id', $bimestre_personas)
                ;
            })
            ->orWhere(function($query) use ($bimestre) {
                $query
                ->where('bienestardispersiones.pagado', 2)
                ->where('listas_dispersiones.status', 'FINALIZADA')
                ;
            })
            ;
        })
        ->distinct('personas_programas.id')
        ->get()
        ;*/

        $dispersion = ListaDispersion::findOrFail($id);
        //mostrar la dispersion, no editable
        if($dispersion->status === 'EN REVISION BIENESTAR' || $dispersion->status === 'ENVIADA' || $dispersion->status === 'EN REVISION FINANCIERO' || $dispersion->status === 'CONFIRMADA' || $dispersion->status === 'RETENIDA' || $dispersion->status === 'FINALIZADA') {

            $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);

            if($dispersion->status === 'ENVIADA') {
                if(Auth::user()->hasRolesModulo(['FINANCIERO VALIDADOR'], 2)) {
                    $dispersion->update(['status' => 'EN REVISION FINANCIERO']);
                }
            }

            $archivos = Archivo::
            where('modulo_id', '=', 2)
            ->where('tablabd_id', '=', 1)
            ->where('registro_num', '=', $dispersion->id)
            ->get()
            ;
            
            return $detalles->with(['tipo' => 'detalles', 'lista' => $dispersion])->render('bienestar::dispersiones.show', ['dispersion' => $dispersion, 'archivos' => $archivos]);
        }

        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO']);

        //continuar la dispersion
        if($dispersion->status === 'CREADA') {
            if(Auth::user()->hasRolesModulo(['COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO'], 2)) {
                $dispersion->update(['status' => 'EN PROCESO']);
            }
        }

        $aniosPrograma = AniosPrograma::leftjoin('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->leftjoin('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
        ->where('programas.nombre', 'BIENESTAR')
        ->where('cat_ejercicios.anio', $dispersion->ejercicio->anio)->first(['anios_programas.id'])->id;

        $enlista = BienestarDispersion::where('lista_id', $dispersion->id)->count();

        $entotal = 1;/*PersonasPrograma::
        join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->where('bienestarpersonas.ejercicio_id', '=', 8)
        ->generarDispersion($dispersion->ejercicio->anio,$dispersion->bimestre,$aniosPrograma,$dispersion->id)
        ->count();*/

        return $listado->with(['tipo'=>'dispersion','lista'=>$dispersion])->render('bienestar::dispersiones.complete',['dispersion'=>$dispersion,'select_all'=>$enlista==$entotal]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO']);

        $dispersion = ListaDispersion::findOrFail($id);
        //if($dispersion->status=='Enviada'||$dispersion->status=='Finaliza')
            //return 'Esta lista no puede ser editada';abort(403, 'No cuentas con los permisos necesarios.');
        if($dispersion->status === 'ENVIADA' || $dispersion->status === 'CONFIRMADA' || $dispersion->status === 'CANCELADA' || $dispersion->status === 'FINALIZADA') {
            abort(403, 'No cuentas con los permisos necesarios.');
        }

        $ejercicios = Ejercicio::all();
        return view('bienestar::dispersiones.create_edit', ['dispersion' => $dispersion, 'ejercicios' => $ejercicios]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO VALIDADOR']);

        if($request->ajax()) {
            // $pendientes = ListaDispersion::where('bimestre',$request->input('bimestre'))
            // ->where('ejercicio_id',$request->input('ejercicio_id'))
            // ->where('status','!=','Finalizada')->count();
            // if($pendientes > 0 )
            //     return response()->json(array('success' => false, 'mensaje' => 'Hay una lista sin terminar en este bimestre'));
            if($request->input('status') === 'ENVIADA') {
                if(BienestarDispersion::where('lista_id', $id)->count() <= 0) {
                    //return response()->json(array('success' => false, 'mensaje' =>'Seleccione almenos 1 beneficiario'));
                    return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . 'Seleccione al menos 1 beneficiario.'], 409);
                }
            }

            try {
                DB::beginTransaction();

                $dispersion = ListaDispersion::find($id);

                $dispersion->update($request->all());

                DB::commit();
                return response()->json(['success' => true, 'code' => 200, 'id' => $dispersion->id, 'estatus' => 'nuevo'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();

                ListaDispersion::destroy($id);

                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}