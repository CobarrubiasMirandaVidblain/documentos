<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Models\BienestarPersona;
use App\Models\AniosPrograma;
use App\Models\BienestarDispersion;
use App\Models\Motivo;
use App\Models\PersonasPrograma;
use App\Models\Region;
use App\Models\Distrito;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Yajra\Datatables\Datatables;

use League\Csv\Writer;

use View;

class BienestarController extends Controller {
    use Bienestar;
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);

        $regiones = Region::select(
            [
                'cat_regiones.id as id',
                DB::raw("CONCAT('REGIÓN: ', cat_regiones.nombre) AS name")
            ]
        )
        ->orderBy('cat_regiones.id', 'ASC')
        ->orderBy('cat_regiones.nombre', 'ASC')
        ->get();

        $padron = BienestarPersona::whereNotNull('posicion')
        ->whereNull('deleted_at')
        ->count();

        $padronRegion = BienestarPersona::select(
            [
                'cat_regiones.id as id',
                DB::raw("CONCAT('REGIÓN: ', cat_regiones.nombre) AS name"),
                DB::raw('count(*) as value')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->orderBy('cat_regiones.id', 'ASC')
        ->orderBy('cat_regiones.nombre', 'ASC')

        ->groupBy('cat_regiones.id')
        ->groupBy('cat_regiones.nombre')

        ->get();

        $generoRegion = BienestarPersona::select(
            [
                DB::raw("CONCAT('REGIÓN: ', CONCAT(cat_regiones.nombre, CONCAT('<br> GÉNERO: ', IF(personas.genero = 'M', 'MASCULINO', 'FEMENINO')))) AS name"),
                'personas.genero as genero',
                'cat_regiones.nombre as region',
                DB::raw('count(*) as value')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->orderBy('cat_regiones.nombre', 'ASC')
        ->orderBy('personas.genero', 'ASC')
        
        ->groupBy('personas.genero')
        ->groupBy('cat_regiones.nombre')
        
        ->get();

        $totalGenero = BienestarPersona::select(
            [
                'personas.genero as genero',
                DB::raw('count(*) as value')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->orderBy('cat_regiones.nombre', 'ASC')
        ->orderBy('personas.genero', 'ASC')
        
        ->groupBy('personas.genero')
        
        ->get();//return $totalGenero;

        $discapacidades =
        DB::select('SELECT
        d.nombre general,
        cd.nombre especifica,
        SUM( cd.id ) total 
    FROM
        bienestarpersonas bp
        INNER JOIN cat_discapacidades cd ON bp.discapacidad_id = cd.id
        INNER JOIN cat_discapacidades d ON d.id = cd.padre_id 
    WHERE
        posicion IS NOT NULL 
        AND bp.ejercicio_id = 8 
        AND bp.deleted_at IS NULL 
        AND cd.deleted_at IS NULL 
    GROUP BY
        cd.id,
        cd.nombre,
        d.nombre 
    ORDER BY
        total DESC');
        
        return view('bienestar::index', compact('regiones', 'padron', 'padronRegion', 'generoRegion', 'discapacidades', 'totalGenero'));
    }

    public function distritos($distrito_id, Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);

        $region = Region::where('cat_regiones.id', '=', $distrito_id)
        ->first();

        $distritos = Distrito::select(
            [
                'cat_distritos.id as id',
                DB::raw("CONCAT('DISTRITO: ', cat_distritos.nombre) AS name")
            ]
        )
        ->where('cat_distritos.region_id', '=', $distrito_id)
        ->orderBy('cat_distritos.id', 'ASC')
        ->orderBy('cat_distritos.nombre', 'ASC')
        ->get();

        $padronDistrito = BienestarPersona::select(
            [
                'cat_distritos.id as id',
                DB::raw("CONCAT('DISTRITO: ', cat_distritos.nombre) AS name"),
                DB::raw('count(*) as value')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->where('cat_regiones.id', '=', $distrito_id)

        ->orderBy('cat_distritos.id', 'ASC')
        ->orderBy('cat_distritos.nombre', 'ASC')

        ->groupBy('cat_distritos.id')
        ->groupBy('cat_distritos.nombre')

        ->get();

        $generoDistrito = BienestarPersona::select(
            [
                DB::raw("CONCAT('DISTRITO: ', CONCAT(cat_distritos.nombre, CONCAT('<br> GÉNERO: ', IF(personas.genero = 'M', 'MASCULINO', 'FEMENINO')))) AS name"),
                'personas.genero as genero',
                'cat_distritos.nombre as distrito',
                DB::raw('count(*) as value')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->where('cat_regiones.id', '=', $distrito_id)

        ->orderBy('cat_distritos.id', 'ASC')
        ->orderBy('cat_distritos.nombre', 'ASC')
        ->orderBy('personas.genero', 'ASC')
        
        ->groupBy('personas.genero')
        ->groupBy('cat_distritos.nombre')
        
        ->get();

        return view('bienestar::distritos', compact('region', 'distritos', 'padronDistrito', 'generoDistrito', 'distrito_id'));
    }

    public function municipios($municipio_id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);

        $distrito = Distrito::where('cat_distritos.id', '=', $municipio_id)
        ->first();

        return view('bienestar::municipios', compact('distrito', 'municipio_id'));
    }

    public function padronMunicipioData($id) {
        $padronMunicipio = BienestarPersona::select(
            [
                'cat_municipios.id as id',
                'cat_municipios.nombre as municipio',
                DB::raw('count(*) as total')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->where('cat_distritos.id', '=', $id)

        ->orderBy('cat_municipios.id', 'ASC')
        ->orderBy('cat_municipios.nombre', 'ASC')

        ->groupBy('cat_municipios.id')
        ->groupBy('cat_municipios.nombre')

        ->get();

        return Datatables::of($padronMunicipio)
        ->editColumn('municipio', function($municipio) {
            return "<a href='/bienestar/bienestarpersonas/export?id=$municipio->id'>$municipio->municipio</a>";
        })
        ->rawColumns(['municipio'])
        ->make(true);
    }

    public function generoMunicipioData($id) {
        $generoMunicipio = BienestarPersona::select(
            [
                'cat_municipios.id as id',
                'cat_municipios.nombre as municipio',
                'personas.genero as genero',
                DB::raw('count(*) as total')
            ]
        )

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        
        ->whereNotNull('bienestarpersonas.posicion')
        ->whereNull('bienestarpersonas.deleted_at')

        ->where('cat_distritos.id', '=', $id)

        ->orderBy('cat_municipios.id', 'ASC')
        ->orderBy('cat_municipios.nombre', 'ASC')
        ->orderBy('personas.genero', 'ASC')

        ->groupBy('cat_municipios.id')
        ->groupBy('personas.genero')
        ->groupBy('cat_municipios.nombre')
        
        ->get();

        return Datatables::of($generoMunicipio)
        ->editColumn('municipio', function($municipio) {
            return "<a href='/bienestar/bienestarpersonas/export?id=$municipio->id&genero=$municipio->genero'>$municipio->municipio</a>";
        })
        ->rawColumns(['municipio'])
        ->make(true);
    }

    public function barras(Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR', 'ASESOR FINANCIERO']);
        
        $datos = Array();

        $labels = ['ENERO-FEBRERO', 'MARZO-ABRIL', 'MAYO-JUNIO', 'JULIO-AGOSTO', 'SEPTIEMBRE-OCTUBRE', 'NOVIEMBRE-DICIEMBRE'];
        
        $colores = ['rgb(255, 195, 0)', 'rgb(255, 51, 193)', 'rgb(245, 13, 37)', 'rgb(144, 51, 255)', 'rgb(51, 144, 255)', 'rgb(17, 120, 16)', 'rgb(51, 255, 57)', 'rgb(255, 87, 51)'];

        $regiones = Region::all();

        $datasets = Array();
        
        for($i = 0; $i < count($regiones); $i++) {
            $datasets[$i]['label'] = $regiones[$i]->nombre;
            $datasets[$i]['backgroundColor'] = str_replace(')', ',0.55)', $colores[$i]);
            $datasets[$i]['borderColor'] = $colores[$i];
            $datasets[$i]['borderWidth'] = 1;
            
            for($j = 1; $j <= 6; $j++) {
                $datasets[$i]['data'][$j] = BienestarDispersion::
                leftjoin('listas_dispersiones', 'listas_dispersiones.id', '=', 'bienestardispersiones.lista_id')
                ->join('personas_programas', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')
                ->join('personas', 'personas.id', '=', 'personas_programas.persona_id')
                ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
                ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
                ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
                ->where('listas_dispersiones.bimestre', $j)
                ->where('cat_regiones.nombre', $regiones[$i]->nombre)
                
                ->where('bienestardispersiones.pagado', $request->pagado)
                
                ->count();
            }
            
            $datasets[$i]['data'] = array_values($datasets[$i]['data']);
        }

        $datos[0] = $labels;

        $datos[1] = $datasets;

        return $datos;
    }

    public function export(Request $request) {
        $bienestarPersona = BienestarPersona::withTrashed()
        ->Beneficiarios()
        ->whereNull('bienestarpersonas.deleted_at')
        ->whereNull('personas_programas.deleted_at')
        ->select(
            [
                'bienestarpersonas.id as id',
                'personas_programas.id as personas_programa_id',
                'bienestarpersonas.posicion as posicion',
                'bienestarpersonas.foliounico as foliounico',

                'discapacidades.nombre as general',

                'cat_discapacidades.nombre as especifica',

                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',
                'personas.genero as genero',

                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',

                'tutor.nombre as tutor_nombre',
                'tutor.primer_apellido as tutor_primer_apellido',
                'tutor.segundo_apellido as tutor_segundo_apellido',
                'tutor.fecha_nacimiento as tutor_fecha_nacimiento',
                'tutor.curp as tutor_curp',
                'tutor.genero as tutor_genero',

                'cuentasbancos.num_cuenta as num_cuenta',

                'tutor_cat_regiones.nombre as tutor_region',
                'tutor_cat_distritos.nombre as tutor_distrito',
                'tutor_cat_municipios.nombre as tutor_municipio',
                'tutor_cat_localidades.nombre as tutor_localidad',
                
                'bienestarpersonas.deleted_at as deleted_at'
            ]
        );

        if($request->input('distrito_id')) {
            $bienestarPersona->where('cat_regiones.id', '=', $request->distrito_id);
        }

        if($request->input('municipio_id')) {
            $bienestarPersona->where('cat_distritos.id', '=', $request->municipio_id);
        }

        if($request->input('id')) {
            $bienestarPersona->where('cat_municipios.id', '=', $request->id);
            if($request->input('genero')) {
                $bienestarPersona->where('personas.genero', '=', $request->genero);
            }
        }
        
        $csvExporter = new \Laracsv\Export();
        
        $writer = $csvExporter->getCsv();
        
        $writer->setOutputBOM(Writer::BOM_UTF8);
        
        $csvExporter
        ->build($bienestarPersona->get(), ['foliounico', 'posicion', 'general', 'especifica', 'nombre', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'curp', 'genero', 'region', 'distrito', 'municipio', 'localidad', 'tutor_nombre', 'tutor_primer_apellido', 'tutor_segundo_apellido', 'tutor_fecha_nacimiento', 'tutor_curp', 'tutor_genero', 'tutor_region', 'tutor_distrito', 'tutor_municipio', 'tutor_localidad'])
        ->download();
    }
}