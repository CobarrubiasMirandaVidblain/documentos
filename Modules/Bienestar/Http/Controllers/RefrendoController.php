<?php

namespace Modules\Bienestar\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\Bienestar\Http\Controllers\BaseController;

use Modules\Bienestar\Http\DataTables\RefrendoDataTable;

use Modules\Bienestar\Entities\Refrendo;

use Modules\Bienestar\Entities\BienestarPersona;

class RefrendoController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(RefrendoDataTable $dataTable) {
        return $dataTable
        //->with(['modulo' => $this->modulo])
        ->render('bienestar::refrendo.index', ['modulo' => $this->modulo]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('bienestar::refrendo.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        return BienestarPersona::select(
            [
                'bienestarpersonas.id as id',
                'bienestarpersonas.foliounico as foliounico'
            ]
        )

        ->whereNotNull('bienestarpersonas.posicion')

        ->join('cat_ejercicios', 'bienestarpersonas.ejercicio_id', '=', 'cat_ejercicios.id')
        ->whereNull('cat_ejercicios.deleted_at')
        ->where('cat_ejercicios.anio', '=', '2018')
        
        ->leftJoin('bien_refrendos_personas', 'bienestarpersonas.id', '=', 'bien_refrendos_personas.bien_persona_id')
        ->whereNull('bien_refrendos_personas.deleted_at')
        ->whereNull('bien_refrendos_personas.id')

        ->join('personas', 'bienestarpersonas.persona_id', '=', 'personas.id')
        ->whereNull('personas.deleted_at')

        ->get();
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('bienestar::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('bienestar::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}