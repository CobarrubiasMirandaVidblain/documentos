<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Ejercicio;
use App\Models\Persona;
use App\Models\Tutor;
use App\Models\CuentasBancos;
use App\Models\BienestarPersona;
use App\Models\AniosPrograma;
use App\Models\StatusBienestarPersona;
use App\Models\PersonasPrograma;

use App\DataTables\PersonasDataTable;
use App\DataTables\Bienestar\TutoresDataTable;
use App\DataTables\Bienestar\SolicitantesDataTable;

use Modules\Bienestar\Http\Requests\SolicitanteRequest;

class SolicitanteController extends Controller {
    use Bienestar;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SolicitantesDataTable $dataTable) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'ASESOR FINANCIERO']);
        
        $ejercicio_id = Ejercicio::where('anio', $this->anio)->first()->id;
        
        return $dataTable
        ->with(['tipo' => 'normal', 'ejercicio_id' => $ejercicio_id])
        ->render('bienestar::solicitantes.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(PersonasDataTable $solicitantes, TutoresDataTable $tutores) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO', 'CAPTURISTA']);

        $solicitantes->with('tipo', 'bienestarsolicitantes');

        $tutores->with('tipo', 'bienestartutores');

        if(request()->get('table') === 'personas') {
            return $solicitantes->render('bienestar::solicitantes.create_edit', compact('solicitantes', 'tutores'));
        }
        else {
            return $tutores->render('bienestar::solicitantes.create_edit', compact('solicitantes', 'tutores'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(SolicitanteRequest $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO', 'CAPTURISTA']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $programa_actual = AniosPrograma::select('anios_programas.*')
                ->with('ejercicio')
                ->with('programa')
                ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
                ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
                ->where('programas.nombre', 'LIKE', $this->programa)
                ->where('cat_ejercicios.anio', '=', $this->anio)
                ->first();

                if(isset($programa_actual)) {
                    if(isset($programa_actual->programa)) {
                        if(isset($programa_actual->ejercicio)) {
                            $persona_id;

                            if($request->tutor === 'SI') {
                                $persona_id = $request->persona_id;
                            }
                            else {
                                $persona_id = $request->tutorado_id;
                            }

                            $datoscuentasbancos = $request->only(['banco_id', 'num_cuenta', 'num_tarjeta']);
                            $datoscuentasbancos['usuario_id'] = Auth::user()->id;

                            $datosbienestarpersona = $request->except(['tutorado_id', 'banco_id', 'num_cuenta', 'num_tarjeta', '_method']);
                            $datosbienestarpersona['usuario_id'] = Auth::user()->id;
                            $datosbienestarpersona['ejercicio_id'] = $programa_actual->ejercicio->id;

                            /*$tutor = Tutor::firstOrCreate(
                                [
                                    'tutorado_id' => $request->tutorado_id,
                                    'programa_id' => $programa_actual->programa->id,
                                    'activo' => 1
                                ],
                                [
                                    'persona_id' => $persona_id,
                                    'usuario_id' => Auth::user()->id
                                ]
                            );*/

                            $tutor = Tutor::create([
                                'tutorado_id' => $request->tutorado_id,
                                'programa_id' => $programa_actual->programa->id,
                                'activo' => 1,
                                'persona_id' => $persona_id,
                                'usuario_id' => Auth::user()->id
                            ]);

                            auth()->user()->bitacora(request(), [
                                'tabla' => 'cuentasbancos',
                                'registro' => $tutor->id . '',
                                'campos' => json_encode($tutor) . '',
                                'metodo' => request()->method()
                            ]);

                            $bienestarpersona = BienestarPersona::firstOrCreate(
                                [
                                    'persona_id' => $request->tutorado_id
                                ], $datosbienestarpersona
                            );

                            auth()->user()->bitacora(request(), [
                                'tabla' => 'cuentasbancos',
                                'registro' => $bienestarpersona->id . '',
                                'campos' => json_encode($bienestarpersona) . '',
                                'metodo' => request()->method()
                            ]);
                        }
                        else {
                            return response()->json(['errors' => array(['code' => 409, 'message' => 'El año no existe.'])], 409);
                        }
                    }
                    else {
                        return response()->json(['errors' => array(['code' => 409, 'message' => 'El programa no existe.'])], 409);
                    }
                }
                else {
                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El programa y el año no existe.'])], 409);
                }

                DB::commit();

                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request) {
        $solicitante = BienestarPersona::findOrFail($request->id);
        return view('bienestar::solicitantes.show', array('solicitante' => $solicitante));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request, PersonasDataTable $solicitantes, TutoresDataTable $tutores) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO']);

        $solicitantes->with('tipo', 'bienestarsolicitantes');

        $tutores->with('tipo', 'bienestartutores');

        $candidato = BienestarPersona::
        where('id', '=', $request->id)
        ->first();

        $tutor = Tutor::where('tutorado_id', $candidato->persona->id)->first();

        if(request()->get('table') === 'personas') {
            return $solicitantes->render('bienestar::solicitantes.create_edit', compact('solicitantes', 'tutores', 'candidato', 'tutor'));
        }
        else {
            return $tutores->render('bienestar::solicitantes.create_edit', compact('solicitantes', 'tutores', 'candidato', 'tutor'));
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $candidato = BienestarPersona::withTrashed()
                ->where('foliounico', '=', $request->foliounico)
                ->where('id', '!=', $request->old_candidato_id)
                ->where('ejercicio_id', '=', 8)
                ->first();

                if(isset($candidato)) {
                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El folio: ' . '<strong>' . $request->foliounico . '</strong>' . ' se encuentra asignado a otro candidato.'])], 409);
                }

                $programa_actual = AniosPrograma::select('anios_programas.*')
                ->with('ejercicio')
                ->with('programa')
                ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
                ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
                ->where('programas.nombre', 'LIKE', $this->programa)
                ->where('cat_ejercicios.anio', '=', $this->anio)
                ->first();

                if(isset($request->old_tutor_id)) {

                    $old_tutor = Tutor::find($request->old_tutor_id);

                    $old_tutor->update(['activo' => 0]);

                    $old_tutor->delete();

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'cuentasbancos',
                        'registro' => $old_tutor->id . '',
                        'campos' => json_encode($old_tutor) . '',
                        'metodo' => request()->method()
                    ]);
                }

                //$old_cuentasbancos_id = CuentasBancos::find($request->old_cuentasbancos_id);

                //$old_cuentasbancos_id->update(['activo' => 0]);

                //$old_cuentasbancos_id->delete();

                $persona_id;

                if($request->tutor === 'SI') {
                    $persona_id = $request->persona_id;
                }
                else {
                    $persona_id = $request->tutorado_id;
                }

                /*$tutor = Tutor::firstOrCreate(
                    [
                        'persona_id' => $persona_id,
                        'tutorado_id' => $request->tutorado_id,
                        'programa_id' => $old_tutor->programa_id,
                        'activo' => 1
                    ],
                    [
                        'usuario_id' => Auth::user()->id
                    ]
                );*/

                $tutor = Tutor::create([
                    'persona_id' => $persona_id,
                    'tutorado_id' => $request->tutorado_id,
                    'programa_id' => $programa_actual->programa->id,
                    'activo' => 1,
                    'usuario_id' => Auth::user()->id
                ]);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'cuentasbancos',
                    'registro' => $tutor->id . '',
                    'campos' => json_encode($tutor) . '',
                    'metodo' => request()->method()
                ]);

                /*$cuentasbancos = CuentasBancos::firstOrCreate(
                    [
                        'persona_id' => $persona_id,
                        'banco_id' => $request->banco_id,
                        'activo' => 1
                    ],
                    [
                        'num_cuenta' => $request->num_cuenta,
                        //'num_tarjeta' => $request->num_tarjeta,
                        'usuario_id' => Auth::user()->id
                    ]
                );*/

                $bienestarpersona = BienestarPersona::withTrashed()->where('id', $request->old_candidato_id)->first();

                $bienestarpersona->update(
                    [
                        'foliounico' => $request->foliounico,
                        'discapacidad_id' => $request->discapacidad_id,
                        'estadocivil_id' => $request->estadocivil_id,
                        'escolaridad_id' => $request->escolaridad_id,
                        'ocupacion_id' => $request->ocupacion_id,
                        'usuario_id' => Auth::user()->id
                    ]
                );

                auth()->user()->bitacora(request(), [
                    'tabla' => 'cuentasbancos',
                    'registro' => $bienestarpersona->id . '',
                    'campos' => json_encode($bienestarpersona) . '',
                    'metodo' => request()->method()
                ]);

                DB::commit();

                return response()->json(array('success' => true, 'id' => $bienestarpersona->id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function convertir_beneficiario(Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'OPERATIVO']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $bienestar_persona = BienestarPersona::find($request->id);
                
                if(!isset($bienestar_persona)) {
                    return response()->json(['success' => false, 'code' => 409, 'message' => 'El solicitante <strong>' . $bienestar_persona->persona->nombrecompleto() . '</strong> con folio <strong>' . $bienestar_persona->foliounico . '</strong> no se puede convertir en beneficiario porque se encuentra inactivo.'], 409);
                }

                if(isset($bienestar_persona->posicion)) {
                    return response()->json(['success' => false, 'code' => 409, 'message' => 'El solicitante <strong>' . $bienestar_persona->persona->nombrecompleto() . '</strong> con folio <strong>' . $bienestar_persona->foliounico . '</strong> no se puede convertir en beneficiario porque tiene la posición <strong> ' . $bienestar_persona->posicion . '</strong>.'], 409);
                }
                
                $posiciones_disponibles = PersonasPrograma::withTrashed()
                ->posicionesdisponibles($this->programa, $this->anio)
                ->get(['bienestarpersonas.posicion as posicion']);
                
                if(count($posiciones_disponibles) > 0) {
                    /*$tmp_bienestar_persona = BienestarPersona::where('posicion', '=', $posiciones_disponibles[0]['posicion'])->get();

                    if(isset($tmp_bienestar_persona[0])) {
                        return response()->json(['success' => false, 'code' => 409, 'message' => 'El solicitante <strong>' . $bienestar_persona->persona->nombrecompleto() . '</strong> con folio <strong>' . $bienestar_persona->foliounico . '</strong> no se puede convertir en beneficiario porque la posición <strong>' . $posiciones_disponibles[0]['posicion'] . '</strong> está en uso.'], 409);
                    }*/

                    $bienestar_persona->update(['posicion' => $posiciones_disponibles[0]['posicion'], 'ejercicio_id' => 8]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'bienestarpersonas',
                        'registro' => $bienestar_persona->id . '',
                        'campos' => json_encode($bienestar_persona) . '',
                        'metodo' => request()->method()
                    ]);
                    
                    $persona_programa = PersonasPrograma::firstOrCreate(['persona_id' => $bienestar_persona->persona_id, 'anios_programa_id' => 2], ['baja' => 0, 'usuario_id' => Auth::user()->id]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'personasprogramas',
                        'registro' => $persona_programa->id . '',
                        'campos' => json_encode($persona_programa) . '',
                        'metodo' => request()->method()
                    ]);
                }
                else {
                    $posicion = PersonasPrograma::withTrashed()
                    ->posicion($this->programa, $this->anio)
                    ->get(['bienestarpersonas.posicion as posicion'])->count() + 1;

                    $tmp_bienestar_persona = BienestarPersona::where('posicion', '=', $posicion)->get();

                    if(isset($tmp_bienestar_persona[0])) {
                        return response()->json(['success' => false, 'code' => 409, 'message' => 'El solicitante <strong>' . $bienestar_persona->persona->nombrecompleto() . '</strong> con folio <strong>' . $bienestar_persona->foliounico . '</strong> no se puede convertir en beneficiario porque la posición <strong>' . $posicion . '</strong> está en uso.'], 409);
                    }
                    
                    if($posicion <= $this->cupo) {
                        $bienestar_persona->update(['posicion' => $posicion, 'ejercicio_id' => 8]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'bienestarpersonas',
                            'registro' => $bienestar_persona->id . '',
                            'campos' => json_encode($bienestar_persona) . '',
                            'metodo' => request()->method()
                        ]);
                    
                        $persona_programa = PersonasPrograma::firstOrCreate(['persona_id' => $bienestar_persona->persona_id, 'anios_programa_id' => 2], ['baja' => 0, 'usuario_id' => Auth::user()->id]);

                        auth()->user()->bitacora(request(), [
                            'tabla' => 'personasprogramas',
                            'registro' => $persona_programa->id . '',
                            'campos' => json_encode($persona_programa) . '',
                            'metodo' => request()->method()
                        ]);
                    }
                    else {
                        return response()->json(['success' => false, 'code' => 409, 'message' => 'El solicitante <strong>' . $bienestar_persona->persona->nombrecompleto() . '</strong> con folio <strong>' . $bienestar_persona->foliounico . '</strong> no se puede convertir en beneficiario porque la matriz está llena.'], 409);
                    }
                }

                DB::commit();
                
                return response()->json(['success' => true, 'code' => 200, 'bienestar_persona' => $bienestar_persona, 'persona_programa' => $persona_programa, 'estatus' => 'nuevo'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();

                return response()->json(['success' => false, 'code' => 409, 'message' => $e->getMessage()], 409);
            }
        }
    }

    public function accesos(Request $request) {
        if($request->ajax()) {
            if(strtoupper($request->usuario) === strtoupper($this->usuario) && strtoupper($request->contrasenia) === strtoupper($this->contrasenia)) {
                return response()->json(['success' => array(['code' => 200])], 200);
            }
            else {
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . 'Las credenciales ingresadas son incorrectas.'])], 409);
            }
        }
    }
    
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();

                DB::commit();

                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }
}