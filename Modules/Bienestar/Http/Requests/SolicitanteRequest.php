<?php

namespace Modules\Bienestar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SolicitanteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'foliounico' => [
                'required',
                'string',
                'max:255',
                Rule::unique('bienestarpersonas')->where(function($query) {
                    return $query->where('foliounico', $this->foliounico);
                })
            ]/*,
            'tutorado_id' => [
                'required',
                Rule::unique('bienestarpersonas')->where(function($query) {
                    return $query->where('persona_id', '=', $this->tutorado_id);
                })
            ]*/
        ];
    }

    public function messages() {
        return [
            //'tutorado_id.unique' => 'Ya se encuentra una persona registrada con el mismo nombre y la misma fecha de nacimiento.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
