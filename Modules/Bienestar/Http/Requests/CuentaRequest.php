<?php

namespace Modules\Bienestar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CuentaRequest extends FormRequest {
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'banco_id' => [
                'required'
            ],
            'num_cuenta' => [
                'required',
                'max:11',                
                Rule::unique('cuentasbancos')->where(function($query) {
                    return $query->whereNotNull('num_cuenta')
                    ->where('num_cuenta', '!=', '')
                    ->where('num_cuenta', $this->num_cuenta)
                    ->where('persona_id', '!=', $this->id)
                    ->whereNull('deleted_at');
                })
            ]
        ];
    }

    public function messages() {
        return [
            'num_cuenta.unique' => 'El número de cuenta le pertenece a otro tutor.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }
}
