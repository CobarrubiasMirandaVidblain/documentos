@extends('bienestar::layouts.master')

@section('content-subtitle')
TABLA DE REFRENDOS
@endsection

@push('head')

<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content')
<div class="box box-primary shadow">
	<div class="box-header with-border">
        <h3 class="box-title">TABLA DE REFRENDOS:</h3>
    </div>

    <div class="box-body">
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" class="form-control" id="inputBuscar" name="inputBuscar">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="buttonBuscar" name="buttonBuscar">BUSCAR</button>
			</span>
		</div>
        
        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'refrendos', 'name' => 'refrendos', 'style' => 'width: 100%']) !!}
    </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>

{!! $dataTable->scripts() !!}

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
var dt = (function() {
    var tabla;

    var inputBuscar;

    var buttonBuscar;

    var setTabla = function(nombre) {
        if(typeof nombre === 'string' || nombre instanceof String) {
            tabla = window.LaravelDataTables[nombre];
        }
    };

    var setInputBuscar = function(nombre) {
        if(typeof nombre === 'string' || nombre instanceof String) {
            
            inputBuscar = $('#' + nombre);
            
            inputBuscar.keypress(function(e) {
				if(e.which === 13) {
                    tabla.search(inputBuscar.val()).draw();
				}
			});
        }
    };

    var setButtonBuscar = function(nombre) {
        if(typeof nombre === 'string' || nombre instanceof String) {
            buttonBuscar = $('#' + nombre);
            
            buttonBuscar.on('click', function() {
                tabla.search(inputBuscar.val()).draw();
            });
        }
    };

    return {
        setTabla: setTabla,
        setInputBuscar: setInputBuscar,
        setButtonBuscar: setButtonBuscar
    };
}());
dt.setTabla('refrendos');
dt.setInputBuscar('inputBuscar');
dt.setButtonBuscar('buttonBuscar');
</script>
@endpush