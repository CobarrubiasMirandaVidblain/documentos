@extends('bienestar::layouts.master')

@section('content-subtitle')
Detalles dispersion
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary shadow">
    <div class="box-header with-border">
        <h3 class="box-title">SELECCION DE BENEFICIARIOS PARA LA DISPERSION: <label>{{$dispersion->nombre_listado}}</label></h3>
        <a href="{{route('bienestar.dispersiones.edit', $dispersion->id)}}" class="btn btn-warning btn-xs pull-right"><i class="fa fa-pencil"></i> Editar</a>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:30%">
                                <i class="fa fa-database margin-r-5"></i>
                                Id:
                            </th>
                            <td>
                                {{ $dispersion->id or '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-file-text-o margin-r-5"></i>
                                Oficio:
                            </th>
                            <td>
                                {{ $dispersion->nombre_listado or '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-calendar margin-r-5"></i>
                                Año:
                            </th>
                            <td>
                                {{ $dispersion->ejercicio->anio or '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-star margin-r-5"></i>
                                Bimestre:
                            </th>
                            <td>
                                {{ $dispersion->bimestre or '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-money margin-r-5"></i>
                                Importe:
                            </th>
                            <td>
                                ${{ $dispersion->importe or '0' }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:30%">
                                <i class="fa fa-user margin-r-5"></i>
                                Creada por:
                            </th>
                            <td>
                                {{ $dispersion->usuario->persona->nombreCompleto() }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-signal margin-r-5"></i>
                                Estado:
                            </th>
                            <td>
                                {{ $dispersion->status or '' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-plus margin-r-5"></i>
                                Fecha de creación:
                            </th>
                            <td>
                                {{ isset($dispersion->created_at) ? date('d/m/Y h:i:s', strtotime($dispersion->created_at)) : 'SIN FECHA DE CREACION' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-check margin-r-5"></i>
                                Fecha de validación:
                            </th>
                            <td>
                                {{ isset($dispersion->fecha_validacion) ? date('d/m/Y h:i:s', strtotime($dispersion->fecha_validacion)) : 'SIN FECHA DE VALIDACION' }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <i class="fa fa-bank margin-r-5"></i>
                                Fecha de pago:
                            </th>
                            <td>
                                {{ isset($dispersion->fecha_pago) ? date('d/m/Y h:i:s', strtotime($dispersion->fecha_pago)) : 'SIN FECHA DE PAGO' }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <input type="checkbox" class="seleccion_total" id="todos" {{ ($select_all === true) ? 'checked' : '' }}> <strong>Seleccionar todo</strong>
            </div>
        </div>
    </div>

    <div class="box-body">
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
            <input type="text" id="search" name="search" class="form-control">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
            </span>
        </div>

        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'candidatos']) !!}
    </div>

    <div class="box-footer">
        <div class="pull-right">
            @if(auth()->user()->hasRoles(['SUPERADMIN','COORDINADOR','OPERATIVO']))
            @if($dispersion->status === 'EN PROCESO')
            <button type="button" class="btn btn-warning" onclick="status_dispersion('EN REVISION BIENESTAR');"><i class="fa fa-exclamation"></i> Revisión</button>
            @endif
            @endif
            <a href="{{ route('bienestar.dispersiones.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
        </div>
    </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    function status_dispersion(status) {
        swal({
            title: '¿Estas seguro?',
            text: '¡La lista se va a cambiar de estatus a ' + status + '!',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'Cerrar',
            allowEscapeKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if(result.value) {
                block();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url : "{{ route('bienestar.dispersiones.update', $dispersion->id) }}" + '?_method=PUT',
                    type: 'POST',
                    data: { 'status': status },
                    //processData: false,
                    //contentType: false,
                    success: function(response) {
                        unblock();

                        swal({
                            title: 'Listado de dispersión guardado.',
                            text: 'Guardado con el id: ' + response.id,
                            type: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then((result) => {
                            if(result.value) {
                                location.reload();
                                //var re = '{{ route('bienestar.dispersiones.index') }}';
                                //window.location.href = re;
                            }
                        });
                    },
                    error: function(response) {
                        unblock();

                        if(response.status === 403) {
                            swal({
                                title: 'Error al tratar de guardar el listado de dispersión.',
                                text: response.responseJSON.message,
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }

                        if(response.status === 409) {
                            swal({
                                title: 'Error al tratar de guardar el listado de dispersión.',
                                text: response.responseJSON.message,
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }

                        if(response.status === 422) {
                            swal({
                                title: 'Error al tratar de guardar el listado de dispersión.',
                                text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }
                    }
                });
                /*swal({
                    title: '¿Estas seguro?',
                    text: '¡La lista no podra ser modificada o eliminada!',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#28a745',
                    cancelButtonColor: '#dc3545',
                    cancelButtonText: 'NO',
                    confirmButtonText: 'SI'
                }).then((result) => {
                    if(result.value) {
                        block();
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url : "{{ route('bienestar.dispersiones.update', $dispersion->id) }}",
                            type: 'put',
                            data: {
                                '_token': $('meta[name="csrf-token"]').attr('content'),
                                'status': 'Enviada'
                        },
                        success: function(response) {
                            unblock();
                        },
                        error: function(response) {
                            unblock();
                        }
                    });
                    }
                });*/
            }
        });
    }

var table = (function() {
    var tabla = undefined,
    btn_buscar = $('#btn_buscar'),
    search = $('#search');

    function init() {
        search.keypress(function(e) {
            if(e.which === 13) {
                tabla.DataTable().search(search.val()).draw();
            }
        });

        btn_buscar.on('click', function() {
            tabla.DataTable().search(search.val()).draw();
        });
    };

    function set_table(valor) {
        tabla = $(valor);
    };

    function get_table() {
        return tabla;
    };

    return {
        init: init,
        set_table: set_table,
        get_table: get_table
    };
})();


    table.set_table($('#candidatos').dataTable());
    table.init();

    $(document).ready(function() {


        $('.seleccion_total').iCheck({
            checkboxClass: 'icheckbox_square-red',
            increaseArea: '20%'
        });

        $('.seleccion_total').on('ifChecked', function(event){
            block();
            $.ajax({
                url : "{{route('bienestar.dispersiones.integrantes.store')}}",
                type: 'post',
                data: {'_token': $('meta[name="csrf-token"]').attr('content'),
                       'detalles':{'miembro_id': 'all',
                       'lista_id': {{$dispersion->id}}}
                },
                success: function(response) {
                    unblock();
                    $('#candidatos').DataTable().ajax.reload();
                    swal({
                        title: '¡Agregados!',
                        text: 'Se han incluido todos en la dispersión',
                        type: 'success',
                        //timer: 1500
                    });
                },
                error: function(response) {
                    unblock();
                    $('#candidatos').DataTable().ajax.reload();
                    alert('ha ocurrido un error, por favor reintente');
                }
            });
        });
        $('.seleccion_total').on('ifUnchecked', function(event){
            block();
            $.ajax({
                url : "{{route('bienestar.dispersiones.integrantes.store')}}" + '/all',
                type: "delete",
                data: { '_token': $('meta[name="csrf-token"]').attr('content'),
                        'detalles':{'miembro_id': 'all','lista_id': {{$dispersion->id}}}
                },
                success: function(response){
                    unblock();
                    swal({
                        title: '¡Eliminados!',
                        text: 'Se han eliminado todos de la dispersión',
                        type: 'success',
                        //timer: 1500
                    });
                    $('#candidatos').DataTable().ajax.reload();
                },
                error : function(response){
                    unblock();
                    $('#candidatos').DataTable().ajax.reload();
                    alert('ha ocurrido un rror, por favor reintente');
                }
            });
        });
    })
    $('#candidatos').on( 'draw.dt', function () {
        //botones en la tabla
        $('.seleccion').each(function(){
            var self = $(this),
            label = self.next(),
            aux = label.text();
            //label.remove();
            var id=parseInt(self.attr('id'));

            self.iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_line',
                increaseArea: '20%'
                //insert: '<div class="icheck_line-icon"></div>' + label_text
            });
            if(aux==1){
                self.iCheck('check');
            }
            label.text(' Seleccionar');
            self.on('ifChecked', function(event){
                block()
                $.ajax({
                    url : "{{route('bienestar.dispersiones.integrantes.store')}}",
                    type: 'post',
                    data: {'_token': $('meta[name="csrf-token"]').attr('content'),
                           'detalles':{'miembro_id': id,'lista_id': {{$dispersion->id}}}
                       },
                    success: function(response) {
                        unblock();
                    },
                    error: function(response) {
                        unblock();
                        $('#candidatos').DataTable().ajax.reload();
                        alert('ha ocurrido un error, por favor reintente');
                    }
                });
            });
            self.on('ifUnchecked',function(event){
                block();
                $.ajax({
                    url : "{{route('bienestar.dispersiones.integrantes.store')}}" + '/' +id,
                    type: "delete",
                    'data': { '_token': $('meta[name="csrf-token"]').attr('content'),
                              'detalles':{'miembro_id': id,'lista_id': {{$dispersion->id}}}
                    },
                    success: function(response){
                        unblock();
                    },
                    error : function(response){
                        unblock();
                        $('#candidatos').DataTable().ajax.reload();
                        alert('ha ocurrido un rror, por favor reintente');
                    }
                });
            });
        });
    });

    function enviar_dispersion() {
        swal({
			title: '¿Estas seguro?',
			text: '¡La lista no podra ser modificada o eliminada!',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'NO',
			confirmButtonText: 'SI'
		}).then((result) => {
			if(result.value) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
                $.ajax({
                    url : "{{route('bienestar.dispersiones.update',$dispersion->id)}}",
                    type: 'put',
                    data: {'_token' : $('meta[name="csrf-token"]').attr('content'),
                           'status' : 'Enviada'
                    },
                    success: function(response) {
                        unblock();
                        if(response.success){
                            swal({
                                title: '¡Enviada!',
                                text: 'Se ha enviado la dispersión correctamente',
                                type: 'success',
                                //timer: 1500
                            }).then((result) => {
                                location.reload();
                            });
                        }else{
                            swal({
                                title: '¡Error!',
                                text: 'Agrega al menos 1 beneficiario',
                                type: 'warning',
                                timer: 3100
                            });
                        }
                    },
                    error: function(response) {
                        unblock();
                    }
                });
			}
		});
    }
</script>
@endpush