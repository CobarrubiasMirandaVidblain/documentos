@extends('bienestar::layouts.master')

@section('content-subtitle')
{{ isset($dispersion) ? 'Editar' : 'Agregar' }} Lista de Dispersión:
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">{{ isset($dispersion) ? 'Editar' : 'Agregar' }} Lista de Dispersión:</h3>
    </div>

    <div class="box-body">
        <form data-toggle="validator" role="form" id="form_dispersion" action="{{ isset($dispersion) ? route('bienestar.dispersiones.update', $dispersion->id) : route('bienestar.dispersiones.store') }}" method="{{ isset($dispersion) ? 'PUT' : 'POST' }}">
            @include('bienestar::dispersiones.fragmento.create_edit')
        </form>
    </div>

    <div class="box-footer">
        <div class="pull-right">
            <button type="button" class="btn btn-success" onclick="create_edit();"><i class="fa fa-database"></i> Guardar</button>
            <a href="{{ route('bienestar.dispersiones.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
        </div>
    </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
    var form = $('#form_dispersion'),
    action = form.attr('action'),
    method = form.attr('method');

    $(':input').on('propertychange input', function(e) {
        var ss = e.target.selectionStart;
        var se = e.target.selectionEnd;
        e.target.value = e.target.value.toUpperCase();
        e.target.selectionStart = ss;
        e.target.selectionEnd = se;
    });

    form.validate({
        rules: {
            nombre_listado: {
                required: true,
                minlength: 3,
                pattern_nombre_dispersion: 'El nombre solo puede contener letras y diagonales.'
            },
            importe: {
                required: true,
                pattern_importe: 'Solo numeros.'
            }
        },
        messages: {
        }
    });

    function create_edit() {
        if(form.valid()) {
            block();

            var formData = new FormData(form[0]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: action + '?_method=' + method,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                success: function(response) {
                    unblock();

                    swal({
                        title: 'Listado de dispersión guardado.',
                        text: 'Guardado con el id: ' + response.id,
                        type: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then((result) => {
                        if(result.value) {
                            var re = '{{ route('bienestar.dispersiones.index') }}';
                            window.location.href = re;
                        }
                    });
                },
                error: function(response) {
                    unblock();

                    if(response.status === 403) {
                        swal({
                            title: 'Error al tratar de guardar el listado de dispersión.',
                            text: response.responseJSON.message,
                            type: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    }

                    if(response.status === 409) {
                        var e = response.responseJSON.errors[0];
                        var l = e.data;
                        var m = e.message;
                        swal({
                            title: 'Error al tratar de guardar el listado de dispersión.',
                            text: m,
                            type: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    }

                    if(response.status === 422) {
                        swal({
                            title: 'Error al tratar de guardar el listado de dispersión.',
                            text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
                            type: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    }
                }
            });
        }
    };
    /*function procesarDispersion() {
        if($('#form_dispersion').valid()){
            block();
            $.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

            $.ajax({
				url: $('#form_dispersion').attr('action'),
    			type: $('#form_dispersion').attr('method'),
                data   : $('#form_dispersion').serialize(),
				success: function(response) {
					if(response.success){
                        unblock();
    					var re = '{{route('bienestar.dispersiones.index')}}';
    				    window.location.href = re;
                    }else{
                        unblock();
                        swal({
                			title    : 'Error',
                			text     : response.mensaje,
                			type     : 'warning'
                		}).then((result) => {
                            var re = '{{route('bienestar.dispersiones.index')}}';
        				    window.location.href = re;
                        });
                    }
				},
				error: function(response) {
					unblock();
				}
			});
        }
    }
    function dispersion_delete(dispersion_id) {
		swal({
			title: '¿Estas seguro?',
			text: '¡Usted no podrá recuperar este registró!',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'NO',
			confirmButtonText: 'SI'
		}).then((result) => {
			if(result.value) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: "{{route('bienestar.dispersiones.index')}}" + '/' + dispersion_id,
					type: 'DELETE',
					success: function(response) {
						unblock();
						swal({
							title: '¡Eliminado!',
							text: 'Su registro ha sido eliminado..',
							type: 'success',
							timer: 1500
						});
                        var re = '{{route('bienestar.dispersiones.index')}}';
                        window.location.href = re;
					},
					error: function(response) {
						unblock();
					}
				});
			}
		});

	}*/
</script>
@endpush