@extends('bienestar::layouts.master')

@section('content-subtitle')
Detalles de la Dispersión <label>{{ $dispersion->nombre_listado }}</label>
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
	.dt-button-collection .dt-button {
		width: 100%;
	}
</style>

<link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary shadow">
    <div class="box-header with-border">
        <h3 class="box-title">Detalles de la Dispersión <label>{{ $dispersion->nombre_listado }}</label>:</h3>
    </div>

    <div class="box-body">
    	<div>
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    			<label>Bimestre:</label>
    			<label id="bimestre" for="bimestre">{{$dispersion->bimestre}}</label>
    		</div>
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    			<label>Importe:</label>
    			<label id="importe" for="importe">${{$dispersion->importe}}</label>
    		</div>
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    			<label>Creada por:</label>
    			<label id="usuario" for="usuario">{{$dispersion->usuario->persona->nombreCompleto()}}</label>
    		</div>
    		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    			<label>Fecha de creacion:</label>
    			<label id="fecha" for="fecha">{{date("d/m/Y h:i:s", strtotime($dispersion->fecha))}}</label>
    		</div>
    	</div>

    	<div class="input-group input-group-sm col-lg-3 pull-right">
    		<input type="text" id="search" class="form-control" placeholder="Buscar" >
    		<span class="input-group-btn">
    			<button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
    		</span>
    	</div>

    	{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'detalles']) !!}
    </div>

    <div class="box-footer">
        <div class="pull-right">
            @if(auth()->user()->hasRoles(['SUPERADMIN','ADMINISTRATIVO']))
            @if($dispersion->status === 'EN REVISION BIENESTAR')
            <button type="button" class="btn btn-primary" onclick="status_dispersion('ENVIADA');"><i class="fa fa-send"></i> Enviar</button>
            @endif
            @endif

            @if(auth()->user()->hasRoles(['SUPERADMIN','FINANCIERO VALIDADOR']))
            @if($dispersion->status === 'EN REVISION FINANCIERO')
            <button type="button" class="btn btn-success" onclick="status_dispersion('CONFIRMADA');"><i class="fa fa-check"></i> Confirmar</button>
            <button type="button" class="btn btn-danger" onclick="status_dispersion('RETENIDA');"><i class="fa fa-ban"></i> Retenido</button>
            @endif
            @endif
            <a href="{{ route('bienestar.dispersiones.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
        </div>
    </div>
</div>

<!--<form action="{{ route('files.store') }}" id="my-dropzone" class="dropzone">
<input type="hidden" name="id" value="{{ $dispersion->id }}" />
<input type="hidden" name="carpeta" value="dispersiones" />
<input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
</form>-->
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>

<script type="text/javascript">
Dropzone.options.myDropzone = {
    dictDefaultMessage: 'Arrastra los archivos aquí para subirlos',
    addRemoveLinks: true,
    dictRemoveFile: 'Remover',
    init: function() {
        @foreach($archivos as $archivo)
            var mockFile = { name: '{{ $archivo->nombre }}', size: '{{ $archivo->tamanio }}' };

            mockFile.id = '{{ $archivo->id }}';
            
            this.emit('addedfile', mockFile);
            
            this.createThumbnailFromUrl(mockFile, '{{ asset("$archivo->url") }}');
            
            this.emit('complete', mockFile);

            var link = document.createElement('a');

            link.setAttribute('href', '{{ asset("$archivo->url") }}');

            link.innerHTML = "<br>Descargar";

            mockFile.previewTemplate.appendChild(link);
        @endforeach

        this.on('removedfile', function(file) {
            block();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({
                url: '{{ route('files.destroy', 0) }}' + '?id=' + file.id,
                type: 'DELETE',
                processData: false,
                contentType: false,
                success: function(response) {
                    unblock();
                },
                error: function(response) {
                }
            });
        });
        this.on('addedfile', function(file) {
            if(file.size > this.options.maxFilesize * 1024 * 1024) {
                file.previewElement.remove();
                var notice = new PNotify({
                    title: '<strong>¡Error algo fallo!</strong>',
                    text: 'Solo se permiten archivos de ' + this.options.maxFilesize + 'MB o menos.',
                    addclass: 'bg-danger',
                    hide: false,
                    buttons: {
                        closer: true,
                        sticker: false
                    }
                });

                notice.get().click(function() {
                    notice.remove();
                });
            }
            if(this.files.length) {
                var _i, _len, _ref = this.files.slice();
                for(_i = 0, _len = this.files.length; _i < _len - 1; _i++) {
                    if(_ref[_i].name === file.name) {
                        this.removeFile(_ref[_i]);
                    }
                }
            }
        });
    },
    success: function(file, response) {
        var link = document.createElement('a');
        link.setAttribute('href', response.url);
        link.innerHTML = '<br>Descargar';
        file.previewTemplate.appendChild(link);

        file.previewElement.querySelector('[data-dz-name]').innerHTML = response.nombre;
        file.id = response.id;
    }
};


    function status_dispersion(status) {
        swal({
            title: '¿Estas seguro?',
            text: '¡La lista se va a cambiar de estatus a ' + status + '!',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Aceptar',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'Cerrar',
            allowEscapeKey: false,
            allowOutsideClick: false
        }).then((result) => {
            if(result.value) {
                block();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url : "{{ route('bienestar.dispersiones.update', $dispersion->id) }}" + '?_method=PUT',
                    type: 'POST',
                    data: { 'status': status },
                    //processData: false,
                    //contentType: false,
                    success: function(response) {
                        unblock();

                        swal({
                            title: 'Listado de dispersión guardado.',
                            text: 'Guardado con el id: ' + response.id,
                            type: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then((result) => {
                            if(result.value) {
                                location.reload();
                                //var re = '{{ route('bienestar.dispersiones.index') }}';
                                //window.location.href = re;
                            }
                        });
                    },
                    error: function(response) {
                        unblock();

                        if(response.status === 403) {
                            swal({
                                title: 'Error al tratar de guardar el listado de dispersión.',
                                text: response.responseJSON.message,
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }

                        if(response.status === 409) {
                            swal({
                                title: 'Error al tratar de guardar el listado de dispersión.',
                                text: response.responseJSON.message,
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }

                        if(response.status === 422) {
                            swal({
                                title: 'Error al tratar de guardar el listado de dispersión.',
                                text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }
                    }
                });
                /*swal({
                    title: '¿Estas seguro?',
                    text: '¡La lista no podra ser modificada o eliminada!',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#28a745',
                    cancelButtonColor: '#dc3545',
                    cancelButtonText: 'NO',
                    confirmButtonText: 'SI'
                }).then((result) => {
                    if(result.value) {
                        block();
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url : "{{ route('bienestar.dispersiones.update', $dispersion->id) }}",
                            type: 'put',
                            data: {
                                '_token': $('meta[name="csrf-token"]').attr('content'),
                                'status': 'Enviada'
                        },
                        success: function(response) {
                            unblock();
                        },
                        error: function(response) {
                            unblock();
                        }
                    });
                    }
                });*/
            }
        });
    }
$(document).ready(function(){
    table.set_table($('#detalles').dataTable());
    table.init();
})
var table = (function() {
	var tabla = undefined,
	btn_buscar = $('#btn_buscar'),
	search = $('#search');

	function init() {
		search.keypress(function(e) {
			if(e.which === 13) {
				tabla.DataTable().search(search.val()).draw();
			}
		});

		btn_buscar.on('click', function() {
			tabla.DataTable().search(search.val()).draw();
		});

	};

	function set_table(valor) {
		tabla = $(valor);
	};

	function get_table() {
		return tabla;
	};

	return {
		init: init,
		set_table: set_table,
		get_table: get_table
	};
})();
(function ($,DataTable){
    var _buildUrl = function(dt, action) {
        var url = dt.ajax.url() || '';
        var params = dt.ajax.params();
        params.action = action;

        if (url.indexOf('?') > -1) {
            return url + '&' + $.param(params);
        }

        return url + '?' + $.param(params);
    };

    DataTable.ext.buttons.MyPrint = {
        className: 'buttons-print',

        text: function (dt) {
            return  '<i class="fa fa-print"></i> Imprimir todo'; //+ dt.i18n('buttons.print', 'Print');
        },

        action: function (e, dt, button, config) {
			alert("cualquier cosita");
            var url = _buildUrl(dt, 'MyPrint');
            window.location = url;
        }
    };
})(jQuery, jQuery.fn.dataTable);

function pago_exito(id){
    actualizarPago('pago realizado con exito',1,id);
}
function pago_fallo (id){
    actualizarPago('error en el pago',0,id);
}
function actualizarPago(observacion,pagado,id){
    block();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: '{{route('bienestar.dispersiones.integrantes.store')}}'+'/'+id,
        type: 'PUT',
        data   : {'id':id, 'datos' :{'observacion':observacion, 'pagado':pagado}},
        success: function(response) {
             $('#detalles').DataTable().ajax.reload();
             unblock();
        },
        error: function(response) {
            unblock();
        }
    });
}
</script>
@endpush