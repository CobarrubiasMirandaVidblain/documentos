@extends('bienestar::layouts.master')

@section('content-subtitle')
Tabla de Tutorados
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary shadow">
	<div class="box-header with-border">
		<h3 class="box-title">Tabla de Tutorados:</h3>
		<a href="{{ route('bienestar.solicitantes.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Agregar</a>
	</div>
	<div class="box-body">
		<!-- /.box-header -->
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>

		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'solicitantes', 'name' => 'solicitantes', 'style' => 'width: 100%']) !!}

	</div>
</div>

<div class="modal fade" id="modal-cuenta">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="cerrar_modal('modal-cuenta')" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modal-cuenta-title">Datos de la cuenta bancaria:</h4>
			</div>
			<div class="modal-body" id="modal-body-cuenta"></div>
			<div class="modal-footer" id="modal-footer-cuenta">
				<button type="button" class="btn btn-success" onclick="cuenta.create_edit()">
					<i class="fa fa-database"></i>
					Guardar</button>  
				<button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-cuenta')">
						<i class="fa fa-remove"></i>
					Cerrar</button>  
			</div>      
		</div>
	</div>
  </div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
@include('bienestar::bancos.js.banco')
{!! $dataTable->scripts() !!}

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#solicitantes').dataTable());
	table.init();

	function abrir_modal_cuenta(url){
		block();
		$.get(url, function(data) {            
        	$('#modal-body-cuenta').html(data.html);                            
        	cuenta.init();
        	$('#modal-cuenta').modal('show');
			$('#form_cuenta').submit(function(e){
    			return false;
  			}); 
			unblock();           
      	});
	}

	function cuenta_create_edit_error(response){
		unblock();
		if(response.status === 422) {
			swal({
				title: 'Error al actualizar la cuenta bancaria.',				
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Regresar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
	}
	
	function cuenta_create_edit_success(response, method) {
		swal({
      		title: '¡Correcto!',
      		text: 'Cuenta bancaria actualizada.',
      		type: 'success',
      		timer: 2000
    	});
		table.get_table().DataTable().ajax.reload(null, false);
		app.set_bloqueo(false);
		unblock();
    	cerrar_modal('modal-cuenta');
	}

	function cerrar_modal(modal){
		$("#"+modal).modal('hide');
	}
</script>
@endpush