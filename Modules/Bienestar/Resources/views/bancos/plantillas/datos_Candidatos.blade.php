    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <img src="{{ asset(isset($candidato) ? $candidato->persona->get_url_fotografia() : 'images/no-image.png') }}" class="img-thumbnail center-block" id="imagen" name="imagen">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Nombre:</label>
                        {{ $candidato->persona->nombre or '' }}
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Apellido Paterno:</label>
                        {{ $candidato->persona->primer_apellido or '' }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Apellido Materno:</label>
                        {{ $candidato->persona->segundo_apellido or '' }}
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Género:</label>
                        @if(isset($candidato))
                            @if($candidato->persona->genero == 'M')
                                MASCULINO
                            @endif
                            @if($candidato->persona->genero == 'F')
                                FEMENINO
                            @endif
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Fecha de Nacimiento:</label>
                        {{ isset($candidato) ? $candidato->persona->get_formato_fecha_nacimiento() : '' }}
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>CURP:</label>
                        {{ $candidato->persona->curp or '' }}
                    </div>
                </div>

                @if(isset($candidato->persona->clave_electoral))
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Clave Electoral:</label>
                        {{ $candidato->persona->clave_electoral or '' }}
                    </div>
                </div>
                @endif

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Discapacidad:</label>
                        {{ $candidato->discapacidad->nombre or '' }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Año de registro:</label>
                        {{ $candidato->ejercicio->anio or '' }}
                    </div>
                </div>

            </div>

        </div>

    </div>

    <button class="btn pull-right collapsed" type="button" data-toggle="collapse" data-target="#div_dir" aria-expanded="false">
        <i class="fa fa-remove"></i>
    </button>
    <h4>Dirección:</h4>
    <hr>

    <div id="div_dir" class="collapse">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group">
                    <label>Calle:</label>
                    {{ $candidato->persona->calle or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <div class="form-group">
                    <label>Número Exterior:</label>
                    {{ $candidato->persona->numero_exterior or '' }}
                </div>
            </div>

            @if(isset($candidato->persona->numero_interior))
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <div class="form-group">
                    <label>Número Interior:</label>
                    {{ $candidato->persona->numero_interior or '' }}
                </div>
            </div>
            @endif

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group">
                    <label>Colonia:</label>
                    {{ $candidato->persona->colonia or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <div class="form-group">
                    <label>Código Postal:</label>
                    {{ $candidato->persona->codigopostal or '' }}
                </div>
            </div>

        </div>

        <div class="row" >

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Región:</label>
                    {{ $candidato->persona->localidad->municipio->distrito->region->nombre or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Distrito:</label>
                    {{ $candidato->persona->localidad->municipio->distrito->nombre or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Municipio:</label>
                    {{ $candidato->persona->localidad->municipio->nombre or '' }}
                </div>
            </div>

            @if(isset($candidato->persona->localidad))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Localidad:</label>
                    {{ $candidato->persona->localidad->nombre or '' }}
                </div>
            </div>
            @endif

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label>Referencias del Domicilio:</label>
                    {{ $candidato->persona->referencia_domicilio or '' }}
                </div>
            </div>

        </div>

    </div>

    <button class="btn pull-right collapsed" type="button" data-toggle="collapse" data-target="#div_con" aria-expanded="false">
        <i class="fa fa-remove"></i>
    </button>
    <h4>Contacto:</h4>
    <hr>
    <div id="div_con" class="collapse">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <div class="form-group">
                    <label for="numero_celular">Celular:</label>
                    {{ $candidato->persona->numero_celular or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Télefono:</label>
                    {{ $candidato->persona->numero_local or '' }}
                </div>
            </div>

            @if(isset($candidato->persona->numero_interior))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Correo Electrónico:</label>
                    {{ $candidato->persona->email or '' }}
                </div>
            </div>
            @endif

        </div>
    </div>
