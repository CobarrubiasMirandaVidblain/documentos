<form data-toggle="validator" role="form" id="form_cuenta" action="{{ URL::to('bienestar/bancos') . '/' . $tutor->persona->id }}" method="PUT">              
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Folio Unico:</label>
                {{isset($solicitante) ? $solicitante->foliounico : ''}}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Tutorado:</label>
                {{isset($solicitante) ? $solicitante->persona->get_nombre_completo() : ''}}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Tutor:</label>
                {{isset($tutor) ? $tutor->persona->get_nombre_completo() : ''}}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="banco_id">Banco:</label>
                <select class="form-control select2" id="banco_id" name="banco_id" style="width: 100%;">                    
                    <option value="1" selected>BANAMEX</option>
                </select>
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="num_cuenta">Numero de Cuenta:</label>
                <input type="text" class="form-control" id="num_cuenta" name="num_cuenta" placeholder="Número de cuenta" value="{{(isset($tutor) && ($tutor->persona->cuentasbancos->first() !== null)) ? $tutor->persona->cuentasbancos->first()->num_cuenta : ''}}">
            </div>
        </div>      
        
    </div>
 </form>