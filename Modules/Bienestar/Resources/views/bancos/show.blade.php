@extends('bienestar::layouts.master')

@section('content-subtitle')
	Consultar Candidato
@endsection

@section('breadcrumbs')
<ol class="breadcrumb">
	<li><a href="{{ route('bienestar.home') }}"><i class="fa fa-dashboard"></i> Bienestar</a></li>
	<li class="active">Consultar Candidato</li>
</ol>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
	<li class="header active">Programa Bienestar</li>

	<li>
		<a href="{{ route('home') }}">
			<i class="fa fa-home"></i><span>IntraDIF</span>
		</a>
	</li>

	<li>
		<a href="{{ route('bienestar.home') }}">
			<i class="fa fa-dashboard"></i><span>Dashboard</span>
		</a>
	</li>

	<li class="treeview active menu-open">
		<a href="#">
			<i class="fa fa-group"></i><span>Candidatos</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ route('bienestar.solicitantes.index') }}"><i class="fa fa-list-ul"></i>Tabla de Candidatos</a></li>
			<li class="active"><a href="{{ route('bienestar.solicitantes.create') }}"><i class="fa fa-user-plus"></i>Nuevo Candidato</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-star"></i><span>Beneficiarios</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ route('bienestar.beneficiarios.index') }}"><i class="fa fa-list-ul"></i>Tabla de Beneficiarios</a></li>
			<li><a href="{{ route('bienestar.beneficiarios.create') }}"><i class="fa fa-user-plus"></i>Nuevo Beneficiario</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-refresh"></i><span>Refrendos</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ route('bienestar.refrendos.index') }}"><i class="fa fa-list-ul"></i>Tabla de Refrendos</a></li>
			<li><a href="{{ route('bienestar.refrendos.create') }}"><i class="fa fa-user-plus"></i>Nuevo Refrendo</a></li>
		</ul>
	</li>

	<li class="treeview">
		<a href="#">
			<i class="fa fa-money"></i><span>Dispersión</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ route('bienestar.dispersiones.index') }}"><i class="fa fa-list-ul"></i>Tabla de Dispersiones</a></li>
			<li><a href="{{ route('bienestar.dispersiones.create') }}"><i class="fa fa-user-plus"></i>Nueva Dispersión</a></li>
		</ul>
	</li>

	<li>
		<a href="#">
			<i class="fa fa-table"></i><span>Matriz</span>
		</a>
	</li>
</ul>
@endsection

@push('head')
<style type="text/css">
	.collapsed .fa {
		transform: rotate(45deg);
	}
	button .fa {
  		transition: .3s transform ease-in-out;
	}
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">CONSULTAR CANDIDATO</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
	@include('bienestar::solicitantes.plantillas.datos_Candidatos')		
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<a href="{{ route('bienestar.solicitantes.index') }}" class="btn btn-danger btn-md"><i class="fa fa-reply"></i> Regresar</a>
		<a href="{{ route('bienestar.solicitantes.edit',$candidato->id) }}" class="btn btn-warning btn-md pull-right"><i class="fa fa-pencil"></i> Editar</a>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript">
</script>
@endpush
