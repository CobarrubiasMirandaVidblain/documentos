<script type="text/javascript">	

	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Estás seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	function cuenta_create_edit_success(response, method) {};
	
	function cuenta_create_edit_error(response) {};

	var cuenta = (function() {
		var form = $('#form_cuenta'),
		banco = $('#banco_id'),
		action = form.attr('action'),
		method = form.attr('method');

		function init() {
			form = $('#form_cuenta'),
            banco = $('#banco_id'),
			action = form.attr('action'),
			method = form.attr('method');	
			banco.select2({
				language: 'es',
				placeholder: 'SELECCIONE UNA OPCIÓN',
                minimumResultsForSearch: Infinity			
			}).change(function(event) {
				banco.valid();				
			});			
			agregar_validacion();
			app.to_upper_case();
		};

		function create_edit() {
			if(form.valid()) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: form.serialize(),
					success: function(response) {
						cuenta_create_edit_success(response, method);
					},
					error: function(response) {
						cuenta_create_edit_error(response);
					}
				});

			}
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					num_cuenta: {
						required: true,
						minlength: 11,
						maxlength: 11,
						pattern_integer: ''
					},
					banco_id: {
						required: true
					}				
				},
				messages: {
				}
			});
		};

		return {
			init: init,			
			create_edit: create_edit
		};
	})();
</script>