@extends('bienestar::layouts.master')

@section('content-subtitle')
Consultar Beneficiario
@endsection

@push('head')
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Consultar Beneficiario:</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-warning" onclick="abrir_modal_bienestar_persona();"><i class="fa fa-plus"></i> Editar</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Datos del Beneficiario:</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Folio Único:</label>
								{{ $solicitante->bienestarpersonas->foliounico or '' }}
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Discapacidad:</label>
								{{ $solicitante->bienestarpersonas->discapacidad->nombre or '' }}
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Estado Civil:</label>
								{{ $solicitante->bienestarpersonas->estadocivil->nombre or '' }}
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Escolaridad:</label>
								{{ $solicitante->bienestarpersonas->escolaridad->nombre or '' }}
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Ocupacion:</label>
								{{ $solicitante->bienestarpersonas->miocupacion->nombre or '' }}
							</div>
						</div>
						
						@if(isset($persona->discapacidad))
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Discapacidad:</label>
								{{ $solicitante->bienestarpersonas->discapacidad->nombre or '' }}
							</div>
						</div>
						@endif
					</div>
					<div class="row">
						<a class="btn btn-warning pull-right" onclick="editar_persona({{ $solicitante->bienestarpersonas->persona->id }});"><i class="fa fa-plus"></i> Editar</a>
					</div>
					@include('personas.iframe.show', ['persona' => $solicitante->bienestarpersonas->persona])

					@foreach($solicitante->bienestardispersiones as $bienestardispersion)
					{{ $bienestardispersion->listasdispersiones->nombre_listado }}

					{{ $bienestardispersion->listasdispersiones->ejercicio->anio }}

					@if($bienestardispersion->pagado === 0)
					<span class="label label-warning"> PENDIENTE</span>
					@endif					
					@if($bienestardispersion->pagado === 1)
					<span class="label label-success"> EXITOSO</span>
					@endif
					@if($bienestardispersion->pagado === 2)
					<span class="label label-danger"> ERROR</span>
					@endif

					{{ $bienestardispersion->observacion }}

					<br>
					@endforeach
				</div>
				<div class="box-footer">
				</div>
			</div>

			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Datos del Tutor:</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body">
					<a class="btn btn-warning pull-right" onclick="editar_persona({{ $solicitante->bienestarpersonas->persona->tutor_bienestar(1)->first()->persona->id }});"><i class="fa fa-plus"></i> Editar</a>
					@include('personas.iframe.show', ['persona' => $solicitante->bienestarpersonas->persona->tutor_bienestar(1)->first()->persona])
				</div>
				<div class="box-footer">
				</div>
			</div>

			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">Datos Bancarios del Tutor:</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					</div>
				</div>
				<div class="box-body">
					@if($solicitante->bienestarpersonas->persona->tutor_bienestar(1)->first()->persona->cuentasbancos->first() !== null)
					<div class="row">

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Banco:</label>
								{{ $solicitante->bienestarpersonas->persona->tutor_bienestar(1)->first()->persona->cuentasbancos->first()->banco->nombre or '' }}
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
							<div class="form-group">
								<label>Número de Cuenta:</label>
								{{ $solicitante->bienestarpersonas->persona->tutor_bienestar(1)->first()->persona->cuentasbancos->first()->num_cuenta or '' }}
							</div>
						</div>

					</div>
					@else
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							Sin Datos Bancarios.
						</div>
					</div>
					@endif
				</div>
				<div class="box-footer">
				</div>
			</div>

			<div class="box-footer">
			</div>
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			<!--<a href="{{ route('bienestar.solicitantes.index') }}" class="btn btn-danger btn-md"><i class="fa fa-reply"></i> Regresar</a>
			<a href="{{ route('bienestar.solicitantes.edit',$solicitante->bienestarpersonas->id) }}" class="btn btn-warning btn-md pull-right"><i class="fa fa-pencil"></i> Editar</a>-->
		</div>
	</div>
	
	<div class="modal fade" id="modal_bienestar_persona">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="modal_title_bienestar_persona">Editar Datos Bienestar Persona:</h4>
				</div>
				<div class="modal-body" id="modal_body_bienestar_persona">
					<form data-toggle="validator" role="form" id="form_bienestar_persona" action="{{ route('bienestar.beneficiarios.update', $solicitante->bienestarpersonas->id) }}" method="PUT">
						<div class="form-group">
							<label for="discapacidad_id">Discapacidad:</label>
							<select id="discapacidad_id" name="discapacidad_id" class="form-control select2" style="width: 100%;">
								@if(isset($solicitante->bienestarpersonas->discapacidad))
								<option value="{{ $solicitante->bienestarpersonas->discapacidad->id }}" selected>{{ $solicitante->bienestarpersonas->discapacidad->nombre }}</option>
								@endif
							</select>
						</div>

						<div class="form-group">
							<label for="estado_civil">Estado Civil:</label>
							<select id="estadocivil_id" name="estadocivil_id" class="form-control select2" style="width: 100%;">
								@if(isset($solicitante->bienestarpersonas->estadocivil))
								<option value="{{ $solicitante->bienestarpersonas->estadocivil->id }}" selected>{{ $solicitante->bienestarpersonas->estadocivil->nombre }}</option>
								@endif
							</select>
						</div>

						<div class="form-group">
							<label for="grado_estudios">Escolaridad:</label>
							<select id="escolaridad_id" name="escolaridad_id" class="form-control select2" style="width: 100%;">
								@if(isset($solicitante->bienestarpersonas->escolaridad))
								<option value="{{ $solicitante->bienestarpersonas->escolaridad->id }}" selected>{{ $solicitante->bienestarpersonas->escolaridad->nombre }}</option>
								@endif
							</select>
						</div>

						<div class="form-group">
							<label for="ocupacion">Ocupación:</label>
							<select id="ocupacion_id" name="ocupacion_id" class="form-control select2" style="width: 100%;">
								@if(isset($solicitante->bienestarpersonas->miocupacion))
								<option value="{{ $solicitante->bienestarpersonas->miocupacion->id }}" selected>{{ $solicitante->bienestarpersonas->miocupacion->nombre }}</option>
								@endif
							</select>
						</div>
					</form>
				</div>
				<div class="modal-footer" id="modal_footer_bienestar_persona">
					<div class="pull-right">
						<button type="button" class="btn btn-success" id="bienestar_persona_edit" onclick="create_edit({{ $solicitante->bienestarpersonas->id }});"><i class="fa fa-plus"></i> Guardar</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-persona">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="modal-title-persona"></h4>
				</div>
				<div class="modal-body" id="modal-body-persona">

				</div>
				<div class="modal-footer" id="modal-footer-persona">
					<div class="pull-right">
						<button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript">
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	function agregar_validacion(form) {
		form.validate({
			rules: {
				discapacidad_id: {
					required: true
				},
				estadocivil_id: {
					required: true
				},
				escolaridad_id: {
					required: true
				},
				ocupacion_id: {
					required: true
				}
			},
			messages: {
			}
		});
	};

	function agregar_discapacidades_select_create(discapacidad) {
			discapacidad.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('discapacidades.select') }}',
					dataType: 'JSON',
					type: 'GET',
					//delay: 500,
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						var datos = [];

						for(var j = 0; j < data.length; j++){
							var found = false;

							for(var i = 0; i < datos.length; i++) {
								if(datos[i].text === data[j].padre) {
									datos[i].children.push({
										'id': data[j].id,
										'text': data[j].nombre
									});

									found = true;

									break;
								}
							}

							if(!found) {
								datos.push({
									text: data[j].padre,

									children: [{
										'id': data[j].id,
										'text': data[j].nombre
									}]
								});
							}
						}

						params.page = params.page || 1;

						return {
							results: datos
						};
					},
					cache: true
				}
			}).change(function(event) {
				discapacidad.valid();
			});
	}

    function agregar_estados_civiles_select_create(estado_civil) {
        estado_civil.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('estadosciviles.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            estado_civil.valid();
        });
    };

    function agregar_escolaridades_select_create(escolaridad) {
        escolaridad.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('escolaridades.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            escolaridad.valid();
        });
    };

    function agregar_ocupaciones_select_create(ocupacion) {
        ocupacion.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('ocupaciones.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            ocupacion.valid();
        });
    };

	agregar_validacion($('#form_bienestar_persona'));

	agregar_discapacidades_select_create($('#discapacidad_id'));

    agregar_estados_civiles_select_create($('#estadocivil_id'));

    agregar_escolaridades_select_create($('#escolaridad_id'));

    agregar_ocupaciones_select_create($('#ocupacion_id'));

	function abrir_modal_bienestar_persona() {
		$('#modal_bienestar_persona').modal('show');
	}

	function create_edit(id) {
		var form = $('#form_bienestar_persona'),
		action = form.attr('action'),
		method = form.attr('method');

		if(form.valid()) {

			block();

			var formData = new FormData(form[0]);

			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url: action + '?_method=' + method,
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				success: function(response) {
					unblock();
					$('#modal_bienestar_persona').modal('hide');
					location.reload();
				},
				error: function(response) {
					unblock();
					alert('Error.');
				}
			});
		}
	}
</script>

@include('personas.js.persona')

<script type="text/javascript">
	var candidato = (function() {
		var modal_personas = $('#modal-personas'),
		modal_tutores = $('#modal-tutores'),
		modal_persona = $('#modal-persona'),
		personas = $('#personas'),
		datatable_personas = undefined,
		datatable_tutores = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search'),
		elemento = undefined,
		ele = undefined,
		tutor = $('#tutor'),
		fila_tutor = $('#fila_tutor'),
		discapacidad = $('#discapacidad_id'),
		estado_civil = $('#estado_civil'),
		grado_estudios = $('#grado_estudios'),
		banco = $('#banco_id'),
		form = $('#form_candidato'),
		action = form.attr('action'),
		method = form.attr('method');

		function agregar_modal_static() {
			$('.modal').modal({
				backdrop: 'static'
			});

			$('.modal').modal('hide');
		};

		/*function abrir_modal_personas(el) {
			elemento = $('#' + el);
			ele = el;
			if(ele === 'tutorado_id') {
				modal_personas.modal('show');
			}
			if(ele === 'persona_id') {
				modal_tutores.modal('show');
			}
		};*/

		function persona_create_edit_success(response) {
			/*if(elemento !== undefined && ele !== undefined) {
				elemento.html("<option value='" + response.id + "'selected>" + response.nombre + "</option>");
				modal_persona.modal('hide');
				if(ele === 'tutorado_id') {
					modal_personas.modal('hide');
				}
				if(ele === 'persona_id') {
					modal_tutores.modal('hide');
				}
			}*/
			unblock();
			location.reload();
		};

		function persona_create_edit_error(response) {
			unblock();
		};

		function init_modal_persona() {
			$.fn.modal.Constructor.prototype.enforceFocus = function() {};

			/*var table = $('#personas').dataTable();

			datatable_personas = $(table).DataTable();

			search.keypress(function(e) {
				if(e.which === 13) {
					datatable_personas.search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				datatable_personas.search(search.val()).draw();
			});

			modal_personas.on('shown.bs.modal', function(event) {
				recargar_tabla_personas();
			});

			var tabla_tutores = $('#tutores').dataTable();

			datatable_tutores = $(tabla_tutores).DataTable();

			$('#search_tutor').keypress(function(e) {
				if(e.which === 13) {
					datatable_tutores.search($('#search_tutor').val()).draw();
				}
			});

			$('#btn_buscar_tutor').on('click', function() {
				datatable_tutores.search($('#search_tutor').val()).draw();
			});

			modal_tutores.on('shown.bs.modal', function(event) {
				recargar_tabla_tutores();
			});*/
		}

		/*function recargar_tabla_personas() {
			if(datatable_personas !== undefined) {
				datatable_personas.ajax.reload();
			}
		};*/

		/*function recargar_tabla_tutores() {
			if(datatable_tutores !== undefined) {
				datatable_tutores.ajax.reload();
			}
		};*/

		/*function seleccionar_persona(id, nombre) {
			if(elemento !== undefined) {
				elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
				if(ele === 'tutorado_id') {
					modal_personas.modal('hide');
				}
				if(ele === 'persona_id') {
					modal_tutores.modal('hide');
				}
			}
		};*/

		/*function agregar_tutor() {
			tutor.change(function() {
				if($(this).val() === 'NO') {
					$('#persona_id').rules('remove', 'required');
					fila_tutor.removeAttr('style').hide();
				}
				if($(this).val() === 'SI') {
					$('#persona_id').rules('add', { required: true });
					fila_tutor.show();
				}
			});
		};*/

		function agregar_discapacidad_select_create() {
			discapacidad.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('discapacidades.select') }}',
					dataType: 'JSON',
					type: 'GET',
					//delay: 500,
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						var datos = [];

						for(var j = 0; j < data.length; j++){
							var found = false;

							for(var i = 0; i < datos.length; i++) {
								if(datos[i].text === data[j].padre) {
									datos[i].children.push({
										'id': data[j].id,
										'text': data[j].nombre
									});

									found = true;

									break;
								}
							}

							if(!found) {
								datos.push({
									text: data[j].padre,

									children: [{
										'id': data[j].id,
										'text': data[j].nombre
									}]
								});
							}
						}

						params.page = params.page || 1;

						return {
							results: datos
						};
					},
					cache: true
				}
			}).change(function(event) {
				discapacidad.valid();
			});

			estado_civil.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});

			grado_estudios.select2({
				language: 'es'
			});

			tutor.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});
		};

		function agregar_banco_select_create() {
			banco.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('bancos.select') }}',
					//delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				banco.valid();
			});
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					foliounico: {
						required: true,
						minlength: 1,
						maxlength: 5,
						pattern_integer: ''
					},
					tutorado_id: {
						required: true
					},
					persona_id: {
						required: true
					},
					discapacidad_id: {
						required: true
					},
					estadocivil_id: {
						required: true
					},
					escolaridad_id: {
						required: true
					},
					ocupacion_id: {
						required: true
					}/*,
					banco_id: {
						required: true
					},
					num_cuenta: {
						required: true,
						minlength: 11,
						maxlength: 11,
						pattern_integer: ''
					},
					num_tarjeta: {
						required: false,
						minlength: 16,
						maxlength: 16,
						pattern_integer: ''
					}*/
				},
				messages: {
				}
			});
		};

		/*function create_edit() {
			if(form.valid()) {

				block();

				var formData = new FormData(form[0]);

				@if(isset($candidato))
				var old_candidato_id = {{ $candidato->id }};
				formData.append('old_candidato_id', old_candidato_id);
				@endif

				@if(isset($tutor))
				var old_tutor_id = {{ $tutor->id }};
				formData.append('old_tutor_id', old_tutor_id);
				@endif

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					success: function(response) {
						app.set_bloqueo(false);
						unblock();
						swal({
							title: 'Solicitante registrado.',
							text: '¿Agregar otro solicitante?',
							type: 'success',
							timer: 10000,
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonClass: 'btn btn-success',
							cancelButtonClass: 'btn btn-danger',
							confirmButtonText: 'Si',
							cancelButtonText: 'No',
							allowEscapeKey: false,
							allowOutsideClick: false
						}).then((result) => {
							if(result.value) {
								location.reload();
							}

							if(result.dismiss) {
								var re = "{{ URL::to('bienestar/solicitantes/') }}";
								window.location.href = re;// + '?n=' + new Date().getTime();
							}

							if(result.dismiss === swal.DismissReason.timer) {
								var re = "{{ URL::to('bienestar/solicitantes/') }}";
								window.location.href = re;// + '?n=' + new Date().getTime();
							}
						});
					},
					error: function(response) {
						unblock();

						if(response.status === 422) {
							swal({
								title: 'Error al tratar de guardar al solicitante.',
								text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Cerrar',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}

						if(response.status === 409) {
							var e = JSON.parse(response.responseText).errors[0];
							var l = e.data;
							var m = e.message;
							swal({
								title: 'Error al tratar de guardar al solicitante.',
								html: m,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Si',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}
					}
				});

			}
		};*/

		return {
			agregar_modal_static: agregar_modal_static,
			persona_create_edit_success: persona_create_edit_success,
			persona_create_edit_error: persona_create_edit_error,
			//abrir_modal_personas: abrir_modal_personas,
			init_modal_persona: init_modal_persona,
			//recargar_tabla_personas: recargar_tabla_personas,
			//seleccionar_persona: seleccionar_persona,
			
			//agregar_tutor: agregar_tutor,
			agregar_discapacidad_select_create: agregar_discapacidad_select_create,
			agregar_banco_select_create: agregar_banco_select_create,
			agregar_validacion: agregar_validacion,
			//create_edit: create_edit
		};
	})();

	/*function seleccionar_persona(id, nombre) {
		candidato.seleccionar_persona(id, nombre);
	}*/

	/*function agregar_persona() {
		var modal_persona = $('#modal-persona');

		var modal_title_persona = $('#modal-title-persona');

		var modal_body_persona = $('#modal-body-persona');

		var modal_footer_persona = $('#modal-footer-persona');

		$.get('/bienestar/personas/search?tipo=create_edit', function(data) {
		})
		.done(function(data) {
		})
		.fail(function(data) {

			modal_title_persona.text('Agregar Persona:');
			modal_body_persona.html($.parseJSON(data.responseText).html);

			app.to_upper_case();

			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_validacion();

			modal_persona.modal('show');

			var btn_create_edit = $('#btn_create_edit');
			btn_create_edit.attr('class', 'btn btn-success');
			btn_create_edit.html('<i class="fa fa-plus"></i> Agregar');

		});
	}*/

	function editar_persona(id) {
		block();
		var modal_persona = $('#modal-persona');

		var modal_title_persona = $('#modal-title-persona');

		var modal_body_persona = $('#modal-body-persona');

		var modal_footer_persona = $('#modal-footer-persona');

		var tipo_persona = "'" + tipo_persona + "'";

		$.get('/bienestar/personas/search?tipo=create_edit&id=' + id, function(data) {
		})
		.done(function(data) {
			unblock();
			modal_title_persona.text('Editar Persona:');
			modal_body_persona.html(data.html);

			app.to_upper_case();

			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_select_edit();
			persona.agregar_validacion();

			modal_persona.modal('show');

			var btn_create_edit = $('#btn_create_edit');
			btn_create_edit.attr('class', 'btn btn-warning');
			btn_create_edit.html('<i class="fa fa-pencil"></i> Editar');

		})
		.fail(function(data) {
			unblock();
		});
	}

	//app.to_upper_case();
	//app.agregar_bloqueo_pagina();

	candidato.agregar_modal_static();
	candidato.init_modal_persona();
	//candidato.agregar_tutor();
	candidato.agregar_discapacidad_select_create();
	candidato.agregar_banco_select_create();
	candidato.agregar_validacion();

	function persona_create_edit_success(response) {
		candidato.persona_create_edit_success(response);
	}

	function persona_create_edit_error(response) {
		candidato.persona_create_edit_error(response);
		if(response.status === 422) {
			swal({
				title: 'Error al tratar de guardar a la persona.',
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Cerrar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}

		if(response.status === 409) {
			var e = JSON.parse(response.responseText).errors[0];
			var l = e.data;
			var m = e.message;
			swal({
				title: 'Error al tratar de guardar a la persona.',
				html: m,
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Cerrar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
	}

    /*function agregar_estadosciviles_select_create(estadocivil) {
        estadocivil.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('estadosciviles.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            estadocivil.valid();
        });
    };*/

    /*function agregar_escolaridades_select_create(escolaridad) {
        escolaridad.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('escolaridades.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            escolaridad.valid();
        });
    };*/

    /*function agregar_ocupaciones_select_create(ocupacion) {
        ocupacion.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('ocupaciones.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            ocupacion.valid();
        });
    };*/

    //agregar_estadosciviles_select_create($('#estadocivil_id'));

    //agregar_escolaridades_select_create($('#escolaridad_id'));

    //agregar_ocupaciones_select_create($('#ocupacion_id'));
</script>
@endpush