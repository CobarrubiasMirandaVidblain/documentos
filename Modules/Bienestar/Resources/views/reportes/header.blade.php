<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte</title>
    <link rel="stylesheet" href="{{ asset('reportes/bootstrap.min.css') }}" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .container{
            margin-left: 0px;
            margin-right: 15px;
            width: 100% !important;
        }

        .row {
            margin-right: 0px; 
            margin-left: 0px; 
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-2"><img id="logo" src="{{ asset('/images/logo_header.png') }}" width="100%"></div>
            <div class="col-xs-4"></div>
            <div class="col-xs-6 text-right">
                <div class="row">ÁREA: Dirección de Atención a Personas con Discapacidad.</div>
                <div class="row">OFICIO NO.: {{ $dispersion->nombre_listado }}</div>
                <div class="row">ASUNTO: Solicitud de Dispersión</div>
                <div class="row">Fecha: {{ $dispersion->fecha }}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 text-center">"2018, Año de la erradicación del trabajo infantil"</div>
        </div>
    </div>
        
    <script src="{{ asset('reportes/jquery.min.js') }}"></script>
    <script src="{{ asset('reportes/bootstrap.min.js') }}" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>