@extends('bienestar::layouts.master')

@section('content-subtitle')
Tabla de Solicitantes
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary shadow">
	<div class="box-header with-border">
		<h3 class="box-title">Tabla de Solicitantes:</h3>
		@if(auth()->user()->hasRoles(['SUPERADMIN', 'COORDINADOR', 'OPERATIVO', 'APOYO OPERATIVO', 'ADMINISTRATIVO', 'LEGAL', 'AUXILIAR', 'FINANCIERO', 'MEDICO', 'CAPTURISTA']))
		<a href="{{ route('bienestar.solicitantes.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Agregar</a>
		@endif
	</div>
	<div class="box-body">
		<!-- /.box-header -->
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>

		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'solicitantes', 'name' => 'solicitantes', 'style' => 'width: 100%']) !!}

	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#solicitantes').dataTable());
	table.init();
	
	function solicitante_convertir(id, folio, nombre) {
		swal({
			title: '¿Estas seguro?',
			html: '¡Usted va a convertir al solicitante <strong>' + nombre + '</strong> con folio <strong>' + folio + '</strong> a beneficiario!',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'No',
			allowEscapeKey: false,
			allowOutsideClick: false
		}).then((result) => {
			if(result.value) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				
				$.ajax({
					url: "{{ URL::to('bienestar/solicitantes/convertirbeneficiario') }}" + '/' + id,
					type: 'POST',
					success: function(response) {
						table.get_table().DataTable().ajax.reload(null, false);

						unblock();

						swal({
							title: '¡Correcto!',
							html: 'El solicitante <strong>' + nombre + '</strong> con folio <strong>' + folio + '</strong> ha sido convertido a beneficiario con posición <strong>' + response.bienestar_persona.posicion + '</strong>.',
							type: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
						
						/*swal({
							title: '¡Correcto!',
							html: 'El solicitante <strong>' + nombre + '</strong> con folio <strong>' + folio + '</strong> ha sido convertido a beneficiario con posición <strong>' + response.bienestar_persona.posicion + '</strong>.',
							type: 'success',
							timer: 3000,
							confirmButtonText: 'Si',
							allowEscapeKey: false,
							allowOutsideClick: false
						});*/
					},
					error: function(response) {
						table.get_table().DataTable().ajax.reload(null, false);

                        unblock();

                        if(response.status === 403) {
                            swal({
                                title: '¡Error!',
                                html: response.responseJSON.message,
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }

                        if(response.status === 409) {
                            swal({
                                title: '¡Error!',
                                html: response.responseJSON.message,
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }

                        if(response.status === 422) {
                            swal({
                                title: '¡Error!',
                                html: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }
					}
				});
			}
		});
	}
</script>
@endpush