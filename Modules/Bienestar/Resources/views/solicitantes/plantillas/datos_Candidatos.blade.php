    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <img src="{{ asset(isset($solicitante) ? $solicitante->persona->get_url_fotografia() : 'images/no-image.png') }}" class="img-thumbnail center-block" id="imagen" name="imagen">
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label>Nombre:</label>
                        {{ $solicitante->persona->nombre or '' }}
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Apellido Paterno:</label>
                        {{ $solicitante->persona->primer_apellido or '' }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Apellido Materno:</label>
                        {{ $solicitante->persona->segundo_apellido or '' }}
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Género:</label>
                        @if(isset($solicitante))
                            @if($solicitante->persona->genero == 'M')
                                MASCULINO
                            @endif
                            @if($solicitante->persona->genero == 'F')
                                FEMENINO
                            @endif
                        @endif
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Fecha de Nacimiento:</label>
                        {{ isset($solicitante) ? $solicitante->persona->get_formato_fecha_nacimiento() : '' }}
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>CURP:</label>
                        {{ $solicitante->persona->curp or '' }}
                    </div>
                </div>

                @if(isset($solicitante->persona->clave_electoral))
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Clave Electoral:</label>
                        {{ $solicitante->persona->clave_electoral or '' }}
                    </div>
                </div>
                @endif

            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Discapacidad:</label>
                        {{ $solicitante->discapacidad->nombre or '' }}
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label>Año de registro:</label>
                        {{ $solicitante->ejercicio->anio or '' }}
                    </div>
                </div>

            </div>

        </div>

    </div>

    <button class="btn pull-right collapsed" type="button" data-toggle="collapse" data-target="#div_dir" aria-expanded="false">
        <i class="fa fa-remove"></i>
    </button>
    <h4>Dirección:</h4>
    <hr>

    <div id="div_dir" class="collapse">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group">
                    <label>Calle:</label>
                    {{ $solicitante->persona->calle or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <div class="form-group">
                    <label>Número Exterior:</label>
                    {{ $solicitante->persona->numero_exterior or '' }}
                </div>
            </div>

            @if(isset($solicitante->persona->numero_interior))
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <div class="form-group">
                    <label>Número Interior:</label>
                    {{ $solicitante->persona->numero_interior or '' }}
                </div>
            </div>
            @endif

            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group">
                    <label>Colonia:</label>
                    {{ $solicitante->persona->colonia or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
                <div class="form-group">
                    <label>Código Postal:</label>
                    {{ $solicitante->persona->codigopostal or '' }}
                </div>
            </div>

        </div>

        <div class="row" >

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Región:</label>
                    {{ $solicitante->persona->localidad->municipio->distrito->region->nombre or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Distrito:</label>
                    {{ $solicitante->persona->localidad->municipio->distrito->nombre or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Municipio:</label>
                    {{ $solicitante->persona->localidad->municipio->nombre or '' }}
                </div>
            </div>

            @if(isset($solicitante->persona->localidad))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Localidad:</label>
                    {{ $solicitante->persona->localidad->nombre or '' }}
                </div>
            </div>
            @endif

        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label>Referencias del Domicilio:</label>
                    {{ $solicitante->persona->referencia_domicilio or '' }}
                </div>
            </div>

        </div>

    </div>

    <button class="btn pull-right collapsed" type="button" data-toggle="collapse" data-target="#div_con" aria-expanded="false">
        <i class="fa fa-remove"></i>
    </button>
    <h4>Contacto:</h4>
    <hr>
    <div id="div_con" class="collapse">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <div class="form-group">
                    <label for="numero_celular">Celular:</label>
                    {{ $solicitante->persona->numero_celular or '' }}
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
                <div class="form-group">
                    <label>Télefono:</label>
                    {{ $solicitante->persona->numero_local or '' }}
                </div>
            </div>

            @if(isset($solicitante->persona->numero_interior))
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Correo Electrónico:</label>
                    {{ $solicitante->persona->email or '' }}
                </div>
            </div>
            @endif

        </div>
    </div>

    <button class="btn pull-right collapsed" type="button" data-toggle="collapse" data-target="#div_tut" aria-expanded="false">
        <i class="fa fa-remove"></i>
    </button>
    <h4>Tutor:</h4>
    <hr>
    <div id="div_tut" class="collapse">
        <div class="row">

            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div class="form-group">
                    <label>Nombre:</label>
                    {{ $solicitante->persona->tutor_bienestar(1)->first()->persona->nombre or '' }}
                </div>
            </div>

            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div class="form-group">
                    <label>Apellido Paterno:</label>
                    {{ $solicitante->persona->tutor_bienestar(1)->first()->persona->primer_apellido or '' }}
                </div>
            </div>

            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div class="form-group">
                    <label>Apellido Materno:</label>
                    {{ $solicitante->persona->tutor_bienestar(1)->first()->persona->segundo_apellido or '' }}
                </div>
            </div>
        </div>
    </div>
