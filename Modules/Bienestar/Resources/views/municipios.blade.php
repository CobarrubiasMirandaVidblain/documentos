@extends('bienestar::layouts.master')

@section('content-subtitle')
Dashboard
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="row cards">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		<div class="small-box bg-green shadow">
			<div class="inner">
				<p>DISTRITO: </p>
				<h4 id="padron">{{ $distrito->nombre }}</h4>
				<a class="btn btn-primary btn-sm btn-flat" href="/bienestar/bienestarpersonas/export?municipio_id={{ $municipio_id }}"><i class="fa fa-fw fa-file-excel-o"></i> Descargar</a>
			</div>
			<!--<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>-->
		</div>
	</div>
</div>
<div class="box box-primary shadow">
	<div class="box-header with-border">
		<h3 class="box-title">Beneficiarios por Municipio</h3>
	</div>
	<div class="box-body">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_1" data-toggle="tab">Tabla de Beneficiarios:</a></li>
				<li><a href="#tab_2" data-toggle="tab">Tabla de Beneficiarios por Genero:</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<table class="table table-bordered" id="municipios-padron" style="width: 100%;">
						<thead>
							<tr>
								<th>Municipio</th>
								<th>Total</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="tab_2">
					<table class="table table-bordered" id="municipios-genero" style="width: 100%;">
						<thead>
							<tr>
								<th>Municipio</th>
								<th>Genero</th>
								<th>Total</th>
							</tr>
						</thead>
					</table>
				</div>
				<!-- /.tab-pane -->
				<!--<div class="tab-pane" id="tab_3">
				</div>-->
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>

<script>
$(function() {
    $('#municipios-padron').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('bienestar.padron.municipios.data', $municipio_id) !!}',
        columns: [
            { data: 'municipio', name: 'municipio' },
            { data: 'total', name: 'total' }
        ]
    });
});
$(function() {
    $('#municipios-genero').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('bienestar.genero.municipios.data', $municipio_id) !!}',
        columns: [
            { data: 'municipio', name: 'municipio' },
            { data: 'genero', name: 'genero' },
            { data: 'total', name: 'total' }
        ]
    });
});
</script>
@endpush