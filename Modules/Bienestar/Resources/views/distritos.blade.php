@extends('bienestar::layouts.master')

@section('content-subtitle')
Dashboard
@endsection

@push('head')
@endpush

@section('content')
<div class="row cards">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		<div class="small-box bg-red shadow">
			<div class="inner">
				<p>REGIÓN: </p>
				<h4 id="padron">{{ $region->nombre }}</h4>
				<a class="btn btn-primary btn-sm btn-flat" href="/bienestar/bienestarpersonas/export?distrito_id={{ $distrito_id }}"><i class="fa fa-fw fa-file-excel-o"></i> Descargar</a>
			</div>
			<!--<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>-->
		</div>
	</div>
</div>
<div class="box box-primary shadow">
	<div class="box-header with-border">
	</div>

	<div class="box-body" id="container" style="width: 100%; height: 650px;"></div>

	<div class="box-footer">
		<div id="mensaje"></div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components\e\echarts.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\echarts-gl.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\ecStat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\dataTool.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\china.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\world.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\bmap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\simplex.js') }}"></script>

<script type="text/javascript">
	(function() {
		var attachEvent = document.attachEvent;
		var isIE = navigator.userAgent.match(/Trident/);
		console.log(isIE);
		var requestFrame = (function() {
			var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ||
			function(fn) { return window.setTimeout(fn, 20); };
			return function(fn) { return raf(fn); };
		})();

		var cancelFrame = (function() {
			var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ||
			window.clearTimeout;
			return function(id) { return cancel(id); };
		})();

		function resizeListener(e) {
			var win = e.target || e.srcElement;
			if(win.__resizeRAF__) cancelFrame(win.__resizeRAF__);
			win.__resizeRAF__ = requestFrame(function() {
				var trigger = win.__resizeTrigger__;
				trigger.__resizeListeners__.forEach(function(fn) {
					fn.call(trigger, e);
				});
			});
		}

		function objectLoad(e) {
			this.contentDocument.defaultView.__resizeTrigger__ = this.__resizeElement__;
			this.contentDocument.defaultView.addEventListener('resize', resizeListener);
		}

		window.addResizeListener = function(element, fn) {
			if(!element.__resizeListeners__) {
				element.__resizeListeners__ = [];
				if(attachEvent) {
					element.__resizeTrigger__ = element;
					element.attachEvent('onresize', resizeListener);
				}
				else {
					if(getComputedStyle(element).position == 'static') element.style.position = 'relative';
					var obj = element.__resizeTrigger__ = document.createElement('object'); 
					obj.setAttribute('style', 'display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%; overflow: hidden; pointer-events: none; z-index: -1;');
					obj.__resizeElement__ = element;
					obj.onload = objectLoad;
					obj.type = 'text/html';
					if(isIE) element.appendChild(obj);
					obj.data = 'about:blank';
					if(!isIE) element.appendChild(obj);
				}
			}
			element.__resizeListeners__.push(fn);
		};

		window.removeResizeListener = function(element, fn) {
			element.__resizeListeners__.splice(element.__resizeListeners__.indexOf(fn), 1);
			if(!element.__resizeListeners__.length) {
				if(attachEvent) element.detachEvent('onresize', resizeListener);
				else {
					element.__resizeTrigger__.contentDocument.defaultView.removeEventListener('resize', resizeListener);
					element.__resizeTrigger__ = !element.removeChild(element.__resizeTrigger__);
				}
			}
		}
	})();
</script>

<script type="text/javascript">
	//var padron = JSON.parse();

	var distritos = JSON.parse('{!! $distritos !!}');

	var arr_distritos = [];

	for(var i = 0; i < distritos.length; i++) {
		arr_distritos.push(distritos[i].name);
	}

	//$('#padron').text(padron);

	var padronRegion = JSON.parse('{!! $padronDistrito !!}');
	var generoRegion = JSON.parse('{!! $generoDistrito !!}');
	for(var i = 0; i < generoRegion.length; i++) {
		if(generoRegion[i].genero === 'M') {
			generoRegion[i].itemStyle = { color: '#E27D60' };
		}
		if(generoRegion[i].genero === 'F') {
			generoRegion[i].itemStyle = { color: '#E8A87C' };
		}
	}
	var dom = document.getElementById('container');
	var eChart = echarts.init(dom);
	var componentIndex;
	var dataIndex;
	//var app = {};
	option = null;
	option = {
		/*title : {
			text: '',
			subtext: '',
			x: 'center'
		},*/
		tooltip : {
			trigger: 'item',
			formatter: function(data) {
				if(componentIndex !== data.componentIndex || dataIndex !== data.dataIndex) {
					$('#mensaje').html('<div class="alert alert-info alert-dismissible" style="background-color: ' + data.color + ' !important; border-color: white; text-align: center;">' +
					//'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
					//'<h4>' +
					//'<i class="icon fa fa-info"></i> ' +
					//data.seriesName + ' ' +
					//'</h4>' +
					'<h4>' +
					data.name +
					'</h4>' +
					'<h4>' +
					'TOTAL: ' + data.value + ' PERSONA(S) ' + ' (' + data.percent + '%)' +
					'</h4>' +
					'</div>');
					//console.log(data);
				}

				componentIndex = data.componentIndex;
				dataIndex = data.dataIndex;
			}
			//formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		legend: {
			orient: 'horizontal',
			top: 'top',
			data: arr_distritos
		},
		series : [
		{
			name: 'REGIÓN',
			type: 'pie',
			selectedMode: 'single',
			radius: '65%',
			label: {
				normal: {
					show: false,
					position: 'inner'
				}
			},
			center: ['50%', '55%'],
			data: padronRegion,
			itemStyle: {
				emphasis: {
					shadowBlur: 10,
					shadowOffsetX: 0,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
		},
		{
			name: 'GENERO',
			type:'pie',
			radius: ['75%', '85%'],
			center: ['50%', '55%'],
			avoidLabelOverlap: true,
			label: {
				normal: {
					show: false,
					formatter:'{d}%'
				}
			},
			data: generoRegion
		}
		]
	};
	;
	if(option && typeof option === 'object') {
		eChart.setOption(option, true);

		eChart.on('dblclick', function(params) {
			if('id' in params.data) {
				window.location.href = '/bienestar/distritos/municipios/' + params.data.id;
			}
		});
	}
	/*$('#container').onresize = function() {
		eChart.resize();
	};*/
	eChartResize = function() {
		eChart.resize();
	};
	addResizeListener(dom, eChartResize);
	//removeResizeListener(dom, eChartResize);
</script>
@endpush