@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@if (auth()->check())
  @section('user-avatar', 'https://www.gravatar.com/avatar/' . md5(auth()->user()->email) . '?d=mm')
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('content-title', 'Taller de Silla de Ruedas')
@section('content-subtitle', 'Principal')

@push('head')
  @include('silladeruedas::layouts.masterCss');
  @yield('localCSS');
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Taller de silla de ruedas</a></li>
    @yield('li-breadcrumbs')
  </ol>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree" id="sidebar">
  <li class="treeview">
    <a href="#">
      <i class="fa fa-handshake-o"></i> <span>Peticiones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('silladeruedas.peticiones.index') }}"><i class="fa fa-list-alt"></i> Lista de peticiones</a></li>
      <li><a href="{{ route('silladeruedas.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li>
    </ul>
	</li>
	<li {{ ($ruta === 'inventario') ? 'class=active' : '' }}>
		<a href="{{ route('inventario') }}">
			<i class="fa fa-home"></i><span>Inventario de material</span>
		</a>
	</li>
</ul>
@endsection

{{--@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
	<li class="header active">Programa Bienestar</li>

	<li {{ ($ruta === 'inventario') ? 'class=active' : '' }}>
		<a href="{{ route('inventario') }}">
			<i class="fa fa-home"></i><span>Inventario de material</span>
		</a>
	</li>

	<li {{ ($ruta === 'almacen') ? 'class=active' : '' }}>
		<a href="{{ route('almacen') }}">
			<i class="fa fa-dashboard"></i><span>Fases de compostura</span>
		</a>
	</li>
	
  <li {{ ($ruta === 'proyectos') ? 'class=active' : '' }}>
		<a href="{{ route('proyectos') }}">
			<i class="fa fa-dashboard"></i><span>Equipo reparado</span>
		</a>
	</li>

  <li {{ ($ruta === 'reportes') ? 'class=active' : '' }}>
		<a href="{{ route('reportes') }}">
			<i class="fa fa-dashboard"></i><span>Reportes</span>
		</a>
	</li>

  <li {{ ($ruta === 'bienestar.home') ? 'class=active' : '' }}>
		<a href="{{ route('bienestar.home') }}">
			<i class="fa fa-dashboard"></i><span>Bienestar</span>
		</a>
	</li>

	<li {{ ($ruta === 'programas.index') ? 'class=active' : '' }}>
		<a href="{{route('afuncionales.programas.index')}}">
			<i class="fa fa-wheelchair"></i> <span>Programas</span>
			<span class="pull-right-container">            
			</span>
		</a>
	</li>

	<!--li class="{{ ($ruta === 'bienestar.solicitantes.index' || $ruta === 'bienestar.solicitantes.create') ? 'treeview active menu-open' : 'treeview' }}">
		<a href="#">
			<i class="fa fa-group"></i><span>Solicitantes</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li {{ ($ruta === 'bienestar.solicitantes.index') ? 'class=active' : '' }}><a href="{{ route('bienestar.solicitantes.index') }}"><i class="fa fa-list-ul"></i>Tabla de Solicitantes</a></li>
			@if(auth()->user()->hasRoles(['SUPERADMIN', 'COORDINADOR', 'OPERATIVO', 'APOYO OPERATIVO', 'ADMINISTRATIVO', 'LEGAL', 'AUXILIAR', 'FINANCIERO', 'MEDICO', 'CAPTURISTA']))
			<li {{ ($ruta === 'bienestar.solicitantes.create') ? 'class=active' : '' }}><a href="{{ route('bienestar.solicitantes.create') }}"><i class="fa fa-user-plus"></i>Nuevo Solicitante</a></li>
			@endif
		</ul>
	</li!-->

</ul>
@endsection--}}

@push('body')
  @include('silladeruedas::layouts.masterJs')
  @yield('localScripts')
@endpush