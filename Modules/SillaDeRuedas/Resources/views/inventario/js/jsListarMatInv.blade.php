<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>

{!! $dataTable->scripts() !!}

<script type="text/javascript">

var table = (function() 
{
    var tabla = undefined,
    btn_buscar = $('#btn_buscar'),
    search = $('#search');

    function init() {
      search.keypress(function(e) {
        if(e.which === 13) {
          tabla.DataTable().search(search.val()).draw();
        }
      });

      btn_buscar.on('click', function() {
        tabla.DataTable().search(search.val()).draw();
      });
    };

    function set_table(valor) {
      tabla = $(valor);
    };

    function get_table() {
      return tabla;
    };

    return {
      init: init,
      set_table: set_table,
      get_table: get_table
    };
  })();

  table.set_table($('#tblMateriales').dataTable());
  table.init();


$(document).ready(function()
{
  
});

var app = (function() 
{
  var bloqueo = false;

  function to_upper_case() 
  {
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
  };

  function set_bloqueo(valor) {
    bloqueo = valor;
  };

  function get_bloqueo() {
    return bloqueo;
  };

  function agregar_bloqueo_pagina() {
    $('form :input').change(function() {
      bloqueo = true;
    });

    window.onbeforeunload = function(e) {
      if(bloqueo)
        return '¿Estás seguro de salir?';
    };
  };

  return {
    to_upper_case: to_upper_case,
    set_bloqueo: set_bloqueo,
    agregar_bloqueo_pagina: agregar_bloqueo_pagina
  };
})();



var frmMaterial = (function() 
{
  var form = $('#form_material'),
  //unidadmedida = $('#unidadmedida_id'),
  action = form.attr('action'),
  method = form.attr('method');

  function init() 
  {
    form = $('#form_material'),
    //unidadmedida = $('#unidadmedida_id'),
    action = form.attr('action'),
    method = form.attr('method');
    /*unidadmedida.select2({
      language: 'es',
      placeholder: 'SELECCIONE UNA OPCIÓN'				
    }).change(function(event) {
      unidadmedida.valid();				
    });*/
    $('#material').keypress(function(e) {
      if(e.which === 13) 
        create_edit();
    });
    agregar_validacion();
    app.to_upper_case();
  };

  function agregar_validacion()
  {
    form.validate({
      rules: {
        material: {
          required: true
        }
      },
      messages: {
      }
    });
  };

  function create_edit()
  {
    if(form.valid()) 
    {
      block();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: action,
        type: method,
        data: form.serialize(),
        success: function(response) {
          material_create_edit_success(response, method);
        },
        error: function(response) {
          material_create_edit_error(response);
        }
      });

    }
  };

  return {
    init: init,
    create_edit: create_edit
  };

  
})();

function abrir_modal(modal, id) 
{
  if(modal === "modal-regismaterial")
  {
    block();
    var url = (id) ? '/silladeruedas/inventario/editmaterial/'+id : '/silladeruedas/inventario/addmaterial';
    
    var title = (id) ? 'Editar material' : 'Registrar material';
    $("#modal-regismaterial-title").text(title);
    $.get(url, function(data) {            
      $('#modal-body-regismaterial').html(data.html);
      frmMaterial.init();
      unblock();
      $('#modal-regismaterial').modal('show');
      
    });
  }else if(modal === "modal-documento"){
    frmMaterial.init();
    $("#form_documento")[0].reset();
        $('#modal-documento').modal('show');
  }else if(modal === "modal-producto"){
    frmMaterial.init();
    $("#form_producto")[0].reset();			
      $('#modal-producto').modal('show');
  }else
    $("#"+modal).modal('show');
}

function cerrar_modal(modal)
{
  $("#"+modal).modal("hide");
}

function material_delete(material_id) 
{
  swal({title:'¿Está seguro de eliminar el registro?', text:'¡El registro se eliminará de forma permanente!', type:'warning',
    showCancelButton: true, confirmButtonColor:'#3085d6', cancelButtonColor:'#d33', confirmButtonText: 'Sí, eliminar', cancelButtonText: 'No, regresar', reverseButtons: true
  }).then((result) => {
    if (result.value) {
      $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      });
      $.ajax({
        url: "{{ URL::to('silladeruedas/inventario/deletematerial') }}" + '/' + material_id,
        type: 'DELETE',
        //data: {_method: 'delete'},
        success: function(response) {
          table.get_table().DataTable().ajax.reload(null, false);
          swal({
            title: '¡Eliminado!',
            text: 'El registro ha sido eliminado.',
            type: 'success',
            timer: 3000
          });
        },
        error: function(response) {
        }
      });
    }
  });
}

function material_create_edit_success(response, method) 
{
  var mensaje = (method === "POST") ? 'registrado' : 'actualizado';

  swal({ title:'¡Correcto!', text:'Material ' + mensaje + '.', type: 'success', timer: 2000 });
  table.get_table().DataTable().ajax.reload(null, false);
  app.set_bloqueo(false);
  unblock();
  cerrar_modal('modal-regismaterial');
}

function material_create_edit_error(response) 
{
    unblock();
    if(response.status === 422) {
      swal({
        title: 'Error al registrar el producto.',				
        text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
        type: 'error',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Regresar',
        allowEscapeKey: false,
        allowOutsideClick: false
      });
    }
}

</script>