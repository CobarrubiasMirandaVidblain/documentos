<?php

Route::group(['middleware' => 'web', 'prefix' => 'silladeruedas', 'namespace' => 'Modules\SillaDeRuedas\Http\Controllers'], function()
{
    Route::resource('peticiones', 'PeticionController',['as'=>'silladeruedas','only'=>['index','show','update','destroy']]);   //Peticiones
    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'silladeruedas' ]);
    Route::resource('programas', 'ProgramaController',['as'=>'silladeruedas']); 

    Route::get('/', 'PeticionController@index');
    
    Route::get('inventario', 'InventarioController@index')->name('inventario');
    //Route::get('/inventario/lista', 'InventarioController@listaMateriales')->name('materiales.lista');
    Route::get('/inventario/addmaterial', 'InventarioController@ShowFrm_AddMaterial')->name('materiales.addmaterial');
    Route::get('/inventario/editmaterial/{id}', 'InventarioController@ShowFrm_EditMaterial')->name('materiales.editmaterial'); //Para mandar a llamar por nombre se hace: 'materiales.editmaterial[id]'
    Route::post('/inventario/storematerial', 'InventarioController@storeMaterial')->name('materiales.storematerial');
    Route::put('/inventario/updatematerial/{id}', 'InventarioController@updateMaterial')->name('materiales.updatematerial');
    Route::delete('/inventario/deletematerial/{id}', 'InventarioController@deleteMaterial')->name('materiales.deletematerial');
    //Route::resource('inventario', 'InventarioController', ['as' => 'silladeruedas']);
    //Route::resource('personafuncional', 'PersonaFuncionalController', ['only' => ['create','store','edit','update']]);
    //Route::post('/peticiones/{peticion}/programas/{programa}/beneficiarios/{beneficiario}/expediente', 'PeticionProgramaBeneficiarioController@expediente')->name('peticiones.programas.beneficiarios.expediente');
    //Route::get('/productos/entradas/edit/{id}', 'EntradaController@edit')->name('productos.entradas.edit');  
    //Route::get('/productos/entradas/show/{id}', 'EntradaController@show')->name('productos.entradas.show');
    //Route::get('/productos/salidas/create', 'SalidaController@create')->name('productos.salidas.create');
    //Route::delete('/productos/entradas/destroy/{id}', 'EntradaController@destroy')->name('productos.entradas.destroy');

    Route::get('almacen', 'AlmacenController@index')->name('almacen');
    //Route::get('almacen', 'AlmacenController@index')->name('almacen.index');
    
    Route::get('proyectos', 'ProyectosController@index')->name('proyectos');
    
    Route::get('reportes', 'ReportesController@index')->name('reportes');
});
