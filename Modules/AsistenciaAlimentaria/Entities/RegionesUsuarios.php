<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\AsistenciaAlimentaria\Entities\Base;

class RegionesUsuarios extends Base {
  protected $dates = ['deleted_at'];
  protected $table = 'alim_regiones_usuarios';
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  protected $fillable = ['region_id', 'access_usuario_id', 'usuario_id'];
  
  public function usuario() {
    return $this->belongsTo('App\Models\Usuario','id','access_usuario_id');
  }
  
  public function region(){
      return $this->belongsTo('App\Models\Region');
  }
}