<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValidacionEntrega extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_validacionesentregas';
  protected $dates = ['deleted_at'];
  protected $fillable=[
    'entregaproveedor_id',
    'oficiovalidacion_id',
    'validacion_aprobada',
    'usuario_id'
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function entregaproveedor()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\EntregaProveedor');
  }

  public function oficiovalidacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\OficioValidacion');
  }
}
