<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Modules\AsistenciaAlimentaria\Entities\Base;

class CostoPresentacionProducto extends Base {
  protected $table = 'alim_costos_productos';
  
  protected $fillable = [
    'presentacion_producto_id',
    'costo',
    'usuario_id'
  ];
  
  public function usuario() {
    return $this->belongsTo('App\Models\Usuario');
  }
  public function presentacionProducto(){
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\PresentacionProducto');
  }
  public function detalledotacion(){
      return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\DetalleDotacion','costo_producto_id','id');
  }
}