<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Convenio extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_convenios';
  protected $dates = ['deleted_at'];
  protected $fillable=['programacion_id','tiene_acta_aceptacion','fecha_convenio','tipoconvenio_id','tiene_acta_comite','tiene_acta_contraloria','ubicacion_fisica_archivo','ubicacion_digital_archivo','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
	}
  
	public function tipoconvenio(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\TipoConvenio'); 
  }
  
  public function programacion(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Programacion');
  }
  
}
