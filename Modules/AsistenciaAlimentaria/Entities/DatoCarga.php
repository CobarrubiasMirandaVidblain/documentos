<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DatoCarga extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_datoscarga';
  protected $dates = ['deleted_at'];
  protected $fillable=[
    'choferproveedor_id',
    'ruta',
    'fecha',
    'placas',
    'usuario_id'
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function chofer(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\ChoferProveedor','choferproveedor_id','id');
  }

  public function requisicionesDatoCarga(){
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\RequisicionDatoCarga', 'datocarga_id', 'id');
  }
 /*  public function entregaproveedor(){
      return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\EntregaProveedor','id','requisicion_datocarga_id');
  } */

  public function getFormatFecha(){
    return (\DateTime::createFromFormat('Y-m-d', $this->fecha))->format('d/m/Y');
  }
}
