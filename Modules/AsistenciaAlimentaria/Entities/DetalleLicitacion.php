<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleLicitacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_detalle_licitacion';
  protected $dates = ['deleted_at'];
  protected $fillable=['licitacion_id','dotacion_id','cantidad'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function licitacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Licitacion');
  }
 
  public function dotacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\dotacion');
  }
}
