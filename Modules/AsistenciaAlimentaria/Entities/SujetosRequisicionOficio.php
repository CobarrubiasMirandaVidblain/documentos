<?php
namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SujetosRequisicionOficio extends Model{
  use SoftDeletes;

  protected $table = 'alim_sujetosrequisicionoficio';
  protected $dates = ['deleted_at'];
  protected $fillable=['programacio0n_id','programa_id','fecha_solicitud','numero_oficio_solicitud','numero_oficio_requisicion','pertence_decsegego','fehca_oficio_requisicion','bimestre','numero_beneficiarios','se_cobra','quien_recibe','cargo_id','observaciones'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function cargo()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Cargo');
  }

  public function programa()
  {
      return $this->belongsTo('App\Models\Programa');
  }

  public function programacion()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Programacion');
  }
}