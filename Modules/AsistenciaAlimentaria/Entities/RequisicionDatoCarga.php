<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequisicionDatoCarga extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_requisiciones_datoscarga';
  protected $dates = ['deleted_at'];
  protected $fillable=[
    'programacion_requisicion_id',
    'datocarga_id',
    'usuario_id'
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function programacionrequisicion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion','programacion_requisicion_id','id');
  }

  public function carga()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\DatoCarga','datocarga_id','id');
  }

  public function entregaproveedor(){
    return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\EntregaProveedor','requisicion_datocarga_id','id');
  }

}
