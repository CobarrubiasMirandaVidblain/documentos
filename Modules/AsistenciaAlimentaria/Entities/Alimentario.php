<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Modules\AsistenciaAlimentaria\Entities\Base;

class Alimentario extends Base {
  protected $table = 'alim_alimentarios';
  
  protected $fillable = ['localidad_id', 'municipio_id', 'sublocalidad', 'latitud', 'longitud', 'foliodif', 'programa_id', 'essedesol', 'usuario_id'];
  
  public function usuario() {
    return $this->belongsTo('App\Models\Usuario');
  }
  
  public function programa() {
    return $this->belongsTo('App\Models\Programa');
  }
  
  public function municipio() {
    return $this->belongsTo('App\Models\Municipio');
  }
  
  public function localidad() {
    return $this->belongsTo('App\Models\Localidad');
  }

  public function programacion() {
    return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\Programacion');
  }

  public function desayuno() {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Desayuno','id','alimentario_id');
  }

  public function sujeto() {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Sujeto','id','alimentario_id');
  }

  public function convenio() {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Convenio');
  }
}