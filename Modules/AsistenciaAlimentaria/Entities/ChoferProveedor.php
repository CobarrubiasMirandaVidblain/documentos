<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChoferProveedor extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_choferesproveedor';
  protected $dates = ['deleted_at'];
  protected $fillable=[
      'modulo_proveedor_id',
      'nombre',
      'primer_apellido',
      'segundo_apellido',
      'usuario_id'
    ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function moduloproveedor()
  {
    return $this->belongsTo('App\Models\ModuloProveedor');
  }

  public function getNombreCompleto(){
    if($this->segundo_apellido)
      return "$this->nombre $this->primer_apellido $this->segundo_apellido";
    return "$this->nombre $this->primer_apellido";
  }
}
