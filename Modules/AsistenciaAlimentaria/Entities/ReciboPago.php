<?php

namespace Modules\AsistenciaAlimentaria\Entities;

class ReciboPago extends Base{
  protected $table = 'alim_pagosrecibos';
  protected $dates = ['deleted_at'];
  protected $fillable = [
    'recibo_id',
    'folio_caja',
    'fecha_pago',
    'importe_total',
    'total_dotaciones',
    'texto_pago',
    'iscancelado',
    'nombre_usuario',
    'motivo_cancelacion',
    'usuario_id',
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  public function recibo()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Recibo');
  }
}
