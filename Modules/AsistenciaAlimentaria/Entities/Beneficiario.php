<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Beneficiario extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_beneficiarios';
  protected $dates = ['deleted_at'];
  protected $fillable=['programacion_id','persona_programa_id','esta_embarazada_lactando','tipoaccion_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  protected $with = ['personaprograma'];
  
  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }

  public function persona()
  {
      return $this->belongsTo('App\Models\Persona');
  }

  public function personaprograma()
  {
      return $this->belongsTo('App\Models\PersonasPrograma','persona_programa_id','id');
  }

  public function programacion()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Programacion','programacion_id','id');
  }

  public function tipoaccion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\TipoAccion');
  }
}
