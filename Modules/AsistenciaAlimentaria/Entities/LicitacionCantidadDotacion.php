<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LicitacionCantidadDotacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_cantidadesdotaciones_licitaciones';
  protected $dates = ['deleted_at'];
  protected $fillable=[
    'licitacion_id',
    'dotacion_id',
    'cantidad',
    'usuario_id'
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
 
  public function licitacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Licitacion');
  }

  public function dotacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Dotacion','dotacion_id','id');
  }
}
