<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostoProducto extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_costo_producto';
  protected $dates = ['deleted_at'];
  protected $fillable=['presentacion_producto_id','costo'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function presentacionproducto()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\PresentacionProducto');
  }
}
