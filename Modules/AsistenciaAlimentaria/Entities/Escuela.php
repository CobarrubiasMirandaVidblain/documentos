<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Escuela extends Model{
  use SoftDeletes;
  
  protected $table = 'cat_escuelas';
  protected $dates = ['deleted_at'];
  protected $fillable=['nombre','clave','nivel_id','turno_id','subsistema_id','padre_id','extension','municipio_id','localidad_id','latitud','longitud',''];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function localidad(){
    return $this->belongsTo('App\Models\Localidad');
	}
  
	public function municipio(){
    return $this->belongsTo('App\Models\Municipio');
  }
  
  public function nivel(){
    return $this->belongsTo('App\Models\NivelEscuela');
	}
	
	public function padre(){
		return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Escuela','padre_id','id');
	}
  
}