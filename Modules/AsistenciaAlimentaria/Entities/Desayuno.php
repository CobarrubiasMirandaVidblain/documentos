<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Desayuno extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_desayunos';
  protected $dates = ['deleted_at'];
  protected $fillable=['alimentario_id','escuela_id','tipodistribucion_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
	}
  
	public function tipoDistribucion_id(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Alimentario');
  }
  
  public function escuela(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Escuela');
  }
  
  public function estado(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Estado');
  }
  
  public function alimentarios(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Alimentario','alimentario_id','id');
  }
  
}
