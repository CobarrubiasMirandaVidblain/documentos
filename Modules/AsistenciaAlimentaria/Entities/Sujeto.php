<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sujeto extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_sujetos';
  protected $dates = ['deleted_at'];
  protected $fillable=['nombre_asociacion','oficio','alimentario_id', 'dedesarrollo_programa_id', 'usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }

  public function alimentario()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Alimentario');
  }

}
