<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoAccion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_tiposaccion';
  protected $dates = ['deleted_at'];
  protected $fillable=['nombre','programa_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function programa(){
    return $this->belongsTo('App\Models\Programa');
  }

  public function dotaciones()
  {
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\Dotacion');
  }
  
}
