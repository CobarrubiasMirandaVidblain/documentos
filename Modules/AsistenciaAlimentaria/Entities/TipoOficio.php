<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoOficio extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_tipo_oficio';
  protected $dates = ['deleted_at'];
  protected $fillable=['nombre'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
 
  public function oficioautorizacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\OficioAutorizacion');
  }
}
