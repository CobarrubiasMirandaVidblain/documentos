<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequisicionCantidadDotacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_cantidadesdotaciones_requisiciones';
  protected $dates = ['deleted_at'];
  protected $fillable=[
    'programacion_requisicion_id',
    'dotacion_id',
    'cantidad_dotaciones',
    'cantidad_beneficiarios',
    'usuario_id'
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
 
  public function requisicionprogramacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion','programacion_requisicion_id','id');
  }

  public function dotacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Dotacion','dotacion_id','id');
  }
}
