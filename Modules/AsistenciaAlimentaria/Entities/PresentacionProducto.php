<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PresentacionProducto extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_presentacion_producto';
  protected $dates = ['deleted_at'];
  protected $fillable=['producto_id','presentacion_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function producto()
  {
    return $this->belongsTo('App\Models\Producto');
  }

  public function presentacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Presentacion');
  }
}
