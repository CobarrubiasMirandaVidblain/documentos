<?php
namespace Modules\AsistenciaAlimentaria\Http\Controllers\Sujetos;

use App\Models\Programa;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Sujeto;
use Modules\AsistenciaAlimentaria\DataTables\Cocinas;
use Modules\AsistenciaAlimentaria\Entities\Alimentario;

use Modules\AsistenciaAlimentaria\Entities\Programacion;

use Modules\AsistenciaAlimentaria\Entities\Financiamiento;
use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;

class SujetoController extends Controller {
  use AlimentariosTrait;
  public function __construct()
  {
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
  }

  public function index(Cocinas $tabla) {
    return $tabla->with(['tipo'=>'lista','programa_id'=>config('asistenciaalimentaria.sujetosId')])
                  ->render('asistenciaalimentaria::alimentarios.index',['programa'=>Programa::find(config('asistenciaalimentaria.sujetosId'))]);
  }

  public function create(Request $request) {
    $this->acceso(['SUPERADMIN']);
    return view('asistenciaalimentaria::alimentarios.create')
          ->with('programa',config('asistenciaalimentaria.sujetosId'))
          ->with('alimentario', $request->alim ? Alimentario::find($request->alim) : null);;
  }

  public function store(Request $request) {
    //return response()->json(array($request->only(['nombre_asociacion','oficio'])));
    try {
      //validamos que no exista una cocina en esa (sub)localidad y municipio
      $conta = Alimentario::join('alim_sujetos','alim_sujetos.alimentario_id','alim_alimentarios.id')
      ->where([
          ['alim_alimentarios.municipio_id',$request->input('municipio_id')],
          ['alim_alimentarios.localidad_id',$request->input('localidad_id')],
          ['alim_alimentarios.programa_id',config('asistenciaalimentaria.sujetosId')],
          ['alim_sujetos.nombre_asociacion',$request->input('nombre_asociacion')]
      ])
      ->where(function($query) use ($request){
        if($request->input('sublocalidad'))
          $query->where('sublocalidad',$request->input('sublocalidad'));
        else
          $query->whereNull('sublocalidad');
        })->get();
      if(count($conta)>0){
        $cocina = $conta[0];
        return new JsonResponse(['message'=>"el sujeto $cocina->foliodif se encuentra en este mismo, municipio, localidad, sublocalidad de ".$cocina->programa->nombre],409);
      }
      //Separamos los datos de la cocina, programacion y sujeto
      $datosAlimentario = $request->except(['financiamiento_id','solicitud_id','esdesarrollo']); 
      $datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
      $datosSujeto = $request->only(['nombre_asociacion','oficio']);
      //Obtenemos el id de la region y generamos un folio en base al numero de cocinas en la region
      $prefolio = Municipio::find($datosAlimentario['municipio_id'])->distrito->region_id;
      $folio = $prefolio.(Alimentario::join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')->where('cat_distritos.region_id',$prefolio)->count() + 1).substr(date('Y'),-2);;
      //Obtenemos al usuario logueado
      $datosAlimentario['usuario_id'] = auth()->user()->id;
      $datosAlimentario['foliodif'] = $folio;
      //empezamos a escribir en la bd
      DB::beginTransaction();

			$alimentario = Alimentario::Create($datosAlimentario);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_alimentarios',
				'registro' => $alimentario->id . '',
				'campos' => json_encode($datosAlimentario) . '',
				'metodo' => request()->method(),
			]);
      //creamos sujeto
      $datosSujeto['usuario_id'] = $datosAlimentario['usuario_id'];
      $datosSujeto['alimentario_id'] = $alimentario->id;
      $datosSujeto['esdesarrollo'] = $request->input('esdesarrollo') ? 1 : 0;

			$suj = Sujeto::create($datosSujeto);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_sujetos',
				'registro' => $suj->id . '',
				'campos' => json_encode($datosProgramacion) . '',
				'metodo' => request()->method(),
			]);
      //completamos los datos de la programacion
      $datosProgramacion['alimentario_id'] = $alimentario->id;
      $datosProgramacion['anios_programa_id'] = Programa::find($datosAlimentario['programa_id'])->aniosprogramas->where('ejercicio.anio',config('asistenciaalimentaria.anioActual'))->first()->id;
      $datosProgramacion['estado_programa_id'] = Programa::find($datosAlimentario['programa_id'])->estadoprogramas->where('estado.nombre','ALTA')->first()->id;
      $datosProgramacion['usuario_id'] = $datosAlimentario['usuario_id'];
      
      $prog = Programacion::create($datosProgramacion);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_programacion',
				'registro' => $prog->id . '',
				'campos' => json_encode($datosProgramacion) . '',
				'metodo' => request()->method(),
			]);
      //guardamos los cambios en la bd
      DB::commit();
      return response()->json(['success' => true, 'folio' => $alimentario->id]);
    }catch(\Exeption $e) {
      DB::rollBack();
      return new JsonResponse(['message'=>'Algo no salio bien '],500);
    }
  }

  public function update($alimentario_id,Request $request){
    try {
      //validamos que no exista una cocina en esa (sub)localidad y municipio
      $conta = Alimentario::where([['municipio_id',$request->input('municipio_id')],['localidad_id',$request->input('localidad_id')],['programa_id',config('asistenciaalimentaria.sujetosId')]])
                          ->where('id','!=',$alimentario_id)
      ->where(function($query) use ($request){
        if($request->input('sublocalidad'))
          $query->where('sublocalidad',$request->input('sublocalidad'));
        else
          $query->whereNull('sublocalidad');
        })->get();
      if(count($conta)>0){
        $cocina = $conta[0];
        return new JsonResponse(['message'=>"la cocina $cocina->foliodif se encuentra en este mismo, municipio, localidad, sublocalidad de ".$cocina->programa->nombre],409);
      }
      $conta = Sujeto::where('nombre_asociacion',$request->input('nombre_asociacion'))->get();
      if(count($conta)>0){
        $sujeto = $conta[0];
        return new JsonResponse(['message'=>"la asociación/institución $sujeto->id ya se encuentra registrada"],409);
      }
      //Separamos los datos de la cocina y programacion
      $datosAlimentario = $request->except(['financiamiento_id','solicitud_id','escuela_id']);
			$datosAlimentario['essedesol'] = $request->input('essedesol') ? 1 : 0;
			$datosAlimentario['sublocalidad'] = $datosAlimentario['sublocalidad'] ?? null;
      $datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
      //empezamos a escribir en la bd
      DB::beginTransaction();
      $prog = Programacion::where('alimentario_id',$alimentario_id)->first();
			$prog->update($datosProgramacion);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_programacion',
				'registro' => $prog->id . '',
				'campos' => json_encode($datosProgramacion) . '',
				'metodo' => request()->method(),
			]);
      //guardamos los cambios en la bd
      $alimentario = Alimentario::find($alimentario_id);
			$alimentario->update($datosAlimentario);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_alimentario',
				'registro' => $alimentario->id . '',
				'campos' => json_encode($datosAlimentario) . '',
				'metodo' => request()->method(),
			]);
      //creamos sujeto
      $datosSujeto = $request->only(['nombre_asociacion','oficio']);
      $datosSujeto['esdesarrollo'] = $request->input('esdesarrollo') ? 1 : 0;
			$suj = Sujeto::where('alimentario_id',$alimentario->id)->first();
			$suj->update($datosSujeto);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_alimentario',
				'registro' => $suj->id . '',
				'campos' => json_encode($datosSujeto) . '',
				'metodo' => request()->method(),
			]);
      DB::commit();
      return response()->json(['success' => true]);
    }catch(\Exeption $e) {
      DB::rollBack();
      return new JsonResponse(['message'=>'Algo no salio bien'],500);
    }
  }
}