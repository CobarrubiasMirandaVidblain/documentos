<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Sujetos;

use Modules\AsistenciaAlimentaria\Http\Controllers\OficioBaseController;

class OficioController extends OficioBaseController{
  
  public function __construct() {
    parent::__construct(config('asistenciaalimentaria.sujetosId'));
  }

}