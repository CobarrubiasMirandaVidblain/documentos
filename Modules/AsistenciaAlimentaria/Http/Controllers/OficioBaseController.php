<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsistenciaAlimentaria\DataTables\OficiosValidacion;
use Illuminate\Support\Facades\DB;
use App\Models\Ejercicio;
use App\Models\Programa;

use View;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Modules\AsistenciaAlimentaria\Entities\OficioValidacion;

class OficioBaseController extends Controller
{
    private $programa;

    function __construct($programa=null){
        $this->middleware(['auth', 'authorized']);
        //$this->middleware('rolModuleV2:alimentarios,VALIDADOR');
        if($programa)
            $this->programa = Programa::find($programa);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(OficiosValidacion $tabla)
    {
        return $tabla->with('programa_id',$this->programa->id)->render('asistenciaalimentaria::requisicion.validacion.oficios');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        return view::make('asistenciaalimentaria::modal',[
            'tipo'=>$request->get('id'),
            'vista'=>'asistenciaalimentaria::requisicion.validacion.forms.create_oficio',
            'programa'=>$this->programa
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
                $data = $request->only(['num_oficio']);
                $data['ejercicio_id'] = Ejercicio::where('anio',$request['anio'])->first()->id;
                $data['fecha_oficio'] = Carbon::createFromFormat('d/m/Y',$request['fecha'])->format('Y-m-d');
                $data['programa_id']  = $this->programa->id;
                $data['usuario_id']   = auth()->user()->id;
                $of = OficioValidacion::create($data);
                auth()->user()->bitacora(request(), [
                    'tabla' => 'alim_oficiosvalidacion',
                    'registro' => $of->id . '',
                    'campos' => json_encode($data) . '',
                    'metodo' => request()->method()
                ]);
            DB::commit();
            return new JsonResponse(['message'=>"Número de oficio #{$request->input('num_oficio')} agregado con éxito"],200);
        }catch (Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message'=>$e->getmessage()],500);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('asistenciaalimentaria::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
                OficioValidacion::destroy($id);
            DB::commit();
            return new JsonResponse(['Se eliminó correctamente.'], 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return new JsonResponse([$th->getMessage()], 500);
        }
    }
}
