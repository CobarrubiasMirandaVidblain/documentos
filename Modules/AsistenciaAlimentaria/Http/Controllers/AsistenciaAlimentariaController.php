<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use App\Models\Region;
use App\Models\Programa;

use App\Models\Ejercicio;
use App\Models\Indicador;
use App\Models\Municipio;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AsistenciaAlimentaria\DataTables\Usuarios;
use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\Financiamiento;
use Modules\AsistenciaAlimentaria\Entities\TipoDistribucion;
use Modules\AsistenciaAlimentaria\DataTables\Proveedor\Requisiciones;

class AsistenciaAlimentariaController extends Controller{
  /**
  * Create a new controller instance.
  * @return void
  */
  public function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN',['only' => ['index']]);
  }
  public function home(Requisiciones $tabla){
    if(auth()->user()->hasRoles(['PROVEEDOR']) && !auth()->user()->hasRoles(['SUPERADMIN']))
      return redirect()->route('alimentarios.proveedor.requisiciones.index',config('asistenciaalimentaria.anioActual'));

    $munXregion = [];
    $munXindicador = [];
    $puntos = [];
    foreach (Region::All() as $region) {
      $munXregion[$region->nombre]=$region->municipios->map(function ($e){ return [$e->id,$e->nombre,$e->latitud,$e->longitud]; });
    }
    $puntos['cocinas'] = Alimentario::where('programa_id',config('asistenciaalimentaria.ccncId'))->whereHas('programacion',function($q){
        $q->whereHas('estadoprograma',function($qu){
            $qu->whereHas('estado',function($que){
                $que->where('nombre','like','%activa%')
                ->where('nombre','!=','ACTIVA - PENDIENTE');
            });
        });
    })->select(['id','latitud','longitud'])->get();
    $puntos['sujetos'] = Alimentario::where('programa_id',config('asistenciaalimentaria.sujetosId'))->whereHas('programacion',function($q){
        $q->whereHas('estadoprograma',function($qu){
            $qu->whereHas('estado',function($que){
                $que->where('nombre','like','%activa%')
                ->where('nombre','!=','ACTIVA - PENDIENTE');
            });
        });
    })->select(['id','latitud','longitud'])->get();
    $puntos['escuelas'] = Alimentario::where('programa_id',config('asistenciaalimentaria.desayunosId'))->whereHas('programacion',function($q){
        $q->whereHas('estadoprograma',function($qu){
            $qu->whereHas('estado',function($que){
                $que->where('nombre','like','%activa%')
                ->where('nombre','!=','ACTIVA - PENDIENTE');
            });
        });
    })->select(['id','latitud','longitud'])->get();
    return view('asistenciaalimentaria::mapa',['regiones'=>json_encode($munXregion),'puntos'=>json_encode($puntos)]);
  }

  public function show(Request $request, $alimentario_id){
    $alimentario = Alimentario::select(['id', 'foliodif', 'programa_id', 'localidad_id', 'municipio_id'])->whereId($alimentario_id)
    ->with([
      'localidad:id,nombre',
      'municipio:id,nombre',
      'municipio.indicadores'=>function($q){
        $q->select('id','indicador_id','municipio_id','poblacion','porcentaje')->wherehas('indicador',function($qu){
          $qu->whereIn('nombre',['POBREZA','POBREZA EXTREMA','CARENCIA POR ACCESO A LA ALIMENTACIÓN','POBLACIÓN TOTAL POR MUNICIPIO']);
        });},
      'municipio.indicadores.indicador',
      'programacion:id,alimentario_id,anios_programa_id',
      'programacion.anioprograma:id,ejercicio_id',
      'programacion.programacionrequisiciones:id,programacion_id,created_at',
      'programacion.programacionrequisiciones.requisicioncantidaddotacion:programacion_requisicion_id,dotacion_id,cantidad_dotaciones,cantidad_beneficiarios',
      'programacion.programacionrequisiciones.requisicioncantidaddotacion.dotacion:id,cuota_recuperacion',
      'programacion.programacionrequisiciones.requisicioncantidaddotacion.dotacion.detallesdotacion:costo_producto_id,dotacion_id,cantidad',
      'programacion.programacionrequisiciones.requisicioncantidaddotacion.dotacion.detallesdotacion.costoproducto:id,costo'
      ])->first();
      // return $alimentario;
    return view::make('asistenciaalimentaria::modal',
      [
        'vista'=>'asistenciaalimentaria::alimentarios.detalles',
        'alimentario'=> $alimentario,
        'tipo'=>$request->get('id')
      ]
    );
  }

  public function create(Request $request) {
    return view('asistenciaalimentaria::alimentarios.create')
            ->with('financiamientos',Financiamiento::all())
            ->with('distribuciones',TipoDistribucion::all())
            ->with('prefix',$request->input('prefix',''));
  }
}
