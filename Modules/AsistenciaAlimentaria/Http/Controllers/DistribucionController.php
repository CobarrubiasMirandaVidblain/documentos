<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Modules\AsistenciaAlimentaria\DataTables\Distribuciones;

use View;

class DistribucionController extends Controller{
  
  function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN');
  }

  public function index($prog_id, Distribuciones $tabla) {
    return $tabla->with('programa_id',$prog_id)->render('asistenciaalimentaria::requisicion.distribuciones');
  }
}