<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use Carbon\Carbon;
use App\Models\Rol;
use App\Models\Region;
use App\Models\UsuariosRol;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\RegionesUsuarios;

class UsuarioController extends Controller{
  /**
   * Display a listing of the resource.
   * @return Response
   * Retorna el dataTable de la programación actual
   */
  public function index(Request $request)
  {
    return Rol::join('cat_modulos','cat_modulos.id','cat_roles.modulo_id')
              ->where('cat_roles.nombre','like','%'.$request['search'].'%')
              ->where('cat_modulos.id',config('asistenciaalimentaria.moduloId'))
              ->get(['cat_roles.id','cat_roles.nombre']);
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create(Request $request)
  {
    return Region::where('nombre','like','%'.$request['search'].'%')->get(['id','nombre']);
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   * Agrega un nuevo de beneficiario
   */
  public function store(Request $request)
  {
  }

  /**
   * Show the specified resource.
   * @return Response
   */
  public function show($usuario,Request $request)
  {
    return view::make('asistenciaalimentaria::modal')
                ->with([
                    'vista' => 'asistenciaalimentaria::usuarios.create',
                    'usuario'=>$usuario,
                    'tipo'=>$request->get('id')
                ])
                ->render();
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit()
  {
      return view('asistenciaalimentaria::edit');
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   * Actualiza beneficiario
   */
  public function update($usuario,Request $request)
  {
    try {
      DB::beginTransaction();
        $usuario_update = UsuariosRol::where('usuario_id',$usuario)->where('modulo_id',config('asistenciaalimentaria.moduloId'))->first();
        $usuario_update->update($request->except('region_id'));
        auth()->user()->bitacora(request(), [
          'tabla' => 'usuarios_roles',
          'registro' => $usuario_update->id . '',
          'campos' => json_encode($request->except('region_id')) . '',
          'metodo' => request()->method()
        ]);
        $ru = RegionesUsuarios::where('access_usuario_id',$usuario)->first();
        if($ru)
          $ru->delete();
        if($request->region_id) {
          $data['access_usuario_id'] = $usuario;
          $data['region_id'] = $request->region_id;
          $data['usuario_id'] = auth()->user()->id;
          RegionesUsuarios::create($data);
        }
      DB::commit();
      return response()->json(array('success' => true));
    } catch(\Exception $e) {
        DB::rollBack();
        return new JsonResponse(['message0'=>$e->getMessage()],500);
    }
  }

  /**
   * Remove the specified resource from storage.
   * @return Response
   */
  public function destroy($programacion,$beneficiario)
  {
    
  }
}
