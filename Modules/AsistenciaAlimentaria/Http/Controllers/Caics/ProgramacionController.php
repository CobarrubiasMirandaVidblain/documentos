<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Caics;

use Modules\AsistenciaAlimentaria\Http\Controllers\ProgramacionBaseController;

class ProgramacionController extends ProgramacionBaseController{
  
  public function __construct() {
    parent::__construct(config('asistenciaalimentaria.lecheId'));
  }

}

