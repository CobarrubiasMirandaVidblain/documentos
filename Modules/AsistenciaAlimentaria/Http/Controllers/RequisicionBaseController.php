<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use Modules\AsistenciaAlimentaria\Entities\RequisicionCantidadDotacion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\DataTables\Requisiciones;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\TipoEntrega;
use App\Models\EstadoPrograma;
use App\Models\Programa;

use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;
use View;

class RequisicionBaseController extends Controller {
    use AlimentariosTrait;
    
    private $programa;
    
    function __construct($programa=null){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
        if($programa)
            $this->programa = Programa::find($programa);
    }
    
    public function index(Requisiciones $tabla){
        return $tabla->with('programa_id',$this->programa->id)->render('asistenciaalimentaria::requisicion.index',['programa'=>$this->programa]);
    }
    
    public function create(Request $request){
        $this->acceso(['SUPERADMIN']);
        return view::make('asistenciaalimentaria::modal',[
            'tipo'=>$request->get('id'),
            'vista'=>'asistenciaalimentaria::requisicion.create',
            'programa_id'=>$this->programa->id,
            'lg'=>true
        ]);
    }

    public function edit(Request $request){
        $this->acceso(['SUPERADMIN']);
        return view::make('asistenciaalimentaria::modal',[
            'tipo'=>$request->get('id'),
            'vista'=>'asistenciaalimentaria::requisicion.create',
            'programa_id'=>$this->programa->id,
            'requisicion'=>Requisicion::find($request->requisicion_id),
            'lg'=>true
        ]);
    }
        
    public function store(Request $request){
        $this->acceso(['SUPERADMIN']);
        try {
            DB::beginTransaction();
            //Bimestre y entrega ya existen
            $existeEntrega = Requisicion::where([
                ['num_bimestre', $request->input('num_bimestre')],
                ['num_entrega' , $request->input('num_entrega')],
                ['programa_id' , $this->programa->id]
            ])->first();
            if(!$existeEntrega) {
                $requisicion = Requisicion::create([
                    'programa_id' => $this->programa->id,
                    'licitacion_id' => $request->input('licitacion_id'),
                    'num_bimestre' => $request->input('num_bimestre'),
                    'num_entrega' => $request->input('num_entrega'),
                    'num_oficio' => $request->input('num_oficio'),
                    'fecha_oficio' => Carbon::createFromFormat('d/m/Y',$request->input('fecha_oficio'))->format('Y-m-d'),
                    'especial' => $request->input('especial'),
                    'tipo_entrega_id' => $request->input('tipo_entrega_id'),
                    'estado_programa_id' => EstadoPrograma::with('estado')->where('programa_id',$this->programa->id)->wherehas('estado',function($q){
                        $q->where('nombre','REVISION');
                    })->first()->id,
                    'periodo' => $request->input('periodo'),
                    'observaciones' => $request->input('observaciones'),
                    'usuario_id' => auth()->user()->id,
                ]);
                auth()->user()->bitacora(request(), [
                    'tabla' => 'alim_requisiciones',
                    'registro' => $requisicion->id . '',
                    'campos' => json_encode($request->all()) . '',
                    'metodo' => request()->method()
                ]);
            } else {
                return new JsonResponse(['message'=>"Entrega {$request->input('num_entrega')} duplicada en el bimestre {$request->input('num_bimestre')}"],409);
            }
            DB::commit();
            return new JsonResponse(['message'=>"Requisición con oficio #{$request->input('num_oficio')} agregada con éxito"],200);
        }catch (Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message'=>$e->getmessage()],500);
        }
    }

    public function update($requisicion_id,Request $request){
        $this->acceso(['SUPERADMIN']);
        try {
            DB::beginTransaction();
                if($request->has('estado')) {
                    $req = Requisicion::find($requisicion_id);
                    $req->estado_programa_id = EstadoPrograma::whereHas('estado',function($query) use ($request) {
                                                $query->where('nombre',$request->input('estado'));
                                            })
                                            ->where('programa_id',$this->programa->id)->first()->id;
                    $data['num_oficio'] = $req->num_oficio;
                    $req->save();
                } else {
                    $data = $request->all();
                    $data['fecha_oficio'] = Carbon::createFromFormat('d/m/Y',$request->input('fecha_oficio'))->format('Y-m-d');
                    Requisicion::find($requisicion_id)->update($data);
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'alim_requisiciones',
                        'registro' => $requisicion_id . '',
                        'campos' => json_encode($data) . '',
                        'metodo' => request()->method()
                    ]);
                }
            DB::commit();
            return new JSONResponse(['message'=>"Requisición con oficio #{$data['num_oficio']} actualizada con éxito"],200);
        }catch (Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message'=>$e->getmessage()],500);
        }
    }
    
    public function destroy($id){
        try{
            DB::beginTransaction();
                $progReq = RequisicionProgramacion::where('requisicion_id',$id);
                $toDelete = $progReq->get()->pluck('id');
                RequisicionCantidadDotacion::whereIn('programacion_requisicion_id',$toDelete)->delete();
                $progReq->delete();
                Requisicion::find($id)->delete();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'alim_requisiciones',
                    'registro' => $id . '',
                    'campos' => '',
                    'metodo' => request()->method()
                ]);
            DB::commit();
            return new JSONResponse(['message'=>'Se ha eliminado la requisición con éxito'],200);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['message'=>$e->getmessage()],500);
        }
    }
    
    public function tiposentrega(Request $request) {
        $entregas = TipoEntrega::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
        ->take(10)
        ->get()
        ->toArray();
        
        return response()->json($entregas);
    }
}