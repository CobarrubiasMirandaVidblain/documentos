<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

use Modules\AsistenciaAlimentaria\Entities\ChoferProveedor;

class ConductorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('asistenciaalimentaria::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('asistenciaalimentaria::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('asistenciaalimentaria::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function search(Request $request) {
        $choferes = ChoferProveedor::whereRaw("concat_ws(' ',nombre,primer_apellido,segundo_apellido) like \"%{$request->input('search')}%\"")
                        ->take(10)
                        ->get(['id', DB::raw('concat_ws(" ",nombre,primer_apellido,segundo_apellido) as label'), 'nombre', 'primer_apellido', 'segundo_apellido'])
                        ->toArray();
        return new JsonResponse($choferes);
    }

    public function find($id) {
        return new JsonResponse(['persona'=>ChoferProveedor::find($id)]);
    }
}
