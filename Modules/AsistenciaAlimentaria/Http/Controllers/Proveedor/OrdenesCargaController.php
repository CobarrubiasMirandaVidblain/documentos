<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

use App\Models\EstadoPrograma;
use Modules\AsistenciaAlimentaria\DataTables\Proveedor\OrdenesCarga;

use Modules\AsistenciaAlimentaria\Entities\DatoCarga;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Carbon\Carbon;
class OrdenesCargaController extends Controller{
    public function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,PROVEEDOR');
    }
    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index($pro_id,OrdenesCarga $tabla){
        return $tabla->render('asistenciaalimentaria::proveedor.ordenescarga');
    }
    /**
     * Cancela una orden de carga, colocando en canceladas las cocinas dentro de esta.
     **/
    public function destroy($pro_id, $ordenCargaId){
        $datoCarga = DatoCarga::findorfail($ordenCargaId);
        RequisicionProgramacion::whereHas('requisicionDatoCarga',function($q) use($datoCarga){
            $q->where('datocarga_id',$datoCarga->id);
        })->update(['estado_programa_id'=>EstadoPrograma::whereHas('estado',function($qu){$qu->where('nombre','CANCELADA');})->first()->id]);
        auth()->user()->bitacora(request(), [
            'tabla' => 'alim_datoscargas',
            'registro' => $datoCarga->id . '',
            'campos' => "{deleted_at:".Carbon::now()."}",
            'metodo' => "DELETE"
        ]);
        $datoCarga->delete();
        auth()->user()->bitacora(request(), [
            'tabla' => 'alim_programacion_requisicion',
            'registro' => 0,
            'comentario' => RequisicionProgramacion::whereHas('requisicionDatoCarga',function($q) use($datoCarga){
                $q->where('datocarga_id',$datoCarga->id);
            })->pluck('id'),
            'campos' => "{estado_programa_id".EstadoPrograma::whereHas('estado',function($qu){$qu->where('nombre','CANCELADA');})->first()->id."}",
            'metodo' => "PUT"
        ]);
        return new JsonResponse(['message'=>'cancelacion exitosa']);
    }
}
