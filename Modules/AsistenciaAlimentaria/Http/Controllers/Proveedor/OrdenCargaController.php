<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\DataTables\Proveedor\OrdenCarga;
use Modules\AsistenciaAlimentaria\DataTables\Proveedor\OrdenesCarga;

use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionDatoCarga;
use Modules\AsistenciaAlimentaria\Entities\ChoferProveedor;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\DatoCarga;

use App\Models\RecmCatProveedor;
use App\Models\ModuloProveedor;
use App\Models\EstadoPrograma;

use Carbon\Carbon;
use View;

class OrdenCargaController extends Controller{
    public function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,PROVEEDOR');
    }
    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index($pro_id,$req_id,OrdenCarga $tabla){
        return $tabla->with([
                'requisicion'=>Requisicion::find($req_id),
                'proveedor_id'=>$pro_id
            ])
            ->render('asistenciaalimentaria::proveedor.ordencarga',[
                'requisicion_id'=>$req_id,
                'proveedor_id'=>$pro_id
            ]);
    }
    
    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create($pro_id, $req_id, Request $request){
        return view::make('asistenciaalimentaria::modal',[
            'tipo'=>$request->get('id'),
            'vista'=>'asistenciaalimentaria::proveedor.modals.orden_carga',
            'requisicion_id' => $req_id,
            'proveedor_id'=>$pro_id
            ]
        );
    }
    
    /**
    * Store a newly created resource in storage.
    * @param  Request $request
    * @return Response
    */
    public function store($pro_id,$req_id,Request $request){
        try{
            $req = Requisicion::find($req_id);
            //obtenemos al proveedor
            DB::beginTransaction();
            $proveedor = RecmCatProveedor::whereHas('moduloProveedor',function($q) use ($pro_id){
                $q->whereHas('licitacion',function ($qu) use ($pro_id){
                    $qu->whereHas('ejercicio',function($que) use ($pro_id) {
                        $que->where('anio',$pro_id);
                    });
                });
            })->first();
            //y el modulo proveedor
            $modProv = ModuloProveedor::where([['modulo_id',config('asistenciaalimentaria.moduloId')],['proveedor_id',$proveedor->id]])->first();
            //En base al nombre se crea u obtiene el registro del chofer
            $chofer = ChoferProveedor::firstOrCreate([
                'modulo_proveedor_id' => $modProv->id,
                'nombre'              => $request->nombre,
                'primer_apellido'     => $request->primer_apellido,
                'segundo_apellido'    => $request->segundo_apellido
            ],['usuario_id'          => auth()->user()->id]);
            //se crea el registro principal de los datos de carga
            $datocarga = DatoCarga::create([
                //'vehiculoproveedor_id' => $request->vehiculoproveedor_id,
                'choferproveedor_id'   => $chofer->id,
                'ruta'                 => $request->ruta,
                'fecha'                => Carbon::createFromFormat('d/m/Y',$request['fecha'])->format('Y-m-d'),
                'placas'               => $request->placas, 
                'usuario_id'           => auth()->user()->id
            ]);
            //se crea un arreglo para insertar el total de programaciones requisiciones enviadas
            // dd($request->datos);
            $insertD = [];
            $idsxD = [];
            foreach ( (object)json_decode($request->datos) as $progReq) {
                RequisicionDatoCarga::firstOrCreate([
                    'programacion_requisicion_id' => json_decode($progReq)->id],
                [
                    'datocarga_id' => $datocarga->id,
                    'usuario_id' => auth()->user()->id
                ]);
                array_push($idsxD,json_decode($progReq)->id);
                // array_push($insertD,(object)$var);
            }
            /*
            dd($insertD);
            RequisicionDatoCarga::insert($insertD);
            dd('es el puto insert');
            */
            $nuevoEdo = EstadoPrograma::with('estado')->where('programa_id',$req->programa_id)->wherehas('estado',function($q){
                $q->where('nombre','ENVIADA');
            })->first();
            RequisicionProgramacion::whereIn('id',$idsxD)->update(['estado_programa_id'=>$nuevoEdo->id]);
            DB::commit();
            return new JsonResponse(['message'=>'Recuerda marcar el resultado en la sección entregas de la tabla principal']);
        }catch(Exception $e){
            DB::rollback();
            return new JsonResponse(['message'=>'Ocurrió un error durante el proceso :('],500);
        }
    }

}
