<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Proveedor;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AsistenciaAlimentaria\DataTables\Proveedor\Requisiciones;
use Modules\AsistenciaAlimentaria\DataTables\Proveedor\Progreso;

class ProveedorController extends Controller{
	
	public function __construct(){
		$this->middleware(['auth', 'authorized']);
		$this->middleware('rolModuleV2:alimentarios,PROVEEDOR');
	}
	/**
	* Display a listing of the resource.
	* @return Response
	*/
	public function index($pro_id, Requisiciones $tabla){
		return $tabla->with('proveedor_id',$pro_id)->render('asistenciaalimentaria::proveedor.Index');
	}
	
	public function show($pro_id, $req_id, Progreso $tabla){
		return $tabla->with(['proveedor_id'=>$pro_id,'requisicion_id'=>$req_id])->render('asistenciaalimentaria::proveedor.show');
	}
	
}
