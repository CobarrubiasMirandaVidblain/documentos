<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use App\Models\Region;
use App\Models\Programa;
use App\Models\Ejercicio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsistenciaAlimentaria\DataTables\Usuarios;
use Modules\AsistenciaAlimentaria\Entities\Financiamiento;

class ConfiguracionController extends Controller
{

  public function index(Usuarios $tabla){
    return $tabla->render(
      'asistenciaalimentaria::configuracion.config',
      [
        'anios'=>Ejercicio::OrderBy('anio','desc')->take(3)->get(),
        'programas'=>Programa::where('codigo','MOD ALIMENTARIO')->get(),
        'tipos_finan'=>Financiamiento::all()
      ]
    );
  }//vista de configuración

  public function update(Request $request){
    $munXregion = [];
    foreach (Region::All() as $region) {
      $aux = [];
      foreach ($region->distritos as $distrito) {
        foreach (array_pluck($distrito->municipios->toArray(),'id') as $mun) {
          array_push($aux,$mun);
        }
      }
      $munXregion[$region->nombre]=$aux;
    }
    return view('asistenciaalimentaria::index',['regiones'=>json_encode($munXregion)]);
  }//actualizar configuraciones

  public function show($anio){
    $main = Programa::where('nombre','ASISTENCIA ALIMENTARIA')->firstOrFail();
    return response()->json(Programa::join('anios_programas','anios_programas.programa_id','programas.id')->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
    ->where([['padre_id',$main->id],['anio',$anio]])
    ->get());
  }
}
