<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Licitacion;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsistenciaAlimentaria\DataTables\Licitacion\Dotaciones;
use View;
use Illuminate\Http\JsonResponse;
use App\Models\Ejercicio;
use Modules\AsistenciaAlimentaria\Entities\Dotacion;
use Modules\AsistenciaAlimentaria\Entities\Licitacion;

class DotacionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Dotaciones $tabla, Request $request)
    {
        if($request->lic_id) {
            return $tabla->with(['licitacion_id'=>$request->lic_id])
                    ->render('asistenciaalimentaria::modal',
                      [
                        'vista'=>'asistenciaalimentaria::licitaciones.dotaciones.list',
                        'licitacion'=> Licitacion::find($request->lic_id),
                        'tipo'=>$request->get('id')
                      ]
                    );
        } else {
            return $tabla->render('asistenciaalimentaria::licitaciones.dotaciones.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        return view::make('asistenciaalimentaria::modal')
                    ->with([
                        'vista' => 'asistenciaalimentaria::licitaciones.dotaciones.create',
                        'tipo'=>$request->get('id')
                    ])
                    ->render();
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
        try {
            DB::beginTransaction();
                $ejercicio = Ejercicio::where('anio',config('asistenciaalimentaria.anioActual'))->first()->id;
                $data = $request->all();
                $data['ejercicio_id'] = $ejercicio;
                $data['usuario_id']   = auth()->user()->id;
                Dotacion::updateOrCreate(
                    ['tipoaccion_id'=>$request->input('tipoaccion_id'), 'ejercicio_id'=>$ejercicio],
                    $data
                );
            DB::commit();
            return new JsonResponse(['Se agregó con éxito'], 200);
        } catch (\Exception $th) {
            DB::rollBack();
            return new JsonResponse([$th->getMessage()], 500);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('asistenciaalimentaria::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
                Dotacion::destroy($id);
            DB::commit();
            return new JsonResponse(['Se eliminó correctamente'], 200);
        } catch (\Exception $th) {
            DB::rollback();
            return new JsonResponse([$th->getMessage()], 500);
        }
    }
}