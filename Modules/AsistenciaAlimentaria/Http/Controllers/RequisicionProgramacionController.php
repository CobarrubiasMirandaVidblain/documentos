<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Modules\AsistenciaAlimentaria\Http\Controllers\RequisicionProgramacionDotacionController;
use Modules\AsistenciaAlimentaria\DataTables\RequisicionProgramacionesNoSelec;
use Modules\AsistenciaAlimentaria\DataTables\RequisicionProgramacionesSelec;
use Modules\AsistenciaAlimentaria\DataTables\RequisicionProgramaciones;

use Modules\AsistenciaAlimentaria\Entities\RequisicionCantidadDotacion;
use Modules\AsistenciaAlimentaria\Entities\CostoPresentacionProducto;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;
use Modules\AsistenciaAlimentaria\Entities\Recibo;
use App\Models\EstadoPrograma;
use App\Models\Region;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\Traits\NumberToString;

use View;

class RequisicionProgramacionController extends Controller
{
  use NumberToString;

  public function __construct()
  {
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL');
  }

  public function index($req_id, RequisicionProgramaciones $tabla)
  {
    $requisicion = Requisicion::find($req_id);
    return $tabla->with('requisicion', $requisicion)->render('asistenciaalimentaria::requisicion.distribucion.index', [
      'requisicion' => $requisicion,
      'regiones' => Region::all(),
      'acciones' => TipoAccion::where('programa_id', $requisicion->programa_id)->get()
    ]);
  }
  public function create($req_id, RequisicionProgramacionesSelec $tabla)
  {
    $estado = Requisicion::find($req_id)->estadoprograma->estado->nombre;
    if (strtoupper($estado) == 'ENVIADA')
      return new JsonResponse(['message' => 'No disponible para selección'], 403);
    $requisicion = Requisicion::find($req_id);
    return $tabla->with('requisicion', $requisicion)->render('asistenciaalimentaria::requisicion.distribucion.create', [
      'requisicion' => $requisicion,
      /* 'regiones'=>Region::all(),
            'acciones'=>TipoAccion::where('programa_id',$requisicion->programa_id)->get() */
    ]);
  }
  public function store($req_id, Request $request)
  {
    $req = Requisicion::find($req_id);
    $edoProg = EstadoPrograma::with('estado')->where('programa_id', $req->programa_id)->wherehas('estado', function ($q) {
      $q->where('nombre', 'REVISION');
    })->first();
    $user = auth()->user()->id;
    if ($request->has('programacion_id')) {
      try {
        DB::beginTransaction();
        $reqProg = RequisicionProgramacion::create([
          'requisicion_id'    => $req_id,
          'programacion_id'   => $request->input('programacion_id'),
          'estado_programa_id' => $edoProg->id,
          'usuario_id'        => $user
        ]);
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_programacion_requisiciones',
          'registro' => $reqProg->id,
          'campos' => "{'requisicion_id':'$req_id','programacion_id':'{$request->input('programacion_id')}','estado_programa_id':'$edoProg->id'}",
          'metodo' => request()->method()
        ]);
        $test = new RequisicionProgramacionDotacionController();
        $tiposAccion = TipoAccion::where('programa_id', $reqProg->requisicion->programa_id)->get();
        $datos = [];
        foreach ($tiposAccion as $accion) {
          $redondeo = ($req->num_bimestre % 2) == 1 ? PHP_ROUND_HALF_UP : PHP_ROUND_HALF_DOWN;
          $aux['tipoaccion_id'] = $accion->id;
          $aux['cantidad_beneficiarios'] = $reqProg->programacion->beneficiarios->where('tipoaccion_id', $accion->id)->count();
          $aux['cantidad_dotaciones'] = ($req->programa_id == config('asistenciaalimentaria.ccncId')) ?
            round($aux['cantidad_beneficiarios'] / 2, 0, $redondeo) : ($req->programa_id == config('asistenciaalimentaria.lecheId') ? $aux['cantidad_beneficiarios'] * 16 : $aux['cantidad_beneficiarios']);
          array_push($datos, $aux);
        }
        $test->agregarDotaciones($reqProg->id, $datos);
        DB::commit();
        return new JsonResponse(['message' => 'Programación agregada'], 200);
      } catch (Exception $e) {
        DB::rollback();
        return new JsonResponse(['message' => $e->getmessage()], 500);
      }
    } else { //En caso de que no se especifique una programacion, se reciben los filtros aplicados del lado del cliente para agregar lo mismo a la requisicion
      try {
        //se compruebas las programaciones actuales en la requisicion
        $progEnRequision = $req->programacionrequisicion->pluck('programacion_id');

        DB::beginTransaction();
        $query = Programacion::GetValidForRequisicion($req, $request);
        //where para que no se repita en la misma requisicion
        // ->whereNotIn('alim_programacion.id',$progEnRequision);
        // dd($query->get());
        foreach ($request->input('filtros', []) as $filtro) {
          if ($filtro['name']=='alim_sujetos.dedesarrollo_programa_id'){
            if (strtoupper($filtro['value']) == 'SI') {
              $query->whereNotNull($filtro['name']);
            } else if (strtoupper($filtro['value']) == 'NO') {
              $query->whereNull($filtro['name']);
            }
          }
          else if ($filtro['name']=='alim_cat_financiamientos.nombre'){
            if (strtoupper($filtro['value']) == 'SI') {
              $query->where($filtro['name'],'PRODUCTO FINANCIERO');
            } else if (strtoupper($filtro['value']) == 'NO') {
              $query->where($filtro['name'],'!=','PRODUCTO FINANCIERO');
            }
          }
          else if ($filtro['name']=='alim_programacion.esampliacion'){
            if (strtoupper($filtro['value']) == 'SI') {
              $query->where($filtro['name'],1);
            } else if (strtoupper($filtro['value']) == 'NO') {
              $query->where($filtro['name'],0);
            }
          }
          else
            $query->where($filtro['name'], 'LIKE', "%{$filtro['value']}%");
        }
        //se crea la estrucra de los datos que se usara para insertar de manera msiva en la BD
        $query->select([DB::raw("$req->id as requisicion_id, alim_programacion.id as programacion_id, $edoProg->id as estado_programa_id, $user as usuario_id, NOW() as created_at, NOW() as updated_at")]);
        // dd($query->get()->toArray());
        //se toma el ultimo id en la db antes de la insercion
        $primerRegistro = RequisicionProgramacion::withTrashed()->count() > 0 ? (RequisicionProgramacion::withTrashed()->get()->last()->id) : 0;
        RequisicionProgramacion::insert($query->get()->toArray());
        //y se toma el ultimo id en la bd despues de la insercoion
        $ultimoRegistro = RequisicionProgramacion::get()->last()->id;
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_programacion_requisiciones',
          'comentario' => "[" . ($primerRegistro + 1) . "-$ultimoRegistro]",
          'campos' => "Este bloque de inserción va del id $primerRegistro al $ultimoRegistro",
          'metodo' => request()->method()
        ]);
        $primerRegistroD = RequisicionCantidadDotacion::withTrashed()->count() > 0 ? (RequisicionCantidadDotacion::withTrashed()->get()->last()->id) : 0;
        $select = [
          "alim_programacion_requisiciones.id as programacion_requisicion_id",
          DB::raw("(SELECT id FROM alim_dotaciones where alim_dotaciones.tipoaccion_id = alim_tiposaccion.id) as dotacion_id"),
          DB::raw("(SELECT COUNT(*) from alim_beneficiarios where alim_beneficiarios.tipoaccion_id =  alim_tiposaccion.id and alim_beneficiarios.programacion_id = alim_programacion_requisiciones.programacion_id and alim_beneficiarios.deleted_at is null) as cantidad_beneficiarios"),
          DB::raw("$user as usuario_id, NOW() as created_at, NOW() as updated_at")
        ];
        if ($req->programa_id == config('asistenciaalimentaria.ccncId')) {
          array_push($select, DB::raw("(CASE WHEN MOD((SELECT num_bimestre FROM alim_requisiciones where alim_requisiciones.id = alim_programacion_requisiciones.requisicion_id),2) = 1	THEN
                         ROUND((SELECT COUNT(*) from alim_beneficiarios where alim_beneficiarios.tipoaccion_id =  alim_tiposaccion.id and alim_beneficiarios.programacion_id = alim_programacion_requisiciones.programacion_id and alim_beneficiarios.deleted_at is null)/2)
                    ELSE
                        FLOOR((SELECT COUNT(*) from alim_beneficiarios where alim_beneficiarios.tipoaccion_id =  alim_tiposaccion.id and alim_beneficiarios.programacion_id = alim_programacion_requisiciones.programacion_id and alim_beneficiarios.deleted_at is null)/2)
                    END) as cantidad_dotaciones"));
        } else if ($req->programa_id == config('asistenciaalimentaria.lecheId')) {
          array_push($select, DB::raw("(SELECT (COUNT(*) * 16) from alim_beneficiarios where alim_beneficiarios.tipoaccion_id = alim_tiposaccion.id and alim_beneficiarios.programacion_id = alim_programacion_requisiciones.programacion_id and alim_beneficiarios.deleted_at is null) as cantidad_dotaciones"));
        } else {
          array_push($select, DB::raw("(SELECT COUNT(*) from alim_beneficiarios where alim_beneficiarios.tipoaccion_id =  alim_tiposaccion.id and alim_beneficiarios.programacion_id = alim_programacion_requisiciones.programacion_id and alim_beneficiarios.deleted_at is null) as cantidad_dotaciones"));
        }
        $superquery = RequisicionProgramacion::crossJoin('alim_tiposaccion')
          ->whereBetween('alim_programacion_requisiciones.id', [$primerRegistro + 1, $ultimoRegistro + 1])
          ->whereRaw(
            "alim_tiposaccion.programa_id =
                    ( SELECT programa_id FROM alim_alimentarios
                    INNER JOIN alim_programacion ON alim_programacion.alimentario_id = alim_alimentarios.id
                    WHERE alim_programacion.id = alim_programacion_requisiciones.programacion_id)"
          )
          ->select($select);
        RequisicionCantidadDotacion::insert($superquery->get()->toArray());
        $ultimoRegistro = RequisicionCantidadDotacion::get()->last()->id;
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_cantidadesdotaciones_requisiciones',
          'comentario' => "Este bloque de inserción va del id $primerRegistro al $ultimoRegistro",
          'campos' => "[" . ($primerRegistroD + 1) . "-$ultimoRegistro]",
          'metodo' => request()->method()
        ]);
        DB::commit();
        return new JsonResponse(['message' => 'Programaciones agregadas'], 200);
      } catch (Exception $e) {
        DB::rollback();
        return new JsonResponse(['message' => $e->getmessage()], 500);
      }
    }
  }
  public function destroy($req_id, $prog_id, Request $request)
  {
    try {
      $progReq = RequisicionProgramacion::where('requisicion_id', $req_id);
      DB::beginTransaction();
      if ($prog_id != 'undefined') {
        $progReq->where('programacion_id', $prog_id);
        $eliminar = clone $progReq;
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_cantidadesdotaciones_requisiciones',
          'comentario' => "Este bloque de eliminación son para aquellos alim_cantidadesdotaciones_requisiciones.programacion_requisicion_id = {$progReq->first()->id}",
          'campos' => "Dónde: programacion_requisicion_id = {$progReq->first()->id}",
          'metodo' => request()->method()
        ]);
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_programacion_requisiciones',
          'registro' => $progReq->first()->id,
          'campos' => 'Se eliminaron todas las alim_cantidadesdotaciones_requisiciones correspondientes',
          'metodo' => request()->method()
        ]);
        RequisicionCantidadDotacion::where('programacion_requisicion_id', $progReq->first()->id)->delete();
        if ($request->m) {
          $p = $eliminar->first();
          $p->motivo_cancelacion = $request->m;
          $p->save();
        }
        $eliminar->delete();
      } else {
        $toDelete = $progReq->get()->pluck('id');
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_cantidadesdotaciones_requisiciones',
          'comentario' => "Este bloque de eliminación son para aquellos alim_cantidadesdotaciones_requisiciones.programacion_requisicion_id = $toDelete",
          'campos' => "Dónde: programacion_requisicion_id = $toDelete",
          'metodo' => request()->method()
        ]);
        auth()->user()->bitacora(request(), [
          'tabla' => 'alim_programacion_requisiciones',
          'comentario' => 'Se eliminaron todas las alim_cantidadesdotaciones_requisiciones correspondientes',
          'campos' => "$toDelete",
          'metodo' => request()->method()
        ]);
        RequisicionCantidadDotacion::whereIn('programacion_requisicion_id', $toDelete)->delete();
        if ($request->m) {
          $p = $progReq->first();
          $p->motivo_cancelacion = $request->m;
          $p->save();
        }
        $progReq->delete();
      }
      DB::commit();
      return new JsonResponse(['message' => 'programacion desagregada'], 200);
    } catch (Exception $e) {
      DB::rollback();
      return new JsonResponse(['message' => $e->getmessage()], 500);
    }
  }
  public function list($req_id, Request $request, RequisicionProgramacionesNoSelec $tabla)
  {
    $requisicion = Requisicion::findOrFail($req_id);
    return $tabla->with(['requisicion' => $requisicion])
      ->render('asistenciaalimentaria::modal', ['vista' => 'asistenciaalimentaria::requisicion.distribucion.noselect_list', 'tipo' => $request->get('id')]);
  }
  public function generarRecibos($req_id)
  {
    $datos = [];
    $requisicion = Requisicion::find($req_id);
    $user = auth()->user();
    $insertRecibos = RequisicionProgramacion::leftJoin('alim_recibos', 'alim_recibos.programacion_requisicion_id', 'alim_programacion_requisiciones.id')
      ->join('alim_requisiciones', 'alim_requisiciones.id', 'alim_programacion_requisiciones.requisicion_id')
      ->where('requisicion_id', $req_id)->whereNull('alim_recibos.id')
      ->select(
        [
          'alim_programacion_requisiciones.id as programacion_requisicion_id',
          DB::raw("$user->id as usuario_id"),
          DB::raw("now() as created_at"),
          DB::raw("now() as updated_at"),
          DB::raw("(select id
                from alim_referenciasbancarias
                where programacion_id = alim_programacion_requisiciones.programacion_id
                and SUBSTR(referencia,12,1) = alim_requisiciones.num_bimestre ) as referenciabancaria_id")
        ]
      );
    $paraGenerar = (clone $insertRecibos)->count();
    if (Recibo::insert($insertRecibos->get()->toArray())) {
      $datos = [];
      $auxs = RequisicionProgramacion::with([
        'recibo:id,programacion_requisicion_id',
        'requisicioncantidaddotacion:programacion_requisicion_id,cantidad_dotaciones,cantidad_beneficiarios,dotacion_id',
        'requisicioncantidaddotacion.dotacion',
        'requisicioncantidaddotacion.dotacion.tipoaccion',
        'programacion:id,alimentario_id',
        'programacion.alimentario:id,localidad_id,municipio_id,foliodif,sublocalidad',
        'programacion.alimentario.municipio:id,nombre',
        'programacion.alimentario.localidad:id,nombre',
        'programacion.alimentario.desayuno:alimentario_id,escuela_id',
        'programacion.alimentario.desayuno.escuela:id,nombre,clave',
        'programacion.alimentario.sujeto',
        'programacion.referenciasbancaria:programacion_id,referencia',

      ])->where('requisicion_id', $requisicion->id)->get();
      foreach ($auxs as $aux) {
        $var = [];
        $var['fecha'] = date('d-m-Y');
        $var['ejercicio'] = $requisicion->licitacion->ejercicio->anio;
        $var['bimestre'] = $requisicion->num_bimestre;
        $var['tipoentrega'] = $requisicion->tipoentrega->nombre;
        $var['programa'] = $requisicion->programa->nombre;
        $var['nummeses'] = $requisicion->periodo;
        $var['folio'] = str_pad('DIF', 8 - strlen($aux->recibo->id), '0') . $aux->recibo->id;
        $var['foliodif'] = $aux->programacion->alimentario->foliodif;
        $var['localidad'] = isset($aux->programacion->alimentario->localidad) ? ($aux->programacion->alimentario->localidad->nombre . (isset($aux->programacion->alimentario->sublocalidad) ? ' - ' . $aux->programacion->alimentario->sublocalidad : '')) : null;
        $var['municipio'] = $aux->programacion->alimentario->municipio->nombre;
        $var['escuela'] = isset($aux->programacion->alimentario->desayuno) ? $aux->programacion->alimentario->desayuno->escuela->nombre : null;
        $var['claveEsc'] = isset($aux->programacion->alimentario->desayuno) ? $aux->programacion->alimentario->desayuno->escuela->clave : null;
        $var['asociacion'] = isset($aux->programacion->alimentario->sujeto) ? $aux->programacion->alimentario->sujeto->nombre_asociacion : null;
        $var['referencia'] = $aux->programacion->referenciasbancaria()->whereRaw('substring(referencia,12,1) = ?', $requisicion->num_bimestre)->first()->referencia;
        $var['befT'] = 0;
        $var['DotT'] = 0;
        $var['importe'] = 0;
        foreach ($aux->requisicioncantidaddotacion as $subprograma) {
          $nombre = explode('(', explode(')', $subprograma->dotacion->tipoaccion->nombre)[0])[1];
          $var['acciones'][$nombre] = [
            'dotaciones' => $subprograma->cantidad_dotaciones,
            'beneficiarios' => $subprograma->cantidad_beneficiarios,
            'costo' => $subprograma->dotacion->cuota_recuperacion * $var['nummeses'],
            'importe' => $requisicion->programa_id == config('asistenciaalimentaria.lecheId') ? $subprograma->cantidad_dotaciones * $subprograma->dotacion->cuota_recuperacion : $subprograma->cantidad_beneficiarios * $subprograma->dotacion->cuota_recuperacion * $var['nummeses']
          ];
          $var['importe'] += $var['acciones'][$nombre]['importe'];
          $var['befT'] += $subprograma->cantidad_beneficiarios;
          $var['DotT'] += $subprograma->cantidad_dotaciones;
          $var['cuota'] = $subprograma->dotacion->cuota_recuperacion;
          $var['costo'] = $subprograma->dotacion->cuota_recuperacion  * $var['nummeses'];
        }
        $var['importeLetras'] = $this->convertir($var['importe']);
        array_push($datos, $var);
      }
      if ($paraGenerar > 0)
        return new JsonResponse(['datos' => $datos, 'message' => 'recibos generados']);
      else
        return new JsonResponse(['datos' => $datos, 'message' => 'recibos no generados, solo mostrados']);
    } else {
      return new JsonResponse(['message' => 'algo salio mal creando recibos'], 500);
    }
  }
  public function programacionRequisiciones($prog_id, Request $request)
  {
    $recibos = RequisicionProgramacion::join('alim_recibos', 'alim_recibos.programacion_requisicion_id', 'alim_programacion_requisiciones.id')
      ->join('alim_requisiciones', 'alim_requisiciones.id', 'alim_programacion_requisiciones.requisicion_id')
      ->join('alim_cat_tiposentrega', 'alim_cat_tiposentrega.id', 'alim_requisiciones.tipo_entrega_id')
      ->leftjoin('alim_pagosrecibos', 'alim_pagosrecibos.recibo_id', 'alim_recibos.id')
      ->where('programacion_id', $prog_id)->get([
        'alim_recibos.id as id',
        'num_oficio',
        'num_bimestre',
        'num_entrega',
        'alim_cat_tiposentrega.nombre as tipoentrega',
        'observaciones',
        'alim_pagosrecibos.fecha_pago',
        'programacion_id',
        'requisicion_id',
        'alim_pagosrecibos.importe_total',
      ]); //->toArray();
    // dd($recibos);
    return view::make('asistenciaalimentaria::modal')
      ->with([
        'vista'                     => 'asistenciaalimentaria::programacion.fragmentos.listado_requisiciones',
        'programacion'              => Programacion::find($prog_id),
        'recibos'                   => $recibos,
        'tipo'                      => $request->get('id')
      ]);
  }
  public function generarRecibo($prog_id, $req_id)
  {
    $requisicion = Requisicion::findorfail($req_id);
    $datos = [];
    $auxs = RequisicionProgramacion::with([
      'recibo:id,programacion_requisicion_id',
      'requisicioncantidaddotacion:programacion_requisicion_id,cantidad_dotaciones,cantidad_beneficiarios,dotacion_id',
      'requisicioncantidaddotacion.dotacion',
      'requisicioncantidaddotacion.dotacion.tipoaccion',
      'programacion:id,alimentario_id',
      'programacion.alimentario:id,localidad_id,municipio_id,foliodif,sublocalidad',
      'programacion.alimentario.municipio:id,nombre',
      'programacion.alimentario.localidad:id,nombre',
      'programacion.alimentario.desayuno:alimentario_id,escuela_id',
      'programacion.alimentario.desayuno.escuela:id,nombre,clave',
      'programacion.alimentario.sujeto',
      'programacion.referenciasbancaria:programacion_id,referencia'
    ])->where([['requisicion_id', $req_id], ['programacion_id', $prog_id]])->get();

    foreach ($auxs as $aux) {
      $var = [];
      $var['fecha'] = date('d-m-Y');
      $var['ejercicio'] = $requisicion->licitacion->ejercicio->anio;
      $var['bimestre'] = $requisicion->num_bimestre;
      $var['tipoentrega'] = $requisicion->tipoentrega->nombre;
      $var['programa'] = $requisicion->programa->nombre;
      $var['nummeses'] = $requisicion->periodo;
      $var['folio'] = str_pad('DIF', 8 - strlen($aux->recibo->id), '0') . $aux->recibo->id;
      $var['foliodif'] = $aux->programacion->alimentario->foliodif;
      $var['localidad'] = $aux->programacion->alimentario->localidad->nombre . (isset($aux->programacion->alimentario->sublocalidad) ? ' - ' . $aux->programacion->alimentario->sublocalidad : '');
      $var['municipio'] = $aux->programacion->alimentario->municipio->nombre;
      $var['escuela'] = isset($aux->programacion->alimentario->desayuno) ? $aux->programacion->alimentario->desayuno->escuela->nombre : null;
      $var['claveEsc'] = isset($aux->programacion->alimentario->desayuno) ? $aux->programacion->alimentario->desayuno->escuela->clave : null;
      $var['asociacion'] = isset($aux->programacion->alimentario->sujeto) ? $aux->programacion->alimentario->sujeto->nombre_asociacion : null;
      $var['referencia'] = $aux->programacion->referenciasbancaria()->whereRaw('substring(referencia,12,1) = ?', $requisicion->num_bimestre)->first()->referencia;
      $var['befT'] = 0;
      $var['DotT'] = 0;
      $var['importe'] = 0;
      foreach ($aux->requisicioncantidaddotacion as $subprograma) {
        $nombre = explode('(', explode(')', $subprograma->dotacion->tipoaccion->nombre)[0])[1];
        $var['acciones'][$nombre] = [
          'dotaciones' => $subprograma->cantidad_dotaciones,
          'beneficiarios' => $subprograma->cantidad_beneficiarios,
          'costo' => $subprograma->dotacion->cuota_recuperacion * $var['nummeses'],
          'importe' => $requisicion->programa_id == config('asistenciaalimentaria.lecheId') ? $subprograma->cantidad_dotaciones * $subprograma->dotacion->cuota_recuperacion : $subprograma->cantidad_beneficiarios * $subprograma->dotacion->cuota_recuperacion * $var['nummeses']
        ];
        $var['importe'] += $var['acciones'][$nombre]['importe'];
        $var['befT'] += $subprograma->cantidad_beneficiarios;
        $var['DotT'] += $subprograma->cantidad_dotaciones;
        $var['cuota'] = $subprograma->dotacion->cuota_recuperacion;
        $var['costo'] = $subprograma->dotacion->cuota_recuperacion  * $var['nummeses'];
      }
      $var['importeLetras'] = $this->convertir($var['importe']);
      array_push($datos, $var);
    }

    return new JsonResponse(['datos' => $datos, 'message' => 'recibo obtenido']);
  }
}
