<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;
use Modules\AsistenciaAlimentaria\Entities\Dotacion;
use View;
use Modules\AsistenciaAlimentaria\Entities\RequisicionCantidadDotacion;
use Symfony\Component\HttpFoundation\JsonResponse;

class RequisicionProgramacionDotacionController extends Controller {
    
    public function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,SUPERADMIN');
    }
    
	public function index($req_id,$prog_id,Request $request){
        $progReq =RequisicionProgramacion::where([['requisicion_id',$req_id],['programacion_id',$prog_id]])->first();
		return view::make('asistenciaalimentaria::modal',[
                'tipo'=>$request->get('id'),
                'vista'=>'asistenciaalimentaria::requisicion.distribucion.detalles.create',
                'requisicionProgramacion'=> $progReq,
                'tiposAccion' => TipoAccion::where('programa_id',$progReq->requisicion->programa_id)->get(),
				'lg'=>true,
				'footernull'=>true
            ]
        );
	}
	public function create($req_id,$prog_id,Request $request) {
        $progReq =RequisicionProgramacion::where([['requisicion_id',$req_id],['programacion_id',$prog_id]])->first();
		return view::make('asistenciaalimentaria::modal',[
                'tipo'=>$request->get('id'),
                'vista'=>'asistenciaalimentaria::requisicion.distribucion.detalles.create',
                'requisicionProgramacion'=> $progReq,
                'tiposAccion' => TipoAccion::where('programa_id',$progReq->requisicion->programa_id)->get(),
				'lg'=>true
            ]
        );
	}
	/* public function store($req_id, $prog_id, Request $request){
        if($request->has('dotaciones')){
            $progReqId = RequisicionProgramacion::where([['requisicion_id',$req_id],['programacion_id',$prog_id]])->first();
            DB::beginTransaction();
            if($this->agregarDotaciones($progReqId->id,$request->dotaciones)){
                DB::commit();
                return new JsonResponse(['message'=>'Dotaciones configuradas'],200);
            }
            return new JsonResponse(['message'=>'Ocurrió un error al configurar dotaciones'],500);
            DB::rollback();
        }
    } */
    /* public function destroy(){

    } */
    public function updateDotaciones($req_id,$prog_id,Request $request){ //update dotaciones
        if($request->has('dotaciones')){
            $progReqId = RequisicionProgramacion::where([['requisicion_id',$req_id],['programacion_id',$prog_id]])->first();
            DB::beginTransaction();
            if($this->editarDotaciones($progReqId->id,json_decode($request['dotaciones']))){
                DB::commit();
                return new JsonResponse(['message'=>'Dotaciones configuradas'],200);
            }
            DB::rollback();
            return new JsonResponse(['message'=>'Ocurrió un error al configurar dotaciones'],500);
        }
    }
    public function editarDotaciones($req_prog_id,$dotaciones){
        try{
            auth()->user()->bitacora(request(), [
                'tabla' => 'alim_cantidadesdotaciones_requisiciones',
                'comentario' => "Dónde: programacion_requisicion_id = $req_prog_id",
                'campos' => json_encode($dotaciones).'',
                'metodo' => 'PUT'
            ]);
            foreach ($dotaciones as $dotacion) {
                $d = Dotacion::where('tipoaccion_id',$dotacion->tipoaccion_id)->first();
                $rcd = RequisicionCantidadDotacion::where('programacion_requisicion_id',$req_prog_id)->where('dotacion_id',$d->id)->first();
                $rcd->update([
                    'cantidad_dotaciones'         => $dotacion->cantidad_dotaciones,
                    'cantidad_beneficiarios'      => $dotacion->cantidad_beneficiarios,
                    'usuario_id'                  => auth()->user()->id
                ]);
            }
            return true;
        }catch(Exception $e){
            return false; 
        }
    }
    public function agregarDotaciones($req_prog_id,$dotaciones){
        try{
            auth()->user()->bitacora(request(), [
                'tabla' => 'alim_cantidadesdotaciones_requisiciones',
                'comentario' => "Dónde: programacion_requisicion_id = $req_prog_id",
                'campos' => json_encode($dotaciones).'',
                'metodo' => 'POST'
            ]);
            foreach ($dotaciones as $dotacion) {
                RequisicionCantidadDotacion::create([
                    'programacion_requisicion_id' => $req_prog_id,
                    'dotacion_id'                 => Dotacion::where('tipoaccion_id',$dotacion['tipoaccion_id'])->first()->id,
                    'cantidad_dotaciones'         => $dotacion['cantidad_dotaciones'],
                    'cantidad_beneficiarios'      => $dotacion['cantidad_beneficiarios'],
                    'usuario_id'                  => auth()->user()->id
                ]);
            }
            return true;
        }catch(Exception $e){
            return false; 
        }
    }
    public function removerDotaciones($progReqId){
        try {
            RequisicionCantidadDotacion::where('programacion_requisicion_id',$progReqId)->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

	
}