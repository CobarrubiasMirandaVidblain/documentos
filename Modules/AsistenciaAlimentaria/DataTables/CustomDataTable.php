<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

use Yajra\DataTables\Services\DataTable;
use Illuminate\Http\JsonResponse;

/*
  Esta clase extiende del detatable pero crea un metodo para obtener los datos en json y asi poder exportar usando jsreports.
*/
class CustomDataTable extends DataTable{

    protected $actions = ['print', 'csv', 'excel', 'pdf', 'customExcel'];
    /**
     * Process dataTables needed render output.
     *
     * @param string $view
     * @param array $data
     * @param array $mergeData
     * @return mixed
     */
    public function render($view, $data = [], $mergeData = []){
      if ($this->request()->ajax() && $this->request()->wantsJson()) {
        return app()->call([$this, 'ajax']);
      }

      if ($action = $this->request()->get('action') and in_array($action, $this->actions)) {
        if ($action == 'print') {
            return app()->call([$this, 'printPreview']);
        }
        if ($action == "customExcel"){
            return app()->call([$this, 'myExcel']);
        }
        return app()->call([$this, $action]);
      }

      return view($view, $data, $mergeData)->with($this->dataTableVariable, $this->getHtmlBuilder());
    }

    public function myExcel(){
        $columns = $this->getExportColumnsFromBuilder();
        $cabeceras = [];
        foreach ($columns as $column) {
            if($column->exportable)
                array_push($cabeceras,$column->title);
        }
        return new JsonResponse(
            [
                'cabeceras'=>$cabeceras ,
                'data'=>$this->mapResponseToColumns($columns, 'exportable'),
                'name'=>$this->getFilename()
            ]
        );
    }

    public function excel()
    {
        $this->buildExcelFile()->download('xlsx');
    }
}
