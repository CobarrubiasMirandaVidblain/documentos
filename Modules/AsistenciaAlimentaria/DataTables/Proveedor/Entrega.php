<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Proveedor;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

class Entrega extends CustomDataTable{
    
    public function dataTable($query){
        $tabla = datatables($query);
        return $tabla->editColumn('estado',function($item){
            $var = '';
            switch ($item->estado) {
                case 'ENVIADA':
                    $var='warning';
                    break;
                case 'RECIBIDA':
                    $var='success';
                    break;
                case 'NO RECIBIDA':
                    $var='danger';
                    break;
                default:
                    $var='light';
                    break;
            }
            return "<span class='badge badge-pill badge-$var'> $item->estado</span>";
        })->addColumn('entrega',function ($programacion){
            $crear = route('alimentarios.proveedor.requisicion.entregas.create',[$this->proveedor_id,$this->requisicion->id]);
            return "<button class='btn btn-".($programacion->entregaproveedor_id ? 'success' : 'info')." btn-xs btn-block dt-btn-entrega' onClick='modal.loadModal(\"$crear\",\"create\",{req_prog_id:$programacion->req_prog_id})'><i class='fas fa-truck-loading'></i></button>";
        })->rawColumns(['entrega','estado']);
    }
    
    public function query(){
        $model = Programacion:://join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
        join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
        //->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
        ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
        ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
        ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
        ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
        ->join('alim_programacion_requisiciones','alim_programacion_requisiciones.programacion_id','alim_programacion.id')
        ->join('alim_requisiciones','alim_requisiciones.id','alim_programacion_requisiciones.requisicion_id')
        ->join('alim_recibos','alim_recibos.programacion_requisicion_id','alim_programacion_requisiciones.id')
        ->join('estados_programas','estados_programas.id','alim_programacion_requisiciones.estado_programa_id')
        ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
        ->join('alim_requisiciones_datoscarga','alim_requisiciones_datoscarga.programacion_requisicion_id','alim_programacion_requisiciones.id')
        ->leftJoin('alim_entregasproveedor','alim_entregasproveedor.requisicion_datocarga_id','alim_requisiciones_datoscarga.id')
        ->where('alim_programacion_requisiciones.requisicion_id',$this->requisicion->id)
        ->whereNull('alim_programacion_requisiciones.deleted_at')
        ->select([
            'alim_programacion_requisiciones.id as req_prog_id',
            'alim_recibos.id as recibo',
            'alim_alimentarios.foliodif as cocina',
            'cat_municipios.nombre as municipio',
            'cat_localidades.nombre as localidad',
            'cat_distritos.nombre as distrito',
            'alim_alimentarios.sublocalidad as sublocalidad',
            'cat_regiones.nombre as region',
            'alim_requisiciones.num_bimestre as bimestre',
            'alim_requisiciones.num_entrega as entrega',
            'alim_requisiciones.periodo',
            'cat_estados.nombre as estado',
            'alim_entregasproveedor.id as entregaproveedor_id',
            DB::raw('(select sum(cantidad_dotaciones) from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            where pr.programacion_id=alim_programacion.id and cd.deleted_at is null) as dot_totales')
        ]);
        
        
        return $model;
        
    }
    
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->parameters($this->getBuilderParameters());
    }
    
    protected function getColumns(){
        $columns =
        [
            //'programacion_id' => ['data' => 'programacion_id', 'name' => 'alim_programacion.id', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false, 'visible' => false],
            'recibo'        => ['name' => 'alim_recibos.id','className'=>'noselect'],
            'cocina'        => ['name' => 'alim_alimentarios.foliodif', 'title'=>'f. dIf'],
            'estado'        => ['name' => 'cat_estados.nombre'],
            'region'        => ['name' => 'cat_regiones.nombre'],
            'distrito'      => ['name' => 'cat_distritos.nombre'],
            'municipio'     => ['name' => 'cat_municipios.nombre'],
            'localidad'     => ['name' => 'cat_localidades.nombre'],
            'sublocalidad'  => ['name' => 'alim_alimentarios.sublocalidad'],
            'bimestre'      => ['name' => 'alim_requisiciones.num_bimestre'],
            'entrega'       => ['name' => 'alim_requisiciones.num_entrega'],
            'periodo'       => ['name' => 'alim_requisiciones.periodo'],
            'dot_totales'   => ['searchable' => false, 'orderable'=>false],
            'entrega'       => ['searchable' => false, 'orderable'=>false],
        ];
        return $columns;
    }
    
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'buttons'=>['colvis'=>'Columnas visibles'],
                'select' => [
                    'rows' => [
                        '_' => "hay %d filas seleccionadas",
                        '0' => "Da click para seleccionar una fila",
                        '1' => "1 fila seleccionada"
                    ]
                ]
            ],
            'deferRender'=> true,
            'rowId' => 'req_prog_id',
            'dom' => 'lBtip',
            'initComplete'=>'function(){
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on("change", function () {
                        column.search($(this).val()).draw();
                    });
                });
            }',
            /* 'drawCallback'=>"function(){
                var ruta = '$crear'
                $('button.dt-btn-entrega').each(function(index) {
                    $(this).click(function(){
                        var req_prog_id = $(this).parent().parent().attr('id')
                        modal.loadModal(ruta+'?'+req_prog_id,'create')  
                    })
                })
                console.log('eventos creados')
            }", */
            'buttons' => [
                'reset', 'reload',
            ],
            'lengthMenu' => [  10, 5, 20 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        
        return $builderParameters;
    }
    
    protected function filename(){
        return 'Requisición al ' . date('Ymd');
    }
}
