<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Programacion;
use App\Models\CargoPrograma;
use App\Models\Programa;
use App\Models\AniosPrograma;

class Programaciones extends CustomDataTable{
  
  public function dataTable($query){
    $prefix = explode("/",$this->request->getPathInfo());
    return datatables($query)
    ->addColumn('editar',function ($anioPrograma) use($prefix) {
      $aux = route("alimentarios.$prefix[2].programacion.edit",$anioPrograma->ejercicio);
      return $anioPrograma->ejercicio <config('asistenciaalimentaria.anioActual')? "<a href='$aux' class='btn btn-xs btn-warning disabled' ><i class='fas fa-edit'></i> Editar</a>" : "<a href='$aux' class='btn btn-xs btn-warning' ><i class='fas fa-edit'></i> Editar</a>";
    })
    ->addColumn('mostrar',function ($anioPrograma) use($prefix) {
      $aux = route("alimentarios.$prefix[2].programacion.show",$anioPrograma->ejercicio);
      return $anioPrograma->ejercicio <config('asistenciaalimentaria.anioActual')? "<a href='$aux' class='btn btn-xs btn-info ' ><i class='fas fa-eye'></i> Mostrar</a>" : "<a href='$aux' class='btn btn-xs btn-success'><i class='fas fa-eye'></i> Continuar</a>";
    })
    ->rawColumns(['editar','mostrar']);

  }
  
  public function query(){
    return Programacion::join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
    ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
    ->where('anios_programas.programa_id',$this->programa_id)
    ->groupBy('anios_programa_id','cat_ejercicios.anio')
    ->select([
      'cat_ejercicios.anio as ejercicio',
      DB::raw('count( * ) as conteo')
    ]);
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    return
    [
      'Ejercicio'           => ['data'=>'ejercicio','name'=>'cat_ejercicios.anio','searchable'=>false],
      'TotalDeCocinas'      => ['data'=>'conteo','searchable'=>false],
      'SeleccioDeCocinas' => ['data'=>'editar','searchable'=>false,'orderable'=>false],
      'Programacion'      => ['data'=>'mostrar','searchable'=>false,'orderable'=>false],
    ];
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'lBtip',
      'buttons' => [
        //'new'
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];
    return $builderParameters;
  }

  protected function filename(){
    return 'ejercicios al ' . date('Ymd');
  }
}
