<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

use Modules\AsistenciaAlimentaria\Entities\CostoProducto;
use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

class CostosProductos extends CustomDataTable {
  public function dataTable($query){
    return datatables($query)
    ->addColumn('importetotal',function($licitacion) {
      return 0;
    })
    ->addColumn('mas',function($licitacion) {
      $ruta = route('alimentarios.licitacion.dotaciones.index',$licitacion->id);
      return "<button type='button' class='btn btn-secondary btn-success' onclick='modal.loadModal(\"$ruta\",\"cantidad_dotacion\");'><i class='fa fa-puzzle-piece' aria-hidden='true'></i></button>";
    })
    ->rawColumns(['importetotal','mas']);
  }
  
  public function query(){
    return CostoProducto::join('alim_presentaciones_productos as pp','pp.id','alim_costos_productos.presentacion_producto_id')
                    ->join('recm_cat_proveedors','recm_cat_proveedors.id','modulos_proveedores.proveedor_id')
                    ->join('cat_ejercicios','cat_ejercicios.id','alim_licitaciones.ejercicio_id')
                    ->select([
                        'alim_licitaciones.id',
                        'anio',
                        'num_licitacion',
                        'nombre',
                        'rfc'
                      ]);
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    return
    [
      'anio'            =>  ['title'=>'Ejercicio','name'=>'anio'],
      'num_licitacion'  =>  ['title'=>'Número licitación','name'=>'anio'],
      'nombre'          =>  ['title'=>'Proveedor','name'=>'nombre'],
      'rfc'             =>  ['title'=>'RFC proveedor','name'=>'rfc'],
      'importetotal'    =>  ['title'=>'Importe total','name'=>'importetotal'],
      'mas'             =>  ['title'=>'Dotaciones','name'=>'mas']
    ];
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'Btip',
      'buttons' => [
        'reset',
        'reload',
        'new'
      ],
      'lengthMenu' => [ 10 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];
    return $builderParameters;
  }

  protected function filename(){
    return 'Licitaciones al ' . date('Ymd');
  }
}