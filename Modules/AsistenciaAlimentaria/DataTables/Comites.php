<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use App\Models\CargoPrograma;

class Comites extends CustomDataTable{
  
  public function dataTable($query){
    return datatables($query)
    /* ->addColumn('acciones',function ($cargo){
      return "<button class='btn btn-warning btn-xs'>Asignar</button>"; */
    ->editColumn('persona',function($cargo){
      $miembro = strtoupper($cargo->persona);
      $badge = 'info';
      if(strpos(strtoupper($cargo->cargo),'CONTRALORIA') != FALSE)
        $badge = 'warning';
      return $cargo->persona ? "<h5><sapn class='badge badge-$badge'>$miembro</span></h5>" : "SIN ASIGNAR";
    })
    ->addColumn('acciones',function($cargo){
        $extra = 'warning';
        $badge = 'info';
        if(strpos(strtoupper($cargo->cargo),'CONTRALORIA') != FALSE) {
            $extra = 'info';
            $badge = 'warning';
        }
        $edit = route('alimentarios.programacion.comites.edit',['programacion_id'=>$this->programacion->id,'comite_id'=>$cargo->comite_id]);
        $new = route('alimentarios.programacion.comites.create',$this->programacion->id);
        return $cargo->persona ? 
        "<button onClick='modal.moreModal(\"$new\",\"create\",{persona_id : $cargo->persona_id,cargo_programa_id : $cargo->id})' class='btn btn-$badge btn-sm'><i class='fas fa-edit'></i> Editar</button>
        <button class='btn btn-dark btn-sm' onClick='modal.moreModal(\"$edit\",\"edit\",)'><i class='fas fa-exchange-alt'></i> Cambiar</button>
        <button class='btn btn-danger btn-sm' onClick='transaccion.formAjax(\"/alimentarios/programacion/".$this->programacion->id."/comites/$cargo->persona_id\",\"DELETE\",\"-asignacion_comites\")'><i class='fas fa-thumbs-down'></i> Eliminar</button>"
        : "<button class='btn btn-$badge btn-xs' onClick='modal.moreModal(\"$new\",\"create\",{cargo_programa_id : $cargo->id})'>Asignar</button>";
    })
    ->rawColumns(['persona','acciones']);
  }
  
  public function query(){
    $model = CargoPrograma::crossJoin('alim_programacion as ap')
    ->join('cat_cargos','cat_cargos.id','cargos_programas.cargo_id')
    ->leftjoin('alim_comites',function($query){
      $query->on('alim_comites.cargo_programa_id','cargos_programas.id')
      ->on('ap.id','alim_comites.programacion_id')
      ->whereNull('alim_comites.deleted_at');
    })
    ->leftjoin('alim_programacion','alim_programacion.id','alim_comites.programacion_id')
    ->leftjoin('personas','personas.id','alim_comites.persona_id')
    ->where('cargos_programas.programa_id',$this->programacion->anioprograma->programa->id)
    ->where('ap.id',$this->programacion->id)
    ->whereNull('alim_comites.deleted_at')
    ->orderBy(DB::raw('1,4'))
    ->select('cargos_programas.id','cat_cargos.nombre as cargo','alim_comites.id as comite_id','cat_cargos.created_at')
    ->selectRaw('CONCAT_WS(" ",personas.nombre,primer_apellido,segundo_apellido) as persona, personas.id as persona_id');
    return $model;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->ajax([
      'url' => route('alimentarios.programacion.comites.index',$this->programacion->id),
      'type' => 'GET',
      //'data' => 'function(d) { d.key = "value"; }',
    ])
    //->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns = [];
    $columns['cargo']     = ['data' => 'cargo', 'name' => 'cat_cargos.nombre'];
    $columns['persona']   = ['data' => 'persona', 'name' => 'personas.nombre'];
    $columns['acciones']  = ['orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false];
    //$columns['acciones']  = [];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'lBtip',
      'buttons' => [
        //'new',
        //'pdf',
        //'excel',
        /* [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => ''
        ], */
        /* [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => ''
          ] */
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
      ],
      'select' => ['style'=>'os','selector' => 'td:first-child'],
      'order'  => [[ 1, 'asc' ]],
      'drawCallback' => 'function() { 
        $(".icheck").each( function () {  
          var aux = $(this).iCheck({
            checkboxClass: "icheckbox_square-green",
            increaseArea: "10%"
          });
          aux.on("ifToggled", function(event){
            cambiarSeleccion($(this).attr("name"),$(this).prop("checked"),$(this).attr("programacion_id"));
          })
        });
      }'
    ];
    if($this->tipo == 'refrendo')
      $builderParameters['dom']='<"toolbar">tip';
    return $builderParameters;
  }

  protected function filename(){
    return 'CCNC al ' . date('Ymd');
  }
}
