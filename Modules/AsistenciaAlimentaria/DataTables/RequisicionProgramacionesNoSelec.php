<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Programacion;

class RequisicionProgramacionesNoSelec extends CustomDataTable{
  
  public function dataTable($query){
    $tabla = datatables($query);
    return $tabla;
  }
  
  public function query(){
    return Programacion::join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
    ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
    ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
    ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
    ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
    ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
    ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
    ->join('estados_programas','estados_programas.id','alim_programacion.estado_programa_id')
    ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
    ->where('alim_alimentarios.programa_id',$this->requisicion->programa_id)
    ->where('cat_ejercicios.anio',config('asistenciaalimentaria.anioActual'))
    ->where('cat_estados.nombre','ACTIVA - BLOQUEADA')
    ->where(function($query){
      $query->whereRaw('(select count(*) from alim_beneficiarios where programacion_id=alim_programacion.id and alim_beneficiarios.deleted_at is null) < 30')
      ->orWhereRaw('(select count(*) from alim_convenios where programacion_id=alim_programacion.id and alim_convenios.deleted_at is null) < 1')
      ->orWhereRaw('(select count(*) from alim_comites '.
                    'inner join cargos_programas on cargos_programas.id = alim_comites.cargo_programa_id '.
                    'inner join cat_cargos on cat_cargos.id = cargos_programas.cargo_id '.
                    'where programacion_id=alim_programacion.id '.
                    'and cargos_programas.programa_id='.$this->requisicion->programa_id.' '.
                    'and alim_comites.deleted_at is null '.
                    'and (nombre = "PRESIDENTE(A)" './/Para cocinas
                    'or nombre = "VOCAL DE ABASTO Y ALMACÉN" './/Para cocinas
                    'or nombre = "VOCAL DE ORIENTACIÓN ALIMENTARIA Y SALUD" './/Para cocinas
                    'or nombre = "SECRETARIO(A)" './/Para sujetos
                    'or nombre = "TESORERO(A)" './/Para sujetos
                    ')) < 3');
    })
    ->select([
      'alim_alimentarios.foliodif as folio',
      'cat_municipios.nombre as municipio',
      'cat_localidades.nombre as localidad',
      'cat_distritos.nombre as distrito',
      'alim_alimentarios.sublocalidad as sublocalidad',
      'cat_regiones.nombre as region',
      'alim_programacion.id as programacion_id',
      DB::raw('(select count(*) from alim_beneficiarios where programacion_id=alim_programacion.id and alim_beneficiarios.deleted_at is null) as b'),
      DB::raw('(select count(*) from alim_convenios where programacion_id=alim_programacion.id and alim_convenios.deleted_at is null) as con'),
      DB::raw('(select count(*) from alim_comites '.
              'inner join cargos_programas on cargos_programas.id = alim_comites.cargo_programa_id '.
              'inner join cat_cargos on cat_cargos.id = cargos_programas.cargo_id '.
              'where programacion_id=alim_programacion.id '.
              'and cargos_programas.programa_id='.$this->requisicion->programa_id.' '.
              'and alim_comites.deleted_at is null '.
              'and (nombre = "PRESIDENTE(A)" './/Para cocinas
              'or nombre = "VOCAL DE ABASTO Y ALMACÉN" './/Para cocinas
              'or nombre = "VOCAL DE ORIENTACIÓN ALIMENTARIA Y SALUD" './/Para cocinas
              'or nombre = "SECRETARIO(A)" './/Para sujetos
              'or nombre = "TESORERO(A)" './/Para sujetos
              ')) as com'),
    ]);
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->ajax([
      'url' => route('alimentarios.programacion.requisiciones.not',$this->requisicion->id),
      'type' => 'GET',
    ])
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns =
    [
      'folio' => ['data' => 'folio', 'name' => 'alim_alimentarios.foliodif'],
      'municipio'  =>  ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
      'localidad' =>  ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
      'distrito' =>  ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
      'sublocalidad' =>  ['data' => 'sublocalidad', 'name' => 'alim_alimentarios.sublocalidad'],
      'region' =>  ['data' => 'region', 'name' => 'cat_regiones.nombre'],
      'b' =>  ['title' => '# Beneficiarios', 'data' => 'b','searchable'=>false],
      'com' =>  ['title' => '# Miembros del comité', 'data' => 'com','searchable'=>false],
      'con' =>  ['title' => '# Convenio', 'data' => 'con','searchable'=>false]
    ];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'buttons'=>['colvis'=>'Columnas visibles']
      ],
      'dom' => 'lBtip',
      'buttons' => [
        'reset', 'reload'
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];
    
    return $builderParameters;
  }

  protected function filename(){
    return 'Programaciones no disponibles al ' . date('Ymd');
  }
}
