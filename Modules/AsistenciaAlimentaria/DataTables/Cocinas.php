<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\Desayuno;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use App\Models\Empleado;

class Cocinas extends CustomDataTable{
  
  public function dataTable($query){
    return datatables($query)
    ->addColumn('seleccionado',function ($alimentario){
      $aux = $alimentario->activa ? "checked='$alimentario->activa'" : ""; 
      return "<input type='checkbox' class='icheck' name='$alimentario->id' id='$alimentario->id' $aux programacion_id='$alimentario->activa'>";
    })
    ->editColumn('sublocalidad',function($alimentario) {
      return strtoupper($alimentario->sublocalidad);
    })
    ->editColumn('essedesol',function($alimentario) {
      $badge = 'badge-danger';
      if($alimentario->essedesol == 'SI')
        $badge = 'badge-success';
      return '<span class="badge '.$badge.'">'.$alimentario->essedesol.'</span>';
    })
    ->editColumn('esampliacion',function($programaciones) {
        $badge = 'badge-danger';
        if($programaciones->esampliacion == 'SI')
          $badge = 'badge-success';
        return '<span class="badge '.$badge.'">'.$programaciones->esampliacion.'</span>';
      })
    ->editColumn('esdesarrollo',function($alimentario) {
      $badge = 'badge-danger';
      if($alimentario->esdesarrollo == 'SI')
        $badge = 'badge-success';
      return '<span class="badge '.$badge.'">'.$alimentario->esdesarrollo.'</span>';
    })
    ->addColumn('escuela',function($alimentario) {
      $desayuno = Desayuno::where('alimentario_id',$alimentario->id)->first();
      return  $desayuno ? strtoupper($desayuno->escuela->nombre) : '';
		})
		->addColumn('extension',function($alimentario) {
      $desayuno = Desayuno::where('alimentario_id',$alimentario->id)->first();
      return  $desayuno ? ($desayuno->escuela->padre ? $desayuno->escuela->padre->nombre : '') : '';
    })
    ->addColumn('actualizar',function($alimentario) {
      $alim = 'ccnc';
      if($this->programa_id == config('asistenciaalimentaria.sujetosId'))
				$alim = 'sujetos';
			else if($this->programa_id == config('asistenciaalimentaria.desayunosId'))
				$alim = 'desayunos';
      return  '<a class="btn btn-info btn-sm" href="'.route('alimentarios.'.$alim.'.create',['alim' => $alimentario->id]).'"><i class="fa  fa-eye"></a>';
    })
    ->rawColumns(['seleccionado','essedesol','esampliacion','esdesarrollo','actualizar','baja']);
  }
  
  public function query(){
    $select_array = [
      'alim_alimentarios.id',
      'alim_alimentarios.foliodif',
      'cat_municipios.id as clv_mun',
      'cat_municipios.nombre as municipio',
      //'cat_localidades.cveinegi as clv_loc',
      DB::raw('if(alim_alimentarios.essedesol = 1,"SI","NO") as essedesol'),
      DB::raw('if(alim_programacion.esampliacion = 1,"SI","NO") as esampliacion'),
      'cat_localidades.nombre as localidad',
      'alim_alimentarios.sublocalidad'
    ];
    $model = Alimentario::leftjoin('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
    ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
    ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
		->leftjoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
		->join('alim_programacion','alim_programacion.alimentario_id','alim_alimentarios.id');
    if($this->programa_id == config('asistenciaalimentaria.sujetosId')) {
      $select_array['esdesarrollo'] = DB::raw('if(alim_sujetos.dedesarrollo_programa_id = 1,"SI","NO") as esdesarrollo');
      $select_array['nombre_asociacion'] = 'nombre_asociacion';
      $model->leftJoin('alim_sujetos','alim_sujetos.alimentario_id','alim_alimentarios.id');
      //Si el usuario pertenece a otra área que no sea ASISTENCIA ALIMENTARIA
      if(!auth()->user()->hasRoles(['SUPERADMIN'])) {
        $area = Empleado::join('cat_areas','cat_areas.id','empleados.area_id')
        ->where('cat_areas.nombre','like','%ALIMEN%')
        ->where('empleados.persona_id',auth()->user()->persona->id)
        ->first() ? true : false;
        if(!$area)
          $model->where('alim_sujetos.dedesarrollo_programa_id',1);
        else
          $model->where('alim_sujetos.dedesarrollo_programa_id','!=',1);
      }
    }
    $model->where('alim_alimentarios.programa_id',$this->programa_id);
    //Si está asignado a una región el usuario
    if(auth()->user()->regionusuario)
      $model->where('cat_regiones.id',auth()->user()->regionusuario->region_id);
    $model->select($select_array);
    if ($this->tipo == "refrendo"){
      $model->select(
        'alim_alimentarios.id',
        'alim_alimentarios.foliodif',
        'cat_municipios.id as clv_mun',
        'cat_municipios.nombre as municipio',
        //'cat_localidades.cveinegi as clv_loc',
        DB::raw('if(alim_alimentarios.essedesol = 1,"SI","NO") as essedesol'),
        DB::raw('if(alim_programacion.esampliacion = 1,"SI","NO") as esampliacion'),
        'cat_localidades.nombre as localidad',
        'alim_alimentarios.sublocalidad',
        DB::raw("(SELECT p.id,p.esampliacion FROM alim_programacion p, anios_programas ap,cat_ejercicios e where alimentario_id = alim_alimentarios.id and ap.id=p.anios_programa_id and ap.ejercicio_id=e.id and e.anio = $this->anio) as activa")
      );
    }
    return $model;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns = [];
    if($this->tipo == 'refrendo')
      $columns['seleccionado']   = ['data' => 'seleccionado','searcheable'=> false,'orderable'  => false,'exportable' => false];
    $columns['folio']            = ['data' => 'foliodif', 'name' => 'alim_alimentarios.foliodif'];
    $columns['clave municipio']  = ['data' => 'clv_mun', 'name' => 'cat_municipios.id'];
    $columns['municipio']        = ['data' => 'municipio', 'name' => 'cat_municipios.nombre'];
    $columns['localidad']        = ['data' => 'localidad', 'name' => 'cat_localidades.nombre'];
    $columns['sublocalidad']     = ['data' => 'sublocalidad', 'name' => 'alim_alimentarios.sublocalidad'];
    if($this->programa_id == config('asistenciaalimentaria.ccncId')) {
      $columns['escuela']        = ['title' => 'Ubicada en la escuela', 'data' => 'escuela', 'searcheable'=> false,'orderable'  => false,'exportable' => false];
      $columns['essedesol']      = ['title' =>'¿Proviene de SEDESOL?', 'data' => 'essedesol', 'name' => 'alim_alimentarios.essedesol'];
      $columns['esampliacion']      = ['title' =>'¿Ampliacion?', 'data' => 'esampliacion', 'name' => 'alim_programacion.esampliacion'];
    } else if($this->programa_id == config('asistenciaalimentaria.desayunosId')) {
			$columns['escuela']        = ['title' => 'Ubicada en la escuela', 'data' => 'escuela', 'searcheable'=> false,'orderable'  => false,'exportable' => false];
			$columns['extension']      = ['title' => 'Extensión', 'data' => 'extension', 'searcheable'=> false,'orderable'  => false,'exportable' => false];
    } else if ($this->programa_id == config('asistenciaalimentaria.sujetosId'))  {
      $columns['esdesarrollo']   = ['title' => 'Pertenece a Desarrollo', 'data' => 'esdesarrollo', 'name' => 'alim_sujetos.dedesarrollo_programa_id'];
      $columns['nombre_asociacion'] = ['name' => 'alim_sujetos.nombre_asociacion'];
      $columns['esampliacion']      = ['title' =>'¿Ampliacion?', 'data' => 'esampliacion', 'name' => 'alim_programacion.esampliacion'];
    }
    $columns['actualizar']       = ['data' => 'actualizar', 'searcheable'=> false,'orderable'  => false,'exportable' => false];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'lBtip',
      'buttons' => [
        //'new',
        'pdf' , 'excel',
        'reset', 'reload',
        'create'
        /* [
          'text' => '<i class="fa fa-plus"></i> Nueva cocina',
          'action' => 'function ( e, dt, node, config ) { irACrearCCNC(); }',
        ] */
        /* [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => ''
        ], */
        /* [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => ''
          ] */
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
      ],
      'select' => ['style'=>'os','selector' => 'td:first-child'],
      'order'  => [[ 1, 'asc' ]],
      'drawCallback' => 'function() { 
        $(".icheck").each( function () {  
          var aux = $(this).iCheck({
            checkboxClass: "icheckbox_square-green",
            increaseArea: "10%"
          });
          aux.on("ifToggled", function(event){
            cambiarSeleccion($(this).attr("name"),$(this).prop("checked"),$(this).attr("programacion_id"));
          })
        });
      }'
    ];
    if($this->tipo == 'refrendo')
      $builderParameters['dom']='<"toolbar">tip';
    return $builderParameters;
  }

  protected function filename(){
    return 'CCNC al ' . date('Ymd');
  }
}
