<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\OficioValidacion;

class OficiosValidacion extends CustomDataTable{
  
  public function dataTable($query){
    return datatables($query)
    ->filterColumn('fecha_oficio',function($query,$keywords) {
      $query->orWhere(DB::raw('date_format("fecha_oficio","%d/%m/%Y")'),$keywords);
    })
    ->addColumn('redirect',function($query) {
      return "<a class='btn btn-xs btn-success' href='".route('alimentarios.validacion.requisiciones.index',$query->ofi_val_id)."'><i class='nav-icon fas fa-dolly'></i> Ir</a>";
    })
    ->addColumn('eliminar',function($query) {
        $prefix = config('asistenciaalimentaria.ccncId') == $this->programa_id ? 'ccnc' : (config('asistenciaalimentaria.sujetosId') == $this->programa_id ? 'sujetos' : (config('asistenciaalimentaria.desayunosId') == $this->programa_id ? 'desayunos' : 'caics'));
        $r = route("alimentarios.$prefix.validacion.oficios.destroy",$query->ofi_val_id);
        return "<button class='btn btn-xs btn-danger' onClick='transaccion.formAjax(\"$r\",\"DELETE\",\"-oficios\")'><i class='nav-icon fas fa-trash'></i> Eliminar</button>";
    })
    ->rawColumns(['redirect','eliminar']);
  }
  
  public function query(){
    return OficioValidacion::join('cat_ejercicios','cat_ejercicios.id','alim_oficiosvalidacion.ejercicio_id')
                          ->join('programas','programas.id','alim_oficiosvalidacion.programa_id')
                          ->where('programa_id',$this->programa_id)
                          ->whereNull('alim_oficiosvalidacion.deleted_at')
                          ->select([
                            'alim_oficiosvalidacion.id as ofi_val_id',
                            'num_oficio',
                            'cat_ejercicios.anio',
                            DB::raw('date_format(fecha_oficio,"%d/%m/%Y") as fecha_oficio'),
                            DB::raw('(select count(*) from alim_validacionesentregas where oficiovalidacion_id = alim_oficiosvalidacion.id) as avance'),
                            'programas.id as prog_id'
                          ]);
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    return
    [
      'num_oficio'    => ['title'=>'Núm. Oficio','name'=>'num_oficio'],
      'anio'          => ['title'=>'Año','name'=>'fecha_oficio'],
      'fecha_oficio'  => ['title'=>'Fecha oficio','name'=>'cat_ejercicios.anio'],
      'avance'        => ['searchable' => false],
      'redirect'      => ['title'=>'Requisiciones','name'=>'redirect'],
      'eliminar'      => ['title'=>'Eliminar oficio','name'=>'eliminar'],
    ];
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'Btip',
      'buttons' => [
        'reset',
        'reload',
        'new'
      ],
      'lengthMenu' => [ 10 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];
    return $builderParameters;
  }

  protected function filename(){
    return 'Oficios de validación al ' . date('Ymd');
  }
}
