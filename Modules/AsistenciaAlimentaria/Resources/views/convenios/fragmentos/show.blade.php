<div class="modal-header mh-bordered">
    <h3 class="modal-title">Informacion de convenio</h3>
</div>

<div class="modal-body">
  <form role="form" id="form-convenios">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
          <label for="tipoconvenio_id">Tipo de convenio*</label>
          <select id="tipoconvenio_id" name="tipoconvenio_id" class="form-control select2" style="width: 100%" required readonly>
            <option value="{{ $convenio->tipoconvenio_id }}">{{ $convenio->tipoconvenio->nombre }}</option>
          </select>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
          <label for="fecha_convenio">Fecha de convenio*</label>
          <input type="text" id="fecha_convenio" name="fecha_convenio" class="form-control date-picker" placeholder="Seleccione fecha de convenio" value="{{ $convenio->fecha_convenio }}" readonly required>
        </div>
      </div>
    </div>{{-- datos convenio --}}
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
          <label for="ubicación_fisica_archivo">Ubicación física del archivo</label>
          <input type="text" id="ubicacion_fisica_archivo" name="ubicacion_fisica_archivo" class="form-control" placeholder="Ingrese fila, columna y pasillo" value="{{ $convenio->ubicacion_fisica_archivo or ''  }}" readonly>
        </div>
      </div>
      <div class="col-xs-12 col-md-6">
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <label><input name="tiene_acta_aceptacion" id="tiene_acta_aceptacion" type="checkbox" class="checkModal" disabled {{$convenio->tiene_acta_aceptacion == 1 ? 'checked' : ''}} ></label>
              <label>¿Tiene acta de aceptación?</label>
            </div>
          </div>
        </div>{{-- info de actas 1 --}}
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <label><input name="tiene_acta_comite" id="tiene_acta_comite" type="checkbox" class="checkModal" disabled {{$convenio->tiene_acta_comite== 1 ? 'checked' : ''}} ></label>
              <label>¿Tiene acta de comité?</label>
            </div>
          </div>
        </div>{{-- info de actas 2 --}}
        <div class="row">
          <div class="col-xs-12">  
            <div class="form-group">
              <label><input name="tiene_acta_contraloria" id="tiene_acta_contraloria" type="checkbox" class="checkModal" disabled {{$convenio->tiene_acta_contraloria== 1 ? 'checked' : ''}} ></label>
              <label>¿Tiene acta de contraloria?</label>
            </div>  
          </div>
        </div>
      </div>{{-- info de actas 3 --}}
    </div>{{-- datos de archivo --}}
    <div class="col-xs-12-col-md-6">
      <img src="{{asset($convenio->ubicacion_digital_archivo ? $convenio->ubicacion_digital_archivo :'storage/asistenciaAlimentaria/convenios/noDoc.jpg' )}}" class="img-conv" alt="convenio">
    </div>
  </form>
</div>

<script>
  (function () {
    $('.checkModal').iCheck({
      checkboxClass: "icheckbox_square-green",
      increaseArea: "10%",
    })
  })()
  
</script>