<div class="modal-header">
  <div class="row">
    <strong>{{ $alimentario->foliodif }}</strong>
    : {{ $alimentario->municipio->nombre }}
    - {{ $alimentario->localidad->nombre }}
    {{ isset($alimentario->sublocalida)? '/'.$alimentario->sublocalidad:'' }}
  </div>
</div>
<div class="modal-body">
  INDICADORES:
  <ul>
  @foreach ($alimentario->municipio->indicadores as $indicadormunicipio)
  @if ($indicadormunicipio->indicador->nombre !== 'POBLACIÓN TOTAL POR MUNICIPIO')
  <li>{{ $indicadormunicipio->indicador->nombre }} - {{ round($indicadormunicipio->porcentaje,2) }}%</li>
  @else
  <li>{{ $indicadormunicipio->indicador->nombre }} - {{ round($indicadormunicipio->poblacion,2) }}</li>
  @endif
  @endforeach
  </ul>
  <div id="accordion" role="tablist">
    @foreach ($alimentario->programacion as $ejercicio)
    <div class="card mb-0">
      <div class="card-header" id="headingOne" role="tab">
        <h5 class="mb-0">
          <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"
            class="collapsed">EJERCICIO {{ $ejercicio->anioprograma->ejercicio->anio }}</a>
        </h5>
      </div>
      <div class="collapse show" id="collapseOne" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
        <div class="card-body">
          <div class="col-12">
            <div class="row">
              <div class="col-6">
                <div class="row">
                  TOTAL DE DOTACIONES: {{ $ejercicio->programacionrequisiciones->reduce(function($total,$programacionrequisicion){
                    return $total + $programacionrequisicion->requisicioncantidaddotacion->reduce(function($pordotacion,$cantidaddotacion){
                      return $pordotacion + $cantidaddotacion->cantidad_dotaciones;
                    ;},0)
                  ;},0) }}
                </div>
                <div class="row">
                  INVERSION TOTAL: ${{ number_format($ejercicio->programacionrequisiciones->reduce(function($total,$programacionrequisicion){
                    return $total + $programacionrequisicion->requisicioncantidaddotacion->reduce(function($pordotacion,$cantidaddotacion){
                      return $pordotacion +$cantidaddotacion->cantidad_dotaciones*$cantidaddotacion->dotacion->detallesdotacion->reduce(function($carry,$dotacion){
                        return $carry+$dotacion->cantidad*$dotacion->costoproducto->costo;
                      },0)
                    ;},0)
                  ;},0),2) }}
                </div>
              </div>
              <div class="col-6">
                <div class="row">
                  TOTAL A RECUPERAR: ${{ number_format($ejercicio->programacionrequisiciones->reduce(function($total,$programacionrequisicion){
                    return $total + $programacionrequisicion->requisicioncantidaddotacion->reduce(function($pordotacion,$cantidaddotacion){
                      return $pordotacion + $cantidaddotacion->cantidad_beneficiarios;
                    ;},0) * 20
                  ;},0),2) }}
                </div>
                <div class="row">
                  TOTAL RECUPERADO: ${{ number_format($ejercicio->programacionrequisiciones->reduce(function($total,$programacionrequisicion){
                    return $total + $programacionrequisicion->recibo->pago->importe_total;
                  ;},0),2) }}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div id="chart"></div>{{-- <label for="">Beneficiarios</label> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>
<div class="modal-footer">

</div>
<script>
  var optionsChart = {
  chart: {
    type: 'line',
    width: '100%',
    height: '300px'
  },
  series: [
    {
      name : 'BENEFICIARIOS',
      data : {{ $ejercicio->programacionrequisiciones->map(function($programacionrequisicion){
        return $programacionrequisicion->requisicioncantidaddotacion->reduce(function($pordotacion,$cantidaddotacion){
          return $pordotacion+$cantidaddotacion->cantidad_beneficiarios;
        },0);
      }) }}
    },
    {
      name : 'DOTACIONES',
      data : {{ $ejercicio->programacionrequisiciones->map(function($programacionrequisicion){
        return $programacionrequisicion->requisicioncantidaddotacion->reduce(function($pordotacion,$cantidaddotacion){
          return $pordotacion+$cantidaddotacion->cantidad_dotaciones;
        },0);
      }) }}
    }
  ],
  xaxis: {
    categories: {!! $ejercicio->programacionrequisiciones->map(function($programacionrequisicion){
      return $programacionrequisicion->created_at->format('Y-m-d');
    }) !!}
  }
}

var chart = new ApexCharts(document.querySelector("#chart"), optionsChart);

chart.render();

</script>
