<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="municipio_id">Municipio*</label>
        <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%" required>
            @if(isset($alimentario->municipio))
                <option value="{{ $alimentario->municipio->id }}" selected>{{ $alimentario->municipio->nombre }}</option>
            @endif
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="localidad_id">Localidad*</label>
            <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%" {{ isset($alimentario) ? '' : 'disabled' }} required>
                @if(isset($alimentario->localidad))
                    <option value="{{ $alimentario->localidad->id }}" selected>{{ $alimentario->localidad->nombre }}</option>
                @endif
            </select>
        </div>
    </div>
</div>
<div class="row" required>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <input id="checkSubLocalidad" type="checkbox" {{ isset($alimentario) ? ($alimentario->sublocalidad ? 'checked' : '') : 'checked' }}>
            <label for>
                Sub-localidad
            </label>
            <input type="text" id="sublocalidad" name="sublocalidad" class="form-control" minlength="5" maxlength="100" placeholder="Ingrese nombre de la sub-localidad" value="{{ $alimentario->sublocalidad or '' }}" {{ isset($alimentario) ? ($alimentario->sublocalidad ? '' : 'disabled') : '' }} required>
        </div>
    </div>
    @if ($programa === config('asistenciaalimentaria.ccncId'))
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
                <input id="checkEscuela" type="checkbox" {{ isset($alimentario) ? ($alimentario->desayuno ? 'checked' : '') : 'checked' }}>
                <label for>
                    ¿Ubicada en una escuela?
                </label>
                <select id="escuela_id" name="escuela_id" class="form-control select2 req" style="width: 100%" {{ isset($alimentario) ? ($alimentario->desayuno ? '' : 'disabled') : '' }} required>
                    @if (isset($alimentario->desayuno))
                        <option value="{{ $alimentario->desayuno->escuela->id }}">{{ $alimentario->desayuno->escuela->nombre }}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
            <input id="checkSedesol" name="essedesol" type="checkbox" {{ isset($alimentario) && $alimentario->essedesol == 1 ? 'checked' : '' }}>
                <label for>
                    ¿Cocina SEDESOL?
                </label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
            <input id="checkAmpliacion" name="esampliacion" type="checkbox" {{ isset($alimentario) && $alimentario->programacion->last()->esampliacion == 1 ? 'checked' : '' }}>
                <label for>
                    ¿Ampliacion?
                </label>
            </div>
		</div>
        <div id="escuelaPadre" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
                    <input id="checkEscuelaPadre" type="checkbox" {{ isset($alimentario) ? (isset($alimentario->desayuno) ? ($alimentario->desayuno->escuela->extension == 1 ? 'checked' : '') : '') : '' }}>
                    <label for>
                            Extensión de
                    </label>
                    <select id="padre_id" name="padre_id" class="form-control select2 req" style="width: 100%" {{ isset($alimentario) ? (isset($alimentario->desayuno) ? ($alimentario->desayuno->escuela->extension == 1 ? '' : 'disabled') : 'disabled') : 'disabled' }} required>
                            @if (isset($alimentario) && isset($alimentario->desayuno) && $alimentario->desayuno->escuela->extension == 1)
                                    <option value="{{ $alimentario->desayuno->escuela->padre->id }}">{{ $alimentario->desayuno->escuela->padre->nombre }}</option>
                            @endif
                    </select>
            </div>
        </div>
    @endif
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="financiamiento_id">Financiamiento*</label>
            <select id="financiamiento_id" name="financiamiento_id" class="form-control select2 req" style="width: 100%" required>
                @if (isset($alimentario))
                    @if ($alimentario->programacion->last()->financiamiento)
                        <option value="{{ $alimentario->programacion->last()->financiamiento->id }}">{{ $alimentario->programacion->last()->financiamiento->nombre }}</option>
                    @endif
                @endif
            </select>
        </div>
    </div>
</div>