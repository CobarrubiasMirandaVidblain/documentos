@extends('asistenciaalimentaria::layouts.master')

@section('title','CCNC-Crear programacion')

@section('content-subtitle', 'Seleccion de Cocinas')

@push('head')
@endpush

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Tabla de cocinas:</h3>
		</div>
		<div class="box-body">
      <div id="tools" class="">
        {{-- <div class="dt-buttons btn-group"> --}}
        <input type="checkbox" id="All" name="All">
        <span><label for="selectAll">Seleccionar todo</label></span>
        <input type="checkbox" id="Last" name="All">
        <span><label for="selectAll">Seleccionar {{ $anio-1 }}</label></span>
        <input type="checkbox" id="Active" name="All">
        <span><label for="selectAll">Seleccionar {{ $anio-1 }} activas</label></span>
        {{-- </div> --}}
      </div>
			<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
				<input type="text" id="search" name="search" class="form-control">
				<div class="input-group-append">
					<button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
        </div>
			</div>

			{!! $dataTable->table(['class' => 'table table-bordered table-striped dt-responsive nowrap', 'style'=>'width:100%', 'id' => 'cocinas']) !!}

		</div>
	</div>
@stop

@push('body')
	<script type="text/javascript">
    //iniciar tabla
		var tabla = ((tablaId) => {
			$('#search').keypress(function(e) {
				if(e.which === 13) {
					$(tablaId).DataTable().search($('#search').val()).draw()
				}
			})
			$('#btn_buscar').on('click', function() {
				$(tablaId).DataTable().search($('#search').val()).draw()
			})
			function recargar() {
				$(tablaId).DataTable().ajax.reload(null,false);
			}
			return {
				recargar : recargar
		}})('#cocinas'); 
    //iniciar icheks
		(function () {
			$('input[name=All]').iCheck({
				checkboxClass: "icheckbox_square-green",
				increaseArea: "10%"
			});
			$('input[name=All]').on("ifClicked", function(event){
				if ($(event.target).prop('checked')) {
					cambiarSeleccion($(this).attr("id"),false);
				} else {
					$('input[name=All]').each(function (index,item) {
						$(item).iCheck('uncheck');
					});
					$(event.target).iCheck('check');
				}
			});
			$('input[name=All]').on("ifChecked", function(event){
				cambiarSeleccion($(this).attr("id"),$(this).prop("checked"));
      });
		})()
    //seleccionar o deseleccionar
		function cambiarSeleccion(id,estado) {
			let ruta = location.pathname.split('/').slice(0,-2).join('/') + (estado ? '':'/'+arguments[arguments.length-1]),
			metodo   = estado ? 'POST' : 'DELETE',
			datos    = {alimentario_id : id};
			transaccion.miniAjax(ruta, metodo, datos);
    }
    //exito en las operaciones anteriores
		function ajaxSuccess(params) {
			tabla.recargar();
    }
    //fallo en las operaciones anteriores
		function ajaxError(params) {
			tabla.recargar();
    }
    $('#cocinas').on( 'init.dt', function () {
      $('#cocinas').DataTable().table().container().prepend($('#tools')[0]);
    } )
	</script>

	{!! $dataTable->scripts() !!}
@endpush
