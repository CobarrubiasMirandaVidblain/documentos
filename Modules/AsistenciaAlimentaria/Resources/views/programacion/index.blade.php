@extends('asistenciaalimentaria::layouts.master')

@section('title','CCNC-Programaciones')

@section('content-subtitle', 'Tabla de Cocinas')

@push('head')
	
@endpush

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Lista de Ejercicios</h3>
		</div>
		<div class="box-body">

      <div class="card">
          <div class="card-header">
          <i class="fa fa-align-justify"></i> Cocinas
          {{-- <div class="card-header-actions">
            <a class="card-header-action" href="https://datatables.net" target="_blank">
            <small class="text-muted">docs</small>
            </a>
          </div> --}}
        </div>
        <div class="card-body">
          <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
            <input type="text" id="search" name="search" class="form-control">
            <div class="input-group-append">
              <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
            </div>
          </div>
          {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'beneficiarios']) !!}
        </div>
      </div>

		</div>
	</div>
@stop

@push('body')
	
	{!! $dataTable->scripts() !!}

  <script type="text/javascript">
  
    /* (function ($,DataTable){
      DataTable.ext.buttons.new = {
        className: '',
        text: function (dt) {
          return  '<i class="fa fa-plus"></i> AGREGAR'; //+ dt.i18n('buttons.print', 'Print');
        },
        action: function (e,dt,button,config) {
          //cargarModal(url);
          window.location.href = "{{ route('alimentarios.ccnc.programacion.create') }}"
        }
      };
    })(jQuery, jQuery.fn.dataTable); */

    /*(function(tablaId) {
      $('#cocinas tbody').on( 'click', 'tr', function () {
        $(tablaId).DataTable().$('tr.info').removeClass('info');
        $(this).addClass('info');
      });
    })('#cocinas')*/;

  </script>

@endpush
