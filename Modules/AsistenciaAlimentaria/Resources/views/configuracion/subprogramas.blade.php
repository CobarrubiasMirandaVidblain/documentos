<div class="box box-primary shadow">
  <div class="box-header with-border">
    <h3 class="box-title">{{ $programa->nombre }}</h3>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-xs-12">
        <label>{{ $programa->subprogramas->count() }} sub programas activos.</label>
        <button class="btn btn-warning btn-sm pull-right" onclick="addPrograma()"><i class="fa fa-gear"></i> Configurar subprogramas</button>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-12">
        <label for="">método de financiamiento predeterminado</label>
        <select name="" id="" class="pull-right">
          @foreach ($tipos_finan as $finan)
              <option value="{{ $finan->id }}">{{ $finan->nombre }}</option>
          @endforeach
        </select>
      </div>
    </div>
    {{-- <form id="programas" class="col-xs-12">
      @foreach ($programa->subprogramas as $subp)
        <div class="row">
            {{ $subp->nombre }}
        </div>
        <div class="row">
          <div class="form-group">
            <label for="">Fuente de financiamiento <strong>{{ strtolower($programa->nombre) }}</strong></label>
            <select name="" id="" class="select2 form-control">
            </select>
          </div>
        </div>
      @endForeach
    </form> --}}
  </div>
</div>