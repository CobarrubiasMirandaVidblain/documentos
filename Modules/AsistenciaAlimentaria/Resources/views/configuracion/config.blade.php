@extends('asistenciaalimentaria::layouts.master')
@push('head')
	<style type="text/css">
		
	</style>
@endPush
@section('content-subtitle','CONFIGURACIÓN')
@section('content')
	<div class="card">
		<div class="card-header">
			<i class="fas fa-users"></i>Usuarios
		</div>
		<div class="card-body">
			<div class="col-xs-12">
				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
					<input type="text" id="search" name="search" class="form-control">
					<div class="input-group-append">
						<button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
					</div>
				</div>
				{!! $dataTable->table(['class' => 'table table-dark table-sm table-bordered dark-table table-striped dt-responsive nowrap', 'style'=>'width:100%', 'id' => 'usuarios']) !!}
			</div>
		</div>
    </div>
@endSection
@push('body')
{!! $dataTable->scripts() !!}
<script>
	var tabla = ((tablaId) => {
		$('#search').keypress(function(e) {
			if(e.which === 13) {
				$(tablaId).DataTable().search($('#search').val()).draw()
			}
		})
		$('#btn_buscar').on('click', function() {
			$(tablaId).DataTable().search($('#search').val()).draw()
		})
		function recargar() {
			$(tablaId).DataTable().ajax.reload(null,false);
		}
		return {
			recargar : recargar
		}
	})('#usuarios');
	function ajaxSuccess(response) {
		tabla.recargar()
		Swal.fire('¡Guardado!','Operación realizada con éxito','success')
		$('.modal').modal('hide')
	}
	function ajaxError(response) {
		tabla.recargar()
		Swal.fire('¡Error!','Algo salió mal','error')
	}
	$('.select2').select2();  
</script>
@endPush