<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Registrar capacitación</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form role="form">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="nombre_asociacion">Tema*</label>
                    <input type="text" id="nombre_asociacion" name="nombre_asociacion" class="form-control" placeholder="Ingrese nombre del tema a tratar">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="folio">Prueba*</label>
                    <input type="text" id="folio" name="folio" class="form-control" placeholder="Ingrese folio">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="alimentario_id">Prueba*</label>
                    <select id="alimentario_id" name="alimentario_id" class="form-control select2" style="width: 100%">
                    </select>
                </div>
            </div>
        </form>
    </div>
    <!-- /.box-body -->
</div>