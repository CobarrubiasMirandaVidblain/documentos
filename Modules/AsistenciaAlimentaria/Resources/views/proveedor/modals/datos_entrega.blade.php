@php
    $alimentario = $programacionrequisicion->programacion->alimentario;
@endphp
<div class="modal-header with-border">
    <h4 class="modal-title">{{ isset($programacionrequisicion->requisicionDatoCarga->entregaproveedor) ? 'Actualizar' : 'Registrar' }} datos de entrega</h4>
</div>
  {{-- modal-header --}}
<div class="modal-body">
  <form id="form-entregas">
    <h5>Datos de la programación con foliodif {{$alimentario->foliodif}}</h5>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="municipio">Municipio:</label>
          <input type="text" class="form-control" id="municipio" value="{{ $alimentario->municipio->nombre or '' }}" readonly/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="localidad">Localidad:</label>
          <input type="text" class="form-control" id="localidad" value="{{ $alimentario->localidad->nombre or '' }}" readonly/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="sublocalidad">Sublocalidad:</label>
          <input type="text" class="form-control" id="sublocalidad" value="{{ $alimentario->sublocalidad or '' }}" readonly/>
        </div>
      </div>
    </div>
    <h5>Datos de generales</h5>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="entrega">¿Quién entrega?</label>
          <input type="text" class="form-control" id="entrega" value="{{ $programacionrequisicion->requisicionDatoCarga->carga->chofer->getNombreCompleto() }}" readonly/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="placas">Placas del vehículo del proveedor:</label>
          <input type="text" class="form-control" id="placas" value="{{ $programacionrequisicion->requisicionDatoCarga->carga->placas }}" readonly/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="ruta">Ruta:</label>
          <input type="text" class="form-control" id="ruta" value="{{ $programacionrequisicion->requisicionDatoCarga->carga->ruta }}" readonly/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="fecha_entrega">*Fecha de entrega:</label>
          <input type="text" class="form-control mask" id="fecha_entrega" name="fecha_entrega" data-inputmask='"mask": "99/99/9999"' value="{{ isset($programacionrequisicion->requisicionDatoCarga->entregaproveedor) ? $programacionrequisicion->requisicionDatoCarga->entregaproveedor->getFormatFecha() : '' }}" {{-- isset($programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor) ? 'disabled' : '' --}} required/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
            <input id="checkEntregado" type="checkbox" name="entregado" checked/>
            <label for="checkEntregado">
                ¿Entregado?
            </label>
        </div>
      </div>
      @if ($programacionrequisicion->requisicionDatoCarga->entregaproveedor)
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="form-group">
                <label for="checkRecibio">
                    Recibió: <strong>{{ $programacionrequisicion->requisicionDatoCarga->entregaproveedor->observaciones ? $programacionrequisicion->requisicionDatoCarga->entregaproveedor->observaciones :  ($programacionrequisicion->requisicionDatoCarga->entregaproveedor->comite->persona->nombreCompleto() .' - '.$programacionrequisicion->requisicionDatoCarga->entregaproveedor->comite->cargoprograma->cargo->nombre) }}</strong>
                </label>
            </div>
        </div>
      @endif
    </div>
    <div class="row" id="comite">
      <div class="col-xs-12 col-md-6 col-lg-12">
        <div class="form-group">
          <input id="checkOtro" type="checkbox" value="otro" checked/>
          <label for="checkOtro">
              Si la persona quien recibe pertenece al comité elegir una opción, si no, desmarcar esta casilla.
          </label>
          <select type="text" class="form-control" id="miembro" name="recibe_comite_id" placeholder="NOMBRE DE QUIEN RECIBE" required>
            {{-- value="{{ isset($programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor) ? $programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor->comite->persona->nombreCompleto() : '' }}" --}}
            @foreach ($programacionrequisicion->programacion->comite as $m)
              @if ($programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor)
                @if ($programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor->recibe_comite_id == $m->id)
                <option value="{{ $m->id }}" selected>({{ $m->persona->nombreCompleto() }}) ({{ $m->cargoprograma->cargo->nombre }})</option>
                @endif
              @else
                <option value="{{ $m->id }}">({{ $m->persona->nombreCompleto() }}) ({{ $m->cargoprograma->cargo->nombre }})</option>
              @endif
            @endforeach
            {{-- @if ($programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor)
              @if ($programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor->otrocomite)
                <option value="{{ $programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor->otrocomite->id }}" selected>({{ $programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor->otrocomite->nombreCompleto() }}) ({{ $programacionrequisicion->requisicionDatoCarga->carga->entregaproveedor->otrocomite->cargo }})</option>
              @endif
            @endif --}}
          </select>
        </div>
      </div>      
    </div>
    <div class="row" id="otro-comite">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="n">*Nombre de la persona que recibe:</label>
          <input type="text" class="form-control" id="n" name="nombre" disabled required/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="pa">*Primer apellido de la persona que recibe:</label>
          <input type="text" class="form-control" id="pa" name="primer_apellido" disabled required/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="sa">Segundo apellido de la persona que recibe:</label>
          <input type="text" class="form-control" id="sa" name="segundo_apellido" disabled/>
        </div>
      </div> 
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="cargo">*Cargo de la persona que recibe:</label>
          <input type="text" class="form-control" id="cargo" name="cargo" disabled required/>
        </div>
      </div>       
    </div>
  </form>
</div>
{{-- modal-body --}}
<div class="modal-footer">
    <button class="btn btn-secondary pull-left mr-auto" tabindex="0" data-custom-dismiss="modal"><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    <label>Los campos con * son obligatorios</label>
    @php
        $r = route('alimentarios.proveedor.requisicion.entregas.store',[$proveedor_id,$requisicion_id]);
    @endphp
    @if ($programacionrequisicion->requisicionDatoCarga->entregaproveedor)
      @if (!$programacionrequisicion->requisicionDatoCarga->entregaproveedor->validacionentrega)
        @php
            $r = route('alimentarios.proveedor.requisicion.entregas.update',[$proveedor_id,$requisicion_id,$programacionrequisicion->requisicionDatoCarga->entregaproveedor->id]);
        @endphp
        <button class="btn pull-right button-dt tool" onclick="transaccion.formAjax('{{ $r }}','PUT','form-entregas',{requisicion_datocarga_id:'{{$programacionrequisicion->requisicionDatoCarga->id}}'},false)"><span><i class="fa fa-save"></i> Guardar</span></button>      
      @else
        <script>
            $('#form-entregas input').attr('disabled',true)
            $('#form-entregas select').attr('disabled',true)
        </script>
      @endif
    @else
        <button class="btn pull-right button-dt tool" onclick="transaccion.formAjax('{{ $r }}','POST','form-entregas',{requisicion_datocarga_id:'{{$programacionrequisicion->requisicionDatoCarga->id}}'},false)"><span><i class="fa fa-save"></i> Guardar</span></button>    
    @endif
</div>
<script>
    (function() {
      validacion.iniciar('form-entregas',{nombre:{pattern_nombre:''},primer_apellido:{pattern_apellido:''},segundo_apellido:{pattern_segundo_apellido:''}})
      $('#otro-comite').hide()
      $('#checkOtro').iCheck({
        checkboxClass: "icheckbox_square-green",
        increaseArea: "10%"
      }).on('ifChanged', function (e) {
        if(e.target.checked) {
          $('#miembro').prop('disabled',false)
          $('#otro-comite input').prop('disabled', true)
          $('#otro-comite').hide()
        } else {
          $('#miembro').prop('disabled',true)
          $('#otro-comite input').prop('disabled', false)
          $('#otro-comite').show()
        }
      })
      $('#checkEntregado').iCheck({
        checkboxClass: "icheckbox_square-green",
        increaseArea: "10%"
      })
      $('#fecha_entrega').datepicker({
          autoclose: true,
          language: 'es',
          format: 'dd/mm/yyyy',
          startDate: moment('2019-01-01').format('DD/MM/YYYY'),
          endDate: moment().format('DD/MM/YYYY')
      })
      $('.mask').inputmask()
      var tablaB = ((tablaId) => {
          $('#search').keypress(function(e) {
              if(e.which === 13) {
                  $(tablaId).DataTable().search($('#search').val()).draw()
              }
          })
          $('#btn_buscar').on('click', function() {
              $(tablaId).DataTable().search($('#search').val()).draw()
          })
          function recargar(params) {
              $(tablaId).DataTable().ajax.reload(null,false);
          }
          return {
              recargar : recargar
      }})('#toentregas')
      if('{{$programacionrequisicion->estadoprograma->estado->nombre}}'=='NO RECIBIDA')
        $('#checkEntregado').iCheck('uncheck')
    })()
</script>