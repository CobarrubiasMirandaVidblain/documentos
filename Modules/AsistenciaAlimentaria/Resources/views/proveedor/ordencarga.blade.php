@extends('asistenciaalimentaria::layouts.master')

@section('content-subtitle', 'Orden de carga')

@push('head')
  <style>
    table.dataTable tbody>tr.selected,
    table.dataTable tbody>tr>.selected {
      background-color:#0275d8 !important;
    }

  </style>
@endpush

@section('content')
    <div class="card">
        <div class="card-header with-border text-center">
                <h4>Selección programaciones</h4>
        </div>
        <div class="card-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                <input type="text" id="search" name="search" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
                </div>
            </div>
            {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'orden'],true) !!}
        </div>
    </div>
@stop

@push('body')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
  var key = "datos{{$requisicion_id}}";
  function addEntry(newItem) {
    // Parse any JSON previously stored in datos
    var existingEntries = JSON.parse(localStorage.getItem(key));
    if(existingEntries == null) existingEntries = [];
    console.log(newItem)
    existingEntries.push(JSON.stringify(newItem));
    localStorage.setItem(key, JSON.stringify(existingEntries));
  }
  function removeEntry(removeItem) {
    console.log(removeItem)
    var existingEntries = JSON.parse(JSON.stringify(localStorage.getItem(key)));
    if(existingEntries == null) return;
    var array = JSON.parse(localStorage.getItem(key));
    array = jQuery.grep(array, function(value) {
      v = JSON.parse(value);
      return v.id != removeItem;
    });
    localStorage.setItem(key, JSON.stringify(array));
  }
  function selectItems() {
    $.unblockUI()
    
    var array = JSON.parse(localStorage.getItem(key)) || [];
    array.forEach(element => {
      v = JSON.parse(element);
      $('#'+(v.id)).addClass('selected');
    });
    $('#orden').DataTable().rows('.selected').select();
    $.unblockUI();
  }
  
  var tabla = 
      ((tablaId) => {
      $('#search').keypress(function(e) {
          if(e.which === 13) {
              $(tablaId).DataTable().search($('#search').val()).draw();
          }
      })
      $('#btn_buscar').on('click', function() {
          $(tablaId).DataTable().search($('#search').val()).draw();
      })
      $(tablaId).DataTable().on( "user-select", function ( e, dt, type, cell, originalEvent ) {
        if ( $(originalEvent.target).index() === 0 ) {
            e.preventDefault();
        } else {
          if ( type === 'row' && !$(originalEvent.target).closest('tr').hasClass('selected')) {
            var data = $(tablaId).DataTable().rows( [cell[0][0].row] ).data();
            addEntry({id:data.pluck('req_prog_id')[0],localidad:data.pluck('localidad')[0],municipio:data.pluck('municipio')[0]},"datos{{$requisicion_id}}");
          }
        }
      });
      $(tablaId).DataTable().on( 'deselect', function ( e, dt, type, indexes ) {
        if ( type === 'row' ) {
          removeEntry($(tablaId).DataTable().rows( indexes ).data().pluck('req_prog_id')[0],"datos{{$requisicion_id}}");
        }
      });
      function recargar(params) {
          $(tablaId).DataTable().ajax.reload(null,false);
      }
      return {
          recargar : recargar
  }})('#orden');

  function ajaxSuccess(response,tabla) {
    $('#orden').DataTable().rows('.selected').deselect();
    localStorage.removeItem(key);
    window.LaravelDataTables[tabla].ajax.reload();
    $('.modal').modal('hide');
    Swal.fire('¡Guardado!','Operación realizada con éxito','success')
  }
  function ajaxError(response) {
    tabla.recargar();
    //console.log(response.responseJSON.message);
    Swal.fire('¡Error!',response.responseJSON.message,'error')
  }
</script>
@endpush