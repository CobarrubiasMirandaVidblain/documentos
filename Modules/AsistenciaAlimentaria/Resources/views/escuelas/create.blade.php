@extends('asistenciaalimentaria::layouts.master')

@push('head')
    <link rel="stylesheet" href="../../plugins/iCheck/all.css">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-title', 'Escuelas')
@section('content-subtitle', 'Asistencia alimentaria')

@section('content')
    @include('asistenciaalimentaria::escuelas.fragmentos.crear_editar')
@stop
@push('body')
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
    <script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } })
        
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        }).on('ifChanged', function (e) {
            if(e.target.checked) {
                $('#sublocalidad').prop('disabled',false)
                $('#sublocalidad').addClass('req')
            } else {
                $('#sublocalidad').prop('disabled',true)
                $('#sublocalidad').removeClass('req')
                $('#sublocalidad').val('')
            }
        })

        $('#municipio_id').select2({
            language: 'es',
            minimumInputLength: 2,
            allowClear : true,
            placeholder : 'Seleccione un municipio',
            ajax: {
                url: '{{ route('municipios.select') }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: (params) => {
                    return {
                        search: params.term
                    }
                },
                processResults: (data, params) => {
                    params.page = params.page || 1
                    return {
                        results: $.map(data, (item) => {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    }
                },
                cache: true
            }
        }).change((event) => {
            $('#municipio_id').valid()
            $('#localidad_id').empty()
            $('#localidad_id').prop('disabled',false)
            $('#localidad_id').select2({
                language: 'es',
                allowClear : true,
                placeholder : 'Seleccione una localidad',
                ajax: {
                    url: '{{ route('localidades.select') }}',
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: (params) => {
                        return {
                            search: params.term,
                            municipio_id: $('#municipio_id').val()
                        }
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1
                        return {
                            results: $.map(data, (item) => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        }
                    },
                    cache: true
                }
            }).change((event) => {
                $('#localidad_id').valid()
            })
        })

        $('#escuela_id').select2({
            language: 'es',
            minimumInputLength: 2,
            allowClear : true,
            placeholder : 'Seleccione una escuela',
            ajax: {
                url: '{{ route('alimentarios.escuelas') }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: (params) => {
                    return {
                        search: params.term
                    }
                },
                processResults: (data, params) => {
                    params.page = params.page || 1
                    return {
                        results: $.map(data, (item) => {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    }
                },
                cache: true
            }
        })

        $('#tipodistribucion_id').select2({
            language: 'es',
            allowClear : true,
            placeholder : 'Seleccione una distribución'
        })

        $('#financiamiento_id').select2({
            language: 'es',
            allowClear : true,
            placeholder : 'Seleccione una distribución',
        })
    </script>
@endpush