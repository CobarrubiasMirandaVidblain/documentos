
<div class="modal-header with-border">
<h5 class="modal-title">{{ $programacion->anioprograma->programa->nombre }}</h5>
    {{-- @if (isset($programacion)) --}}
        {{-- @if (strpos($programacion->estadoprograma->estado->nombre,"BLOQUEADA")!==false) --}}
            {{-- <button class="btn float-right btn-secondary" tabindex="0" onclick="Swal.fire('Atención','No puedes modificar los beneficiarios en esta cocina','info')"><span><i class="fa fa-plus"></i> Agregar beneficiario</span></button> --}}
        {{-- @else --}}
            <button class="btn float-right btn-secondary" tabindex="0" onclick="modal.moreModal('{{ route('alimentarios.programacion.beneficiarios.create',$programacion->id) }}','create');"><span><i class="fa fa-plus"></i> Agregar beneficiario</span></button>
        {{-- @endif --}}
    {{-- @endif --}}
</div>
{{-- modal-header --}}
<div class="modal-body">
    <div class="card">
        <div class="card-header">
            <i class="fa fa-align-justify"></i> Beneficiarios de alimentario con folio {{$programacion->alimentario->foliodif.', '.$programacion->alimentario->municipio->nombre}}{{isset($programacion->alimentario->localidad) ? ', '.$programacion->alimentario->localidad->nombre : ''}}
            <strong>
                {{ $programacion->anioprograma->programa->id == config('asistenciaalimentaria.desayunosId') ? ($programacion->alimentario->desayuno->escuela->nivel->nombre.' '.$programacion->alimentario->desayuno->escuela->nombre.' '.$programacion->alimentario->desayuno->escuela->clave) : '' }}
            </strong>
            {{-- <div class="card-header-actions">
                <a class="card-header-action" href="https://datatables.net" target="_blank">
                <small class="text-muted">docs</small>
                </a>
            </div> --}}
        </div>
        <div class="card-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                <input type="text" id="search-beneficiarios" name="search-beneficiarios" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-secondary" id="btn_buscar_beneficiarios" name="btn_buscar_beneficiarios">Buscar</button>
                </div>
            </div>
            {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'beneficiarios']) !!}
        </div>
    </div>
</div>
{{-- modal-body --}}
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal"><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    <label>Los campos con * son obligatorios</label>
</div>
{!! $dataTable->scripts() !!}

<script>
    (function() {
        var tablaB = ((tablaId) => {
            $('#search-beneficiarios').keypress(function(e) {
                if(e.which === 13) {
                    $(tablaId).DataTable().search($('#search-beneficiarios').val()).draw()
                }
            })
            $('#btn_buscar_beneficiarios').on('click', function() {
                $(tablaId).DataTable().search($('#search-beneficiarios').val()).draw()
            })
            function recargar(params) {
                console.log("holo")
                $(tablaId).DataTable().ajax.reload(null,false);
            }
            return {
                recargar : recargar
        }})('#beneficiarios')
    })()
</script>