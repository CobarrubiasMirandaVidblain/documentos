@extends('asistenciaalimentaria::layouts.master')

@section('title','CCNC-Crear programacion')

@section('content-subtitle', 'Seleccion de Cocinas')

@push('head')
	
  {{-- <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet"> --}}
	{{-- <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet"> --}}
  <style>
    .error{
      width: 100%;
    }
    .img-conv {
      max-width: 50%;
      margin-inline: 25%;
    }
  </style>
@endpush

@section('content')
	<div class="card">
		<div class="card-header">
			<i class="fa fa-align-justify"></i> Beneficiarios
			{{-- <div class="card-header-actions">
				<a class="card-header-action" href="https://datatables.net" target="_blank">
				<small class="text-muted">docs</small>
				</a>
			</div> --}}
		</div>
		<div class="card-body">
			<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
				<input type="text" id="search" name="search" class="form-control">
				<div class="input-group-append">
					<button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
				</div>
			</div>
			{!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'beneficiarios']) !!}
		</div>
	</div>
	{{-- <div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Planeacion:</h3>
		</div>
		<div class="box-body">
			<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
				<input type="text" id="search" name="search" class="form-control">
				<div class="input-group-append">
					<button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
        </div>
			</div>
			{!! $dataTable->table(['class' => 'table table-bordered table-striped dt-responsive nowrap', 'style'=>'width:100%', 'id' => 'beneficiarios']) !!}
		</div>
	</div> --}}
@stop

@push('body')
	<script type="text/javascript" src="{{ Module::asset('asistenciaalimentaria:js/all.js') }}"></script>
  {!! $dataTable->scripts() !!}
  <script type="text/javascript">
		var tabla = ((tablaId) => {
			$('#search').keypress(function(e) {
				if(e.which === 13) {
					$(tablaId).DataTable().search($('#search').val()).draw()
				}
			})
			$('#btn_buscar').on('click', function() {
				$(tablaId).DataTable().search($('#search').val()).draw()
			})
			function recargar(params) {
				$(tablaId).DataTable().ajax.reload(null,false);
			}
			return {
				recargar : recargar
		}})('#beneficiarios');



		function ajaxSuccess(response,tabla) {
			window.LaravelDataTables[tabla].ajax.reload();
			Swal.fire('¡Guardado!','Operación realizada con éxito','success')
		}
		function ajaxError(response) {
			tabla.recargar();
      //console.log(response.responseJSON.message);
      Swal.fire('¡Error!',response.responseJSON.message,'error')
		}
	</script>
@endpush