@php
    $form_name = isset($vista_beneficiario) ? 'form-beneficiarios' : (isset($cargo_programa_id) ? 'form-asignacion_comites' : 'form')
@endphp
<div class="modal-header">
  <h4 class="modal-title">
    {{ isset($persona) ? 'Editar' : 'Agregar' }} {{ isset($vista_beneficiario)?'beneficiario':'miembro del comité' }}
    @if (isset($persona))
      <i class="fas fa-user-edit" id="edit-icon"></i>
    @else
      <i class="fas fa-user-plus"></i>
    @endif
  </h4>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body with-border">
  <form data-toggle="validator" role="form" id="{{ $form_name }}" class="needs-validation">
    <ul class="nav nav-tabs" id="tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tab-persona" role="tab" aria-controls="home" aria-selected="true">Datos generales</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade show active" id="tab-persona" role="tabpanel" aria-labelledby="home-tab">
        @include('asistenciaalimentaria::beneficiarios.fragmentos.datos_persona')
        @isset($vista_beneficiario)
          @includeIf($vista_beneficiario)
        @endisset
      </div>
    </div>
  </form>
</div>
<div class="modal-footer">
  <span class="mr-auto">Los campos con * son obligatorios</span>
  <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  @isset($vista_beneficiario)
    <div id="btn-save-bf"><button class="btn float-right btn-primary" onclick="transaccion.formAjax('{{ isset($persona) ? route('alimentarios.programacion.beneficiarios.update',[$programacion->id,$beneficiario->id]) : route('alimentarios.programacion.beneficiarios.store',$programacion->id) }}','{{ isset($persona) ? 'PUT' : 'POST'}}','{{ $form_name }}',{},false)"><span><i class="fa fa-save"></i> Guardar</span></button></div>
  @endisset
  @if(isset($cargo_programa_id) || isset($comite)){{-- Esto es para guardar a alguien del comite --}}
    @if (!isset($persona))
      @php
          $ruta = $tipo == 'create' ? route("alimentarios.programacion.comites.store",$programacion->id) : route("alimentarios.programacion.comites.update",[$programacion->id,$comite_id]);
      @endphp
      <button class="btn pull-right button-dt tool" onclick="transaccion.formAjax('{{ $ruta }} ','{{ $tipo == 'create' ? 'POST' : 'PUT' }}','{{ $form_name }}',{cargo_programa_id : {{ $cargo_programa_id }}},false)"><span><i class="fa fa-save"></i> Guardar</span></button>
    @else
    @php
        $r = "alimentarios/programacion/$programacion->id/miembro/$persona->id"
    @endphp
      <button class="btn pull-right button-dt tool" onclick="transaccion.formAjax('{{ URL::to($r) }}','PUT','{{ $form_name }}',{},false)"><span><i class="fa fa-save"></i> Guardar</span></button>
    @endif
  @endif
</div>
{{-- </div> --}}
<script>
  (function() {
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    function genero_alim() {
      genero = $('#genero').val().toUpperCase()
      fecha_nacimiento = ($('#fecha_nacimiento').val()).split('/')
      nac = moment(fecha_nacimiento[2]+'-'+fecha_nacimiento[1]+'-'+fecha_nacimiento[0])
      hoy = moment(moment().format('YYYY')+'-12-31')
      anios = hoy.diff(nac,'years')
      meses = hoy.diff(nac,'months')
      console.log(anios+' '+meses+' '+genero)
      if(genero == 'F')
        if(anios > 8)
          $('#genero-alim').show()
        else
          $('#genero-alim').hide()
      else
        $('#genero-alim').hide()
    }
    function buscarPersona() {
      /*
        Este proceso se realizó por que el formulario se enviaba a ún cuando faltaban
        campos por rellenar en el tab con id "tab-beneficiario"
      */
      $('#fecha_nacimiento').focusout(function() {
        genero_alim()
      })
      $('#genero').on('change',function(){
        genero_alim()
      })
      $('#genero').focus(function(){
        genero_alim()
      })
      /*
        autocompletado para agilizar proceso de llenado
      */
      var cache=[];
      $('#curp').autocomplete({
        minLength : 5,
        source: function(request, response) {
          var term = request.term;
          if (term in cache) {
            response(cache[ term ]);
            return;
          }
          $.ajax({
            url: "{{ route('personas.index') }}",
            dataType: "json",
            global: false,
            data: {
              columns : {
                0 : {
                  name : 'personas.curp',
                  search : {
                    value : term.replace('_','')
                  }
                }
              },
              tipo : 'select'
            },
            success: function (data) {
              cache[ term ] = data.data;
              response(data.data);
            }
          });
        },
        select: function(event, ui) {
          //encontrarPersona(ui.item.edit);
          var persona = (ui.item.edit).split("/");
          encontrarPersona(persona[persona.length-1]);
        },
        appendTo: "#{{ $tipo }}"
      });
    }
    function encontrarPersona(persona) {
      $.get('/alimentarios/persona/'+persona,function (data) {
        /* if(data.bienestar === 1) {
          Swal.fire({
            title: '¡Esta persona pertenece al padrón de bienestar!',
            type: 'warning',
            timer: 2500,
            toast : true,
            position : 'center',
            showConfirmButton: false
          });
        } */
        Object.keys(data.persona).forEach(element => {
          /* if(data.bienestar === 1){
            $('#'+element).prop('readonly',true);
            $('#tab-persona select').prop('disabled',true)
            $('#num_acta_nacimiento').removeAttr('readonly');
            $('#esta_embarazada_lactando,#tipoaccion_id').prop('disabled',false)
          }
          else{
            $('#'+element).removeAttr('readonly');
            $('#tab-persona select').prop('disabled',false)
          } */
          var aux = data.persona[element]
          if (aux != null) {
            if(element.indexOf('fecha')>=0)
              aux = aux.split('-').reverse().join('/')
            if(element == 'tiporopa_id')
              $('#tiporopa_id option[value='+aux+']').attr('selected', 'selected').trigger('change');
            else
              $('#'+element).val(aux);
          }
        });
        $('#municipio_id').html('<option value="'+data.persona.municipio_id+'" selected>'+data.mun+'</option>').trigger('change');
        $('#localidad_id').html('<option value="'+data.persona.localidad_id+'" selected>'+data.loc+'</option>').trigger('change');
        $('#etnia_id').html('<option value="'+data.persona.etnia_id+'" selected>'+data.etn+'</option>').trigger('change');
        $('#genero').trigger('change');
        add = $('#edit-icon');
        if (Object.keys(add).length === 0) {
          $('#curp').prop('readonly',true)
          Swal.fire(
            '¡Importante!',
            'En este proceso no puedes modificar la curp',
            'warning'
          )
        }
      })
    }
    function initComponets() {
      $('.select2').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumResultsForSearch: Infinity
      });
      /* $('#fecha_nacimiento').datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: '01-01-1900',
        endDate: '0d',
        orientation: 'bottom',
        enableOnReadonly : false
      });
      $('#fecha_talla').datepicker({
          autoclose: true,
          language: 'es',
          format: 'dd/mm/yyyy',
          startDate: moment('2000-01-01').format('DD/MM/YYYY'),
          endDate: moment().format('DD/MM/YYYY')
      });
      $('#fecha_peso').datepicker({
          autoclose: true,
          language: 'es',
          format: 'dd/mm/yyyy',
          startDate: moment('2000-01-01').format('DD/MM/YYYY'),
          endDate: moment().format('DD/MM/YYYY')
      }); */
      $('#municipio_id').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('municipios.select') }}',
          delay: 500,
          dataType: 'JSON',
          global: false,
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#municipio_id').valid();
        $('#localidad_id').empty();
        $('#localidad_id').select2({
          language: 'es',
          placeholder : 'SELECCIONE ...',
          ajax: {
            url: '{{ route('localidades.select') }}',
            delay: 500,
            dataType: 'JSON',
            global: false,
            type: 'GET',
            data: function(params) {
              return {
                search: params.term,
                municipio_id: $('#municipio_id').val()
              };
            },
            processResults: function(data, params) {
              params.page = params.page || 1;
              return {
                results: $.map(data, function(item) {
                  return {
                    id: item.id,
                    text: '('+item.cve_localidad+') '+item.nombre,
                    slug: item.nombre,
                    results: item
                  }
                })
              };
            },
            cache: true
          }
        }).change(function(event) {
          $('#localidad_id').valid();
        });
      });
      $('.mask').inputmask();
      $('a[data-toggle="tab"]').on('click', function(){
        if ($(this).parent('li').hasClass('disabled')) {
          return false;
        }
      });
      $('#etnia_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        ajax: {
          url: '{{ route('vale.etnias.select') }}',
          delay: 500,
          dataType: 'JSON',
          global: false,
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#etnia_id').valid();
      });
      /* $('#genero').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
      }); */
      $('#tipoaccion_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        ajax: {
          url: '{{ route('alimentarios.programas.search',$programacion->id) }}',
          delay: 500,
          dataType: 'JSON',
          global: false,
          type: 'GET',
          data: function(params) {
            return {
              search: params.term,
              genero: $('#genero').val(),
              edad: function() {
                var fecha_nacimiento = ($('#fecha_nacimiento').val()).split('/')
                var nac = moment(fecha_nacimiento[2]+'-'+fecha_nacimiento[1]+'-'+fecha_nacimiento[0])
                var hoy = moment(moment().format('YYYY')+'-12-31')
                return hoy.diff(nac,'years')
              },
              meses: function() {
                var fecha_nacimiento = ($('#fecha_nacimiento').val()).split('/')
                var nac = moment(fecha_nacimiento[2]+'-'+fecha_nacimiento[1]+'-'+fecha_nacimiento[0])
                var hoy = moment(moment().format('YYYY')+'-12-31')
                return hoy.diff(nac,'months')
              }
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        /*
          En el caso de ciertos subprogramas se solicita peso y talla
        */
        $('#tipoaccion_id').valid();
        let programa = $('#tipoaccion_id').select2('data')[0].results.nombre
        if(programa === 'ATENCIÓN A MENORES DE 5 AÑOS EN RIESGO NO ESCOLARIZADOS (1 AÑO A 5 AÑOS 11 MESES)'
          || programa === 'DESAYUNOS ESCOLARES  (6 A 12 AÑOS)'
          || programa === 'NIÑOS Y NIÑAS DE 1 A 12 AÑOS') {
          $('#extra-datos').show()
        } else {
          @if ($programacion->alimentario->programa->id == config('asistenciaalimentaria.desayunosId'))
            $('#extra-datos').show()
          @else
            $('#extra-datos').hide()
          @endif
        }
      });
    }
    initComponets()
    buscarPersona();
    $('#{{ $form_name }}').validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          pattern_nombre: ''
        },
        primer_apellido: {
          required: true,
          minlength: 1,
          pattern_apellido: ''
        },
        segundo_apellido: {
          //required: true,
          minlength: 1,
          pattern_apellido: ''
        },
        fecha_nacimiento: {
          required: true,
          pattern_fecha: ''
        },
        curp: {
          required: true,
          minlength: 18,
          maxlength: 19,
          pattern_curp: ''
        },
        genero: {
          required: true
        },
        calle: {
          //required: true,
          minlength: 3
        },
        numero_exterior: {
          //required: true,
          minlength: 1,
          pattern_numero: ''
        },
        numero_interior: {
          //required: false,
          minlength: 1,
          pattern_numero: ''
        },
        colonia: {
          //required: true,
          minlength: 3
        },
        codigopostal: {
          //required: false,
          minlength: 5,
          maxlength: 5,
          pattern_integer: ''
        },
        municipio_id: {
          required: true
        },
        localidad_id: {
          required: false
        },
        referencia_domicilio: {
          //required: true,
          minlength: 3
        },
        num_acta_nacimiento: {
          required: true
        },
        numero_celular: {
          required: false,
          pattern_telefono: ''
        },
        numero_local: {
          required: false,
          pattern_telefono: ''
        },
        email: {
          required: false,
          email: true
        },
        etnia_id: {
          required: true,
        },
        talla: {
            required: {depends: function(element) {return validarPesoTalla()}},
            number: true,
            minlength: 2,
            maxlength: 5
        },
        peso: {
            required: {depends: function(element) {return validarPesoTalla()}},
            number: true,
            minlength: 2,
            maxlength: 6
        },
        fecha_talla: {
            required: {depends: function(element) {return validarPesoTalla()}},
            minlength: 10,
            maxlength: 10,
            pattern_fecha: ''
        },
        fecha_peso: {
            required: {depends: function(element) {return validarPesoTalla()}},
            minlength: 10,
            maxlength: 10,
            pattern_fecha: ''
        },
        tiporopa_id: {
            required: {depends: function(element) {return validarPesoTalla()}}
        }/* ,
        grado: {
            required: true
        } */
      },
      messages: {
      }
    });
    $.validator.addMethod('pattern_fecha', function (value, element) {
        return this.optional(element) || /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/.test(value);
    }, 'Revise los caracteres y vuelve a intentarlo')
    validacion.iniciar('{{ $form_name }}')

    $('#curp').focus(function(){
      if($('#curp').val().length==18) return false
      var fecha = $('#fecha_nacimiento').val().split('/')
      var vcurp = new curp($('#nombre').val(),$('#primer_apellido').val(),$('#segundo_apellido').val(),fecha[2],fecha[1],fecha[0],$('#genero').val(),'')
      $('#curp').val(vcurp.validar())
      var event = new InputEvent('input', {
        view: window
      });
      var cb = document.getElementById('curp');
      cb.dispatchEvent(event);
      $('#curp').setSelectionRange(16,16)
    })
    function validarPesoTalla() {
      if($('#peso').val() != '' || $('#talla').val() != '' || $('#fecha_peso').val() != '' || $('#fecha_talla').val() != '' || $('#tiporopa_id').val() != '')
        return true
      return false
    }
  })()
  $(document).ready(function() {
    $('#nombre').focus();
  })
</script>
