<h5>Elección</h5>

<hr>
@if ($programacion->alimentario->programa->id != config('asistenciaalimentaria.desayunosId'))
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" id="genero-alim">
        <div class="form-group">
            <label for="esta_embarazada_lactando">*¿Embarazada o lactando?:</label>
            <select type="text" class="form-control select2" id="esta_embarazada_lactando" name="esta_embarazada_lactando" required>          
                <option selected value="">Seleccione una opción...</option>
                <option value="0" {{ isset($beneficiario->esta_embarazada_lactando) ? ($beneficiario->esta_embarazada_lactando == 0 ? 'selected' : '') : 'selected' }}>NO</option>
                <option value="1"  {{ isset($beneficiario->esta_embarazada_lactando) ? ($beneficiario->esta_embarazada_lactando == 1 ? 'selected' : '') : '' }}>SI</option>
            </select>
        </div>
    </div>
  </div>
@endif
<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
    <div class="form-group">
      <label for="tipoaccion_id" class="center-block">*Pertenece a:</label>
      <select id="tipoaccion_id" name="tipoaccion_id" class="form-control select2" style="width: 100%;" required>
        @if(isset($beneficiario->tipoaccion_id))
          <option value="{{ $beneficiario->tipoaccion_id }}" selected>{{ $beneficiario->tipoaccion->nombre }}</option>
        @elseif($programacion->alimentario->programa->id == config('asistenciaalimentaria.desayunosId'))
          <option value="7">DESAYUNOS ESCOLARES FRIOS (DESAYUNOS ESCOLARES FRIOS)</option>
        @endif
      </select>
    </div>
  </div>
</div>
{{--Para "extra-datos", sólo se muestra para ciertos sub-programas--}}
@php
    $hide = false;
    if(($beneficiario && ($beneficiario->tipoaccion->nombre == 'DESAYUNOS ESCOLARES  (6 A 12 AÑOS)' 
      || $beneficiario->tipoaccion->nombre == 'ATENCIÓN A MENORES DE 5 AÑOS EN RIESGO NO ESCOLARIZADOS (1 AÑO A 5 AÑOS 11 MESES)'
      || $beneficiario->tipoaccion->nombre == 'NIÑOS Y NIÑAS DE 1 A 12 AÑOS'))
      || $programacion->alimentario->programa->id == config('asistenciaalimentaria.desayunosId')
      || $programacion->alimentario->programa->id == config('asistenciaalimentaria.pesoTallaId'))
      {
        $hide = true;
      }
@endphp
<div id="extra-datos" style="{{ $hide == false ? 'display: none;' : '' }}">
    @include('asistenciaalimentaria::beneficiarios.fragmentos.peso_talla')
</div>
{{-- @if(isset($beneficiario->programacion))
  @if($beneficiario->programacion->alimentario->programa->id == config('asistenciaalimentaria.desayunosId'))
    @include('asistenciaalimentaria::beneficiarios.fragmentos.peso_talla')  
  @endif
@endif --}}