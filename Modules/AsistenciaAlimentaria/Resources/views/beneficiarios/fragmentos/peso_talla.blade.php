@php
    $extra = (object) array('1' => 'foo');
    if($persona)
        $extra = $persona->extradatos->last();
@endphp
<h5>Peso y talla</h5>

<hr>

@if ($programacion->alimentario->programa->id == config('asistenciaalimentaria.desayunosId') || $programacion->alimentario->programa->id == config('asistenciaalimentaria.pesoTallaId'))
    <div class="row" style="padding-left: 0; padding-right: 0;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="grado">*Grado:</label>
                <select type="text" class="form-control select2" id="grado" name="grado" title="Se refiere el grado escolar
    al que pertenece el menor que 
    deberá escribirse en número
    arábigo.">          
                    <option selected value="">Seleccione un grado</option>
                    @for ($i = 1; $i <= 6/*(isset($edad) ? ($edad >= 8 ? 6 : 3 ) : 6)*/; $i++)
                        <option value="{{$i}}" {{isset($extra->grado) ? ($extra->grado == $i ? 'selected' : '') : ''}}>{{$i}} °grado</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
@endif

<div class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="talla">*Talla (en centímetros):</label>
            <input type="text" class="form-control" id="talla" name="talla" maxlength="5" value="{{ $extra->talla or '' }}" title="Anotar la talla
en centímetros y milímetros,
por ejemplo: 128.5
usando cuatro números,
separados por punto.">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="fecha_talla">*Fecha talla (última vez que se tomó la talla):</label>
            <input type="text" class="form-control datepicker mask" id="fecha_talla" name="fecha_talla" data-inputmask='"mask": "99/99/9999"' value="{{ isset($extra->fecha_talla) ? $extra->get_formato_fecha_talla() : '' }}" title="Anotar la fecha en
que se tomó la última talla; debe
escribirse en el formato: dia/mes/año,
utilizando números: 21/10/2018.">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="peso">*Peso (en kilogramos):</label>
            <input type="text" class="form-control" id="peso" name="peso" maxlength="6" value="{{ $extra->peso or '' }}" title="Anotar el peso
en kilogramos y cientos de gramos,
por ejemplo: 41.200.">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="fecha_peso">*Fecha peso (última vez que se tomó el peso):</label>
            <input type="text" class="form-control datepicker mask" id="fecha_peso" name="fecha_peso" data-inputmask='"mask": "99/99/9999"' value="{{ isset($extra->fecha_peso) ? $extra->get_formato_fecha_peso() : '' }}" title="Anotar la fecha en
que se tomó el último
peso; debe escribirse
en el formato: dia/mes/año,
utilizando números: 21/10/2018.">
        </div>
    </div>
</div>

<div class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="tiporopa_id">*Ropa:</label>
            <select type="text" class="form-control select2" id="tiporopa_id" name="tiporopa_id" title="Describir el tipo de ropa con la
que fue pesado (a) el/la niño (a);
1=Ligera, que se refiere a los niños pesados
únicamente con el uniforme; y 2=Gruesa,
que incluye cualquier otro tipo
de prenda extra (Suéter,chamarra,bufanda).">
                <option selected value="">Seleccione un grado</option>
                <option value="1" {{isset($extra->tiporopa_id) ? ($extra->tiporopa_id == 1 ? 'selected' : '') : ''}}>Gruesa</option>
                <option value="2" {{isset($extra->tiporopa_id) ? ($extra->tiporopa_id == 2 ? 'selected' : '') : ''}}>Ligera</option>
            </select>
        </div>
    </div>
</div>