<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <label for="proveedor_id">*Nombre proveedor:</label>
            <div class="input-group mb-3">
                <select id="proveedor_id" name="proveedor_id" class="form-control select2" style="width:90%;" aria-describedby="basic-addon2" required></select>
                <button type="button" class="btn-secondary" data-toggle="tooltip" style="width:10%;"  onclick="modal.moreModal('/proveedor/create','proveedor');">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="num_licitacion">*Número licitación:</label>
            <input type="text" name="num_licitacion" id="num_licitacion" class="form-control" maxlength="3" placeholder="Ingresar número de licitación" required>
        </div>
    </div>
</div>