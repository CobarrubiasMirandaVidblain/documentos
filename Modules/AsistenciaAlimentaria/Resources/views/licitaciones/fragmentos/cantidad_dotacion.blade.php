<div class="modal-header">
    <h4 class="modal-title">Dotaciones para la licitacion {{ $licitacion->num_licitacion }}</h4> <i class="fa fa-shopping-basket"></i>
    <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
  {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'cantidades-dotaciones']) !!}
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-dismiss="modal" aria-hidden="true" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
</div>
{!! $dataTable->scripts() !!}