<div class="modal-header">
    <h4 class="modal-title">Registrar licitación</h4> <i class="fa fa-shopping-basket"></i>
    <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form data-toggle="validator" role="form" id="form-licitacions" class="needs-validation">
        @include('asistenciaalimentaria::licitaciones.fragmentos.general')
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-dismiss="modal" aria-hidden="true" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    <button class="btn btn-primary" onclick="transaccion.formAjax('{{ route('alimentarios.licitaciones.store') }}','POST','form-licitacions')">Guardar</button>
</div>
<script type="text/javascript">
    (function(){
        validacion.iniciar('form-licitacions')
        $('.select2').select2({
            allowClear : true,
            language: 'es',
            placeholder : 'SELECCIONE ...',
            minimumResultsForSearch: Infinity
        });
        $('#proveedor_id').select2({
            language: 'es',
            placeholder : 'SELECCIONE ...',
            ajax: {
                url: '{{ route('proveedores.select') }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre + ' (' + item.rfc + ')',
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            $('#proveedor_id').valid();
        });
    })()
</script>