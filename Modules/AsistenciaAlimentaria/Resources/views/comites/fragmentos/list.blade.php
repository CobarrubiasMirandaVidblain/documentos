<div class="modal-header">
  <h5 class="modal-title">{{$programacion->anioprograma->programa->nombre}}</h5>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="card">
      <div class="card-header">
        <i class="fa fa-align-justify"></i> Comité de alimentario con folio {{$programacion->alimentario->foliodif.', '.$programacion->alimentario->municipio->nombre}}{{isset($programacion->alimentario->localidad) ? ', '.$programacion->alimentario->localidad->nombre : ''}} 
        <strong>
            {{ $programacion->anioprograma->programa->id == config('asistenciaalimentaria.desayunosId') ? ($programacion->alimentario->desayuno->escuela->nivel->nombre.' '.$programacion->alimentario->desayuno->escuela->nombre.' '.$programacion->alimentario->desayuno->escuela->clave) : '' }}
        </strong>
        {{-- <div class="card-header-actions">
          <a class="card-header-action" href="https://datatables.net" target="_blank">
          <small class="text-muted">docs</small>
          </a>
        </div> --}}
      </div>
      <div class="card-body">
        {{-- <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
          <input type="text" id="search" name="search" class="form-control">
          <div class="input-group-append">
            <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
          </div>
        </div> --}}
        {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'asignacion_comites']) !!}
      </div>
    </div>
</div>
  {!! $dataTable->scripts() !!}