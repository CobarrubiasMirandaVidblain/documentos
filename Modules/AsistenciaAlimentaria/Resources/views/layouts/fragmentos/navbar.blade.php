@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree" id="sidebar">
  <li class="treeview">
    <a href="#">
      <i class="fa fa-handshake-o"></i> <span>Peticiones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('asistenciaalimentaria.peticiones.index') }}"><i class="fa fa-list-alt"></i> Lista de peticiones</a></li>
      <li><a href="{{ route('asistenciaalimentaria.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li>
    </ul>
  </li>
  <li>
    <a href="{{ route('alimentarios.configuracion.index') }}"><i class="fa fa-gear"></i><span>Configuración</span></a>
  </li>
  <li class="header">Cocinas comedor <br> nutricional comunitario</li>
    <li><a href="{{ route('alimentarios.ccnc.programacion.index') }}"><i class="fa fa-star"></i><span>Programación</span></a></li>
    <li class="treeview">
      <a href="{{ route('alimentarios.ccnc.index') }}">
          <i class="fa fa-cutlery"></i> <span>Cocinas</span>
          <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
          {{-- <li><a href=""><i class="fa fa-plus-circle"> Registrar cocina</i></a></li> --}}
          <li><a href="{{ route('alimentarios.ccnc.index') }}"><i class="fa fa-list-alt"> Lista de cocinas</i></a></li>
          <li><a href="#"><i class="fa fa-gavel"> Convenios</i></a></li>
          <li><a href="{{ route('alimentarios.programa.beneficiarios',config('asistenciaalimentaria.ccncId')) }}"><i class="fa fa-star"> Beneficiarios</i></a></li>
          <li><a href="#"><i class="fa fa-address-card-o"> Comités</i></a></li>
          <li><a href="#"><i class="fa fa-tasks"> Cedulas</i></a></li>
        </ul>
      </li>
    <li><a href=""><i class="fa fa-bank"></i> <span> Requisiciones </span> </a></li>
    <li><a href="#"><i class="fa fa-external-link-square"></i> <span> Apoyos adicionales </span> </a></li>
    <li><a href="#"><i class="fa fa-file-text-o"></i> <span> Oficios de distribución </span> </a></li>
    <li><a href="#"><i class="fa fa-truck"></i> <span> Proveedor </span> </a></li>
    <li><a href="#"><i class="fa fa-check"></i> <span> Validación </span> </a></li>
  </li>
  <li class="header">Desayunos escolares</li>
    <li class="treeview">
      <a href="#"> <i class="fa fa-building-o "></i> <span>Escuelas</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i></span>
      </a>
      <ul class="treeview-menu">
        <li><a href=""><i class="fa fa-plus-circle"></i> Registrar escuela </a></li>
        <li><a href="#"><i class="fa fa-list-alt"></i> Lista de escuelas </a></li>
        <li><a href="#"><i class="fa fa-gavel"></i> Convenios </a></li>
        <li><a href="#"><i class="fa fa-address-card-o"></i> Comités </a></li>
        <li><a href="#"><i class="fa fa-start"></i> por si acaso </a></li>
      </ul>
    </li>
    <li><a href="#"><i class="fa fa-bank"></i> <span> Requisiciones</span></a></li>
    <li><a href="#"><i class="fa fa-file-text-o"></i> <span> Oficios de distribución</span></a></li>
    <li><a href="#"><i class="fa fa-truck"></i> <span> Proveedor</span></a></li>
  </li>
  <li class="header">Sujetos vulnerables</li>
    <li class="treeview">
      <a href="#"> <i class="fa fa-building-o "></i> <span>Sujetos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i></span>
      </a>
      <ul class="treeview-menu">
        <li><a href=""><i class="fa fa-plus-circle"> Registrar Sujeto</i></a></li>
        <li><a href="#"><i class="fa fa-list-alt"> Lista de sujetos</i></a></li>
        <li><a href="#"><i class="fa fa-start"> Beneficiarios</i></a></li>
        <li><a href="#"><i class="fa fa-gavel"> Convenios</i></a></li>
        <li><a href="#"><i class="fa fa-address-card-o"> Comités</i></a></li>
      </ul>
    </li>
    <li><a href="#"><i class="fa fa-bank"></i><span> Requisiciones</span></a></li>
    <li><a href="#"><i class="fa fa-file-text-o"></i><span> Oficios de distribución</span></a></li>
    <li><a href="#"><i class="fa fa-truck"></i><span> Proveedor</span></a></li>
    <li><a href="#"><i class="fa fa-check"></i><span> Validación</span></a></li>
  </li>
  {{-- <li class="header">Familias en desamparo</li>
    <li><a href="#"><i class="fa"><span>No</span></i></a></li>
    <li><a href="#"><i class="fa"><span>Se</span></i></a></li>
    <li><a href="#"><i class="fa"><span>Que</span></i></a></li>
    <li><a href="#"><i class="fa"><span>Poner</span></i></a></li>
  </li> --}}
  <li class="header">General</li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-dollar"></i> <span> Licitaciones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
    <li><a href=""><i class="fa">algo</i></a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-bullhorn"></i> <span> Capacitaciones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href=""><i class="fa">index</i></a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-heartbeat"></i> <span> Nutrición</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="#"><i class="fa">algo</i></a></li>
    </ul>
  </li>
  <li class="header">pruebas</li>
</ul>
@endsection