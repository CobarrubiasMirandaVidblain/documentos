@php
    $i = 0;
@endphp
@foreach ($acciones as $accion)
  <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
    <div>
      <strong>{{$accion["beneficiarios"]}}</strong>
    </div>
    <small>
      {{ explode('(',explode(')',$accion["accion"])[0])[1] }}</small>
  </div>
  @php
      $i+=$accion["beneficiarios"];
  @endphp
@endforeach
<script>
  $("#total-benef").text("{{$i}}");
</script>