<!DOCTYPE html>

<html lang="es">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    {{-- <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template"> --}}
    {{-- <meta name="author" content="Łukasz Holeczek"> --}}
    {{-- <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Main styles for this application-->
    <link href="{{ asset('modules/asistenciaalimentaria/css/app.css') }}" rel="stylesheet">
    <style>
        .sidebar-minimized .sidebar .nav-item:hover > .Cocinas, .sidebar-minimized .sidebar .nav-item:hover > .Cocinas .nav-icon{
            background-color: #DFB707 !important;
            color: white !important;
        }
        .sidebar-minimized .sidebar .nav-item:hover > .Desayunos, .sidebar-minimized .sidebar .nav-item:hover > .Desayunos .nav-icon{
            background-color: #20AFD6 !important;
            color: white !important;
        }
        .sidebar-minimized .sidebar .nav-item:hover > .Sujetos, .sidebar-minimized .sidebar .nav-item:hover > .Sujetos .nav-icon{
            background-color: #57AA65 !important;
            color: white !important;
        }
        .Cocinas, .Cocinas > .nav-icon{
            color: #DFB707 !important;
        }
        .Desayunos, .Desayunos > .nav-icon {
            color: #20AFD6 !important;
        }
        .Sujetos, .Sujetos > .nav-icon {
            color: #57AA65 !important;
        }
        .Cocinas:hover, .Cocinas:hover > .nav-icon {
            color: white !important;
            background-color: #DFB707 !important;
        }
        .Desayunos:hover, .Desayunos:hover > .nav-icon {
            color: white !important;
            background-color: #20AFD6 !important;
        }
        .Sujetos:hover, .Sujetos:hover > .nav-icon {
            color: white !important;
            background-color: #57AA65 !important;
        }
    </style>
    @stack('head')
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show sidebar-minimized">
    
    @include('asistenciaalimentaria::layouts.includes.navbar')

    <div class="app-body">
      
      @include('asistenciaalimentaria::layouts.includes.sidebar')

      <main class="main">
        
        @include('asistenciaalimentaria::layouts.includes.breadcrumb')

        <div class="container-fluid">
          
          @yield('content')
          
        </div>

      </main>
      
      
      @include('asistenciaalimentaria::layouts.includes.aside')  

    </div>
    <footer class="app-footer">
      <div>
        <span>DIF Oaxaca &copy; Copyright 2019.</span>
      </div>
      <div class="ml-auto">
        <span color="#444">Unidad de Informática</span>
      </div>
    </footer>
    <script src="{{ asset('modules/asistenciaalimentaria/js/app.js') }}"></script>
    <script src="{{ asset('modules/asistenciaalimentaria/js/all.js') }}"></script>
    <script>
        $('.aside-menu').one('transitionend',function() {
            $.each(LaravelDataTables,(i,x)=>{
                x.columns.adjust()
            })
        })
        if(/^\s*$/.test($('.aside-menu').html())){
            $('.navbar-toggler.aside-menu-toggler').hide()
        }
    </script>
    @stack('body')
  </body>

</html>
