<div class="modal-header">
  <h4 class="modal-title">
    Cambiando estado de la programación con folio: {{$programacion->alimentario->foliodif.', '.$programacion->alimentario->municipio->nombre}}{{isset($programacion->alimentario->localidad) ? ', '.$programacion->alimentario->localidad->nombre : ''}} 
  </h4>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body with-border">
  <form id="form-estados">
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-12">
        <div class="form-group">
          <label for="estado_programa_id">*Seleccione estado:</label>
          <select name="estado_programa_id" id="programa" class="form-control select2" style="width: 100%;" required></select>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-12" id="div-motivo">
        <div class="form-group">
          <label for="motivo">*Escriba el motivo:</label>
          <textarea name="motivo" id="motivo" class="form-control" rows="10" required></textarea>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="modal-footer">
  <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  @php
    $r = route('alimentarios.programacion.status',$programacion->id);
  @endphp
  <button class="btn pull-right button-dt tool" onclick="transaccion.formAjax('{{ URL::to($r) }}','PUT','form-estados',{},false)"><span><i class="fa fa-save"></i> Guardar</span></button>
</div>
<script>
  (function(){
    validacion.iniciar('form-estados')
    $('.select2').select2({
      allowClear : true,
      language: 'es',
      placeholder : 'SELECCIONE ...',
      minimumResultsForSearch: Infinity
    })
    $('#programa').select2({
      language: 'es',
      placeholder : 'SELECCIONE ...',
      ajax: {
        url: '{{ route('alimentarios.estados') }}',
        delay: 500,
        dataType: 'JSON',
        global: false,
        type: 'GET',
        data: function(params) {
          return {
            search: params.term,
            programa_id: "{{$programacion->anioprograma->programa->id}}"
          };
        },
        processResults: function(data, params) {
          params.page = params.page || 1;
          return {
            results: $.map(data, function(item) {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    }).change(function(event) {
      $('#programa').valid();
      programa = $('#programa').select2('data')[0].results.nombre.toUpperCase()
      if(programa.search('BAJA') == -1 && programa.search('CANCELADA') == -1) {
        $('#div-motivo').hide()
        $('#motivo').prop('disabled',true)
      } else {
        $('#div-motivo').show()
        $('#motivo').prop('disabled',false)
      }
    })
  })()
</script>