@extends('asistenciaalimentaria::layouts.master')
@push('head')
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/jquery-ui/themes/cupertino/jquery-ui.css') }}">
<style type="text/css">
  .center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
    margin-top: auto;
  }
  .map-control{
    background: transparent;
    border: 0px;
  }
  .checkbox{
    margin-top: 5px;
    margin-bottom: 5px;
  }
  .btn-cañada{
    background: #F0000080;
    color: white;
  }
  .btn-costa{
    background: #FF35E280;
    color: white;
  }
  .btn-istmo{
    background: #2900FF80;
    color: white;
  }
  .btn-mixteca{
    background: #0096A380;
    color: white;
  }
  .btn-papaloa{
    background: #0A9D1880;
    color: white;
  }
  .btn-sierraN{
    background: #ADA90480;
    color: white;
  }
  .btn-sierraS{
    background: #02470580;
    color: white;
  }
  .btn-vallesC{
    background: #5A118F80;
    color: white;
  }

  .btn-group button {
    background-color:white !important;
    margin-right: 5px;
    border-radius: 2px !important;

  }

  .map-control li {
      background-color: white;
      padding: 5px;
  }

  .map-control label {
      padding: 5px;
      padding-top: 1px;
  }
</style>
@endpush
@section('content')
<div class="box box-primary">
  <div class="box-header with-border">
    {{-- <h3 class="box-title">{{ $title }}</h3> --}}
  </div>
  <div class="box-body shadow" style="WIDTH:100%">
    <div id="map" class="center-block" style="height: 830px;"></div>
  </div>
  <div style="display:none">
    <div id="controles" class="btn-group" role="group" style="margin: 10px 290px 0 0 ;">
      <div id="search" class="btn-group" role="group">
        <button id="busqueda" class="btn btn-default dropdown-toggle" style="height:40px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Busqueda">
          <i class="fa fa-search"></i><span class="caret"></span>
        </button>
        <ul class="dropdown-menu map-control">
          <li>
            <input type="text" id ="municipios" class="form-control" placeholder="Buscar ...">
            {{-- <select name="municipios" id="municipios" style="width:100%">
              <option></option>
            </select> --}}
          </li>
        </ul>
      </div>
      <div class="btn-group" role="group">
        <button class="btn btn-default dropdown-toggle" style="height:40px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Regiones">
          <i class="fa fa-filter"></i><span class="caret"></span>
        </button>
        <ul class="dropdown-menu map-control">
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="cañada"  data-offstyle="default" data-width="95" data-on="Cañada" data-off="Cañada"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="costa"   data-offstyle="default" data-width="95" data-on="Costa" data-off="Costa"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="istmo"   data-offstyle="default" data-width="95" data-on="Istmo" data-off="Istmo"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="mixteca" data-offstyle="default" data-width="95" data-on="Mixteca" data-off="Mixteca"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="papaloa" data-offstyle="default" data-width="95" data-on="Papaloapan" data-off="Papaloapan"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="sierraN" data-offstyle="default" data-width="95" data-on="Sierra Norte" data-off="Sierra Norte"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="sierraS" data-offstyle="default" data-width="95" data-on="Sierra Sur" data-off="Sierra Sur"></div></li>
          <li><div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-size="mini" data-onstyle="vallesC" data-offstyle="default" data-width="95" data-on="Valles Centrales" data-off="Valles Centrales"></div></li>
        </ul>
      </div>
      <div class="btn-group" role="group">
        <button class="btn btn-default dropdown-toggle" style="height:40px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Ver">
          <i class="fa fa-eye"></i><span class="caret"></span>
        </button>
        <ul class="dropdown-menu map-control">
          <li>
            <div class="checkbox"><input type="checkbox" data-toggle="toggle" data-on="Cocinas" data-off="Cocinas" data-size="mini" data-onstyle="warning" data-offstyle="default" data-width="85"></div>
          </li>
          <li>
            <div class="checkbox"><input type="checkbox" data-toggle="toggle" data-on="Escuelas" data-off="Escuelas" data-size="mini" data-onstyle="success" data-offstyle="default" data-width="85"> </div>
          </li>
          <li>
            <div class="checkbox"><input type="checkbox" data-toggle="toggle" data-on="Asociaciones" data-off="Asociaciones" data-size="mini" data-onstyle="primary" data-offstyle="default" data-width="125"> </div>
          </li>
          {{-- <li>
            <div class="checkbox"><input type="checkbox" data-toggle="toggle" data-on="Circulos" data-off="Circulos" data-size="mini" data-onstyle="primary" data-offstyle="default" data-width="85"> </div>
          </li> --}}
          {{-- <li>
            <div class="checkbox"><input type="checkbox" data-toggle="toggle" data-on="Todo" data-off="Todo" data-size="mini" data-onstyle="danger" data-offstyle="default" data-width="85">
          </li> --}}
        </ul>
      </div>
      {{-- <button class="btn btn-default" title=""></button> --}}
    </div>
    {{-- <div id="search" class="search">
      <select name="municipios" id="municipios" style="width:100%">
        <option></option>
      </select>
    </div>
    <div id="control-alimentarios" class="panel panel-default" style="margin-left:7px; width:120px;">
      <div class="panel-body">
          <div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-on="Cocinas" data-off="Cocinas" data-size="mini" data-onstyle="warning" data-offstyle="default" data-width="85"> </div>
          <div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-on="Escuelas" data-off="Escuelas" data-size="mini" data-onstyle="success" data-offstyle="default" data-width="85"> </div>
          <div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-on="Casas" data-off="Casas" data-size="mini" data-onstyle="primary" data-offstyle="default" data-width="85"> </div>
          {{-- <div class="checkbox"><input type="checkbox" checked data-toggle="toggle" data-on="Todo" data-off="Todo" data-size="mini" data-onstyle="danger" data-offstyle="default" data-width="85"> </div>
      </div>
    </div> --}}
  </div>
</div>
@stop
@push('body')
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/jquery-ui/jquery-ui.js') }}"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDpYJe13cGrxH5Hk-9u5fu4PFQI-YMD5M&libraries=visualization"></script>
{{-- <script type="text/javascript" src="{{ asset('js/puntosPruebaCocinas.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('js/oax_map.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/coordMunicipios.js') }}"></script>
<script type="text/javascript">

  var munXregion = {!! $regiones !!};
  var puntos = @json(json_decode($puntos));
  var Regiones = {};
  var map, oaxaca, infoWindow;
  var cocinas, casas, escuelas;
  var cmarks = [], emarks = [], smarks = [], circulos = [];
//   var muniCir = [2, 21, 39, 42, 44, 184, 278, 177, 545, 73, 14, 295, 298, 334, 413, 526, 474, 482, 318, 565];
  var colores = {
      "CAÑADA"           : '#F00000',
      "COSTA"            : '#FF35E2',
      "ISTMO"            : '#2900FF',
      "MIXTECA"          : '#0096A3',
      "PAPALOAPAN"       : '#0A9D18',
      "SIERRA NORTE"     : '#ADA904',
      "SIERRA SUR"       : '#024705',
      "VALLES CENTRALES" : '#5A118F'
    }
  $(document).ready(function () {
    initMap();
    initControls();
  })

  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom   : 8.33,
      center : {lat : 17.15727971021128, lng: -96.23866181640627 },
      minZoom: 8.33,
      styles : [
        {
          "featureType": "administrative.country",
          "elementType": "geometry.fill",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "geometry.stroke",
          "stylers": [
          {
            "visibility": "on"
          }
          ]
        },
        {
          "featureType": "administrative.country",
          "elementType": "labels.text",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "administrative.province",
          "elementType": "geometry.fill",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "labels",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "administrative.neighborhood",
          "elementType": "labels.text",
          "stylers": [
          {
            "visibility": "on"
          }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "landscape",
          "elementType": "geometry.stroke",
          "stylers": [
          {
            "visibility": "off"
          },
          {
            "saturation": "1"
          }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "landscape.man_made",
          "elementType": "labels",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "geometry.fill",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "landscape.natural",
          "elementType": "labels",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "road",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "off"
          }
          ]
        },
        {
          "featureType": "water",
          "elementType": "all",
          "stylers": [
          {
            "visibility": "on"
          }
          ]
        }
      ],
      disableDefaultUI: true,
      mapTypeControl: false,
      zoomControlOptions: {
       position: google.maps.ControlPosition.RIGHT_CENTER
      },
      fullscreenControlOptions:{
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: true,
      zoomControl: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    limitarMapa();
    mapaDeCalor();
    pintarMunicipios();

    infoWindow = new google.maps.InfoWindow;

  }
  function pintarMunicipios() {
    let nombreRegion = Object.keys(munXregion);
    nombreRegion.forEach(region => {
      let muns = [];
      for (let i = 0; i < munXregion[region].length; i++) {
        //generando un poligono con el contorno del municipio}
        muns[i] = new google.maps.Polygon({
          paths: coordMunicipios[munXregion[region][i][0]],
          strokeColor: '#999',//colores[region],
          strokeOpacity: .5,
          strokeWeight: .5,
          fillColor: '#fff',//colores[region],
          fillOpacity: .33,
          map : map
        });
      }
      //relacionando poligonos a regiones
      Regiones[region] = muns;
    });
  }
  function detalles(conta) {
    return 'prueba ' + conta;
  }
  function limitarMapa() {
    //evitar que el mapa se mueva mas alla de los limites de oaxaca
    var activeCenterLat;
    var activeCenterLng;
    //configurando lis limites solo 1 vez cuadndo se termina de dibujar
    google.maps.event.addListenerOnce(map,'idle',function() {
      limit = borders();
    });
    //configurar el zoom minimo cada vez que se entra o sale de pantalla comlpeta
    document.addEventListener("fullscreenchange", recentrar);       //Estandar (jajajajajajaja)
    document.addEventListener("webkitfullscreenchange", recentrar); //Webkit (Safari, Chrome y Opera 15+)
    document.addEventListener("mozfullscreenchange", recentrar);    //Firefox
    document.addEventListener("MSFullscreenChange", recentrar);     //Internet Explorer 11+
    //al alejar el mapa y ver todo oaxaca, centrar solo oaxaca
    google.maps.event.addListener(map,'zoom_changed',function () {
      toggleMarkersHeatMap();
      if(map.getZoom() == 9){
        map.setCenter({lat : 17.15727971021128, lng: -96.22866181640627 });
        map.setZoom(8.5)
      }
    });
    //revisar que no se salga
    google.maps.event.addListener(map,'drag',function() {
      if(map.getZoom() < 9)
        map.setCenter(limit.center);
      else{
        current = borders();
        if( current.maxLng <= limit.maxLng && current.minLng >= limit.minLng )
          activeCenterLng = current.center.lng();
        else
          map.setCenter(
            new google.maps.LatLng(
              activeCenterLat,
              activeCenterLng
            )
          );
        if( current.maxLat <= limit.maxLat && current.minLat >= limit.minLat )
          activeCenterLat = current.center.lat();
        else
          map.setCenter(
            new google.maps.LatLng(
              activeCenterLat,
              activeCenterLng
            )
          );
      }
    });
    //centrando la atencion en oaxaca
    let coordmapini=[
      {lat:15.258438722900348, lng:-99.75428681640625},
      {lat:15.258438722900348, lng:-92.72303681640625},
      {lat:19.03689065576206,  lng:-92.72303681640625},
      {lat:19.03689065576206,  lng:-99.75428681640625}
    ];
    //generando un poligono cuandrado con el contorno de oaxaca en el centro
    oaxaca = new google.maps.Polygon({
      paths: [coordmapini,oaxCoord],
      strokeColor: '#FFF',
      strokeOpacity: .5,
      strokeWeight: 3,
      fillColor: '#000000BF',
      fillOpacity: 1
    });
    //y asignado al mapa
    oaxaca.setMap(map);
  }
  function borders(){
    return {
      maxLat : map.getBounds().getNorthEast().lat(),
      maxLng : map.getBounds().getNorthEast().lng(),
      minLat : map.getBounds().getSouthWest().lat(),
      minLng : map.getBounds().getSouthWest().lng(),
      center : map.getCenter()
    }
  }
  function mapaDeCalor() {
    var tamañoBolita = 55;
    for (const programa in puntos) {
        if (puntos.hasOwnProperty(programa)) {
            element = puntos[programa];
            puntos[programa] = element.map((item) => new google.maps.LatLng(item.latitud,item.longitud))
        }
    }

    var tamañoMarkers = 32;
    var ie = new google.maps.MarkerImage(
      "{{ asset('images/AsisAlimentaria/escuelas-icon.png') }}",
      new google.maps.Size(tamañoMarkers,tamañoMarkers),
      new google.maps.Point(0,0),
      new google.maps.Point((tamañoMarkers/2),(tamañoMarkers/2)),
      new google.maps.Size(tamañoMarkers,tamañoMarkers)
    );
    var is = {
      url : "{{ asset('images/AsisAlimentaria/apoyo-icon.png') }}",
      size: new google.maps.Size(tamañoMarkers,tamañoMarkers),
      origin : new google.maps.Point(0,0),
      anchor : new google.maps.Point((tamañoMarkers/2),(tamañoMarkers/2)),
      scaledSize: new google.maps.Size(tamañoMarkers, tamañoMarkers)
    };
    var ic = {
      url : "{{ asset('images/AsisAlimentaria/cocinas-icon.png') }}",
      size: new google.maps.Size(tamañoMarkers,tamañoMarkers),
      origin : new google.maps.Point(0,0),
      anchor : new google.maps.Point((tamañoMarkers/2),(tamañoMarkers/2)),
      scaledSize: new google.maps.Size(tamañoMarkers, tamañoMarkers)
    };

    // var  part1 = [], part2 = [], part3 = [], part4 = [];

    /* puntos.map(function(location, i) {
      if(i % 3 == 0)
      part1.push(new google.maps.LatLng(location.Latitude,location.Longitude));
      else if(i % 2 == 0)
      part2.push(new google.maps.LatLng(location.Latitude,location.Longitude));
      else
      part3.push(new google.maps.LatLng(location.Latitude,location.Longitude));
    }); */

    /* muniCir.forEach((item)=>{
      part4.push(new google.maps.LatLng(Object.values(munXregion).flat().filter(e => e[0] == item)[0][2], Object.values(munXregion).flat().filter(e => e[0] == item)[0][3]));
    }); */

    cocinas = new google.maps.visualization.HeatmapLayer({
      data: puntos.cocinas,
      //map : map ,
      gradient : [
      'rgba(0, 255, 255, 0)',
      'rgba(211, 255, 0, 1)',
      'rgba(211, 255, 0, 1)',
      'rgba(211, 255, 0, 1)',
      'rgba(211, 255, 0, 1)',
      /* 'rgba(211, 196, 0, 0.2)',
      'rgba(211, 127, 0, 0.3)',
      'rgba(211, 96, 0, 0.4)', */
      'rgba(168, 96, 0, 1)',
      'rgba(168, 96, 0, 1)',
      'rgba(168, 96, 0, 1)',
      'rgba(168, 96, 0, 1)',
      /* 'rgba(150, 68, 0, 0.6)',
      'rgba(140, 50, 0, 0.7)',
      'rgba(130, 50, 0, 0.8)', */
      'rgba(170, 20, 0, 1)',
      'rgba(170, 20, 0, 1)',
      'rgba(170, 20, 0, 1)',
      'rgba(170, 20, 0, 1)'
      ],
      radius : tamañoBolita
    });

    /* part4.forEach(function (item, index) {
      let temp = new google.maps.Marker({
        position: item,
        label: "" + (index + 1)
      });
      circulos.push(
        temp
      );
    }) */
    puntos.cocinas.forEach(function (item, index) {
      let temp = new google.maps.Marker({
        position: item,
        //map: map,
        icon: ic,
        title: "cocina "+index,
        zindex: 11101
      })
      temp.addListener('click', function (event) {
        infoWindow.setContent(detalles(index));
        infoWindow.open(map, temp)
      });
      cmarks.push(
        temp
      );
    })
    escuelas = new google.maps.visualization.HeatmapLayer({
      data: puntos.escuelas,
      //map : map,
      gradient : [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 80, 1)',
      'rgba(0, 191, 80, 1)',
      'rgba(0, 127, 80, 1)',
      'rgba(0, 96, 70, 1)',
      'rgba(0, 63, 60, 1)',
      'rgba(0, 63, 55, 1)',
      'rgba(0, 63, 40, 1)',
      'rgba(0, 63, 30, 1)',
      'rgba(0, 63, 20, 1)'
      ],
      radius : tamañoBolita
    });
    puntos.escuelas.forEach(function (item, index) {
      let temp = new google.maps.Marker({
        position: item,
        //map: map,
        icon: ie,
        title: "escuela"+index,
        zindex: 11101
      })
      temp.addListener('click', function (event) {
        infoWindow.setContent(detalles(index));
        infoWindow.open(map, temp)
      });
      emarks.push(
        temp
      );
    })
    casas = new google.maps.visualization.HeatmapLayer({
      data: puntos.sujetos,
      //map : map,
      gradient : [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 255, 1)',
      'rgba(0, 191, 255, 1)',
      'rgba(0, 127, 255, 1)',
      'rgba(0, 63, 255, 1)',
      'rgba(0, 0, 255, 1)',
      'rgba(0, 0, 223, 1)',
      'rgba(0, 0, 191, 1)',
      'rgba(0, 0, 159, 1)',
      'rgba(0, 0, 127, 1)'
      ],
      radius : tamañoBolita
    });
    puntos.sujetos.forEach(function (item, index) {
      let temp = new google.maps.Marker({
        position: item,
        //map: map,
        icon: is,
        title: "casa de dia "+index,
        zindex: 11101
      })
      temp.addListener('click', function (event) {
        infoWindow.setContent(detalles(index));
        infoWindow.open(map, temp)
      });
      smarks.push(
        temp
      );
    })
  }
  function toggleDatos(data,state) {
    switch (data) {
      case 'cocinas':
        if (map.getZoom() > 10) {
          cmarks.forEach(function(punto){
            punto.setMap(punto.getMap() ? null : map);
          });
        } else {
          cocinas.setMap(cocinas.getMap() ? null : map);
        }
        break;
      case 'escuelas':
        if (map.getZoom() > 10) {
          emarks.forEach(function(punto){
            punto.setMap(punto.getMap()?null:map);
          });
        } else {
          escuelas.setMap(escuelas.getMap() ? null : map);
        }
        break;
      case 'asociaciones':
        if (map.getZoom() > 10) {
          smarks.forEach(function(punto){
            punto.setMap(punto.getMap()?null:map);
          })
        } else {
          casas.setMap(casas.getMap() ? null : map);
        }
        break;
      /* case 'circulos':
        circulos.forEach(function(punto){
          punto.setMap(punto.getMap() ? null : map);
        });
      break;  */
      case 'todo':
        toggleDatos('cocinas');
        toggleDatos('escuelas');
        toggleDatos('asociaciones');
        break;
      default:
        Regiones[data.toUpperCase()].forEach(function (mun) {
          mun.setOptions({
            strokeColor : state?colores[data.toUpperCase()]:'#00000080',
            strokeOpacity: state?.5:.3,
            fillColor   : state?colores[data.toUpperCase()]:'#000',
          });
        });
        break;
    }
  }
  function toggleMarkersHeatMap(params) {
    if(map.getZoom() > 10 ){
      if(cocinas.getMap()!=null){
        cmarks.forEach(function(punto){
          punto.setMap(map);
        });
        cocinas.setMap(null);
      }
      if(escuelas.getMap()!=null){
        emarks.forEach(function(punto){
          punto.setMap(map);
        });
        escuelas.setMap(null);
      }
      if(casas.getMap()!=null){
        smarks.forEach(function(punto){
          punto.setMap(map);
        });
        casas.setMap(null);
      }
    } else {
      if(cmarks[0].getMap()!=null){
        cocinas.setMap(map);
        cmarks.forEach(function(punto){
          punto.setMap(null);
        });
      }
      if(emarks[0].getMap()!=null){
        escuelas.setMap(map);
        emarks.forEach(function(punto){
          punto.setMap(null);
        });
      }
      if(smarks[0].getMap()!=null){
        casas.setMap(map);
        smarks.forEach(function(punto){
          punto.setMap(null);
        });
      }
    }
  }
  function recentrar(){
    if ( $(map.getDiv()).children().eq(0).height() == window.innerHeight && $(map.getDiv()).children().eq(0).width()  == window.innerWidth )
      map.setOptions({minZoom : 8.8})
    else
      map.setOptions({minZoom : 8.33})
  }
  function initControls() {
    $('#municipios').autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: '{{ route('municipios.select') }}',
          dataType: "json",
          method : 'get',
          data: {
    					search: request.term
    			},
          success: function (data) {
            $.map(data,function(n,i){
            /*   var pa = n.primer_apellido == null ? '' : n.primer_apellido
              var sa = n.segundo_apellido == null ? '' : n.segundo_apellido */
              n.label = n.nombre
            })
            response(data);
          }
        });
      },
      minLength: 4,
      select: function( event, ui ) {
        let data = ui.item;
        map.setZoom(15);
        map.setCenter({lat: parseFloat(data.lat), lng: parseFloat(data.lng)});
        $('#municipios').val('')
      },
      appendTo : "#controles"
    });
    $('#busqueda').click(function () {
      $('#municipios').val('');
    });
    $('input:checkbox').change(function (event) {
      toggleDatos($(event.target).attr('data-on').toLowerCase(),$(event.target).prop('checked'));
    })

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.querySelector('#controles'));
  }
</script>
@endpush
