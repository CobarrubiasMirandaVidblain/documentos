<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reporte</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            .container{
                margin-left: 0px;
                margin-right: 15px;
                width: 100% !important;
            }
            .row {
                margin-right: 0px; 
                margin-left: 0px; 
            }
            #logo{
                max-height: 50px;
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row" style="text-align:center;">

                <div class="col-6">Revisó</div>
                <div class="col-6">Solicitó</div>
            </div>
            <hr style= "border:3px solid #979A9A">
        
        </div>
        <script src="{{ asset('reportes/bootstrap.min.js') }}" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>