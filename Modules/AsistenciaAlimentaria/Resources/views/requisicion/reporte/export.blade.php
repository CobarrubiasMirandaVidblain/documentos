<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Print Table</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body{
                display: flex;
                margin-top: 0;
                flex-wrap: wrap;
            }
            .flex-container {
                width: 100%;
                display: flex;
                justify-content: flex-end;
            }
            .flex-container > div {
                text-align: center;
                font-size: 8px;
                align-self: center;
            }
            div.footer{
                width: 100%;
            }
            div.footer img {
                width: 100%;
                height: auto;
            }
            body {
                display: flex;
                min-height: 95vh;
                flex-direction: column;
            }
            main {
                flex: 1 0 auto;
                border:solid 2px #e4e4e4;
            }
            header{
                display: flex;
                justify-content: flex-end;
            }
            #logo{
                width: auto;
                max-height: 70px;
                margin-bottom: 5px;
                float: right;;
            }
            .page-break{ display: block; page-break-before: always; }
            table{
                margin-bottom: 50px;
                margin-top: 50px;
            }
        </style>
    </head>
    <body style="font-size:12px">
        <table class="table table-bordered table-sm table-striped">
            
               
        </table>
    </body>
</html>
