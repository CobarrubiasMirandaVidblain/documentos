<div class="modal-header">
    <div class="modal-title">
        <h5><strong>Validando</strong></h5>
    </div>
    <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form id="form-validacions">
        <div class="form-group row">
            <label class="col-sm-2 col-form-label">Folio recibo:</label>
            <div class="col-sm-4">
                <input type="text" class="form-control-plaintext" value="{{ $progReq->recibo->id }}" readonly>
            </div>
            <label class="col-sm-2 col-form-label">Folio cocina:</label>
            <div class="col-sm-4">
                <input type="text" class="form-control-plaintext" value="{{ $progReq->programacion->alimentario->foliodif }}" readonly>
            </div>
        </div>
        @if($progReq->requisicion->programa_id == config('asistenciaalimentaria.desayunosId'))
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Nombre escuela:</label>
          <div class="col-sm-4">
              <input type="text" class="form-control-plaintext" value="{{ $progReq->programacion->alimentario->desayuno->escuela->nombre }}" readonly>
          </div>
          <label class="col-sm-2 col-form-label">Clave escuela:</label>
          <div class="col-sm-4">
              <input type="text" class="form-control-plaintext" value="{{ $progReq->programacion->alimentario->desayuno->escuela->clave }}" readonly>
          </div>
        </div>
        @endif
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Municipio:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ $progReq->programacion->alimentario->municipio->nombre }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Localidad:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ $progReq->programacion->alimentario->localidad->nombre }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Sub-localidad:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ $progReq->programacion->alimentario->sublocalidad or '----------' }}" readonly>
                    </div>
                </div>
                @foreach ($comites as $comite)
                    @if (strpos($comite->cargoprograma->cargo->nombre,'PRESIDENTE')!==false)
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Presidente(a):</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" value="{{ $comite->persona->nombreCompleto() }}" readonly>
                        </div>
                    </div>
                    @endif
                    @if ($oficio->programa_id != config('asistenciaalimentaria.ccnId') && strpos($comite->cargoprograma->cargo->nombre,'SECRETARIO')!==false)
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Secretari@:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" value="{{ $comite->persona->nombreCompleto() }}" readonly>
                        </div>
                    </div>
                    @endif
                    @if ($oficio->programa_id != config('asistenciaalimentaria.ccncId') && strpos($comite->cargoprograma->cargo->nombre,'TESORERO')!==false)
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Tesorer@:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" value="{{ $comite->persona->nombreCompleto() }}" readonly>
                        </div>
                    </div>
                    @endif
                    @if ($oficio->programa_id == config('asistenciaalimentaria.ccncId') && strpos($comite->cargoprograma->cargo->nombre,'ABASTO')!==false)
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Vocal de abasto:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" value="{{ $comite->persona->nombreCompleto() }}" readonly>
                        </div>
                    </div>
                    @endif
                    @if ($oficio->programa_id == config('asistenciaalimentaria.ccncId') && strpos($comite->cargoprograma->cargo->nombre,'SALUD')!==false)
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Vocal de salud:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" value="{{ $comite->persona->nombreCompleto() }}" readonly>
                        </div>
                    </div>
                    @endif
                @endforeach
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Fecha de entrega:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ isset($progReq->requisicionDatoCarga) ? (isset($progReq->requisicionDatoCarga->entregaproveedor) ? $progReq->requisicionDatoCarga->entregaproveedor->fecha_entrega : 'SIN ENTREGAR') : 'SIN ENTREGAR' }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Quien entrego:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ isset($progReq->requisicionDatoCarga) ? strtoupper($progReq->requisicionDatoCarga->carga->chofer->getNombreCompleto()) : 'SIN REGISTRAR'  }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Placas vehículo:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ isset($progReq->requisicionDatoCarga) ? $progReq->requisicionDatoCarga->carga->placas : 'SIN REGISTRAR' }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Quien recibió:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ isset($progReq->requisicionDatoCarga->entregaproveedor) ? (isset($progReq->requisicionDatoCarga->entregaproveedor->comite) ? $progReq->requisicionDatoCarga->entregaproveedor->comite->persona->nombreCompleto() : $progReq->requisicionDatoCarga->entregaproveedor->observaciones ) : 'SIN REGISTRAR' }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">No de oficio:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ $oficio->num_oficio }}" readonly>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                @php
                    $conta=0;
                    $costo=0;
                @endphp
                @foreach ($progReq->requisicioncantidaddotacion()->with('dotacion')->get()->sortBy('dotacion.tipoaccion_id') as $cd)
                    @if ($progReq->requisicion->programa_id == config('asistenciaalimentaria.ccncId'))
                        <div class="form-group row">
                            <label class="col-sm-8 col-form-label">Dot. {!! mb_strtolower(explode('(',explode(')',$cd->dotacion->tipoaccion->nombre)[0])[1]) !!}:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control-plaintext" value="{{ $cd->cantidad_dotaciones }}" readonly>
                            </div>
                        </div>
                    @endif
                    @php
                        $conta+=$cd->cantidad_dotaciones;
                        $costo+=$cd->cantidad_dotaciones * $cd->dotacion->costo();
                    @endphp
                @endforeach
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Dotaciones totales:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ $conta }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Importe:</label>
                    <div class="col-sm-8">
                        <input id="costo" type="text" class="form-control-plaintext" value="$" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Entregado:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control-plaintext" value="{{ isset($progReq->requisicionDatoCarga->entregaproveedor) ? 'ENTREGADO' : 'NO ENTREGADO' }}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Pago:</label>
                    <div class="col-sm-8">
                        <select type="text" class="form-control" name="estatus">
                            <option value="1">APROBADO</option>
                            <option value="0">RECHAZADO</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <input type="text" name="entregaproveedor_id" style="display:none;" value="{{ isset($progReq->requisicionDatoCarga) ? (isset($progReq->requisicionDatoCarga->entregaproveedor) ? $progReq->requisicionDatoCarga->entregaproveedor->id : '')   : '' }}"/>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    @if (isset($progReq->requisicionDatoCarga))
        @if(isset($progReq->requisicionDatoCarga->entregaproveedor) && isset($progReq->requisicionDatoCarga->entregaproveedor->validacion))
            @if (auth()->user()->hasRoles(['ADMINVALIDADOR']))
                <button class="btn btn-warning pull-right" onclick='transaccion.miniAjax("{{ route("alimentarios.validacion.entregas.destroy",[$oficio->id,$progReq->requisicion_id,$progReq->requisicionDatoCarga->entregaproveedor->validacion->id]) }}","DELETE")'>
                    <span><i class="fas fa-unlock"></i> Desbloquear</span>
                </button>
            @endif
        @else
        <button class="btn btn-primary pull-right" onclick='transaccion.formAjax("{{ route("alimentarios.validacion.entregas.store",[$oficio->id,$progReq->requisicion_id]) }}","POST","form-validacions")' {{ isset($progReq->requisicionDatoCarga->entregaproveedor) ? '' : 'disabled' }}>
            <span><i class="fas fa-save"></i> Guardar</span>
        </button>
        @endif
    @endif
</div>
<script type="text/javascript">
    (function(){
        validacion.iniciar('form-validacions')
        $("#costo").val('$'+new Intl.NumberFormat().format("{{ $costo }}"))
    })()
    
</script>
