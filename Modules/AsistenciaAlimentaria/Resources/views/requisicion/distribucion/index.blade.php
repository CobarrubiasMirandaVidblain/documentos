@extends('asistenciaalimentaria::layouts.master')

@section('content-subtitle', 'Tabla de selección de cocinas')

@push('head')
@endpush

@section('content')
<div class="box box-primary">
    <div class="box box-header with-border text-center">
        <h4>PROGRAMACIONES DE <strong class="font-italic">{{$requisicion->programa->nombre}}</strong> PARA LA SIG. REQUISICIÓN:</h4>
        <div class="container">
            <div class="row">
                <div class="col-sm"><h5><strong class="font-italic">NÚMERO DE LICITACIÓN: </strong>  {{$requisicion->licitacion->num_licitacion or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">BIMESTRE: </strong>  {{$requisicion->num_bimestre or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">TIPO DE ENTREGA: </strong>  {{$requisicion->num_entrega or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">NÚMERO DE OFICIO: </strong>  {{$requisicion->num_oficio or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">PERIODO: </strong>  {{$requisicion->periodo or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">¿ESPECIAL? </strong>  {{$requisicion->especial == 1 ? 'SI' : 'NO'}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">ESTADO </strong>  {{$requisicion->estadoprograma->estado->nombre or ''}}</h5></div>
            </div>
        </div>
    </div>
    <div class="box box-body">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Programaciones seleccionadas
                <div class="card-header-actions">
                    <a class="card-header-action" href="https://datatables.net" target="_blank">
                    <small class="text-muted">docs</small>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                    <input type="text" id="search" name="search" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </div>
                </div>
                {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap
                dataTables_wrapper dt-bootstrap4', 'style'=>'width:100%', 'id' => 'seleccion'],true) !!}
            </div>
        </div>
    </div>
</div>
@stop
@include('asistenciaalimentaria::requisicion.distribucion.info')
@push('body')
	{!! $dataTable->scripts() !!}
	<script type="text/javascript">
    var tabla = 
        ((tablaId) => {
        $('#search').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search').val()).draw()
            }
        })
        $('#btn_buscar').on('click', function() {
            $(tablaId).DataTable().search($('#search').val()).draw()
        })
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        return {
            recargar : recargar
    }})('#seleccion');
    //exito en las operaciones anteriores
    function ajaxSuccess(params) {
        tabla.recargar();
        Swal.fire('¡Guardado!','Operación realizada con éxito, verifique antes de salir','success')
    }
    //fallo en las operaciones anteriores
    function ajaxError(params) {
        tabla.recargar();
        Swal.fire('¡Error!',params.responseJSON.message,'error')
    }
    function enviarProveedor(url) {
        Swal.fire({
            title: "¿Estás seguro?",
            text: "Se enviará al proveedor esta requisición!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, hazlo!",
            cancelButtonText: "No."
        }).then((result) => {
            if (result.value) {
                transaccion.miniAjax(url,"PUT",{estado:"ENVIADA"})
                location.reload(true)
            }
        })
    }
    $('#seleccion').on('xhr.dt', function ( e, settings, json, xhr ) {
        var td = 0,tb = 0;
        for (const r in json.regiones) {
            console.log(r);
            if (json.regiones.hasOwnProperty(r)) {
                $('#region-'+r).html(json.regiones[r]);
            }
        }
        for (const a in json.acciones) {
            if (json.acciones.hasOwnProperty(a)) {
                $('#accion-'+a).html(json.acciones[a]);
                td+=parseInt(json.acciones[a]);
            }
        }
        for (const b in json.beneficiarios) {
            if (json.beneficiarios.hasOwnProperty(b)) {
                $('#beneficiarios-'+b).html(json.beneficiarios[b]);
                tb+=parseInt(json.beneficiarios[b]);
            }
        }
        $("#tp").html(json.recordsTotal)
        $("#td").html(td)
        $("#tb").html(tb)
    })
    function swalCancelacion(url,type,table) {
        Swal.fire({
            title: 'Motivo de cancelación',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
                cancelButtonText: 'Cancelar',
            confirmButtonText: 'Guardar',
            showLoaderOnConfirm: true,
            preConfirm: (motivo) => {
                transaccion.miniAjax(url+'?m='+motivo,type,{},'json',true,'application/x-www-form-urlencoded; charset=UTF-8',table.split('-')[1])
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }
  </script>
@endpush