{{-- Esta vista se usa en la selección de programaciones y en las programaciones seleccionadas --}}
@section('aside')
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#regiones" role="tab">
            <i class="fas fa-globe-americas"></i>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#beneficiarios" role="tab">
            <i class="fas fa-users"></i>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#dotaciones" role="tab">
            <i class="fas fa-box"></i>
        </a>
    </li>
</ul>
<!-- Tab panes-->
<div class="tab-content">
    <div class="tab-pane active" id="regiones" role="tabpanel">
        <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Total de programaciones seleccionadas
            </div>
            <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
                <div class="text-center">
                    <strong id="tp"> <i class="fas fa-spinner fa-spin"></i> </strong>
                </div>
            </div>
        </div>
        <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Programaciones por region
            </div>
            @foreach ($regiones as $region)
                <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
                    <div>
                        <strong id="region-{{ $region->id }}"> <i class="fas fa-spinner fa-spin"></i> </strong>
                    </div>
                    <small> {{ $region->nombre }} </small>
                </div>  
            @endforeach
        </div>
    </div>
    <div class="tab-pane " id="beneficiarios" role="tabpanel">
        <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Total de beneficiarios
            </div>
            <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
                <div class="text-center">
                    <strong id="tb"> <i class="fas fa-spinner fa-spin"></i> </strong>
                </div>
            </div>
        </div>
        <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Beneficiario por supPrograma
            </div>
            @foreach ($acciones as $accion)
            <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
                <div>
                    <strong id="beneficiarios-{{ $accion->id }}"> <i class="fas fa-spinner fa-spin"></i> </strong>
                </div>
                <small> {{ $accion->nombre }} </small>
            </div>
            @endforeach
        </div>
    </div>
    <div class="tab-pane " id="dotaciones" role="tabpanel">
        <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Total de dotaciones a entregar
            </div>
            <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
                <div class="text-center">
                    <strong id="td"> <i class="fas fa-spinner fa-spin"></i> </strong>
                </div>
            </div>
        </div>
        <div class="list-group list-group-accent">
            <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">
                Dotaciones por supPrograma
            </div>
            @foreach ($acciones as $accion)
            <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
                <div>
                    <strong id="accion-{{ $accion->id }}"> <i class="fas fa-spinner fa-spin"></i> </strong>
                </div>
                <small> {{ $accion->nombre }} </small>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection