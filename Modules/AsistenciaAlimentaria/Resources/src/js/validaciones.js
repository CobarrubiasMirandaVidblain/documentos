var validacion = (function () {
  
  var form = null

  function init(formId,extraRules=false,addReglas=true) {    
    form = $("#"+formId)
    if(addReglas)
      form.validate({
        rules: beforeOfValidate(formId,extraRules)
      })
  }
  
  function check() {
    return form.valid()
  }

  function beforeOfValidate(formId,extraRules=false) {
    let rulesAlim = {}
    $(':text,select').each(function(index,item){
      let componentJQ = $(item)
      rulesAlim[componentJQ.attr('name')] = {
        minlength: function(element) {
          return (componentJQ.attr('minlength') == '' || componentJQ.attr('minlength') === undefined) ? 0 : parseInt(componentJQ.attr('minlength'))
        },
        required: {
          depends: function(element) {
            return componentJQ.attr('required')?true:false
          }
        }
      }
    })
    if(extraRules){
      for(const i in extraRules){
        if(rulesAlim.hasOwnProperty(i)) {
          for (const key in extraRules[`${i}`]) {
            rulesAlim[`${i}`][`${key}`] = extraRules[`${i}`][`${key}`]
          }
        } else {
          rulesAlim[`${i}`] = extraRules[`${i}`]
        }
      }
    }
    return rulesAlim
  }

  return {
    iniciar : init,
    revisar : check
  }
}()) 