$.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } })

var transaccion = (function(){

  function swalDataAjax(url, type, formNameId, extraData = false,contype=undefined) {
    if(type.toLowerCase() != 'delete')
      if(!validacion.revisar()) return false
    var accion    = '',elemento = ''
    separadores   = ['-','_']
    arrayElemento = formNameId.split(new RegExp(separadores.join('|'),'g'))
    preElemento   = arrayElemento[arrayElemento.length-1]
    elemento      = preElemento.substring(preElemento.length-1,preElemento.length) === 's' ? preElemento.slice(0,-1) : preElemento
    accion        = type.toLowerCase() == 'post' ? 'creara' : type.toLowerCase() == 'put' ? 'modificara' : ' dará de baja'
    Swal.fire({
      title: '¿Estas seguro?',
      text: `¡Se ${accion} un(a) ${elemento}!`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#28a745',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'SI',
      cancelButtonText: 'NO'
    }).then( (result) => {
      if (result.value) {
        if(type.toLowerCase == 'delete')
          transaccion.miniAjax(url,type,{},'json',true,'application/x-www-form-urlencoded; charset=UTF-8',formNameId.split('-')[1])
        else {
          var formData = new FormData($('#'+formNameId)[0])
          if(extraData){
            for(const i in extraData){
              formData.append(`${i}`,extraData[i])
            }
          }
          if(type=='PUT'){
            type='POST'
            formData.append('_method','PUT')
          }
          if(contype!=undefined)
            transaccion.miniAjax(url,type,formData,'json',false,contype,formNameId.split('-')[1])
          else
            transaccion.miniAjax(url,type,formData,'json',false,false,formNameId.split('-')[1])
        }
      }
    })
  }
  
  function sendRequest(url, type, formData={}, dataType='json', processData=true, contentType='application/x-www-form-urlencoded; charset=UTF-8', tableName) {
    var tipo, titulo
    $.ajax({
      url:  url,
      type: type,
      data: formData,
      dataType: dataType,
      processData: processData, 
      contentType: contentType
    })
    .done(function (data, textStatus, jqXHR) {
      if (typeof ajaxSuccess === 'function') {
        ajaxSuccess(jqXHR, tableName);
      } else {
        titulo = 'Operacion exitosa'
        tipo = 'success'
        $('.modal').modal('hide')
        Swal.fire({
          title: titulo,
          text: jqXHR.responseJSON.message,
          type: tipo
        })
      }
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      if (typeof ajaxError === 'function') {
        ajaxError(jqXHR, tableName);
      } else {
        titulo = 'Ocurrio un error'
        tipo = 'warning'
      }
      Swal.fire({
        title: titulo,
        text: jqXHR.responseJSON.message,
        type: tipo
      })
    })
    .always(function (data, textStatus, jqXHR){
      $.each(window.LaravelDataTables, function (index, item) {
        item.ajax.reload(null,false)
      })
    })
  }
  
  return {
    formAjax : swalDataAjax,
    miniAjax : sendRequest
  }
})()

