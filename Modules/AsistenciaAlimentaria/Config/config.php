<?php

return [
    'name'        => 'AsistenciaAlimentaria',
    'anioActual'  => 2019,  //podran elegir que en que año trabajar
    'fatherId'    => 607,
    'ccncId'      => 608,
    'desayunosId' => 609,
    'sujetosId'   => 611,
    'lecheId'     => 612,
    'finCCNC'     => 1,     //id correspondiente a federal
    'moduloId'    => 23,
    'areaDesarrolloId' => 9,
    'cuota'       => 10     //monto en pesos de lo que se debe pagar pór dotación
];
