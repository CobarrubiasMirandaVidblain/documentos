<?php

namespace Modules\Panaderia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Panaderia\Entities\Tipoproducto;

class TiposProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Tipoproducto::create(['nombre'=>'PAN SALADO']);
        Tipoproducto::create(['nombre'=>'PAN DULCE']);
        Tipoproducto::create(['nombre'=>'REPOSTERIA']);
        // $this->call("OthersTableSeeder");
    }
}
