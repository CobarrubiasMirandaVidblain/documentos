<?php

namespace Modules\Panaderia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PanaderiaDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(EstatusPanaderiaSeeder::class);
        $this->call(TiposProductosTableSeeder::class);
        // $this->call("OthersTableSeeder");
     
        $this->call(ProductosSeeder::class);        
        $this->call(CatEstatusEquiposSeeder::class);       

    }
}
