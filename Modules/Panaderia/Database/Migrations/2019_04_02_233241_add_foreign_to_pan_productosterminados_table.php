<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanProductosterminadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_productosterminados', function (Blueprint $table) {
            $table->foreign('produccion_id')->references('id')->on('pan_producciones');
            $table->foreign('producto_id')->references('id')->on('pan_productos');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_productosterminados', function (Blueprint $table) {
            $table->dropForeign(['produccion_id']);
            $table->dropForeign(['producto_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_productosterminados_produccion_id_foreign');  
            $table->dropIndex('pan_productosterminados_producto_id_foreign');  
            //$table->dropIndex('pan_productosterminados_usuario_id_foreign');
        });
    }
}
