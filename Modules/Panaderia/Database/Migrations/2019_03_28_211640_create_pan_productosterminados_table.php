<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yulissa
  MOTIVO: Registro de los productos terminados de la producción del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/

class CreatePanProductosterminadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_productosterminados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('produccion_id');
            $table->unsignedInteger('producto_id');
            $table->integer('cantidad');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_productosterminados');
    }
}
