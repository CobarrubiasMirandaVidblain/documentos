<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_equipos', function (Blueprint $table) {
            $table->foreign('estatusequipo_id')->references('id')->on('pan_cat_estatusequipos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_equipos', function (Blueprint $table) {
            $table->dropForeign(['estatusequipo_id']);
            $table->dropIndex('pan_equipos_estatusequipo_id_foreign');
        });
    }
}
