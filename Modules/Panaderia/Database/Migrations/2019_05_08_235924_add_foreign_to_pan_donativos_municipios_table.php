<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanDonativosMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_donativos_municipios', function (Blueprint $table) {
            $table->foreign('donativo_id')->references('id')->on('pan_donativos');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
           // $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_donativos_municipios', function (Blueprint $table) {
            $table->dropForeign(['donativo_id']);
            $table->dropForeign(['municipio_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_donativos_municipios_donativo_id_foreign');  
            $table->dropIndex('pan_donativos_municipios_municipio_id_foreign');  
            //$table->dropIndex('pan_beneficiarios_donativos_usuario_id_foreign');  
        });
    }
}
