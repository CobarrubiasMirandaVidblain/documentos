<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanDevolucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_devoluciones', function (Blueprint $table) {
            $table->foreign('pedido_id')->references('id')->on('pan_pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_devoluciones', function (Blueprint $table) {
            $table->dropForeign(['pedido_id']);       
            $table->dropIndex('pan_devoluciones_pedido_id_foreign'); 
        });
    }
}
