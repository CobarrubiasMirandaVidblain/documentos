<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Yulissa
  MOTIVO: Registro de los productos pertenecientes a una producción programada del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanProductosprogramadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_productosprogramados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('produccionprogramada_id');
            $table->unsignedInteger('producto_id');
            $table->integer('cantidad');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_productosprogramados');
    }
}
