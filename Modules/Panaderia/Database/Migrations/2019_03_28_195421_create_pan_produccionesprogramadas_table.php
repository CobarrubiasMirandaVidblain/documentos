<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de la calendarización de producción normal.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanProduccionesprogramadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_produccionesprogramadas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('estatus_id');
            $table->date('fecha_programada');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_produccionesprogramadas');
    }
}
