<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanDetallessolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_detallessolicitud', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('pan_solicitudes');
            $table->foreign('producto_id')->references('id')->on('pan_productos');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_detallessolicitud', function (Blueprint $table) {
            $table->dropForeign(['solicitud_id']);
            $table->dropForeign(['producto_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_detallessolicitud_solicitud_id_foreign');  
            $table->dropIndex('pan_detallessolicitud_producto_id_foreign');  
            //$table->dropIndex('pan_detallessolicitud_usuario_id_foreign');
        });
    }
}
