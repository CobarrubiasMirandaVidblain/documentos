<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanSalidasinsumoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_salidasinsumo', function (Blueprint $table) {
            $table->foreign('insumo_id')->references('id')->on('pan_insumos');   
            $table->foreign('produccion_id')->references('id')->on('pan_producciones');
            $table->foreign('producto_id')->references('id')->on('pan_productos');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_salidasinsumo', function (Blueprint $table) {
            $table->dropForeign(['insumo_id']);
            $table->dropForeign(['produccion_id']);                    
            $table->dropForeign(['producto_id']);
            //$table->dropForeign(['usuario_id']);     
            $table->dropIndex('pan_salidasinsumo_insumo_id_foreign');    
            $table->dropIndex('pan_salidasinsumo_produccion_id_foreign'); 
            $table->dropIndex('pan_salidasinsumo_producto_id_foreign'); 
             
            //$table->dropIndex('pan_salidasinsumo_usuario_id_foreign');
        });
    }
}
