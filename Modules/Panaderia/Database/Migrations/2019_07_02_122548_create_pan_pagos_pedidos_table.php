<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Yulissa
  MOTIVO: Registro del historial los pagos de los pedidos. 
  ALCANCE: Panadería (Residencia)
*/
class CreatePanPagosPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_pagos_pedidos', function (Blueprint $table) {
            $table->increments('id');           
            $table->unsignedInteger('pedido_id');
            $table->date('fecha');
            $table->float('pago',8,2);
            $table->string('pago_id')->unique()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_pagos_pedidos');
    }
}
