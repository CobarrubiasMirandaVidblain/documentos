<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yulissa
  MOTIVO: Para el registro del donativo y de la dependencia que lo solicita.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanInstitucionesDonativos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_instituciones_donativos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('institucion_id');
            $table->unsignedInteger('donativo_id')->unique();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_instituciones_donativos');
    }
}
