<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_recetas', function (Blueprint $table) {
            $table->foreign('producto_id')->references('id')->on('pan_productos');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_recetas', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_recetas_producto_id_foreign');
            //$table->dropIndex('pan_recetas_usuario_id_foreign');
        });
    }
}
