<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanBeneficiariosDonativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_beneficiarios_donativos', function (Blueprint $table) {
            $table->foreign('donativo_id')->references('id')->on('pan_donativos');
            $table->foreign('persona_id')->references('id')->on('personas');
           // $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_beneficiarios_donativos', function (Blueprint $table) {
            $table->dropForeign(['donativo_id']);
            $table->dropForeign(['persona_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_beneficiarios_donativos_donativo_id_foreign');  
            $table->dropIndex('pan_beneficiarios_donativos_persona_id_foreign');  
            //$table->dropIndex('pan_beneficiarios_donativos_usuario_id_foreign');  
        });
    }
}
