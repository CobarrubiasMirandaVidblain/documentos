<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de la información de las ventas y el total del ingreso.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_ventas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->float('total',8,2);
            $table->date('fecha_venta');
            $table->integer('cantidad_articulos');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_ventas');
    }
}
