<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Para el registro del catálogo de productos que se ofrecen en el Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tipoproducto_id');
            $table->string('nombre')->unique();
            $table->float('precio',8,2);
            $table->integer('stock');            
            $table->boolean('activo');
            $table->string('image')->nullable();
            $table->boolean('solicitado');
            $table->text('descripcion')->nullable();
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_productos');
    }
}
