<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yulissa
  MOTIVO: Control de las salidas de insumo para la producción del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/

class CreatePanSalidasinsumoTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_salidasinsumo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('insumo_id');
            $table->unsignedInteger('produccion_id');
            $table->unsignedInteger('producto_id');
            $table->date('fecha_salida');
            $table->float('cantidad',8,3);
            $table->char('tipo');
            $table->integer('porcion');
            $table->float('costo_salida',8,2);            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_salidasinsumo');
    }
}
