<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanDonativosRegionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_donativos_regiones', function (Blueprint $table) {
            $table->foreign('donativo_id')->references('id')->on('pan_donativos');
            $table->foreign('region_id')->references('id')->on('cat_regiones');
            // $table->foreign('usuario_id')->references('id')->on('usuarios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_donativos_regiones', function (Blueprint $table) {
            $table->dropForeign(['donativo_id']);
            $table->dropForeign(['region_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_donativos_regiones_donativo_id_foreign');  
            $table->dropIndex('pan_donativos_regiones_region_id_foreign');  
            //$table->dropIndex('pan_beneficiarios_donativos_usuario_id_foreign');  
        });
    }
}
