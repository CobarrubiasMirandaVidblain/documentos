<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Para el registro de los tipos de productos que se elaboran en el Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanCatTiposproductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_cat_tiposproductos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_cat_tiposproductos');
    }
}
