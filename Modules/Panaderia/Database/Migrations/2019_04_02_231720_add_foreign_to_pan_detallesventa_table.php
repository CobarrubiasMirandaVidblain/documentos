<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanDetallesventaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_detallesventa', function (Blueprint $table) {
            $table->foreign('venta_id')->references('id')->on('pan_ventas');
            $table->foreign('producto_id')->references('id')->on('pan_productos');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_detallesventa', function (Blueprint $table) {
            $table->dropForeign(['venta_id']);
            $table->dropForeign(['producto_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_detallesventa_venta_id_foreign');  
            $table->dropIndex('pan_detallesventa_producto_id_foreign');  
            //$table->dropIndex('pan_detallesventa_usuario_id_foreign');
        });
    }
}
