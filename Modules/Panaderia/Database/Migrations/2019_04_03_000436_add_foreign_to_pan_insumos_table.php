<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanInsumosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_insumos', function (Blueprint $table) {
            $table->foreign('insumo_id')->references('id')->on('recm_cat_productos');
            $table->foreign('presentacion_id')->references('id')->on('recm_cat_presentacions');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_insumos', function (Blueprint $table) {
            $table->dropForeign(['insumo_id']);
            $table->dropForeign(['presentacion_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_insumos_insumo_id_foreign'); 
            $table->dropIndex('pan_insumos_presenctacion_id_foreign');
            //$table->dropIndex('pan_insumos_usuario_id_foreign');
        });
    }
}
