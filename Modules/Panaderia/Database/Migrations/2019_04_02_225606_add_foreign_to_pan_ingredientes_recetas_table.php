<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanIngredientesRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_ingredientes_recetas', function (Blueprint $table) {
            $table->foreign('receta_id')->references('id')->on('pan_recetas');
            $table->foreign('insumo_id')->references('id')->on('recm_cat_productos');
            $table->foreign('presentacion_id')->references('id')->on('recm_cat_presentacions');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_ingredientes_recetas', function (Blueprint $table) {
            $table->dropForeign(['receta_id']);
            $table->dropForeign(['insumo_id']);
            $table->dropForeign(['presentacion_id']);
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_ingredientes_recetas_receta_id_foreign');  
            $table->dropIndex('pan_ingredientes_recetas_insumo_id_foreign');  
            $table->dropIndex('pan_ingredientes_recetas_presentacion_id_foreign');
            //$table->dropIndex('pan_ingredientes_recetas_usuario_id_foreign');  
        });
    }
}
