<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanDonativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_donativos', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('pan_solicitudes');
            $table->foreign('solicitudgeneral_id')->references('id')->on('solicitudes');
            //$table->foreign('empleado_id')->references('id')->on('pan_empleados');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_donativos', function (Blueprint $table) {
            $table->dropForeign(['solicitud_id']);
            $table->dropForeign(['solicitudgeneral_id']);
            //$table->dropForeign(['empleado_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_donativos_solicitud_id_foreign');
            $table->dropIndex('pan_donativos_solicitudgeneral_id_foreign');
            //$table->dropIndex('pan_donativos_empleado_id_foreign');
            //$table->dropIndex('pan_donativos_usuario_id_foreign');
        });
    }
}
