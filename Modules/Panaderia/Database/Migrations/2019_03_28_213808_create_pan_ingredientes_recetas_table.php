<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Yulissa
  MOTIVO: Registro de los insumos necesarios para la elaboración de los productos del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanIngredientesrecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_ingredientes_recetas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('receta_id');
            $table->unsignedInteger('insumo_id');
            $table->unsignedInteger('presentacion_id');
            $table->float('cantidad',8,3);

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_ingredientes_recetas');
    }
}
