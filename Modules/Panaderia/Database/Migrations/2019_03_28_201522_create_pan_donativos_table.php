<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de la información detallada de los donativos del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanDonativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_donativos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solicitud_id')->unique();
            $table->unsignedInteger('solicitudgeneral_id')->unique();
            $table->string('folio');
            $table->text('justificacion');
        
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_donativos');
    }
}
