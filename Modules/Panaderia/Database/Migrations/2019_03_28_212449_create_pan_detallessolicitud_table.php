<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Yulissa
  MOTIVO: Especificaciones de las solicitudes de donativo o pedidos realizados en el Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanDetallessolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_detallessolicitud', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solicitud_id');
            $table->unsignedInteger('producto_id');
            $table->integer('cantidad');
            $table->float('precio_unitario',8,2);
            $table->float('subtotal');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_detallessolicitud');
    }
}
