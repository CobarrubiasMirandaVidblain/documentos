<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanProductosprogramadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_productosprogramados', function (Blueprint $table) {
            $table->foreign('produccionprogramada_id')->references('id')->on('pan_produccionesprogramadas');
            $table->foreign('producto_id')->references('id')->on('pan_productos');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_productosprogramados', function (Blueprint $table) {
            $table->dropForeign(['produccionprogramada_id']);
            $table->dropForeign(['producto_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_productosprogramados_produccionprogramada_id_foreign');  
            $table->dropIndex('pan_productosprogramados_producto_id_foreign');  
            //$table->dropIndex('pan_productosprogramados_usuario_id_foreign');
        });
    }
}
