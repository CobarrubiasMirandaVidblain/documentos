let mix = require('laravel-mix');

mix.js('Resources/assets/js/app.js', './../../public/panaderiam')
  .sass('Resources/assets/sass/app.scss', './../../public/panaderiam')
  // .options({
  //     processCssUrls: false
  // })
  .setPublicPath('./../../public/panaderiam');