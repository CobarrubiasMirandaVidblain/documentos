<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use App\Models\Modulo;
use \Firebase\JWT\JWT;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PanaderiaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','authorized'], ['only' => ['index','permissions']]);
        $this->middleware('rolModuleV2:panaderia,ALMACEN,ADMINISTRADOR,VENDEDOR,ENCARGADO,PRODUCCION', ['only' => ['index','permissions']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
   
        $usuario = auth()->user();
       
        $token = [
            'usuario_id' => $usuario->id,
            'modulo_id' => Modulo::where('nombre', 'like', 'PANADERIA')->first()->id,
            'rol_id' => $usuario->rolespanaderia->first()->pivot->rol_id
        ];
        $encoded = JWT::encode($token, config('app.jwt_panaderia_token'), 'HS256');
        $usuario->token = $encoded;
        $usuario->vida_token = Carbon::now()->addMinutes(40); 
        $usuario->save();
        return view('panaderia::indexPanaderia', ['token' => $encoded]);   
    
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('panaderia::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function redirect(){
        redirect('/panaderia/spa');
    }

    public function permissions(){
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        $modulo = Modulo::where('nombre', 'like', 'panaderia')->first();
        $roles = \App\Models\UsuariosRol::select('id', 'modulo_id', 'rol_id', 'usuario_id')->where('modulo_id', $modulo->id)->where('usuario_id', $usuario->id)->with('rol:id,usuario_id,nombre')->get();

        if(count($roles) <= 0) return new JsonResponse([['id' => 0, 'name' => 'VISITANTE']]);

        foreach($roles as $role){
            $role->name = $role->rol->nombre;
        }
        return new JsonResponse($roles);
    }
}
