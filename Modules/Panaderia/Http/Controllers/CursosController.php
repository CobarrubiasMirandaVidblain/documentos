<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\CatEvento;
use App\Models\Statusproceso;
use App\Models\EventosTaller;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;

class CursosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','update' ,'show','destroy']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store','update', 'destroy']]);
    }
    public function index()
    {
        $eventos=EventosTaller::with(['area','beneficio','estatus'])
                    ->whereHas('area', function($q) {
                    // Query the id field in area table
                    $q->where('id',2); // '=' is optional
                })->orderBy('id','DESC')->get();  

       return $eventos;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $curso=(object)$request->curso;
            $beneficio=(object)$curso->beneficio;
            $municipio=(object)$curso->municipio;
            $localidad=(object)$curso->localidad;
            $evento=(object)$curso->tipoEvento;

            $estatus= Statusproceso::where('status','PENDIENTE')->select('id')->first();
            $contador = EventosTaller::orderBy('id','DESC')->first();
            
            $Nuevo = EventosTaller::create([
                'tipo_evento_id' => $evento->id,
                'localidad_id' => $localidad->id,
                'municipio_id' => $municipio->id,
                'status_id' => $estatus->id,
                'calle' => strtoupper($curso->calle),
                'numExt' => $curso->numeroExterior,
                'numInt' => $curso->numeroInterior,
                'colonia' => strtoupper($curso->colonia),
                'descripcionEvento' => strtoupper($curso->descripcion),
                'codigoPostal' => $curso->codigo_postal,                
                'area_id' => 2,//Direccion y administracion de finanzas
                'beneficiosprogramas_id' => $beneficio->id, 
                'fechaEvento' =>Carbon::createFromFormat('d-m-Y',$curso->fecha),
                'usuario_id' => auth()->user()->id,
                'folio' => 'DIF-PAN-'.$beneficio->id.'-'.($contador->id + 1).'-'.date("Y"),
            ]);
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'orev_eventos',
                'registro' => $Nuevo->id. '',
                'campos' => json_encode($request->curso) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'Curso'=>$Nuevo], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $evento=EventosTaller::findOrFail($id);
        $evento->area;
        $evento->beneficio;
        $evento->estatus;
        $evento->participantes;
        $evento->municipio;
        $evento->localidad;
        $evento->tipoEvento;
        return $evento;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        try{
            DB::beginTransaction();
            $curso=(object)$request->curso;
            $beneficio=(object)$curso->beneficio;
            $municipio=(object)$curso->municipio;
            $localidad=(object)$curso->localidad;
            $evento=(object)$curso->tipoEvento;
            
            $cursoActual=EventosTaller::find($id);            
            $cursoActual->fechaEvento=Carbon::createFromFormat('d-m-Y',$curso->fecha);
            $cursoActual->tipo_evento_id=$evento->id;
            $cursoActual->localidad_id=$localidad->id;
            $cursoActual->municipio_id=$municipio->id;
            $cursoActual->calle=strtoupper($curso->calle);
            $cursoActual->numExt=$curso->numeroExterior;
            $cursoActual->numInt=$curso->numeroInterior;
            $cursoActual->colonia=strtoupper($curso->colonia);
            $cursoActual->descripcionEvento=strtoupper($curso->descripcion);
            $cursoActual->codigoPostal=$curso->codigo_postal;      
            $cursoActual->beneficiosprogramas_id=$beneficio->id;            
            $cursoActual->save();
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'orev_eventos',
                'registro' => $cursoActual->id . '',
                'campos' => json_encode($request->curso) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'curso'=>$cursoActual], 200);
        }        
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
       
    }

    ///listado de beneficios
    public function beneficios(){
        return DB::select('call getBeneficiosPanaderia');
    }
    //listado de tipos de eventos
    public function tiposEventos(){
        return CatEvento::get();
		}
		
		public function resportePdf(Request $request){
			return $request;
		}
		public function cursoPdf($id){
			$evento=EventosTaller::findOrFail($id);
        $evento->area;
        $evento->beneficio;
        $evento->estatus;
        $evento->participantes;
        $evento->municipio;
        $evento->localidad;
				$evento->tipoEvento;				

			$pdf=PDF::loadView('panaderia::reportes.cursos.reporteCursos',['curso'=>$evento]);
			 $pdf->setPaper("letter");
					 return $pdf->stream('cursos.pdf');
		}
}
