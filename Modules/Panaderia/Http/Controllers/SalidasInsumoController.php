<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Panaderia\Entities\Produccion;
use Modules\Panaderia\Entities\InsumosPanaderia;
use Barryvdh\DomPDF\Facade as PDF;

class SalidasInsumoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store']]);
        $this->middleware('rolModuleV2:panaderia,ALMACEN,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store']]);
    }
    public function index(Request $request)
    {
				$historialSalidas=Produccion::has('salidasInsumos')
				->orderBy('id','DESC')
				->paginate($request->per_page);
        return $historialSalidas;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $produccion=Produccion::find($request->produccion_id);
            $producto=(object)$request->producto;
            foreach ($producto->ingredientes as $key => $value) {
                $ingrediente=(object)$value;
                foreach ($ingrediente->salidas as $key => $value) {
                    $salida=(object)$value;
                    if($salida->seleccionado=='true'){
                        $insumo=InsumosPanaderia::find($salida->id);
                        $insumo->cantidad=(floatval($insumo->cantidad)- floatval($salida->salida));
                        $insumo->save();
                        $produccion->salidasInsumos()->attach($salida->id,[
                            'producto_id'=>$producto->producto_id,
                            'fecha_salida'=>date("Y-m-d"),
                            'cantidad'=>$salida->salida,
                            'tipo'=>'N',
                            'porcion'=>$producto->cantidadElaborar,
                            'costo_salida'=>$salida->costo_salida,
                            'usuario_id'=>auth()->user()->id
                        ]);
                    }
                }           
            }
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_salidasinsumo',
                'registro' => $produccion->id . '',
                'campos' => json_encode($request->producto) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'producto'=>$producto], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $produccion=Produccion::find($id);                
        $detalles=[];
        $productos=DB::select('call getProductosProduccion('.$id.')');
        foreach ($productos as $key => $value){
        $pro=(object)$value;
        $insumos=DB::select('call getProduccionInsumosProducto('.$id.','.$pro->id.')');
        $total=0;
        
        foreach ($insumos as $key => $value) {
            $value=(object)$value;
            $total+=$value->costo_salida;
        }
        $pro->{"total"}=$total;
        $costoPorProducto=$total/$pro->porcion;
        $pro->{"costoPorProducto"}=$costoPorProducto;
        $detalles[]=(object)[
            "producto"=>$pro,            
            "insumos"=>$insumos            
        ];
        }       
            $pdf = PDF::loadView(
                'panaderia::reportes.almacen.salidasInsumos',
                [                    
                    "produccion"=>$produccion,
                    "salidas"=>$detalles
                ]                
            );
            $pdf->setPaper("letter");
            return $pdf->stream('salidaIsumos.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscarSalida($folio){
        $historialSalidas=Produccion::has('salidasInsumos')->where('folio',$folio)->get();
        return $historialSalidas;
    }
}
