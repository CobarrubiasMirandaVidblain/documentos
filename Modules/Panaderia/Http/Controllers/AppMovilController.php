<?php
namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Producto;
use Modules\Panaderia\Entities\Persona;
use App\Models\Usuario;
use Modules\Panaderia\Entities\Solicitud;
use Modules\Panaderia\Entities\Pedido;
use Modules\Panaderia\Entities\PagoPedido;

use Modules\Panaderia\Emails\PedidoMail;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Auth;
use App\Models\Modulo;
use \Firebase\JWT\JWT;
use Carbon\Carbon;
class AppMovilController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct()
    {           
    $this->middleware('panaderia', ['only' => ['getProductos','editarUsuario' ,'listaPedidos','nuevoPedido','actualizarPassword']]);
    }
    public function getProductos(Request $request){
        $productos=Producto::with('tipo')
        ->whereHas('tipo',function($query)use($request){
            $query->where('nombre','like','%'.$request->tipo.'%');
        })
        ->where('activo','1')
        ->where('solicitado','1')
        ->has('receta')->get();
        return $productos;
    }
    public function login(Request $request){
        
        if(Auth::attempt(['usuario' => $request->usuario, 'password' => $request->password])){
            $usuario = auth()->user();
            $token = [
                'usuario_id' =>$usuario->id,
                'modulo_id' => Modulo::where('nombre', 'like', 'PANADERIA')->first()->id,
                'rol_id' => '1'
            ];
            $encoded = JWT::encode($token, config('app.jwt_panaderia_token'), 'HS256');
            $usuario->token = $encoded;
            $usuario->vida_token = Carbon::now()->addMinutes(40); 
            $usuario->save();
            $persona=Persona::find(auth()->user()->persona_id);
            return ['usuario'=>auth()->user(),'persona'=>$persona,'token'=>$encoded];
        }
        return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
    }    
    public function registro(Request $request){        
        $rcliente=(object)$request->cliente;        
        $rUsuario=(object)$request->usuario;
        try{
            DB::beginTransaction();
            //se crea a la persona            
            $cliente = new Persona();
            $cliente->nombre = strtoupper($rcliente->nombre);
            $cliente->primer_apellido = strtoupper($rcliente->primerapellido);
            $cliente->segundo_apellido = strtoupper($rcliente->segundoapellido);            
            $cliente->numero_celular = $rcliente->celular;                   
            $cliente->save();
           
            $usuario = Usuario::create([
                'usuario' => $rUsuario->usuario,
                'persona_id' => $cliente->id,
                'password' => bcrypt($rUsuario->password),
                'email' => strtolower($rUsuario->email),
                'vida_token' => \Carbon\Carbon::now()->toDateTimeString(),
                'autorizado' => 1,
                'ficha' => 0
            ]);           
            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'Usuario'=>$usuario], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }        
    }
    public function editarUsuario(Request $request){        
        $rcliente=(object)$request->cliente;         
        try{
            DB::beginTransaction();                       
            $cliente =Persona::find($rcliente->persona_id);
            $cliente->nombre = strtoupper($rcliente->nombre);
            $cliente->primer_apellido = strtoupper($rcliente->primer_apellido);
            $cliente->segundo_apellido = strtoupper($rcliente->segundo_apellido);           
            $cliente->numero_celular = $rcliente->numero_celular;            
            $cliente->save();

            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'cliente'=>$cliente], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }        
    }
    public function actualizarPassword(Request $request){       
        $usuario=Usuario::find($request->usuario_id);
        $rUsuario=(object)$request->usuario;        
        try{
            DB::beginTransaction();
            $usuario->password=bcrypt($rUsuario->password);
            $usuario->save();
            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'Usuario'=>$usuario], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }

    }
    public function listaPedidos(Request $request){
        $miUsuario=Usuario::find($request->usuario_id);
        $misPedidos=Persona::with(['pedidos'=> function ($q){
            $q->with('solicitud.estatus','localidad')->orderBy("id","DESC")->limit(10);
        }])->find($miUsuario->persona_id);
        $misPedidos->pedidos;
        return $misPedidos;
    }
    public function detallePedido($id){        
        $pedido=Pedido::findOrFail($id);
        $pedido->solicitud;
        $pedido->cliente;
        $pedido->localidad;
        $pedido->pagos;
        return $pedido; 
    }
    public function detalleProducto(Request $request){
        return Producto::find($request->id);
    }

    public function nuevoPedido(Request $request){        
        try {
        DB::beginTransaction();  
        $usuario=Usuario::find($request->usuario_id);

        $solicitud = new Solicitud();
        $solicitud->folio = '-';
        $solicitud->estatus_id = '9';
        $solicitud->fecha_solicitud = date("Y-m-d");        
        $solicitud->fecha_entrega = $request->fecha_entrega;
        $solicitud->total=$request->total;
        $solicitud->usuario_id=$request->cliente;
        $solicitud->save();   
        $solicitud->folio='PAN-PED-'.$solicitud->id.'-'.date("Y");
        $solicitud->save();  

        $rpedido = (object)$request->pedido;
        $pedido = new Pedido();           
        $pedido->solicitud_id=$solicitud->id;
        $pedido->cliente_id=$request->cliente; 
        $pedido->pagado='1';  
        $pedido->calle=strtoupper($rpedido->calle);
        $pedido->numero_exterior=strtoupper($rpedido->numext);
        $pedido->numero_interior=strtoupper($rpedido->numint);
        $pedido->colonia=strtoupper($rpedido->colonia);
        $pedido->localidad_id=$request->localidad;
        $pedido->referencia_domicilio=strtoupper($rpedido->referencia);           
        $pedido->usuario_id=$request->usuario_id;
        $pedido->save();    
        
        $detalles = $request->lista;
        
        foreach ($detalles as $detalle) {
            $producto=(object)$detalle;
            $solicitud->detalles()->attach($producto->id,[
                'cantidad'=>$producto->cantidad,
                'precio_unitario'=>$producto->precio,
                'subtotal'=>$producto->subtotal,
                'usuario_id'=>'1']); 
        } 
        
        $pago = new PagoPedido();
        $pago->pedido_id = $pedido->id;
        $pago->fecha = date("Y-m-d"); 
        $pago->pago = $request->total;
        $pago->pago_id=$request->pagoPayPal_id;
        $pago->save();
        DB::commit();
        Mail::to($usuario->email)->queue(new PedidoMail($usuario->persona,$pago,$solicitud,$detalles));
        return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$solicitud->folio], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            \Log::debug($e);
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        } 
    }
    

}
