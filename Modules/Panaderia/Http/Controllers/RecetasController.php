<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Receta;

class RecetasController extends Controller
{
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','update','destroy']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO,PRODUCCION',
        ['only' => ['store','update','destroy']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $recetas=Receta::with('producto')->get();        
        return $recetas;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $receta=(object)$request->receta;
        
        $nuevaReceta=new Receta();
        $nuevaReceta->nombre=strtoupper($receta->nombre);
        $nuevaReceta->porciones=$receta->porciones;
        $nuevaReceta->producto_id=((object)$receta->producto)->id;
        $nuevaReceta->usuario_id='1';
        $nuevaReceta->save();

        $ingredientes=$request->ingredientes;

        foreach ($ingredientes as $key => $value) {
            $obj=(object)$value;
            $insumo=(object)$obj->insumo;           

            $nuevaReceta->ingredientes()->attach($insumo->proID,[
                'cantidad'=>$obj->cantidad,'presentacion_id'=>$insumo->preID,'usuario_id'=>'1'
            ]);
            
        }
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_recetas',
                'registro' => $nuevaReceta->id . '',
                'campos' => json_encode($request->receta) . '',
                'metodo' => $request->method()    
            ]);
      
        return $request;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $receta=Receta::find($id);
        $receta->producto;
        $receta->ingredientes;
        $receta->presentacion;

        return $receta;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $receta=(object)$request->receta;
        
        $nuevaReceta=Receta::find($id);
        $nuevaReceta->ingredientes()->detach();
        $nuevaReceta->nombre=strtoupper($receta->nombre);
        $nuevaReceta->porciones=$receta->porciones;
        $nuevaReceta->producto_id=((object)$receta->producto)->id;
        $nuevaReceta->usuario_id='1';
        $nuevaReceta->save();

        $ingredientes=$request->ingredientes;

        foreach ($ingredientes as $key => $value) {
            $obj=(object)$value;
            $insumo=(object)$obj->insumo;            

            $nuevaReceta->ingredientes()->attach($insumo->proID,[
                'cantidad'=>$obj->cantidad,'presentacion_id'=>$insumo->preID,'usuario_id'=>'1'
            ]);
            
        }
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_recetas',
                'registro' => $nuevaReceta->id . '',
                'campos' => json_encode($request->receta) . '',
                'metodo' => $request->method()    
            ]);

        return $request;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $receta=Receta::find($id);
        $receta->ingredientes()->detach();   
        $receta->delete();     
    }
}
