<?php

namespace Modules\Panaderia\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Produccionprogramada;
use Modules\Panaderia\Entities\Producto;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProduccionesProgramadasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {  
       $programadas = Produccionprogramada::with('programados','estatus')->paginate($request->per_page);  
        return [                        
            'programadas' => $programadas,
        ];  
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $produccion=new Produccionprogramada();        
        $produccion->estatus_id='9'; 
        $produccion->fecha_programada=Carbon::createFromFormat('d-m-Y',$request->fecha);           
        $produccion->usuario_id=auth()->user()->id;
        $produccion->save();    
        
        $productos = $request->lista;
        
        foreach ($productos as $p) {
            $producto=(object)$p;
            $produccion->programados()->attach($producto->id,[
                'cantidad'=>$producto->cantidad,                
                'usuario_id'=>'1']); 
        } 
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $produccion = Produccionprogramada::findOrfail($id);
        $produccion->programados;
        $produccion->estatus;
        return $produccion;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $produccion =Produccionprogramada::findOrfail($id); 
        $produccion->estatus_id = $request->estatus;        
        $produccion->save();           
        return;
    }

    public function editar(Request $request,$id){
        $produccion=Produccionprogramada::findOrfail($id);     
        $produccion->fecha_programada=Carbon::createFromFormat('d-m-Y',$request->fecha);           
        $produccion->usuario_id=auth()->user()->id;
        $produccion->save();    
        
        $productos = $request->lista;
        
        foreach ($productos as $p) {
            $producto=(object)$p;
            $produccion->programados()->attach($producto->id,[
                'cantidad'=>$producto->cantidad,                
                'usuario_id'=>auth()->user()->id]); 
        } 
    }
    

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
