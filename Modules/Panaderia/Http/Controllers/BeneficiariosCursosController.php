<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Persona;
use App\Models\EventosTaller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class BeneficiariosCursosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','update' ,'show','destroy']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store','update', 'destroy']]);
    }
    public function index()
    {
        return view('panaderia::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $participante=(object)$request->participante;
            $curso=EventosTaller::find($request->id);
            if($participante->id!=null){
                $curso->participantes()->attach($participante->id);
                DB::commit();
                $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
                $usuario->bitacora($request, [
                    'tabla' => 'personas',
                    'registro' => $participante->id.'',
                    'campos' => json_encode($request->participante). '',
                    'metodo' => $request->method()    
                ]);
                $usuario->bitacora($request, [
                    'tabla' => '`orev_eventos_participantes',
                    'registro' => $participante->id.'',
                    'campos' => 'participante_id'. '',
                    'metodo' => $request->method()    
                ]);
                return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'curso'=>$curso,'participante'=>$participante], 200);
            }            
            else{
                $persona=new Persona();
                $persona->nombre=strtoupper($participante->nombre);
                $persona->primer_apellido=strtoupper($participante->apPaterno);
                $persona->segundo_apellido=strtoupper($participante->apMaterno);
                $persona->fecha_nacimiento=Carbon::createFromFormat('d-m-Y',$participante->fechaNacimiento);
                $persona->numero_local=$participante->telefono;
                $persona->genero=$participante->genero;
                $persona->email=$participante->correo;
                $persona->save();
                $curso->participantes()->attach($persona->id);
                DB::commit();
                $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
                $usuario->bitacora($request, [
                    'tabla' => 'personas',
                    'registro' => $persona->id.'',
                    'campos' => json_encode($request->participante). '',
                    'metodo' => $request->method()    
                ]);
                $usuario->bitacora($request, [
                    'tabla' => '`orev_eventos_participantes',
                    'registro' => $persona->id.'',
                    'campos' => 'participante_id'. '',
                    'metodo' => $request->method()    
                ]);

                return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'curso'=>$curso,'participante'=>$persona], 200);
            }
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('panaderia::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {        
        try{
            DB::beginTransaction();
            $participante=(object)$request->participante;
            $persona =Persona::find($id);        
            $persona->nombre=strtoupper($participante->nombre);
            $persona->primer_apellido=strtoupper($participante->apPaterno);
            $persona->segundo_apellido=strtoupper($participante->apMaterno);
            $persona->fecha_nacimiento=Carbon::createFromFormat('d-m-Y',$participante->fechaNacimiento);
            $persona->numero_local=$participante->telefono;
            $persona->genero=$participante->genero;
            $persona->email=$participante->correo;
            $persona->save();
            DB::commit();    
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'personas',
                'registro' => $persona->id . '',
                'campos' => json_encode($request->participante) . '',
                'metodo' => $request->method()    
            ]);        
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'participante'=>$persona], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id,$curso)
    {
        try{
            DB::beginTransaction();
            $curso=EventosTaller::find($curso);
            $curso->participantes()->detach($id);
            DB::commit();            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'curso'=>$curso,'participante'=>$id], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }        
    }

    public function buscarPersona(Request $request){
        $columns = ['nombre','primer_apellido','segundo_apellido','curp', 'fecha_nacimiento'];
        $term = $request->curp;
        $words_search = explode(" ",$term);
        $personaOption =Persona::where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                        foreach ($columns as $column) {
                            $query->orWhere($column,'like',"%$word%");
                        }
                    });
                }
            });
            $personaOption = $personaOption->paginate(6);   
        return $personaOption;
    }
}
