<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Persona;
use Illuminate\Support\Facades\DB;
class ClientesController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','update' ,'show']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO,PRODUCCION,VENDEDOR',
        ['only' => ['store','update','show']]);
    } 
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $clientes = Persona::with('localidad')->has('pedidos')->get();
        return[
            'clientes'=>$clientes
        ]; 
       
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         try {
            DB::beginTransaction();  
            $rcliente = (object)$request->cliente;
            $cliente = new Persona();
            $cliente->nombre = strtoupper($rcliente->nombre);
            $cliente->primer_apellido = strtoupper($rcliente->primerapellido);
            $cliente->segundo_apellido = strtoupper($rcliente->segundoapellido);
            /* $cliente->calle = strtoupper($rcliente->calle);
            $cliente->numero_exterior = $rcliente->numexterior;
            $cliente->numero_interior = $rcliente->numinterior;
            $cliente->colonia = strtoupper($rcliente->colonia);
            $cliente->localidad_id = $request->localidad; */
            $cliente->numero_celular = $rcliente->celular;
            $cliente->numero_local = $rcliente->telefono;
            //$cliente->referencia_domicilio =strtoupper($rcliente->referencia);
            $cliente->usuario_id=auth()->user()->id;
            $cliente->save();
             DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'personas',
                'registro' => $cliente->id . '',
                'campos' => json_encode($request->cliente) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'cliente'=>$cliente], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }  
       
    }

    public function show($id)
    {
        $cliente = Persona::findOrFail($id);
       // $cliente->localidad;      
        return $cliente;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
         try {
            DB::beginTransaction(); 
            $cliente =Persona::findOrfail($id);      
            $cliente->update($request->all());
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'personas',
                'registro' => $cliente->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'cliente'=>$cliente->id], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        } 
    
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
