<?php

namespace Modules\Panaderia\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
//use Modules\Panaderia\Entities\Solicitud;
use Modules\Panaderia\Entities\Donativo;
use Modules\Panaderia\Entities\Detallesolicitud;
use Modules\Panaderia\Entities\SolicitudGeneral;
use Modules\Panaderia\Entities\Beneficioprograma;
use Modules\Panaderia\Entities\BeneficioprogramaSolicitud;
use App\Models\Solicitud;
use App\Models\Programa;
use App\Models\ProgramasSolicitud;
use Illuminate\Support\Facades\DB;
use App\Models\Tiposrecepcion;
use App\Models\Tiposremitente;
use App\Models\StatusSolicitud;
use App\Models\Statusproceso;



class SolicitudesController extends Controller
{
   /*  public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['show','update']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR',
        ['only' => ['update','show']]);
    } */
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {       
      
       /*  $beneficios = BeneficioprogramaSolicitud::with('beneficios')
        ->whereHas('beneficios',function($q){
            $q->where('anio_programa_id','=','424');})->get();      */  
        $peticiones = SolicitudGeneral::with('beneficiossolicitados','tiposremitente','tiposrecepcion','solicitudesmunicipios','solicitudespersonales','solicitudesregiones','solicitudesinstituciones')//->paginate($request->per_page);     
        ->whereHas('beneficiossolicitados.beneficios',function($q){
             $q->where('anio_programa_id','=','424');})->paginate($request->per_page);                                           
        return [                        
            'peticiones' =>$peticiones,
           // 'beneficios' =>$beneficios,
        ];          
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
       
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
		  $resultado=BeneficioprogramaSolicitud::where('solicitud_id',$id)->get();         
      return $resultado;       
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        //return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {       
        $beneficio =BeneficioprogramaSolicitud::findOrfail($id);   
        $beneficio->status()->attach($beneficio->id,[
            'statusproceso_id'=>$request->estatus,
            'usuario_id'=>'1']);           
        $beneficio->save();    
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscarsolicitud($folio){
        $nfolio=str_replace('-','/',$folio);        
        $solicitud=SolicitudGeneral::with('beneficiossolicitados','tiposremitente','tiposrecepcion','solicitudesmunicipios','solicitudespersonales','solicitudesregiones','solicitudesinstituciones')        
        ->whereHas('beneficiossolicitados.beneficios',function($q){
            $q->where('anio_programa_id','=','424');})
        ->where('folio',$nfolio)->first();
        return $solicitud;   
    } 

    public function getBeneficios($id){
        $beneficios=BeneficioprogramaSolicitud::where('solicitud_id',$id);
        //$beneficios->beneficios;
        return $beneficios;
        /* $beneficios = BeneficioprogramaSolicitud::where('solicitud_id',$id);
        with('beneficios')
        ->whereHas('beneficios',function($q){
            $q->where('anio_programa_id','=','424');})->get();  */
    }
   /*public function actualizarEstatus(Request $request,$id){
        $beneficio =BeneficioprogramaSolicitud::findOrfail($id);   
        $beneficio->status()->attach($beneficio->id,[
            'statusproceso_id'=>$request->estatus,
            'usuario_id'=>'1']);           
        $beneficio->save();    

    } */
}
