<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Donativo;
use Modules\Panaderia\Entities\Solicitud;
use Modules\Panaderia\Entities\Status;
use Modules\Panaderia\Entities\Producto;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class DonativosController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','show','update']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO,PRODUCCION',
        ['only' => ['store','update','show']]);
    } 
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {    
       $solicitudes = Solicitud::count();                           
       $donativosPendientes = Solicitud::has('donativos')->where('estatus_id','9')->count();                           
       $donativos = Donativo::with('solicitud','solicitudGeneral')->paginate($request->per_page);                                                     
        return [                        
            'donativos' => $donativos, 
            'donativosPendientes' => $donativosPendientes,  
            'solicitudes' => $solicitudes                                                      
        ];            
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
      try {
        DB::beginTransaction();       
        $rsolicitud = (object)$request->solicitudes;
        $solicitud = new Solicitud();
        $solicitud->folio = '-';
        $solicitud->estatus_id = '9';
        $solicitud->fecha_solicitud = $rsolicitud->fechaoficio;        
        $solicitud->fecha_entrega = Carbon::createFromFormat('d-m-Y',$request->fecha_entrega);
        $solicitud->total=$request->total;
        $solicitud->usuario_id=auth()->user()->id;;
        $solicitud->save();   
        $solicitud->folio='PAN-DON-'.$solicitud->id.'-'.date("Y");
        $solicitud->save();        
                        
        $donativo=new Donativo();
        $donativo->solicitud_id=$solicitud->id;
        $donativo->solicitudgeneral_id=$rsolicitud->id; 
        $donativo->folio=$rsolicitud->folio;
        $donativo->justificacion=$rsolicitud->asunto;       
        $donativo->usuario_id=auth()->user()->id;;
        $donativo->save();    
        
        $detalles = $request->lista;
        
        foreach ($detalles as $detalle) {
            $producto=(object)$detalle;
            $solicitud->detalles()->attach($producto->id,[
                'cantidad'=>$producto->cantidad,
                'precio_unitario'=>$producto->precio,
                'subtotal'=>$producto->subtotal,
                'usuario_id'=>auth()->user()->id]); 
        }    
       $remitente = $rsolicitud->remitente_id;
        if($remitente==4){
            $donativo->donativoBeneficiarios()->attach($remitente,['usuario_id'=>auth()->user()->id]);
        }
        if ($remitente==3) {
            $donativo->donativoDependencias()->attach($remitente,['usuario_id'=>auth()->user()->id]);
        }
        if($remitente==2){
            $donativo->donativoMunicipios()->attach($remitente,['usuario_id'=>auth()->user()->id]);        
        }
        if($remitente==1){            
            $donativo->donativoRegiones()->attach($remitente,['usuario_id'=>auth()->user()->id]);
        }        
        DB::commit();
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_solicitudes',
                'registro' => $solicitud->id. '',
                'campos' => json_encode($request->solicitudes) . '',
                'metodo' => $request->method()    
            ]);
        return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'id'=>$donativo->id], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
         
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $donativo=Donativo::with('solicitud','solicitudGeneral','donativoBeneficiarios','donativoMunicipios','donativoRegiones','donativoDependencias')->findOrFail($id);
        //dd($donativo->fecha_entrega);
       
        /* $donativo->donativos;
        $donativo->detalles;
        $donativo->donativoBeneficiarios;
        $donativo->donativoMunicipios;
        $donativo->donativoRegiones;
        $donativo->donativoDependencias; */
        return $donativo;   
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }
    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        try{
            DB::beginTransaction(); 
            $donativo =Solicitud::findOrfail($id); 
            $donativo->estatus_id = $request->estatus; 
            $donativo->save();  
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_solicitudes',
                'registro' => $donativo->id. '',
                'campos' => json_encode($request->estatus) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        } 
        
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function mostrarpdf($id)
    {
        $solicitud=Solicitud::find($id);                    
        $productos = $solicitud->detalles;
            $pdf = PDF::loadView(
                'panaderia::reportes.donativos.productossolicitados',
                [                    
                    "solicitud"=>$solicitud,
                    "productos"=>$productos,
                  
                ]                
            );
            $pdf->setPaper("letter");
            return $pdf->stream('productossolicitados.pdf');
    }
    public function entregar(Request $request,$id){
        $solicitud =Solicitud::findOrfail($id); 
        $solicitud->estatus_id = $request->estatus; 
        $lista = $solicitud->detalles;
        
        foreach ($lista as $item) {
            $producto=(object)$item;
            $elaborado=Producto::find($producto->id);           
            $elaborado->stock = ($elaborado->stock-$producto->pivot->cantidad);
            $elaborado->save();

        } 
        $solicitud->save();  
        return;
    }
    public function buscarDonativo($folio){
        $solicitud=Solicitud::where('folio',$folio)->first();
        $pedido = Donativo::with('solicitud','solicitudGeneral')->where('solicitud_id',$solicitud->id)->get();
        
        
        return $pedido;
    }

    public function reportePdf(Request $request)
    {
        
        $pdf = PDF::loadView(
          'panaderia::reportes.donativos.reportePdf',
          [
            'lineal' => $request->lineal,
            'pastel' => $request->pastel, 
            'donativos' => $request->donativos, 
            'costo' => $request->costo,
            'ganancia' => $request->ganancia, 
            'periodo' => $request->periodo
          ]
        );

        
        return $pdf->stream();
    }
 

}
