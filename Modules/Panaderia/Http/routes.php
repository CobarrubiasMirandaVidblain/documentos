<?php

use Modules\Panaderia\Http\Controllers\LocalidadesController;

Route::group(['prefix' => 'panaderia', 'namespace' => 'Modules\Panaderia\Http\Controllers'], function()
{
    Route::get('/', 'PanaderiaController@index')->middleware('web');      
    //Rutas de tipo Resource
    Route::group(['middleware' => 'web','prefix'=>'api'],function(){
        Route::resource('/productos','ProductosController');
        Route::resource('/equipos','EquiposController');  
        Route::resource('/donativos','SolicitudesController');
        Route::resource('/donativo/seguimiento','DonativosController');
        Route::resource('/produccion','ProduccionesProgramadasController');
        Route::resource('/producciones','ProduccionesController');
        Route::resource('/insumos','InsumosController'); 
        Route::resource('/recetas','RecetasController');
        Route::resource('/cursos/beneficiarios','BeneficiariosCursosController');
        Route::resource('/cursos','CursosController');
        Route::resource('/pedidos','PedidosController');
        Route::resource('/clientes','ClientesController');
        Route::resource('/ventas','VentasController');
        Route::resource('/salidas/insumo','SalidasInsumoController');
        Route::delete('/cursos/beneficiarios/delete/persona/{persona}/{curso}','BeneficiariosCursosController@destroy');    
    });    
    //Rutas especificas para acciones Unicas
    Route::group(['middleware' => 'web'],function(){
        Route::get('/permissions', 'PanaderiaController@permissions')->middleware('panaderia');
        Route::post('/produccionnormal','ProduccionesController@produccionnormal');   
        Route::get('/insumo/existe/{folio}','InsumosController@existeOD');
        Route::get('/insumo/listado','InsumosController@listaInsumos');
        Route::get('/insumos/disponibles/{id}','InsumosController@buscarInsumoExistencia');
        Route::get('/cursos/beneficiarios/buscar/persona','BeneficiariosCursosController@buscarPersona');    
        Route::get('/beneficios/listado','CursosController@beneficios');
        Route::get('/eventos/listado','CursosController@tiposEventos');    
        Route::get('/ventas/buscarventa/{folio}','VentasController@buscarVenta');
        Route::get('/producciones/salidasPendientes','ProduccionesController@salidasPendientes');
        Route::get('/producciones/productosProduccion/{id}','ProduccionesController@productosProduccion');
        Route::get('/producciones/productosProduccionProgramada/{id}','ProduccionesController@productosProduccionProgramada');
        Route::get('/producciones/productosProduccionSolicitud/{id}','ProduccionesController@productosProduccionSolicitud');
        Route::put('/donativo/actualizar/{id}','SolicitudesController@actualizarEstatus');
        Route::get('/todos/productos/','ProductosController@getTodos');
        Route::get('/pedidos/buscar','PedidosController@buscarCliente'); 
        Route::get('/pedidos/buscarpedido/{folio}','PedidosController@buscarPedido');
        Route::get('/donativos/buscardonativo/{folio}','DonativosController@buscardonativo');
        Route::get('/producciones/buscarproduccionsolicitud/{folio}','ProduccionesController@buscarProduccionsolicitud');
        Route::get('/producciones/buscarproduccionnormal/{folio}','ProduccionesController@buscarProduccionnormal');   
        Route::get('/producciones/verproduccion/{id}','ProduccionesController@getProduccion');
        Route::get('/solicitudes/buscar/{folio}','SolicitudesController@buscarsolicitud');
        Route::get('/donativos/productossolicitados/{id}','DonativosController@mostrarpdf');
        Route::get('/pedidos/productossolicitados/{id}','PedidosController@mostrarpdf');
        Route::get('/producciones/solicitud/{id}','ProduccionesController@showSolicitud');
        Route::get('/venta/ticket/{id}','VentasController@ticket');
        Route::put('/produccion/finalizarproduccionnormal/{id}','ProduccionesController@finalizarProduccionNormal');
        Route::put('/produccion/finalizarproduccionsolicitud/{id}','ProduccionesController@finalizarProduccionSolicitud');
        Route::put('/solicitudes/entregar/{id}','DonativosController@entregar');
        Route::get('/donativos/beneficios/{id}','SolicitudesController@getBeneficios');
        Route::get('/pedidos/listadepedidos/{id}','PedidosController@listaPedidos');
        Route::post('/pedidos/cancelaciones','PedidosController@cancelar');
        Route::get('/pedidos/ticket/{id}','PedidosController@ticket');

        Route::get('/salidas/insumo/{folio}','SalidasInsumoController@buscarSalida');
        Route::get('/productos/sinReceta','ProductosController@sinReceta');
        Route::get('/productos/ventas','ProductosController@productosVentas');
        Route::get('/localidades/{id}','LocalidadesController@index');
        Route::get('/municipios','LocalidadesController@municipios');
    }); 
    ///Rutas para reportes
    Route::group(['middleware' => 'web','prefix'=>'reportes'],function(){
        ///panaderia/reportes/ventas
        Route::group(['prefix'=>'ventas'],function(){
            Route::get('/mes/{mes}','ReportesController@ventasMes');
            Route::get('/masVendidosMes/{mes}','ReportesController@masVendidosMes');
            Route::get('/delDia/{fecha}','ReportesController@ventasDelDia');
            Route::post('/','ReportesController@reportesVentas');            
            Route::get('/{fechaInicio}/{fechaFin}','ReportesController@ventas');
            Route::post('/pdf','VentasController@pdfReporte');
        });
        Route::group(['prefix'=>'pedidos'],function(){           
            Route::post('/','ReportesController@reportesPedidos');  
            Route::post('/pdf','PedidosController@reportePdf');                        
                      
        });
        Route::group(['prefix'=>'donativos'],function(){           
            Route::post('/','ReportesController@reportesDonativos');  
            Route::post('/pdf','DonativosController@reportePdf');                        
                      
				});  
				Route::group(['prefix'=>'general'],function(){           
					Route::post('/','ReportesController@reporteGeneral');
				  Route::post('/pdf','ReportesController@reportePdf');                   										
			 });        
        Route::get('/anios','ReportesController@getAnios');				
		
				Route::group(['prefix'=>'cursos'],function(){           
					Route::post('/','ReportesController@reportesCursos');
					Route::post('/pdf','CursosController@reportePdf');                        
					Route::get('/cursoPdf/{id}','CursosController@cursoPdf');
				});

				Route::group(['prefix'=>'insumos'],function(){           				
					Route::get('/reportePdf','InsumosController@reportePdf');
				});								        
        Route::get('/anios','ReportesController@getAnios');
    });
    //Rutas para la aplicacion Móvil
    Route::group(['prefix'=>'pedidosMovil'],function(){                
        Route::post('productosPanaderia','AppMovilController@getProductos');
        Route::get('detalleProducto','AppMovilController@detalleProducto');
        Route::post('login','AppMovilController@login');        
        Route::post('registerUser','AppMovilController@registro');
        Route::put('editUser','AppMovilController@editarUsuario');
        Route::put('updatePassword','AppMovilController@actualizarPassword');
        Route::post('misPedidos','AppMovilController@listaPedidos');
        Route::post('nuevoPedido','AppMovilController@nuevoPedido');
        Route::get('detallePedido/{id}','AppMovilController@detallePedido');
    });
});