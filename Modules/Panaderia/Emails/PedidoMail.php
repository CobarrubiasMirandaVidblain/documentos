<?php

namespace Modules\Panaderia\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PedidoMail extends Mailable
{
    use Queueable, SerializesModels;
    public $persona,$pago,$solicitud,$detalles;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($persona,$pago,$solicitud,$detalles)
    {
        $this->pago=$pago;
        $this->solicitud=$solicitud;
        $this->detalles=$detalles;
        $this->persona=(object)$persona;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('panaderia::emails.pedido');
    }
}
