import Vuex from 'vuex';
import Vue from 'vue';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // empleado: null
    roles: [],
    loaded: false,
  },
  mutations: {
    // setEmpleado: function (state, empleado) {
    //   state.empleado = empleado;
    // }
    setRoles(state, roles){
      Vue.set(state, 'roles', roles);
      // state.roles = roles;
      if(roles.length > 0)  Vue.set(state, 'loaded', true);
    }
  },
  getters: {    
    loadedRoles: state => state.loaded,
    isRoleInRoles: (state) => (acceptRoles) => {
      // return state.roles.filter(role => role.name === acceptRoles);
      return state.roles.filter(role => acceptRoles.includes(role.name));
    },
    has: (state, getters) => (acceptRoles) => {
      return getters.isRoleInRoles(acceptRoles).length > 0;
    }
  },
  actions: {
    fetchRoles(context) {
      axios.defaults.headers.common["Pan-Token"] = sessionStorage.getItem("pan_token");
      return axios.get('/panaderia/permissions').then(response => response.data).then(roles => context.commit('setRoles', roles));
      // context.commit('setRoles', JSON.parse(sessionStorage.getItem('roles')));
      // sessionStorage.setItem('roles', null);
    }
  }
});