import Vue from 'vue'
import VueRouter from 'vue-router' 
import VueTables from 'vue-tables-2'
import ToggleButton from 'vue-js-toggle-button'
import vSelect from 'vue-select'
import Datepicker from 'vue2-datepicker';
import VeeValidate, { Validator } from 'vee-validate';
import es from 'vee-validate/dist/locale/es';
import store from './store'
/////////////
import Home from './components/Home.vue';
/////////////Almacen
import Equipos from './components/almacen/Equipos.vue';
import Productos from './components/productos/Productos.vue';
import Entradas from './components/almacen/Entradas.vue';
import Salidas from './components/almacen/Salidas.vue';
import Existencias from './components/almacen/Existencias.vue';
import HistorialSalidas from './components/almacen/HistorialSalidas.vue'
/////Donativos
import Donativos from './components/donativos/Donativos.vue';
import Aceptados from './components/donativos/Aceptados.vue';
////////////Pedidos
import Pedidos from './components/pedidos/Pedidos.vue';
import Lista from './components/pedidos/Lista.vue';
import Pagos from './components/pedidos/Pagos.vue';
import Cancelaciones from './components/pedidos/Cancelaciones.vue';
////////Clientes
import Clientes from './components/Clientes/Clientes.vue';
//////////////Ventas
import Ventas from './components/ventas/Ventas.vue';
import NuevaVenta from './components/ventas/NuevaVenta.vue';
////Recetas
import NuevaReceta from './components/recetas/NuevaReceta.vue';
import ListaRecetas from './components/recetas/ListaRecetas.vue';
////Producción
import Programadas from './components/produccion/Programadas.vue'
import Producciones from './components/produccion/Producciones.vue'

//cursos
import listaCursos from './components/cursos/listadoCursos.vue'
///Styles
import 'vue-select/dist/vue-select.css';
///error 404
import error404 from './components/error/error404.vue'
//error 403
import NoAutorizado from './components/error/error403.vue'
//reportes
import reportes from './components/reportes/reportes.vue';

///
Vue.use(ToggleButton)
Vue.use(VueRouter)
Vue.use(Datepicker)
Vue.use(VueTables.ClientTable,{ theme: 'bootstrap4',template: 'default' })
Vue.component('v-select', vSelect)
Vue.use(VeeValidate)
Validator.localize('es', es);


const router = new VueRouter({
  //mode: 'history',
  routes: [
    {
      path:'/',
      name:'home',
			component: Home,
			props:{
        rol:store.state,
      },
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/')
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','VENDEDOR','ALMACEN','PRODUCCION'])){
          return next('/403');
        }
        next();
      }
    },   
    {
      path: '/productos/listado',
      name: 'productos',
      component: Productos,
      props:{
        rol:store.state,
      },
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','VENDEDOR'])){
          return next('/403');
        }
        next();
      }       
    },
    {
      path: '/inventario/equipos',
      name: 'equipos',
      component: Equipos,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','ALMACEN'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/donativos',
      name: 'donativos',
      component: Donativos,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO'])){
          return next('/403');
        }
        next();
      }
    },
    { 
      path: '/almacen/entradas',
      name: 'entradas',
      component: Entradas,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','ALMACEN'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/almacen/existencias',
      name: 'existencias',
      component: Existencias,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','ALMACEN'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path:'/almacen/salidas',
      name:'salidas',
      component:Salidas,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','ALMACEN'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/almacen/historialSalidas',
      name:'HistorialSalidas',
      component: HistorialSalidas,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','ALMACEN'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/donativos/aceptados',
      name: 'aceptados',
      component: Aceptados,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','PRODUCCION'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/ventas/listado',
      name: 'Ventas',
      component: Ventas,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','VENDEDOR'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/ventas/nuevaVenta',
      name: 'nuevaVenta',
      component: NuevaVenta,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','VENDEDOR'])){
          return next('/403');
        }
        next();
      }
    },     
    {
      path: '/recetas/nuevaReceta/:id?',
      name:'nuevaReceta',
      component: NuevaReceta,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','PRODUCCION'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/recetas/listado',
      name:'listaRecetas',
      component: ListaRecetas,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','PRODUCCION'])){
          return next('/403');
        }
        next();
      }
    },    
    {
      path: '/produccion/programacion',
      name: 'programacion',
      component: Programadas,    
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','PRODUCCION'])){
          return next('/403');
        }
        next();
      }  
    },
    {
      path: '/produccion/producciones',
      name: 'producciones',
      component: Producciones,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','PRODUCCION'])){
          return next('/403');
        }
        next();
      }
    },
    {      
      path: '/cursos/listado',
      name:'cursosPanaderia',
      component: listaCursos,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO'])){
          return next('/403');
        }
        next();
      }
    },
    {      
      path: '/pedidos',
      name:'pedidos',
			component: Pedidos,
			props:{
        rol:store.state,
      },
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','VENDEDOR'])){
          return next('/403');
        }
        next();
      }
    },
    {      
      path: '/pedidos/pagos/:id?',
      name:'pagos',
      component: Pagos,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','VENDEDOR'])){
          return next('/403');
        }
        next();
      }
    },
    {      
      path: '/pedidos/cancelaciones/:id?',
      name:'cancelaciones',
      component: Cancelaciones,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO'])){
          return next('/403');
        }
        next();
      }
    },
    {      
      path: '/pedidos/listado',
      name:'listado',
      component: Lista,
      props:{
        rol:store.state,
      },
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO','PRODUCCION','VENDEDOR'])){
          return next('/403');
        }        
        next();
      }
    },
    {      
      path: '/clientes',
      name:'clientes',
      component: Clientes,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO'])){
          return next('/403');
        }
        next();
      }
    },    
    {
      path:'/reportes',
      name:'reportes',
      component:reportes,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','ENCARGADO'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path:'/403',
      name:'error403',
      component:NoAutorizado
    },
    {
      path: '*',
      name:'error404',
      component: error404,
    },
  ],
});

const app = new Vue({
  el: '#app',
  store,
  // components: {  },
  router,  
  created(){
    store.dispatch('fetchRoles');

  },   
});