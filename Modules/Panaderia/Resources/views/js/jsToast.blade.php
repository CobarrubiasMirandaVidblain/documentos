<script>
const toast = Swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  background: '#71DBD4'
});
const toastW = Swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,  
  background: '#F5CB08',
});
const toastE = Swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  background: '#F58EAA'
});
const toastI = Swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  background: '#AFEEEE'
});

const toastConfirm=Swal.mixin({
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
});
</script>