<script>
  function formatPesos(n) {
      return new Intl.NumberFormat("en-ES").format(n);
    }
  function formatFecha(date){
      return moment(date).format('DD/MM/YYYY');
    }
</script>