{{-- <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Gráfica</title>
</head>
<body>
     
  <table width="100%">
        <tr>
        <td align="bottom"><img src={{asset('images/logo_header.png')}} alt="" width="150"/></td>
            <td align="right">               
                <h3>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</h3>
                <pre>
                    TALLER DE PANADERIA DE DIF
                    DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS
                    OAXACA DE JUÁREZ A {{$fecha}}
                </pre>
            </td>
        </tr>
      </table>       
   <div>
    <table width="100%" margin-top:10%>
      <td align="center"><img src=" style="  width: 500px; height: 300px;"></td>
    </table>     
  </div>
  <div>
      <table width="100%" margin-top:10%>
        <td align="center"><img src="" style="  width: 500px; height: 300px;"></td>
      </table>     
    </div>
</body>
</html>
<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>
 --}}
 <!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Reporte de Pedidos</title>  
  <style type="text/css">
  p{
    font-size: 12px;
    text-transform: uppercase !important;
  }
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>
</head>

<body>  
      <table width="100%">
          <tr>
          <td align="bottom"><img src={{asset('images/logo_header.png')}} alt="" width="150"/></td>
              <td align="right">
                  <?php
        setlocale (LC_TIME, "es_MX");
        $fecha= strtoupper(strftime("%d de %B de %Y"));
        ?>
                  <h3>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</h3>
                  <pre>
                      TALLER DE PANADERIA
                      DIRECCIÓN DE ADMINISTRACION Y FINANZAS
                      REPORTE DE PEDIDOS
                      OAXACA DE JUÁREZ A {{$fecha}}
                  </pre>
              </td>
          </tr>
      
        </table>
  <br>
  <table width="100%">
    <tbody>
      <tr>
        <td>
          <p>En la siguiente gráfica se muestra el total de pedidos durante el periodo de {{$periodo}}:</p>
        </td>
      </tr>
      <tr>
        <td align="center">
          <img src="<?php echo $lineal;?>" style="  width: 500px; height: 250px;">
        </td>        
      </tr>     
    </tbody>
  </table>
  <br>
  <table width="100%">
      <tbody>  
        <tr>
          <td>
            <p>En la siguiente gráfica se muestra el total de pedidos, costos de producción y ganancias del periodo: {{$periodo}}</p>
          </td>
        </tr>     
        <tr align="center">
            <td>
                <img src="<?php echo $pastel;?>" style="  width: 500px; height: 280px;">
            </td>
        </tr>
      </tbody>
    </table>
  <br>
  <table width="100%" style="border-collapse:collapse;border-color:#ddd;" border="1" cellpadding="0" cellspacing="1" bordercolor="#000000">
    <thead>
      <tr>
        <th>Total de pedidos</th>
        <th>Costo de Producción</th>
        <th>Ganancias</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <td>${{$pedidos}}</td>
        <td>${{$costo}}</td>
        <td>${{$ganancia}}</td>
      </tr>
    </tbody>
  </table>
  
  <footer style="bottom:40px; position:fixed;">
    <img class="img-fluid mt-2 footer" style="margin-bottom:50px ; width: 100%;" src={{asset('images/logo_footer.png')}}>
  </footer>
</body>

</html>