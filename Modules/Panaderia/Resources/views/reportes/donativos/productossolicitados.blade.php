
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Productos solicitados</title>
    <link rel="stylesheet" href="style.css" media="all" />
  </head>
  <body>
      <?php
      setlocale (LC_TIME, "es_MX");
      $meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");
      $fecha=date('d')." DE ".$meses[date('n')-1]. " DE ".date('Y') ;
      ?>  
    <header class="clearfix">
      <div id="logo">
         <img src={{asset('images/logo_header.png')}}>
      </div>
      <div id="company">
        <div>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</div>
				<div>DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS</div>
				<div>TALLER DE PANADERÍA</div>
        <div>DONATIVOS</div>
        <div>OAXACA DE JUÁREZ A {{$fecha}}</div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div style="font-weight:bold" class="invoice">PRODUCTOS SOLICITADOS PARA DONATIVO</div>
       </div>
        <div id="invoice">
          <div class="invoice">FOLIO: {{$solicitud->folio}}</div>
          <div class="invoice" >Fecha solicitud: {{date("d-m-Y", strtotime($solicitud->fecha_solicitud))}}</div>
          <div class="invoice" >Fecha entrega: {{date("d-m-Y", strtotime($solicitud->fecha_entrega))}}</div>
        </div>
      </div>
      <table id="tablita">
        <thead style="background-color: lightgray;">
            <tr>
              <th colspan="1">Cantidad</th>
          <th colspan="3">Producto</th>
          <th colspan="6">Precio</th>   
          <th colspan="8">Subtotal</th>               
            </tr>
          </thead>
          <tbody>
                @foreach ($productos as $item)                      
                <tr>
                  <td  colspan="1">{{$item->pivot->cantidad}}</td>
                  <td  colspan="3">{{$item->nombre}}</td>
                  <td  colspan="6">${{$item->pivot->precio_unitario}}</td>
                  <td  colspan="8">{{$item->pivot->subtotal}}</td>                           
                </tr>
               @endforeach   
          </tbody>
        <br>
      </table>
           <div id="invoice" style="font-weight:bold">Total: <span>${{$solicitud->total}}</span></div>
    
    </main>
    <footer style="bottom:40px; position:fixed;">
    <img class="img-fluid mt-2 footer" style="margin-bottom:50px ; width: 100%;" src={{asset('images/logo_footer.png')}} >
 </footer>
  </body>
</html>
<style>
  @font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 17cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: Verdana, Arial, sans-serif

}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

#logo img {
  height: 85px;
}

#company {
 /* font-size: 30px;
/*   float: right;
 */  text-align: right;
}


#details {
  margin-bottom: 50px;
}

#client {
  padding-left: 6px;
  float: left;
}

#client .to {
  color: #777777;
}

 h2.name {
  font-size: 10px;
  font-weight: normal;
  margin: 0;
} 

#invoice {
  padding-right: 6px;
   /*  float: right;*/
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}
/*  */
#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}
table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}

#tablita td, #tablita th {
  border: 1px solid #ddd;
  padding: 8px;
}
</style>