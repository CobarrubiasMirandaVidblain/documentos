<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Reporte de Ventas</title>  
  <style type="text/css">
  p{
    font-size: 12px;
    text-transform: uppercase !important;
  }
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>
</head>

<body>  
      <table width="100%">
          <tr>
          <td align="bottom"><img src={{asset('images/logo_header.png')}} alt="" width="150"/></td>
              <td align="right">
                  <?php
				setlocale (LC_TIME, "es_MX");
				$meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");

        $fecha=date('d')." DE ".$meses[date('n')-1]. " DE ".date('Y') ;
        ?>
                  <h3>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</h3>
                  <pre>                      
											DIRECCIÓN DE ADMINISTRACION Y FINANZAS
											TALLER DE PANADERÍA
                      REPORTE DE VENTAS
                      OAXACA DE JUÁREZ A {{$fecha}}
                  </pre>
              </td>
          </tr>
      
        </table>
  <br>
  <table width="100%">
    <tbody>
      <tr>
        <td>
          <p>En la siguiente gráfica se muestra el total de ventas por día del periodo de {{$periodo}}:</p>
        </td>
      </tr>
      <tr>
        <td align="center">
          <img src="<?php echo $grafica1;?>" style="  width: 500px; height: 250px;">
        </td>        
      </tr>     
    </tbody>
  </table>
  <br>
  <table width="100%">
      <tbody>  
        <tr>
          <td>
            <p>En la siguiente gráfica se muestra el total de ventas, costos de producción y las ganancias de este periodo:</p>
          </td>
        </tr>     
        <tr align="center">
            <td>
                <img src="<?php echo $grafica2;?>" style="  width: 500px; height: 280px;">
            </td>
        </tr>
      </tbody>
    </table>
  <br>
  <table width="100%" style="border-collapse:collapse;border-color:#ddd;" border="1" cellpadding="0" cellspacing="1" bordercolor="#000000">
    <thead>
      <tr>
        <th>Total de ventas</th>
        <th>Costo de producción</th>
        <th>Ganancias</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      <td>${{$ventas}}</td>
        <td>${{$costo}}</td>
        <td>${{$ganancia}}</td>
      </tr>
    </tbody>
  </table>
  
  <footer style="bottom:40px; position:fixed;">
    <img class="img-fluid mt-2 footer" style="margin-bottom:50px ; width: 100%;" src={{asset('images/logo_footer.png')}}>
  </footer>
</body>

</html>