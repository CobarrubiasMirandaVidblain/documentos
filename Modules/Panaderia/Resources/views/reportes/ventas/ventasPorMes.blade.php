<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ventas Panaderia</title>
  <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components\e\echarts.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components\e\echarts-gl.min.js') }}"></script>
  <style>
    @media print{
                  .oculto-impresion, .oculto-impresion *{
                    display: none !important;
                  }
                }
  </style>
</head>
<body>
    <div id="main" style="width: 600px;height:400px;"></div>    
    {{-- <button class="oculto-impresion" onclick="imprimir()">Imprimir</button> --}}
    <script type="text/javascript">
        // based on prepared DOM, initialize echarts instance
        var myChart = echarts.init(document.getElementById('main'));

        // specify chart configuration item and data
        var option = {
            title: {
                text: 'ECharts entry example'
            },
            tooltip: {},
            legend: {
                data:['Sales']
            },
            xAxis: {
                data: ["shirt","cardign","chiffon shirt","pants","heels","socks"]
            },
            yAxis: {},
            series: [{
                name: 'Sales',
                type: 'bar',
                data: [5, 20, 36, 10, 10, 20]
            }]
        };

        // use configuration item and data specified to show chart
        myChart.setOption(option);        
        function imprimir(){
          window.print();
        }
        try { this.print(); } catch (e) { window.onload = window.print; } 
        /* window.onload = function(){
          window.print();
          } */
    </script>

  
</body>
</html>