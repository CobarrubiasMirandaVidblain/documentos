@extends('vendor.admin-lte.layouts.main')
<?php $ruta = \Request::route()->getName(); ?>
@if (auth()->check())
<!-- @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia())) -->
<!-- @section('user-name', auth()->user()->persona->nombre) -->
@section('user-job')
@section('user-profile', route('users.edit', auth()->user()->id))
<!-- @section('user-log', auth()->user()->created_at) -->
@endif

@push('head')
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<link rel="stylesheet" href="{{asset('dist/css/blue.css')}}" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{asset('css/sweetalert2.min.css')}}" type="text/css" rel="stylesheet">
@yield('mystyles')
<style>
  input{
    border-radius: 4px;
    text-transform: uppercase !important;
  }
  textarea{text-transform: uppercase !important;  
  }
  .fade-enter-active, .fade-leave-active {
    transition: opacity .25s;
  }
  .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
    opacity: 0;
  }
  .badge-success {
    background-color: #00a65a;
  }
  .badge-danger {
    background-color: #D73925;
  }
  .badge-primary {
    background-color: #007BFF;
  }
  .badge-secondary {
    background-color: #17A2B8;
  }
  .badge-warning {
    background-color: #E08E0B;
  }
  .modal-header {
  padding: 9px 15px;
  border-bottom: 1px solid #eee;
  background-color:#1B96B8;
  color: #fff;
}
  
}
  </style>    
@endpush
@section('logo',asset('images\CANNA\ImagenCannaIntranet.png'))    
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
  <li class="header">Mi Panaderia</li>
  <li><a href="/panaderia#/"><i class="fa fa-dashboard" aria-hidden="true"></i> <span class="pull-left-container"> <span>Panel de Control</span> </a></li>
  {{-----------------------------------------------------Equipos---}}
  {{-- @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO'],'PANADERIA'))
    <li><a href="/panaderia#/inventario/equipos"> <i class="fa fa-wrench"></i> <span class="pull-left-container"> <span>Maquinaria</span> </a></li>
  @endif  --}} 
  {{------------------------------------------------------Almacen--}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','ALMACEN'],'PANADERIA'))
    <li class="treeview">
      <a href="#">
        <i class="fa fa-archive"></i> <span>Almacén</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">                 
        <li><a href="/panaderia#/almacen/entradas"><i class="fa fa-arrow-right"></i> Agregar insumos</a></li>      
        <li><a href="/panaderia#/almacen/salidas"><i class="fa fa-arrow-left"></i> Salidas pendientes</a></li>  
        <li><a href="/panaderia#/almacen/historialSalidas"><i class="fa fa-history" aria-hidden="true"></i> Historial de salidas</a></li>  
        <li><a href="/panaderia#/almacen/existencias"> <i class="fa  fa-list"></i>Insumos</a></li>    
      </ul>
    </li>
  @endif
  {{------------------------------------------------------Productos--}} 
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','VENDEDOR'],'PANADERIA'))
  <li><a href="/panaderia#/productos/listado"> <i class="fa fa-ship"></i> <span class="pull-left-container"> <span>Productos</span> </a></li>
  @endif  
  {{-------------------------------------------------------Ventas-----------}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','VENDEDOR'],'PANADERIA'))      
  <li><a href="/panaderia#/ventas/nuevaVenta"> <i class="fa fa-usd"></i> <span class="pull-left-container"> <span>Ventas</span> </a></li>
  @endif
  {{-------------------------------------------------------Donativos------------}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','PRODUCCION'],'PANADERIA'))
  <li class="treeview"><a href="#"><i class="fa fa-handshake-o"></i> <span>Donativos</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
    <ul class="treeview-menu">
			@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO'],'PANADERIA'))
			<li><a href="/panaderia#/donativos"><i class="fa fa-list"></i>Listado de peticiones</a></li>     
			@endif
{{--       <li><a href="/panaderia#/donativos/programas"><i class="fa fa-cubes"></i>Beneficios</a></li> 
 --}}      <li><a href="/panaderia#/donativos/aceptados"><i class="fa fa-fw fa-check"></i>Donativos aceptados</a></li>
    </ul>
  </li>
  @endif
  {{-------------------------------------------------------Pedidos------------}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','VENDEDOR','PRODUCCION'],'PANADERIA'))
  <li class="treeview"><a href="#"><i class="fa fa-pencil-square-o"></i><span>Pedidos</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
    <ul class="treeview-menu">
      @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','VENDEDOR'],'PANADERIA'))
      <li><a href="/panaderia#/pedidos"><i class="fa fa-plus"></i>Registrar pedido</a></li>  
      @endif      
      <li><a href="/panaderia#/pedidos/listado"><i class="fa fa-list"></i>Lista de pedidos</a></li>                       
    </ul>
  </li>
  @endif
  {{-------------------------------------------------------Clientes------------}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO'],'PANADERIA'))
  <li><a href="/panaderia#/clientes"> <i class="fa fa-user"></i><span class="pull-left-container"> <span>Clientes</span> </a></li>
  @endif 
  {{--Producción--}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','PRODUCCION'],'PANADERIA'))
  <li class="treeview"><a href="#"><i class="fa fa-product-hunt"></i><span>Producción</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
    <ul class="treeview-menu">
      <li><a href="/panaderia#/produccion/programacion"><i class="fa fa-list"></i>Producciones programadas</a></li>           
      <li><a href="/panaderia#/produccion/producciones"><i class="fa fa-list"></i>Producciones</a></li> 
    </ul>
  </li>
  @endif
  {{-------------------------------------------------------Recetario------------}}
  @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO','PRODUCCION'],'PANADERIA'))      
  <li class="treeview"><a href="#"><i class="fa fa-book" aria-hidden="true"></i><span>Recetario</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
    <ul class="treeview-menu">
      <li><a href="/panaderia#/recetas/listado"><i class="fa fa-list"></i>Listado de Recetas</a></li>     
      <li><a href="/panaderia#/recetas/nuevaReceta"><i class="fa fa-cubes"></i>Nueva Receta</a></li>       
    </ul>      
  </li> 
  @endif 
   {{-------------------------------------------------------Cursos------------}}
   @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO'],'PANADERIA'))      
   <li class="treeview"><a href="#"><i class="fa fa-book"></i> <span>Cursos</span> <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
     <ul class="treeview-menu">
       <li><a href="/panaderia#/cursos/listado"><i class="fa fa-list"></i>Listado de cursos</a></li>     
     </ul>      
   </li> 
   @endif 
   {{----------------Reportes--------------------}}
   @if (auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ENCARGADO'],'PANADERIA'))
   <li><a href="/panaderia#/reportes"> <i class="fa fa-bar-chart"></i><span class="pull-left-container"> <span>Reportes</span> </a></li>
   @endif 
</ul>
@endsection
@section('content-title')
<img src="{{asset('images/logoMiPanaderiaDIF.png')}}" height="50" alt="">
@endsection
@section('content-subtitle','')
@section('breadcrumbs')
    
@endsection

@section('message-url-all', '#')
@section('message-all', 'Ver todos los mensajes')
@section('message-number')
@section('message-text', 'Tienes 1 mensages')
@section('message-list')
<!-- start message -->
<li>
  <a href="#">
      <div class="pull-left">
          <!-- User Image -->
          <img src="https://www.gravatar.com/avatar/?d=mm" class="img-circle" alt="User Image">
      </div>
      <!-- Message title and timestamp -->
      <h4>
          Support Team
          <small><i class="fa fa-clock-o"></i> 5 mins</small>
      </h4>
      <!-- The message -->
      <p>Why not buy a new awesome theme?</p>
  </a>
</li>
<!-- end message -->
@endsection

@section('notification-id', 'notificaciones')
@section('notification-link', 'notificaciones-link')
@section('notification-dropdown-ul', 'notificaciones-ul')
@section('notification-number')
{{-- @section('notification-text', 'Hola mundo') --}}
@section('notification-dropdown-id', 'notificaciones-titulo')
{{-- @section('notification-list')
<a href="/recmat">
    <i class="fa fa-refresh text-yellow" id="reinicio"></i> Recargue la pagina
</a>
@endsection --}}
@section('notification-dropdown-li-id', 'notificaciones-lista')

@section('task-number')
@section('task-text', 'Tienes 1 tarea')
@section('task-url-all')

@endsection
@section('task-list')
<li>
  <a href="#">
      <!-- Task title and progress text -->
      <h3>
          Design some buttons
          <small class="pull-right">20%</small>
      </h3>
      <!-- The progress bar -->
      <div class="progress xs">
          <!-- Change the css width attribute to simulate progress -->
          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
              <span class="sr-only">20% Complete</span>
          </div>
      </div>
  </a>
</li>
@endsection

@push('body')
<script src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script src="{{asset('js/sweetalert2.all.js')}}"></script>
<script>
  function block() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: 'none',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .8,
            color: '#fff',
        },
        baseZ: 10000,
        message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>',
    });

    function unblock_error() {
        if($.unblockUI())
            alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
    }
  }
  function unblock() {
    $.unblockUI();
  }
</script>
<script>
    try{
    sessionStorage.setItem('pan_token', '{{$token}}');    
  }catch(error){
    swal('Oops !', 'Algo salio mal. Intente recargar la página', 'warning');
  }
  </script>
<script src="{{ asset('panaderiam/app.js') }}"></script>
@yield('miscript')
@endpush