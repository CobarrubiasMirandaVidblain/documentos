<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Programa extends Model
{
    use SoftDeletes;
  
    protected $table = 'programas';    
    protected $dates = ['deleted_at'];    
    protected $fillable = ['nombre', 'activo', 'descripcion', 'tipo', 'codigo', 'fecha_inicio', 'fecha_fin', 'oficial', 'usuario_id', 'padre_id', 'puede_agregar_benef'];
        
    
    public function usuario() {
      return $this->belongsTo('App\Models\Usuario');
    }        
        
    public function programassolicitudes() {
      return $this->hasMany('App\Models\ProgramasSolicitud');
    } 
    
    
    
}
