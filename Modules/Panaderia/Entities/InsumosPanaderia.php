<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsumosPanaderia extends Model
{
    use SoftDeletes;
    protected $table = 'pan_insumos';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','insumo_id','presentacion_id','folio_ordencompra',
    'cantidad_entrada','fecha_caducidad','lote','cantidad','precio_unitario','observacion','usuario_id'];

    public function usuario() {//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
      }
    
    public function insumo(){
        return $this->belongsTo(Insumo::class,'insumo_id');
    }
    public function presentacion(){
        return $this->belongsTo(Presentacion::class,'presentacion_id');
    }

}
