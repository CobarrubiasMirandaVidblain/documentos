<?php
namespace Modules\Panaderia\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class BeneficioprogramaSolicitud extends Model
{
    use SoftDeletes;    
    protected $appends=['laststatus'];    
    protected $table = 'beneficiosprogramas_solicitudes';    
    protected $dates = ['deleted_at'];    
    protected $fillable = ['solicitud_id', 'beneficioprograma_id', 'cantidad', 'folio','usuario_id'];
        
    public function usuario(){
        return $this->belongsTo('App\Models\Usuario');
    }   
    public function status(){
        return $this->belongsToMany(Statusproceso::class,'estados_solicitudes','beneficioprograma_solicitud_id','statusproceso_id')
        ->withPivot('usuario_id');
    }


    public function beneficios(){
        return $this->belongsTo(Beneficioprograma::class,'beneficioprograma_id')
        ->where('anio_programa_id','=','424');
    }

    public function getLaststatusAttribute(){
        return $this->status->last();   
    }

    public function solicitud(){
        return $this->belongsTo(SolicitudGeneral::class,'solicitud_id')
        ->with('beneficiossolicitados','tiposremitente','tiposrecepcion','solicitudespersonales','solicitudesmunicipios','solicitudesregiones','solicitudesinstituciones');
    }
}
