<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receta extends Model
{
    use SoftDeletes;
    protected $table = 'pan_recetas';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','producto_id','nombre','porciones','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function ingredientes(){
        return $this->belongsToMany(Insumo::class,'pan_ingredientes_recetas','receta_id','insumo_id')
        ->withPivot('cantidad','presentacion_id','usuario_id');
    }    
    public function presentacion(){
        return $this->belongsToMany(Presentacion::class,'pan_ingredientes_recetas','receta_id','presentacion_id')
        ->withPivot('cantidad','insumo_id','usuario_id');
    }

    public function producto(){
        return $this->belongsTo(Producto::class,'producto_id','id');
    }
}
