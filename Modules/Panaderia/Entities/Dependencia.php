<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Dependencia extends Model
{
    use SoftDeletes;
    protected $table = 'cat_instituciones';
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","calle","numero","colonia","telefono","email","encargado","cargoencargado","tipoinstitucion_id","entidad_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario() {//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }
    
    public function tipo(){
		return $this->belongsTo(App\Models\TipoInstitucion::class,'tipoinstitucion_id');
    }
    
    public function solicitudesinstituciones(){
      return $this->belongsToMany(SolicitudGeneral::class,'solicitudes_dependencias','institucion_id','solicitud_id');       
   } 
}