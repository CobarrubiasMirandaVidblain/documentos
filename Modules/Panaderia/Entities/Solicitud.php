<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitud extends Model
{
    use SoftDeletes;
    protected $table = 'pan_solicitudes';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','estatus_id','fecha_solicitud','fecha_entrega','total','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }
 
    public function estatus(){
        return $this->belongsTo(Status::class,'estatus_id');
    }

    public function pedidos(){
        return $this->hasOne(Pedido::class,'solicitud_id','id');
    }

    public function donativos(){
        return $this->hasOne(Donativo::class,'solicitud_id','id');
    }

    public function detalles(){
        return $this->belongsToMany(Producto::class,'pan_detallessolicitud','solicitud_id','producto_id')
        ->withPivot('cantidad','precio_unitario','subtotal','usuario_id');
    }

}
