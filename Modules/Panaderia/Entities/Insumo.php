<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Insumo extends Model
{
    use SoftDeletes;
    protected $table = 'recm_cat_productos';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','producto','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function entradas(){
        return $this->hasMany(InsumosPanaderia::class,'insumo_id','id')->with('presentacion');
    }
        
    public function unidadesMedida(){
        return $this->belongsToMany(Presentacion::class,'recm_presentacion_productos','producto_id','presentacion_id');
    }

    public function salidas(){
        return $this->belongsToMany(Produccion::class,'pan_salidasinsumo','insumo_id','produccion_id')
        ->withPivot('fecha_salida','cantidad','usuario_id');
    }

    public function recetas(){
        return $this->belongsToMany(Receta::class,'pan_ingredientes_receta','insumo_id','receta_id')
        ->withPivot('cantidad','usuario_id');
    }


}
