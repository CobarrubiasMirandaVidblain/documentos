<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    protected $table = 'cat_municipios';  
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'distrito_id', 'latitud', 'longitud'];

    public function personas()
    {
    	return $this->hasMany('App\Models\Persona');
    }    

    public function donativos(){
        return $this->belongsToMany(Donativo::class,'pan_donativos_municipios','municipio_id','donativo_id')
        ->withPivot('usuario_id');
      } 

      public function solicitudesmunicipio(){
        return $this->belongsToMany(SolicitudGeneral::class,'solicitudes_municipios','municipio_id','solicitud_id')
        ->withPivot('persona_id','cargo_id');
      
    }

    public function persona(){
      return $this->belongsTo('App\Models\Persona');
  }
}
