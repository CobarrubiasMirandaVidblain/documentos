<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productoterminado extends Model
{
    use SoftDeletes;
    protected $table = 'pan_productosterminados';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','produccion_id','producto_id','cantidad','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function producto(){
        return $this->belongsTo('Modules\Panaderia\Entities\Producto');
    }

    public function produccion(){
        return $this->belongsTo('Modules\Panaderia\Entities\Produccion');
    }
}
