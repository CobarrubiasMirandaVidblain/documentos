<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Status extends Model
{
    use SoftDeletes;
    protected $table = 'pan_cat_estatus';   
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $fillable=['id','nombre'];

    public function solicitudes(){
        return $this->hasMany('Modules\Panaderia\Entities\Solicitud');
    }
    
    public function produccionesProgramadas(){
        return $this->hasMany('Modules\Panaderia\Entities\Produccionprogramada');
    }
}
