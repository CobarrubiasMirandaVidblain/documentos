<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    use SoftDeletes;
    protected $table = 'pan_productos';
    protected $fillable = ['id','tipoproducto_id','nombre','precio',
                            'stock','activo','image','solicitado','descripcion','usuario_id'];
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function tipo(){
        return $this->belongsTo(Tipoproducto::class,'tipoproducto_id');
    }

    public function programados(){
        return $this->belongsToMany(Produccionprogramada::class,'pan_productosprogramados','producto_id','produccionprogramada_id')
        ->withPivot('cantidad','usuario_id');
    }
    
    public function terminados(){
        return $this->belongsToMany(Produccion::class,'pan_productosterminados','producto_id','produccion_id')
        ->withPivot('cantidad','usuario_id');
    }

    public function solicitados(){
        return $this->belongsToMany(Solicitud::class,'pan_detallessolicitud','producto_id','solicitud_id')
        ->withPivot('cantidad','precio_unitario','subtotal','usuario_id');
    }

    public function vendidos(){
        return $this->belongsToMany(Venta::class,'pan_detallesventa','producto_id','venta_id')
        ->withPivot('cantidad','precio_unitario','subtotal','usuario_id');
    } 
    
    public function receta(){
        return $this->hasOne(Receta::class,'producto_id','id');
    }

    public function producciones(){
        return $this->belongsToMany(Produccion::class,'pan_salidasinsumo','producto_id','produccion_id');
    }

}
