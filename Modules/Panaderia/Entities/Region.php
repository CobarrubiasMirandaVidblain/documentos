<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use SoftDeletes;
    protected $table = 'cat_regiones';    
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function donativos(){
        return $this->belongsToMany(Donativo::class,'pan_donativos_regiones','region_id','donativo_id')
        ->withPivot('usuario_id');
    }
    
    public function solicitudesregiones(){
      return $this->belongsToMany(SolicitudGeneral::class,'solicitudes_regiones','region_id','solicitud_id')
      ->withPivot('persona_id','cargo_id');  
    }

    public function getPersonaAttribute(){
      return $this->belongsTo('App\Models\Persona');
  }
}