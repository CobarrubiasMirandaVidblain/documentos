<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleado extends Model
{
    use SoftDeletes;
    protected $table = 'pan_empleados';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','persona_id','tipoempleado_id','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }
    
    public function tipoEmpleado(){
        return $this->belongsTo('Modules\Panaderia\Entities\Tipoempleado');
    }

    public function pedidos(){
        return $this->hasMany('Modules\Panaderia\Entities\EmpleadoPedido');
    }

    public function donativos(){
        return $this->hasMany('Modules\Panaderia\Entities\Donativo');
    }

    public function ventas(){
        return $this->hasMany('Modules\Panaderia\Entities\Venta');
    }

    public function produccion(){
        return $this->hasMany('Modules\Panaderia\Entities\Produccion');
    }

    public function salidasInsumo(){
        return $this->hasMany('Modules\Panaderia\Entities\Salidainsumo');
    }

    public function produccionProgramada(){
        return $this->hasMany('Modules\Panaderia\Entities\Produccionprogramada');
    }
}
