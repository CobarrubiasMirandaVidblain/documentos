<?php

namespace Modules\Panaderia\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes;

    protected $table = 'personas';
    protected $dates = ['deleted_at'];
    protected $appends = ['nombre_completo','direccion_completa'];
    protected $fillable = ['titulo', 'nombre', 'primer_apellido', 'segundo_apellido', 'calle', 'numero_exterior', 'numero_interior', 'colonia', 'codigopostal', 'municipio_id', 'localidad_id', 'curp', 'genero', 'fecha_nacimiento', 'clave_electoral', 'numero_celular', 'numero_local', 'email', 'referencia_domicilio', 'curpo', 'fotografia', 'usuario_id','etnia_id','num_acta_nacimiento'];    

    public function getNombreCompletoAttribute() 
    {  
      return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;  
    }

    public function getDireccionCompletaAttribute() 
    {  
      return $this->calle . ' ' . $this->numero_exterior . ' ' . $this->colonia;  
    }

    public function localidad(){
        return $this->belongsTo('App\Models\Localidad');
    }

    public function solicitudespersonales(){
        return $this->belongsToMany(SolicitudGeneral::class,'solicitudes_personales','persona_id','solicitud_id');        
    }   

    public function donativos(){
        return $this->belongsToMany(Donativo::class,'pan_beneficiarios_donativos','persona_id','donativo_id')
        ->withPivot('usuario_id');
      }   
    
    public function usuario() {//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario', 'id', 'usuario_id');
    }

    public function usuario2() {//una persona tipo usuario
        return $this->hasOne('App\Models\Usuario');
    }

    public function empleado() {
        return $this->hasOne('App\Models\Empleado');
    }

    
    public function obtenerNombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    
    public function solicitudes() {
        return $this->hasMany('App\Models\Solicitud', 'id');
    }

    
    public function solicitudespersonas() {
        return $this->hasMany('App\Models\SolicitudesPersona')->orderBy('updated_at','DESC');
    }

    
    public function nombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    public function pedidos(){
        return $this->hasMany(Pedido::class,'cliente_id');
    }

    
}