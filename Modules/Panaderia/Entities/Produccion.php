<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produccion extends Model
{
    use SoftDeletes;
    protected $table = 'pan_producciones';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','folio','fecha_elaboracion','observacion','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }
    
    public function empleado(){
        return $this->belongsTo('Modules\Panaderia\Entities\Empleado');
    }

    public function productosTerminados(){
        return $this->belongsToMany(Producto::class,'pan_productosterminados','produccion_id','producto_id')
        ->withPivot('cantidad','usuario_id');
    }

    public function salidasInsumos(){
        return $this->belongsToMany(InsumosPanaderia::class,'pan_salidasinsumo','produccion_id','insumo_id')
        ->withPivot('fecha_salida','cantidad','costo_salida','producto_id','tipo','usuario_id');
    }

    public function solicitud(){
        return $this->hasOne(Produccionsolicitud::class,'produccion_id','id')->with('solicitud');
    }

    public function normal(){
        return $this->hasOne(Produccionnormal::class,'produccion_id','id')->with('programada');
    }

    public function productos(){
        return $this->belongsToMany(Producto::class,'pan_salidasinsumo','produccion_id','producto_id');        
    }


}
