<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produccionprogramada extends Model
{
    use SoftDeletes;
    protected $table = 'pan_produccionesprogramadas';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','estatus_id','fecha_programada','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }
    
    public function estatus(){
        return $this->belongsTo(Status::class,'estatus_id');
    }
    public function programados(){
        return $this->belongsToMany(Producto::class,'pan_productosprogramados','produccionprogramada_id','producto_id')
        ->withPivot('cantidad','usuario_id');
    }

    public function normal(){
        return $this->hasOne(ProduccionNormal::class,'produccionprogramada_id','id');
    }

}

