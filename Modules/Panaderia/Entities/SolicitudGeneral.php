<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudGeneral extends Model
{
    protected $table = 'solicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["folio","fechaoficio","fecharecepcion","numoficio","asunto","observaciones","compromiso","tiposrecepcion_id","tiposremitente_id","persona_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');      
    
    
    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }

    public function tiposrecepcion(){
    	return $this->belongsTo('App\Models\Tiposrecepcion');
    }

    public function beneficiossolicitados(){
        return $this->hasMany(BeneficioprogramaSolicitud::class,'solicitud_id','id')->with('beneficios')
                    ->whereHas('beneficios');
                                                           
        
    }
    public function tiposremitente(){
    	return $this->belongsTo('App\Models\Tiposremitente');
    }

 /*    public function beneficios(){
        return $this->belongsToMany(Beneficioprograma::class,'beneficiosprogramas_solicitudes','solicitud_id','beneficioprograma_id')      
        ->withPivot('cantidad','folio','usuario_id');
    }
     */
    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

    public function solicitudesmunicipios(){
        return $this->belongsToMany(Municipio::class,'solicitudes_municipios','solicitud_id','municipio_id')
        ->withPivot('persona_id','cargo_id');      
    }

    public function solicitudespersonales(){
        return $this->belongsToMany(Persona::class,'solicitudes_personales','solicitud_id','persona_id');       
    }

    public function solicitudesregiones(){
        return $this->belongsToMany(Region::class,'solicitudes_regiones','solicitud_id','region_id')
        ->withPivot('persona_id','cargo_id');      
    }
    public function solicitudesinstituciones(){
        return $this->belongsToMany(Dependencia::class,'solicitudes_dependencias','solicitud_id','institucion_id');     
    }    
    
}
