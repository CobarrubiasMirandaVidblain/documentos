require("./bootstrap");
import Vuetify from "vuetify";
import Vue from "vue";
import VeeValidate from "vee-validate";
import VueSwal from "vue-swal";
import excel from "vue-excel-export";
import Chart from "chart.js";
import vSelect from "vue-select";
import Permissions from "./mixins/Permissions";

Vue.mixin(Permissions);

Vue.use(excel);

Vue.use(VueSwal);
Vue.use(Vuetify);
Vue.use(VeeValidate);

window.Vue = require("vue");

Vue.component(
  "form-create",
  require("./components/FormCreate/FormCreate.vue").default
);

Vue.component(
  "form-edit-petition",
  require("./components/FormEditPetition.vue").default
);
Vue.component(
  "form-assign-lawer",
  require("./components/FormAssignLawer.vue").default
);

Vue.component("reassign-lawer", require("./reasignlawer/index.vue").default);
Vue.component(
  "reassign-format",
  require("./reasignlawer/reasignFormat.vue").default
);
Vue.component("list-users", require("./reasignlawer/listUsers.vue").default);

Vue.component(
  "form-assign-process",
  require("./components/FormAssignProcess.vue").default
);
Vue.component(
  "address-component",
  require("./components/AddressComponent.vue").default
);
Vue.component(
  "relationship-component",
  require("./components/RelationshipComponent.vue").default
);
Vue.component(
  "beneficios-component",
  require("./components/BeneficiosComponent.vue").default
);

Vue.component(
  "advisor-component",
  require("./components/lawerprocess/AdvisorComponent.vue").default
);
Vue.component(
  "mediation-component",
  require("./components/lawerprocess/MediationComponent.vue").default
);
Vue.component(
  "expedient-component",
  require("./components/lawerprocess/ExpedientComponent.vue").default
);
Vue.component(
  "razon-component",
  require("./components/lawerprocess/RazonDateComponent.vue").default
);

Vue.component(
  "calendar-component",
  require("./components/CalendarComponent.vue").default
);
Vue.component(
  "victims-report-component",
  require("./components/VictimsReportComponent.vue").default
);

Vue.component(
  "card-lawer",
  require("./staticcomponents/CardLawer.vue").default
);
Vue.component(
  "card-petitioner",
  require("./staticcomponents/CardPetitioner.vue").default
);
Vue.component(
  "card-petition",
  require("./staticcomponents/CardPetition.vue").default
);
Vue.component(
  "card-appointment",
  require("./staticcomponents/CardAppointment.vue").default
);
Vue.component(
  "card-entailment",
  require("./staticcomponents/CardEntailment.vue").default
);

Vue.component(
  "test-card",
  require("./staticcomponents/UnusedCardVictim.vue").default
);

Vue.component(
  "administrador-component",
  require("./components/home/AdministradorComponent.vue").default
);
Vue.component(
  "procurador-component",
  require("./components/home/ProcuradorComponent.vue").default
);
Vue.component(
  "subprocurador-component",
  require("./components/home/SubprocuradorComponent.vue").default
);
Vue.component(
  "abogado-component",
  require("./components/home/AbogadoComponent.vue").default
);
Vue.component(
  "capturista-component",
  require("./components/home/CapturistaComponent.vue").default
);
Vue.component(
  "nuevo-component",
  require("./components/home/NuevoComponent.vue").default
);

Vue.component(
  "report-component",
  require("./reports/ReportComponent.vue").default
);
Vue.component("line-report", require("./reports/LineReport.vue").default);

Vue.component(
  "modal-report",
  require("./components/shows/ModalReport.vue").default
);
Vue.component(
  "shows-appointments",
  require("./components/appointments/ShowsAppointments.vue").default
);

Vue.component(
  "list-petitions",
  require("./components/petitions/listPetitionsComponent.vue").default
);

Vue.component(
  "search-panel",
  require("./components/petitions/search/SearchPanelComponent.vue").default
);
Vue.component(
  "search-petitions",
  require("./components/petitions/search/SearchPetitionsComponent.vue").default
);
Vue.component(
  "search-victims",
  require("./components/petitions/search/SearchVictimsComponent.vue").default
);

Vue.component(
  "expedient-list-component",
  require("./components/lawerprocess/expedientAssets/ExpedientListComponent.vue")
    .default
);
Vue.component(
  "show-action-component",
  require("./components/lawerprocess/expedientAssets/ShowActionComponent.vue")
    .default
);
Vue.component(
  "cita-por-expediente",
  require("./components/lawerprocess/expedientAssets/CitaPorExpediente.vue")
    .default
);
Vue.component(
  "evidencia-por-expediente",
  require("./components/lawerprocess/expedientAssets/EvidenciaPorExpediente.vue")
    .default
);
Vue.component(
  "reporte-por-expediente",
  require("./components/lawerprocess/expedientAssets/ReportePorExpediente.vue")
    .default
);
Vue.component(
  "reporte-citas",
  require("./components/lawerprocess/expedientAssets/reporteporexpediente/ReporteCitas.vue")
    .default
);

Vue.component(
  "card-victims",
  require("./staticcomponents/CardVictimComponent.vue").default
);
Vue.component(
  "update-victims",
  require("./components/FormCreate/UpdateVictim.vue").default
);

Vue.component(
  "expedient-show-component",
  require("./components/lawerprocess/expedientAssets/ExpedientShowComponent.vue")
    .default
);

Vue.component(
  "message-component",
  require("./components/messages/MessageComponent.vue").default
);
Vue.component(
  "message-complet-component",
  require("./components/messages/MessageCompletComponent.vue").default
);

const app = new Vue({
  el: "#app"
});
