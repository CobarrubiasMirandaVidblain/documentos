<?php

namespace App\Http\Controllers;

use Auth;
use DB;

use App\User;

use Illuminate\Http\Request;

class LawerController extends Controller
{
    public function index()
    {

        if (Auth::user()->isAdmin()){
            $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->get();
            return view( 'lawers.index',compact('lawers') );
        }
        if (Auth::user()->isSubprocurador()){
            $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->where( 'subprocuracy_id' ,Auth::user()->subprocuracy_id)
            ->get();
            return view( 'lawers.index',compact('lawers') );
        }
        else{
            return 'No Permitido';
        }
    }

    public function getLawerExcept( User $user )
    {
        if (Auth::user()->isSubprocurador()){
            $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->where( 'subprocuracy_id' ,Auth::user()->subprocuracy_id)
            ->where( 'id','!=', $user->id  )
            ->get();
            return $lawers;
        }
        else{
            return "NO PERMITIDO";
        }
    }

    public function getLawers()
    {
        if (Auth::user()->isAdmin()){
            $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->get();
            return $lawers;
        }
        if (Auth::user()->isSubprocurador()){
            $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->where( 'subprocuracy_id' ,Auth::user()->subprocuracy_id)
            ->get();
            return $lawers;
        }
        else{
            return 'No Permitido';
        }
    }
}
