<?php

namespace App\Http\Controllers;

use App\Message;
use App\Petition;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function create(Request $request,Petition $petition)
    {
        Message::create($request->all());
        return "Creado con exito";
    }

}
