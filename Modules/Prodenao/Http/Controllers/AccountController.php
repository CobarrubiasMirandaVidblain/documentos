<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Action;

class AccountController extends Controller
{

    public function index()
    {
        //
    }

    public function store(Request $request,Action $action)
    {
        $account = Account::create($request->all());
        $account->update( ['action_id'=> $action->id,'expedient_id' => $action->expedient_id ] );
        return "Creado reporte con exito";
    }

    public function show($id)
    {
        //
    }
}
