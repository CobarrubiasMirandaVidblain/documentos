<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Meeting;
use App\Action;

class MeetingController extends Controller
{

    public function index()
    {
    }

    public function store(Request $request,Action $action)
    {
        $action->expedient->update(["campo"=>$request->dateappointment]);
        $action->update( ['date_in_process'=> 1 ] );
        $fecha = $request->dateappointment." ".$request->timeappointment;
        $dateappointment = Carbon::createFromFormat('Y-m-d H:i', $fecha);
        $request->merge(['dateappointment' => $dateappointment]);

        $meeting = Meeting::create( $request->all() );
        $meeting->update( ['action_id' => $action->id,'expedient_id' => $action->expedient_id ] );
        return "Guardado correctamente";
    }

    public function updatereport(Request $request, Meeting $meeting)
    {
        $meeting->action->expedient->update(["campo"=>$request->name]);
        $meeting->action->update( ['date_in_process'=> 0 ] );
        if ($request->hasFile('reporte')) {
            $url = $request->reporte->store('public/meetingsreport'); 
            $meeting->update( [ 'url_reporte'=> $url] );
            $meeting->update( [ 'report_complet'=> 1] );
            $meeting->update( [ 'report_name'=> $request->name] );
            $meeting->update( [ 'conclusion'=> $request->conclusion] );
            return "guardado correctamente";
        }
        return "No se guardo la cita";
    }

}
