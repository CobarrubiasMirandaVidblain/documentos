<?php

namespace App\Http\Controllers;

use Auth;
use DB;

use App\Affair;
use App\User;
use App\Petition;
use App\Victim;
use App\Message;

use Illuminate\Http\Request;
use App\Mail\ProcuraduriaNotificaciones;
use App\Mail\AdministradorProcuraduria;
use Illuminate\Support\Facades\Mail;

class PetitionController extends Controller
{
    public function index()
    {
        $ultima_peticion =  Petition::get()->last();
        //Validar que por lo menos exista una peticion
        if( $ultima_peticion->victims &&  $ultima_peticion->victims->isEmpty() )
        {
            $ultima_peticion->delete();
            $max = DB::table('petitions')->max('id') + 1; 
            DB::statement("ALTER TABLE petitions AUTO_INCREMENT =  $max");
        }
          
        if (Auth::user()->isSubprocurador()){
            $petitions = Petition::where('subprocuracy_id', Auth::user()->subprocuracy_id )->get();
            
        }
        elseif(Auth::user()->isAbogado()){
            $affairs = Affair::where( 'user_id',Auth::user()->id )->get();
            $petitions = collect();
            foreach( $affairs as $affair){
                $petitions->push( $affair->petition );
            }
        }
        else{
            $petitions = Petition::get();
        }
        toast('Indexación Correcta!','success','top-right');
        return view('petitions.index' ,compact('petitions') );
    }

    public function search()
    {
        $ultima_peticion =  Petition::get()->last();
        if( $ultima_peticion->victims->isEmpty() )
        {
            $ultima_peticion->delete();
            $max = DB::table('petitions')->max('id') + 1; 
            DB::statement("ALTER TABLE petitions AUTO_INCREMENT =  $max");
        }
          
        if (Auth::user()->isSubprocurador()){
            $petitions = Petition::where('subprocuracy_id', Auth::user()->subprocuracy_id )->get();
            $petitions = collect();
        }
        elseif(Auth::user()->isAbogado()){
            $affairs = Affair::where( 'user_id',Auth::user()->id )->get();
            $petitions = collect();
            foreach( $affairs as $affair){
                $petitions->push( $affair->petition );
            }
        }
        else{
            $petitions = Petition::get();
        }
        $victims = Victim::get();
        return view('petitions.search' ,compact('petitions','victims') );
    }

    public function create()
    {
        return view( 'petitions.create' );
    }

    public function store(Request $request)
    {
        $slug = $request->addressestado. $request->addressmunicipio.
                $request->addresscolonia. $request->addresscalle.
                $request->addressnumero ;
        $request->request->add( ['slug' => $slug] );

        $this->validate($request, [
            'description' => 'required|min:1',
            'slug' => 'unique:petitions',
        ]);

        $petition = Petition::create($request->all());
        $petition->update( ['subprocuracy_id' => ( $petition->id%2==0 )? "2" : "1" ]);

        $request->request->add( ['petition_id' => $petition->id] );

        return $request->all();
    }
    
    public function show(Petition $petition)
    {
        $victimas = $petition->victims;
        $mensajes = Message::get();
        return view('petitions.show' ,compact('petition','victimas','mensajes'));
    }

    public function edit(Petition $petition)
    {
        $victimas = $petition->victims;
        return view( 'petitions.edit' ,compact('petition','victimas') );
    }


    public function update(Request $request, Petition $petition)
    {
        $petition->update( $request->all() );
        return $request->all();
    }

    public function updateconclusion(Request $request, Petition $petition)
    {
        $petition->update( ['completed' => 1,'conclusion'=>$request->conclusion ]);
        $petition->update( ['finish_at' => now()]);
        return back();
    }

    public function destroy(Petition $petition)
    {
        return 'ELIMINADO ALFA-LSO';
    }

    public function editlawer(Petition $petition)
    {
        $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->where( 'subprocuracy_id' ,$petition->subprocuracy_id )
            ->get();
        return view( 'affairs.asignlawer' ,compact('petition','lawers') );
    }


    public function process()
    {
        if (Auth::user()->isSubprocurador()){
            $petitions = Petition::where('assignedaffair',"!=","0" )
                ->where('subprocuracy_id',Auth::user()->subprocuracy_id )->paginate();
        }
        elseif(Auth::user()->isAbogado()){
            $affairs = Affair::where( 'user_id',Auth::user()->id )->get();
            $petitions = collect();
            foreach( $affairs as $affair){
                $petitions->push( $affair->petition );
            }
        }
        elseif(Auth::user()->isAdmin()){
            $petitions = Petition::where('assignedaffair',"!=","0" )->paginate();
        }
        return view( 'petitions.trash.tableassigned',compact('petitions') );
    }

    public function pendients()
    {
        if (Auth::user()->isSubprocurador() || Auth::user()->isAbogado()){
            $petitions = Petition::where('assignedaffair',"0" )
            ->where('subprocuracy_id',Auth::user()->subprocuracy_id )->paginate();
        }
        elseif(Auth::user()->isAdmin()){
            $petitions = Petition::where('assignedaffair',"0" )->paginate();
        }
        else{
            return back();
        }

        return view( 'petitions.trash.tableunassigned',compact('petitions') );
    }

    public function existpersononexistspetitions(Request $request)
    {
        $persona = Petition::where( 'name',$request->name )
                            ->where( 'namepaterno',$request->namepaterno )
                            ->where( 'namematerno',$request->namematerno )
                            ->where( 'age',$request->age )
                            ->where( 'gender',$request->gender )->get();
        return $persona;
    }

    public function sendmail(Petition $petition)
    {
        $message = "Una nueva solicitud ha sido creada, y asignada a su subprocuraduría";
        $emisor = "Capturista";
        $receptor = "Subprocurador";
        Mail::to("zombie1288@outlook.com")
        ->send(new ProcuraduriaNotificaciones($petition,$message,$emisor,$receptor));
        return "exito";
    }
    public function assignmail(Request $request,Petition $petition)
    {
        Message::create($request->except('email'));
        Mail::to($request->email)
        ->send(new ProcuraduriaNotificaciones($petition, $request->mensaje,$request->emisor,$request->receptor ));
        return "exito";
    }

    public function reassignmail(Request $request)
    {
        $user = User::find($request->user_id);
        Mail::to($request->email)
        ->send(new AdministradorProcuraduria( $user ));
        return $user;
    }
    

}