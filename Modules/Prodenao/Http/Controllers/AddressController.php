<?php

namespace App\Http\Controllers;
use App\Municipio;
use App\Localidad;


use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class AddressController extends Controller
{
    public function indexMunicipios(){
        return new JsonResponse(Municipio::all());
    }
    public function indexLocalidades($id){
        return new JsonResponse(Localidad::where('municipio_id','=',$id)->get());
    }
}