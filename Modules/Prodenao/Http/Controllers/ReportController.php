<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Affair;
use App\Report;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function store(Request $request, Affair $affair )
    {
        $reporte = Report::create( $request->all() );
   
        if( $affair->advisor ){
            $reporte->update( ['advisor_id' => $affair->advisor->id ] );
        }
        elseif( $affair->mediation ){
            $reporte->update( ['mediation_id' => $affair->mediation->id ] );
        }
        elseif( $affair->expedient ){
            $reporte->update( [ 'expedient_id'=> $affair->expedient->id] );
        } 
        
        return "Exito al guardar el reporte";
    }
}