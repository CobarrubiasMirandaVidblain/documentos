<?php

namespace App\Http\Controllers;

use App\Advisor;
use App\Mediation;
use App\Expedient;

use App\Affair;
use App\Petition;
use App\User;

use Auth;
use DB;

use Illuminate\Http\Request;

class AffairController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'petition_id' => 'required|min:1',
            'user_id' => 'required|min:1',
            'description' => 'required|min:1',
        ]);


        if( Affair::where('petition_id', $request->petition_id)->exists() ){
            Affair::where('petition_id', $request->petition_id)
            ->update(['user_id'=>$request->user_id,'description'=>$request->description]);

            Petition::find($request->petition_id)
            ->update(['assignedaffair' => 1]);
            
            return $request->all();
        }

        $affair = Affair::create($request->all());
        
        $petition = Petition::find($request->petition_id);
        $petition->update( ['assignedaffair' => $affair->id] );
        
        return $request->all();
        
    }

    public function reasignlawer( User $user )
    {
        if (Auth::user()->isSubprocurador()){
            $lawers = DB::table('role_user')
            ->join( 'users' , 'role_user.user_id','users.id')
            ->join( 'roles' , 'role_user.role_id','roles.id')
            ->select( 'users.*' ,'roles.slug')
            ->where( 'slug' ,'abogado' )
            ->where( 'subprocuracy_id' ,Auth::user()->subprocuracy_id)
            ->get();

            $casos = Petition::where( 'subprocuracy_id', Auth::user()->subprocuracy_id  )
                                ->where( 'assignedaffair', '!=' ,'0' )
                                ->where( 'completed', '!=' ,'1' )->get();

            foreach( $casos as $caso){
                $caso->affair;
            }
            return view( 'affairs.reasignlawer',compact('user','lawers','casos') ); 
        }
        else{
            return 'No Permitido';
        }
        
    }

    public function types(Petition $petition)
    {
        return view( 'affairs.assignaffairprocess' ,compact('petition') );
    }

    public function updateconclusion(Request $request, Petition $petition)
    {
        $petition->affair->update( [ 'changed' =>$request->state ] );
        $petition->update( [ 'changed' =>$request->state ] );

        $petition->update( ['conclusion' => $request->conclusion]);
        $petition->update( ['finish_at' => now()]);
        return "exito";
    }
    public function demitconclusion(Request $request, Petition $petition)
    {
        $petition->affair->update( [ 'changed' =>0 ] );
        $petition->update( [ 'changed' =>0 ] );
        
        $petition->affair->update( [ 'step' =>1 ] );

        $petition->update( ['conclusion' => $request->conclusion]);
        $petition->update( ['finish_at' => now()]);
        return back();
    }

    public function defineOptionType(Request $request ,Petition $petition)
    {
        if ($request->type_name == "advisor"){
            $asesoria = Advisor::create(['affair_id' => $petition->affair->id ,'description'=>$request->description]);
            $petition->update( [ 'process'=>$request->type_name ] );
            return redirect()->route( 'petitions.index' ) -> with( 'info' ,'Asignada la asesoria Correctamente' );
        }
        elseif($request->type_name == "mediation"){
            $asesoria = Mediation::create(['affair_id' => $petition->affair->id ,'description'=>$request->description]);
            $petition->update( [ 'process'=>$request->type_name ] );
            return redirect()->route( 'petitions.index' ) -> with( 'info' ,'Asignada la Mediacion Correctamente' );
        }
        elseif($request->type_name == "expedient"){
            $asesoria = Expedient::create(['affair_id' => $petition->affair->id ,'description'=>$request->description]);
            $petition->update( [ 'process'=>$request->type_name ] );
            return redirect()->route( 'petitions.index' ) -> with( 'info' ,'Asignada el Expediente Correctamente' );
        }
        return back()->with('info','No es una opción de Seguimiento del caso');
    }

    public function assignstep(Request $request, Affair $affair)
    {
        $affair->update( [ 'step'=>$request->paso ] );
        return "cambiado correctamente";
    }


}
