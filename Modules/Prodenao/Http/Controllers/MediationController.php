<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Mediation;

use Illuminate\Http\Request;

class MediationController extends Controller
{
    public function show(Petition $petition)
    {
        return view( 'mediations.show' ,compact('petition') );
    }
    public function update(Request $request ,Mediation $mediation)
    {
        $mediacion = Mediation::find($mediation->id);
        $mediacion->update( ['description' => $request->description] ); 
        return redirect()->route( 'petitions.index' ) -> with( 'info' ,'Actializacion de Asesoria Correcta' );
    }
}
