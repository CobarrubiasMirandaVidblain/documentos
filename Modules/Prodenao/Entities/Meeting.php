<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $guarded =[];
    protected $dates = [
        'created_at',
        'updated_at',
        'dateappointment'
    ];
    
    public function action()
    {
        return $this->belongsTo('App\Action');
    }
    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    }
}
