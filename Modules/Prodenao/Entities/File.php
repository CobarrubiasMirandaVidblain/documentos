<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    protected $guarded =[];
    protected $appends = ['fullpath'];
    
    public function action()
    {
        return $this->belongsTo('App\Action');
    }
    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    }
    public function getFullPathAttribute()
    {
        return Storage::url($this->attributes['url']);
    }
}
