<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mediation extends Model
{
    protected $guarded =[];

    public function affair()
    {
        return $this->belongsTo('App\Affair');
    } 

    public function entailments()
    {
        return $this->hasMany('App\Entailment');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function evidences()
    {
        return $this->hasMany('App\Evidence');
    }
    
    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }
}
