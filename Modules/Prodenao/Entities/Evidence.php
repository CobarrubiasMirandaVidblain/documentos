<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    protected $table = 'evidences';
    protected $guarded =[];

    public function advisor()
    {
        return $this->belongsTo('App\Advisor');
    } 

    public function mediation()
    {
        return $this->belongsTo('App\Mediation');
    }

    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    }
}
