<?php

namespace App;

use App\Entailment;
use App\Report;
use App\Evidence;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    protected $guarded =[];

    public function affair()
    {
        return $this->belongsTo('App\Affair');
    } 

    public function entailments()
    {
        return $this->hasMany('App\Entailment');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function evidences()
    {
        return $this->hasMany('App\Evidence');
    }
}
