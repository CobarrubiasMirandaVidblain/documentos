<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $guarded =[];
    protected $appends = ['folio'];

    public function victims()
    {
        return $this->belongsToMany('App\Victim');
    }

    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function accounts()
    {
        return $this->hasMany('App\Account');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }


    function getFolioAttribute(){
        return str_pad(  $this->attributes['id'], 6, "0", STR_PAD_LEFT);
    }

}
