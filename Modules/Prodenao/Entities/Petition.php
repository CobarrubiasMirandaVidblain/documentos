<?php

namespace Modules\prodenao\Entities;

use Illuminate\Database\Eloquent\Model;

class Petition extends Model
{
    protected $guarded = [];
    protected $appends = ['folio', 'addressfull', 'namefull'];

    protected $dates = [
        'created_at',
        'updated_at',
        'finish_at'
    ];

    public function subprocuracy()
    {
        return $this->belongsTo('Modules\prodenao\Entities\Subprocuracy');
    }

    /* public function victims()
    {
        return $this->hasMany('App\Victim');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    public function affair()
    {
        return $this->hasOne('App\Affair');
    } */


    function getNameFullAttribute()
    {
        return  $this->attributes['name'] . ' ' .
            $this->attributes['namepaterno'] . ' ' .
            $this->attributes['namematerno'];
    }

    function getContactFullAttribute()
    {
        return  $this->attributes['correo'] . ' ' .
            $this->attributes['numerotelefono'];
    }

    function getAddressFullAttribute()
    {
        return  $this->attributes['addressestado'] . ' ,' .
            $this->attributes['addressmunicipio'] . ' ,' .
            $this->attributes['addresscolonia'] . ' ,' .
            $this->attributes['addresscalle'] . ' #' .
            $this->attributes['addressnumero'];
    }

    function getColoniaAttribute()
    {
        return str_replace(" ", "+", $this->attributes['addresscolonia']);
    }
    function getMunicipioAttribute()
    {
        return str_replace(" ", "+", $this->attributes['addressmunicipio']);
    }

    function getFolioAttribute()
    {
        return str_pad($this->attributes['id'], 6, "0", STR_PAD_LEFT);
    }
}