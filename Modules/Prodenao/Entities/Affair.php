<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affair extends Model
{
    protected $guarded =[];

    public function user()
    {
        return $this->belongsTo('App\User');
    } 
    public function petition()
    {
        return $this->belongsTo('App\Petition');
    } 
    public function advisor()
    {
        return $this->hasOne('App\Advisor');
    } 
    public function mediation()
    {
        return $this->hasOne('App\Mediation');
    }
    public function expedient()
    {
        return $this->hasOne('App\Expedient');
    }

    
    
}
