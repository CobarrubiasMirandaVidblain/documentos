<?php

namespace Modules\prodenao\Entities;

use Illuminate\Database\Eloquent\Model;

class Subprocuracy extends Model
{
    protected $table = 'pdn_subprocuracies';

    protected $guarded = [];

    public function Users()
    {
        return $this->hasMany('App\Models\Usuario');
    }

    public function petitions()
    {
        return $this->hasMany('Modules\prodenao\Entities\Petition');
    }
}