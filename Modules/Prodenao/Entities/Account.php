<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $guarded =[];
    
    public function action()
    {
        return $this->belongsTo('App\Action');
    }
    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    }
}
