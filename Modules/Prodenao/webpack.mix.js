let mix = require('laravel-mix');

mix.js('Resources/src/js/app.js', './../../public/prodena/js')
  .sass('Resources/src/sass/app.scss', './../../public/prodena/css')
  .setPublicPath('./../../public/prodena');
