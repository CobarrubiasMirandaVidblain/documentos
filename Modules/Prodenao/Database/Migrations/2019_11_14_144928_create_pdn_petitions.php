<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdnPetitions extends Migration
{
    public function up()
    {
        Schema::create('pdn_petitions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('via', array('Presencial', 'Telefónica', 'Correo'));
            $table->enum('anonymous', array('true', 'false'))->default('false');

            $table->string('name')->nullable();
            $table->string('namepaterno')->nullable();
            $table->string('namematerno')->nullable();

            $table->string('addressestado')->nullable();
            $table->string('addressmunicipio')->nullable();
            $table->string('addresscolonia')->nullable();
            $table->string('addresscalle')->nullable();
            $table->integer('addressnumero')->nullable();
            $table->integer('addresscodigopostal')->nullable();

            $table->tinyInteger('age')->nullable();
            $table->enum('gender', array('Hombre', 'Mujer', 'otro'))->nullable();

            $table->string('relationship')->nullable();

            $table->string('numerotelefono')->nullable();
            $table->string('correo')->nullable();

            $table->text('description');
            $table->text('conclusion')->nullable();

            $table->text('slug')->nullable();
            $table->integer('assignedaffair')->default('0');

            $table->string('process')->default('sin_asignar');
            $table->boolean('completed')->default('0');

            $table->boolean('changed')->default('0');

            $table->integer('subprocuracy_id')->unsigned()->index()->nullable();
            $table->foreign('subprocuracy_id')->references('id')->on('pdn_subprocuracies');

            $table->timestamp('finish_at')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('');
    }
}