<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExtraDatosPersona extends Model{
  use SoftDeletes;
  
  protected $table = 'extradatospersonas';
  protected $dates = ['deleted_at'];
  protected $fillable=['persona_id','grado','talla','fecha_talla','peso','fecha_peso','tiporopa_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function persona()
  {
      return $this->belongsTo('App\Models\Persona');
  }

  public function get_formato_fecha_peso() {
    return (\DateTime::createFromFormat('Y-m-d', $this->fecha_peso))->format('d/m/Y');
  }

  public function get_formato_fecha_talla() {
    return (\DateTime::createFromFormat('Y-m-d', $this->fecha_talla))->format('d/m/Y');
  }
}
