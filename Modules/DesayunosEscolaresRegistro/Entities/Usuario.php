<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuario extends Model{
  use SoftDeletes;
  
  protected $table = 'peta_instituciones_usuarios';
  protected $dates = ['deleted_at'];
  protected $fillable=['institucion_id','access_usuario_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function usuario()
  {
    return $this->belongsTo('App\Models\Usuario','id','usuario_id');
  }

  public function institucion()
  {
    return $this->belongsTo('App\Models\Instituciones');
  }

  public function accessusuario()
  {
    return $this->belongsTo('App\Models\Usuario','id','access_usuario_id');
  }
}
