<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DesayunosUsuarios extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_desayunos_usuarios';
  protected $dates = ['deleted_at'];
  protected $fillable=['usuario_id','desayuno_id','usuariosis_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function desayuno()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Desayuno');
  }
}
