@extends('desayunosescolaresregistro::layouts.master')

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle','Seguimiento de registro peso y talla')

@section('content')
    <hr>    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 id="progresion" class="box-title">Listado de Escuelas {{$registrados.' de '.$total}}</h3>
                </div>
                <div class="box-body">
                    <input type="checkbox" name="showRecord" id="showRecord"><span> Sólo quienes faltan por registrar</span>
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="search" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                        </span>
                    </div>
                    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'desayunos-table', 'name' => 'desayunos-table', 'style' => 'width: 100%']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-beneficiario">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title-beneficiario">Actualización de datos</h4>
                </div>
                <div class="modal-body" id="modal-body-beneficiario"></div>
                <div class="modal-footer" id="modal-footer-beneficiario">
                    <div class="pull-right">
                        <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="beneficiario.guardar();"><i class="fa fa-save"></i> Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('body')
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jsreport.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } })

        var tabla = ((tablaId) => {
                $('#btn_buscar').keypress(function(e) {
                    if(e.which === 13) {
                        $(tablaId).DataTable().search($('#search').val()).draw()
                    }
                })
                $('#btn_buscar').on('click', function() {
                    $(tablaId).DataTable().search($('#search').val()).draw()
                })
                function recargar(params) {
                    $(tablaId).DataTable().ajax.reload(null,false);
                }

                var aux = $("#showRecord").iCheck({
                    checkboxClass: "icheckbox_flat-orange",
                })

                aux.on("ifToggled", function(event){
                    tabla.recargar()
                })

                return {
                    recargar : recargar
                }
            })('#desayunos-table')

        function recargarGral() {
            $.get('/desayunosescolares/registrados/count', function(data) {
            })
            .done(function(data) {
                console.log(data)
                $("#progresion").text("Listado de Escuelas, "+data.registrados+" de "+data.total+" beneficiarios registrados")
            })
        }
    </script>
    {!! $dataTable->scripts() !!}    
@endpush