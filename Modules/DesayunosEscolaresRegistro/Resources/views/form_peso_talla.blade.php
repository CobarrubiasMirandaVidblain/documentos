<form data-toggle="validator" role="form" id="form-beneficiario" action="{{ isset($beneficiario) ? URL::to('desayunosescolares/beneficiarios') . '/' . $beneficiario->id : URL::to('desayunosescolares/beneficiarios') }}" method="{{ isset($beneficiario) ? 'PUT' : 'POST' }}">
{{--<form id="form-beneficiario">--}}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="nombre">Nombre* :</label>
                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $beneficiario->nombre or '' }}" disabled>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="primer_apellido">Apellido Paterno* :</label>
                <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $beneficiario->primer_apellido or '' }}" disabled>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="segundo_apellido">Apellido Materno* :</label>
                <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $beneficiario->segundo_apellido or '' }}" disabled>
            </div>
        </div>

        {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="genero">Género* :</label>
                <select id="genero" name="genero" class="form-control select2" style="width: 100%;">
                    <option value="M"
                    @if(isset($beneficiario))
                        @if($beneficiario->genero == 'M')
                            selected
                        @endif
                    @else
                        selected
                    @endif
                    >MASCULINO</option>
                    <option value="" {{ (isset($beneficiario) && $beneficiario->genero == 'F') ? 'selected' : '' }}>FEMENINO</option>
                </select>
            </div>
        </div>--}}

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="curp">CURP:</label>
                <input data-inputmask='"mask": "aaaa999999aaaaaa*9"' data-mask type="text" class="form-control mask" id="curp" name="curp" value="{{ $beneficiario->curp or '' }}" title="Se refiere a la Clave Única de Registro de Población (CURP)
                la cual está formada de 18 dígitos,
                ejemplo: LOMV090124MDFRRS04." {{isset($beneficiario->bienestarpersona) && (!empty($beneficiario->curp) && $beneficiario->curp != null) ? 'disabled' : ''}}>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="fecha_nacimiento">Fecha nacimiento* :</label>
                <input type="text" class="form-control datepicker" id="fecha_nacimiento" name="fecha_nacimiento" maxlength="10" value="{{ isset($beneficiario->fecha_nacimiento) ? $beneficiario->fecha_nacimiento : '' }}" {{--isset($beneficiario->fecha_nacimiento) != null ? 'disabled' : '' --}}>
            </div>
        </div>

    </div>

    <h4>Peso y talla:</h4>

    <hr>

    <div class="row" style="padding-left: 0; padding-right: 0;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="grado">Grado* :</label>
                <select type="text" class="form-control select2" id="grado" name="grado" title="Se refiere el grado escolar
al que pertenece el menor que 
deberá escribirse en número
arábigo.">          
                    <option selected value="">Seleccione un grado</option>
                    @for ($i = 1; $i <= 6/*(isset($edad) ? ($edad >= 8 ? 6 : 3 ) : 6)*/; $i++)
                        <option value="{{$i}}" {{isset($beneficiario->extradatos->grado) ? ($beneficiario->extradatos->grado == $i ? 'selected' : '') : ''}}>{{$i}} °grado</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
    
    <div class="row" style="padding-left: 0; padding-right: 0;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="talla">Talla* (en centímetros):</label>
                <input {{--data-inputmask='"mask": "999.9"' data-mask--}} type="text" class="form-control {{--mask--}}" id="talla" name="talla" maxlength="5" value="{{ $beneficiario->extradatos->talla or '' }}" title="Anotar la talla
en centímetros y milímetros,
por ejemplo: 128.5
usando cuatro números,
separados por punto.">
            </div>
        </div>
    
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="fecha_talla">Fecha talla* (última vez que se tomó la talla):</label>
                <input type="text" class="form-control datepicker" id="fecha_talla" name="fecha_talla" maxlength="10" value="{{ $beneficiario->extradatos->fecha_talla or '' }}" title="Anotar la fecha en
que se tomó la última talla; debe
escribirse en el formato: dia/mes/año,
utilizando números: 21/10/2018.">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="peso">Peso* (en kilogramos):</label>
                <input {{--data-inputmask='"mask": "99.999"' data-mask--}} type="text" class="form-control {{--mask--}}" id="peso" name="peso" maxlength="6" value="{{ $beneficiario->extradatos->peso or '' }}" title="Anotar el peso
en kilogramos y cientos de gramos,
por ejemplo: 41.200.">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="fecha_peso">Fecha peso* (última vez que se tomó el peso):</label>
                <input type="text" class="form-control datepicker" id="fecha_peso" name="fecha_peso" maxlength="10" value="{{ $beneficiario->extradatos->fecha_peso or '' }}" title="Anotar la fecha en
que se tomó el último
peso; debe escribirse
en el formato: dia/mes/año,
utilizando números: 21/10/2018.">
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 0; padding-right: 0;">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="tiporopa_id">Ropa* :</label>
                <select type="text" class="form-control select2" id="tiporopa_id" name="tiporopa_id" title="Describir el tipo de ropa con la
que fue pesado (a) el/la niño (a);
1=Ligera, que se refiere a los niños pesados
únicamente con el uniforme; y 2=Gruesa,
que incluye cualquier otro tipo
de prenda extra (Suéter,chamarra,bufanda).">
                    <option selected value="">Seleccione un grado</option>
                    <option value="1" {{isset($beneficiario->extradatos->tiporopa_id) ? ($beneficiario->extradatos->tiporopa_id == 1 ? 'selected' : '') : ''}}>Gruesa</option>
                    <option value="2" {{isset($beneficiario->extradatos->tiporopa_id) ? ($beneficiario->extradatos->tiporopa_id == 2 ? 'selected' : '') : ''}}>Ligera</option>
                </select>
            </div>
        </div>
    </div>
</form>