@extends('vendor.admin-lte.layouts.main')

@if(auth()->check())
  @section('user-avatar')
    @php
        $userimg = auth()->user()->persona->get_url_fotografia();
        $userimg = $userimg != 'images/no-image.png'? $userimg : 'images/serv med/usuario.png';
    @endphp
    {{ asset($userimg) }}
  @endsection
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('sidebar-menu')
    
@endsection
@section('content-title', 'Desayunos Escolares')
@section('content-subtitle','Actualización de peso y talla de beneficiarios')

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
  <script type="text/javascript">
    function block() {
        $.blockUI({
            css: {
                border: 'none',
                padding: '0px',
                backgroundColor: 'none',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#fff'
            },
            baseZ: 10000,
            message: '<div align="center"><img style="width:168px; height:168px;" src="{{ asset('images/preloader/loading.gif') }}"><br /><p style=" color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger; ">Procesando...</p></div>',
        });

        function unblock_error() {
            if($.unblockUI())
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
        }

        setTimeout(unblock_error, 120000);
    }

    function unblock() {
        $.unblockUI();
    }
  </script>
@endpush
