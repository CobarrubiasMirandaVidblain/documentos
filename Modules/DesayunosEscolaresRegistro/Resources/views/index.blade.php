@extends('desayunosescolaresregistro::layouts.master')

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
    <hr>

    <div class="row">
        @php
            $escuela = auth()->user()->desayunousuario->desayuno->escuela;
        @endphp
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Región: </strong> {{$escuela->municipio->distrito->region->nombre or '-----'}} </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Distrito: </strong> {{$escuela->municipio->distrito->nombre or '-----'}} </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Localidad: </strong> {{$escuela->localidad->nombre or '-----'}} </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Municipio: </strong> {{$escuela->municipio->nombre or '-----'}} </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Nombre Escuela: </strong> {{$escuela->nombre or '-----'}} </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Clave Escuela: </strong> {{$escuela->clave or '-----'}} </p>
        </div>
        {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Nivel: </strong> {{auth()->user()->desayunousuario->desayuno->escuela->nivel->nivel or '-----'}} </p>
        </div>--}}
        {{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <p><strong>Tipo Escuela: </strong> ----- </p>
        </div>--}}
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado de Beneficiarios</h3>
                </div>
                <div class="box-body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="search" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                        </span>
                    </div>
                    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'desayunos-table', 'name' => 'desayunos-table', 'style' => 'width: 100%']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-beneficiario">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title-beneficiario">Actualización de datos</h4>
                </div>
                <div class="modal-body" id="modal-body-beneficiario"></div>
                <div class="modal-footer" id="modal-footer-beneficiario">
                    <div class="pull-right">
                        <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="beneficiario.guardar();"><i class="fa fa-save"></i> Guardar</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('body')
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jsreport.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
    <script type="text/javascript" src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- bootstrap datepicker locales -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
    
    <script>
        var tabla
        $(document).ready(function() {
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } })
            
            tabla = ((tablaId) => {
                $('#btn_buscar').keypress(function(e) {
                    if(e.which === 13) {
                        $(tablaId).DataTable().search($('#search').val()).draw()
                    }
                })
                $('#btn_buscar').on('click', function() {
                    $(tablaId).DataTable().search($('#search').val()).draw()
                })
                function recargar(params) {
                    $(tablaId).DataTable().ajax.reload(null,false);
                }
                return {
                    recargar : recargar
                }})('#desayunos-table');
            })
            
            var beneficiario = (function() {
                var modal_beneficiario = $("#modal-beneficiario")
                var modal_title_beneficiario = $("#modal-title-beneficiario")
                var modal_body_beneficiario = $("#modal-body-beneficiario")
                var modal_footer_beneficiario = $("#modal-footer-beneficiario")
                var edad

            var agregar = (id,anios) => {
                block()
                edad = anios
                $.ajax({
                    url: "{{ URL::to('desayunosescolares/beneficiarios') }}"+"/"+id,
                    dataType: 'JSON',     
                    type: 'GET',
                    success: function(response) {
                        // console.log(response) 
                        modal_body_beneficiario.html(response.html)
                        unblock()
                        inicializarModal()
                        modal_beneficiario.modal("show")
                    },
                    error: (res) => {
                        swal(        
                            'Petición cancelada',
                            '',
                            'error'
                        )
                        unblock()
                    }
                })
            }

            function guardar() {
                block()
                validarForm()
                var talla = $("#talla")
                var peso = $("#peso")
                
                if($("#form-beneficiario").valid()) {
                    console.log("edad: "+edad)
                    var t,p
                    if(edad != 0) {
                        console.log("no 0")
                        t = (edad <= 3 && (parseFloat(talla.val()) <= 160.0 && parseFloat(talla.val()) >= 50.0)) ||
                                (edad > 3 && (parseFloat(talla.val()) <= 190.0 && parseFloat(talla.val()) >= 80.0))
                                ? true : false

                        p = (edad <= 3 && (parseFloat(peso.val()) <= 30.0 && parseFloat(peso.val()) >= 8.0)) ||
                                (edad > 3 && (parseFloat(peso.val()) <= 100.0 && parseFloat(peso.val()) >= 14.0))
                                ? true : false
                    } else {
                        console.log("0")
                        t = (parseFloat(talla.val()) <= 190.0 && parseFloat(talla.val()) >= 50.0)
                                ? true : false

                        p = (parseFloat(peso.val()) <= 100.0 && parseFloat(peso.val()) >= 8.0)
                                ? true : false
                    }
                    console.log(t+" "+p)
                    if(t) {
                        if(p) {
                            $("#curp").val($("#curp").val().toUpperCase())
                            $.ajax({
                                url: $("#form-beneficiario").attr('action'),
                                type: $("#form-beneficiario").attr('method'),
                                data: $("#form-beneficiario").serialize(),
                                success: function(response) {     
                                    unblock();
                                    swal(                            
                                        '¡Correcto!',
                                        'Beneficiario actualizado',
                                        'success'
                                    )                                                  
                                    tabla.recargar()
                                },
                                error: function(response){
                                    unblock();
                                    swal(
                                        'Error',
                                        'Intentar nuevamente',
                                        'error'
                                    )               
                                }
                            })
                        } else {
                            unblock()
                            swal(
                                'Datos no creibles en peso',
                                '',
                                'error'
                            )
                        }
                    } else {
                        unblock()
                        swal(        
                            'Datos no creibles en talla',
                            '',
                            'error'
                        )
                    }
                } else {
                    unblock()
                    console.log('mal')
                }
            }

            function inicializarModal() {
                $('.mask').inputmask()

                $('.datepicker').datepicker({
                    autoclose: true,
                    language: 'es',
                    format: 'dd/mm/yyyy',
                    startDate: moment('2000-01-01').format('DD/MM/YYYY'),
                    endDate: moment().format('DD/MM/YYYY')
                })

                if($("#fecha_nacimiento").val() != "")
                    $("#fecha_nacimiento").val(moment($("#fecha_nacimiento").val()).format('DD/MM/YYYY'))
                
                if($("#fecha_talla").val() != "")
                    $("#fecha_talla").val(moment($("#fecha_talla").val()).format('DD/MM/YYYY'))
                
                if($("#fecha_peso").val() != "")
                    $("#fecha_peso").val(moment($("#fecha_peso").val()).format('DD/MM/YYYY'))

            }

            function validarForm() {
                $("#form-beneficiario").validate({
                    rules: {
                        curp: {
                            minlength: 18,
                            maxlength: 18,
                            required: true,
                            pattern_curp: ''
                        },
                        talla: {
                            required: true,
                            number: true,
                            minlength: 3,
                            maxlength: 5
                        },
                        peso: {
                            required: true,
                            number: true,
                            minlength: 2,
                            maxlength: 6
                        },
                        fecha_talla: {
                            required: true,
                            minlength: 10,
                            maxlength: 10,
                            pattern_fecha: ''
                        },
                        fecha_peso: {
                            required: true,
                            minlength: 10,
                            maxlength: 10,
                            pattern_fecha: ''
                        },
                        grado: {
                            required: true
                        },
                        genero: {
                            required: true
                        },
                        ropa: {
                            required: true
                        },
                        fecha_nacimiento: {
                            required: true,
                            minlength: 10,
                            maxlength: 10,
                            pattern_fecha: ''
                        }
                    }
                })

                $.validator.addMethod('pattern_curp', function (value, element) {
                    return this.optional(element) || /^[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/.test(value.toUpperCase());
                }, 'Revise los caracteres y vuelve a intentarlo')

                $.validator.addMethod('pattern_fecha', function (value, element) {
                    return this.optional(element) || /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/.test(value);
                }, 'Revise los caracteres y vuelve a intentarlo')
            }

            return {
                agregar: agregar,
                guardar:guardar
            }
        })()
    </script>
    {!! $dataTable->scripts() !!}    
@endpush