<?php

namespace Modules\DesayunosEscolaresRegistro\Http\Controllers;

use View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Persona;
use App\Models\PersonasPrograma;
use Modules\DesayunosEscolaresRegistro\Entities\Beneficiario;
use Modules\DesayunosEscolaresRegistro\Entities\ExtraDatosPersona;
use App\Models\AniosPrograma;
use Modules\DesayunosEscolaresRegistro\Entities\Usuario;

class BeneficiarioController extends Controller{
  public function __construct(){
    $this->middleware(['auth', 'authorized']);
    //$this->middleware('rolModuleV2:pesotalla,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
  }

  /**
   * Show the form for creating a new resource.
   * @return Response
   */
  public function create()
  {
      
  }

  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   * Agrega un nuevo de beneficiario
   */
  public function store(Request $request)
  {
    try {
        DB::beginTransaction();
          $hereExistsB = Beneficiario::join('personas_programas','personas_programas.id','peta_beneficiarios.persona_programa_id')
                                      ->join('personas','personas.id','personas_programas.persona_id')
                                      ->join('peta_instituciones_usuarios','peta_instituciones_usuarios.id','peta_beneficiarios.institucion_usuario_id')
                                      ->where('peta_instituciones_usuarios.access_usuario_id',auth()->user()->id)
                                      ->where('curp',$request['curp'])
                                      ->first();
          $otherExistsB = Beneficiario::join('personas_programas','personas_programas.id','peta_beneficiarios.persona_programa_id')
                                      ->join('personas','personas.id','personas_programas.persona_id')
                                      ->join('peta_instituciones_usuarios','peta_instituciones_usuarios.id','peta_beneficiarios.institucion_usuario_id')
                                      ->join('cat_instituciones','cat_instituciones.id','peta_instituciones_usuarios.institucion_id')
                                      ->where('curp',$request['curp'])
                                      ->first(['peta_instituciones_usuarios.access_usuario_id']);
          if($hereExistsB)
            abort(409,'Esta persona ya pertenece a este grupo, sólo falta actualizar');
          if($otherExistsB)
            abort(410,'Esta persona ya pertenece a otra institución, espera que le den de baja o comunicate con DIF Oaxaca');
          //Se actualiza o crea una persona
          $request['fecha_nacimiento'] = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
          $data_persona = $request->except(['peso','talla','fecha_peso','fecha_talla']);
          $data_persona['usuario_id'] = auth()->user()->id;
          //validar persona
          $persona = Persona::updateOrCreate([
            'curp'=>$data_persona['curp']
          ],$data_persona);
          //
          //$persona = Persona::updateOrCreate(['curp'=>$request->curp],$data_persona);
          if($persona) {
            auth()->user()->bitacora(request(), [
                'tabla' => 'personas',
                'registro' => $persona->id . '',
                'campos' => json_encode($data_persona) . '',
                'metodo' => request()->method()
            ]);
          }
          $data_persona_programa = [];
          $data_persona_programa['persona_id'] = $persona->id;
          $data_persona_programa['usuario_id'] = auth()->user()->id;
          $data_persona_programa['anios_programa_id'] = AniosPrograma::join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
                                                                      ->where('programa_id',config('desayunosescolaresregistro.programaId'))
                                                                      ->where('anio',\Carbon\Carbon::now()->format('Y'))
                                                                      ->first(['anios_programas.id'])
                                                                      ->id;
          $persona_programa = PersonasPrograma::where('persona_id',$persona->id)
                                              ->where('anios_programa_id',$data_persona_programa['anios_programa_id'])
                                              ->withTrashed()
                                              ->first();
          if($persona_programa) {
            $persona_programa->restore();
          } else {
            $persona_programa = PersonasPrograma::create($data_persona_programa);
          }
          /* $persona_programa = PersonasPrograma::updateOrCreate(['persona_id'=>$persona->id,
                                                                'anios_programa_id'=>$data_persona_programa['anios_programa_id']
                                                              ],$data_persona_programa); */
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas_programas',
              'registro' => $persona_programa->id . '',
              'campos' => json_encode($data_persona_programa) . '',
              'metodo' => request()->method()
          ]);
          $data_beneficiario_peta = [];
          $data_beneficiario_peta['persona_programa_id'] = $persona_programa->id;
          $data_beneficiario_peta['institucion_usuario_id'] = Usuario::where('access_usuario_id',auth()->user()->id)->first()->id;
          $data_beneficiario_peta['usuario_id'] = auth()->user()->id;
          $beneficiario_peta = Beneficiario::where('persona_programa_id',$persona_programa->id)
                                              ->where('institucion_usuario_id',$data_beneficiario_peta['institucion_usuario_id'])
                                              ->withTrashed()
                                              ->first();
          if($beneficiario_peta) {
            $beneficiario_peta->restore();
          } else {
            $beneficiario_peta = Beneficiario::create($data_beneficiario_peta);
          }
          //$beneficiario_peta = Beneficiario::firstOrCreate(['persona_programa_id'=>$persona_programa->id],$data_beneficiario_peta);
          auth()->user()->bitacora(request(), [
              'tabla' => 'peta_beneficiarios',
              'registro' => $beneficiario_peta->id . '',
              'campos' => json_encode($data_beneficiario_peta) . '',
              'metodo' => request()->method()
          ]);
          $dataextra_beneficiario_peta = $request->only(['peso','talla','fecha_talla','fecha_peso','grado','tiporopa_id']);
          $dataextra_beneficiario_peta['fecha_talla'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_peta['fecha_talla'])->format('Y-m-d');
          $dataextra_beneficiario_peta['fecha_peso'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_peta['fecha_peso'])->format('Y-m-d');
          $dataextra_beneficiario_peta['usuario_id'] = auth()->user()->id;
          $dataextra_beneficiario_peta['persona_id'] = $persona->id;
          $extra_datos = ExtraDatosPersona::create(
            $dataextra_beneficiario_peta
          );
          auth()->user()->bitacora(request(), [
              'tabla' => 'extradatospersonas',
              'registro' => $extra_datos->id . '',
              'campos' => json_encode($dataextra_beneficiario_peta) . '',
              'metodo' => request()->method()
          ]);
        DB::commit();
        return response()->json(array('success' => true));
    } catch(\Exception $e) {
        DB::rollBack();
        abort(500,$e->getMessage());
    }
  }

  /**
   * Show the specified resource.
   * @return Response
   */
  public function show($beneficiario,Request $request)
  {
    $match = Persona::find($beneficiario);
    return view::make('asistenciaalimentaria::modal')
                ->with([
                    'vista_beneficiario' => 'desayunosescolaresregistro::beneficiario.peso_talla',
                    'vista' => 'desayunosescolaresregistro::beneficiario.create_edit',
                    'persona' => $match,
                    'tipo'=>$request->get('id'),
                ])
                ->render();
  }

  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit()
  {
      
  }

  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   * Actualiza beneficiario
   */
  public function update($persona_id,Request $request)
  {
    try {
      DB::beginTransaction();
        //Se actualiza
        $request['fecha_nacimiento'] = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
        $data_persona = $request->except(['peso','talla','fecha_peso','fecha_talla']);
        //validar persona
        $persona = Persona::find($persona_id);
        $persona->update($data_persona);
        //
        //$persona = Persona::updateOrCreate(['curp'=>$request->curp],$data_persona);
        if($persona) {
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas',
              'registro' => $persona->id . '',
              'campos' => json_encode($data_persona) . '',
              'metodo' => request()->method()
          ]);
        }
        
        $dataextra_beneficiario_peta = $request->only(['peso','talla','fecha_talla','fecha_peso','grado','tiporopa_id']);
        $dataextra_beneficiario_peta['fecha_talla'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_peta['fecha_talla'])->format('Y-m-d');
        $dataextra_beneficiario_peta['fecha_peso'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_peta['fecha_peso'])->format('Y-m-d');
        $dataextra_beneficiario_peta['usuario_id'] = auth()->user()->id;
        $dataextra_beneficiario_peta['persona_id'] = $persona->id;
        $extra_datos = ExtraDatosPersona::create(
          $dataextra_beneficiario_peta
        );
        auth()->user()->bitacora(request(), [
            'tabla' => 'extradatospersonas',
            'registro' => $extra_datos->id . '',
            'campos' => json_encode($dataextra_beneficiario_peta) . '',
            'metodo' => request()->method()
        ]);
      DB::commit();
      return response()->json(array('success' => true));
    } catch(\Exception $e) {
        DB::rollBack();
        abort(500,$e->getMessage());
    }
  }

  /**
   * Remove the specified resource from storage.
   * @return Response
   */
  public function destroy($beneficiario)
  {
    try {
      DB::beginTransaction();
        $beneficiario_baja = Beneficiario::find($beneficiario);
        //PersonasPrograma::find($beneficiario_baja->persona_programa_id)->delete();
        $beneficiario_baja->delete();
        auth()->user()->bitacora(request(), [
            'tabla' => 'peta_beneficiarios',
            'registro' => $beneficiario_baja->id . '',
            'campos' => '',
            'metodo' => request()->method()
        ]);
      DB::commit();
      return response()->json(array(200,'success'));
    } catch(\Exception $e) {
      DB::rollBack();
      abort(500,$e->getMessage());
    }
  }
  
  //Autocomplete de un beneficiario
  public function find($id) {
    $xt = ExtraDatosPersona::where('persona_id',$id)->first();
    $persona = null;
    if($xt)
      $persona = Persona::join('extradatospersonas','extradatospersonas.persona_id','personas.id')
                        ->where('personas.id',$id)
                        ->orderBy('extradatospersonas.created_at','desc')
                        ->first();
    else
      $persona = Persona::find($id);
    return
        [
            'persona'=>$persona,
            'mun'=>$persona->municipio->nombre,
            'loc'=>$persona->localidad ? $persona->localidad->nombre : 'No registrada',
            'etn'=>$persona->etnia->nombre,
            'bienestar'=> (Persona::join('bienestarpersonas','bienestarpersonas.persona_id','personas.id')
                                  ->where('persona_id',$id)->whereNull('bienestarpersonas.deleted_at')->first() ? 1 : 0)
        ];
  }
}
