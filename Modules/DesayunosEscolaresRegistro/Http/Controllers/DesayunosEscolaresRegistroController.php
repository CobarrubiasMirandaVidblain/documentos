<?php

namespace Modules\DesayunosEscolaresRegistro\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Modules\DesayunosEscolaresRegistro\DataTables\EscuelasDesayunosFrios;
use Modules\DesayunosEscolaresRegistro\DataTables\ConsultaRegistro;

use App\Models\Persona;
use Modules\DesayunosEscolaresRegistro\Entities\ExtraDatosPersona;
use Modules\DesayunosEscolaresRegistro\Entities\Beneficiario;

class DesayunosEscolaresRegistroController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        //$this->middleware(['auth', 'authorized', 'roles:ADMINISTRADOR,CAPTURISTA']);
        //$this->middleware('rolModuleV2:desayunosescolares,ADMINISTRADOR', ['only' => ['show', 'index']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(EscuelasDesayunosFrios $dataTable)
    {
        return $dataTable->render('desayunosescolaresregistro::newindex');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(ConsultaRegistro $dataTable)
    {
        $total = Beneficiario::join('peta_instituciones_usuarios','peta_instituciones_usuarios.id','peta_beneficiarios.institucion_usuario_id')
                            ->count();
        $registrados  = ExtraDatosPersona::groupBy('persona_id')->count();
        return $dataTable->render('desayunosescolaresregistro::newshow',['total' => $total,'registrados'=>$registrados]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($persona_id)
    {
        $beneficiario = Persona::findOrFail($persona_id);
        return response()->json([
            'html' =>  View::make('desayunosescolaresregistro::form_peso_talla')
            ->with('beneficiario',$beneficiario)
            ->with('edad',($beneficiario->fecha_nacimiento ? (\Carbon\Carbon::parse($beneficiario->fecha_nacimiento))->age : 0))
            ->render() ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('desayunosescolaresregistro::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($beneficiario, Request $request)
    {
        $persona_update = Persona::findOrFail($beneficiario);
        //$extradatos_persona_update = ExtraDatosPersona::where('persona_id',$beneficiario)->first();
        try {
            DB::beginTransaction();
                $request['usuario_id'] = auth()->user()->id;
                //$request['tiporopa_id'] = $request->ropa;
                
                if(isset($request->curp) || isset($request->fecha_nacimiento)) {
                    if(isset($request->curp))
                        $persona_update->curp = $request->curp;
                    if(isset($request->fecha_nacimiento))
                        $persona_update->fecha_nacimiento = Carbon::createFromFormat('d/m/Y',$request->input('fecha_nacimiento'))->format('Y-m-d');
                    $persona_update->save();
                }
                
                $request['fecha_talla'] = Carbon::createFromFormat('d/m/Y',$request->input('fecha_talla'))->format('Y-m-d');
                $request['fecha_peso'] = Carbon::createFromFormat('d/m/Y',$request->input('fecha_peso'))->format('Y-m-d');
                $data = $request->except(['curp']);

                ExtraDatosPersona::updateOrCreate(
                    ['persona_id' => $beneficiario],
                    $data
                );
                // if($extradatos_persona_update) {
                    //$extradatos_persona_update->updateOrCreate($data);
                // } else {
                //     ExtraDatosPersona::firstOrCreate($data);
                // }
            DB::commit();
            return response()->json(array('success' => true));
        } catch(\Exception $e) {
            DB::rollBack();
            abort(500,$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function getRegistro()
    {
        $total = Beneficiario::join('peta_instituciones_usuarios','peta_instituciones_usuarios.id','peta_beneficiarios.institucion_usuario_id')
                            ->count();
        $registrados = Beneficiario::join('personas_programas','personas_programas.id','peta_beneficiarios.persona_programa_id')
                                    ->join('personas','personas.id','personas_programas.persona_id')
                                    ->join('peta_instituciones_usuarios','peta_instituciones_usuarios.id','peta_beneficiarios.institucion_usuario_id')
                                    ->join('extradatospersonas','extradatospersonas.persona_id','personas.id')
                                    ->count();
        return response()->json(array('total' => $total,'registrados'=>$registrados));
    }
}
