@extends('sosdif::layouts.master')

@section('content-title', 'Actualización de Servicios')
@section('content-subtitle', 'Mostrar Servicios')

@section('content')

<div class="row">
	<div class="col-md-6">
          <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
            	<h3 class="box-title">ACTUALIZAR SERVICIOS</h3>
            </div>
            @if(isset($status))
            <div class="alert alert-success">{{$status}}</div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            @if(isset($msg))
            	<h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{url('sosdif/servicios/mod/' . $servicio->id)}}" method="post"> <!--es mejor utilizar una url por que es un modulo el que se esta utilizando-->
            	{{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->
              {{ method_field('PUT')}}
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Número de servicio</label>
                  <input value="{{$servicio->numero_servicio}}" readonly name="numero_servicio" type="number" class="form-control" id="exampleInputEmail1" placeholder="Número de servicio" value="{{$servicio->numero_servicio}}">
                
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nombre</label>
                  <input value="{{$servicio->nombre}}" name="nombre" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre" value="{{$servicio->nombre}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Tipo de servicio</label>
                  <select value="{{$servicio->tiposervicio_id}}" name="tiposervicio_id" type="number" id="exampleInputFile">
                    @foreach($tipos as $tipo)
                    <option value="{{$tipo->id}}">
                      {{$tipo->tipo}}
                    </option>
                    @endforeach
                  </select>

                 
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Modificar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>

@stop <!--fin del contenido-->
