@extends('sosdif::layouts.master')

@section('content-title', 'Servicios')
@section('content-subtitle', 'Mostrar Servicios')

@section('content')

@if(auth()->user()->sosdif())
<div class="row">
	<div class="col-md-6">
          <!-- general form elements -->
          @if (session('status'))
             <div class="alert alert-success">
             {{ session('status') }}
              </div>
            @endif
        <div class="box box-primary">
            <div class="box-header with-border">
            	<h3 class="box-title">INSERTAR SERVICIOS</h3>

            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            
            @if(isset($msg))
            	<h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{ route('sscorrectivo') }}" method="post">
            	{{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Número de servicio</label>
                  <input value="{{++$ultimonumero}}" readonly name="numero_servicio" type="number" class="form-control" id="exampleInputEmail1" placeholder="Número de servicio">
                
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nombre</label>
                  <input name="nombre" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre">
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Tipo de servicio</label>
                  <select name="tiposervicio_id" type="number" id="exampleInputFile">
                    @foreach($tipos as $tipo)
                    <option value="{{$tipo->id}}">
                      {{$tipo->tipo}}
                    </option>
                    @endforeach
                  </select>

                 
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>
@endif




 <section class="content-header">
      <h1>
        Lista de los Servicios existentes
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Datos de la tabla</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Numero de servicio</th>
                  <th>Nombre</th>
                  <th>Tipo de servicio</th>
                  @if(auth()->user()->sosdif())
                  <th>Acciones</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                  @foreach($servicios as $servicio)
                <tr>
                  <td>{{$servicio->numero_servicio}}</td>
                  <td>{{$servicio->nombre}}</td>
                  <td>{{$servicio->tipossoporteservicios->tipo}}</td>


                 @if(auth()->user()->sosdif())
                  <td>
                     <form role="form" action="{{ route('sscorrectivo') }}" method="post">
                     </form>

                    <a class="btn btn-success" href="edit/{{$servicio->id}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="#" role="button" onclick="event.preventDefault(); document.getElementById('delete-servicio').submit();"><i class="fa fa-trash-o"></i></a> <!--forma para eliminar un registro y que sea de forma segura-->
                    <form action="{{url('sosdif/servicios/borra/' . $servicio->id)}}" style="display: none;" id="delete-servicio" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                    </form>
                   </td> 
                   @endif


                </tr>
                @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>



@stop <!--fin del contenido-->

