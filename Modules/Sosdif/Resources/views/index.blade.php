@extends('sosdif::layouts.master')

@section('content-title', 'Home')
@section('content-subtitle', 'Dashboard')

@section('content')


@if(auth()->user()->hasRoles(['SUPERADMINISTRADOR']))
<h1>{{auth()->user()->usuario}}</h1>

@else
<h1>Usuario Invitado</h1>
@endif

@stop <!--fin del contenido-->
