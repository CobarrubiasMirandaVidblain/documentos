@extends('sosdif::layouts.master')

@section('content-title', 'Actualizacion de Equipos')
@section('content-subtitle', 'Mostrar Equipos')

@section('content')

<div class="row">
	<div class="col-md-6">
          <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
            	<h3 class="box-title">ACTUALIZAR EQUIPOS DEL CATALOGO</h3>
            </div>
             @if(isset($status))
            <div class="alert alert-success">{{$status}}</div>
            @endif
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            @if(isset($msg))
            	<h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{url('sosdif/inventario/modificar/' . $equipo->id)}}" method="post">
            	{{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->
              {{ method_field('PUT')}}
              <div class="box-body">
                 <div class="form-group">
                  <label for="exampleInputPassword1">Nombre</label>
                  <input value="{{$equipo->nombre}}" name="nombre" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre del equipo" value="{{$equipo->nombre}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Numero de serie</label>
                  <input value="{{$equipo->num_serie}}" name="num_serie" type="text" class="form-control" id="exampleInputPassword1" placeholder="Numero de serie" value="{{$equipo->num_serie}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Numero de inventario</label>
                  <input value="{{$equipo->num_inventario}}" name="num_inventario" type="text" class="form-control" id="exampleInputPassword1" placeholder="Numero de inventario" value="{{$equipo->num_inventario}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Direccíon IP</label>
                  <input value="{{$equipo->direccion_ip}}" name="direccion_ip" type="text" class="form-control" id="exampleInputPassword1" placeholder="Direccion IP" value="{{$equipo->direccion_ip}}">
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Capacidad</label>
                  <input value="{{$equipo->capacidad}}" name="capacidad" type="text" class="form-control" id="exampleInputPassword1" placeholder="Capacidad del equipo" value="{{$equipo->capacidad}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Marca</label>
                  <select value="{{$equipo->marca_id}}" name="marca_id" type="number" class="form-control" id="exampleInputFile">
                  @foreach($marcas as $marca)
                  <option value="{{$marca->id}}">
                    {{$marca->nombre}}
                  </option>
                  @endforeach
                </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Modelo</label>
                  <select value="{{$equipo->modelo_id}}" name="modelo_id" type="number" class="form-control" id="exampleInputPassword1">
                @foreach($modelos as $modelo)
                <option value="{{$modelo->id}}">
                  {{$modelo->nombre}}
                </option>
                @endforeach
              </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tipo de Equipo</label>
                  <select value="{{$equipo->tipoequipo_id}}" name="tipoequipo_id" type="number" class="form-control" id="exampleInputPassword1">
                @foreach($tipos as $tipo)
                <option value="{{$tipo->id}}">
                  {{$tipo->tipo}}
                </option>
                @endforeach
              </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Resguardo</label>
                  <input value="{{$equipo->resguardo_id}}" name="resguardo_id" type="text" class="form-control" id="exampleInputPassword1" placeholder="Resguardo de equipo" value="{{$equipo->resguardo_id}}">
                </div>  
              </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Modificar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>
@stop