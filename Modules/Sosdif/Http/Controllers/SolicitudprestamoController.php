<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\SoporteSolicitudes;
use \App\Models\Empleado;
use \App\Models\TiposSoporteServicios;
use \App\Models\SoporteServicios;
use \App\Models\Equipos;



class SolicitudprestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tipos=TiposSoporteServicios::all();
        $solicitudes=SoporteSolicitudes::where('solicitante_id', auth()->user()->persona->empleado->id)->get();
        $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
        return view('sosdif::solicitudes.sprestamo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos,  'ultimonumero'=>$ultimonumero[0]->numero_solicitud]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $tipos=TiposSoporteServicios::all();
        $solicitudes=SoporteSolicitudes::all();
        $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
        
        if(($ultimonumero[0]->numero_solicitud+1)!=$request->numero_solicitud){
            $solicitudes = SoporteSolicitudes::all();

        return view('sosdif::solicitudes.sprestamo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'msg' => 'El numero de la solicitud es incorrecto']);
        }

    $solicitud=SoporteSolicitudes::create([ 
            'numero_solicitud' => $request->numero_solicitud,
            'solicitante_id' => $request->solicitante_id,
            'servicio_id' => $request->servicio_id,
            'equipo_id' => $request->equipo_id,
            'fecha_inicio' => $request->fecha_inicio,
            'fecha_fin' => $request->fecha_fin,
            'asunto' => $request->asunto]);
        $msg = '';
        if(isset($solicitud->id)){
            $msg = 'Se guardo la solicitud';
        }else{
            $msg = 'Ocurrio un error';
        }
        $solicitudes = SoporteSolicitudes::all();
        return view('sosdif::solicitudes.sprestamo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'msg' => 'El numero de la solicitud es incorrecto', 'msg' => $msg]);

     }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('sosdif::show');
        dd('Mostrando el detalle de la solicitud: '.$id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function preEdit($id)
    {
       // return view('sosdif::edit');
        $tipos=TiposSoporteServicios::all();
        $solicitud=SoporteSolicitudes::where('id', $id)->first();

        return view('sosdif::solicitudes.sprestamo', ['solicitud'=>$solicitud, 'tipos'=>$tipos]);
   
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $nuevoNumerosolicitud=$request->input('numero_solicitud');
        $nuevoSolicitante=$request->input('solicitante_id');
        $nuevoServicio=$request->input('servicio_id');
        $nuevoEquipo=$request->input('equipo_id');
        $nuevofechainicio=$request->input('fecha_inicio');
        $nuevofechafin=$request->input('fecha_fin');
        $nuevoAsunto=$request->input('asunto');

        $solicitud= SoporteSolicitudes::findOrFail($id);

        $solicitud->numero_solicitud=$nuevoNumerosolicitud;
        $solicitud->solicitante_id=$nuevoSolicitante;
        $solicitud->servicio_id=$nuevoServicio;
        $solicitud->equipo_id=$nuevoEquipo;
        $solicitud->fecha_inicio=$nuevofechainicio;
        $solicitud->fecha_fin=$nuevofechafin;
        $solicitud->asunto=$nuevoAsunto;
        $solicitud->save();

        $tipos=TiposSoporteServicios::all();

        //$sequipos=SoporteSolicitudesEquipos::all();
        return view('sosdif::solicitudes.peditar', ['solicitud'=>$solicitud, 'tipos'=>$tipos, 'status' => 'Actualizado correctamente']);



    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $solicitud = SoporteSolicitudes::findOrFail($id);
        $solicitud->delete();
        return redirect()->action('\Modules\Sosdif\Http\Controllers\SolicitudprestamoController@index')->with('status', 'Solicitud eliminada!');
   
    }
    public function buscarequipo(Request $request){

        $equipos=Equipos::where('num_serie', 'LIKE', '%'.$request->search.'%')->get();
        return response()->json($equipos);
        // '%'. comodin se utilizan para que al momento de hacer la busqueda regrese los valores que coincidan, aunque tenga mas datos antes o despues de los valores..

    }

    
}
