<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\SoporteSolicitudesEquipos;
use \App\Models\SoporteSolicitudes;
use \App\Models\Inventario;
use \App\Models\TiposSoporteServicios;
use \App\Models\Empleado;
use \App\Models\TiposEquipos;
use \App\Models\Equipos;
use DB;
class SolicitudequipoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $cat_equipos=Equipos::all();
        $tipos=TiposSoporteServicios::all();
        $soliEquipos=SoporteSolicitudesEquipos::all();
        $solicitudes=SoporteSolicitudes::where('servicio_id','!=', 4)->get();
        $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
        return view('sosdif::solicitudes.sequipo', ['solicitudes'=>$solicitudes, 'soliEquipos'=>$soliEquipos, 'tipos'=>$tipos,  'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'cat_equipos'=>$cat_equipos]);
    }
     public function buscar($id)
    {
    
       
        $solicitudes=SoporteSolicitudes::find($id);
       
        

        return response()->json($solicitudes);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    //     $tipos=TiposSoporteServicios::all();
    //     $solicitudes=SoporteSolicitudes::all();
    //     $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
        
    //     if(($ultimonumero[0]->numero_solicitud+1)!=$request->numero_solicitud){
    //         $solicitudes = SoporteSolicitudes::all();

    //     return view('sosdif::solicitudes.sequipo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'msg' => 'El numero de la solicitud es incorrecto']);
    //     }

    // $solicitud=SoporteSolicitudes::create([ 
    //         'numero_solicitud' => $request->numero_solicitud,
    //         'solicitante_id' => $request->solicitante_id,
    //         'servicio_id' => $request->servicio_id,
    //         'equipo_id' => $request->equipo_id,
    //         'fecha_inicio' => $request->fecha_inicio,
    //         'fecha_fin' => $request->fecha_fin,
    //         'asunto' => $request->asunto,
    //         'tecnico_id' => $request->tecnico_id,
    //         'estado_equipo' => $request->estado_equipo]);
    //     $msg = '';
    //     if(isset($solicitud->id)){
    //         $msg = 'Se guardo la solicitud';
    //     }else{
    //         $msg = 'Ocurrio un error';
    //     }
    //     $solicitudes = SoporteSolicitudes::all();
    //     return view('sosdif::solicitudes.sequipo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'msg' => 'El numero de la solicitud es incorrecto', 'msg' => $msg]);
        $fila = SoporteSolicitudesEquipos::where('solicitud_id', $request->numero_solicitud)->take(1)->get();
        if($fila->count() > 0){
            $fila[0]->tecnico_id = $request->tecnico_id;
            $fila[0]->estado_equipo = $request->estado_equipo;
            $fila[0]->observaciones = $request->observaciones;
            $fila[0]->save();
        }else{
            $SoporteSolicitudesEquipos= SoporteSolicitudesEquipos::create([
                'solicitud_id'=>$request->numero_solicitud,
                'tecnico_id'=>$request->tecnico_id,
                'estado_equipo'=>$request->estado_equipo,
                'observaciones'=>$request->observaciones,
            ]);    
        }
        
        $cat_equipos=Equipos::all();
        $tipos=TiposSoporteServicios::all();
        $solicitudes=SoporteSolicitudes::where('servicio_id','!=', 4)->get();
        $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
         return view('sosdif::solicitudes.sequipo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos,  'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'cat_equipos'=>$cat_equipos]);
   

     }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('sosdif::show');
        dd('Mostrando el detalle de la solicitud: '.$id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function preEdit($id)
    {
       // return view('sosdif::edit');
        $tipos=TiposSoporteServicios::all();
        $solicitud=SoporteSolicitudes::where('id', $id)->first();

        return view('sosdif::solicitudes.sequipo', ['solicitud'=>$solicitud, 'tipos'=>$tipos]);
   
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $nuevoNumerosolicitud=$request->input('numero_solicitud');
        $nuevoSolicitante=$request->input('solicitante_id');
        $nuevoServicio=$request->input('servicio_id');
        $nuevoEquipo=$request->input('equipo_id');
        $nuevofechainicio=$request->input('fecha_inicio');
        $nuevofechafin=$request->input('fecha_fin');
        $nuevoAsunto=$request->input('asunto');
        $nuevoTecnico=$request->input('tecnico_id');
        $nuevoEstado=$request->input('estado_equipo');

        $solicitud= SoporteSolicitudes::findOrFail($id);

        $solicitud->numero_solicitud=$nuevoNumerosolicitud;
        $solicitud->solicitante_id=$nuevoSolicitante;
        $solicitud->servicio_id=$nuevoServicio;
        $solicitud->equipo_id=$nuevoEquipo;
        $solicitud->fecha_inicio=$nuevofechainicio;
        $solicitud->fecha_fin=$nuevofechafin;
        $solicitud->asunto=$nuevoAsunto;
        $solicitud->save();

        $soequipo= SoporteSolicitudes_equipos::findOrFail($id);
        $soequipo->tecnico_id=$nuevoTecnico;
        $soequipo->estado_equipo=$nuevoEstado;
        $soequipo->save();

        $tipos=TiposSoporteServicios::all();

        //$sequipos=SoporteSolicitudesEquipos::all();
        return view('sosdif::solicitudes.eeditar', ['solicitud'=>$solicitud, 'tipos'=>$tipos, 'status' => 'Actualizado correctamente']);



    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $solicitud = SoporteSolicitudes::findOrFail($id);
        $solicitud->delete();
        return redirect()->action('\Modules\Sosdif\Http\Controllers\SolicitudequipoController@index')->with('status', 'Solicitud eliminada!');
   
    }

    public function buscartecnico(Request $request){

        $personas=Empleado::select('empleados.id', 'personas.nombre')->join('personas', 'persona_id', '=', 'personas.id')->where('nombre', 'LIKE', '%'.$request->search.'%')->get();
        return response()->json($personas);
        // se hace la relación de la tabla empleado y persona, el empleado con su id y la persona porque tiene los campos que son el nombre//
        // '%'. comodin se utilizan para que al momento de hacer la busqueda regrese los valores que coincidan, aunque tenga mas datos antes o despues de los valores..

    }
}
