<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SosdifController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['only' => ['index']]);
        $this->middleware('roles:SUPERADMINISTRADOR,CAPTURISTA', ['only' => ['index']]);
    }
    /*public function __construct(){
        $this->middleware('auth', ['only' => ['index']]);
    }*/
    /*public function __construct(){
        $this->middleware('auth', ['except' => ['index']]);
        $this->middleware('auth', ['only' => ['index', 'create']]);
        formas para proteger las funciones, el except es todos excepto los que esten escritos, y el only protege solo los que estan en el arreglo.
    }*/

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       return view('sosdif::index');
       // return 'Hola! desde el controlador';
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sosdif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sosdif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
/* EJEMPLO DE LA CREACION DE UNA RUTA
    public function contacto(){
        return 'MINERVA PEREZ MARIN';
    }
*/

}
