<?php

Route::group(['middleware' => 'web', 'prefix' => 'sosdif', 'namespace' => 'Modules\Sosdif\Http\Controllers'], function()
{
    Route::get('/', 'SosdifController@index')->name('sinicio');
    /*EJEMPLO DE UNA RUTA*/
   /* Route::get('contacto', 'SosdifController@contacto');*/
   /* Route::get('/', function (){
    	return view('auth.login');
    });*/
     Route::get('servicios/correctivo', 'ServicioscController@index')->name('scorrectivo');
     Route::post('correctivo', 'ServicioscController@store')->name('sscorrectivo');
     Route::get('servicios/edit/{id}', 'ServicioscController@preEdit');
     Route::put('servicios/mod/{id}', 'ServicioscController@update');
     Route::delete('servicios/borra/{id}', 'ServicioscController@destroy');

/* las rutas pueden quedar igual solo con un metodo diferente como post, delete, get*/
	 /*Route::get('servicios/preventivo', 'ServiciospController@index')->name('spreventivo');
	 Route::post('servicios', 'ServiciospController@store')->name('sserviciosp');

     Route::get('servicios/instalacion', 'InstalacionController@index')->name('sinstalacion');
     Route::post('instalacion', 'InstalacionController@store')->name('ssinstalacion');

     Route::get('servicios/prestamo', 'PrestamoController@index')->name('sprestamo');
     Route::post('prestamo', 'PrestamoController@store')->name('ssprestamo');
     */
	 
     Route::get('inventario/equipo', 'EquiposController@index')->name('iequipo');
	 Route::post('equipos', 'EquiposController@store')->name('iequipos');
     Route::get('inventario/editar/{id}', 'EquiposController@preEdit');
     Route::put('inventario/modificar/{id}', 'EquiposController@update');
     Route::delete('inventario/borrar/{id}', 'EquiposController@destroy');
    Route::get('inventario/select', 'EquiposController@buscarempleado')->name('empleados.select');
    Route::get('personas', '\App\Http\Controllers\PersonaController@buscarpersona')->name('empleados.buscar');

     /*Route::get('inventario/dispositivo', 'DispositivosController@index')->name('idispositivo');
     Route::post('dispositivos', 'DispositivosController@store')->name('idispositivos');
     Route::get('inventario/edit/{id}', 'DispositivosController@preEdit');
     Route::put('inventario/mod/{id}', 'DispositivosController@update');
     Route::delete('inventario/borra/{id}', 'DispositivosController@destroy');
     */

     /*Route::get('solicitudes/solicitud', 'SolicitudController@index')->name('solicitud');
     Route::post('solicitudes', 'SolicitudController@store')->name('ssolicitudes');*/


     Route::get('equipo/sequipo', 'SolicitudequipoController@index')->name('soliequipo');
     Route::post('sequipos', 'SolicitudequipoController@store')->name('ssequipos');
     Route::get('solicitudes/buscar/{id}', 'SolicitudequipoController@buscar');
     Route::get('solicitudes/edit/{id}', 'SolicitudequipoController@preEdit');
     Route::put('solicitudes/mod/{id}', 'SolicitudequipoController@update');
     Route::delete('solicitudes/borra/{id}', 'SolicitudEquipoController@destroy');
    /* Route::get('solicitudes/select', 'SolicitudEquipoController@buscartecnico')->name('tecnicos.select');*/
     Route::get('tecnicos', 'SolicitudequipoController@buscartecnico')->name('empleados.buscar');
   /* Route::get('equipos/select', 'SolicitudequipoController@buscarequipo')->name('equipos.select');*/


     Route::get('equipo/stecnico', 'SotecnicosController@index')->name('solitecnico');
     Route::post('stecnicos', 'SotecnicosController@store')->name('sstecnicos');
    
    

     Route::get('prestamo/sprestamo', 'SolicitudprestamoController@index')->name('soliprestamo');
     Route::post('sprestamos', 'SolicitudprestamoController@store')->name('spequipos');
     Route::get('solicitudes/editar/{id}', 'SolicitudprestamoController@preEdit');
     Route::put('solicitudes/modificar/{id}', 'SolicitudprestamoController@update');
     Route::delete('solicitudes/borrar/{id}', 'SolicitudprestamoController@destroy');
     Route::get('equipos/select', 'SolicitudprestamoController@buscarequipo')->name('equipos.select');
});

