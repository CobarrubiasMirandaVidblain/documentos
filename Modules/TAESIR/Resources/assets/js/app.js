// window.Vue = require('vue');
import Vue from 'vue'
import VueRouter from 'vue-router'
import VueTables from 'vue-tables-2';
import VeeValidate from 'vee-validate';
import 'vue-select/dist/vue-select.css';
import vSelect from 'vue-select'
import IEcharts from 'vue-echarts-v3/src/full.js';
import Datepicker from 'vue2-datepicker';



//require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
//require('@coreui/coreui/dist/js/coreui.min.js');
//vue.use(IEcharts);
Vue.use(VueRouter);
Vue.use(VueTables.ClientTable);
Vue.use(VeeValidate);
Vue.use(Datepicker);

Vue.component('v-select', vSelect)



//seccion para importar componentes
import Home from './components/Home.vue';
import Herramientas from './components/Herramientas/herramientas.vue';
import Materiales from './components/Materiales/materiales.vue';
import ApoyoFuncional from './components/ApoyosFuncionales/apoyofuncional.vue';
import CrearApoyo from './components/ApoyosFuncionales/crearApoyo.vue';
import Diagnostico from './components/Servicios/Reparaciones/Diagnostico.vue';
import Beneficiario from './components/Servicios/Reparaciones/ReparacionTest.vue';
import ReparacionListado from './components/Servicios/Reparaciones/ReparacionListado.vue';
import Donativos from './components/Servicios/Donativos/Donativos.vue';
import PrestamosNuevo from './components/Servicios/Prestamos/PrestamosNuevo.vue';
import CatalogoAF from  './components/ApoyosFuncionales/catalogoAF.vue';
import CrearDonativos from './components/Servicios/Donativos/CrearDonativos.vue';
import Prestamo from './components/Servicios/Prestamos/PrestamosLista.vue';
import error404 from './components/404/error404.vue';
import agregarServicios from './components/Servicios/Reparaciones/agregarServicios.vue'
import RegistrarDonativos from './components/Servicios/Donativos/RegistrarDonativos.vue'
import Requisiciones from './components/Requisiciones/Requisiciones.vue'
import ListaRequisiciones from './components/Requisiciones/ListadoRequisiciones.vue'
import Reportes from './components/Reportes/Reportes2.vue'
// Vue.component('example-component', require('./components/Home.vue').default);
import descontarMateriales from './components/Servicios/Reparaciones/descontarMateriales.vue';
import donmecanico from './components/Servicios/DonMecanico/DonMecanico.vue';

const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/herramientas',
      name: 'herramientas',
      component: Herramientas
    },
    {
      path: '/materiales',
      name: 'materiales',
      component: Materiales
    },
    {
      path: '/apoyofuncional',
      name: 'apoyofuncional',
      component: ApoyoFuncional
    },
    {
      path: '/crearapoyo',
      name: 'crearapoyo',
      component: CrearApoyo
    },
    {
      path: '/diagnostico',
      name: 'diagnostico',
      component: Diagnostico
    },
    {
      path: '/servicios/reparacion/nuevo',
      name: 'reparacion',
      component: Beneficiario
    },
    {
      path: '/servicios/reparacion/listado',
      name: 'listado',
      component: ReparacionListado

    },
    {
      path:'/servicios/donativos',
      name:'donativos',
      component: Donativos
    },
    {
      path: '/servicios/prestamos/nuevo',
      name: 'prestamonuevo',
      component: PrestamosNuevo
    },
    {
      path: '/catalogoapoyosfuncionales',
      name: 'catalogoapoyosfuncionales',
      component: CatalogoAF
    },
    {
      path:'/creardonativos',
      name:'creardonativos',
      component:CrearDonativos
    },
    {
      path: '/servicios/prestamos/lista',
      name: 'prestamolista',
      component: Prestamo
    },
    {
      path: '/servicios/reparacion/agregarservicios/:id',
      name: 'agregarservicio',
      component: agregarServicios
    },
    {
      path: '/servicios/reparacion/descontarmaterial/:id',
      name: 'descontarMateriales',
      component: descontarMateriales
    },
    {
      path: '/error404',
      name: 'error',
      component: error404
    },
    { path: '*', redirect: '/error404' },
    {
      path:'/servicios/donativos/registrardonativos/:id',
      name:'registrardonativos',
      component:RegistrarDonativos
    },
    {
      path:'/requisiciones/vista/:id',
      name: 'requisiciones',
      component: Requisiciones
    },
    {
      path:'/requisiciones/lista',
      name: 'listarequisiciones',
      component: ListaRequisiciones
    },
    {
      path: '/reportes',
      name: 'reportes',
      component: Reportes
    },
    {
      path: '/donmecanicoweb',
      name: 'donmecanico',
      component: donmecanico
    }
  ],
});

const app = new Vue({
  el: '#app',
  // components: {  },
  router,
});