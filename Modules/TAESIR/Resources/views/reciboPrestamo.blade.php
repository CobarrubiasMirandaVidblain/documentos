<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Recibo Prestamo</title>
<script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\echarts.min.js') }}"></script>
<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>

  <table width="100%">
    <tr>
    <td align="bottom"><img src={{asset('images/logo_header.png')}} alt="" width="150"/></td>
        <td align="right">
            <?php
  setlocale (LC_TIME, "es_MX");
  $fecha= strtoupper(strftime("%d de %B de %Y"));
  ?>
            <h3>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</h3>
            <pre>
                TALLER DE ARMADO Y ENSAMBLADO DE SILLA DE RUEDAS
                DIRECCION DE OPERACIÓN DE BIENESTAR
                ORDEN DE REPARACIÓN
                OAXACA DE JUÁREZ A {{$fecha}}
                FOLIO: {{$folio}}
            </pre>
        </td>
    </tr>

  </table>

  <p style="font-size:12px; LINE-HEIGHT:20px; text-align: justify;">
      POR MEDIO DE LA PRESENTE SE HACE CONSTAR QUE EL BENEFICIARIO <strong> {{$persona}}</strong> RECIBIO POR PARTE DEL
      TALLER DE ARMADO Y ENSAMBLADO DE SILLAS DE RUEDAS (TAESIR) UN(A) <strong>{{$apoyo_funcional}}</strong>, COMPROMETIENDOSE
      A HACER UN CORRECTO USO DEL MISMO Y ENTREGARLO ANTES DEL <strong>{{$fecha_devolucion}}.</strong>
    ACORDANDO QUE DE NO CUMPLIRSE CON LO ESTABLECIDO SERA MERECEDOR A <strong>UNA PENALIZACIÓN.</strong>
  </p>
<br/>
<br/>
<table width="100%">
    <tr>
        <td><strong>AUTORIZÓ: </strong> RICARDO CRUZ ILESCAS</td>
        <td align="right"><strong>BENEFICIARIO: </strong>{{$persona}}</td>
        <td align="right"><strong >FIRMA: </strong>___________________</td>
    </tr>

  </table>

</body>

</html>