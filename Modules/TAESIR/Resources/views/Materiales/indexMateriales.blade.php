@extends('taesir::layouts.master')
@section('mystyles')
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
</style>
@endsection

@section('content')
    <div id="app">
        <transition name="fade" mode="out-in">
            <router-view></router-view>
        </transition>
    </div>
@stop


@section('miscript')
@include('taesir::blockUI.blockuiComponent')
@include('taesir::Materiales.Componentes.componenteMateriales')

<script src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ asset('js/vue-router.js') }}"></script>


<script>
const routes = [
    { path: '/materiap', component: Formulario },
    { path: '/', component: ListaMateriales},
];
const router = new VueRouter({
  routes
});
const app = new Vue({
  router
}).$mount('#app');

</script>
@stop
