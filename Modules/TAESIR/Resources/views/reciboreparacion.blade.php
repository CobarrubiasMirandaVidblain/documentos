<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Recibo Reparacion</title>
<script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\echarts.min.js') }}"></script>
<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }


/*
    footer {
                position: fixed; 
                bottom: -30px; 
                left: 0px; 
                right: 0px;
                height: 50px; 

               
                color: black;
                text-align: left;
                line-height: 35px;
            }
*/

            #bodystyle { page-break-after: always; }
      .footer { position: fixed; bottom: 0px; }
      .pagenum:before { content: counter(page);}
     
</style>

</head>
<div class="footer">Página: <span class="pagenum"></span> <span> Folio: {{$folio}}</span></div>

<body id="bodystyle">
  <table width="100%">
    <tr>
    <td align="bottom"><img src={{asset('images/logo_header.png')}} alt="" width="150"/></td>
        <td align="right">
            <?php
  setlocale (LC_TIME, "es_MX");
  $fecha= strtoupper(strftime("%d de %B de %Y"));
  ?>
            <h3>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</h3>
            <pre>
                TALLER DE ARMADO Y ENSAMBLADO DE SILLA DE RUEDAS
                DIRECCION DE OPERACIÓN DE BIENESTAR
                ORDEN DE REPARACIÓN
                OAXACA DE JUÁREZ A {{$fecha}}
                FOLIO: {{$folio}}
            </pre>
        </td>
    </tr>

  </table>

 
<div id="paginacion">
  <table id="contenido" width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th>CÓDIGO</th>
        <th>NOMBRE</th>
        <th>PRECIO UNITARIO</th>
        <th>CANTIDAD</th>
        <th>COSTO</th>
      </tr>
    </thead>
    <tbody>
      
          @foreach ($diagnosticos as $item)
          <tr>
      <th scope="row" align="center">{{$item->pivot->servicio_id}}</th>
      <td>{{strtoupper($item->beneficiosprogramas->nombre)}}</td>
      <td align="right">${{$item->precio}}.00</td>
          <td align="right">{{$item->pivot->cantidad}}</td>
          <td align="right">${{$item->pivot->costo}}</td>
    </tr>
          @endforeach
     
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <td align="right">Subtotal $</td>
        <td align="right">${{$subtotal}}.00</td>
        </tr>
                <tr>
                <td colspan="3"></td>
                <td align="right">Descuento</td>
                <td align="right">${{$descuento}}</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td align="right">Total $</td>
            <td align="right" class="gray">${{$total}}.00</td>
        </tr>

    </tfoot>
  </table>
<br>
<br>
<table width="100%">
    <tr>
        <td><strong>AUTORIZÓ: </strong> RICARDO CRUZ ILESCAS</td>
        <td align="right"><strong>BENEFICIARIO: </strong>{{$beneficiario}}</td>
        <td align="right"><strong >FIRMA: </strong>___________________</td>
    </tr>
  </table>
</div>
</body>

</html>