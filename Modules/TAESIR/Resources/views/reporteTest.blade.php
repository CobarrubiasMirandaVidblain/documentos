<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Reporte periodico</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }


}
</style>

</head>
<body>
<div >
  <table width="100%" >
    <tr>
    <td align="bottom"><img src={{asset('images/logo_header.png')}} alt="" width="150"/></td>
        <td align="right">
            <?php
  setlocale (LC_TIME, "es_MX");
  $fecha= strtoupper(strftime("%d de %B de %Y"));
  $total =0;
  if($tipo=='mensual'){
       $fechacon = DateTime::createFromFormat('!m', $mes);
   $mes = strtoupper(strftime("%B", $fechacon->getTimestamp()));
  }
  else{
    $mes="";
  }



  ?>
            <h3>SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA DEL ESTADO DE OAXACA</h3>
            <pre>
                TALLER DE ARMADO Y ENSAMBLADO DE SILLA DE RUEDAS
                DIRECCION DE OPERACIÓN DE BIENESTAR
                OAXACA DE JUÁREZ A {{$fecha}}
                ASUNTO: REPORTE DE SERVICIOS
         
            </pre>
        </td>
    </tr>

  </table>



  <br/>
<p style="font-size:12px">LA SIGUIENTE TABLA MUESTRA LA CUOTA DE RECUPERACIÓN POR SERVICIO Y LA CANTIDAD DE VECES QUE ESTE FUE PROPORCIONADO EN {{$mes}} 2019. </p>
  <br/>
  <table  width="100%" style="border-collapse:collapse;border-color:#ddd;" border="1" cellpadding="0" cellspacing="1" bordercolor="#000000">
    <thead>
      <tr>
        <th>SERVICIO</th>
        <th align="center">CANTIDAD</th>
        <th align="right">CUOTA DE RECUPERACIÓN</th>
      </tr>
    </thead>
    <tbody>
      
     
      @foreach ($servicios as $value)
      <tr>
        <td>{{$value['nombre']}}</td>
        <td align="center">{{$value['cantidad']}}</td>
      <td align="right">${{$value['ganancia']}}</td>
      ${{$total+=$value['ganancia']}}
      </tr>
     
      
      @endforeach
     
    </tbody>
    <tfoot>

  </tfoot>
  </table>
 <br/>
<table width="100%">
<tr>
  <td>
    <td align="right">CUOTA DE RECUPERACIÓN TOTAL: ${{$total}}</td>
  </td>
</tr>
</table>
</div>
<br/><br/>
<div >
  <table width="100%" margin-top:10%>
    <td align="center"><img src="<?php echo $base64;?>" style="  width: 700px; height: 400px;"></td>
  </table>
  
</div>

</body>

</html>