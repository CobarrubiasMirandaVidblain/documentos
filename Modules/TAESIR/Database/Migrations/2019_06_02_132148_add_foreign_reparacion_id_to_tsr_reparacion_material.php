<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignReparacionIdToTsrReparacionMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_reparacion_material', function (Blueprint $table) {
            $table->foreign('reparacion_id')->references('id')->on('tsr_reparaciones');
            $table->foreign('material_id')->references('id')->on('tsr_materiales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_reparacion_material', function (Blueprint $table) {
            $table->dropForeign(['reparacion_id']);
            $table->dropIndex('tsr_reparacion_material_reparacion_id_foreign');

            $table->dropForeign(['material_id']);
            $table->dropIndex('tsr_reparacion_material_material_id_foreign');
        });
    }
}
