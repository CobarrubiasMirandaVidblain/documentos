<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUsuarioIdToTsrRequisicionMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_requisiciones_materiales', function (Blueprint $table) {
            $table->foreign('material_id')->references('id')->on('tsr_materiales');
            $table->foreign('requisicion_id')->references('id')->on('tsr_requisiciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_requisiciones_materiales', function (Blueprint $table) {
            $table->dropForeign(['material_id']);
            $table->dropForeign(['requisicion_id']);
            $table->dropIndex('tsr_requisiciones_materiales_material_id_foreign');
            $table->dropIndex('tsr_requisiciones_materiales_requisicion_id_foreign');
        });
    }
}
