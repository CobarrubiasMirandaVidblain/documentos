<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUsuarioIdToTsrDetalledonativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_detalledonativos', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('donativo_id')->references('id')->on('tsr_donativos');
            $table->foreign('material_id')->references('id')->on('tsr_materiales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('tsr_detalledonativos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['donativo_id']);
            $table->dropForeign(['material_id']);
            $table->dropIndex('tsr_detalledonativos_usuario_id_foreign');
            $table->dropIndex('tsr_detalledonativos_donativo_id_foreign');
            $table->dropIndex('tsr_detalledonativos_material_id_foreign');
        });
    }
}
