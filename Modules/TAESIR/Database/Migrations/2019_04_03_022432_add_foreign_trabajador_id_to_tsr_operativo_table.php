<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignTrabajadorIdToTsrOperativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_operativos', function (Blueprint $table) {
            $table->foreign('trabajador_id')->references('id')->on('tsr_trabajadores');
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_operativos', function (Blueprint $table) {
            $table->dropForeign(['trabajador_id']);
            $table->dropIndex('tsr_operativos_trabajador_id_foreign');
        });
    }
}
