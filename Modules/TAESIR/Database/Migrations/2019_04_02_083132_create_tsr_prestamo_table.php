<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrPrestamoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_prestamos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folio')->unique();
            $table->unsignedinteger('ApoyoFuncional_id');
            $table->unsignedinteger('beneficiario_id');
            $table->unsignedinteger('administrativo_id');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->date('fecha_devolucion');
            $table->string('observaciones')->nullable();
            $table->boolean('estatus');
            

            $table->unsignedinteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_prestamos');
    }
}
