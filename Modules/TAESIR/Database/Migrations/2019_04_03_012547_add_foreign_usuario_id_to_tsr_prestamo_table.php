<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUsuarioIdToTsrPrestamoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_prestamos', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('apoyofuncional_id')->references('id')->on('tsr_apoyosfuncionales');
            $table->foreign('beneficiario_id')->references('id')->on('tsr_beneficiarios');
            $table->foreign('administrativo_id')->references('id')->on('tsr_administrativos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_prestamos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['apoyofuncional_id']);
            $table->dropForeign(['beneficiario_id']);
            $table->dropForeign(['administrativo_id']);
            $table->dropIndex('tsr_prestamos_usuario_id_foreign');
            $table->dropIndex('tsr_prestamos_apoyofuncional_id_foreign');
            $table->dropIndex('tsr_prestamos_beneficiario_id_foreign');
            $table->dropIndex('tsr_prestamos_administrativo_id_foreign');
        });
    }
}
