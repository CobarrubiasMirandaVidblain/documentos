<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignMunicipioIdToTsrCaravanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_caravanas', function (Blueprint $table) {
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('operativo_id')->references('id')->on('tsr_operativos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_caravanas', function (Blueprint $table) {
            $table->dropForeign(['municipio_id']);
            $table->dropForeign(['operativo_id']);
            $table->dropIndex('tsr_caravanas_municipio_id_foreign');
            $table->dropIndex('tsr_caravanas_operativo_id_foreign');
        });
    }
}
