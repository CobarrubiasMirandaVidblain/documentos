<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrDiagnosticoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_diagnosticos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedinteger('beneficiario_id');
            $table->unsignedinteger('operativo_id');
            $table->date('fecha');
            $table->unsignedinteger('usuario_id');
            $table->float('subtotal',8,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
    public function down()
    {
        Schema::dropIfExists('tsr_diagnosticos');
    }
}
