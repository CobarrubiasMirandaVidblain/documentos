<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrCaravanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_caravanas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion');
            $table->boolean('status');
            $table->boolean('notificado');
            $table->integer('municipio_id')->unsigned();
            $table->integer('operativo_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_caravanas');
    }
}
