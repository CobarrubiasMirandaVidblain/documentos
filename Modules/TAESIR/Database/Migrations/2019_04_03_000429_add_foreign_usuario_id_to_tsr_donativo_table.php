<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUsuarioIdToTsrDonativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_donativos', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('tsr_donativos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('tsr_donativos_usuario_id_foreign');
            $table->dropForeign(['persona_id']);
            $table->dropIndex('tsr_donativos_persona_id_foreign');
        });
    }
}
