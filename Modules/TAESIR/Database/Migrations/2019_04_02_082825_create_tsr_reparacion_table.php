<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrReparacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_reparaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_orden');
            $table->string('folio');
            $table->string('caracteristicas');
            $table->string('estatus');
            $table->string('observaciones')->nullable();
            $table->float('total',8,2);
            $table->float('descuento',8,2);
            $table->integer('tipo');
            $table->string('folio_lineadeCaptura');

            $table->unsignedinteger('usuario_id');
            $table->unsignedinteger('diagnostico_id');
            $table->unsignedinteger('operativo_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_reparaciones');
    }
}
