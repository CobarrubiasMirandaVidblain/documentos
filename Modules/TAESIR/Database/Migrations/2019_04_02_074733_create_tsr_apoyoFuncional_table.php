<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrApoyoFuncionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_apoyosfuncionales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('num_inventario')->unique();
            $table->string('marca')->nullable();
            $table->string('caracteristicas')->nullable();
            $table->string('estatus');
            $table->string('condicion');

            $table->unsignedinteger('apoyo_id');
            
            $table->unsignedinteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_apoyosfuncionales');
    }
}
