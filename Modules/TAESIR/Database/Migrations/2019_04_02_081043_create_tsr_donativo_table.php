<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrDonativoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_donativos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('nombre');
            $table->string('observaciones')->nullable();
            $table->string('estatus');
            $table->unsignedInteger('persona_id');
            $table->unsignedinteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_donativos');
    }
}
