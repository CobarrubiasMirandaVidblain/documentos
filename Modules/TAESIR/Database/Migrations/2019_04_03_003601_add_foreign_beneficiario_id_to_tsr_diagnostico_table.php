<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignBeneficiarioIdToTsrDiagnosticoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_diagnosticos', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('beneficiario_id')->references('id')->on('tsr_beneficiarios');
            $table->foreign('operativo_id')->references('id')->on('tsr_operativos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_diagnosticos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['beneficiario_id']);
            $table->dropForeign(['operativo_id']);
            $table->dropIndex('tsr_diagnosticos_usuario_id_foreign');
            $table->dropIndex('tsr_diagnosticos_beneficiario_id_foreign');
            $table->dropIndex('tsr_diagnosticos_operativo_id_foreign');
            
        });
    }
}
