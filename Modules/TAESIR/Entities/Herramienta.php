<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class herramienta extends Model
{
  use SoftDeletes;
   protected $table = 'tsr_herramientas';
   protected $fillable = ['num_inventario', 'nombre','observaciones','estatus'];
   protected $dates=['deleted_at'];
   protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
