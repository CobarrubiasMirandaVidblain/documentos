<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class diagnostico extends Model
{
    use SoftDeletes;
    protected $table='tsr_diagnosticos';
    protected $fillable = ['beneficiario_id','operativo_id','fecha','usuario_id','subtotal'];
    protected $dates= ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    

    public function Servicios(){
        return $this->belongsToMany(serviciosbeneficios::class,'tsr_presupuestos','diagnostico_id','servicio_id')
        ->withPivot('id','servicio_id','diagnostico_id','cantidad','costo');
    }

    public function Beneficiario(){
        return $this->belongsTo(beneficiario::class,'beneficiario_id');
    }

 /*  public function ServiciosBeneficios(){
       return $this->belogsToMany(serviciosbeneficios::class,'tsr_presupuestos','diagnostico_id','servicio_id');
   }*/
}
