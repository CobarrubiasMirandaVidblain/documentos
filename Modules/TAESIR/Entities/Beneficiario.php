<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class beneficiario extends Model
{
    use SoftDeletes;
    protected $table='tsr_beneficiarios';
    protected $appends=['persona'];
    protected $fillable = ['curp','solicitud_acuse','acta_nacimiento','comprobante_domicilio','tipo','respinsable','persona_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $with = ['Persona'];
    
    public function getPersonaAttribute(){
        $persona=(object) $this->belongsTo(Persona::class,'persona_id','id');
        return $persona;
    }

    public function Persona(){
        return $this->belongsTo(Persona::class,'persona_id','id');
    }

    public function Diagnostico(){
         return $this->hasMany(diagnostico::class,'beneficiario_id');
    }

    public function Prestamo(){
        return $this->hasMany(prestamo::class,'beneficiario_id');
   }
}
