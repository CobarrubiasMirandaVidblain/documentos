<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class catservicio extends Model
{
    use SoftDeletes;
    protected $table='tsr_cat_servicios';
    protected $fillable = ['nombre','precio'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function Materiales(){
        return $this->belongsToMany(Materiales::class,'tsr_materiales_servicios','servicio_id','material_id');
    }

    public function Diagnosticos(){
        return $this->belongsToMany(diagnostico::class,'tsr_presupuestos','servicio_id','diagnostico_id');
    }
}
