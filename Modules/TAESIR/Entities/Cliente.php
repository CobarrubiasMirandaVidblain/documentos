<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class cliente extends Model
{
    use SoftDeletes;
    protected $table='tsr_clientes';
    protected $fillable = ['folio_cliente','persona_id'];
    protected $dates =['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
