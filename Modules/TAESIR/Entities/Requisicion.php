<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Requisicion extends Model
{
    use SoftDeletes;
    protected $table = 'tsr_requisiciones';
    protected $fillable = ['id', 'folio_ordendecompra', 'fecha', 'observaciones','usuario_id'];
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function Materiales(){
        return $this->belongsToMany(Materiales::class,'tsr_requisiciones_materiales','requisicion_id','material_id')->withPivot('cantidad','material_id');
    }
}
