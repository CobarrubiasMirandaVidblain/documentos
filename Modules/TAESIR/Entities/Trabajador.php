<?php

namespace Modules\TAESIR\Entities;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class trabajador extends Model
{
    use SoftDeletes;
    protected $table = 'tsr_trabajadores';
    protected $fillable = ['codigo_trabajador','area','horario_trabajo','persona_id'];
    protected $dates =['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    
    public function Persona(){
        return $this->belongsTo(Persona::class,'persona_id');
    }
}
