<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class serviciosbeneficios extends Model
{
    use SoftDeletes;
    protected $with = ['beneficiosprogramas'];
    protected $table='tsr_servicios';
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $fillable = [];

    public function beneficiosprogramas(){
        return $this->belongsTo(beneficiosprogramas::class,'beneficiosprogramas_id','id');
    }

    public function Diagnosticos(){
        return $this->belongsToMany(diagnostico::class,'tsr_presupuestos','servicio_id','diagnostico_id');
    }
}
