<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materiales extends Model
{
    use SoftDeletes;
    protected $table = 'tsr_materiales';
    protected $fillable = ['id', 'nombre', 'presentacion', 'minimo','existencia'];
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function Servicios(){
       return $this->belongsToMany(catservicio::class,'tsr_materiales_servicios','material_id','servicio_id');
    }

    public function Reparaciones(){
        return $this->belongsToMany(reparacion::class,'tsr_reparacion_material','material_id','reparacion_id');
    }

    public function Donativos(){
        return $this->belongsToMany(donativo::class,'tsr_detalledonativos','material_id','donativo_id');
    }
    public function Requisiciones(){
        return $this->belongsToMany(requisicion::class,'tsr_requisiciones_materiales','material_id','requisicion_id');
    }

    public function Caravanas(){
        return $this->belongsToMany(requisicion::class,'tsr_materiales_caravanas','material_id','caravana_id');
    }
}
