<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class apoyofuncional extends Model
{
    use SoftDeletes;
    protected $table = 'tsr_apoyosfuncionales';
    protected $fillable = ['num_inventario','marca','caracteristicas','estatus','condicion','apoyo_id','usuario_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function catapoyofuncional(){
       return $this->belongsTo(Catapoyofuncional::class,'apoyo_id');
       
    }
}
