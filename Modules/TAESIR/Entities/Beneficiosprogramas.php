<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use illuminate\Database\Eloquent\SoftDeletes;
class beneficiosprogramas extends Model
{
    use SoftDeletes;
    protected $table = 'beneficiosprogramas';
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $fillable = ['nombre'];

    public function serviciosbeneficios(){
        return $this->hasOne(serviciosbeneficios::class);
    }
}
