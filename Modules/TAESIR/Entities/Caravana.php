<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caravana extends Model
{
    protected $table='tsr_caravanas';
    protected $fillable = ['municipio_id','operativo_id','descripcion'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function Materiales(){
        return $this->belongsToMany(Materiales::class,'tsr_materiales_caravanas','caravana_id','material_id')->with('Materiales');
    }
}
