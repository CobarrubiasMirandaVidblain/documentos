<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
//use App\Http\Controllers\Materiales;


class reparacion extends Model
{
    use SoftDeletes;
    protected $table='tsr_reparaciones';
    protected $fillable = ['num_orden','folio','caracteristicas','estatus','observaciones','total','descuento','usuario_id','diagnostico_id','operativo_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
    
   public function diagnostico(){
        return $this->belongsTo(diagnostico::class,'diagnostico_id','id')
        ->with('beneficiario');
   }

   public function Materiales(){
     return $this->belongsToMany(Materiales::class,'tsr_reparacion_material','reparacion_id','material_id');
 }
}
