<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DashboardController extends Controller
{

    public function getReparacionesMes()
    {
        $reparaciones = DB::table('tsr_reparaciones as rep')
            ->join('tsr_diagnosticos as diag', 'rep.diagnostico_id', '=', 'diag.id')
            ->whereMonth('diag.fecha', '=', date('m'))
            ->whereYear('diag.fecha', '=', date('Y'))
            ->select(DB::raw('extract(DAY FROM diag.fecha) as dia'), DB::raw('count(*) as cantidad'))
            ->groupby('diag.fecha')
            ->get();

        $cantidad = DB::table('tsr_reparaciones as rep')
            ->join('tsr_diagnosticos as diag', 'rep.diagnostico_id', '=', 'diag.id')
            ->whereYear('diag.fecha', '=', date('Y'))
            ->count();

        $hoy = DB::table('tsr_reparaciones as rep')
            ->join('tsr_diagnosticos as diag', 'rep.diagnostico_id', '=', 'diag.id')
            ->whereDay('diag.fecha', '=', date('d'))
            ->count();

        return ['cantidad' => $cantidad, 'reparaciones' => $reparaciones, 'hoy' => $hoy];
    }

    public function getPrestamosMes()
    {
        $prestamos =  DB::table('tsr_prestamos as pre')
            ->whereMonth('pre.fecha_inicio', '=', date('m'))
            ->whereYear('PRE.fecha_inicio', '=', date('Y'))
            ->select(DB::raw('extract(DAY FROM pre.fecha_inicio) as dia'), DB::raw('count(*) as cantidad'))
            ->groupby('pre.fecha_inicio')
            ->get();

        $cantidad = DB::table('tsr_prestamos as pre')
            ->whereYear('PRE.fecha_inicio', '=', date('Y'))
            ->count();

        $hoy = DB::table('tsr_prestamos as pre')
            ->whereDay('pre.fecha_inicio', '=', date('d'))
            ->count();

        return ['prestamos' => $prestamos, 'cantidad' => $cantidad, 'hoy' => $hoy];
    }

    public function getDonacionesMes()
    {
        $donativos = DB::table('tsr_donativos as don')
            ->whereMonth('don.fecha', '=', date('m'))
            ->select(DB::raw('extract(DAY FROM don.fecha) as dia'), DB::raw('count(*) as cantidad'))
            ->groupby('don.fecha')
            ->get();

        $cantidad=DB::table('tsr_donativos as don')
        ->whereYear('don.fecha', '=', date('Y'))
        ->count();

        $hoy=DB::table('tsr_donativos as don')
        ->whereDay('don.fecha', '=', date('d'))
        ->count();

        return ['donativos' => $donativos, 'cantidad' => $cantidad, 'hoy' => $hoy];

    }
}
