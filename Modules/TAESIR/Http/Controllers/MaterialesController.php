<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Materiales;
use Modules\TAESIR\Entities\UnidadesMedida;
use Modules\TAESIR\Entities\reparacion;
use Modules\TAESIR\Entities\Donativo;
use Modules\TAESIR\Entities\Requisicion;
use Modules\TAESIR\Entities\Caravana;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\DB;

class MaterialesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */


    public function index()
    {

        $materiales = Materiales::get();
        return ['materiales' => $materiales];
    }
    public function getUnidades()
    {
        $unidadmedidas = UnidadesMedida::get();
        return ['unidadmedidas' => $unidadmedidas];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $materiales = new Materiales();
        $materiales->nombre = $request->nombre;
        $materiales->presentacion = $request->presentacion;
        $materiales->minimo = $request->minimo;
        $materiales->existencia = $request->existencia;
        $materiales->save();
        return $materiales;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $materiales = Materiales::find($id);
        return $materiales;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $materiales = Materiales::findOrFail($id);
        $materiales->update($request->all());
        return $materiales;
    }

    public function descontarMaterial(Request $request)
    {
        $rep = reparacion::find($request->reparacion_id);
        $material = $request->materiales;
        foreach ($material as $key => $mat) {
            $mass = (object) $mat;
            $materi = Materiales::find($mass->id);
            $materi->existencia = $mass->restante;
            $materi->save();
        }
        foreach ($material as $key => $mat) {
            $materialito = (object) $mat;
            $rep->Materiales()->attach($materialito->id, [
                'cantidad' => $materialito->cantidad,
            ]);
        }
    }


    public function descontarMaterialCaravana(Request $request)
    {
        //return 'ok';
        //$rep=reparacion::find($request->reparacion_id);
        try {
            DB::beginTransaction();
            //crea caravana
            $caravana = new caravana();
            $caravana->municipio_id = $request->municipio_id;
            $caravana->operativo_id = $request->operativo_id;
            $caravana->status = true;
            $caravana->notificado=false;
            $caravana->descripcion = $request->descripcion;
            $caravana->save();

            //descuenta material

            $material = $request->materiales;
            foreach ($material as $key => $mat) {
                $mass = (object) $mat;
                $materi = Materiales::find($mass->id);
                $materi->existencia = $mass->restante;
                $materi->save();
            }

            //ingresar material a la tabla pivote


            foreach ($material as $key => $mat) {
                $materialito = (object) $mat;
                $caravana->Materiales()->attach($materialito->id, [
                    'cantidad' => $materialito->cantidad,
                ]);
            }

            DB::commit();
            return 'ok';
        } catch (Exception $e) {
            DB::rollback();
            return "error";
        }
    }

    public function agregarMaterial(Request $request)
    {
        $rep = donativo::find($request->donativo_id);
        $material = $request->materiales;

        foreach ($material as $key => $mat) {
            $mass = (object) $mat;
            $materi = Materiales::find($mass->id);
            $materi->existencia = $mass->restante;
            $rep->estatus = 'REGISTRADO';
            $rep->save();
            $materi->save();
        }
        foreach ($material as $key => $mat) {
            $materialito = (object) $mat;
            $rep->Materiales()->attach($materialito->id, [
                'cantidad' => $materialito->cantidad,
            ]);
        }
    }

    public function ordenMaterial(Request $request){
        $auxiliar=array();
        $material=$request->materiales;
        foreach($material as $key=> $valor){
            $aux=(Object)$valor;
            if($aux->id===0){
                $nuevoMaterial = new Materiales();
                $nuevoMaterial->nombre = $aux->nombre;
                $nuevoMaterial->presentacion= $aux->presentacion;
                $nuevoMaterial->minimo=$aux->nuevaexistencia;
                $nuevoMaterial->existencia=$aux->nuevaexistencia;
                $nuevoMaterial->save();
                $nuevoMaterial->nuevaexistencia=$aux->nuevaexistencia;
                $nuevoMaterial->cantidad=$aux->nuevaexistencia;
              array_push($auxiliar,$nuevoMaterial);               
            }else{array_push($auxiliar,$aux);  }       
        }
        $requi=requisicion::find($request->requisicion_id);
        foreach ($auxiliar as $key => $mat) {
            $mass=(Object)$mat;
            $materi=Materiales::find($mass->id);
            $materi->existencia=$mass->nuevaexistencia;
            $materi->save();
        }
        foreach ($auxiliar as $key => $mat) {
            $materialito = (Object)$mat;
            $requi->Materiales()->attach($materialito->id, [
                'cantidad' => $materialito->cantidad,
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        Materiales::destroy($id);
        return 'Eliminado';
    }
}
