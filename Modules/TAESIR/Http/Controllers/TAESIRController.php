<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Firebase\JWT\JWT;
use Carbon\Carbon;
use App\Models\Modulo;

class TAESIRController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
   
   
   public function __construct()
   {
       $this->middleware(['auth','authorized'],['only'=>['index']]);

   }
  
     public function index()
    {
      $usuario = auth()->user();
       
      $token = [
          'usuario_id' => $usuario->id,
          'modulo_id' => Modulo::where('nombre', 'like', 'taesir')->first()->id,
          'rol_id' => $usuario->rolestaesir->first()->pivot->rol_id
      ];
      $encoded = JWT::encode($token, config('app.jwt_taesir_token'), 'HS256');
      $usuario->token = $encoded;
      $usuario->vida_token = Carbon::now()->addMinutes(40); 
      $usuario->save();
      return view('taesir::index', ['token' => $encoded,'rol'=>'Admin']);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('taesir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function herramientas(){
      return view('taesir::herramientas.indexherramientas');
    }

    public function materiales(){
      return view('taesir::materiales.indexmateriales'); 
    }
}
