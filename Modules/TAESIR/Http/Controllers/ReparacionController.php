<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Reparacion;
use Modules\TAESIR\Entities\diagnostico;
use Modules\TAESIR\Entities\Persona;
use Barryvdh\DomPDF\Facade as PDF;

class ReparacionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($tipo)
    {
        //todas las reparaciones
        if ($tipo == 2) { 
            $reparacion = Reparacion::with('diagnostico')->where('estatus','=','1')
            ->get();
            return ['reparacion' => $reparacion];
        }

        $reparacion = Reparacion::with('diagnostico')->where('estatus', '=', '1')
            ->where('tipo', '=', $tipo) //reparaciones taesir o don mecanico 0=taller 1=donMecanico
            ->get();
        return ['reparacion' => $reparacion];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $per = Persona::find($request->operativo_id)->Operativo;


        $reparacion = new Reparacion();
        $reparacion->num_orden = $request->num_orden;
        $reparacion->caracteristicas = $request->caracteristicas;
        $reparacion->observaciones = $request->observaciones;
        if(!$request->acuse){
            $reparacion->total = $request->total;
            $reparacion->descuento = $request->descuento;
        }else{
            $reparacion->total = 0;
            $reparacion->descuento = $request->total;
        }
        $reparacion->estatus = true;
        $reparacion->tipo = 0;
        $reparacion->folio = 'TSR-000';
        $reparacion->usuario_id = auth()->user()->id;
        $reparacion->diagnostico_id = $request->diagnostico_id;
        $reparacion->operativo_id = $per[0]->id;
        $reparacion->save();
        $reparacion->folio = 'TSR-F' . $reparacion->id;
        $reparacion->save();



        return $reparacion->id;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $reparacion = Reparacion::find($id);
        return $reparacion;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function agregarServicios($id)
    {

        $reparacion = reparacion::find($id);
        $diag = Diagnostico::with('Servicios')->find($reparacion->diagnostico_id); //Diagnostico::with('Servicios')->where('diagnostico_id','=','1')->get();
        $subtotal = $diag->subtotal;
        $diag = ($diag->servicios);
        return ['servicios' => $diag, 'subtotal' => $subtotal, 'diagnostico_id' => $reparacion->diagnostico_id];
    }

    public function update(Request $request, $id)
    {
        $diag = Diagnostico::find($id);
        $rep = Reparacion::find($request->reparacion_id);
        $rep->total = $request->subtotal;
        $rep->save();
        $diag->subtotal = $request->subtotal;
        $diag->save();
        $serv = $request->servicios;
        $diag->Servicios()->detach();
        foreach ($serv as $key => $servicio) {
            $servicito = (object) $servicio;
            $diag->Servicios()->attach($servicito->id, [
                'cantidad' => $servicito->cantidad, //agregar lo que llega en la request
                'costo' => $servicito->subtotalservicio, //agregar lo que llega de la request
            ]);
        }
        return 'si se pudo';
    }
    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    { }
}
