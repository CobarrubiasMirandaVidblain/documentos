<?php

namespace Modules\TAESIR\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TallerSillas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $jwt = $request->header('taesirToken');
        if(!$jwt && !$request->token){
            return new JsonResponse(['message' => 'Error'], 401);
        }
        try{
            if($jwt){
                $inf = JWT::decode($jwt, config('app.jwt_taesir_token'), ['HS256']);
            }else{
                $inf = JWT::decode($request->token, config('app.jwt_taesir_token'), ['HS256']);
            }
            $request['usuario_id'] = $inf->usuario_id;
            $request['modulo_id'] = $inf->modulo_id;
            $request['rol_id'] = $inf->rol_id;
            return $next($request);
        } catch(\UnexpectedValueException $e){
            return new JsonResponse(['message' => 'Token incorrecto'], 403);
        }
    }

    
}
