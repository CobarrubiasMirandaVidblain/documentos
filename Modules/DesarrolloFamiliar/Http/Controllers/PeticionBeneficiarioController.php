<?php

namespace Modules\DesarrolloFamiliar\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\PeticionBeneficiarioBaseController;

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController{
	function __construct(){
		//NO se le pasa el nombre del programa padre
		//porque se mostrarán las peticiones de acuerdo al área del usuario
    	parent::__construct('');
  	}
}