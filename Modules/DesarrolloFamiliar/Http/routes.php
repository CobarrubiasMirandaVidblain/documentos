<?php

Route::group(['middleware' => 'web', 'prefix' => 'desarrollofamiliar', 'namespace' => 'Modules\DesarrolloFamiliar\Http\Controllers'], function() {
    Route::get('/', 'PeticionController@index');
    Route::resource('peticiones', 'PeticionController',['as'=>'desarrollofamiliar','only'=>['index','show','update','destroy']]);   //Peticiones
    Route::resource('programas', 'ProgramaController',['as'=>'desarrollofamiliar']); //Programas (beneficios)
    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'desarrollofamiliar' ]);                                //Beneficiarios
});
