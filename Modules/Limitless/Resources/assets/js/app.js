window.$ = window.jQuery = require('./main/jquery.min');
require('./main/bootstrap.bundle.min');
require('./plugins/prism.min');
require('./plugins/sticky.min');
require('./main/app');
require('./pages/components_scrollspy');
