<?php

Route::group(['middleware' => 'web', 'prefix' => 'limitless', 'namespace' => 'Modules\Limitless\Http\Controllers'], function()
{
    Route::get('/', 'LimitlessController@index');
});
