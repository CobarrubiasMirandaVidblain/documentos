let mix = require('laravel-mix');

mix.js('Resources/assets/js/app.js', './../../public/limitlesss')
  .sass('Resources/assets/sass/app.scss', './../../public/limitlesss')
  .options({
      processCssUrls: false
  })
  .setPublicPath('./../../public/limitlesss');