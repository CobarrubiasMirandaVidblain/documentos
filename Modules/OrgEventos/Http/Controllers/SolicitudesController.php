<?php

namespace Modules\OrgEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Models\EventosTaller;
use App\Models\CatEvento;
use App\Models\Statusproceso;
use App\Models\Persona;
use App\Models\Empleado;
use App\Models\Beneficiosprograma;

use Maatwebsite\Excel\Facades\Excel;
use Modules\OrgEventos\Entities\EventosParticipantes;
use App\Models\BeneficiosprogramasSolicitud;

class SolicitudesController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:orgeventos,ADMINISTRADOR,ADMIN,CAPTURISTA', ['only' => ['show', 'index','store','edit','update','destroy','listEventos','tipoEventoList','quitarParticipante','repParticipantes','search']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        return view('orgeventos::solicitud.solicitud');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
     public function store(Request $request)
    {      
        
		if($request->con_folio == true){
            $folio = BeneficiosprogramasSolicitud::where('folio',$request->folio_peticion)->get();
            if(count($folio) == 0)
                return response()->json(['success' => false, 'code' => 403, 'estatus' => false, 'msj'=>'Folio Incorrecto'], 200);   
			}

        try {
            DB::beginTransaction();

            $areaId= Empleado::where('persona_id',auth()->user()->persona_id)->first()->area_id;
            $estatus= Statusproceso::where('status','PENDIENTE')->select('id')->first();
            $contador = EventosTaller::withTrashed()->count() + 1;
            
            $evento = EventosTaller::create([
                'tipo_evento_id' => $request->tipo_evento_id,
                'localidad_id' => $request->localidad_id,
                'municipio_id' => $request->municipio_id,
                'status_id' => $estatus->id,
                'calle' => $request->calle,
                'numExt' => $request->numExt,
                'numInt' => $request->numInt,
                'colonia' => $request->colonia,
                'descripcionEvento' => $request->descripcionEvento,
                'codigoPostal' => $request->codigoPostal,
                'area_id' => $areaId,
                'beneficiosprogramas_id' => $request->nombreEvento['id'], 
                'fechaEvento' => substr($request->fechaEvento,0,10), 
                'usuario_id' => auth()->user()->id,
                'folio' => 'DIF-BEN-'.$request->nombreEvento['id'].'-'.$contador.'-'.date("Y"),
                'folio_peticion' => $request->folio_peticion,
                'peticion_id' => $request->con_folio == true ? $folio[0]->id : null,
            ]);
            
            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'Evento',
                'registro' => $evento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);   
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request)
    {
        return EventosTaller::with('municipio','localidad','tipoEvento','participantes','estatus','beneficio')->where('id', $request->idEvento)->first();   
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        return EventosTaller::with('municipio','localidad','tipoEvento','beneficio')->where('id', $request->idEvento)->first();
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $evento = EventosTaller::findOrFail($request->id);
            $evento->tipo_evento_id = $request->tipo_evento_id;
            $evento->localidad_id = $request->localidad_id;
            $evento->municipio_id = $request->municipio_id;
            $evento->fechaEvento = substr($request->fechaEvento,0,10);
            $evento->calle = $request->calle;
            $evento->numExt = $request->numExt;
            $evento->numInt = $request->numInt;
            $evento->colonia = $request->colonia;
            $evento->descripcionEvento = $request->descripcionEvento;
            $evento->codigoPostal = $request->codigoPostal;
            $evento->save();

            auth()->user()->bitacora(request(), [
                'tabla' => 'Evento',
                'registro' => $evento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
         try {
            DB::beginTransaction();

            EventosTaller::destroy($request->id);

                auth()->user()->bitacora(request(), [
                'tabla' => 'Evento',
                'registro' => $request->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => 'destroy'], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    public function listEventos (Request $request)
    {
				$areaId= Empleado::where('persona_id',auth()->user()->persona_id)->first()->area_id;
				// dd($areaId);
        if(auth()->user()->hasRolesStrModulo(['ADMIN'], 'orgeventos'))
            $eventosList = EventosTaller::with('estatus','municipio','localidad','tipoEvento','participantes','beneficio')->orderBy('status_id','ASC')->get();
        else
            $eventosList = EventosTaller::with('estatus','municipio','localidad','tipoEvento','participantes','beneficio')->orderBy('status_id','ASC')-> where('area_id',$areaId)->get();

        return response()->json([
            'lista' => $eventosList
        ],200);
    }

    public function tipoEventoList ()
    {   
        $areaId= Empleado::where('persona_id',auth()->user()->persona_id)->select('area_id')->first();
        $areas=array_map( function($o) { return $o->id; }, DB::select('call getProgramas(?)',[$areaId->area_id]));
        $beneficio = Beneficiosprograma::with('anioPrograma.programa')->wherehas('anioPrograma.programa',function ($qry) use ($areas)
        {
            $qry->wherein('id',$areas);
        })->get(['beneficiosprogramas.id','beneficiosprogramas.nombre']);

        return response()->json(['lista'=>CatEvento::all(),'listStatus'=>Statusproceso::all(),  'beneficios' => $beneficio ],200);
    }

    public function listaParticipantes (Request $request)
    {
         $participanteList =EventosTaller::with('participantes')->where('id',$request->id)->orderBy('id','DESC')->first();

        return response()->json([
                'lista' => $participanteList
            ],200);
    }

    public function quitarParticipante(Request $request)
    {
			// return $request->all();
			try {
				// DB::beginTransaction();
				EventosParticipantes::where('evento_id',$request->evento_id)->where('persona_id',$request->id)->delete();              
				// DB::commit();

				auth()->user()->bitacora(request(), [
						'tabla' => 'Evento',
						'registro' => $request->id . '',
						'campos' => json_encode($request->all()) . '',
						'metodo' => request()->method(),
						'usuario_id' => auth()->user()->id
				]); 
				return response()->json(['success' => true, 'code' => 200, 'evento' =>$request->evento_id], 200);
			}
			catch(\Exception $e) {
					DB::rollBack();
					return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
			}
    }
    public function repParticipantes ($id){

        $GLOBALS['id'] = $id;
        $evento = EventosTaller::where('id', $id)->first();
       
        Excel::create('ParticipantesEvento-'.$evento->nombreEvento, function($excel) {

            $excel->sheet('Lista', function($sheet) {
 
                $participanteList = DB::table('personas')
                    ->select('personas.nombre','personas.primer_apellido as primerApellido','personas.segundo_apellido as segundoApellido','personas.genero','personas.fecha_nacimiento as fechaNacimiento','personas.numero_local as numero','personas.email as correo')
                    ->join('orev_eventos_participantes','orev_eventos_participantes.persona_id','=','personas.id') 
                    ->where('orev_eventos_participantes.evento_id',$GLOBALS['id']) 
                    ->get();

                $participanteList = json_decode(json_encode($participanteList), true);
                $sheet->fromArray($participanteList);
            });
        })->export('xls');
    }
    public function repEventos (){

        $areaId= Empleado::where('persona_id',auth()->user()->persona_id)->select('area_id')->first();
        $GLOBALS['areaId'] = $areaId->area_id;
        Excel::create('ListadeEventos', function($excel) {

            $excel->sheet('Lista', function($sheet) {
                $eventosList = EventosTaller::join('beneficiosprogramas','beneficiosprogramas.id','orev_eventos.beneficiosprogramas_id')
                    ->join('cat_localidades','orev_eventos.localidad_id','=','cat_localidades.id')
                    ->join('cat_municipios','orev_eventos.municipio_id','=','cat_municipios.id')
                    ->join('cat_statusprocesos','orev_eventos.status_id','=','cat_statusprocesos.id')
                    ->join('orev_cat_eventos','orev_eventos.tipo_evento_id','=','orev_cat_eventos.id')
                    ->where('orev_eventos.deleted_at',null)
                    ->where('orev_eventos.area_id', $GLOBALS['areaId'])
                    ->select('beneficiosprogramas.nombre','cat_statusprocesos.status','orev_cat_eventos.nombre as tipoEvento','orev_eventos.fechaEvento','orev_eventos.descripcionEvento','cat_municipios.nombre as municipio','cat_localidades.nombre as localidad','orev_eventos.colonia','orev_eventos.calle','orev_eventos.numExt','orev_eventos.numInt','orev_eventos.codigopostal')
                    ->get();

                $eventosList = json_decode(json_encode($eventosList), true);
                $sheet->fromArray($eventosList);
 
            });
        })->export('xls');
    }

    public function search($item)
    {
        $areaId= Empleado::where('persona_id',auth()->user()->persona_id)->first()->area_id;
        if(auth()->user()->hasRolesStrModulo(['ADMIN'], 'orgeventos'))
        {
            return new JsonResponse(EventosTaller::with('estatus','municipio','localidad','tipoEvento','participantes','beneficio','area')->whereHas('beneficio',function ($query) use ( $item){
                $query->where('nombre','like', "%$item%");
            })->take(10)->get());   
        }
        else{
            return new JsonResponse(EventosTaller::where('area_id',$areaId)->with('estatus','municipio','localidad','tipoEvento','participantes','beneficio','area')->whereIn('status_id',"!=",['7'])->whereHas('beneficio',function ($query) use ( $item){
                $query->where('nombre','like', "%$item%");
            })->take(10)->get());  
        }
    }

    public function busquedaEvento (Request $request)
    {
			
        $areaId= Empleado::where('persona_id',auth()->user()->persona_id)->first()->area_id;
        $term = $request->dato;
        if(auth()->user()->hasRolesStrModulo(['ADMIN'], 'orgeventos'))
        {
            $evento = EventosTaller::with('estatus','municipio','localidad','tipoEvento','participantes','beneficio')->whereHas('beneficio',function ($query) use ( $term){
                $query->where('nombre','like', "%$term%");
            })->paginate(15);   
        }
        else{
            $evento = EventosTaller::where('area_id',$areaId)->with('estatus','municipio','localidad','tipoEvento','participantes','beneficio')->whereHas('beneficio',function ($query) use ( $term){
                $query->where('nombre','like', "%$term%");
            })->paginate(15);  
        }
         

        return response()->json([
                'pagination'=>[
                    'total'         => $evento->total(),
                    'current_page'  => $evento->currentPage(),
                    'per_page'      => $evento->perPage(),
                    'last_page'     => $evento->lastPage(),
                    'from'          => $evento->firstItem(),
                    'to'            => $evento->lastPage(),
                ],
                'lista' => $evento
            ],200);
    }

    public function searchPersona (Request $request)
    {
        $columns = ['nombre','primer_apellido','segundo_apellido','curp', 'fecha_nacimiento'];
        $term = $request->curp;
        $words_search = explode(" ",$term);
        $personaOption =Persona::with('personaBeneficio')->where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                        foreach ($columns as $column) {
                            $query->orWhere($column,'like',"%$word%");
                        }
                    });
                }
            });
            $personaOption = $personaOption->paginate(6);   
        return response()->json($personaOption);
    }
    
}
