<?php

namespace Modules\OrgEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\EventosTaller;
use App\Models\Persona;
use App\Models\Empleado;


class OrgEventosController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('roles:ADMIN,ADMINISTRADOR,CAPTURISTA');
       
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {   
        
        // if(auth()->user()->hasRoles(['SUPERADMIN']))
        // {
        //     $listaSolicitudes = EventosTaller::where('status_id',4)->get();
        //     return view('orgeventos::index')->with('listaSolicitudes', $listaSolicitudes);
        // }
        // else
           return view('orgeventos::solicitud.solicitud');
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('orgeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(EventoStoreRequest $request)
    {
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $areaId= Empleado::where('persona_id',auth()->user()->persona_id)->first()->area_id;

        return response()->json([
            'urgentes' => EventosTaller::where('status_id',1)->where('area_id',$areaId)->count(),
            'participantes' => Persona::has('evento')->with('evento')->count(),
            'finalizado'=>EventosTaller::where('status_id',8)->where('area_id',$areaId)->count(),
            'eventos'=>EventosTaller::where('area_id',$areaId)->count(),
            'listaEspera' =>  EventosTaller::where('status_id',5)->where('area_id',$areaId)->count(),
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('orgeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        // if(!$request->ajax()) return redirect('/');

        DB::transaction(function() use($request) {
            $evento = EventosTaller::findOrFail($request->id);
            $evento->status_id = $request->dato ? $request->dato:5;
            $evento->save();
        });

        return response()->json(['success' => true], 200);

    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {

    }

    public function table(Request $request)
    {   

        $listaSolicitudes = EventosTaller::where('status_id',$request->status)->get();
        return view('orgeventos::tabla')->with('listaSolicitudes', $listaSolicitudes);
    }

}
