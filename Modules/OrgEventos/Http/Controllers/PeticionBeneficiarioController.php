<?php

namespace Modules\OrgEventos\Http\Controllers;

use Illuminate\Routing\Controller;

use App\Http\Controllers\PeticionBeneficiarioBaseController;

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController {
    public function __construct(){
        parent::__construct('SENSIBILIZACIION');
    }
}
