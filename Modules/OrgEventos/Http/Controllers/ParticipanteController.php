<?php

namespace Modules\OrgEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Models\Participante;
use App\Models\Persona;
use App\Models\PersonasPrograma;
use App\Models\Beneficiosprograma;
use Modules\OrgEventos\Http\Requests\ParticipanteStoreRequest;

use Maatwebsite\Excel\Facades\Excel;
use Modules\OrgEventos\Entities\EventosParticipantes;
use App\Models\BeneficiosPersonas;

class ParticipanteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('authorized');
        $this->middleware('roles:ADMIN,ADMINISTRADOR,CAPTURISTA');
        
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('orgeventos::participantes.participantes');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('orgeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(ParticipanteStoreRequest $request)
    {
    //    return $request->all();
        try {
            DB::beginTransaction();

            if (Persona::where('curp',$request->curp)->first()) {
                $persona = Persona::where('curp', $request->curp)->with('evento')->first();
                $tabla = "IntermediaParticipante";

            }else{
                $tabla = "Persona";

                $persona = Persona::create([
                    'nombre' => $request->nombre,
                    'primer_apellido' => $request->primer_apellido,
                    'segundo_apellido' => $request->segundo_apellido,
                    'genero' => $request->genero,
                    'numero_local' => $request->numero_local,
                    'curp' => $request->curp,
                    'email' => $request->email,
                    'fecha_nacimiento' => substr($request->fecha_nacimiento,0,10),
                    'usuario_id' => auth()->user()->id,
                ]);
            }
            $beneficio = Beneficiosprograma::find($request->beneficio)->id;

            $personaExiste = Persona::where('id',$persona->id)->with('personabeneficio')->first();
            
            if($personaExiste->personabeneficio == null)
            {
                BeneficiosPersonas::create([
                    'beneficioprograma_id' => $beneficio,
                    'persona_id' => $persona->id,
                    'usuario_id' => auth()->user()->id,
                ]);
            }

            // if($personaExiste == [])
            // {
            //     $personaProgramas = PersonasPrograma::create([
            //         'persona_id' =>  $persona->id,
            //         'anios_programa_id' =>  $beneficio,
            //         'usuario_id' => auth()->user()->id,
            //     ]);
            // }

            $persona->evento()->attach($request->idEvento);

            auth()->user()->bitacora(request(), [
                'tabla' => $tabla,
                'registro' => $persona->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('orgeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('orgeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();

            $participante = Persona::find($request->id);

            $participante->numero_local = $request->numero_local;
            $participante->email = $request->email;

            $participante->save();

                auth()->user()->bitacora(request(), [
                'tabla' => 'Personas',
                'registro' => $participante->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);

            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
       /* $participante = Participante::destroy($request->id);

        return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);*/
    }

    

    public function fileXLS (Request $request){
       Excel::create('ParticipantesEvento', function($excel) {
 
            $excel->sheet('Lista', function($sheet) {
                $participanteList = Participante::where('id',$request->id)->first();
                $participanteList = json_decode(json_encode($participanteList), true);
                $sheet->fromArray($participanteList);
            });
        })->export('xls');
    }

    public function listaParticipantes (){
        $edades = DB::table('personas')
        ->select(DB::raw(' personas.genero, YEAR(CURDATE())-YEAR(personas.fecha_nacimiento)+IF(DATE_FORMAT(CURDATE(),"%m-%d")>DATE_FORMAT(personas.fecha_nacimiento,"%m-%d"),0,-1) as edad'))
        ->join('personas_programas','personas_programas.persona_id','=','personas.id')
        ->where('personas.deleted_at',null)
        ->where('personas_programas.anios_programa_id',8) // id del programa creado en la tabla
        ->get();

        $mujeres = 0;
        $hombre  = 0;
        $menorH  = 0;
        $menorM  = 0;

        foreach ($edades as $key => $value) {
            
            if($value->genero == 'H' && $value->edad >= 18)
                $hombre++;
            elseif ($value->genero == 'M' && $value->edad < 18)
                $menorM++;
            elseif ($value->genero == 'H' && $value->edad < 18)
                $menorH++;
            else
                $mujeres++;
        };

        return response()->json([
            'hombres'   =>  $hombre,
            'mujeres'   =>  $mujeres,
            'menorH'    =>  $menorH,
            'menorM'    =>  $menorM,
        ],200);
       
    }
    
    // public function reporteParticipantesAll()
    // {
    //     Excel::create('ListaParticipantes', function($excel) {

    //         $excel->sheet('Lista', function($sheet) {
    //             $participantesAll =  DB::table('personas')
    //                 ->select('personas.nombre','personas.primer_apellido as primerApellido','personas.segundo_apellido as segundoApellido','personas.genero','personas.fecha_nacimiento',DB::raw(' personas.genero, YEAR(CURDATE())-YEAR(personas.fecha_nacimiento)+IF(DATE_FORMAT(CURDATE(),"%m-%d")>DATE_FORMAT(personas.fecha_nacimiento,"%m-%d"),0,-1) as edad'),'personas.numero_local as numero','personas.email as correo')
    //                 ->join('personas_programas','personas_programas.persona_id','=','personas.id')
    //                 ->where('personas_programas.anios_programa_id',8) // 8 numero del programa creado
    //                 ->get();

    //             $participantesAll = json_decode(json_encode($participantesAll), true);
    //             $sheet->fromArray($participantesAll);
    //         });
    //     })->export('xls');
    // }
    public function search (Request $request)
    {
        $columns = ['nombre','primer_apellido','segundo_apellido','curp','genero','fecha_nacimiento','numero_local','email'];
        $term = $request->dato;
        $words_search = explode(" ",$term);
        $personaList =Persona::has('evento')->with('evento')
            ->where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                        foreach ($columns as $column) {
                            $query->orWhere($column,'like',"%$word%");
                        }
                    });
                }
            });
        $personaList = $personaList
            ->select('personas.*')
            ->paginate(10);

            return response()->json([
            'pagination'=>[
                'total'         => $personaList->total(),
                'current_page'  => $personaList->currentPage(),
                'per_page'      => $personaList->perPage(),
                'last_page'     => $personaList->lastPage(),
                'from'          => $personaList->firstItem(),
                'to'            => $personaList->lastPage(),
            ],
            'lista' => $personaList
        ],200); 
    }

    public function showPersona ($id)
    {
        return response()->json(Persona::where('id',$id)->has('evento')->with('evento')->firstOrFail());
    }

    public function check(Request $request)
    {
        $participante = EventosParticipantes::where('persona_id',$request->persona_id)->where('evento_id',$request->evento_id)->first();

        if($request->tipo == 1)
            $participante->evaluado = $request->delete == 1 ? 0 : 1;
        else
            $participante->constancia = $request->delete == 1 ? 0 : 1;

        $participante->save();

        auth()->user()->bitacora(request(), [
            'tabla' => 'orev_eventos_participantes',
            'registro' => $participante->id . '',
            'campos' => json_encode($request->all()) . '',
            'metodo' => request()->method(),
            'usuario_id' => auth()->user()->id
        ]);

        return new JsonResponse(['data' => $participante , 'success' => "true"]);
        
    }
}