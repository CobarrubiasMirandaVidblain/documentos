<?php

namespace Modules\OrgEventos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventoUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            'nombreEvento' => 'required|unique:Evento,nombreEvento,'. Sthis->evento,
            'fechaEvento' => 'required',
            'municipio_id' => 'required',
            'localidad_id' => 'required',
            'colonia' => 'required',
            'codigoPostal' => 'required',
            'calle' => 'required',
            'tipo_evento_id' => 'required',
            'descripcionEvento' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
