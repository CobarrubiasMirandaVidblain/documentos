<?php

namespace Modules\OrgEventos\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParticipanteStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'primer_apellido' => 'required',
            'segundo_apellido' => 'required',
            'genero' => 'required|in:H,M',
            'fecha_nacimiento' => 'required',
            // 'numero_local' => 'numeric|digits_between:7,10',
            // 'curp' => 'required|unique:Participante,curp'
            // 'correo' => 'email',
            // 'telefono' => 'required|numeric|regex:/^[0-9]{10}$/',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
