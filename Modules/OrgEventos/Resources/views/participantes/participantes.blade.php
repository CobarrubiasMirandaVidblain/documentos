@extends('orgeventos::layouts.master')

@section('content-subtitle')
Lista de Participantes
@endsection

@push('head')

<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
	[v-cloak] {display: none;}
</style>

@endpush

@section('content')
<div id="participante">
	<div v-cloak>	
		{{-- <section class="content"> --}}
			<v-select :options="listaEventos" label="titulo" v-model="evento" @search="findProduct" :filterable='false'></v-select>
			<br>
			<br>
			
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#detalle" data-toggle="tab">Detalle</a></li>
						<li><a href="#inscrito" data-toggle="tab">Inscritos</a></li>
						<li><a href="#evaluado" data-toggle="tab">Evaluados</a></li>
						<li><a href="#constancia" data-toggle="tab">Constancia</a></li>
						<li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="detalle">
							<b>Nombre : @{{evento.beneficio.nombre}} - <small>Folio : @{{evento.folio}}</small>	</b>
							<p><b>Area : </b> @{{evento.area.nombre}}</p>
							<p><b>Descripción : </b> <br> @{{evento.descripcionEvento}}</p>
							<p><b>Direccion : </b> Municipio : @{{evento.municipio.nombre}} - Localidad : @{{evento.localidad.nombre}} - Colonia : @{{evento.colonia}} - Calle : @{{evento.calle}} - Numero : @{{evento.numExt}} - Numero Interior : @{{evento.numInt ? evento.numInt : 'S/N'}} - CP : @{{evento.codigoPostal}}</p>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="inscrito">
							<div class="box">
								<div class="box-header">
									
								</div>
									<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tbody>
											<tr>
												<th>Nombre</th>
												<th>Inscrito</th>
												<th>Evaluado</th>
											</tr>
											<tr v-for="(p,index) in evento.participantes">
												<td>@{{p.nombre + ' ' +p.primer_apellido + ' ' +p.segundo_apellido}}</td>
												<td><span class="label label-success"><i class="fa fa-check"></i> Inscrito</span></td>
												<td>
													<button  v-if="p.pivot.evaluado != 1" title="Evaluado" class="btn btn-warning btn-xs" @click.prevent="check(p.id,1,index)"><i class="fa fa-check-square-o"></i> Evaluar</button>
													<span v-else class="label label-success"><i class="fa fa-check"></i> Evaluado</span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="evaluado">
							<div class="box">
								<div class="box-header">
									
								</div>
									<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tbody>
											<tr>
												<th>Nombre</th>
												<th>Inscrito</th>
												<th>Constancia</th>
												<th>Opciones</th>
											</tr>
											<template v-for="(p,index) in evento.participantes">
												<tr v-if="p.pivot.evaluado == 1">
													<td>@{{p.nombre + ' ' +p.primer_apellido + ' ' +p.segundo_apellido}}</td>
													<td><span class="label label-success"><i class="fa fa-check"></i> Evaluado</span></td>
													<td>
														<button v-if="p.pivot.constancia != 1" title="Constancia" class="btn btn-warning btn-xs" @click.prevent="check(p.id,2,index)"><i class="fa fa-graduation-cap"></i> Constancia</button>
														<span v-else class="label label-success"><i class="fa fa-check"></i> Constancia</span>
													</td>
													<td><button v-if="p.pivot.constancia != 1" title="Constancia" class="btn btn-danger btn-xs" @click.prevent="check(p.id,1,index,1)"><i class="fa fa-user-times"></i> Quitar</button></td>
												</tr>
											</template>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="constancia">
							<div class="box">
								<div class="box-header">
									
								</div>
									<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tbody>
											<tr>
												<th>Nombre</th>
												<th>Constancia</th>
												<th>Opciones</th>
											</tr>
											<template v-for="(p,index) in evento.participantes">
												<tr v-if="p.pivot.constancia == 1">
													<td>@{{p.nombre + ' ' +p.primer_apellido + ' ' +p.segundo_apellido}}</td>
													<td>
														<span class="label label-success"><i class="fa fa-check"></i> Constancia</span>
													</td>
													<td><button  title="Constancia" class="btn btn-danger btn-xs" @click.prevent="check(p.id,2,index,1)"><i class="fa fa-user-times"></i> Quitar</button></td>
												</tr>
											</template>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- /.tab-pane -->
					  </div>
					  <!-- /.tab-content -->
					</div>
					<!-- nav-tabs-custom -->
				  </div>
		{{-- </section> --}}

    </div>
</div>
@stop

@push('body')

<script type="text/javascript" src="{{ asset('bower_components/vue/vue-multiselect.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/vue/vee-validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/vue/vuejs-datepicker.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#fechaNacimiento").addClass( "form-control" );
	});
	$(".modal").modal({
	   	keyboard: false,
	   	backdrop: "static"
	 });
	 $(".modal").modal('hide');
	 
  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);
  
  new Vue({
    el: '#app',
    data: {
		listaEventos:[],
		evento:{beneficio:{},area:{},municipio:{},localidad:{},participantes:{}},
    },
    computed: {
      
    },
    methods: {
		findProduct(search, loading) {
			let me = this;
			if(search.length > 3)
			{
				loading(true);
				block();
				var url = "/orgeventos/solicitudes/search/"+search;
				axios.get(url).then(function (response) {
					unblock();		
					if(response.data.length > 0){
						me.listaEventos = response.data.map(item => {
						item.titulo = item.beneficio.nombre + ' - ' + item.folio;
							return item;
						})
					}
					else
						swal({position: 'top-end', type: 'info', title: 'Sin resultados', showConfirmButton: false, timer: 1700})	
					loading(false);
				}).catch(function (error){
					unblock();
					console.log("Error "+error)
				})
			}
		},
		check(persona,tipo,index,eliminar){
			let me = this;
			var url = "/orgeventos/participantes/check";
			axios.post(url,{persona_id:persona,evento_id:me.evento.id,tipo:tipo,delete:eliminar?eliminar:0}).then(function (response) {
				unblock();	
				if(response.data.success)
				{
					me.evento.participantes[index].pivot = response.data.data
					swal({position: 'top-end', type: 'success', title: 'Se actualizo correctamente', showConfirmButton: false, timer: 1700})
				}	
				else
					swal({position: 'top-end', type: 'error', title: 'Error al guardar', showConfirmButton: false, timer: 1700})

				console.log(response.data)
			}).catch(function (error){
				unblock();
				console.log("Error "+error)
			})
		}
      
    },
    mounted: function() {
     
    },
    created(){
    },
    watch:{ 
      
      },
    
  });
</script>

@endpush
