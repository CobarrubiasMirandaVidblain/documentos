<div class="box-body no-padding">
	<div class="table-responsive col-md-12">
		<table class="table table-striped table-bordered" style="width: 100%" id="tablaFiltro">
            <thead>
                <th>Captura</th>
                <th>Nombre</th>
                <th>Fecha inicio</th>
                {{-- <th>Estatus</th> --}}
               
                {{-- <td>Solicitar actualización</td> --}}
            </thead>
            <tbody>
               @foreach ($listaSolicitudes as $solicitud)
               		<tr>
               			<td>{{$solicitud->userCaptura}}</td>
               			<td>{{$solicitud->nombreEvento}}</td>
               			<td>{{$solicitud->fechaEvento}}</td>
               			{{-- <td>{{$solicitud->estatus->status}}</td> --}}
               			
               		</tr>
               @endforeach
            </tbody>
        </table>
	</div>	
</div>

<script type="text/javascript">
	$(document).ready( function () {
	    $('#tablaFiltro').DataTable({
	    	'language':{
			    "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			}
	    });
	} );
</script>