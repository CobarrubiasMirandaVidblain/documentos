@extends('orgeventos::layouts.master')
@push('head')
	<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet">
	<style type="text/css">
		[v-cloak] {display: none;}
	</style>
@endpush

@section('content-subtitle','')
@section('li-breadcrumbs')
    <li class="active">Solicitudes</li>
@endsection
@section('content')
<div id="solicitud">
	<div v-cloak>
		@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ADMIN'], 'orgeventos'))
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-aqua">
					<span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Eventos</span>
						<span class="info-box-number">@{{eventos}}</span>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-red">
					<span class="info-box-icon"><i class="fa fa-warning"></i></span>
					<div class="info-box-content">
							<span class="info-box-text">Urgentes</span>
							<span class="info-box-number">@{{urgentes}}</span>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-green">
					<span class="info-box-icon"><i class="fa fa-hand-o-right"></i></span>
					<div class="info-box-content">
							<span class="info-box-text">Finalizado</span>
							<span class="info-box-number">@{{finalizado}}</span>
					</div>
				</div>
			</div>

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-yellow">
					<span class="info-box-icon"><i class="fa fa-users"></i></span>
					<div class="info-box-content">
							<span class="info-box-text">Participantes</span>
							<span class="info-box-number">@{{participantes}}</span>
					</div>
				</div>
			</div>
		</div>
		@endif
		
		<div class="box box-success ">
			<div class="box-header with-border">
	  			<h3 class="box-title">Lista</h3>
	  			<div class="box-tools pull-right">
	    			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	  			</div>
			</div>
	
			<div class="box-body" >
				<div id="solicitantes_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
					<div class="dt-buttons"> 
						
					</div>
				</div>
				<br>	
				
				{{-- NUEVA TABLA  --}}
				<v-client-table ref="listEventos" :data="listEventos" :columns="columns" :options="options">
					<div slot="beforeFilter" >
						<button class="dt-button buttons-reset button-dt tool" data-toggle="modal" data-target="#modal" @click.prevent="participante = false, evento= false, resetNewEvento(), editMode=false" >
							<span> <i class="fa fa-plus"></i> </span>
						</button>
						<button class="btn btn-success" type="button"  @click="GenerarExcel()">
								<i class="fa fa-file-excel-o"></i>
								Exportar a Excel
						</button>
					</div>
					<template slot="opciones" slot-scope="props">
						<button title="Detalle" class="btn btn-info btn-sm" @click.prevent="showEvento(props.row)"> <i class="fa fa-eye"></i></button>
						<template v-if="props.estatus != 'FINALIZADO'">
							<button title="Editar" class="btn btn-warning btn-sm" @click.prevent="editarEvento(props.row)"> <i class="fa fa-edit"></i></button>
							@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ADMIN'], 'orgeventos'))
								<template v-if="props.row.participantes == 0 && props.row.estatus == 'PENDIENTE'">
									<button title="Eliminar" class="btn btn-danger btn-sm" @click.prevent="eliminarEvento(props.row)"> <i class="fa fa-trash"></i></button>
								</template>
							@endif
						</template>
					</template>
				</v-client-table>
				{{-- FIN NUEVA TABLA --}}
        {{-- <div class=" table-responsive">
          <table class="table table-bordered table-hover data-table dataTable dtr-inline" style="width: 100%" id="dt007">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Fecha inicio</th>
								<th>Estatus</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="evento in listEventos">
								<td>@{{evento.beneficio.nombre}} -  @{{evento.folio_peticion}}</td>
								<td>@{{evento.fechaEvento}}</td>
								<td>
									<label :class="evento.estatus.descripcion">@{{evento.estatus.status}}</label>
								</td>
								<td>
									<button title="Detalle" class="btn btn-info btn-sm" @click.prevent="showEvento(evento)"> <i class="fa fa-eye"></i></button>
									<template v-if="evento.estatus.status != 'FINALIZADO'">
										<button title="Editar" class="btn btn-warning btn-sm" @click.prevent="editarEvento(evento)"> <i class="fa fa-edit"></i></button>
										@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ADMIN'], 'orgeventos'))
											<template v-if="evento.participantes.length == 0">
												<button title="Eliminar" class="btn btn-danger btn-sm" @click.prevent="eliminarEvento(evento)"> <i class="fa fa-trash"></i></button>
											</template>
										@endif
									</template>
								</td>
							</tr>
						</tbody>
          </table>
					<nav>
						<ul class="pagination">
							<li v-if="pagination.current_page > 1">
								<a href="#" @click.prevent="changePage(pagination.current_page - 1)">
										<span>Atras</span>
								</a>
							</li>
			
							<li v-for="page in pagesNumber" :class="[page == isActived ? 'active':'']">
								<a href="#" @click.prevent="changePage(page)">
										@{{page}}
								</a>
							</li>
							
							<li  v-if="pagination.current_page < pagination.last_page">
								<a href="#" @click.prevent="changePage(pagination.current_page + 1)">
										<span>Siguiente</span>
								</a>
							</li>
						</ul>
					</nav>   
        </div> --}}
			</div>
		</div>
	<!-- div info del evento -->
		<div v-if="evento">
			<div class="box box-success ">
				<div class="box-header with-border">
					<div class="box-tools pull-right">
						<button @click.prevent="cerrarDetalle" type="button" class="btn btn-box-tool" ><i class="fa fa-times"></i></button>
					</div>
				</div>
			
				<div class="box-body" >
					<div class="box box-widget widget-user">
						<div class="widget-user-header bg-teal-active">
							<h3 class="widget-user-username">@{{newEvento.beneficio.nombre}}</h3>
							<h5 class="widget-user-desc">@{{newEvento.descripcionEvento}}</h5>
							<h6 class="widget-user-desc">@{{newEvento.folio}}</h6>
						</div>

						<div class="box-footer">
							<div class="row">
								<div class="col-sm-4 border-right">
									<div class="description-block">
										<h5 class="description-header"> <i class="fa fa-calendar"></i> @{{newEvento.fechaEvento}}</h5>
									</div>
								</div>

								<div class="col-sm-4 border-right">
									<div class="description-block">
										<h5 class="description-header"><i class="fa fa-users"></i> @{{listaParticipantes.length}}</h5>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="description-block">
										<h5 class="description-header"><span :class="newEvento.estatus.descripcion"> @{{newEvento.estatus.status}}</span></h5> 	
									</div>
								</div>
								<hr>

								<div class="col-sm-4">
									<div class="description-block">
										<h5 class="description-header">Municipio: <br>@{{nombreMunicipio}}</h5>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="description-block">
										<h5 class="description-header">Localidad: <br> @{{nombreLocalidad}}</h5>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="description-block">
										<h5 class="description-header">Calle:<br> @{{newEvento.calle}}</h5>
									</div>
								</div>

								<div class="col-sm-2">
									<div class="description-block">
										<h5 class="description-header">Numero Ext. <br> @{{newEvento.numExt}}</h5>
									</div>
								</div>

								<div class="col-sm-2">
									<div class="description-block">
										<h5 class="description-header">Numero Int. <br> @{{newEvento.numInt}}</h5>
									</div>
								</div>

								<div class="col-sm-2">
									<div class="description-block">
										<h5 class="description-header">Codigo Postal:<br> @{{newEvento.codigoPostal}}</h5>
									</div>
								</div>

								<div class="col-sm-3">
									<div class="description-block">
										<h5 class="description-header">Colonia: <br> @{{newEvento.colonia}}</h5>
									</div>
								</div>	

								<div class="col-sm-3">
									<div class="description-block">
										<h5 class="description-header">Tipo evento: <br>@{{newEvento.tipo_evento_id}}</h5>
									</div>
								</div>

							</div>
						</div>
				 	</div>
					<!-- Tabla -->
					<br>
					<div class="box-header with-border">
					  <h3 class="box-title">Lista de participantes</h3>
					</div>
					<br>
					<div class="input-group input-group-sm col-sm-4 pull-right">
						<input type="text"  class="form-control" v-model="searchParticipante">
						<span class="input-group-btn">
								<button  class="btn btn-info btn-flat" @click.prevent="participantesFilter()">Buscar</button>
						</span>
					</div>

					<div id="solicitantes_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
						<div class="dt-buttons">   
						  
						</div>
					</div>
					<br>	
					<v-client-table ref="listaParticipantes" :data="listaParticipantes" :columns="columns2" :options="options">
							<div slot="beforeFilter" >
									<template v-if="newEvento.estatus.status != 'FINALIZADO' && newEvento.estatus.status != 'CANCELADO'">
											<button class="dt-button buttons-reset button-dt tool" @click.prevend="participante = true, clase(), resetNewParticipante()"  data-toggle="modal" data-target="#modal">
												<span><i class="fa fa-user-plus"></i> </span>
											</button> 
										</template> 
								<button class="btn btn-success" type="button"  @click="GenerarExcelParticipantes()">
										<i class="fa fa-file-excel-o"></i>
										Exportar a Excel
								</button>
							</div>
							<template slot="opciones" slot-scope="props">
								<button  v-if="newEvento.estatus.status != 'FINALIZADO' && newEvento.estatus.status != 'CANCELADO'" class="btn btn-danger btn-sm" @click.prevent="deleteParticipante(props.row)"> <i class="fa fa-trash"></i></button>
							</template>
						</v-client-table>
					{{-- <div class=" table-responsive">
						<table class="table table-bordered table-hover data-table dataTable dtr-inline" style="width: 100%">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Primer Apellido</th>
									<th>Segundo Apellido</th>
									<th>Fecha nacimiento</th>
									<th>Genero</th>
									<th>Telefono</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(participante, index) in listaParticipantes" v-show="(pag - 1) * NUM_RESULTS <= index  && pag * NUM_RESULTS > index">
									<th>@{{participante.nombre}}</th>
									<th>@{{participante.primer_apellido}}</th>
									<th>@{{participante.segundo_apellido}}</th>
									<th>@{{participante.fecha_nacimiento}}</th>
									<th>@{{participante.genero}}</th>
									<th>@{{participante.numero_local}}</th>
									<th>
										<template v-if="newEvento.estatus.status != 'FINALIZADO'">
											<button  class="btn btn-danger btn-sm" @click.prevent="deleteParticipante(participante)"> <i class="fa fa-trash"></i></button>
										</template>
									</th>
								</tr>
							</tbody>
						</table>
						<nav aria-label="Page navigation" class="text-right">
							<ul class="pagination text-center">
								<li>
									<a href="#" aria-label="Previous" v-show="pag != 1" @click.prevent="pag -= 1">
										<span aria-hidden="true">Anterior</span>
									</a>
								</li>
								<li>
									<a href="#" aria-label="Next" v-show="pag * NUM_RESULTS / listaParticipantes.length < 1" @click.prevent="pag += 1">
										<span aria-hidden="true">Siguiente</span>
									</a>
								</li>
							</ul>
						</nav>
					</div> --}}
				</div>
			</div>
		</div> <!-- Fin div evento -->
		{{-- Inicio modal --}}
	
		<div class="modal fade" id="modal" style="display: none;">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4 v-if="!participante" class="modal-title">@{{tituloModal}} <small>@{{newEvento.beneficio.nombre}}</small></h4>
						<h4 v-if="participante" class="modal-title">Agregar participante al evento <small>@{{newEvento.beneficio.nombre}}</small></h4>
					</div>
					<div class="modal-body">
						<div v-if="!participante"> <!-- nueo evento -->
							<form autocomplete="off">
								<div class="row">

									<div class="col-md-4">
										<div :class="{ 'form-group': true, 'has-error': errors.has('fechaEvento') }">
											<label for="fecha">Fecha</label>
											<vuejs-datepicker v-validate="'required:true'" v-model="newEvento.fechaEvento" id="fechaEvento" name="fechaEvento"></vuejs-datepicker>
										</div>
										<span class="help-block" v-text="errors.first('fechaEvento')"></span>
									</div>

									<div class="col-md-8">
										<div :class="{ 'form-group': true, 'has-error': errors.has('nombre') }">
											<label for="nombre">Nombre Beneficio</label>
											<multiselect v-model="newEvento.nombreEvento" deselect-label="Eliminar" label="nombre" placeholder="Seleccione beneficio" :options="listaBeneficios" :searchable="true" :allow-empty="false" name="nombre" v-validate="'required:true'"></multiselect>
										</div>
										<span class="help-block" v-text="errors.first('nombre')"></span>
									</div>
									
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-6">
												<div :class="{ 'form-group': true, 'has-error': errors.has('municipio') }">
													<label for="municipio">Municipio</label>
													{{-- <multiselect v-model="municipioselect" deselect-label="Eliminar" label="nombre" placeholder="Seleccione municipio" :options="listMunicipios" :searchable="true" :allow-empty="false" name="municipio" v-validate="'required:true'"></multiselect> --}}
													<multiselect :class="errors.first('municipio') ? 'has-error': ''" v-model="municipioselect" :options="listaMunicipios" value="'id'" label="nombre" name="municipio" v-validate="'required'" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="buscarMunicipio" :block-keys="['Delete']" placeholder="Selecciona">
														<template slot="noOptions">Comience a escribir para buscar.</template>
														<template slot="noResult"><em style="color: #F2698D;">No tenemos registro con lo que estas buscando</em></template>
													</multiselect>
												</div>
												<span class="help-block" v-text="errors.first('municipio')"></span>
											</div>

											<div class="col-md-6">
												<div :class="{ 'form-group': true, 'has-error': errors.has('localidad') }">
													<label for="localidad">Localidad</label>
													{{-- <multiselect v-model="localidadselect"  deselect-label="Eliminar" label="nombre" placeholder="Seleccione localidad" :options="listLocalida" :searchable="true" :allow-empty="false" name="localidad" v-validate="'required:true'">
														<template slot="singleLabel" slot-scope="{ option }"><strong>@{{ option.nombre }}</strong></template>
													</multiselect> --}}
													<multiselect
														name="Localidad"
														v-model="localidadselect" 
														:options="listaLocalidad" 
														:clear-on-select="true"
														:preserve-search="false"
														placeholder="Busca o agrega la localidad"
														label="nombre"
														track-by="nombre"
														:internal-search="false"
														@search-change="buscarLocalidad"
														tag-placeholder="Enter para agregar nueva presentación"
														v-validate="'required'"
														:block-keys="['Delete']"
														>
															<template slot="noOptions">Comience a escribir para buscar.</template>
															<template slot="noResult"><em style="color: #F2698D;">No tenemos registro con lo que estas buscando</em></template>
													</multiselect>
												</div>
												<span class="help-block" v-text="errors.first('localidad')"></span>
											</div>
										</div>
									</div>
									
									<div class="col-md-4">
										<div :class="{ 'form-group': true, 'has-error': errors.has('colonia') }">
											<label>Colonia</label>
											<input type="text" class="form-control" placeholder="Colonia" v-model="newEvento.colonia" v-validate="'required:true'" name="colonia">
										</div>
										<span class="help-block" v-text="errors.first('colonia')"></span>
									</div>

									<div class="col-md-4">
										<div :class="{ 'form-group': true, 'has-error': errors.has('codigoPostal') }">
											<label for="cp">Codigo Postal</label>
											<input type="text" class="form-control" placeholder="Codigo Postal" v-model="newEvento.codigoPostal" v-validate="{ required: true, digits:5 }" name="codigoPostal">
										</div>
										<span class="help-block" v-text="errors.first('codigoPostal')"></span>
									</div>

									<div class="col-md-4">
										<div :class="{ 'form-group': true, 'has-error': errors.has('calle') }">
											<label for="calle">Calle</label>
											<input name="calle" type="text" class="form-control" placeholder="Calle" v-model="newEvento.calle" v-validate="'required:true'">
										</div>
										<span class="help-block" v-text="errors.first('calle')"></span>
									</div>

									<div class="col-md-12">
										<div class="row">
											<div class="col-md-2">
												<div class="form-group">
													<label for="numInt">Numero Int.</label>
													<input type="text" class="form-control" placeholder="Numero" v-model="newEvento.numInt">
												</div>
											</div>

											<div class="col-md-2">
												<div class="form-group">
													<label for="numExt">Numero Ext.</label>
													<input type="text" class="form-control" placeholder="Numero" v-model="newEvento.numExt">
												</div>
											</div>

											<div class="col-md-4">
												<div :class="{ 'form-group': true, 'has-error': errors.has('evento') }">
													<label for="evento">Tipo Evento</label>
													<select name="evento" class="form-control" v-model="newEvento.tipo_evento_id" v-validate="'required:true'">
														<option value="">Selecciona Tipo</option>
														<option  v-for="tipo in tipoEventos" :value="tipo.id">@{{tipo.nombre}}</option>
													</select>
												</div>
												<span class="help-block" v-text="errors.first('evento')"></span>
											</div>

											<div class="col-md-4">
												<div :class="{ 'form-group': true, 'has-error': errors.has('solicitud') }">
													<label for="solicitud">Es de una Solicitud</label>
													<select name="solicitud" class="form-control" v-validate="'required:true'">
														<option @click.prevent="newEvento.con_folio = false">No</option>
														<option @click.prevent="newEvento.con_folio = true">Si</option>
													</select>
												</div>
												<span class="help-block" v-text="errors.first('solicitud')"></span>
											</div>

											<div class="col-md-12" v-if="newEvento.con_folio">
												<div :class="{ 'form-group': true, 'has-error': errors.has('folioPeticion') }">
													<label for="folioPeticion">Folio de la solicitud</label>
													<input type="text" v-model="newEvento.folio_peticion" v-validate="newEvento.con_folio ? 'required:true':'required:false'" class="form-control">
												</div>
												<span class="help-block" v-text="errors.first('folioPeticion')"></span>
											</div>

											<template v-if='editMode'>
												@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ADMIN'], 'orgeventos'))												
													<div class="col-md-4" v-if="newEvento.estatus.status != 'CANCELADO'">
														<label for="estatus">Estatus actual : </label> 
														<label :class="newEvento.estatus.descripcion">@{{newEvento.estatus.status}}</label><br>
														<template v-if="newEvento.estatus.status == 'PENDIENTE'">
															<button class="btn btn-info" @click.prevent="changeStatus(8)">Finalizado</button>
															<button class="btn btn-danger" @click.prevent="changeStatus(7)">Cancelar</button>
														</template>
													</div>
												@endif
											</template>
										</div>
									</div>
									
									<div class="col-md-12">
										<div :class="{ 'form-group': true, 'has-error': errors.has('descripcion') }">
											<label class="control-label" for="descripcion">Descripción</label>
											<textarea class="form-control" rows="3" name="descripcion" v-model="newEvento.descripcionEvento" v-validate="'required:true'"></textarea>
										</div>
										<span class="help-block" v-text="errors.first('descripcion')"></span>
									</div>

								</div>
							</form>
						</div>
						<!-- div nueva persona -->
						<div v-if="participante">
							<form autocomplete="off">
								<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-12">
												<h3>Buscar persona <small></small></h4>
												<div class="row">
													<div class="col-md-10">
														<input type="text" name="curpSearch" class="form-control" v-model="curpSearch">
													</div>
													<div class="col-md-2">
														<button class="btn btn-primary" @click.prevent="btnCurpSearch()">Buscar</button>
													</div>
													<template v-if="listaOpcionPersonas.length">
														<div class="col-md-12">
															<div class=" table-responsive">
																<table class="table table-bordered table-hover data-table dataTable dtr-inline" style="width: 100%">
																	<thead>
																		<tr>
																				<th>Nombre</th>
																				<th>Primer Apellido</th>
																				<th>Segundo Apellido</th>
																				<th>Fecha nacimiento</th>
																				<th>Opciones</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr v-for="person in listaOpcionPersonas">
																			<th>@{{person.nombre}}</th>
																			<th>@{{person.primer_apellido}}</th>
																			<th>@{{person.segundo_apellido}}</th>
																			<th>@{{person.fecha_nacimiento}}</th>
																			<th>
																				<button class="btn btn-default" @click.prevent="seleccionPersona(person)">Seleccionar</button> 
																			</th>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
													</template>
												</div>
											</div>
											<div class="col-md-4">
												<div :class="{ 'form-group': true, 'has-error': errors.has('nombre') }">
													<label>Nombre</label>
													<input type="text" class="form-control" placeholder="Nombre" v-model="newParticipante.nombre" v-validate="'required:true'" name="nombre">
												</div>
												<span class="help-block" v-text="errors.first('nombre')"></span>
											</div>

											<div class="col-md-4">
												<div :class="{ 'form-group': true, 'has-error': errors.has('primerApellido') }">
													<label>Primer apellido</label>
													<input type="text" class="form-control" placeholder="Primer apellido" v-model="newParticipante.primer_apellido" v-validate="'required:true'" name="primerApellido">
												</div>
												<span class="help-block" v-text="errors.first('primerApellido')"></span>
											</div>

											<div class="col-md-4">
												<div :class="{ 'form-group': true, 'has-error': errors.has('segundoApellido') }">
													<label>Segundo apellido</label>
													<input type="text" class="form-control" placeholder="Segundo apellido" v-model="newParticipante.segundo_apellido" v-validate="'required:true'" name="segundoApellido">
												</div>
												<span class="help-block" v-text="errors.first('segundoApellido')"></span>
											</div>
										</div>
									</div>

									<div class="col-md-3">
										<div :class="{ 'form-group': true, 'has-error': errors.has('fecha_nacimiento') }">
											<label for="fecha_nacimiento">Fecha nacimiento</label>
											<vuejs-datepicker v-validate="'required:true'" v-model="newParticipante.fecha_nacimiento" id="fechaNacimiento" name="fecha_nacimiento"></vuejs-datepicker>
										</div>
										<span class="help-block" v-text="errors.first('fecha_nacimiento')"></span>
									</div>

									<div class="col-md-3">
										<div :class="{ 'form-group': true, 'has-error': errors.has('telefono') }">
											<label for="telefono">Telefono</label>
											<input type="text" class="form-control" placeholder="Telefono" v-model="newParticipante.numero_local" v-validate="{ digits:10}" name="telefono">
										</div>
										<span class="help-block" v-text="errors.first('telefono')"></span>
									</div>

									<div class="col-md-3">
										<div :class="{ 'form-group': true, 'has-error': errors.has('genero') }">
											<label>Genero</label>
											<select class="form-control" v-model="newParticipante.genero" v-validate="'required:true'" name="genero">
												<option value="H">Masculino</option>
												<option value="M">Femenino</option>
											</select>
										</div>
										<span class="help-block" v-text="errors.first('genero')"></span>
									</div>

									<div class="col-md-3">
										<div :class="{ 'form-group': true, 'has-error': errors.has('correo') }">
											<label>Correo</label>
											<input type="email" name="correo" v-model="newParticipante.email" class="form-control" v-validate="'required:true'">
										</div>
										<span class="help-block" v-text="errors.first('correo')"></span>
									</div>	

								</div>
							</form>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
						<button v-if="!participante && !editMode" type="button" class="btn btn-primary" @click.prevend="saveEvento()">Guardar</button>
						<button v-if="participante && !editMode" type="button" class="btn btn-primary" @click.prevend="saveParticipante()">Guardar</button>
						<button v-if="!participante && editMode && newEvento.estatus.status == 'PENDIENTE'"	 type="button" class="btn btn-primary" @click.prevend="actualizarEvento()">Actualizar</button>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>
@stop

@push('body')
	<script type="text/javascript" src="{{ asset('bower_components/vue/vue-multiselect.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/vue/vee-validate.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/vue/vuejs-datepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('eventos/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/vue/vue-tables-2.min.js') }}"></script>
	{{-- <script type="text/javascript" src="{{ asset('eventos/evento.js') }}"></script> --}}
	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		Vue.use(VeeValidate);
		Vue.use(VueTables.ClientTable);
		new Vue({
		components: {
			Multiselect: window.VueMultiselect.default,
      vuejsDatepicker
    },
		el:'#solicitud',
		data:{
			listaOpcionPersonas	:[],
			curpSearch 		: null,
			participantes 	: null,
			urgentes 		: null,
			finalizado 		: null,
			eventos 		: null,
			evento			: false,
			participante 	: false,
			listEventos 	: [],
			tipoEventos 	: null,
			listaMunicipios 	: [],
			listaLocalidad 	: [],
			listStatus 		: [],
			listaBeneficios:[],
			municipioselect : null,
			localidadselect : null,
			nombreMunicipio : null,
			nombreLocalidad : null,
			newEvento		: {
				folio 				: 0,
				nombreEvento	:null,
				beneficio 		: {},
				fechaEvento 		: null,
				estatus 			: {},
				estatus_id			: null,
				municipio_id		: null,
				localidad_id 		: null,
				colonia 			: null,
				codigoPostal 		: null,
				calle 				: null,
				numExt				: null,
				numInt				: null,
				tipo_evento_id		: null,
				descripcionEvento	: null,
				con_folio : false,
				folio_peticion:null
			},
			newParticipante 	:{
				nombre 				: null,
				primer_apellido 	: null,
				segundo_apellido 	: null,
				fecha_nacimiento 	: null,
				genero 				: null,
				numero_local		: null,
				idEvento 			: null,
				email 				: null,
				curp 				: null
			},
			listaParticipantes 		:[],
			tituloModal 			:'Nuevo',
			editMode 				:false,
			pagination:{
				'total'         :0, 
				'current_page'  :0, 
				'per_page'      :0, 
				'last_page'     :0, 
				'from'          :0, 
				'to'            :0
			},
			offset              :3,
			NUM_RESULTS: 10, // Numero de resultados por página
			pag: 1,
			searchParticipante 	:null,
			searchEvento		:null,
			columns: ['folio','nombre','fecha','estatus','opciones'],
			columns2: ['nombre','primerApellido','segundoApellido','fechaNacimiento','genero','telefono','opciones'],
			options: {
				filterable: ["folio", "nombre", "fecha","estatus","primerApellido","segundoApellido","fechaNacimiento","genero","telefono"],
				texts: {
					count:
						" {from} a {to} de {count} registros|{count} Registros|1 Registro",
					first: "First",
					last: "Last",
					filter: "Filtrar:",
					filterPlaceholder: "Buscar",
					limit: "Registros:",
					page: "Pagina:",
					noResults: "No se encontraron coincidencias",
					filterBy: "Filtrar",
					loading: "Cargando...",
					defaultOption: "Seleccionar {column}",
					columns: "Columnas"
				},
				highlightMatches: true,
				filterByColumn: true
			}
		},
		computed:{
			
    },
		methods:{
			getDatos(){
				let me = this;
				var url = "/orgeventos/eventos/datos";
				block();
				axios.get(url).then(function(r){
					unblock();
					if(r.status === 200)
					{
						me.participantes	= r.data.participantes
						me.urgentes 			= r.data.urgentes 
						me.finalizado 		= r.data.finalizado 
						me.eventos	 			= r.data.eventos 
					}
					else
						me.getDatos();
				}).catch(function(error){
					me.notificacion('error',error.message);
				})
			},
		
			showEvento(evento){
				block();
				let me = this;
				me.evento = true;
				me.listaParticipantes = [];
				var url = "/orgeventos/solicitudes/show";
				axios.post(url,{idEvento:evento.id}).then(function (response){
					unblock();
					if(response.status === 200)
					{
						me.newEvento 								= response.data
						me.nombreMunicipio 					= response.data.municipio.nombre
						me.newEvento.tipo_evento_id = response.data.tipo_evento.descripcion
						// me.listaParticipantes 			= response.data.participantes
						me.listaParticipantes 			= response.data.participantes.map(e=>({id:e.id,nombre:e.nombre,fecha:e.fecha_nacimiento, primerApellido:e.primer_apellido, segundoApellido:e.segundo_apellido,fechaNacimiento:e.fecha_nacimiento,genero:e.genero, telefono:e.telefono, curp:e.curp}));
						me.nombreLocalidad 					= response.data.localidad.nombre
					}	
					else
						me.showEvento();
				}).catch(function (error){
					me.notificacion('error',error.message);
				})
			},
			participantesList(id){
				let me = this;
				me.listaParticipantes = [];
				url = "/orgeventos/solicitudes/lista";
				axios.post(url,{id:id}).then(function (response) {
					me.listaParticipantes 	=  response.data.lista.participantes.map(e=>({id:e.id,nombre:e.nombre,fecha:e.fecha_nacimiento, primerApellido:e.primer_apellido, segundoApellido:e.segundo_apellido,fechaNacimiento:e.fecha_nacimiento,genero:e.genero, telefono:e.telefono}));
				}).catch(function (error) {
					me.notificacion('error',error.message);
				});
			},
			cerrarDetalle(){
				let me = this;
				me.evento = false;
			},
			modalPersona(){
				let me = this;
				me.participante = true;
				$("modal").modal('show')
			},
			listaEventos(){
				block();
				var url = "/orgeventos/solicitudes/listEventos";
				let me = this;
				axios.get(url).then(function (response) {
					// return console.log(response.data);
					unblock();
					if(response.status === 200){
						me.listEventos 	= response.data.lista.length ? response.data.lista.map(e=>({id:e.id,folio:e.folio,nombre:e.folio_peticion != null ? `${e.beneficio.nombre} - ${e.folio_peticion}` :`${e.beneficio.nombre}`,fecha:e.fechaEvento,estatus:e.estatus.status,participantes:e.participantes.length})): [];
					}
					else
						me.listaEventos();					
				}).catch(function (error) {
					me.notificacion('error',error.message);
					// location.reload();
				})
			},
			notificacion(tipo,mensaje){
				unblock();
				swal({position: 'top-end',type: tipo,	title: mensaje,	showConfirmButton: false,	timer: 1700})
			},
			getCatEventos(){
				let me = this;
				var url = "/orgeventos/solicitudes/getCatEventos"
				axios.get(url).then(function (response) {
					if(response.status === 200 ){
						me.tipoEventos = response.data.lista
						me.listStatus = response.data.listStatus
						me.listaBeneficios = response.data.beneficios
					}
					else
						me.getCatEventos();
				}).catch(function (error) {
					me.notificacion('error','Error al consultar tipos de eventos');
				})
			},
			buscarMunicipio(search){
				this.listaMunicipios = []
				if(search.length < 3) return;
				axios.get(`/dcredencial/municipios/${search}`).then(response => { 
					this.listaMunicipios = response.data.data; 
				}).catch(error => { console.log(error) });
			},
			resetNewEvento(){
				let me = this;
			    me.newEvento = {
					folio 				: 0,
					nombreEvento 		: '',
					beneficio				:{},
					fechaEvento 		: '',
					estatus 			: '',
					municipio_id		: '',
					localidad_id 		: '',
					colonia 			: '',
					codigoPostal 		: '',
					calle 				: '',
					numExt				: '',
					numInt				: '',
					tipo_evento_id		: '',
					descripcionEvento	: '',
					con_folio : false,
					folio_peticion:null
				}
				me.municipioselect = '',
				me.localidadselect = '',
				me.nombreLocalidad = '',
				me.nombreMunicipio = '',
			    me.$validator.reset();
			    me.tituloModal = 'Nuevo'
			},
			resetNewParticipante(){
				let me = this;
				me.newParticipante 	= {
					nombre 						: '',
					primer_apellido 	: '',
					segundo_apellido 	: '',
					fecha_nacimiento 	: '',
					genero 						: '',
					numero_local			: '',
					idEvento 					: '',
					email 						: '',
					curp 							: '',
					beneficio					:''
				}
				me.listaOpcionPersonas = [],
				me.curpSearch = "",
				me.$validator.reset();
				me.editMode = false

			},
			clase()	{
				$("#fechaEvento, #fechaNacimiento").addClass( "form-control" );
			},
			saveEvento(){
				let me = this;
				this.$validator.validateAll().then((result) => {
					if(result) {
						block();
						var url = "/orgeventos/solicitudes/registrar"
						axios.post(url,me.newEvento).then(function (response) {
							unblock();
							if(response.data.success){
								me.getDatos();  
								me.listaEventos();
								me.notificacion('success','Se guardo correctamente');
								$('#modal').modal('toggle');
								me.resetNewEvento();
							}
							else 
								me.notificacion('info',response.data.msj)
						}).catch(function (error){
							unblock();
							me.notificacion('error',error.message)
							console.log(error)
						})
					}
				});
			},
			eliminarEvento(evento){
				let me = this;
				var url = "/orgeventos/solicitudes/eliminar/"+evento.id;
				// return console.log(evento);
				swal({
				  title: 'Estas seguro de eliminar '+evento.nombre,
				  text: "Ya no podras ver su informacion!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, eliminar!'
				}).then((result) => {
				  if (result.value) {
				  	axios.delete(url).then(function(response){
				  		if (response.data.success) {
				  			swal(
						      'Eliminado!',
						      'Se elimino correctamente',
						      'success'
						    )
					  		me.listaEventos();
				  		}else{
				  			me.notificacion('error',response.data.message)
				  		}
				  		
				  	}).catch(function(error){
				  		me.notificacion('error',error.message)
				  	})
				  }
				})
			},
			editarEvento(evento){
				let me = this;
				me.resetNewEvento();
				me.participante = false;
				me.evento = false;
				var url = "/orgeventos/solicitudes/show";
				axios.post(url,{idEvento:evento.id}).then(function (response){
					unblock();
					if(response.status === 200)
					{
						me.newEvento 								= response.data
						me.municipioselect 					= response.data.municipio
						me.newEvento.tipo_evento_id = response.data.tipo_evento.descripcion
						me.listaParticipantes 			= response.data.participantes
						me.newEvento.nombreEvento   = response.data.beneficio
						me.newEvento.tipo_evento_id = response.data.tipo_evento.id
						me.tituloModal = "";
						if(response.data.folio_peticion != null)
							me.newEvento.con_folio = true
						setTimeout(function(){
							me.localidadselect = response.data.localidad
							me.newEvento.localidad_id = response.data.localidad.id
						}, 1000);
						
							$('#modal').modal('toggle');
						}	
					else
						me.showEvento();
				}).catch(function (error){
					me.notificacion('error',error.message);
				})
				me.editMode = true
			},
			actualizarEvento(){
				let me = this;
				var url = "/orgeventos/solicitudes/actualizar";
				axios.put(url, me.newEvento).then(function (response){
					if (response.data.success) {
						$('#modal').modal('toggle');
						me.notificacion('success','Se guardo correctamente');
						me.listaEventos();
						me.editMode = false;
					}else{
						me.notificacion('error',response.data.message)
					}

				}).catch(function(error){
					me.notificacion('error',error.message)
				})
			},
			saveParticipante(){
				let me = this;				
				this.$validator.validateAll().then((result) => {
					if(result) {
						var fecha = JSON.stringify(me.newParticipante.fecha_nacimiento);
						var curp = me.newParticipante.curp != '' ? me.newParticipante.curp : me.newParticipante.curp = me.newParticipante.primer_apellido.substring(0, 2)+me.newParticipante.segundo_apellido.substring(0,1)+me.newParticipante.nombre.substring(0,1)+fecha.substring(3, 5)+fecha.substring(6,8)+fecha.substring(9,11)+me.newParticipante.genero;
						let participantes = this.listaParticipantes;
						participantes = participantes.filter(participante => {
							return participante.curp.toLowerCase().match(curp.toLowerCase());
						})
						if (participantes.length > 0) {
							swal("persona ya registrada en el curso")
							return;
						}
						me.newParticipante.idEvento = me.newEvento.id;
						me.newParticipante.curp = curp;
						me.newParticipante.beneficio = me.newEvento.beneficio.id;
						block();
						var url = "/orgeventos/participantes/registrar"
						axios.post(url,me.newParticipante).then(function (response) {
							unblock();
							if(response.data.success){
								me.notificacion('success','Se guardo correctamente');
								$('#modal').modal('toggle');
								me.resetNewParticipante();
								me.participantesList(me.newEvento.id);
								me.listaEventos();
							}
							else
								me.notificacion('error',response.data)
							
						}).catch(function (error){
							unblock();
							me.notificacion('error',error.data)
						})
					}
				});
			},
			deleteParticipante(participante){
				let me = this;
				var url = "/orgeventos/solicitudes/quitar"
				swal({
				  title: 'Estas seguro de quitar a '+participante.nombre, 
				  text: "Ya no estara en el curso!",
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, quitar!'
				}).then((result) => {
				  if (result.value) {
						participante.evento_id = me.newEvento.id;
				  	axios.post(url,participante).then(function(response){
				  		if (response.data.success) {
				  			swal(
						      'Eliminado!',
						      'Se elimino correctamente',
						      'success'
						    )
					  		me.participantesList(response.data.evento);
				  		}else{
				  			me.notificacion('error',response.data.message)
				  		}
				  		
				  	}).catch(function(error){
				  		me.notificacion('error',error.message)
				  	})
				    
				  }
				})

			},
			editParticipante(persona){
				swal("editarParticipante")
			},
			showParticipante(){
				swal("showParticipante")
			},
			participantesFilter() {
				let listaParticipantes = this.listaParticipantes;
				if(this.searchParticipante)
					{
						listaParticipantes = listaParticipantes.filter(participante => {
							let search = this.searchParticipante.toLowerCase();
								return participante.nombre.toLowerCase().match(search) || (participante.primer_apellido).match(search) || participante.segundo_apellido.toLowerCase().match(search) || participante.fecha_nacimiento.toLowerCase().match(search) || participante.genero.toLowerCase().match(search) || participante.numero_local.toLowerCase().match(search);
							});
					}
					if (listaParticipantes.length > 0) {
						this.pag=1 
						return this.listaParticipantes = listaParticipantes;
					}

					else return this.notificacion('info','Sin resultados')
			},
			changeStatus(valor){
				let me = this;
				swal({
					title: 'Estas seguro?',
					// text: "You won't be able to revert this!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: (valor == 8) ?'Si, Finalizar!':'Si, Cancelar!',
					allowOutsideClick: false,
					allowEscapeKey: 	false
				}).then((result) => {
					if (result.value) {
						block();
						var url = "/orgeventos/eventos/updateStatus";
							axios.post(url, {dato:valor, id:me.newEvento.id}).then(function(response){
								unblock();	   
								$('#modal').modal('toggle');
						me.listaEventos();     		
								me.notificacion('success', 'Se realizo correctamente el cambio')
							}).catch(function(error){
								me.notificacion('error', 'Problemas de conexion, intente de nuevo')
							})		  	
					}
				})
			},
			btnCurpSearch(){
				let me = this;
				if(me.curpSearch.length)
				{
					block();
					var url = "/orgeventos/solicitudes/busquedaCURP";
					axios.post(url, {curp:me.curpSearch}).then(function(response){
					unblock();	   
						if (response.data.data.length >0) 
							me.listaOpcionPersonas = response.data.data
						else{
							me.listaOpcionPersonas = [];
							me.notificacion('info', 'Sin resultados')
						}
					}).catch(function(error){
						me.notificacion('error', 'Problemas de conexion, intente de nuevo')
					})		
				}
				else
					me.notificacion('error', 'CURP no valida')
			},
			seleccionPersona(persona)
			{
				let me = this;
				me.newParticipante = persona;
			},
			GenerarExcel() {
					block();
					var data = {};
					data.titulo ="Reporte de Eventos",
    			data.subtitulo= "Lista de Eventos",
					data.cabeceras=Object.keys(this.$refs.listEventos.allFilteredData[0]).map(e=>e.replace("_"," ")).map(e=>e.toUpperCase());
					data.datos=this.$refs.listEventos.allFilteredData.map(e=>Object.values(e));
					jsreport.serverUrl = 'http://187.157.97.110:3001';
					var request = {
							template:{
									shortid: "HJ5VttAyS"
							},
							data: data
					};
					jsreport.renderAsync(request).then(function(res) {
									res.download(`REPORTE.xlsx`)
					unblock();
					 });
				},
				GenerarExcelParticipantes(){
					block();
					var data = {};
					data.titulo ="Reporte de Participantes",
    			data.subtitulo= this.newEvento.beneficio.nombre,
					data.cabeceras=Object.keys(this.$refs.listaParticipantes.allFilteredData[0]).map(e=>e.replace("_"," ")).map(e=>e.toUpperCase());
					data.datos=this.$refs.listaParticipantes.allFilteredData.map(e=>Object.values(e));
					jsreport.serverUrl = 'http://187.157.97.110:3001';
					var request = {
							template:{
									shortid: "HJ5VttAyS"
							},
							data: data
					};
					jsreport.renderAsync(request).then(function(res) {
									res.download(`REPORTE.xlsx`)
					unblock();
					 });
				},
			buscarLocalidad(search){
				let me = this;
				if(this.municipioselect.hasOwnProperty('id')){
					axios.get(`/dcredencial/municipios/${me.municipioselect.id}/localidades/${search}`).then(response => {
						me.listaLocalidad = response.data.data;
					}).catch(error => { console.log("error") });
				}
			},
		},
		watch:{
			municipioselect(){
				let me = this;
				if (me.municipioselect.hasOwnProperty('id')) 
          me.newEvento.municipio_id = me.municipioselect.id
			},
			localidadselect(){
				let me = this;
				if (me.localidadselect.hasOwnProperty('id')) 
          me.newEvento.localidad_id = me.localidadselect.id
			},
			// 'newEvento.municipio_id':function(){
			// 	let me = this;
			// 	if(me.newEvento.municipio_id != ""){ //^\d+$
			// 		var url ="/dcredencial/listLocalidad/"+me.newEvento.municipio_id
			// 		axios.get(url).then(function (response) {
			// 			me.newEvento.localidad_id=""
			// 			me.localidadselect = ""
			// 			me.listLocalida=response.data.data
			// 		}).catch(function (error) {
			// 			me.notificacion('error','Error al consultar localidades');
			// 		})
			// 	}
			// },			
    },
		mounted (){
			let me = this;
			me.listaEventos();
			me.getCatEventos();
			me.getDatos();
			$("#fechaEvento, #fechaNacimiento").addClass( "form-control" );	
		}
	})
	</script>
@endpush