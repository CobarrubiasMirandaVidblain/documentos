let mix = require('laravel-mix');

mix.js('Resources/assets/js/app.js', './../../public/adopciones/js')
  .sass('Resources/assets/sass/app.scss', './../../public/adopciones/css')
  .setPublicPath('./../../public/adopciones');

