<?php

Route::group(['middleware' => 'web', 'prefix' => 'adopcion', 'namespace' => 'Modules\Adopcion\Http\Controllers'], function()
{
    Route::get('/', 'AdopcionController@index');

    Route::get('catalogos/escolaridades/index', 'CatalogosController@escolaridades');
    Route::get('catalogos/paises/index', 'CatalogosController@paises');
    Route::get('catalogos/estados/index', 'CatalogosController@estados');
    Route::get('catalogos/municipios/{estado}', 'CatalogosController@municipios');

    Route::get('catalogos/etnias/index', 'CatalogosController@etnias');
    Route::get('catalogos/discapacidades/index', 'CatalogosController@discapacidades');
    Route::get('catalogos/enfermedadescronicas/index', 'CatalogosController@enfermedadescronicas');

    Route::get('catalogos/casashogares/index', 'CatalogosController@casashogares');
    Route::get('catalogos/motivosingreso/index', 'CatalogosController@motivosingreso');
    Route::get('catalogos/motivosegreso/index', 'CatalogosController@motivosegreso');

    Route::get('catalogos/subprocuradurias/index', 'CatalogosController@subprocuradurias');
});
