<?php

namespace Modules\Adopcion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


class AdopcionController extends Controller
{

    public function index()
    {
        return view('adopcion::index');
    }

    public function create()
    {
        return view('adopcion::create');
    }

    public function store(Request $request)
    {
    }

    public function show()
    {
        return view('adopcion::show');
    }

    public function edit()
    {
        return view('adopcion::edit');
    }

    public function update(Request $request)
    {
    }

    public function destroy()
    {
    }
}
