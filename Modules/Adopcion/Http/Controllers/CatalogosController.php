<?php

namespace Modules\Adopcion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Http\JsonResponse;

use Modules\Adopcion\Entities\Escolaridad;
use Modules\Adopcion\Entities\Pais;
use Modules\Adopcion\Entities\Estado;
use Modules\Adopcion\Entities\Municipio;

use Modules\Adopcion\Entities\Etnia;
use Modules\Adopcion\Entities\Discapacidad;
use Modules\Adopcion\Entities\Enfermedadescronicas;

use Modules\Adopcion\Entities\Casahogar;
use Modules\Adopcion\Entities\Subprocuraduria;

class CatalogosController extends Controller
{

  public function escolaridades()
  {
    return Escolaridad::get();
  }

  public function paises()
  {
    return Pais::get();
  }

  public function estados()
  {
    return Estado::get();
  }

  public function municipios( int $estado_id )
  {
    if( $estado_id == 20 )
    {
      return Municipio::get();
    }
  }

  public function etnias()
  {
    return Etnia::get();
  }

  public function discapacidades()
  {
    return Discapacidad::get();
  }

  public function enfermedadescronicas()
  {
    return Enfermedadescronicas::get();
  }

  public function casashogares()
  {
    return CasaHogar::get();
  }

  public function subprocuradurias()
  {
    return Subprocuraduria::get();
  }

  public function motivosingreso()
  {
    $motivos = collect();
    $motivos->push( [
      'header' => 'casa'
    ] );
    $motivos->push( [
      'name' => 'prueba',
      'group' => 'grupo',
      'avatar' => 'menu_book'
    ] );
    $motivos->push( [
      'divider' => 'true'
    ] );
    $motivos->push( [
      'header' => 'segundo'
    ] );
    $motivos->push( [
      'name' => 'dos',
      'group' => 'grupo',
      'avatar' => 'menu_book'
    ] );
    return $motivos;
  }

  public function motivosegreso()
  {
    return "";
  }




}
