<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
  protected $table = 'cat_entidades';
  protected $fillable = [];

    public function municipios()
    {
        return $this->hasMany('Modules\Adopcion\Entities\Municipio');
    }

}
