<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Escolaridad extends Model
{
    protected $table = 'cat_nivelesescuela';
    protected $fillable = [];

    protected $appends = ['name','group','avatar'];

    public function getNameAttribute(){
      return $this->attributes['nombre'];
    }

    public function getGroupAttribute(){
      return $this->attributes['id'];
    }

    public function getAvatarAttribute(){
      return 'school';
    }
}
