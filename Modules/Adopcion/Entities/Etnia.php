<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Etnia extends Model
{
    protected $table = 'cat_etnias';
    protected $fillable = [];
    protected $appends = ['name','group','avatar'];

    public function getNameAttribute(){
      return $this->attributes['nombre'];
    }

    public function getGroupAttribute(){
      return 'etnia de Oaxaca';
    }

    public function getAvatarAttribute(){
      return 'face';
    }
}
