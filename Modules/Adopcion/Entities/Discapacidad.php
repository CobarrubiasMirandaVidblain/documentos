<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Discapacidad extends Model
{
    protected $table = 'cat_discapacidades';
    protected $fillable = [];
    protected $appends = ['name','group','avatar'];

    public function getNameAttribute(){
      return $this->attributes['nombre'];
    }

    public function getGroupAttribute(){
      return 'disc';
    }

    public function getAvatarAttribute(){
      return 'accessible';
    }
}
