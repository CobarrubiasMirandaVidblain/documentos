<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'cat_municipios';
    protected $fillable = [];

    public function estado()
    {
        return $this->belongsTo('Modules\Adopcion\Entities\Estado');
    }
}
