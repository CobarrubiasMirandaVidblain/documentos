<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Subprocuraduria extends Model
{
  protected $table = 'adop_cat_subprocuradurias';
  protected $fillable = [];
  protected $appends = ['name','group','avatar'];

  public function getNameAttribute(){
    return $this->attributes['nombre'];
  }

  public function getGroupAttribute(){
    return $this->attributes['responsable'];
  }

  public function getAvatarAttribute(){
    return 'local_library';
  }
}
