<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Enfermedadescronicas extends Model
{
    protected $table = 'adop_cat_enfermedadescronicas';
    protected $fillable = [];
    protected $appends = ['name','group','avatar'];

    public function getNameAttribute(){
      return $this->attributes['enfermedad'];
    }

    public function getGroupAttribute(){
      return $this->attributes['descripcion'];
    }

    public function getAvatarAttribute(){
      return $this->attributes['icon'];
    }
}
