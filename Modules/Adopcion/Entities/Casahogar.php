<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Casahogar extends Model
{
    protected $table = 'adop_cat_casashogar';
    protected $fillable = [];
    protected $appends = ['name','group','avatar'];

    public function getNameAttribute(){
      return $this->attributes['nombre'];
    }

    public function getGroupAttribute(){
      return $this->attributes['responsable'];
    }

    public function getAvatarAttribute(){
      return 'account_balance';
    }
}
