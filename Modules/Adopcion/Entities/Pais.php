<?php

namespace Modules\Adopcion\Entities;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'cat_paises';

    protected $fillable = [];

    protected $appends = ['name','group','avatar'];

    public function getNameAttribute(){
      return $this->attributes['nombre'];
    }

    public function getGroupAttribute(){
      return $this->attributes['codigo'];
    }

    public function getAvatarAttribute(){
      return 'flag';
    }

}
