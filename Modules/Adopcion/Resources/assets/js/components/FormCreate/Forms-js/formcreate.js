export default {
  data() {
    return {
      step: 3,
      petition_id: null,
      uploadFieldName: 'file',

      buffer: 100,

      registration: {
        anonymous: null,
        name: "",
        firstname: "",
        secondname: "",
        dateborn: null,
        gender: null,
        relationship: null,
        age: null,
        numerotelefono: null,
        correo: null,

        via: null,
        addressestado: null,
        addressmunicipio: null,
        addresscolonia: null,
        addresscalle: null,
        addressnumero: null,
        description: null
      }
    }
  },

  methods: {
    launchFilePicker(){
      this.$refs.file.click();
    },
    onFileChange(fieldName, file) {
      const { maxSize } = this
      let imageFile = file[0]
      if (file.length>0) {
        let size = imageFile.size / maxSize / maxSize
        if (!imageFile.type.match('image.*')) {
          // check whether the upload is an image
          this.errorDialog = true
          this.errorText = 'Selecciona una imagen'
        } else if (size>1) {
          // check whether the size is greater than the size limit
          this.errorDialog = true
          this.errorText = 'Por favor un archivo menor a 1MB'
        } else {
          // Append file into FormData and turn file into image URL
          let formData = new FormData()
          let imageURL = URL.createObjectURL(imageFile)
          formData.append(fieldName, imageFile)
          // Emit the FormData and image URL to the parent component
          this.$emit('input', { formData, imageURL })
        }
      }
    }
  }
};
