require("./bootstrap");
import Vuetify from "vuetify";
import Vue from "vue";
import es from 'vee-validate/dist/locale/es'
import VeeValidate, { Validator } from "vee-validate";

Vue.use(Vuetify);
Vue.use(VeeValidate)
Validator.localize("es", es);
VeeValidate.Validator.localize("es");

window.Vue = require("vue");

Vue.component("form-create",
  require("./components/FormCreate/FormCreate.vue").default
);

Vue.component("campo-nombre",
  require("./components/FormCreate/partsformcreate/CampoNombres.vue").default);
Vue.component("selector-fecha",
  require("./components/FormCreate/partsformcreate/SelectorFecha.vue").default);
Vue.component("catalogo-generos",
  require("./components/FormCreate/partsformcreate/CatalogoGeneros.vue").default);
Vue.component("lista-elementos-catalogados",
  require("./components/FormCreate/partsformcreate/ListaElementosCatalogados.vue").default);

Vue.component("foto-perfil",
  require("./components/FormCreate/partsformcreate/0_foto_subida/MainImageSelector.vue").default);

Vue.component("ask-yes-not",
  require("./components/FormCreate/partsformcreate/AskYesOrNot.vue").default);
Vue.component("address-component",
  require("./components/FormCreate/partsformcreate/AddressComponent.vue").default);

Vue.component("address-number-street",
  require("./components/FormCreate/partsformcreate/AddressNumberStreet.vue").default);

Vue.component("telephone-component",
  require("./components/FormCreate/partsformcreate/TelephoneComponent.vue").default);
Vue.component("email-component",
  require("./components/FormCreate/partsformcreate/EmailComponent.vue").default);

  Vue.component("toggle-option",
  require("./components/FormCreate/partsformcreate/ToggleOption.vue").default);

  Vue.component("text-area-component",
  require("./components/FormCreate/partsformcreate/TextAreaComponent.vue").default);

const app = new Vue({
  el: "#app"
});
