<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="nav-icon fa fa-home"></i> INTRADIF

                </a>
            </li>

            @if(auth()->user()->hasRolesStrModulo(['PDN_PROCURADOR'],
            'prodenao'))
            <li class="nav-title">Procurador</li>
            @endif
            @if(auth()->user()->hasRolesStrModulo(['PDN_CAPTURISTA'],
            'prodenao'))
            <li class="nav-title">Capturista</li>
            @endif
            @if(auth()->user()->hasRolesStrModulo(['PDN_SUBPROCURADOR'],
            'prodenao'))
            <li class="nav-title">Subprocurador</li>
            @endif
            @if(auth()->user()->hasRolesStrModulo(['PDN_ABOGADO'],
            'prodenao'))
            <li class="nav-title">Abogado</li>
            @endif
            @if(auth()->user()->hasRolesStrModulo(['PDN_ADMINISTRADOR'],
            'prodenao'))
            <li class="nav-title">Administrador</li>
            @endif


        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>