<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">

      <link href="{{ asset('assets/css/estiloslayoutmaster.css') }}" rel="stylesheet">
      <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('adopciones/css/app.css') }}">
      <title>Adopciones</title>
    </head>


    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
      @include('adopcion::layouts.includes.navbar')

      <div class="app-body">

          @include('adopcion::layouts.includes.sidebar')

          <main class="main">

              <div id = "app" class="content-wrapper" >
                  @yield('content')
              </div>

          </main>
      </div>
      <footer class="app-footer d-print-none">
          <div>
              <span>DIF Oaxaca &copy; Copyright 2019.</span>
          </div>
          <div class="ml-auto">
              <span>Unidad de Informática</span>
          </div>
      </footer>

      <script src="{{ asset('adopciones/js/app.js') }}"></script>
  </body>
</html>
