<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnfermedadesCronicas extends Migration
{
    public function up()
    {
        Schema::create('adop_cat_enfermedadescronicas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('enfermedad');
            $table->string('descripcion')->nullable();
            $table->string('slug')->nullable();
            $table->string('icon')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('adop_cat_enfermedadescronicas');
    }
}
