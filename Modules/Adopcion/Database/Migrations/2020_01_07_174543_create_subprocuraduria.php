<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubprocuraduria extends Migration
{
    public function up()
    {
        Schema::create('adop_cat_subprocuradurias', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->string('responsable');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('adop_cat_subprocuradurias');
    }
}
