<?php

namespace Modules\Adopcion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Adopcion\Database\Seeders\CasasHogaresTableSeeder;
use Modules\Adopcion\Database\Seeders\EnfermedadesCronicasTableSeeders;
use Modules\Adopcion\Database\Seeders\SubprocuraduriaTableSeeder;

class AdopcionDatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->call(CasasHogaresTableSeeder::class);
        $this->call(EnfermedadesCronicasTableSeeder::class);
        $this->call(SubprocuraduriaTableSeeder::class);
    }
}
