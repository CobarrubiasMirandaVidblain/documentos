<?php

namespace Modules\Adopcion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Adopcion\entities\Enfermedadescronicas;

class EnfermedadesCronicasTableSeeder extends Seeder
{
  public function run()
  {
      Model::unguard();

      Enfermedadescronicas::create([
        'enfermedad' => 'Enfermedades cardiovasculares',
        'descripcion' => ' infartos o accidentes cerebrovasculares',
        'slug' => 'Ec',
        'icon' => 'add_circle',
      ]);
  }
}
