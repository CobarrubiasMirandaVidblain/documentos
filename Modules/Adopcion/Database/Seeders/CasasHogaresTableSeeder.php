<?php

namespace Modules\Adopcion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Adopcion\entities\Casahogar;

class CasasHogaresTableSeeder extends Seeder
{
  public function run()
  {
      Model::unguard();

      Casahogar::create([
        'nombre' => 'Casa Hogar uno',
        'responsable' => 'Juan Perez Diaz Robles',
      ]);

      Casahogar::create([
        'nombre' => 'Casa Hogar dos',
        'responsable' => 'Rodrigo Perez Diaz Robles',
      ]);

      Casahogar::create([
        'nombre' => 'Albergue tránsito',
        'responsable' => 'Manuel Perez Diaz Robles',
      ]);
  }
}
