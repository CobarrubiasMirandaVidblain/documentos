<?php

namespace Modules\Adopcion\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Modules\Adopcion\entities\Subprocuraduria;

class SubprocuraduriaTableSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        Subprocuraduria::create([
          'nombre' => 'sub1',
          'responsable' => 'Juan Perez Diaz Robles',
        ]);

        Subprocuraduria::create([
          'nombre' => 'sub2',
          'responsable' => 'Rodrigo Perez Diaz Robles',
        ]);

    }
}
