@extends('admin-lte::layouts.main')
@if (auth()->check())
@section('user-avatar', 'https://www.gravatar.com/avatar/' . md5(auth()->user()->email) . '?d=mm')
@section('user-name', auth()->user()->persona->nombre)
@section('user-job')
@section('user-log', auth()->user()->created_at)
@endif
@section('content-title', 'Centro de atención a personas ciegas y debiles visuales')
@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{ route('braille.home') }}"><i class="fa fa-dashboard"></i>BRAILLE</a></li>
  @yield('li-breadcrumbs')
</ol>
@endsection
@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
  <li>
    <a href="{{route('braille.home')}}">
      <i class="fa fa-home"></i> <span>Inicio</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-handshake-o "></i> <span>Peticiones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('braille.peticiones.index') }}"><i class="fa fa-list-alt"></i>Lista de peticiones</a></li>
      <li><a href="{{ route('braille.programas.index') }}"><i class="fa fa-cubes"></i>Programas</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-group"></i> <span>Beneficiarios</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('braille.beneficiarios.index')}}"><i class="fa fa-list-alt"></i>Lista de beneficiarios</a></li>
      <li><a href="{{ route('braille.beneficiarios.create')}}"><i class="fa fa-user-plus"></i>Nuevo beneficiario</a></li>
      <li><a href="{{ route('braille.beneficiario.notas.create')}}"><i class="fa fa-bookmark"></i>Registrar notas clinicas</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-book"></i> <span>Cursos</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('braille.cursos.index')}}"><i class="fa fa-list-alt"></i>Lista de cursos</a></li>
      <li><a href="{{ route('braille.cursos.personas.index')}}"><i class="fa fa-plus-circle"></i>Inscripciones</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-briefcase"></i> <span>Talleres</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('braille.talleres.index')}}"><i class="fa fa-list-alt"></i>Lista de talleres</a></li>
      <li><a href="{{ route('braille.talleres.create')}}"><i class="fa fa-plus-circle"></i>Nuevo registro</a></li>
    </ul>
  </li>
  <li>
    <a href="{{route('braille.home')}}">
      <i class="fa fa-calendar"></i> <span>Bitacora</span>
    </a>
  </li>
</ul>
@endsection
@push('head')
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
@endpush
@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript">
  $(':input').on('propertychange input', function(e) {
    var ss = e.target.selectionStart;
    var se = e.target.selectionEnd;
    e.target.value = e.target.value.toUpperCase();
    e.target.selectionStart = ss;
    e.target.selectionEnd = se;
  });
	function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff',
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});
    
		function unblock_error() {
			if($.unblockUI())
      alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}
    
		setTimeout(unblock_error, 120000);
	}
  
	function unblock() {
		$.unblockUI();
	}
</script>
@endpush
