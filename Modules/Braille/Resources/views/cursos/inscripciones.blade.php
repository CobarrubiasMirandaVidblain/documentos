@extends('braille::layouts.master')

@section('content-subtitle', 'Inscripciones a cursos')

@push('head')
	<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">

	<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('css/animate.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Tabla de Cursos:</h3>
		</div>
		<div class="box-body">
			<form id="inscripcion" action="{{ route('braille.cursos.personas.store') }}" method="POST" class="was-validated">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                	<div class="form-group">
	                    	<label>Seleccionar curso:</label>
	                    	<select id="curso_id" name="curso_id" class="form-control select2" required>
	                        	
	                    	</select>
	                	</div>
	            	</div>
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                	<div class="form-group">
	                    	<label>Seleccionar persona:</label>
	                    	<select id="persona_id" name="persona_id" class="form-control select2" required>
	                        		
	                    	</select>
	                	</div>
	            	</div>
	            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	                	<div class="form-group">
	            	<button type="button" class="btn btn-success pull-right" onclick="inscribir()">Inscribir</button>
	                	</div>
	            	</div>
				</div>
			</form>
			<hr>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-6 col-lg-4">
						<label>Personas en este curso:</label>
					</div>
					<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-6 col-lg-4 pull-right">
						<input type="text" id="search" name="search" class="form-control">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
						</span>
					</div>
				</div>
			</div>
			{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas']) !!}	
		</div>
	</div>
@stop

@push('body')
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

	<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>

	<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

	{!! $dataTable->scripts() !!}

	<script type="text/javascript">

	$("#inscripcion").validate({
		rules : {
			curso_id : {
				required : true
			},
			persona_id : {
				required : true
			}
		}
	});

	$.extend($.validator.messages, {
		required: 'Este campo es obligatorio.',
		remote: 'Por favor, rellena este campo.'
	});

	$.validator.setDefaults({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorPlacement: function(error, element) {
			$(element).parents('.form-group').append(error);
		}
	});

	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#personas').dataTable());
	table.init();

	function inscribir() {
		if($("#inscripcion").valid()){
			block();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url      : $('#inscripcion').attr('action'),
				type     : $('#inscripcion').attr('method'),
				data     : $('#inscripcion').serialize(),	
				dataType : 'JSON',
				success  : function(response) {
					unblock();
					swal({
						title : '¡Guardado!',
						text  : 'La persona se ha inscrito correctamente',
						type  : 'success',
					})
					$('#personas').DataTable().ajax.reload();
				},
				error : function(response){
					unblock();
					swal({
						title: '¡Error!',
						text: 'El curso no se ha guarado',
						type: 'error',
						timer: 2500
					})
				}
			});
		}
	}

	function eliminarCurso(url) {
		swal({
			title              : '¿Está seguro?',
			text               : '¡Usted va a eliminar un curso!',
			type               : 'warning',
			focusCancel		   : true,
			showCancelButton   : true,
			confirmButtonColor : '#C51414',
			cancelButtonColor  : '#554747',
			confirmButtonText  : 'Si',
			cancelButtonText   : 'No',
			allowEscapeKey     : true,
			allowOutsideClick  : true,
			animation          : false,
			customClass        : 'animated pulse',
		}).then((result)=>{
			if(result.value){
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: url,
					type: 'DELETE',
					success: function(response) {
						unblock();
						swal({
							title             : 'Eliminado!',
							text              : 'El curso se ha eliminado correctamente',
							type              : 'info',
							timer             : 3000,
							animation         : false,
							customClass       : 'animated jackInTheBox',
							showConfirmButton : false,
						})
						$('#cursos').DataTable().ajax.reload();
					},
					error : function(response){
						unblock();
						swal({
							title: '¡Error!',
							text: 'El curso no se ha eliminado',
							type: 'error',
							timer: 2500
						})
					}
				});
			}
		});
	}

	$('#curso_id').select2({
		language    : 'es',
		placeholder : "Seleccione un curso",
		ajax        : {
			url      : '{{ route('braille.cursos.index') }}',
			delay    : 500,
			dataType : 'JSON',
			type     : 'GET',
			data     : function(params) {
                return {
					columns : {
						1 : {
							data   : 'nombre',
							name   : 'cat_cursos.nombre',
							search : {
                                value : params.term,
                                regex : false
                            }
                        }
                    }
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.data, function(item) {
                        return {
							id      : item.id,
							text    : item.nombre,
							slug    : item.nombre,
							results : item
                        }
                    })
                };
            },
            cache: true
        }
    }).change(function(event) {
    	$('#personas').DataTable().ajax.reload();
    });

    $('#persona_id').select2({
		language    : 'es',
		placeholder : "Seleccione una persona",
		ajax        : {
			url      : '{{ route('braille.beneficiarios.index') }}',
			delay    : 500,
			dataType : 'JSON',
			type     : 'GET',
			data     : function(params) {
                return {
                    search : {
                        value : params.term
                    },
                    tipo   : 'select'
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.data, function(item) {
                        return {
							id      : item.id,
							text    : item.completo,
							slug    : item.completo,
							results : item
                        }
                    })
                };
            },
            cache: true
        }
    })
	</script>
@endpush
