@extends('braille::layouts.master')

@push('head')
<style type="text/css">
select[readonly] {
    background: #eee;
    cursor: no-drop;
}

select[readonly] option {
    display: none;
}
</style>

<link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle', 'Nuevo beneficiario')

@section('li-breadcrumbs')
<li><a href="{{route('braille.beneficiarios.index')}}">Beneficiarios</a></li>
<li class="active">Nuevo beneficiario</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Nuevo beneficiario</h3>
    </div>

    <form id="form_beneficiario" data-toggle="validator" role="form"
    action="{{isset($beneficiario) ? route('braille.beneficiarios.update',$beneficiario->id) : route('braille.beneficiarios.store')}}"
    method="{{isset($beneficiario) ? 'PUT' : 'POST' }}">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label>Buscar beneficiario</label>
                        <div class="input-group">
                            <select id="beneficiario_id" name="beneficiario_id" class="form-control select2" style="width: 100%;" readonly="readonly"></select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-personas', 'beneficiario_id');">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="discapacidad_id">Discapacidad:</label>
                        <select id="discapacidad_id" name="discapacidad_id" class="form-control select2" style="width: 100%;">
                            @foreach ($discapacidades as $discapacidad)
                                <option value="{{ $discapacidad->id }}"
                                    @isset($beneficiario)
                                        @if ($discapacidad->id==$beneficiario->discapacidad_id)
                                            selected
                                        @endif
                                    @endisset
                                >{{ $discapacidad->nombre }}</option>
                            @endforeach
                            @if(isset($beneficiario))
                                <option value="{{ $beneficiario->discapacidad->id }}" selected>{{ $beneficiario->discapacidad->nombre }}</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Documentacion:</label>
                            @foreach ($documentos as $doc)
                                @if ($loop->iteration % 2 != 0)
                                    <div class="row">
                                @endif
                                <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <input type="checkbox" id="{{$doc->documento->id.'-persona'}}" name="beneficiario" class="palomita" id="{{$doc->documento->id}}-explorador">
                                    <label>{{$doc->documento->nombre or ''}}</label>
                                </div>
                                @if ($loop->iteration % 2 == 0 || $loop->iteration % 2 != 0 && $loop->last)
                                    </div>
                                @endif
                            @endforeach
                    </div>          
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label>Buscar tutor</label>
                            <div class="input-group">
                                <select id="tutor_id" name="tutor_id" class="form-control select2" style="width: 100%;" readonly="readonly"></select>
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-personas', 'tutor_id');">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Documentacion tutor:</label>
                        <br>
                        @foreach ($documentos as $doc)
                            @if ($loop->iteration % 2 != 0)
                                <div class="row">
                            @endif
                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <input type="checkbox" id="{{$doc->documento->id.'-tutor'}}" name="tutor" class="palomita">
                                <label>{{$doc->documento->nombre or ''}}</label>
                            </div>
                            @if ($loop->iteration % 2 == 0 || $loop->iteration % 2 != 0 && $loop->last)
                                </div>
                            @endif
                        @endforeach
                    </div> 
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="procesarBeneficiario()">GUARDAR</button>
                    <a href="{{route('braille.beneficiarios.index')}}" class="btn  btn-danger">CANCELAR</a>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="modal-personas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-personas">Tabla Personas:</h4>
            </div>
            <div class="modal-body" id="modal-body-personas">

                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search" name="search" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </span>
                </div>

                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}

            </div>
            <div class="modal-footer" id="modal-footer-personas">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-persona">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-persona"></h4>
            </div>
            <div class="modal-body" id="modal-body-persona">

            </div>
            <div class="modal-footer" id="modal-footer-persona">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

{{-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script> --}}
<script src="/vendor/datatables/buttons.server-side.js"></script>

{!! $dataTable->scripts() !!}

@include('personas.js.persona');

<script type="text/javascript">

    var elemento = undefined;

    var modal_persona = $('#modal-persona');
    var modal_personas = $('#modal-personas');
    var modal_title_persona = $('#modal-title-persona');
    var modal_body_persona = $('#modal-body-persona');
    var modal_footer_persona = $('#modal-footer-persona');

    $('.palomita').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        increaseArea: '10%'
    })

    $("#form_beneficiario").validate({
        rules : {
            beneficiario_id : {
                required : true
            },
            tutor_id : {
                required : true
            },
            discapacidad_id : {
                required : true
            }
        }
    });

    function obtener_datos() {
        var infoDocumentos=new Array();
        $('.palomita').each(function() {
            if ($(this).parent().attr('aria-checked')=="true") {
                var doc    = new Object();
                doc.nombre = $(this).attr('name');
                doc.id     = parseInt($(this).attr('id'));
                infoDocumentos.push(doc);
            }
        });
        datos = {
            persona_id     : $("#beneficiario_id").val(),
            tutor_id       : $("#tutor_id").val(),
            discapacidad_id: $("#discapacidad_id").val(),
            infoDocumentos : JSON.stringify(infoDocumentos)
        };
        return datos;
    }       

    function procesarBeneficiario() {
        if($("#form_beneficiario").valid()){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url          : $('#form_beneficiario').attr('action'),
                type         : $('#form_beneficiario').attr('method'),
                data         : obtener_datos(),
                dataType     : 'JSON',
                success      : function(response) {
                    unblock();
                    swal({
                        title: '¡Guardado!',
                        text : 'El beneficiario se ha agregado correctamente',
                        type : 'success'
                    }).then((x)=>{
                        window.location.href = "{{ route('braille.beneficiarios.index') }}";
                    });
                },
                error        : function(response){
                    unblock();
                    swal({
                        title: '¡Error!',
                        text : 'El beneficiario no se ha guarado',
                        type : 'error',
                        timer: 2500
                    })
                }
            });
        }
    }

    $.fn.modal.Constructor.prototype.enforceFocus = function() {};

    function agregar_persona() {
        $.get('/personas/search?tipo=create_edit', function(data) {
        })
        .done(function(data) {
        })
        .fail(function(data) {
            modal_title_persona.text('Agregar Persona:');
            modal_body_persona.html(data.responseJSON.html);
            modal_footer_persona.html(
                '<button type="submit" class="btn btn-success" onclick="persona.create_edit();">GUARDAR</button>' +
                '<button type="button" class="btn btn-danger" data-dismiss="modal">CERRAR</button>'
                );
            app.to_upper_case();
            persona.init();
            persona.editar_fotografia();
            persona.agregar_fecha_nacimiento();
            persona.agregar_inputmask();
            persona.agregar_select_create();
            persona.agregar_validacion();
            modal_persona.modal('show');
        });
    }

    function abrir_modal(modal, el) {
        if(modal == 'modal-personas')
            elemento = $('#' + el);
        $("#"+modal).modal('show');
        $("#personas").DataTable().ajax.reload();
    };

    function cerrar_modal(modal){
        $("#"+modal).modal("hide");
    };

    function seleccionar_persona(id, nombre){
        if(elemento !== undefined) {
            elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
            modal_personas.modal('hide');
        }
    }

    $.extend($.validator.messages, {
        required: 'Este campo es obligatorio.',
        remote: 'Por favor, rellena este campo.',
        email: 'Por favor, escribe una dirección de correo válida.',
        url: 'Por favor, escribe una URL válida.',
        date: 'Por favor, escribe una fecha válida.',
        dateISO: 'Por favor, escribe una fecha (ISO) válida.',
        number: 'Por favor, escribe un número válido.',
        digits: 'Por favor, escribe sólo dígitos.',
        creditcard: 'Por favor, escribe un número de tarjeta válido.',
        equalTo: 'Por favor, escribe el mismo valor de nuevo.',
        extension: 'Por favor, escribe un valor con una extensión aceptada.',
        maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
        minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
        rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
        range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
        max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
        min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
        nifES: 'Por favor, escribe un NIF válido.',
        nieES: 'Por favor, escribe un NIE válido.',
        cifES: 'Por favor, escribe un CIF válido.'
    });

    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function(error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    var table = (function() {
        var tabla = undefined,
        btn_buscar = $('#btn_buscar'),
        search = $('#search');

        function init() {
            search.keypress(function(e) {
                if(e.which === 13) {
                    tabla.DataTable().search(search.val()).draw();
                }
            });

            btn_buscar.on('click', function() {
                tabla.DataTable().search(search.val()).draw();
            });
        };

        function set_table(valor) {
            tabla = $(valor);
        };

        function get_table() {
            return tabla;
        };

        return {
            init: init,
            set_table: set_table,
            get_table: get_table
        };
    })();

    table.set_table($('#personas').dataTable());
    table.init();

</script>
@endpush
