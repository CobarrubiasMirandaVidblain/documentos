<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use Modules\BancaDIF\Entities\TipoPago;

class TipoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return "xD";
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|unique:siaf_cat_tipospago|max:20',
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        try{
            DB::beginTransaction();

            $tipoPago = TipoPago::create([
                'nombre' => $request->nombre,
                // 'usuario_id' => $request->usuario_id
            ]);
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $tipoPago, 'message'=>"Se guardo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return new JsonResponse(['data' => TipoPago::orderBy('id','DESC')->paginate(25) , 'message' => "ok"]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        return new JsonResponse(['data' => TipoPago::find($id) , 'message' => "ok"]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:20',
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data' => null , 'message' => $validator->errors()],480);
        }
        $tipoPago = TipoPago::find($request->id);
        if($tipoPago == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        try{
            DB::beginTransaction();

            $tipoPago->nombre =  $request->nombre;
            $tipoPago->save();
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $tipoPago, 'message'=>"Se guardo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $pago = TipoPago::find($id);
        if($pago == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        try{
            DB::beginTransaction();
            
            $pago->delete();
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $pago, 'message'=>'Se elimino correctamente']);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }
}
