<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Modules\BancaDIF\Entities\TiposAsignaciones;

class TiposAsignacionesController extends Controller
{
    public function index()
    {
        return view('bancadif::index');
    }

    public function lista()
    {
        return new JsonResponse(['data' => TiposAsignaciones::all(), 'message' => "bien"]); 
    }

     
}
