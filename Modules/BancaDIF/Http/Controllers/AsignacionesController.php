<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\Asignacion;
use Modules\BancaDIF\Traits\Util;
use Modules\BancaDIF\Entities\AsignacionGasto;

class AsignacionesController extends Controller
{
    use Util;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('bancadif::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage. Metodo para guardar una asignacion
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $validator = Validator::make($request->all(), [
                'fondorotatorioarea_id' => 'required|integer',
                'monto' => 'required|numeric',
                'tipo' => 'required|integer'
            ]);
            if ($validator->fails()) {
                return new JsonResponse(['data' => null, 'message' => $validator->errors()], 480);
            }
            $fondo = FondoRotatorioArea::find($request->fondorotatorioarea_id);
            if($fondo == null)
                return new JsonResponse(['data' => null, 'message' => 'No se encontro el fondo rotatorio'], 404);
            $asignacion = $fondo->asignaciones()->create([
                'monto' => $request->monto,
                'tipo_asignacion_id' => $request->tipo,
                'fecha' => new \Carbon\Carbon,
								'usuario_id' => $request->usuario_id]);
						if($request->tipo != 4)
            	$fondo->saldo_disponible += $request->monto;
						
							$fondo->save();
						
            if($request->tipo == 4){
                $gasto = new AsignacionGasto();
                $gasto->nombre = $request->nombre;
                $gasto->saldo_disponible = $request->monto;
                $gasto->asignacion_id =  $asignacion->id;
                $gasto->tipo_pago_id = $request->tipo_pago;
                $gasto->save();
            }
                
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'siaf_asignaciones',
                'registro' => $fondo->asignaciones->last()->id . '',
                'campos' => json_encode($fondo->asignaciones->last()) . '',
                'metodo' => $request->method()    
            ]);
            $usuario->bitacora($request, [
                'tabla' => 'siaf_fondorotatorioareas',
                'registro' => $fondo->id . '',
                'campos' => json_encode($fondo) . '',
                'metodo' => $request->method()
            ]);
            return new JsonResponse(['data' => $fondo, 'message' => 'ok']);
        }catch(\Exception $e){
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bancadif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bancadif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($asignacion_id)
    {
        $asignacion = Asignacion::find($asignacion_id);
        if($asignacion == null){
            return new JsonResponse(['data' => null, 'message' => 'No se encontró'], 404);
        }
        $asignacion->delete();
        return new JsonResponse(['data' => $asignacion, 'message' => 'Eliminado']);
    }

    public function list($areaId)
    {
        $areaFondo = $this->areaFondo($areaId);        
        $asignaciones = Asignacion::has('gasto')->with('gasto','comprobacion','fondo')->where('fondorotatorioarea_id',$areaFondo->id)->get();
        // return $asignaciones;
        $lista = collect([]);

        foreach ($asignaciones as $l) {
            if($l->comprobacion == null)            
                $lista->prepend(['id' => $l->id, 'nombre'=> $l->gasto->nombre]);
        }
        return new JsonResponse(['data' => $lista, 'message' => 'ok']);
    }
}
