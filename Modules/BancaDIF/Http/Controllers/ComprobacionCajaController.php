<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
// use Zend\Diactoros\Response\JsonResponse;
use Illuminate\Http\JsonResponse;
use Modules\BancaDIF\Entities\ComprobacionCaja;
use Modules\BancaDIF\Entities\ComprobacionCajaHistorial;
use Modules\BancaDIF\Traits\Util;
use Illuminate\Support\Facades\DB;
use Modules\BancaDIF\Entities\Comprobaciones;
use Modules\BancaDIF\Entities\Asignacion;
use Modules\BancaDIF\Entities\AsignacionPago;
use Modules\BancaDIF\Entities\CuentasBancos;
use Modules\BancaDIF\Entities\EstadoPago;
use Modules\BancaDIF\Entities\TiposAsignaciones;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\MetodoPago;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\PagosBancos;
use Modules\BancaDIF\Entities\TipoPago;

class ComprobacionCajaController extends Controller
{
    use Util;
    public function __construct()
    {
        $this->middleware('auth', ['only' => 'index']);
        $this->middleware('rolModuleV2:fondorotatorio,FINANCIERO,ENLACE,ADMINISTRADOR,CAPTURISTA', ['only' => 'index']);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('bancadif::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bancadif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bancadif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function findComprobacion($id)
    {
        $pagoCaja = ComprobacionCaja::where('comprobacion_id',$id)->first();
        if(!$pagoCaja) return $this->jsonResponse(null, 'No se encontro el pago', 404);

        return new JsonResponse(['data' => $pagoCaja, 'message' => "Bien"], 200);
    }

    public function lista()
    {
				$pagos =  ComprobacionCaja::with('estatuslast.estadoModulo.estatus','comprobacion.fondo.area','comprobacion.fondo.historialenlace.empleado.persona','comprobacion.fondo.historialenlace.empleado.nivel')->get();
				$gasto = TiposAsignaciones::where('nombre','GASTO A COMPROBAR')->first()->id;
				// $caja = MetodoPago::where('nombre','PAGO CAJA')->first()->id;
				$listGastos =  AsignacionPago::with('pago.estatuslast')->whereHas('asignacion',function ($q) use ($gasto){$q->where('tipo_asignacion_id',$gasto);})->whereHas('pago', function ($query) use ($gasto){$query->Orwhere('metodopago_id','!=',$gasto)->Orwhere('metodopago_id',null);})->get();

        $list = collect([]);

        foreach ($pagos as $p) {
            $list->prepend(['id' => $p->id, 'area'=>$p->comprobacion->fondo->area->nombre, 'estatus'=>$p->estatuslast->estadoModulo->estatus->nombre, 'monto'=>$p->monto, 'fecha'=>$p->created_at->toDateString(), 'folio'=>$p->folio, 'responsable'=>$p->comprobacion->fondo->historialenlace->empleado->persona->nombre.' '.$p->comprobacion->fondo->historialenlace->empleado->persona->primer_apellido.' '.$p->comprobacion->fondo->historialenlace->empleado->persona->segundo_apellido, 'nivel'=>$p->comprobacion->fondo->historialenlace->empleado->nivel->nivel, 'tipo'=>'COMPROBACION']);
				}
				
				foreach ($listGastos as $g) {
					if($g->pago->estatuslast->estadoModulo->estatus->nombre != 'NUEVO')
						$list->prepend(['id' => $g->pago->id, 'area'=>$g->pago->empleado->persona->nombre.' '.$g->pago->empleado->persona->primer_apellido.' '.$g->pago->empleado->persona->segundo_apellido, 'monto'=> $g->pago->monto_total, 'fecha'=>  $g->pago->fecha_elaboracion->toDateString(), 'folio'=>$g->pago->folio,'estatus'=>$g->pago->estatuslast->estadoModulo->estatus->nombre == 'ENVIADO' ? 'ENVIADO' : ($g->pago->estatuslast->estadoModulo->estatus->nombre == 'REINTEGRO TOTAL' ? 'REINTEGRO TOTAL': 'PAGADO'), 'tipo'=>"PAGO"]); //caravanas
				}

        return new JsonResponse(['data' => $list, 'message' => "Bien"],200);
    }

    public function changeStatus(Request $request)
    {
			$pago = ComprobacionCaja::find($request->id);
			if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);

			$estatusList = $this->estatusBancaDIF();

			try{
				DB::beginTransaction();
				$historico = ComprobacionCajaHistorial::create([
						'comprobacioncaja_id'=> $pago->id,
						'estado_modulo_id'=> $estatusList->where('estatus',$request->status)->first()['id'],
						'usuario_id'=>  auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id
				]);
				if($request->status == 'PAGADO')
				{
						$comprobacion = Comprobaciones::with('asignacion','fondo')->where('id',$pago->comprobacion_id)->first();
						if($comprobacion->asignacion->nombre != 'GASTO A COMPROBAR')
						{ // se crea la asignacion para que el area tenga el nuevo saldo
							Asignacion::create([
									'fondorotatorioarea_id'=>$comprobacion->fondorotatorio_id,
									'monto'=>$pago->monto,
									'fecha'=> new \Carbon\Carbon,
									'tipo_asignacion_id' => TiposAsignaciones::where('nombre','RECUPERACION')->first()->id,
									'usuario_id' => auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id
							]);
							// se actualiza el saldo disponible del fondo
							$fondo = FondoRotatorioArea::find($comprobacion->fondorotatorio_id);
							$fondo->saldo_disponible +=  $pago->monto;
							$fondo->save();
						}
						
				}
				DB::commit();
				return new JsonResponse(['data' => $historico, 'message' => "Se actualizo correcto"], 200);
			}catch(Exception $e){
				DB::rollBack();
				return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
			}
	}
	public function pagarPago($idpago)
	{
		$pago = Pago::find($idpago);
			if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);
		
		$estatusList = $this->estatusBancaDIF();
		$estadoId = $estatusList->where('estatus','PAGADO')->first()['id'];
			if(!$estadoId) return $this->jsonResponse(null, 'No se encontro el estado', 404);

		EstadoPago::create([
			'pago_id' =>$pago->id,
			'estado_programa_id' => $estadoId,
			'fecha' =>  new \Carbon\Carbon,
			'usuario_id' => request()->usuario_id,
		]);

		return $this->jsonResponse($pago->load('estatuslast'), 'Pago actualizado correctamente');
	}
}


