<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\BancaDIF\Entities\AsignacionPago;
use Modules\BancaDIF\Entities\CuentasBancos;
use Modules\BancaDIF\Entities\EstadoPago;
use Modules\BancaDIF\Entities\MetodoPago;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\PagosBancos;
use Modules\BancaDIF\Entities\TiposAsignaciones;
use Modules\BancaDIF\Traits\Util;

class BancosController extends Controller
{

	use Util;

    public function bancos()
    {
        $bancos = \App\Models\Banco::all();
        return new JsonResponse(['data' => $bancos, 'message' => 'Ok']);
    }
		
    public function lista()
    {
        $gasto = TiposAsignaciones::where('nombre','GASTO A COMPROBAR')->first()->id;
        $banco = MetodoPago::withTrashed()->where('nombre','PAGO BANCO')->first()->id;
        $listGastos =  AsignacionPago::with('pago.estatuslast')->whereHas('asignacion',function ($q) use ($gasto){$q->where('tipo_asignacion_id',$gasto);})->whereHas('pago', function ($query) use ($banco){$query->where('metodopago_id',$banco);})->get();

        $list = collect([]);
        
        foreach ($listGastos as $g) {
            if($g->pago->estatuslast->estadoModulo->estatus->nombre != 'NUEVO')
                $list->prepend(['id' => $g->pago->id, 'area'=>$g->pago->empleado->persona->nombre.' '.$g->pago->empleado->persona->primer_apellido.' '.$g->pago->empleado->persona->segundo_apellido, 'monto'=> $g->pago->monto_total, 'fecha'=>  $g->pago->fecha_elaboracion->toDateString(), 'folio'=>$g->pago->folio,'estatus'=>$g->pago->estatuslast->estadoModulo->estatus->nombre == 'ENVIADO' ? 'ENVIADO' : ($g->pago->estatuslast->estadoModulo->estatus->nombre == 'REINTEGRO TOTAL' ? 'REINTEGRO TOTAL': 'PAGADO'), 'tipo'=>"PAGO"]);
        }

        return new JsonResponse(['data' => $list, 'message' => "Bien"],200);
    }
		
    public function cuentas($search)
    {
        return new JsonResponse( CuentasBancos::Orwhere('concepto','like', "%$search%")->Orwhere('num_cuenta', 'like', "%$search%")->take(15)->get());
    }

    public function pagarPago($idpago, $bancoid, $fecha, $usuarioId)
    {
        $pago = Pago::find($idpago);
            if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);
        
        $cuenta = CuentasBancos::find($bancoid);
            if(!$cuenta) return $this->jsonResponse(null, 'No se encontro la cuenta', 404);

        $estatusList = $this->estatusBancaDIF();
        $estadoId = $estatusList->where('estatus','PAGADO')->first()['id'];
            if(!$estadoId) return $this->jsonResponse(null, 'No se encontro el estado', 404);

        EstadoPago::create([
            'pago_id' =>$pago->id,
            'estado_programa_id' => $estadoId,
            'fecha' =>  new \Carbon\Carbon,
            'usuario_id' => $usuarioId,
        ]);

        PagosBancos::create([
            'fecha_pagado' =>$fecha,
            'pago_id' => $pago->id,
            'numcuenta_id' =>  $bancoid,
        ]);

        return $this->jsonResponse($pago->load('estatuslast'), 'Pago actualizado correctamente');
    }
}
