<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Validator;

use App\Models\Persona;
use App\Models\Empleado;

use Modules\BancaDIF\Traits\Util;
use App\Models\AreasResponsable;

class EmpleadoController extends Controller
{

    use Util;
    
    public function empleadosNoenlace()
    {
        $empleados = Empleado::doesntHave('enlacefinanciero')->with('persona:id,nombre,primer_apellido,segundo_apellido')->get();
        return new JsonResponse(['data' => $empleados, 'message' => 'ok']);
    }
        
    public function empleados($nombre)
    {
        $empleados = Empleado::select('id', 'rfc', 'persona_id', 'area_id', 'nivel_id')->with(['persona:id,nombre,primer_apellido,segundo_apellido', 'nivel', 'area'])->whereHas('persona', function($query) use($nombre){
            $query->where('nombre', 'like', "%$nombre%")->orWhere('primer_apellido', 'like', "%$nombre%")->orWhere('segundo_apellido', 'like', "$nombre%");
        })->get();
        foreach($empleados as $empleado){
            if($empleado->segundo_apellido == null){
                $empleado->segundo_apellido = '';
						}
						$empleado->titular = AreasResponsable::where('empleado_id',$empleado->id)->whereHas('area', function ($q){$q->whereIn('tipoarea_id',[1,2]);})->count();
        }
        return new JsonResponse(['data' => $empleados, 'message' => 'Ok']);
    }

    public function empleadosRFC($rfc)
    {
        $empleados = Empleado::where('rfc', 'like', "%$rfc%")->with('persona')->get();
        foreach($empleados as $empleado){
            $empleado->nombreCompleto = $empleado->persona->nombre . ' ' . $empleado->persona->primer_apellido . ' ' . $empleado->persona->segundo_apellido;
        }
        return new JsonResponse(['data' => $empleados, 'message' => 'Ok']);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'primerApellido' => 'required',
            'curp' => 'required|unique:personas,curp',
            'rfc' => 'required|unique:empleados,rfc',
            'nivel_id' => 'required',
            'tipoempleado_id' => 'required'
        ]);
        if ($validator->fails()) {
            \Log::debug($validator->errors());
            return new JsonResponse(['data' => null, 'message' => $validator->errors()], 480);
        }
        try{
            DB::beginTransaction();

            $etnia = \App\Models\Etnia::where('nombre', 'NO APLICA')->first();
            if($etnia == null) return new JsonResponse(['data' => null, 'message' => 'No se encontro la etnia NO APLICA'], 404);

            $localidad = \App\Models\Localidad::find($request->localidad_id);
            if($localidad == null) return new JsonResponse(['data' => null, 'message' => 'No se encontro la localidad'], 404);

            $fechaNacimiento = new \Carbon\Carbon($request->fechaNacimiento);
            
            $persona = Persona::create([
                'nombre' => $request->nombre,
                'primer_apellido' => $request->primerApellido,
                'segundo_apellido' => $request->segundoApellido,
                'fecha_nacimiento' => $fechaNacimiento->format('Y-m-d'),
                'curp' => $request->curp,
                'genero' => $request->genero,
                'calle' => $request->calle,
                'numero_exterior' => $request->ext,
                'numero_interior' => $request->int,
                'colonia' => $request->colonia,
                'codigopostal' => $request->cp,
                'numero_celular' => $request->celular,
                'numero_local' => $request->local,
                'email' => $request->email,
                'localidad_id' => $localidad->id,
                'etnia_id' => $etnia->id,
                'municipio_id' => $localidad->municipio_id,
                'usuario_id' => $request->usuario_id
            ]);

            $empleado = Empleado::create([
                'persona_id' => $persona->id,
                'rfc' => $request->rfc,
                'nivel_id' => $request->nivel_id,
                'tiposempleado_id' => $request->tipoempleado_id,
                'area_id' => $request->area_id,
                'persona_emergencia' => $request->personaEmergencia,
                'telefono_emergencia' => $request->telefonoEmergencia,
                'usuario_id' => $request->usuario_id
            ]);

            DB::commit();
            return new JsonResponse(['data' => $empleado->load(['persona:id,nombre,primer_apellido,segundo_apellido', 'nivel']), 'message' => 'Persona ok'], 201);
        }catch(\Exception $e){
            DB::rollBack();
            \Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }
}
