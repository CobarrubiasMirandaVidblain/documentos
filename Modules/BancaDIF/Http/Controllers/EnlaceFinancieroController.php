<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use Modules\BancaDIF\Entities\EnlaceFinanciero;
use Modules\BancaDIF\Entities\HistorialAreaEnlace;

class EnlaceFinancieroController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return "xD";
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre_cuenta' => 'required|unique:siaf_enlacesfinancieros|max:50',
            'clabe' => 'required|unique:siaf_enlacesfinancieros|max:50',
            'banco_id' => 'required',
            'fondorotatorioarea_id' => 'required',
            'fecha_asignacion' => 'required',
            'empleado_id' => 'required',

        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        try{
            DB::beginTransaction();

            $enlace = EnlaceFinanciero::create([
                'nombre_cuenta' => $request->nombre_cuenta,
                'clabe' => $request->clabe,
                'banco_id' => $request->banco_id,
                'usuario_id' => $request->usuario_id
            ]);

            $enlace = HistorialAreaEnlace::create([
                'fondorotatorioarea_id' => $request->fondorotatorioarea_id,
                'enlacefinanciero_id' => $enlace->id,
                'fecha_asignacion' => $request->fecha_asignacion,
                'fecha_baja' => $request->fecha_baja,
                'activo' => 1,
                'empleado_id' => $request->empleado_id,
                'usuario_id' => $request->usuario_id
            ]);
            
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_metodospago',
            //     'registro' => $metodoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $enlace, 'message' => 'Se guardo correctamente']);
            // return new JsonResponse(['success' => true, 'message'=>$enlace]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return new JsonResponse(['data' => EnlaceFinanciero::with('banco')->orderBy('id','DESC')->paginate(25), 'message' => 'ok']);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        return new JsonResponse(['data' => EnlaceFinanciero::with('banco')->find($id), 'message' => 'ok']);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre_cuenta' => 'required|max:50',
            'clabe' => 'required|max:50',
            'banco_id' => 'required',
            'fondorotatorioarea_id' => 'required',
            'fecha_asignacion' => 'required',
            'fecha_baja' => 'required',
            'empleado_id' => 'required',

        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        $enlace = EnlaceFinanciero::with('historial')->find($request->id);
        if($enlace == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        
        try{
            DB::beginTransaction();
            $enlace->nombre_cuenta = $request->nombre_cuenta;
            $enlace->clabe = $request->clabe;
            $enlace->banco_id = $request->banco_id;
            $enlace->usuario_id = $request->usuario_id;
            $enlace->save();
            
            $historial = HistorialAreaEnlace::find($enlace->historial->id);
            $historial->delete();
            
            $enlace = HistorialAreaEnlace::create([
                'fondorotatorioarea_id' => $request->fondorotatorioarea_id,
                'enlacefinanciero_id' => $enlace->id,
                'fecha_asignacion' => $request->fecha_asignacion,
                'fecha_baja' => $request->fecha_baja,
                'activo' => 1,
                'empleado_id' => $request->empleado_id,
                'usuario_id' => $request->usuario_id
            ]);

            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $enlace, 'message'=>"Se guardo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
            
        $enlace = EnlaceFinanciero::with('historial')->find($id);
        if($enlace == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        
        $historial = HistorialAreaEnlace::where('id',$enlace->historial->id)->delete();
        $enlace->delete();
        return new JsonResponse(['data' => $enlace, 'message' => 'Eliminado']);

        // auth()->user()->bitacora(request(), [
        //     'tabla' => 'siaf_cat_tipospago',
        //     'registro' => $tipoPago->id . '',
        //     'campos' => json_encode($request) . '',
        //     'metodo' => request()->method()
        // ]);
        
    }
}
