<?php

namespace Modules\BancaDIF\Http\Controllers;

use App\Models\Localidad;
use App\Models\Municipio;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\BancaDIF\Entities\Proveedores;
use Illuminate\Support\Facades\DB;

use Modules\BancaDIF\Entities\Proveedor;
use Modules\BancaDIF\Entities\ProveedorSQL;

class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('bancadif::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
		if(Proveedores::where('rfc',$request->rfc)->count() >= 1) 
			return new JsonResponse(['data' => null, 'message' => "Ya esta registrado el RFC "], 404);				
		try{
			DB::beginTransaction();

				$proveedor = Proveedores::create([
					'rfc' => $request->rfc,
					'nombre' => $request->nombre,
					'calle' => $request->calle,
					'numero_interior' => $request->noInt,
					'numero_exterior' => $request->noExt,
					'colonia' => $request->colonia,
					'localidad_id' => $request->localidad['id'],
					'municipio_id' => $request->municipio['id'],
					'codigo_postal' => $request->cp,
					'email' => $request->correo,
					'tipo' =>$request->tipo,
				]);
				// Guardar cuando se indique en true el campo guardado de lo contrario solo guarda en la bd de maria
				if($request->guardado == 0){

					try{
						DB::connection('sqlsrv_proveedor')->beginTransaction();
		
						$proveedorSQL = ProveedorSQL::create([
							'PRO_RFCBEN' => $request->rfc,
							'RFC_FINAN_MAL' => '',
							'PRO_NOMBRE' => $request->nombre,
							'PRO_DOMICILIO' => $request->calle.' '.$request->noExt.' '.$request->colonia,
							'Cp_Telefono' => '',
							'Cp_Fax' => null,
							'Cp_Tipo' => null,
							'Cp_Nombre' => $request->nombre,
							'Cp_ApePat' => '',
							'Cp_ApeMat' => '',
							'Cp_Curp' => '',
							'Cp_Calle' => $request->calle,
							'Cp_Referencia' => null,
							'Cp_NoExterior' => $request->noExt,
							'Cp_NoInterior' => $request->noInt,
							'Cp_Colonia' => $request->colonia,
							'Cp_Estado' => 'OAXACA',
							'Cp_Municipio' => $request->municipio['nombre'],
							'Cp_Localidad' => $request->localidad['nombre'],
							'Cp_CP' => $request->cp,
							'Cp_Email' => $request->correo,
							'Cp_StatusSCP' => 0,
							'Cp_Baja' => 0,
							'cp_cadenasprod' => 'NO',
							'cp_celular' => '',
							'cp_tipoproducto' => 2,
							'cp_contacto' => ''
						]);
						DB::connection('sqlsrv_proveedor')->commit();
						
					}catch(\Exception $e){
						
						DB::connection('sqlsrv_proveedor')->rollBack();
						\Log::debug($e);
						throw new Exception('Error captura proveedor sql.');
					}

				}
				
			DB::commit();
			return new JsonResponse(['data' => $proveedor, 'message' => "Se guardo correctamente"], 200);
		}catch(\Exception $e){
			\Log::debug($e);
			DB::rollBack();
			return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
		}

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bancadif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
		$proveedor = Proveedores::find($id);
		if(!$proveedor) 
			return new JsonResponse(['data' => null, 'message' => "No existe informacion"], 404);	

		$proveedor->correo = $proveedor->email;
		$proveedor->cp = $proveedor->codigo_postal;
		$proveedor->noInt = $proveedor->numero_interior;
		$proveedor->noExt = $proveedor->numero_exterior;
		$proveedor->municipio = Municipio::find($proveedor->municipio_id);
		$proveedor->localidad = Localidad::find($proveedor->localidad_id);
		
		return new JsonResponse(['data' => $proveedor, 'message' => "Bien"], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request ,$id)
    {
		// return $request;
		$proveedor = Proveedores::find($id);
		if(!$proveedor) 
			return new JsonResponse(['data' => null, 'message' => "No existe informacion"], 404);	

		$proveedor->nombre = $request->nombre;
		$proveedor->rfc = $request->rfc;
		$proveedor->calle = $request->calle;
		$proveedor->numero_interior = $request->noInt;
		$proveedor->numero_exterior = $request->noExt;
		$proveedor->colonia = $request->colonia;
		$proveedor->codigo_postal = $request->cp;
		$proveedor->email = $request->correo;
		
		$proveedor->save();

		return new JsonResponse(['data' => $proveedor, 'message' => "Se actualizo Correctamente"], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
		
    public function lista($dato)
    {
			
		// return new JsonResponse(ProveedorSQL::all());
		if(strlen($dato) == 1)
			$lista = Proveedores::where('tipo','PROVEEDOR')->OrderBy('created_at', 'DESC')->take(200)->get();
		else
			$lista = Proveedores::where('tipo','PROVEEDOR')->where('rfc', 'like', "%$dato%")->orWhere('nombre', 'like', "%$dato%")->OrderBy('created_at', 'asc')->take(250)->get();
		// return new JsonResponse($lista[0]);
		$proveedores = collect([]);
		foreach ($lista as $p) {
			$proveedores->prepend(['id' => $p->id, 'nombre'=> $p->nombre, 'correo'=> $p->email, 'rfc'=>  $p->rfc]);
		}
		return new JsonResponse($proveedores);
    }

    public function buscar($proveedor)
    {
		$proveedores = Proveedor::where('rfc', 'like', "%$proveedor%")->orWhere('nombre', 'like', "%$proveedor%")->take(15)->get();
		return new JsonResponse(['data' => $proveedores, 'message' => 'Ok']);
    }
}
