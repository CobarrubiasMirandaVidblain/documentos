<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use Modules\BancaDIF\Entities\MetodoPago;

class MetodoPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return "xD";
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|unique:siaf_cat_tipospago|max:20',
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        try{
            DB::beginTransaction();

            $metodoPago = new MetodoPago();
            $metodoPago->nombre =  $request->nombre;
            $metodoPago->save();
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_metodospago',
            //     'registro' => $metodoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $metodoPago, 'message'=>"Se guardo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return new JsonResponse(['data' => MetodoPago::orderBy('id','ASC')->get()->take(2), 'message' => 'o2k']);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        return new JsonResponse(['data' => MetodoPago::find($id), 'message' => 'ok']);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|max:20',
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data' => null, 'message' => $validator->errors()], 480);
        }
        $metodoPago = MetodoPago::find($request->id);
        if($metodoPago == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        try{
            DB::beginTransaction();

            $metodoPago->nombre =  $request->nombre;
            $metodoPago->save();
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $metodoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $metodoPago, 'message' => "Se guardo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],480);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $metodoPago = MetodoPago::find($id);
        if($metodoPago == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        try{
            DB::beginTransaction();
            
            $metodoPago->delete();
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $metodoPago, 'message'=>'Se elimino correctamente']);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }
}
