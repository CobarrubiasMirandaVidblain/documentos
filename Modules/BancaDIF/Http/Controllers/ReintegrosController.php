<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\BancaDIF\Entities\Reintegro;

class ReintegrosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('bancadif::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($idUser)
    {
        // $reintegros = Reintegro::where('usuario_id',$idUser)->get();
        if($idUser == 1)
            $reintegros = Reintegro::all();
        else
            $reintegros = Reintegro::where('usuario_id',$idUser)->get();
        
        $reintegrosLista = collect([]);
		foreach ($reintegros as $r) {
            $reintegrosLista->prepend(['folio' => $r->folio, 'cantidad'=> $r->cantidad, 'fecha'=> $r->created_at->toDateString(), 'foliopago'=>  $r->pago->folio, 'id' => $r->id]);
		}
        return new JsonResponse(['data' => $reintegrosLista]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $reintegro = Reintegro::find($id);
        if(!$reintegro) return $this->jsonResponse(null, 'No se encontro el reintegro', 404);

        $reintegro->empleado = $reintegro->pago->empleado->persona->nombre.' '.$reintegro->pago->empleado->persona->primer_apellido.' '.$reintegro->pago->empleado->persona->seundo_apellido;
        $reintegro->empleadoArea = $reintegro->pago->empleado->area->nombre;
        $reintegro->areaFondo = $reintegro->pago->fondo->area->nombre;
        $reintegro->folioPago = $reintegro->pago->folio;
        $reintegro->montoPago = $reintegro->pago->monto_total;
        $reintegro->fechaPago = $reintegro->pago->fecha_elaboracion->toDateString();

        return new JsonResponse(['data' => $reintegro]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
