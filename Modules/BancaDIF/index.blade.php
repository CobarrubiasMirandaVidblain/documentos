@extends('viaticos::layouts.master')

@section('content')
    <div id="app">
        <h1>Hello World</h1>
        <p>This view is loaded from module: {!! config('viaticos.name') !!}</p>
        {{-- <example-component></example-component> --}}
        <router-view></router-view>
    </div>
@stop
