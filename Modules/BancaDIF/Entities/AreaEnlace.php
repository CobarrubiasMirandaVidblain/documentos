<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaEnlace extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_historial_areas_enlaces';
    protected $fillable = ['fondorotatorioarea_id', 'enlacefinanciero_id', 'fecha_asignacion', 'fecha_baja', 'activo', 'empleado_id', 'usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $with = ['empleado.persona', 'enlacefinanciero'];

    public function empleado()
    {
        return $this->hasOne('App\Models\Empleado', 'id', 'empleado_id');
    }

    public function enlacefinanciero()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\EnlaceFinanciero', 'enlacefinanciero_id', 'id');
    }
}
