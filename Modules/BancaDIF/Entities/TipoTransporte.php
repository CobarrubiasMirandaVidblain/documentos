<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoTransporte extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_cat_tipostransportes';
    protected $fillable = ['nombre'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
