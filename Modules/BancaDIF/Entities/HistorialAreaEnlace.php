<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistorialAreaEnlace extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_historial_areas_enlaces';
    protected $fillable = ['fecha_asignacion','fondorotatorioarea_id','enlacefinanciero_id','fecha_baja','activo','empleado_id','usuario_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function empleado()
    {
        return $this->belongsTo('App\Models\Empleado', 'empleado_id', 'id');
    }
}
