<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AsignacionPago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_asignaciones_pagos';
    protected $fillable = ['asignacion_id', 'pago_id'];

    public function pago()
    {
    return $this->hasOne('Modules\BancaDIF\Entities\Pago', 'id','pago_id');
    }
    
    public function asignacion()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\Asignacion', 'asignacion_id','id');
    }
}
