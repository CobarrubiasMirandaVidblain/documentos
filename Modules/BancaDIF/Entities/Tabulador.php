<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tabulador extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_tabuladorviaticos';
    protected $fillable = ['origen', 'destino', 'cuota', 'estado'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

}
