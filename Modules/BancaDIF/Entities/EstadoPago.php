<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoPago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_estados_pagos';
    protected $fillable = ['pago_id', 'estado_programa_id', 'fecha', 'usuario_id','observacion'];
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at'];

    public function estadoModulo()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\Estatus', 'estado_programa_id', 'id');
    }
}
