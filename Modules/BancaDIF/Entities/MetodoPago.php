<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetodoPago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_cat_metodospago';
    protected $fillable = ['nombre'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
