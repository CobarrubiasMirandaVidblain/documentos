<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asignacion extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_asignaciones';
    protected $fillable = ['fondorotatorioarea_id', 'monto', 'fecha', 'usuario_id','tipo_asignacion_id'];
    protected $dates = ['fecha'];

    protected $with = ['tipo'];

    public function tipo()  //Tipo de asignacion
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\TiposAsignaciones', 'tipo_asignacion_id','id');
    }

    public function gasto()
    {
        return $this->hasOne('Modules\BancaDIF\Entities\AsignacionGasto', 'asignacion_id', 'id');
    }

    public function comprobacion()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\Comprobaciones', 'asignacion_id','id');
    }

    public function fondo()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\FondoRotatorioArea', 'fondorotatorioarea_id');
    }
}
