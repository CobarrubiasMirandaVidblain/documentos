<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArchivoPago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_archivos_pagos';
    protected $fillable = ['pago_id', 'archivo_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
