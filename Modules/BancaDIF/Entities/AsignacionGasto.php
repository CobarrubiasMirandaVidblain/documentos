<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AsignacionGasto extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_asignaciones_gastos';
    protected $fillable = ['nombre', 'saldo_disponible', 'asignacion_id', 'tipo_pago_id'];
}
