<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClavePresupuesto extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_clavespresupuestales';
    protected $fillable = [];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function area()
    {
        return $this->belongsTo('App\Models\Area', 'area_id', 'id');
    }

    public function unidad()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\UnidadEjecutora', 'unidadejecutora_id', 'id');
    }
}
