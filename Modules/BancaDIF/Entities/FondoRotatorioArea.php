<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FondoRotatorioArea extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_fondorotatorioareas';
    protected $fillable = ['area_id', 'techo_presupuestal', 'saldo_disponible', 'usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function area()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function areaenlace()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\AreaEnlace', 'id', 'fondorotatorioarea_id');
    }

    public function asignaciones()
    {
        return $this->hasMany('Modules\BancaDIF\Entities\Asignacion', 'fondorotatorioarea_id', 'id');
    }
    public function historialenlace()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\HistorialAreaEnlace', 'id', 'fondorotatorioarea_id')->latest();
    }
}
