<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;

class AreaSQL extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'BdCatalogos.dbo.v_catalubicafisica';
    protected $primaryKey = 'Cp_Id';
    protected $guarded = [];
}
