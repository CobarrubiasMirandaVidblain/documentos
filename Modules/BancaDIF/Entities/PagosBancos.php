<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PagosBancos extends Model
{
	use SoftDeletes;
	protected $table = 'siaf_pagos_cuentas';
	protected $fillable = ['fecha_pagado', 'pago_id','numcuenta_id'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
