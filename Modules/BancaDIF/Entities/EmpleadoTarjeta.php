<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmpleadoTarjeta extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_empleados_tarjeta';
    protected $fillable = ['num_tarjeta', 'banco_id', 'empleado_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function banco(){
		return $this->belongsTo('App\Models\Banco', 'banco_id', 'id');
	}
}
