<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComprobacionCajaHistorial extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_comprobacion_caja_historial';
    protected $fillable = ['comprobacioncaja_id','estado_modulo_id','usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function estadoModulo()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\Estatus', 'estado_modulo_id', 'id');
    }
}
