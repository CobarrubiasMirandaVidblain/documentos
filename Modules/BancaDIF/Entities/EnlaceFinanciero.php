<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnlaceFinanciero extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_enlacesfinancieros';
    protected $fillable = ['nombre_cuenta', 'clabe', 'banco_id', 'usuario_id'];
    protected $hidden = ['usuario_id', 'created_at', 'updated_at', 'deleted_at'];

    public function banco()
    {
        return $this->belongsTo('App\Models\Banco', 'banco_id', 'id');
    }
    public function historial()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\HistorialAreaEnlace','id','enlacefinanciero_id');
    }
}
