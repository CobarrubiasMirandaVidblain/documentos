<?php

namespace Modules\BancaDIF\Patterns\Strategy;
use Modules\BancaDIF\Patterns\Strategy\PagoStrategy;

class PagoContext {

    private $pagoStrategy;

    public function __construct(PagoStrategy $pagoStrategy)
    {
        $this->pagoStrategy = $pagoStrategy;
    }

    public function createPago($data)
    {
        return $this->pagoStrategy->create($data);
    }
}