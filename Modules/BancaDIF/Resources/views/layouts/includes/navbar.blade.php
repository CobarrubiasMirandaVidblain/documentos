<header class="app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">
    <img class="navbar-brand-full" src="{{ asset('images/ImagenBienestarB.png') }}" alt="IntraDIF">
    <img class="navbar-brand-minimized" src="{{ asset('images/ImagenBienestarA.png') }}" width="30" height="30" alt="CoreUI Logo">
  </a>
  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <ul class="nav navbar-nav ml-auto">
    <li class="nav-item d-md-down-none">
      <a class="nav-link">
        <i class="fa fa-bell"></i>
        {{-- <span class="badge badge-pill badge-danger">5</span> --}}
      </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img class="img-avatar" src="{{ asset('images/user.png')}}" alt="{{auth()->user() ? auth()->user()->persona->nombre : ''}}">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
          <strong>{{auth()->user() ? auth()->user()->persona->nombre : ''}}</strong>
        </div>
        
        <a class="dropdown-item" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
          <i class="fa fa-lock"></i> Logout</a>
        <form id="logout-form" action="/logout" method="POST" style="display: none;">
          <input type="hidden" name="_token" value="{{csrf_token()}}">
        </form>
      </div>
    </li>
  </ul>
</header>