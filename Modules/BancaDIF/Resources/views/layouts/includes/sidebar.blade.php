<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="nav-icon fa fa-home"></i> Home
          {{-- <span class="badge badge-primary">NEW</span> --}}
        </a>
      </li>
        @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR', 'FINANCIERO'], 'fondorotatorio'))
        <li class="nav-item">
            <a class="nav-link" href="/fondorotatorio#/home">
                <i class="nav-icon fa fa-th-large"></i> Administrar
            {{-- <span class="badge badge-primary">NEW</span> --}}
            </a>
        </li>
        @endif
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/enlaces">
          <i class="nav-icon fa fa-users"></i> Enlaces
          {{-- <span class="badge badge-primary">NEW</span> --}}
        </a>
      </li>
      <li class="nav-title">Viaticos</li>
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/crearviatico">
          <i class="nav-icon fa fa-plus"></i> Nuevo Viatico</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/tramites/1">
          <i class="nav-icon fa fa-list"></i> Consultar Viaticos</a>
      </li>
      <li class="nav-title">Vales</li>
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/crearvale">
          <i class="nav-icon fa fa-plus-square"></i> Nuevo Vale</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/tramites/2">
          <i class="nav-icon fa fa-list-ul"></i> Consultar Vales</a>
      </li>
      <li class="nav-title">Gastos en comisión</li>
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/creargasto">
          <i class="nav-icon fa fa-plus-square"></i> Nuevo Gasto</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/fondorotatorio#/tramites/3">
          <i class="nav-icon fa fa-list-ul"></i> Consultar Gasto</a>
      </li>
    @if(auth()->user()->hasRolesStrModulo(['PRESUPUESTAL','ADMINISTRADOR'], 'fondorotatorio'))
        <li class="nav-title">Presupuestal</li>
        <li class="nav-item">
          <a class="nav-link" href="/fondorotatorio#/presupuesto/comprobacion/COMPROBACION">
          <i class="nav-icon fa fa-money"></i> Recuperacion Fondo</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/fondorotatorio#/presupuesto/comprobacion/PAGO">
          <i class="nav-icon fa fa-money"></i> Gastos a Comprobar</a>
        </li>
    @endif
        <li class="nav-title">Enlace</li>
        <li class="nav-item">
            <a class="nav-link" href="/fondorotatorio#/adminenlace">
            <i class="nav-icon fa fa-money"></i> Pagos</a>
        </li>
        <li class="nav-title">Caja</li>
        <li class="nav-item">
            <a class="nav-link" href="/fondorotatorio#/caja">
            <i class="nav-icon fa fa-archive"></i> Pagos Caja</a>
				</li>
			</li>
			@if(auth()->user()->hasRolesStrModulo(['BANCOS','ADMINISTRADOR','FINANCIERO'], 'fondorotatorio'))
				<li class="nav-title">Bancos</li>
					<li class="nav-item">
						<a class="nav-link" href="/fondorotatorio#/bancos">
						<i class="nav-icon fa fa-money"></i> Gastos a Comprobar</a>
				</li>
			@endif
			@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','FINANCIERO','CAJA'], 'fondorotatorio'))
				<li class="nav-title">Reportes</li>
					<li class="nav-item">
						<a class="nav-link" href="/fondorotatorio#/reportes">
						<i class="nav-icon fa fa-list"></i> Reporte</a>
				</li>
			@endif
      {{-- <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon icon-puzzle"></i> Base</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="base/breadcrumb.html">
              <i class="nav-icon icon-puzzle"></i> Breadcrumb</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="base/cards.html">
              <i class="nav-icon icon-puzzle"></i> Cards</a>
          </li>
        </ul>
      </li> --}}
      
			<li class="divider"></li>
			@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'fondorotatorio'))
				<li class="nav-title">Pago </li>
					<li class="nav-item">
						<a class="nav-link" href="/fondorotatorio#/pagodetalle">
						<i class="nav-icon fa fa-list"></i> Pago Detalle</a>
				</li>
			@endif
      
      {{-- <li class="nav-item mt-auto">
        <a class="nav-link nav-link-success" href="https://coreui.io" target="_top">
          <i class="nav-icon icon-cloud-download"></i> Download CoreUI</a>
			</li> --}}
			@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','FINANCIERO'], 'fondorotatorio'))
				<li class="nav-title">Proveedores</li>
					<li class="nav-item">
						<a class="nav-link" href="/fondorotatorio#/proveedores">
						<i class="nav-icon fa fa-user-plus"></i> Proveedores</a>
				</li>
      @endif
      @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','CAJA'], 'fondorotatorio'))
				<li class="nav-title">Reintegros</li>
					<li class="nav-item">
						<a class="nav-link" href="/fondorotatorio#/reintegros">
						<i class="nav-icon fa fa-balance-scale"></i> Reintegros</a>
				</li>
			@endif
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>