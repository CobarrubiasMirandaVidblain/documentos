@extends('dcredencial::layouts.master')

@section('content-subtitle')
Lista de Solicitudes
@endsection

@push('head')

<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>

<div id="dsolicitud">
    <div v-cloak>
        <div class="box box-primary shadow">
            <div class="box-header with-border">
                <h3 class="box-title">Tabla de solicitudes:</h3>
                @if(auth()->user()->hasRoles(['SUPERADMIN', 'COORDINADOR', 'OPERATIVO', 'APOYO OPERATIVO', 'ADMINISTRATIVO', 'LEGAL', 'AUXILIAR', 'FINANCIERO', 'MEDICO', 'CAPTURISTA']))
                @endif
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="dt-buttons">
                        <a  href="{{ route('solicitudfilexls') }}" class="dt-button buttons-excel button-dt tool" tabindex="0" aria-controls="solicitantes">
                            <span><i class="fa fa-file-excel-o"></i> Excel</span>
                        </a>
                        <button  @click.prevent="pendientesLista()" class="dt-button buttons-reload button-dt tool" tabindex="0" aria-controls="solicitantes">
                            <span><i class="fa fa-refresh"></i> Recargar</span>
                        </button> 
                        <div class="input-group input-group-sm col-xs-8 col-sm-8 col-md-8 col-lg-4 pull-right">
                            <input type="text" v-model="busqueda.curp" class="form-control">
                            <span class="input-group-btn">
                                <button  @click.prevent="busquedaDatos()" class="btn btn-info btn-flat" >Buscar</button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-hover data-table dataTable dtr-inline" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Folio</th>
                                <th>Persona</th>
                                <th>Descripcion</th>
                                <th>Fecha inicio</th>
                                <th>Fecha fin</th>
                                <th>Comprobante</th>
                                <th>Estatus</th>
                                @if(auth()->user()->hasRoles(['manuel','root']))
                                    <th>Opciones</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="pendiente in listaPendientes">
                                <td>@{{pendiente.id}}</td>
                                <td>@{{pendiente.nombre}} - <small><b>@{{pendiente.curp}}</b></small></td>
                                <td>@{{pendiente.motivo}}</td>
                                <td>@{{pendiente.fecha_solicitud}}</td>
                                <td>@{{pendiente.created_at}}</td>
                                <td>
                                    <span class="dtr-data" @click="verImagen(pendiente)"><a class="btn btn-info btn-xs"><i class="fa fa-eye"></i> ver</a></span>
                                </td>
                                <td>
                                    <span class="label" :class="pendiente.status_solicitud_id == 1?'label-warning':'label-success'">@{{pendiente.status_solicitud_id == 1? 'Pendiente':'Resuelto'}}</span>
                                </td>
                                @if(auth()->user()->hasRoles(['manuel','root']))
                                    <td>
                                        <span class="dtr-data" @click="opciones(pendiente)"><a class="btn btn-info btn-xs"><i class="fa fa-edit"></i> opcion</a></span>
                                    </td>
                                @endif
                                
                            </tr>
                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination">
                            <li v-if="pagination.current_page > 1">
                                <a href="#" @click.prevent="changePage(pagination.current_page - 1)">
                                    <span>Atras</span>
                                </a>
                            </li>
                            <li v-for="page in pagesNumber" :class="[page == isActived ? 'active':'']">
                                <a href="#" @click.prevent="changePage(page)">
                                    @{{page}}
                                </a>
                            </li>
                            <li  v-if="pagination.current_page < pagination.last_page">
                                <a href="#" @click.prevent="changePage(pagination.current_page + 1)">
                                    <span>Siguiente</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="modalSolicitud" style="display: none;">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4>Comprobante</h4>
                    </div>
                    <div class="modal-body">
                        <div class="box box-success">
                            <div class="box-header ui-sortable-handle" style="cursor: move;" v-if="!modeSave">
                                <div class="col-md-12">
                                    <img :src="pendienteDatos.rutaImagen" alt="Comprobante" style="width:100%;">
                                </div>
                            </div>
                            <div class="box-footer" v-if="modeSave">
                                <select name="status" id="status" class="form-control" v-model="pendienteDatos.status">
                                    <option value="1">Pendiente</option>
                                    <option value="2">Resuelto</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" @click.prevent="saveStatus()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>  
    </div> 
</div>

@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>

<script type="text/javascript" src="{{ asset('bower_components/vue/vee-validate.js') }}"></script>
{{-- {!! $dataTable->scripts() !!} --}}

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>



{{-- <script type="text/javascript" src="{{ asset('solicitud/solicitud.js') }}"></script> --}}

{{-- <script type="text/javascript" src="{{ asset('credencial/jquery.twbsPagination.js') }}"></script> --}}

<script type="text/javascript"> 

</script>
@endpush
