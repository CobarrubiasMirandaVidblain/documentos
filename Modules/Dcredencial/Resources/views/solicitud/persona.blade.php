@extends('dcredencial::layouts.master')

@section('content-subtitle')
Nuevo Beneficiario
@endsection

@push('head')

<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<style>
	.modal-XL{
			width: 75%;
			margin: 15px auto;
	}
	[v-cloak] {display: none;}
</style>

<div id="persona">
	<div v-cloak>
		<div class="box box-primary shadow">
			<div class="box-header with-border">
				<h3 class="box-title">Nuevo</h3>
			</div>
			<div class="box-body">
				<div class="box-body no-padding">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('curp') ? 'has-error' : '' ">
									<label>CURP</label>
									<input type="text" class="form-control" v-model="persona.curp" v-validate="'required:true'" name="curp">
									<span>@{{ errors.first('curp') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('nombre') ? 'has-error' : '' ">
									<label>Nombre</label>
									<input type="text" class="form-control" v-model="persona.nombre" v-validate="'required:true'" name="nombre">
									<span>@{{ errors.first('nombre') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('primerApellido') ? 'has-error' : '' ">
									<label>Primer Apellido</label>
									<input type="text" class="form-control" v-model="persona.primerApellido" v-validate="'required:true'" name="primerApellido">
									<span>@{{ errors.first('primerApellido') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('segundoApellido') ? 'has-error' : '' ">
									<label>Segundo Apellido</label>
									<input type="text" class="form-control" v-model="persona.segundoApellido" v-validate="'required:true'" name="segundoApellido">
									<span>@{{ errors.first('segundoApellido') }}</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('fechaNacimiento') ? 'has-error' : '' ">
									<label>Fecha Nacimiento</label>
									<input type="date" class="form-control" v-model="persona.fechaNacimiento" v-validate="'required:true'" name="fechaNacimiento">
									<span>@{{ errors.first('fechaNacimiento') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('genero') ? 'has-error' : '' ">
									<label for="genero">Genero</label>
									<select class="form-control" name="genero" id="genero" v-model="persona.genero" v-validate="'required:true'">
										<option value="M">Masculino</option>
										<option value="F">Femenino</option>
									</select>
									<span>@{{ errors.first('genero') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('clave') ? 'has-error' : '' ">
									<label for="celectoral">Clave electoral</label>
									<input type="text" class="form-control" id="celectoral" placeholder="Clave electoral" name="clave" v-model="persona.claveElectoral" v-validate="{ required: true, digits:13 }">
									<span>@{{ errors.first('clave') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('email') ? 'has-error' : '' ">
										<label>Correo electronico</label>
										<input class="form-control" placeholder="correo" v-model="persona.email"  v-validate="{ required: true, email: true }" type="email" name="email">
										<span>@{{ errors.first('email') }}</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('celular') ? 'has-error' : '' ">
									<label>Celular</label>
									<input type="text" class="form-control" placeholder="Celular" v-model="persona.celular" name="celular" v-validate="{ required: true, digits:10 }">
									<span>@{{ errors.first('celular') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('telefono') ? 'has-error' : '' ">
									<label>Telefono</label>
									<input type="text" class="form-control" placeholder="telefono" v-model="persona.telefono" name="telefono" v-validate="{ required: true, digits:10 }">
									<span>@{{ errors.first('telefono') }}</span>
								</div>
							</div>
							<div class="container">
								<div class="col-md-3">
									<label>Fotografia
										<input type="file" id="file" ref="file" v-on:change="handleFileUpload()"/>
									</label>
								</div>
							</div>
						</div>
						<div class="box-header with-border">
							<h3 class="box-title">Dirección</h3>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('calle') ? 'has-error' : '' ">
									<label for="calle">Calle</label>
									<input type="text" class="form-control" placeholder="Calle" v-model="persona.calle" name="calle" v-validate="'required:true'">
									<span>@{{ errors.first('calle') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('colonia') ? 'has-error' : '' ">
									<label for="colonia">Colonia</label>
									<input type="text" class="form-control" placeholder="Colonia" v-model="persona.colonia" name="colonia" v-validate="'required:true'">
									<span>@{{ errors.first('colonia') }}</span>
								</div>
							</div>
							<div class="col-md-2" >
								<div class="form-group" >
									<label for="numero">Numero Int.</label>
									<input type="text" class="form-control" placeholder="Numero" v-model="persona.numInt" name="NoInterior" >
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group" :class="errors.first('NoExterior') ? 'has-error' : '' ">
									<label for="aPaterno">Numero Ext.</label>
									<input type="text" class="form-control" placeholder="Numero Exterior" v-model="persona.numExt" name="NoExterior" v-validate="'required:true'">
									<span>@{{ errors.first('NoExterior') }}</span>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group" :class="errors.first('CP') ? 'has-error' : '' ">
									<label for="cp">Codigo postal</label>
									<input type="text" class="form-control" id="cp" placeholder="Codigo postal" v-model="persona.cp" name="CP"  v-validate="{ required: true, digits:5 }" >
									<span>@{{ errors.first('CP') }}</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('municipio') ? 'has-error' : '' ">
									<label>Municipio</label>
									<multiselect :class="errors.first('municipio') ? 'has-error': ''" v-model="persona.municipio" :options="listaMunicipios" value="'id'" label="nombre" name="municipio" v-validate="'required'" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="buscarMunicipio" :block-keys="['Delete']" placeholder="Selecciona">
										<template slot="noOptions">Comience a escribir para buscar.</template>
										<template slot="noResult"><em style="color: #F2698D;">No tenemos registro con lo que estas buscando</em></template>
									</multiselect>
									<span>@{{ errors.first('municipio') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('Localidad') ? 'has-error' : '' ">
									<label>Localidad</label>
									<multiselect
										name="Localidad"
										v-model="persona.localidad" 
										:options="listaLocalidad" 
										:clear-on-select="true"
										:preserve-search="false"
										placeholder="Busca o agrega la localidad"
										label="nombre"
										track-by="nombre"
										:internal-search="false"
										@search-change="buscarLocalidad"
										tag-placeholder="Enter para agregar nueva presentación"
										v-validate="'required'"
										:block-keys="['Delete']"
										>
											<template slot="noOptions">Comience a escribir para buscar.</template>
											<template slot="noResult"><em style="color: #F2698D;">No tenemos registro con lo que estas buscando</em></template>
									</multiselect>
									<span>@{{ errors.first('Localidad') }}</span>
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group" :class="errors.first('referencia') ? 'has-error' : '' ">
									<label>Referencias del Domicilio:</label>
									<textarea class="form-control" rows="1" name="referencia" v-model="persona.referencia" v-validate="'required:true'"></textarea>
									<span>@{{ errors.first('referencia') }}</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('institucion') ? 'has-error' : '' ">
									<label for="institucion">Institucion</label>
									<select name="institucion" class="form-control" v-model="persona.institucion" v-validate="'required:true'">
											<option value="">Selecciona Opcion</option>
											<option  v-for="inst in listIntituciones" :value="inst.id">@{{inst.nombre}}</option>
									</select>
									<span>@{{ errors.first('institucion') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<label>Constancia
									<input type="file" id="constancia" ref="constancia" v-on:change="constancia()"/>
								</label>
							</div>
						</div>
						<div class="box-header with-border">
							<h3 class="box-title">Datos de contacto en caso de emergencia</h3>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('NombreContacto') ? 'has-error' : '' ">
									<label>Nombre</label>
									<input class="form-control" placeholder="Nombre" v-model="persona.nombreC" v-validate="{ required: true}" type="text" name="NombreContacto">
									<span>@{{ errors.first('NombreContacto') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('ApellidoContacto') ? 'has-error' : '' ">
									<label>Primer Apellido</label>
									<input class="form-control" placeholder="Primer Apellido" v-model="persona.apellidoC"  v-validate="{ required: true}" type="text" name="ApellidoContacto">
									<span>@{{ errors.first('ApellidoContacto') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('SApellidoContacto') ? 'has-error' : '' ">
									<label>Segundo Apellido</label>
									<input class="form-control" placeholder="Segundo Apellido" v-model="persona.apellidoSC"  v-validate="{ required: true}" type="text" name="SApellidoContacto">
									<span>@{{ errors.first('SApellidoContacto') }}</span>
								</div>
							</div>
						</div>
						<div class="box-header with-border">
							<h3 class="box-title">Direccion del contacto</h3>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('calleContacto') ? 'has-error' : '' ">
									<label >Calle</label>
									<input type="text" class="form-control" placeholder="Calle" v-model="persona.calleC" name="calleContacto" v-validate="'required:true'">
									<span>@{{ errors.first('calleContacto') }}</span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('coloniaContacto') ? 'has-error' : '' ">
									<label>Colonia</label>
									<input type="text" class="form-control" placeholder="Colonia" v-model="persona.coloniaC" name="coloniaContacto" v-validate="'required:true'">
									<span>@{{ errors.first('coloniaContacto') }}</span>
								</div>
							</div>
							<div class="col-md-3" >
								<div class="form-group" :class="errors.first('Numero') ? 'has-error' : '' ">
									<label for="numero">Numero</label>
									<input type="text" class="form-control" placeholder="Numero" v-model="persona.numeroC" name="Numero" v-validate="'required:true'">
									<span>@{{ errors.first('Numero') }}</span>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('municipioA') ? 'has-error' : '' ">
									<label>Municipio</label>
									<multiselect v-model="persona.municipioC" :options="listaMunicipios" value="'id'" label="nombre" name="municipioA" v-validate="'required'" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="buscarMunicipio" :block-keys="['Delete']" placeholder="Selecciona">
										<template slot="noOptions">Comience a escribir para buscar.</template>
										<template slot="noResult"><em style="color: #F2698D;">No tenemos registro con lo que estas buscando</em></template>
									</multiselect>
									<span>@{{ errors.first('municipioA') }}</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('Localidad') ? 'has-error' : '' ">
									<label>Localidad</label>
									<multiselect
										name="Localidad"
										v-model="persona.localidadC" 
										:options="listaLocalidad" 
										:clear-on-select="true"
										:preserve-search="false"
										placeholder="Busca o agrega la localidad"
										label="nombre"
										track-by="nombre"
										:internal-search="false"
										@search-change="buscarLocalidadC"
										tag-placeholder="Enter para agregar nueva presentación"
										v-validate="'required'"
										:block-keys="['Delete']"
										>
										<template slot="noOptions">Comience a escribir para buscar.</template>
										<template slot="noResult"><em style="color: #F2698D;">No tenemos registro con lo que estas buscando</em></template>
								</multiselect>
									<span>@{{ errors.first('Localidad') }}</span>
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="form-group" :class="errors.first('CPContacto') ? 'has-error' : '' ">
									<label>Codigo postal</label>
									<input type="text" class="form-control" placeholder="Codigo postal" v-model="persona.cpC" name="CPContacto"  v-validate="{ required: true, digits:5 }">
									<span>@{{ errors.first('CPContacto') }}</span>
								</div>
							</div>
							<div class="col-md-3">
									<div class="form-group" :class="errors.first('telefonoC') ? 'has-error' : '' ">
										<label for="telefonoC">Telefono</label>
										<input type="text" class="form-control" placeholder="telefono" v-model="persona.telefonoC" name="telefonoC" v-validate="{ required: true, digits:7 }">
										<span>@{{ errors.first('telefonoC') }}</span>
									</div>
							</div>
						</div>

						<button type="button" class="btn btn-block btn-success" @click.prevent="savePersona()">Guardar</button>
					</div>
				</div>
			</div>
		</div>   
	</div>       
</div>

@stop

@push('body')

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- vee-validate -->
<script type="text/javascript" src="{{ asset('bower_components/vue/vee-validate.js') }}"></script>
<!-- vue-multiselect -->
<script type="text/javascript" src="{{ asset('bower_components/vue/vue-multiselect.min.js') }}"></script>

<script type="text/javascript">
	Vue.use(VeeValidate);
	var credencial = new Vue ({
		components: {
			Multiselect: window.VueMultiselect.default
		},
		el:'#persona',
			data:{
				persona:{
					nombre:null,
					primerApellido:null,
					segundoApellido:null,
					fechaNacimiento:null,
					curp:null,
					genero:null,
					claveElectoral:null,
					calle:null,
					colonia:null,
					numInt:null,
					numExt:null,
					cp:null,
					municipio:null,
					localidad:null,
					referencia:null,
					email:null,
					telefono:null,
					celular:null,
					institucion:null,
					telefonoC:null,
					cpC:null,
					localidadC:null,
					municipioC:null,
					numeroC:null,
					coloniaC:null,
					calleC:null,
					apellidoSC:null,
					apellidoC:null,
					nombreC:null
				},
				listaMunicipios:[],
				listaLocalidad:[],
				listIntituciones:[],
				fotografia: null,
				certificado: null
				
			},
			methods:{
				getInstituciones(){
					let me = this
					axios.get("/dcredencial/getInstituciones").then(r =>{
						me.listIntituciones = r.data
					}).catch(error =>{
						console.log("Error de conexion, Codigo: 002 "+error)
					})
				},
				buscarMunicipio(search){
					this.listaMunicipios = []
					if(search.length < 3) return;
					axios.get(`/dcredencial/municipios/${search}`).then(response => { 
						this.listaMunicipios = response.data.data; 
					}).catch(error => { console.log(error) });
				},
				buscarLocalidad(search){
					let me = this;
					if(this.persona.municipio.hasOwnProperty('id')){
						axios.get(`/dcredencial/municipios/${me.persona.municipio.id}/localidades/${search}`).then(response => {
							me.listaLocalidad = response.data.data;
						}).catch(error => { console.log("error") });
					}
				},
				buscarLocalidadC(search){
					let me = this;
					me.listaLocalidad = []
					if(this.persona.municipioC.hasOwnProperty('id')){
						axios.get(`/dcredencial/municipios/${me.persona.municipioC.id}/localidades/${search}`).then(response => {
							me.listaLocalidad = response.data.data;
						}).catch(error => { console.log("error") });
					}
				},
				savePersona(){
					this.$validator.validate().then(valid => {
						if (!valid) {
							return swal("Existen campos vacios")
						}
						block();
						let me = this;
						let data = new  FormData();
						data.append('fotografia', me.fotografia);
						data.append('certificado', this.certificado);
						$.each( me.persona, function( key, value ) {
							data.append(key, value);
						});
						data.append('municipio', me.persona.municipio.id);
						data.append('localidad', me.persona.localidad.id);
						data.append('localidadC', me.persona.localidadC.id);
						data.append('municipioC', me.persona.municipioC.id);
						data.append('_method', 'post');
						axios.post("/dcredencial/nuevo",data,{headers: {'Content-Type': 'multipart/form-data'}}).then(r => {
							unblock();
							if (r.data.success){
								me.resetDatos();
								this.$validator.reset();
								swal({type: 'success', title: 'Bien',	text: 'Se guardo correctamente!',	footer: 'Clave: '+r.data.id})
							}
							else
								swal({type: 'error', title: 'Oops...', text: 'Ocurrio un error!',	footer: "Error "+r.data.message})
							
						}).catch(error => {
							unblock();
							swal("Error!", "Error al guardar", "warning");
						})
					});
				},
				resetDatos(){
					let me = this;
					me.persona.nombre = null,
					me.persona.primerApellido = null,
					me.persona.segundoApellido = null,
					me.persona.fechaNacimiento = null,
					me.persona.curp = null,
					me.persona.genero = null,
					me.persona.claveElectoral = null,
					me.persona.calle = null,
					me.persona.colonia = null,
					me.persona.numInt = null,
					me.persona.numExt = null,
					me.persona.cp = null,
					me.persona.municipio = null,
					me.persona.localidad = null,
					me.persona.referencia = null,
					me.persona.email = null,
					me.persona.telefono = null,
					me.persona.celular = null,
					me.persona.institucion = null,
					me.persona.telefonoC = null,
					me.persona.cpC = null,
					me.persona.localidadC = null,
					me.persona.municipioC = null,
					me.persona.numeroC = null,
					me.persona.coloniaC = null,
					me.persona.calleC = null,
					me.persona.apellidoSC = null,
					me.persona.apellidoC = null,
					me.persona.nombre = null				
					me.listaMunicipios = [],
					me.listaLocalidad = [],
					me.fotografia =  null,
					me.certificado =  null
				},
				handleFileUpload(){
					this.fotografia = this.$refs.file.files[0];
				},
				constancia(){
					this.certificado = this.$refs.constancia.files[0];
				}
			},
			mounted() {
				this.getInstituciones();
			},
			watch: {
				
			},
	})
</script>

@endpush

