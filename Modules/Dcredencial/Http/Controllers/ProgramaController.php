<?php

namespace Modules\Dcredencial\Http\Controllers;

use App\Http\Controllers\ProgramaBaseController; 
use Illuminate\Routing\Controller; 

class ProgramaController extends ProgramaBaseController {
    function __construct() {
        parent::__construct('CREDENCIALIZACION', 'dcredencial'); // cambiar datos
    }
}