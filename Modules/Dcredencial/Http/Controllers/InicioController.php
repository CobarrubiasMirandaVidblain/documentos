<?php

namespace Modules\Dcredencial\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Municipio;
use App\Models\Programa;
use Illuminate\Support\Facades\DB;

class InicioController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('dcredencial::inicio.inicio');

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dcredencial::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('dcredencial::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('dcredencial::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function consultaDatos (){

			$programa = Programa::select('id','nombre')->where('nombre','CREDENCIALIZACION')->whereNull('padre_id')->with('aniosprogramas:id,programa_id')->first();
			$grafica = DB::table('cat_municipios')
			->select(DB::raw('count(personas.id) as cantidad, cat_regiones.nombre'))
			->join('cat_distritos','cat_municipios.distrito_id','=','cat_distritos.id')
			->join('cat_regiones','cat_distritos.region_id','=','cat_regiones.id')
			->join('personas','cat_municipios.id','=','personas.municipio_id')
			->join('personas_programas','personas_programas.persona_id','=','personas.id')
			->where('personas_programas.anios_programa_id',$programa->aniosprogramas[0]['id'])
			// ->where('personas_tipos.deleted_at','=',null)
			->groupBy('cat_regiones.id','cat_regiones.nombre')
			->get();

			$labels = collect();
			$datos  = collect();
			$color  = collect();
		
			$colores = ['rgb(255, 195, 0)','rgb(255, 51, 193)','rgb(245, 13, 37)','rgb(144, 51, 255)','rgb(51, 144, 255)','rgb(17, 120, 16)','rgb(51, 255, 57)','rgb(255, 87, 51)'];
			$i=0;
			foreach ($grafica as $key => $value) {
					$i++;
					$datos->push($value->cantidad);
					$labels->push($value->nombre);
					$color->push(str_replace(')', ',0.55)', $colores[$i]));
			}

			$edades = DB::table('personas')
			->select(DB::raw(' personas.genero, YEAR(CURDATE())-YEAR(personas.fecha_nacimiento)+IF(DATE_FORMAT(CURDATE(),"%m-%d")>DATE_FORMAT(personas.fecha_nacimiento,"%m-%d"),0,-1) as edad'))
			->join('personas_programas','personas_programas.persona_id','=','personas.id')
			->where('personas_programas.anios_programa_id',$programa->aniosprogramas[0]['id'])
			->get();

			$mujeres = 0;
			$hombre = 0;
			$menorH = 0;
			$menorM = 0;

			foreach ($edades as $key => $value) {
					
					if($value->genero == 'M' && $value->edad >=18)
							$hombre++;
					elseif ($value->genero == 'F' && $value->edad <18)
							$menorM++;
					elseif ($value->genero == 'M' && $value->edad <18)
							$menorH++;
					else
							$mujeres++;
					
			};
			
			return [

					'hombres'   =>  $hombre,
					'mujeres'   =>  $mujeres,
					'menorH'    =>  $menorH,
					'menorM'    =>  $menorM,
					'datos'     =>  $datos,
					'labels'    =>  $labels,
					'color'     =>  $color
			];

        // dd($edades);
    }
}
