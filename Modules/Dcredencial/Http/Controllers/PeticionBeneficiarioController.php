<?php

namespace Modules\Dcredencial\Http\Controllers;

use App\Http\Controllers\PeticionBeneficiarioBaseController; 

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController { 
    public function __construct() {
        parent::__construct('CREDENCIALIZACION');
    }
}
