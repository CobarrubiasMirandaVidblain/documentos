let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js').sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/auth.js', 'public/js').sass('resources/assets/sass/auth.scss', 'public/css');

mix.js('resources/assets/js/admin-lte.js', 'public/js').sass('resources/assets/sass/admin-lte.scss', 'public/css');

mix.autoload({
  jquery: ['$', 'jQuery', 'jquery'],
});

mix.extract([
  'lodash',
  'jquery',
  'bootstrap-sass',
  'fastclick',
  'jquery-slimscroll',
  'admin-lte',
  'vue',
  'axios',
  'sweetalert2',
  'datatables.net-buttons-bs',
  'datatables.net-responsive-bs',
  'datatables.net-rowgroup-bs',
  'bootstrap-daterangepicker',
  'jquery-validation'
], 'public/js/vendor.js');

mix.styles([
  'node_modules/datatables.net-bs/css/dataTables.bootstrap.min.css',
  'node_modules/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
  'resources/assets/css/datatables/responsive.bootstrap.info.css',
  //'node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
  'node_modules/datatables.net-rowgroup-bs/css/rowGroup.bootstrap.min.css',
  //'node_modules/bootstrap-daterangepicker/daterangepicker.scss'
], 'public/css/base.css');

mix.scripts([
  'resources/assets/js/blockui/jquery.blockUI.js',
  'resources/assets/js/datatables/buttons.server-side.js',
  'resources/assets/js/jquery-validation/Defaults.js',
  'resources/assets/js/jquery-validation/Spanish.js'
], 'public/js/base.js');

mix.version();