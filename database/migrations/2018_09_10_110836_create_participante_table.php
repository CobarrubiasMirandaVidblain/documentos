<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipanteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participante', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->string('primerApellido');
            $table->string('segundoApellido');
            $table->enum('genero',['H','M']);
            $table->date('fecha_nacimiento');
            $table->string('telefono')->nullable();
            $table->string('curp')->unique();
            $table->string('correo')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participante');
    }
}
