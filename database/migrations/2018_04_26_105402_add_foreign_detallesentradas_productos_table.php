<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDetallesentradasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detallesentradas_productos', function (Blueprint $table) {
            $table->foreign('entradas_producto_id')->references('id')->on('entradas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('areas_producto_id')->references('id')->on('areas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detallesentradas_productos', function (Blueprint $table) {
            $table->dropForeign(["entradas_producto_id"]);
            $table->dropForeign(["areas_producto_id"]);
        });
    }
}
