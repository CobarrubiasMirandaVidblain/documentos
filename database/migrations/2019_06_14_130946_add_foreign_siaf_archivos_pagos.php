<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar los pagos con sus respectivos archivos
    ALCANCE: fondorotatorio
*/

class AddForeignSiafArchivosPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_archivos_pagos', function (Blueprint $table) {
            $table->foreign('pago_id')->references('id')->on('siaf_pagos');
            $table->foreign('archivo_id')->references('id')->on('siaf_archivos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_archivos_pagos', function (Blueprint $table) {
            $table->dropForeign(['pago_id']);
            $table->dropForeign(['archivo_id']);

            $table->dropIndex('siaf_archivos_pagos_pago_id_foreign');
            $table->dropIndex('siaf_archivos_pagos_archivo_id_foreign');
        });
    }
}
