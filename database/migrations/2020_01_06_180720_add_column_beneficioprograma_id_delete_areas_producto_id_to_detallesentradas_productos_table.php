<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBeneficioprogramaIdDeleteAreasProductoIdToDetallesentradasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detallesentradas_productos', function (Blueprint $table) {
            $table->unsignedInteger('beneficioprograma_id')->after('precio');
        });

        Schema::table('detallesentradas_productos', function (Blueprint $table) {
          $table->dropForeign(['areas_producto_id']);
          $table->dropIndex('detallesentradas_productos_areas_producto_id_foreign');

          $table->foreign('beneficioprograma_id')->references('id')->on('beneficiosprogramas');
        });

        Schema::table('detallesentradas_productos', function (Blueprint $table) {
          $table->dropColumn('areas_producto_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('detallesentradas_productos', function (Blueprint $table) {
        $table->unsignedInteger('areas_producto_id')->after('precio');
      });

        Schema::table('detallesentradas_productos', function (Blueprint $table) {
          $table->dropForeign(['beneficioprograma_id']);
          $table->dropIndex('detallesentradas_productos_beneficioprograma_id_foreign');

          $table->foreign('areas_producto_id')->references('id')->on('areas_productos');
        });

        Schema::table('detallesentradas_productos', function (Blueprint $table) {
          $table->dropColumn('beneficioprograma_id');
      });
    }
}
