<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para almacenar la conversación que se tiene acerca de la respuesta sin oficio
    ALCANCE: SICODOC
*/

class AddForeignSdocChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_chat', function (Blueprint $table) {
            $table->foreign('seguimiento_documento_id')->references('id')->on('sdoc_seguimiento_documentos');
            $table->foreign('area_id')->references('id')->on('cat_areas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_chat', function (Blueprint $table) {
            $table->dropForeign(['seguimiento_documento_id']);
            $table->dropForeign(['area_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('sdoc_chat_seguimiento_documento_id_foreign');
            $table->dropIndex('sdoc_chat_area_id_foreign');
            $table->dropIndex('sdoc_chat_usuario_id_foreign');
        });
    }
}
