<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_Alumnos', function(Blueprint $table){
        $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_Alumnos', function(Blueprint $table){
        $table->dropForeign(['persona_id']);
      });
    }
}
