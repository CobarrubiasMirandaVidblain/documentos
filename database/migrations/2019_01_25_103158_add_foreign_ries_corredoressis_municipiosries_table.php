<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de riesgos de temas sísmicos, inundaciones, vientos fuertes, heladas, deslizamientos e incendios forestales
  ALCANCE: Riesgos
*/

class AddForeignRiesCorredoressisMunicipiosriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('ries_corredoressis_municipiosries', function (Blueprint $table) {
      $table->foreign('corredor_id')->references('id')->on('ries_cat_corredoressismicos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('municipio_riesgo_id')->references('id')->on('ries_municipios_riesgos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('ries_corredoressis_municipiosries', function (Blueprint $table) {
      $table->dropForeign(['corredor_id']);
      $table->dropForeign(['municipio_riesgo_id']);

      $table->dropIndex('ries_corredoressis_municipiosries_corredor_id_foreign');
      $table->dropIndex('ries_corredoressis_municipiosries_municipio_riesgo_id_foreign');
    });
  }
}
