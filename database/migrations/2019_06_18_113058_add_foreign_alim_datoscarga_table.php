<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimDatoscargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->foreign('vehiculoproveedor_id')->references('id')->on('vehiculos');
            $table->foreign('choferproveedor_id')->references('id')->on('alim_choferespreveedor');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->dropForeign(['vehiculoproveedor_id']);
            $table->dropForeign(['choferproveedor_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_datoscarga_vehiculoproveedor_id_foreign');
            $table->dropIndex('alim_datoscarga_choferproveedor_id_foreign');
            $table->dropIndex('alim_datoscarga_usuario_id_foreign');
        });
    }
}
