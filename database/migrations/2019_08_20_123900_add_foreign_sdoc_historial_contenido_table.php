<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Anthony.
    MOTIVO: Llaves foráneas para tabla de historial de contenido de un documento
    ALCANCE: SICODOC
*/


class AddForeignSdocHistorialContenidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_historial_contenido', function (Blueprint $table) {
            $table->foreign('documento_id')->references('id')->on('sdoc_documentos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_historial_contenido', function (Blueprint $table) {
            $table->dropForeign(['documento_id']);
            $table->dropForeign(['usuario_id']);
						$table->dropIndex('sdoc_historial_contenido_documento_id_foreign');
						$table->dropIndex('sdoc_historial_contenido_usuario_id_foreign');
        });
    }
}
