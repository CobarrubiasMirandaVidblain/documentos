<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToEntradasproductosDependenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('entradasproductos_dependencias', function (Blueprint $table) {
            $table->foreign('dependencia_id')->references('id')->on('cat_dependencias')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('entradas_producto_id')->references('id')->on('entradas_productos')
                ->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('entradasproductos_dependencias', function (Blueprint $table) {
            $table->dropForeign(['dependencia_id']);
            $table->dropForeign(['entradas_producto_id']);
        });
    }
}
