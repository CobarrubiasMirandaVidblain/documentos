<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para indicar el id de programa que tienen los registros que únicamente son de desarrollo
    ALCANCE: Alimentarios
*/

class RenameEsdesarrolloToDedesarrolloProgramaId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_sujetos', function (Blueprint $table) {
						$table->unsignedInteger('esdesarrollo')->nullable(true)->change();
        });

				Schema::table('alim_sujetos', function (Blueprint $table) {
            $table->renameColumn('esdesarrollo', 'dedesarrollo_programa_id');
        });

				DB::statement('UPDATE alim_sujetos SET dedesarrollo_programa_id=null WHERE dedesarrollo_programa_id=0');

				Schema::table('alim_sujetos', function (Blueprint $table) {
            $table->foreign(['dedesarrollo_programa_id'])->references('id')->on('programas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_sujetos', function (Blueprint $table) {
          $table->dropForeign(['dedesarrollo_programa_id']);
					$table->dropIndex('alim_sujetos_dedesarrollo_programa_id_foreign');
        });

				DB::table('alim_sujetos')->whereNull('dedesarrollo_programa_id')->update(['dedesarrollo_programa_id'=>0]);

				Schema::table('alim_sujetos', function (Blueprint $table) {
            $table->boolean('dedesarrollo_programa_id')->nullable(false)->change();
        });

				Schema::table('alim_sujetos', function (Blueprint $table) {
						$table->renameColumn('dedesarrollo_programa_id', 'esdesarrollo');
        });
    }
}
