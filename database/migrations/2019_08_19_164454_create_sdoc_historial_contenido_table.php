<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Anthony.
    MOTIVO: Para guardar el registro de cambios en el contenido de un documento
    ALCANCE: SICODOC
*/

class CreateSdocHistorialContenidoTable extends Migration
{
    /**
     * Run the migrations.
		 * 
		 * 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_historial_contenido', function (Blueprint $table) {
						$table->increments('id');
						$table->unsignedInteger('documento_id');
						$table->text('contenido');
            $table->unsignedInteger('usuario_id');
						$table->timestamps();
						$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_historial_contenido');
    }
}
