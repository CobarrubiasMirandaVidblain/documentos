<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolumnsNombretamanioArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archivos', function(Blueprint $table){
          $table->string('nombre',50)->after('short_url');
          $table->string('tamanio',15)->after('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archivos', function(Blueprint $table){
          $table->dropColumn('nombre');
          $table->dropColumn('tamanio');
        });
    }
}
