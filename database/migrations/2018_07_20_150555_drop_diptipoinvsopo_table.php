<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropDiptipoinvsopoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('inventario',function(Blueprint $table){
        $table->dropForeign(['dispositivo_id']);
        $table->dropForeign(['equipo_id']);
        $table->dropForeign(['resguardo_id']);
      });
      Schema::table('soportesolicitudes_prestamos', function(Blueprint $table){
        $table->dropForeign(['inventario_id']);
      });

      Schema::dropIfExists('dispositivos');
      Schema::dropIfExists('tiposequipos');
      Schema::dropIfExists('inventario');
      Schema::dropIfExists('soportesolicitudes_prestamos');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      /*$table->dropIndex(['state']);*/
      Schema::create('dispositivos', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('marca_id')->unsigned();
        $table->integer('modelo_id')->unsigned();
        $table->string('numero_serie');
        $table->string('tipo');
        $table->string('nombre');
        $table->string('capacidad');
        $table->string('observaciones');
        $table->timestamps();
        $table->softDeletes();

        $table->foreign('marca_id')->references('id')->on('cat_marcasequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('modelo_id')->references('id')->on('cat_modelosequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });

      Schema::create('tiposequipos', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('marca_id')->unsigned();
        $table->integer('modelo_id')->unsigned();
        $table->string('numero_serie');
        $table->string('tipo');
        $table->string('nombre');
        $table->string('estado');
        $table->string('direccionip');
        $table->string('observaciones');
        $table->timestamps();
        $table->softDeletes();

        $table->foreign('marca_id')->references('id')->on('cat_marcasequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('modelo_id')->references('id')->on('cat_modelosequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });

      Schema::create('inventario',function(Blueprint $table){
        $table->increments('id');
        $table->integer('equipo_id')->unsigned();
        $table->integer('dispositivo_id')->unsigned();
        $table->integer('resguardo_id')->unsigned();
        $table->boolean('oficial');
        $table->timestamps();
        $table->softDeletes();

        $table->foreign('equipo_id')->references('id')->on('tiposequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('dispositivo_id')->references('id')->on('dispositivos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('resguardo_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
      });

      Schema::create('soportesolicitudes_prestamos', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('solicitud_id')->unsigned();
        $table->integer('inventario_id')->unsigned();
        $table->timestamps();
        $table->softDeletes();

        $table->foreign('solicitud_id')->references('id')->on('soportesolicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('inventario_id')->references('id')->on('inventario')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
    }
}
