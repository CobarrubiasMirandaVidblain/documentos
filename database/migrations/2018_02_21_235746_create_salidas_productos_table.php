<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalidasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salidas_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('fechasalida');
            $table->string('recibo')->nullable();
            $table->integer('tipossalida_id')->unsigned();
            $table->integer('persona_id')->unsigned()->nullable();
            $table->integer('empleado_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salidas_productos');
    }
}
