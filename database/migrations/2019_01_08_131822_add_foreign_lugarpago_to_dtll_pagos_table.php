<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class AddForeignLugarpagoToDtllPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_pagos', function (Blueprint $table) {
      $table->foreign('lugarpago_id')->references('id')->on('dtll_cat_lugarespago')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_pagos', function (Blueprint $table) {
      $table->dropForeign(['lugarpago_id']);
      $table->dropIndex('dtll_pagos_lugarpago_id_foreign');
    });
  }
}
