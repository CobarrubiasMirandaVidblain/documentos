<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeGruposdisponiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vale_gruposdisponibles', function (Blueprint $table) {
            $table->foreign('nivel_id')->references('id')->on('gruposniveles')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('grupo_id')->references('id')->on('vale_grupos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vale_gruposdisponibles', function (Blueprint $table) {
            $table->dropForeign(["nivel_id"]);
            $table->dropForeign(["grupo_id"]);
        });
    }
}
