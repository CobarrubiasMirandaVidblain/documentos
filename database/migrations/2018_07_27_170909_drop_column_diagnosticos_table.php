<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('diagnosticos', function(Blueprint $table){
        $table->dropForeign(['discapacidad_id']);
        $table->dropForeign(['suive_id']);
        $table->dropColumn('discapacidad_id');
        $table->dropColumn('suive_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('diagnosticos', function(Blueprint $table){
        $table->integer('discapacidad_id')->after('cie10_id')->unsigned()->nullable();
        $table->integer('suive_id')->after('discapacidad_id')->unsigned();
        $table->foreign('discapacidad_id')->references('id')->on('cat_discapacidades')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('suive_id')->references('id')->on('cat_suive')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }
}
