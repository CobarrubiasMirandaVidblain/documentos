<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasclinicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notasclinicas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_programa_id')->unsigned();
            $table->string('titulo');
            $table->string('descripcion');
            $table->integer('usuario_id')->unsigned();
            $table->integer('curso_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notasclinicas');
    }
}
