<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_archivos
  ALCANCE: Para el sistema SICODOC
*/


class AddForeignSdocArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_archivos', function (Blueprint $table) {
            $table->foreign('tipoarchivo_id')->references('id')->on('sdoc_cat_tiposarchivo');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            

          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_archivos', function (Blueprint $table) {
            $table->dropForeign(['tipoarchivo_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('sdoc_archivos_tipoarchivo_id_foreign');
            $table->dropIndex('sdoc_archivos_usuario_id_foreign');
        });
    }
}
