<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimComitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_comites', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('persona_id');
            $table->unsignedInteger('cargo_id');
            $table->unsignedInteger('programacion_id');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_comites');
    }
}
