<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel, Jhony, Emanuel.
    MOTIVO: Para ingresar datos de personas que tienen un beneficios sin generar una solicitud
    ALCANCE: Solicitudes
*/

class CreateBeneficiosprogPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiosprog_personas', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('beneficioprograma_id');
            $table->unsignedInteger('persona_id');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiosprog_personas');
    }
}
