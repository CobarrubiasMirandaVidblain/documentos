<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de riesgos de temas sísmicos, inundaciones, vientos fuertes, heladas, deslizamientos e incendios forestales
  ALCANCE: Riesgos
*/

class CreateRiesMunicipiosRiesgosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('ries_municipios_riesgos', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('municipio_id');
      $table->unsignedInteger('riesgo_id');
      
      $table->unsignedInteger('usuarios_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('ries_municipios_riesgos');
  }
}
