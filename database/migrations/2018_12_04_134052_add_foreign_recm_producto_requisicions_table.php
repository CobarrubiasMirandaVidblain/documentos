<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmProductoRequisicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_producto_requisicions
        Schema::table('recm_producto_requisicions', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('recm_requisicions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_producto_id')->references('id')->on('recm_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('status_id')->references('id')->on('recm_cat_estatuses')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }
//presentacion_producto_id
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_producto_requisicions', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropForeign(['status_id']);
            $table->dropIndex('recm_producto_requisicions_requisicion_id_foreign');
            $table->dropIndex('recm_producto_requisicions_presentacion_producto_id_foreign');
            $table->dropIndex('recm_producto_requisicions_status_id_foreign');
        });
    }
}
