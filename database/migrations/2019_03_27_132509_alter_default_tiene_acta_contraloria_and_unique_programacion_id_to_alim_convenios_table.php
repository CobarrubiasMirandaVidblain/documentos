<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Cambios necesarios en la tabla
  ALCANCE: Para el sistema alimentarios
*/

class AlterDefaultTieneActaContraloriaAndUniqueProgramacionIdToAlimConveniosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_convenios', function (Blueprint $table) {
      $table->boolean('tiene_acta_contraloria')->tinyInteger('tiene_acta_contraloria')->unsigned()->default(0)->change();
      $table->unique(['programacion_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_convenios', function (Blueprint $table) {
      $table->boolean('tiene_acta_contraloria')->tinyInteger('tiene_acta_contraloria')->unsigned()->change();
      $table->dropUnique(['programacion_id']);
    });
  }
}
