<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para que la tabla de alim_desayunos deje de relacionarse con alim_cat_escuelas y ahora se relacione con cat_escuelas
  ALCANCE: Alimentarios
*/

class DelForeignEscuelaIdToAlimDesayunos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_desayunos', function (Blueprint $table) {
            $table->dropForeign(['escuela_id']);
            $table->dropIndex('alim_desayunos_escuela_id_foreign');
        });

        Schema::table('alim_desayunos', function (Blueprint $table) {
            $table->foreign('escuela_id')->references('id')->on('cat_escuelas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_desayunos', function (Blueprint $table) {
            $table->dropForeign(['escuela_id']);
            $table->dropIndex('alim_desayunos_escuela_id_foreign');
        });

        Schema::table('alim_desayunos', function (Blueprint $table) {
            $table->foreign('escuela_id')->references('id')->on('alim_cat_escuelas');
        });
    }
}
