<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para uso de datos en la tabla cat_escuelas
  ALCANCE: General
*/

class AlterAdddelcolsToCatEscuelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_escuelas', function (Blueprint $table) {
            $table->dropColumn('tipo');
            $table->dropColumn('servicio');

            $table->unsignedInteger('nivel_id')->after('clave');
            $table->unsignedInteger('turno_id')->after('nivel_id');
            $table->unsignedInteger('padre_id')->nullable()->after('turno_id');
            $table->tinyInteger('extension')->after('padre_id');
            $table->string('latitud',50)->nullable()->after('localidad_id');
            $table->string('longitud',50)->nullable()->after('latitud');
            $table->tinyInteger('oficial')->unsigned()->after('longitud');
        });

        $registro = DB::table('cat_nivelesescuela')->first();
        if ($registro)
            DB::table('cat_escuelas')->update(['nivel_id'=>$registro->id]);

        $registro = DB::table('cat_turnosescuela')->first();
        if ($registro)
            DB::table('cat_escuelas')->update(['turno_id'=>$registro->id]);

        $registro = DB::table('alim_cat_escuelas')->first();
        $registro2 = DB::table('cat_escuelas')->first();
        if ($registro && !$registro2)
            DB::statement('INSERT INTO cat_escuelas (id, nombre, clave, nivel_id, turno_id, padre_id, extension, municipio_id, localidad_id, latitud, longitud, oficial, entidad_id, created_at, updated_at)
                        SELECT id, nombre, clave, nivel_id, turno, padre_id, extension, municipio_id, localidad_id, latitud, longitud, oficial, 20, created_at, created_at
                        FROM alim_cat_escuelas');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_escuelas', function (Blueprint $table) {
            $table->string('tipo')->after('nombre');
            $table->string('servicio')->nullable()->after('clave');

            $table->dropColumn('nivel_id');
            $table->dropColumn('turno_id');
            $table->dropColumn('padre_id');
            $table->dropColumn('extension');
            $table->dropColumn('latitud');
            $table->dropColumn('longitud');
            $table->dropColumn('oficial');
        });
    }
}
