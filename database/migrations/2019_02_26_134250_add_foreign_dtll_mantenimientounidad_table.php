<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDtllMantenimientounidadTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_mantenimientosunidad', function (Blueprint $table) {
      $table->foreign('unidad_id')->references('id')->on('unidades')->onUpdate('CASCADE')->onDelete('CASCADE');            
      $table->foreign('mantenimiento_id')->references('id')->on('dtll_cat_mantenimientos')->onUpdate('CASCADE')->onDelete('CASCADE');            
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_mantenimientosunidad', function (Blueprint $table) {
      $table->dropForeign(['unidad_id']);
      $table->dropForeign(['mantenimiento_id']);
      $table->dropIndex('dtll_mantenimientosunidad_unidad_id_foreign');
      $table->dropIndex('dtll_mantenimientosunidad_mantenimiento_id_foreign');
    });
  }
}
