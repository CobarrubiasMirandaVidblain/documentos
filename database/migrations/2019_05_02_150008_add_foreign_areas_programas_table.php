<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para ingresar datos de las nuevas áreas 2019
  ALCANCE: Para todos los sistemas
*/

class AddForeignAreasProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas_programas', function (Blueprint $table) {
            $table->foreign('area_id')->references('id')->on('cat_areas');
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas_programas', function (Blueprint $table) {
            $table->dropForeign(['area_id']);
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('areas_programas_area_id_foreign');
            $table->dropIndex('areas_programas_programa_id_foreign');
            $table->dropIndex('areas_programas_usuario_id_foreign');
        });
    }
}
