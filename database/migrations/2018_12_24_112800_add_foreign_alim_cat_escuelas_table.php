<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimCatEscuelasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_cat_escuelas', function (Blueprint $table) {
      $table->foreign('nivel_id')->references('id')->on('alim_cat_niveles');
      $table->foreign('padre_id')->references('id')->on('alim_cat_escuelas');
      $table->foreign('municipio_id')->references('id')->on('cat_municipios');
      $table->foreign('localidad_id')->references('id')->on('cat_localidades');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_cat_escuelas', function (Blueprint $table) {
      $table->dropForeign(['nivel_id']);
      $table->dropForeign(['padre_id']);
      $table->dropForeign(['municipio_id']);
      $table->dropForeign(['localidad_id']);
      $table->dropForeign(['usuario_id']);
      
      $table->dropIndex('alim_cat_escuelas_nivel_id_foreign');
      $table->dropIndex('alim_cat_escuelas_padre_id_foreign');
      $table->dropIndex('alim_cat_escuelas_municipio_id_foreign');
      $table->dropIndex('alim_cat_escuelas_localidad_id_foreign');
      $table->dropIndex('alim_cat_escuelas_usuario_id_foreign');
    });
  }
}
