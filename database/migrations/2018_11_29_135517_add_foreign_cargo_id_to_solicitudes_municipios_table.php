<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Captura de datos
  ALCANCE: Atención Ciudadana
*/

class AddForeignCargoIdToSolicitudesMunicipiosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('solicitudes_municipios', function (Blueprint $table) {
			$table->foreign('cargo_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('solicitudes_municipios', function (Blueprint $table) {
			$table->dropForeign(['cargo_id']);
      $table->dropIndex('solicitudes_municipios_cargo_id_foreign');
		});
	}
}
