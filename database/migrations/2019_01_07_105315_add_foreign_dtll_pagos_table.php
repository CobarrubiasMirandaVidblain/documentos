<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class AddForeignDtllPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_pagos', function (Blueprint $table) {
      $table->foreign('servicioadquirido_id')->references('id')->on('dtll_serviciosadquiridos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_pagos', function (Blueprint $table) {
      $table->dropForeign(['servicioadquirido_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('dtll_serviciosadquiridos_servicioadquirido_id_foreign');
      $table->dropIndex('dtll_serviciosadquiridos_usuario_id_foreign');
    });
  }
}
