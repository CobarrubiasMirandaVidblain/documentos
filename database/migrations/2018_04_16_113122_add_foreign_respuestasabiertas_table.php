<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRespuestasabiertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestasabiertas', function (Blueprint $table) {
            $table->foreign('preguntas_persona_id')->references('id')->on('preguntas_personas')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestasabiertas', function (Blueprint $table) {
            $table->dropForeign(["preguntas_persona_id"]);
             
        });
    }
}
