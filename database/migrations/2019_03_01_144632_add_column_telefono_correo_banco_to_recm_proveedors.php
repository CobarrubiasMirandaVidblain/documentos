<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agregan columna adicionales
  ALCANCE: para el sistema de requisiciones recmat
*/

class AddColumnTelefonoCorreoBancoToRecmProveedors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_cat_proveedors', function (Blueprint $table) {
            $table->string('telefono')->nullable()->after('clabe');
            $table->string('correo')->nullable()->after('clabe');
            $table->string('banco')->nullable()->after('clabe');
            $table->string('no_inscripcion')->nullable()->after('clabe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_cat_proveedors',function(Blueprint $table){
            $table->dropColumn('telefono');
            $table->dropColumn('correo');
            $table->dropColumn('banco');
            $table->dropColumn('no_inscripcion');
        });
    }
}
