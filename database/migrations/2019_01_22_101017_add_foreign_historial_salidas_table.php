<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignHistorialSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_historial_salidas', function (Blueprint $table) {
            $table->foreign('salidassolicitud_id')->references('id')->on('mopi_salidas_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('estatus_id')->references('id')->on('mopi_cat_estatus')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_historial_salidas', function (Blueprint $table) {
            $table->dropForeign(['salidassolicitud_id']);
            $table->dropForeign(['estatus_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('mopi_historial_salidas_salidassolicitud_id_foreign');
            $table->dropIndex('mopi_historial_salidas_usuario_id_foreign');
            $table->dropIndex('mopi_historial_salidas_estatus_id_foreign');
        });
    }
}
