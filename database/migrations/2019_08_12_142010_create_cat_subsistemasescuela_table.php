<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para registar el subsistema de una escuela, por ejemplo si es cobao, cecyte, telesecundaria, etc.
    ALCANCE: General
*/

class CreateCatSubsistemasescuelaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_subsistemasescuela', function (Blueprint $table) {
            $table->increments('id');
						$table->string('nombre');
            $table->timestamps();
						$table->softDeletes();

						$table->unique(['nombre']);
        });

				DB::table('cat_subsistemasescuela')->insert(['nombre'=>'NO DEFINIDO', 'created_at'=>now(), 'updated_at'=>now()]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_subsistemasescuela');
    }
}
