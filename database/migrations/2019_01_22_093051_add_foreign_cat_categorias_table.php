<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignCatCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_cat_categorias', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('mopi_cat_progamas')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_cat_categorias', function (Blueprint $table) {
            $table->dropForeign(['programa_id']);
            $table->dropIndex('mopi_cat_categorias_programa_id_foreign');
        });
    }
}
