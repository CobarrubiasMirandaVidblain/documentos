<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: se cambia el tipo de dato para que puedan guardar decimales
  ALCANCE: modulo de monte de piedad
*/

class ChangeCantidadToMopisolicitudesProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_solicitudes_productos', function (Blueprint $table) {
            $table->decimal('cantidad', 10, 2)->change();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_solicitudes_productos', function (Blueprint $table) {
            $table->integer('cantidad')->change();
        });
    }
}
