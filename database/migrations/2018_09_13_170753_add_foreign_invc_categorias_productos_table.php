<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignInvcCategoriasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invc_categorias_productos', function(Blueprint $table){
            $table->foreign('producto_id')->references('id')->on('cat_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('categoria_id')->references('id')->on('cat_categorias')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invc_categorias_productos', function(Blueprint $table){
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['categoria_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
