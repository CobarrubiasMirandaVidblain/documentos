<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrevEventosParticipantesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('orev_eventos_participantes', function (Blueprint $table) {
      $table->increments('id');
      
      $table->integer('evento_id')->unsigned();
      $table->integer('participante_id')->unsigned();
      
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('orev_eventos_participantes');
  }
}
