<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllCatTipospasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dtll_cat_tipospasajeros', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->timestamps();
        $table->softDeletes();
      });

      DB::table('dtll_cat_tipospasajeros')->insert([
        ['nombre' => 'BENEFICIARIO', 'created_at'=>NOW()],
        ['nombre' => 'ACOMPAÑANTE', 'created_at'=>NOW()]
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_cat_tipospasajeros');
    }
}
