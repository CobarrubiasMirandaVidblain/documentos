<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCtliAreasUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ctli_areas_usuarios', function(Blueprint $table){
            $table->foreign('area_id')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ctli_areas_usuarios', function(Blueprint $table){
            $table->dropForeign(['area_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
