<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: 
    ALCANCE: 
*/

class AddColumnBeneficioprogramaIdDropColumnAreasProductoIdToDetallessalidasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('detallessalidas_productos', function (Blueprint $table) {
				$table->unsignedInteger('beneficioprograma_id')->after('salidas_producto_id');
				$table->foreign('beneficioprograma_id')->references('id')->on('beneficiosprogramas');
			});

			Schema::table('detallessalidas_productos', function (Blueprint $table) {
					$table->dropForeign(['areas_producto_id']);
					$table->dropIndex('detallessalidas_productos_areas_producto_id_foreign');
					$table->dropColumn('areas_producto_id');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detallessalidas_productos', function (Blueprint $table) {
					$table->unsignedInteger('areas_producto_id')->after('cantidad');
          $table->foreign('areas_producto_id')->references('id')->on('areas_productos');
        });

				Schema::table('detallessalidas_productos', function (Blueprint $table) {
					$table->dropForeign(['beneficioprograma_id']);
					$table->dropIndex('detallessalidas_productos_beneficioprograma_id_foreign');
					$table->dropColumn('beneficioprograma_id');
			});
    }
}
