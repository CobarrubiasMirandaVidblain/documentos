<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para ingresar datos de contrarecibo de pagos en caja por conceptos de alimentarios
    ALCANCE: Alimentarios
*/

class AddColumnsTotalDotacionesTextoPagoRenameColumnCantidadToImporteTotalToAlimRecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->decimal('importe_total', 12, 2)->nullable()->after('fecha_pago');
            $table->smallInteger('total_dotaciones')->nullable()->after('importe_total');
            $table->text('texto_pago')->nullable()->after('total_dotaciones');
        });

        DB::raw('UPDATE alim_recibos SET importe_total=cantidad');
        
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->dropColumn('cantidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->decimal('cantidad', 12, 2)->nullable()->after('folio_caja');
        });

        DB::raw('UPDATE alim_recibos SET cantidad=importe_total');

        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->dropColumn('importe_total');
            $table->dropColumn('total_dotaciones');
            $table->dropColumn('texto_pago');
        });
    }
}
