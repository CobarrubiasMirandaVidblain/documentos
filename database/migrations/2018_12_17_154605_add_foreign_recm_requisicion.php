<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmRequisicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->unsignedInteger('estatus_id');
            $table->foreign('estatus_id')->references('id')->on('recm_cat_estatuses')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->dropForeign(['estatus_id']);
            $table->dropIndex('recm_requisicions_estatus_id_foreign');
        });
    }
}
