<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmRequisicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_requisicions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->string('justificacion');
            $table->date('fecha_captura');
            $table->date('fecha_plazo');
            $table->unsignedInteger('area_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_requisicions');
    }
}
