<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony.
  MOTIVO: Para relacionar indicar el estado en el que está una solicitud
  ALCANCE: Atención ciudadana y Apoyos Funcionales.
*/

class AddForeignEstadosSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('estados_solicitudes', function (Blueprint $table) {
            $table->foreign('beneficioprograma_solicitud_id')->references('id')->on('beneficiosprogramas_solicitudes');
            $table->foreign('statusproceso_id')->references('id')->on('cat_statusprocesos');
            $table->foreign('motivo_programa_id')->references('id')->on('motivos_programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estados_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['beneficioprograma_solicitud_id']);
            $table->dropForeign(['statusproceso_id']);
            $table->dropForeign(['motivo_programa_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('estados_solicitudes_beneficioprograma_solicitud_id_foreign');
            $table->dropIndex('estados_solicitudes_statusproceso_id_foreign');
            $table->dropIndex('estados_solicitudes_motivo_programa_id_foreign');
            $table->dropIndex('estados_solicitudes_usuario_id_foreign');
        });
    }
}
