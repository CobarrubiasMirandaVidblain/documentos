<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignSolicitudesProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_solicitudes_productos', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('mopi_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_producto_id')->references('id')->on('mopi_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('estatus_id')->references('id')->on('mopi_cat_estatus')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_solicitudes_productos', function (Blueprint $table) {
            $table->dropForeign(['solicitud_id']);
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropForeign(['estatus_id']);
            $table->dropIndex('mopi_solicitudes_productos_presentacion_producto_id_foreign');
            $table->dropIndex('mopi_solicitudes_productos_solicitud_id_foreign');
            $table->dropIndex('mopi_solicitudes_productos_estatus_id_foreign');
        });
    }
}
