<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para poder almacenar datos referentes a alimentarios
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AlterDeladdcolEstadoIdForEstadoProgramaIdToTablasalimTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    /* ************** PARA LA TABLA alim_programacion ************** */
    Schema::Table('alim_programacion', function(Blueprint $table){
      $table->dropForeign(['estado_id']);
      $table->dropIndex('alim_programacion_estado_id_foreign');
      $table->dropColumn('estado_id');

      $table->unsignedInteger('estado_programa_id')->after('anios_programa_id');
    });
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_programacion', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_programacion')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_programacion SET estado_programa_id=(SELECT MIN(id) FROM estados_programas)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('estado_programa_id')->references('id')->on('estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
    
    /* ************** PARA LA TABLA alim_alimentarios_requisiciones ************** */
    Schema::Table('alim_alimentarios_requisiciones', function(Blueprint $table){
      $table->dropForeign(['estado_id']);
      $table->dropIndex('alim_alimentarios_requisiciones_estado_id_foreign');
      $table->dropColumn('estado_id');

      $table->unsignedInteger('estado_programa_id')->after('inversion');
    });
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_alimentarios_requisiciones', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_alimentarios_requisiciones')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_alimentarios_requisiciones SET estado_programa_id=(SELECT MIN(id) FROM estados_programas)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('estado_programa_id')->references('id')->on('estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
    
    /* ************** PARA LA TABLA alim_requisiciones ************** */
    Schema::Table('alim_requisiciones', function(Blueprint $table){
      $table->dropForeign(['estado_id']);
      $table->dropIndex('alim_requisiciones_estado_id_foreign');
      $table->dropColumn('estado_id');

      $table->unsignedInteger('estado_programa_id')->after('entrega');
    });
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_requisiciones', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_requisiciones')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_requisiciones SET estado_programa_id=(SELECT MIN(id) FROM estados_programas)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('estado_programa_id')->references('id')->on('estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    /* ************** PARA LA TABLA alim_programacion ************** */
    Schema::Table('alim_programacion', function(Blueprint $table){
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('alim_programacion_estado_programa_id_foreign');
      $table->dropColumn('estado_programa_id');

      $table->unsignedInteger('estado_id')->after('anios_programa_id');
    });
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_programacion', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_programacion')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_programacion SET estado_id=(SELECT MIN(id) FROM alim_cat_estados)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('estado_id')->references('id')->on('alim_cat_estados')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
    
    /* ************** PARA LA TABLA alim_alimentarios_requisiciones ************** */
    Schema::Table('alim_alimentarios_requisiciones', function(Blueprint $table){
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('alim_alimentarios_requisiciones_estado_programa_id_foreign');
      $table->dropColumn('estado_programa_id');

      $table->unsignedInteger('estado_id')->after('inversion');
    });
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_alimentarios_requisiciones', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_alimentarios_requisiciones')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_alimentarios_requisiciones SET estado_id=(SELECT MIN(id) FROM alim_cat_estados)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('estado_id')->references('id')->on('alim_cat_estados')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
    
    /* ************** PARA LA TABLA alim_requisiciones ************** */
    Schema::Table('alim_requisiciones', function(Blueprint $table){
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('alim_requisiciones_estado_programa_id_foreign');
      $table->dropColumn('estado_programa_id');

      $table->unsignedInteger('estado_id')->after('entrega');
    });
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_requisiciones', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_requisiciones')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_requisiciones SET estado_id=(SELECT MIN(id) FROM alim_cat_estados)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('estado_id')->references('id')->on('alim_cat_estados')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
