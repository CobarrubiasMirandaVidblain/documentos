<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para guaradar los datos de las comprobaciones
    ALCANCE: fondorotatorio
*/

class CreateSiafComprobacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_comprobaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio',100);
            $table->unsignedInteger('fondorotatorio_id');
            $table->unsignedInteger('tipo_asignacion_id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('estado_modulo_id');
            $table->float('monto_total');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_comprobaciones');
    }
}
