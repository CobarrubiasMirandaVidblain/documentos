<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAtncEventosSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('atnc_eventos_solicitudes', function(Blueprint $table){
        $table->foreign('evento_id')->references('id')->on('atnc_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('solicitud_id')->references('id')->on('solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('atnc_eventos_solicitudes', function(Blueprint $table){
        $table->dropForeign(['evento_id']);
        $table->dropForeign(['solicitud_id']);
        $table->dropForeign(['usuario_id']);
      });
    }
}
