<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->nullable();
            $table->string('nombre');
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nullable();
            $table->string('calle')->nullable();
            $table->string('numero_exterior');
            $table->string('numero_interior')->nullable();
            $table->string('colonia')->nullable();
            $table->string('codigopostal')->nullable();
            $table->integer('localidad_id')->unsigned()->nullable();
            $table->string('curp')->unique()->nullable();
            $table->char('genero',1)->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('clave_electoral')->unique()->nullable();
            $table->string('numero_celular')->nullable();
            $table->string('numero_local')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('referencia_domicilio')->nullable();
            $table->string('curpo')->unique()->nullable();
             $table->string('fotografia')->nullable();
            $table->string('firma')->nullable();
            $table->string('huella')->nullable();
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
