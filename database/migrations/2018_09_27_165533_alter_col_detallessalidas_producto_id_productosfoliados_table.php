<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColDetallessalidasProductoIdProductosfoliadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('productosfoliados', function(Blueprint $table){
        $table->integer('detallessalidas_producto_id')->unsigned()->nullable()->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('productosfoliados', function(Blueprint $table){
        $table->integer('detallessalidas_producto_id')->unsigned()->change();
      });
    }
}
