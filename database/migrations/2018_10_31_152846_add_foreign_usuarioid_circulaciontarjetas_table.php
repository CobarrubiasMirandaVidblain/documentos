<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian 
  MOTIVO: Se presentó en la necesidad en el desarrollo del sistema DIF te lleva
  ALCANCE: Para todos
*/

class AddForeignUsuarioidCirculaciontarjetasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('circulaciontarjetas_usuario_id_foreign');
    });
  }
}
