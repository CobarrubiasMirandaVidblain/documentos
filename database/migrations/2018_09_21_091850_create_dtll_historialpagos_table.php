<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllHistorialpagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dtll_historialpagos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pasajero_id');
            $table->date('fechaInicio');
            $table->date('fechaFin');
            $table->string('folioPago',7);
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_historialpagos');
    }
}
