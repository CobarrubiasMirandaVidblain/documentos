<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimValidacionesentregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_validacionesentregas', function (Blueprint $table) {
            $table->foreign('entregaproveedor_id')->references('id')->on('alim_entregasproveedor');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_validacionesentregas', function (Blueprint $table) {
            $table->dropForeign(['entregaproveedor_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_validacionesentregas_entregaproveedor_id_foreign');
            $table->dropIndex('alim_validacionesentregas_usuario_id_foreign');
        });
    }
}
