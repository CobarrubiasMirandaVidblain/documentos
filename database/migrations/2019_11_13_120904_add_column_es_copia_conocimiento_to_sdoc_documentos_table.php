<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para indicar si un tipo de documento es para copia de conocimiento
    ALCANCE: SICODOC
*/

class AddColumnEsCopiaConocimientoToSdocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_documentos', function (Blueprint $table) {
            $table->boolean('es_copia_conocimiento')->after('folio')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_documentos', function (Blueprint $table) {
            $table->dropColumn('es_copia_conocimiento');
        });
    }
}
