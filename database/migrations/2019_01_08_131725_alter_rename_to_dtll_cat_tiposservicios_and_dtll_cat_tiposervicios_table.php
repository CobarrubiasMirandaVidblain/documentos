<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class AlterRenameToDtllCatTiposserviciosAndDtllCatTiposerviciosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::dropIfExists('dtll_regservspasajeros');
    Schema::rename('dtll_cat_tiposervicios','dtll_cat_tiposservicio');
    Schema::rename('dtll_cat_tipospasajeros','dtll_cat_tipospasajero');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::rename('dtll_cat_tiposervicio','dtll_cat_tiposervicios');
    Schema::rename('dtll_cat_tipospasajero','dtll_cat_tipospasajeros');

    Schema::create('dtll_regservspasajeros', function (Blueprint $table) {
      $table->increments('id');
      $table->tinyInteger('acompanante');
      $table->string('clave',7);
      $table->integer('pasajero_id')->unsigned();
      $table->datetime('fecha');
      $table->integer('conductor_id')->unsigned();
      $table->integer('tiposervicio_id')->unsigned();
      $table->integer('usuario_id')->unsigned();
      $table->timestamps();
      $table->softDeletes();

      $table->foreign('pasajero_id')->references('id')->on('dtll_pasajeros')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('tiposervicio_id')->references('id')->on('dtll_cat_tiposervicios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('conductor_id')->references('id')->on('conductores')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
