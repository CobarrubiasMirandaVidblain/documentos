<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
     * PETICIÓN: Beto.
     * MOTIVO: Tener el catalogo de estatus de Pagos
     * ALCANCE: Fondo Rotatorio
*/

class ChangeSiafEstadosPagoTable extends Migration
{
    /**
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_estados_pagos', function (Blueprint $table) {
            $table->dropForeign(['estado_programa_id']);
            $table->dropIndex('siaf_estados_pagos_estado_programa_id_foreign');
            $table->foreign('estado_programa_id')->references('id')->on('estados_modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_estados_pagos', function (Blueprint $table) {
            $table->dropForeign(['estado_programa_id']);
            $table->dropIndex('siaf_estados_pagos_estado_programa_id_foreign');
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
        });
    }
}
