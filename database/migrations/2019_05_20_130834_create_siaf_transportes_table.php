<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yo (beto)
  MOTIVO: Faltan columnas en las tablas de viaticos
  ALCANCE: Módulo de BancaDIF
*/

class CreateSiafTransportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_transportes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tipotransporte_id');
            $table->string('placas', 20);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_transportes');
    }
}
