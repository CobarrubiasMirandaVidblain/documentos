<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignEventoidGiraspregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('giraspreregis', function(Blueprint $table){
        $table->foreign('evento_id')->references('id')->on('atnc_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('giraspreregis', function(Blueprint $table){
        $table->dropForeign(['evento_id']);
      });
    }
}
