<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiafViaticoestatalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_viaticoestatal', function (Blueprint $table) {
            $table->unsignedInteger('viatico_id')->unique();
            $table->unsignedInteger('origenmunicipio_id');
            $table->unsignedInteger('destinomunicipio_id');
            $table->unsignedInteger('destinolocalidad_id');
            $table->string('localidad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_viaticoestatal');
    }
}
