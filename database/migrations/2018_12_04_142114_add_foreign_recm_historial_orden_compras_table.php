<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmHistorialOrdenComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_historial_orden_compras
        Schema::table('recm_historial_orden_compras', function (Blueprint $table) {
            $table->foreign('orden_compra_id')->references('id')->on('recm_ordenescompra')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('status_id')->references('id')->on('recm_cat_estatuses')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_historial_orden_compras', function (Blueprint $table) {
            $table->dropForeign(['orden_compra_id']);
            $table->dropForeign(['status_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_historial_orden_compras_usuario_id_foreign');
            $table->dropIndex('recm_historial_orden_compras_status_id_foreign');
            $table->dropIndex('recm_historial_orden_compras_orden_compra_id_foreign');
            
        });
    }
}
