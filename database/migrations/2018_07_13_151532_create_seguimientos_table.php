<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha')->nullable();
            $table->text('observaciones')->nullable();

            $table->unsignedInteger('id_oficio');
            $table->unsignedInteger('id_estatusseguimiento');

            $table->foreign('id_oficio')->references('id')->on('oficios');
            $table->foreign('id_estatusseguimiento')->references('id')->on('cat_estatusseguimientos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguimientos');
    }
}
