<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignNotasclinicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notasclinicas', function (Blueprint $table) {
            $table->foreign('persona_programa_id')->references('id')->on('personas_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('curso_id')->references('id')->on('cat_cursos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notasclinicas', function (Blueprint $table) {
            $table->dropForeign(["persona_programa_id"]);
            $table->dropForeign(["usuario_id"]);
            $table->dropForeign(["curso_id"]);
        });
    }
}
