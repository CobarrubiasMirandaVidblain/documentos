<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRecmCatProvedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_cat_proveedors', function (Blueprint $table) {
            $table->string('codigopostal', 5)->nullable()->after('colonia');
            $table->string('ciudad')->nullable()->after('codigopostal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_cat_proveedors',function(Blueprint $table){
            $table->dropColumn('codigopostal');
            $table->dropColumn('ciudad');
        });
    }
}
