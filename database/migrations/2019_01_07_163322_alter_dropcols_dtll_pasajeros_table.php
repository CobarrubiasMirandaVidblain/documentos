<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class AlterDropcolsDtllPasajerosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_pasajeros', function (Blueprint $table) {
      $table->dropForeign(['tiposervicio_id']);
      $table->dropForeign(['tipopasajero_id']);
      $table->dropForeign(['lugarPago_id']);
      $table->dropIndex('dtll_pasajeros_tiposervicio_id_foreign');
      $table->dropIndex('dtll_pasajeros_tipopasajero_id_foreign');
      $table->dropIndex('dtll_pasajeros_lugarPago_id_foreign');


      $table->dropColumn('tiposervicio_id');
      $table->dropColumn('tipopasajero_id');
      $table->dropColumn('fechaIniServ');
      $table->dropColumn('fechaFinServ');
      $table->dropColumn('timestampFinServ');
      $table->dropColumn('numMesesServ');
      $table->dropColumn('folioPago');
      $table->dropColumn('lugarPago_id');
      $table->dropColumn('numPasajero');
      $table->dropColumn('numPasajeroAmbos');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_pasajeros', function (Blueprint $table) {
        $table->integer('tiposervicio_id')->unsigned()->after('id');
        $table->integer('tipopasajero_id')->unsigned()->after('tiposervicio_id');
        $table->date('fechaIniServ')->after('tipopasajero_id');
        $table->date('fechaFinServ')->after('fechaIniServ');
        $table->integer('timestampFinServ')->after('fechaFinServ');
        //$table->index:"B0009"
        $table->tinyInteger('numMesesServ')->unsigned()->after('timestampFinServ');
        $table->string('folioPago',15)->after('numMesesServ');
        $table->integer('lugarPago_id')->unsigned()->after('folioPago');
        $table->string('numPasajero',7)->after('lugarPago_id');
        $table->string('numPasajeroAmbos',7)->nullable()->after('numPasajero');

        $table->foreign('tiposervicio_id')->references('id')->on('dtll_cat_tiposervicios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('tipopasajero_id')->references('id')->on('dtll_cat_tipospasajeros')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('lugarPago_id')->references('id')->on('dtll_cat_lugarespago')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
