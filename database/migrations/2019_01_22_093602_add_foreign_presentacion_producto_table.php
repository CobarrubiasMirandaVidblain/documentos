<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignPresentacionProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_presentacion_productos', function (Blueprint $table) {
            $table->foreign('producto_id')->references('id')->on('mopi_cat_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_id')->references('id')->on('mopi_cat_presentaciones')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('categoria_id')->references('id')->on('mopi_cat_categorias')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_presentacion_productos', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['presentacion_id']);
            $table->dropForeign(['categoria_id']);
            $table->dropIndex('mopi_presentacion_productos_producto_id_foreign');
            $table->dropIndex('mopi_presentacion_productos_presentacion_id_foreign');
            $table->dropIndex('mopi_presentacion_productos_categoria_id_foreign');
        });
    }
}
