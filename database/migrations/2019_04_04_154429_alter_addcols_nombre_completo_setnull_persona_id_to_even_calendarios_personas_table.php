<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolsNombreCompletoSetnullPersonaIdToEvenCalendariosPersonasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    //even_calendarios_personas
    Schema::table('even_calendarios_personas', function (Blueprint $table) {
      $table->unsignedInteger('persona_id')->nullable()->change();
      $table->string('nombre')->nullable()->after('persona_id');
      $table->string('primer_apellido')->nullable()->after('nombre');
      $table->string('segundo_apellido')->nullable()->after('primer_apellido');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_calendarios_personas', function (Blueprint $table) {
      $table->unsignedInteger('persona_id')->change();
      $table->dropColumn(['nombre','primer_apellido','segundo_apellido']);
    });
  }
}
