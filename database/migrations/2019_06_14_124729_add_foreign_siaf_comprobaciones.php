<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: 
    ALCANCE: fondorotatorio
*/

class AddForeignSiafComprobaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->foreign('fondorotatorio_id')->references('id')->on('siaf_fondorotatorioareas');
            $table->foreign('tipo_asignacion_id')->references('id')->on('siaf_cat_tipos_asignaciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('estado_modulo_id')->references('id')->on('estados_modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->dropForeign(['fondorotatorio_id']);
            $table->dropForeign(['tipo_asignacion_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['estado_modulo_id']);

            $table->dropIndex('siaf_comprobaciones_fondorotatorio_id_foreign');
            $table->dropIndex('siaf_comprobaciones_tipo_asignacion_id_foreign');
            $table->dropIndex('siaf_comprobaciones_usuario_id_foreign');
            $table->dropIndex('siaf_comprobaciones_estado_modulo_id_foreign');
        });
    }
}
