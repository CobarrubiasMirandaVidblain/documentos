<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO:
  ALCANCE:
*/

class DeleteColumnOrevEventosParticipantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Table('orev_eventos_participantes', function(Blueprint $table){
            $table->dropForeign(['participante_id']);
            $table->dropIndex('orev_eventos_participantes_participante_id_foreign');
            $table->dropColumn('participante_id');
        });

        Schema::Table('orev_eventos_participantes', function(Blueprint $table){
            $table->integer('persona_id')->unsigned()->after('evento_id');
            $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orev_eventos_participantes', function (Blueprint $table) {
            $table->integer('participante_id')->unsigned()->after('evento_id');
            $table->foreign('participante_id')->references('id')->on('orev_participantes')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::Table('orev_eventos_participantes', function(Blueprint $table){
            $table->dropForeign(['persona_id']);
            $table->dropIndex('orev_eventos_participantes_persona_id_foreign');
            $table->dropColumn('persona_id');
        });
    }
}
