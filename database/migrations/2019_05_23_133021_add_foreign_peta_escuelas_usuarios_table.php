<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class AddForeignPetaEscuelasUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peta_escuelas_usuarios', function (Blueprint $table) {
            $table->foreign('institucion_usuario_id')->references('id')->on('peta_instituciones_usuarios');
            $table->foreign('escuela_id')->references('id')->on('cat_escuelas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peta_escuelas_usuarios', function (Blueprint $table) {
            $table->dropForeign(['institucion_usuario_id']);    // Ya no genra index por el unique
            $table->dropForeign(['escuela_id']);
            $table->dropForeign(['usuario_id']);
            
            $table->dropIndex('peta_escuelas_usuarios_escuela_id_foreign');
            $table->dropIndex('peta_escuelas_usuarios_usuario_id_foreign');
        });
    }
}
