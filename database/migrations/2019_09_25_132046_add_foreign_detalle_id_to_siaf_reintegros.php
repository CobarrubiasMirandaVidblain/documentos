<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Llaves foráneas para indicar el detallepago_id al que hace referencia el reintegro
    ALCANCE: SIAF
*/

class AddForeignDetalleIdToSiafReintegros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('siaf_reintegros', function (Blueprint $table) {
				$table->foreign('detalle_id')->references('id')->on('siaf_detallespago');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('siaf_reintegros', function (Blueprint $table) {
				$table->dropForeign(['detalle_id']);
				$table->dropIndex('siaf_reintegros_detalle_id_foreign');
			});
    }
}
