<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_requisiciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('oficio_requisicion');
            $table->timestamp('fecha_oficio');
            $table->string('titulo',50);
            $table->tinyInteger('bimestre')->unsigned();
            $table->tinyInteger('entrega')->unsigned();
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('tiporequisicion_id');
            $table->unsignedInteger('licitacion_id');
            $table->string('observacion')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['oficio_requisicion', 'deleted_at']);
            $table->unique(['oficio_requisicion', 'bimestre', 'entrega', 'deleted_at'],'alim_requisiciones_ofireq_bim_entrega_del_at_unique'); // Especificamos un nombre porque eloquent crea un nombre muy largo y no lo acepta mysql
        });

        // DB::statement("ALTER TABLE alim_requisiciones ADD UNIQUE alim_requisiciones_ofireq_bim_entrega_del_at_unique (oficio_requisicion, bimestre, entrega, deleted_at)");  // Agregamos manualmente un unique, porque poreloquent crea un nombre muy largo y no lo acepta mysql
        
                        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_requisiciones');
    }
}
