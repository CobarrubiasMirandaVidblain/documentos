<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para ingresar datos de las nuevas áreas 2019
  ALCANCE: Para todos los sistemas
*/

class AlterAddColAniosProgramaIdDelcolProgramaIdToBeneficiosprogramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    //Borramos columna programa_id
    Schema::table('beneficiosprogramas', function (Blueprint $table) {
      $table->dropForeign(['programa_id']);
      $table->dropIndex('beneficiosprogramas_programa_id_foreign');
    });
    Schema::table('beneficiosprogramas',function(Blueprint $table){
      $table->dropColumn('programa_id');
    });

    // Agregamos columna anio_programa_id
    Schema::table('beneficiosprogramas', function (Blueprint $table) {
      $table->unsignedInteger('anio_programa_id')->after('id');
    });
    $registro = DB::table('anios_programas')->first();
    if ($registro)
      DB::table('beneficiosprogramas')->update(['anio_programa_id'=>$registro->id]);
    

    //Agregamos columna predeterminado
    Schema::table('beneficiosprogramas',function(Blueprint $table){
      $table->tinyInteger('predeterminado')->unsigned()->after('nombre');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //Borramos predeterminado
    Schema::table('beneficiosprogramas',function(Blueprint $table){
      $table->dropColumn('predeterminado');
    });

    // Agregamos columna programa_id
    Schema::table('beneficiosprogramas', function (Blueprint $table) {
      $table->unsignedInteger('programa_id')->after('id');
    });
    $registro = DB::table('beneficiosprogramas')->first();
    if ($registro)
      DB::table('beneficiosprogramas')->update(['programa_id'=>$registro->id]);
    Schema::table('beneficiosprogramas', function (Blueprint $table) {
      $table->foreign('programa_id')->references('id')->on('programas');
    });

    //Borramos columna anio_programa_id
    Schema::table('beneficiosprogramas',function(Blueprint $table){
      $table->dropColumn('anio_programa_id');
    });
  }
}
