<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimDetallesdotacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_detallesdotacion', function (Blueprint $table) {
            $table->foreign('costo_producto_id')->references('id')->on('alim_costos_productos');
            $table->foreign('dotacion_id')->references('id')->on('alim_dotaciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_detallesdotacion', function (Blueprint $table) {
            $table->dropForeign(['costo_producto_id']);
            $table->dropForeign(['dotacion_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_detallesdotacion_costo_producto_id_foreign');
            $table->dropIndex('alim_detallesdotacion_dotacion_id_foreign');
            $table->dropIndex('alim_detallesdotacion_usuario_id_foreign');
        });
    }
}
