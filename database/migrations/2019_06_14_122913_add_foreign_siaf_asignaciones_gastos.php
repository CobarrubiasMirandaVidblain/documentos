<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: 
    ALCANCE: fondorotatorio
*/

class AddForeignSiafAsignacionesGastos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_asignaciones_gastos', function (Blueprint $table) {
            $table->foreign('tipo_pago_id')->references('id')->on('siaf_cat_tipospago');
            $table->foreign('asignacion_id')->references('id')->on('siaf_asignaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_asignaciones_gastos', function (Blueprint $table) {
            $table->dropForeign(['tipo_pago_id']);
            $table->dropForeign(['asignacion_id']);

            $table->dropIndex('siaf_asignaciones_gastos_tipo_pago_id_foreign');
            $table->dropIndex('siaf_asignaciones_gastos_asignacion_id_foreign');
        });
    }
}
