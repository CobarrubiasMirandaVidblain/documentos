<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para uso de datos en la tabla cat_escuelas
  ALCANCE: General
*/

class AddForeignNivelIdTurnoIdPadreIdToCatEscuelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_escuelas', function (Blueprint $table) {
            $table->foreign('nivel_id')->references('id')->on('cat_nivelesescuela'); 
            $table->foreign('turno_id')->references('id')->on('cat_turnosescuela');
            $table->foreign('padre_id')->references('id')->on('cat_escuelas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_escuelas', function (Blueprint $table) {
            $table->dropForeign(['nivel_id']);
            $table->dropForeign(['turno_id']);
            $table->dropForeign(['padre_id']);
            
            $table->dropIndex('cat_escuelas_nivel_id_foreign');
            $table->dropIndex('cat_escuelas_turno_id_foreign');
            $table->dropIndex('cat_escuelas_padre_id_foreign');
        });
    }
}
