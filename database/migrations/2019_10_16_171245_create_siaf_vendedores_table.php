<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

/*
    PETICIÓN: Beto.
    MOTIVO: Tabla para guardar los proveedores del siaf.
    ALCANCE: SIAF
*/

class CreateSiafVendedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('db:refactor', [
            '--class' => 'ArchivosBackupTableRefactor',
        ]);
        
        Schema::create('siaf_cat_proveedores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rfc', 13);
            $table->string('nombre')->nullable();
            $table->string('calle')->nullable();
            $table->string('numero_exterior', 10)->nullable();
            $table->string('numero_interior', 10)->nullable();
            $table->string('colonia')->nullable();
            $table->unsignedInteger('localidad_id')->nullable();
            $table->unsignedInteger('municipio_id')->nullable();
            $table->string('codigo_postal', 5)->nullable();
            $table->string('email')->nullable();
            $table->enum('tipo', ['PROVEEDOR', 'EMPLEADO']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_cat_proveedores');
    }
}
