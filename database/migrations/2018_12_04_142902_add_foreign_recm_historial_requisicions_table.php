<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmHistorialRequisicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_historial_requisicions
        Schema::table('recm_historial_requisicions', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('recm_requisicions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('status_id')->references('id')->on('recm_cat_estatuses')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_historial_requisicions', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['status_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_historial_requisicions_usuario_id_foreign');
            $table->dropIndex('recm_historial_requisicions_status_id_foreign');
            $table->dropIndex('recm_historial_requisicions_requisicion_id_foreign');
        });
    }
}
