<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: 
    ALCANCE: fondorotatorio
*/

class AddForeignSiafAsignacionesPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_asignaciones_pagos', function (Blueprint $table) {
            $table->foreign('asignacion_id')->references('id')->on('siaf_asignaciones');
            $table->foreign('pago_id')->references('id')->on('siaf_pagos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_asignaciones_pagos', function (Blueprint $table) {
            $table->dropForeign(['asignacion_id']);
            $table->dropForeign(['pago_id']);

            $table->dropIndex('siaf_asignaciones_pagos_asignacion_id_foreign');
            $table->dropIndex('siaf_asignaciones_pagos_pago_id_foreign');
        });
    }
}
