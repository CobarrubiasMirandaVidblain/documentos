<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignConsultaPsicologicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('consulta_psicologica', function (Blueprint $table){
        $table->foreign('consulta_id')->references('id')->on('consultas')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }
  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('consulta_psicologica', function (Blueprint $table) {
        $table->dropForeign(["consulta_id"]);
      });
    }
}
