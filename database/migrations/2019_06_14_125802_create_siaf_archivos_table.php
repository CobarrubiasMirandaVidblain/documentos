<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
    PETICIÓN: Manuel.
    MOTIVO: para guardar los archivos para la comprobacion del gasto
    ALCANCE: fondorotatorio
*/

class CreateSiafArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_archivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('nombre',150);
            $table->string('mime',100);
            $table->float('size');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_archivos');
    }
}
