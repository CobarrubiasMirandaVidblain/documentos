<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de licitaciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimLicitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_licitaciones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('ejercicio_id');
            $table->unsignedInteger('proveedor_id');
            $table->string('num_licitacion',30);

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['ejercicio_id','proveedor_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_licitaciones');
    }
}
