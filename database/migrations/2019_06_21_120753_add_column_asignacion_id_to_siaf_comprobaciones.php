<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAsignacionIdToSiafComprobaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->unsignedInteger('asignacion_id')->after('monto_total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->dropColumn('asignacion_id');
        });
    }
}
