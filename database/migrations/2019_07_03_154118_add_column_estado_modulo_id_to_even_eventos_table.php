<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para indicar el estado de la ficha
    ALCANCE: Eventos
*/

class AddColumnEstadoModuloIdToEvenEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Obetenemos el id del modulo
        $cat_modulo = DB::table('cat_modulos')->where('nombre','AGENDA DE EVENTOS')->first();
        $even_eventos = DB::table('even_eventos')->first();
        if ($even_eventos){
            $cat_estados = DB::table('cat_estados')->where('nombre','EN PROCESO')->first();
            if (!$cat_estados) {
                // Obtenemos los id de estados
                $nueva = DB::table('cat_estados')->where('nombre','NUEVA')->first();
                if (!$nueva)
                    $nueva = DB::table('cat_estados')->insertGetId(
                        ['nombre'=>'NUEVA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
                    );
                else $nueva = $nueva->id;

                $en_proceso = DB::table('cat_estados')->where('nombre','EN PROCESO')->first();
                if (!$en_proceso)
                    $en_proceso = DB::table('cat_estados')->insertGetId(
                        ['nombre'=>'EN PROCESO', 'created_at'=>NOW(), 'updated_at'=>NOW()]
                    );
                else
                    $en_proceso=$en_proceso->id;
                
                $finalizada = DB::table('cat_estados')->where('nombre','FINALIZADA')->first();
                if (!$finalizada)
                    $finalizada = DB::table('cat_estados')->insertGetId(
                        ['nombre'=>'FINALIZADA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
                    );
                else
                    $finalizada = $finalizada->id;

                $estados_modulos = [
                    [ 'estado_id'=>$nueva, 'modulo_id'=>$cat_modulo->id, 'orden'=>1, 'usuario_id'=>91, 'created_at'=>NOW(), 'updated_at'=>NOW() ],
                    [ 'estado_id'=>$en_proceso, 'modulo_id'=>$cat_modulo->id, 'orden'=>2, 'usuario_id'=>91, 'created_at'=>NOW(), 'updated_at'=>NOW() ],
                    [ 'estado_id'=>$finalizada, 'modulo_id'=>$cat_modulo->id, 'orden'=>3, 'usuario_id'=>91, 'created_at'=>NOW(), 'updated_at'=>NOW() ]
                ];

                DB::table('estados_modulos')->insert($estados_modulos);
            }
        }

        Schema::table('even_eventos', function (Blueprint $table) {
            $table->unsignedInteger('estado_modulo_id')->after('tipoevento_id');
        });

        
        $nueva = DB::table('cat_estados')->where('nombre','NUEVA')->first();
        $registro = DB::table('estados_modulos')->where('estado_id',$nueva->id)->where('modulo_id',$cat_modulo->id)->first();
        if ($registro)
            DB::table('even_eventos')->update(['estado_modulo_id'=>$registro->id]);

        Schema::table('even_eventos', function (Blueprint $table) {
            $table->foreign('estado_modulo_id')->references('id')->on('estados_modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_eventos', function (Blueprint $table) {
            $table->dropForeign(['estado_modulo_id']);
            $table->dropIndex('even_eventos_estado_modulo_id_foreign');
        });

        Schema::table('even_eventos', function (Blueprint $table) {
            $table->dropcolumn('estado_modulo_id');
        });
    }
}
