<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('producto');
            $table->string('vigencia');
            $table->integer('unidadmedida_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_productos');
    }
}
