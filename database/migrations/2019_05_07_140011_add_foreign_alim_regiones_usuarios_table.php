<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para ingresar datos de usuarios en alimentarios
  ALCANCE: Para Alimentarios
*/

class AddForeignAlimRegionesUsuariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_regiones_usuarios', function (Blueprint $table) {
      $table->foreign('region_id')->references('id')->on('cat_regiones');
      $table->foreign('access_usuario_id')->references('id')->on('usuarios');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_regiones_usuarios', function (Blueprint $table) {
      $table->dropForeign(['region_id']);
      $table->dropForeign(['access_usuario_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_regiones_usuarios_region_id_foreign');
      $table->dropIndex('alim_regiones_usuarios_access_usuario_id_foreign');
      $table->dropIndex('alim_regiones_usuarios_usuario_id_foreign');
    });
  }
}
