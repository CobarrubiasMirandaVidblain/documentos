<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimDotacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->foreign('subprograma_id')->references('id')->on('programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['subprograma_id']);
            $table->dropForeign(['usuario_id']);
            // $table->dropIndex('alim_dotaciones_programa_id_foreign');  //Ya no es necesario por el unique
            $table->dropIndex('alim_dotaciones_subprograma_id_foreign');
            $table->dropIndex('alim_dotaciones_usuario_id_foreign');
        });
    }
}
