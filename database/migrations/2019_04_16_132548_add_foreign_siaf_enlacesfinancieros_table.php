<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafEnlacesfinancierosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_enlacesfinancieros', function (Blueprint $table) {
      $table->foreign('banco_id')->references('id')->on('cat_bancos');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_enlacesfinancieros', function (Blueprint $table) {
      $table->dropForeign(['banco_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_enlacesfinancieros_banco_id_foreign');
      $table->dropIndex('siaf_enlacesfinancieros_usuario_id_foreign');
    });
  }
}
