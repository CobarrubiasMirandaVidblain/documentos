<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAutorizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autorizaciones', function (Blueprint $table) {
            $table->foreign('solicita_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('autoriza_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('modulo_id')->references('id')->on('cat_modulos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autorizaciones', function (Blueprint $table) {
            $table->dropForeign(['solicita_id']);
            $table->dropForeign(['autoriza_id']);
            $table->dropForeign(['modulo_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
