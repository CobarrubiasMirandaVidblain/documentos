<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimCantidadesdotacionesRequisisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_cantidadesdotaciones_requisisiones', function (Blueprint $table) {
            $table->foreign('programacion_requisicion_id', 'alim_cantdotaciones_requisisiones_prog_req_id_foreign')->references('id')->on('cat_ejercicios');
            $table->foreign('dotacion_id')->references('id')->on('proveedores');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_cantidadesdotaciones_requisisiones', function (Blueprint $table) {
            $table->dropForeign('alim_cantdotaciones_requisisiones_prog_req_id_foreign');
            $table->dropForeign(['dotacion_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
