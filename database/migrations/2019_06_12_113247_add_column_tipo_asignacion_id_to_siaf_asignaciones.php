<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para guaradar el tipo de asignacion
    ALCANCE: fondorotatorio
*/

class AddColumnTipoAsignacionIdToSiafAsignaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_asignaciones', function (Blueprint $table) {
            $table->unsignedInteger('tipo_asignacion_id')->after('fecha');
        });

        $registro = DB::table('siaf_cat_tipos_asignaciones')->first();
        if ($registro)
            DB::table('siaf_asignaciones')->update(['tipo_asignacion_id'=>$registro->id]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_asignaciones', function (Blueprint $table) {
            $table->dropColumn('tipo_asignacion_id');
        });
    }
}
