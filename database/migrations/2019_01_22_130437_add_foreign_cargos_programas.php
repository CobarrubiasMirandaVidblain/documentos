<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de alimentarios
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignCargosProgramas extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('cargos_programas', function (Blueprint $table) {
      $table->foreign('cargo_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cargos_programas', function (Blueprint $table) {
      $table->dropForeign(['cargo_id']);
      $table->dropForeign(['programa_id']);
      $table->dropIndex('cargos_programas_cargo_id_foreign');
      $table->dropIndex('cargos_programas_programa_id_foreign');
    });
  }
}
