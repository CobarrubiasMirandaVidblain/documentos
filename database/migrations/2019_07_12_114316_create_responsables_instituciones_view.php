<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Anthony.
    MOTIVO: Para crear vista de responsables de instituciones
    ALCANCE: SICODOC
*/

class CreateResponsablesInstitucionesView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE VIEW responsables_instituciones AS 
            (
                SELECT
                    r.id,
                    i.nombre as area,
                    i.encargado as responsable,
                    i.cargoencargado as cargo
                FROM 
                    sdoc_responsables r
                INNER JOIN 
                    cat_instituciones i on (r.instituciones_responsables_id = i.id)
                WHERE
                    i.deleted_at is null  
            )"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS responsables_instituciones");
    }
}
