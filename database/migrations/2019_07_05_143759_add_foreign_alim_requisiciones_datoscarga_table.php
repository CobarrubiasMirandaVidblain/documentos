<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para el ingreso de una entrega de proveedor
    ALCANCE: Alimentarios
*/

class AddForeignAlimRequisicionesDatoscargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_requisiciones_datoscarga', function (Blueprint $table) {
            $table->foreign('programacion_requisicion_id','alim_requisiciones_datoscarga_prog_requisicion_id_foreign')->references('id')->on('alim_programacion_requisiciones');
            $table->foreign('datocarga_id')->references('id')->on('alim_datoscarga');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_requisiciones_datoscarga', function (Blueprint $table) {
            $table->dropForeign('alim_requisiciones_datoscarga_prog_requisicion_id_foreign');
            $table->dropForeign(['datocarga_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_requisiciones_datoscarga_prog_requisicion_id_foreign');
            $table->dropIndex('alim_requisiciones_datoscarga_datocarga_id_foreign');
            $table->dropIndex('alim_requisiciones_datoscarga_usuario_id_foreign');
        });
    }
}
