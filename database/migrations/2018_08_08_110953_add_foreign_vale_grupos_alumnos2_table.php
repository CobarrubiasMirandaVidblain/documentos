<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeGruposAlumnos2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_GruposAlumnos', function(Blueprint $table){
        $table->foreign('grupo_horario_id')->references('id')->on('vale_GruposHorarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('alumno_id')->references('id')->on('vale_Alumnos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_GruposAlumnos', function(Blueprint $table){
        $table->dropForeign(['grupo_horario_id']);
        $table->dropForeign(['alumno_id']);
      });
    }
}
