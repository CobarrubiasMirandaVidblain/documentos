<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel, Jhony.
    MOTIVO: No es servicial el campo puede_agregar_benef en esta tabla, se pasa a la tabla beneficiosprogramas.
    ALCANCE: Solicitudes
*/

class AlterDelcolPuedeAgregarBenefToProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programas', function (Blueprint $table) {
            $table->dropColumn('puede_agregar_benef');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programas', function (Blueprint $table) {
            $table->boolean('puede_agregar_benef')->after('oficial')->default(0);
        });
    }
}
