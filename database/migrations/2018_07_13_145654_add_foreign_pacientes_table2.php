<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPacientesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function(Blueprint $table){
          $table->foreign('pais_id')->references('id')->on('cat_paises')->onUpdate('CASCADE')->onDelete('CASCADE');
          $table->foreign('entidad_id')->references('id')->on('cat_entidades')->onUpdate('CASCADE')->onDelete('CASCADE');
          $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pacientes', function(Blueprint $table){
          $table->dropForeign(['pais_id']);
          $table->dropForeign(['entidad_id']);
          $table->dropForeign(['usuario_id']);
        });
    }
}
