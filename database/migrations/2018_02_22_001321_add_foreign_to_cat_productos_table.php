<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToCatProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_productos', function (Blueprint $table) {
            $table->foreign('unidadmedida_id')->references('id')->on('cat_unidadmedidas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_productos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
             $table->dropForeign(['unidadmedida_id']);
        });
    }
}
