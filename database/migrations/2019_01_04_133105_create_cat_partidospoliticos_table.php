<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateCatPartidospoliticosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cat_partidospoliticos', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre');
      $table->string('siglas');
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['nombre','deleted_at']);
      $table->unique(['siglas','deleted_at']);
    });

    DB::table('cat_partidospoliticos')->insert([
      ['nombre'=>'PARTIDO ACCIÓN NACIONAL', 'siglas'=>'PAN', 'created_at'=>NOW()],
      ['nombre'=>'PARTIDO REVOLUCIONARIO INSTITUCIONAL', 'siglas'=>'PRI', 'created_at'=>NOW()],
      ['nombre'=>'PARTIDO DE LA REVOLUCIÓN DEMOCRÁTICA', 'siglas'=>'PRD', 'created_at'=>NOW()],
      ['nombre'=>'PARTIDO DEL TRABAJO', 'siglas'=>'PT', 'created_at'=>NOW()],
      ['nombre'=>'PARTIDO VERDE ECOLOGISTA DE MÉXICO', 'siglas'=>'PVEM', 'created_at'=>NOW()],
      ['nombre'=>'MOVIMIENTO CIUDADANO', 'siglas'=>'MC', 'created_at'=>NOW()],
      ['nombre'=>'MOVIMIENTO REGENERACIÓN NACIONAL', 'siglas'=>'MORENA', 'created_at'=>NOW()],
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('cat_partidospoliticos');
  }
}
