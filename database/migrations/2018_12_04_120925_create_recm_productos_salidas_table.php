<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmProductosSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_productos_salidas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salida_id');
            $table->unsignedInteger('presentacion_producto_id');
            $table->unsignedInteger('cantidad');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_productos_salidas');
    }
}
