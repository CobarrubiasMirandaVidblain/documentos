<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para relacionar la caravana de itinerantes con un evento
    ALCANCE: Itinerantes
*/

class AddColumnEventoIdToItnCaravanaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itn_caravana', function (Blueprint $table) {
            $table->unsignedInteger('evento_id')->after('municipio_id')->nullable();
						$table->foreign('evento_id')->references('id')->on('even_eventos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itn_caravana', function (Blueprint $table) {
            $table->dropForeign(['evento_id']);
						$table->dropIndex('itn_caravana_evento_id_foreign');
						$table->dropColumn('evento_id');
        });
    }
}
