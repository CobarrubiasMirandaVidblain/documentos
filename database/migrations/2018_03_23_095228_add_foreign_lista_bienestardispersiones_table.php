<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignListaBienestardispersionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bienestardispersiones', function (Blueprint $table) {
            $table->foreign('lista_id')->references('id')->on('listas_dispersiones')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bienestardispersiones', function (Blueprint $table) {
            $table->dropForeign(["lista_id"]);
           
        });
    }
}
