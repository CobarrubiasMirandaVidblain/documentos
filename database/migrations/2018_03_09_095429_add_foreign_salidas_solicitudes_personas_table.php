<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSalidasSolicitudesPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes_personas', function (Blueprint $table) {
            $table->integer('salidas_producto_id')->unsigned()->nullable();
            $table->foreign('salidas_producto_id')->references('id')->on('salidas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes_personas', function (Blueprint $table) {
            $table->dropForeign(["salidas_producto_id"]);
           
        });
    }
}
