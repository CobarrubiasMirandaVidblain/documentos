<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignOrevEventosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('orev_eventos', function (Blueprint $table) {
      $table->foreign('tipo_evento_id')->references('id')->on('orev_cat_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('localidad_id')->references('id')->on('cat_localidades')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('status_id')->references('id')->on('cat_statusprocesos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('orev_eventos', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropForeign(['status_id']);
      $table->dropForeign(['municipio_id']);
      $table->dropForeign(['localidad_id']);
      $table->dropForeign(['tipo_evento_id']);

      $table->dropIndex('orev_eventos_usuario_id_foreign');
      $table->dropIndex('orev_eventos_status_id_foreign');
      $table->dropIndex('orev_eventos_municipio_id_foreign');
      $table->dropIndex('orev_eventos_localidad_id_foreign');
      $table->dropIndex('orev_eventos_tipo_evento_id_foreign');
    });
  }
}
