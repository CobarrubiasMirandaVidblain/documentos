<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar quien guardo el archivo
    ALCANCE: fondorotatorio
*/

class AddForeignSiafArchivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('siaf_archivos_usuario_id_foreign');
        });
    }
}
