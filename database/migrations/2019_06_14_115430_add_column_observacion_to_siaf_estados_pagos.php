<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para guaradar la observacion del cambio de estatus
    ALCANCE: fondorotatorio
*/

class AddColumnObservacionToSiafEstadosPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_estados_pagos', function (Blueprint $table) {
            $table->string('observacion',255)->after('fecha')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_estados_pagos', function (Blueprint $table) {
            $table->dropColumn('observacion');
        });
    }
}
