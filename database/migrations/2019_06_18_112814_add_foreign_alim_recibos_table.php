<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimRecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->foreign('programacion_requisicion_id')->references('id')->on('alim_programacion_requisiciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->dropForeign(['programacion_requisicion_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_recibos_programacion_requisicion_id_foreign');
            $table->dropIndex('alim_recibos_usuario_id_foreign');
        });
    }
}
