<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invc_entradas', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('ingreso');
            $table->string('folio');
            $table->string('origen');
            $table->string('recibe');
            $table->string('vehiculo')->nullable();
            $table->string('conductor')->nullable();
            $table->text('observaciones')->nullable();
            $table->unsignedInteger('proveedor_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invc_entradas');
    }
}