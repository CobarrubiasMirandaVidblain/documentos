<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignEjercicioListasdispersionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('listas_dispersiones', function (Blueprint $table) {
            $table->integer('ejercicio_id')->unsigned()->nullable();
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('listas_dispersiones', function (Blueprint $table) {
            $table->dropForeign(["ejercicio_id"]); 
        });
    }
}
