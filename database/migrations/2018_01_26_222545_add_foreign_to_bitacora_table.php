<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bitacora', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('actividad_id')->references('id')->on('cat_actividades')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('modulo_id')->references('id')->on('cat_modulos')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bitacora', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['actividad_id']);
            $table->dropForeign(['modulo_id']);
        });
    }
}
