<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimAlimentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_alimentarios', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('localidad_id');
            $table->unsignedInteger('municipio_id');
            $table->string('sublocalidad',100)->nullable();
            $table->string('latitud',20)->nullable();
            $table->string('longitud',20)->nullable();
            $table->string('folio',20);
            $table->unsignedInteger('programa_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_alimentarios');
    }
}
