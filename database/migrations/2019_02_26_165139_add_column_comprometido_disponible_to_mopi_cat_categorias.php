<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega la columna disponible, comprometido
  ALCANCE: Para tener un control de lo gastado y disponible para el sistema de recursos materiales
*/

class AddColumnComprometidoDisponibleToMopiCatCategorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_cat_categorias', function (Blueprint $table) {
            $table->unsignedInteger('disponible')->after('programa_id');
            $table->unsignedInteger('comprometido')->after('programa_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_cat_categorias',function(Blueprint $table){
            $table->dropColumn('disponible');
            $table->dropColumn('comprometido');
        });
    }
}
