<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para ingresar datos de indicadores
    ALCANCE: General
*/

class CreateCatFuentesindicadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_fuentesindicadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',20);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['nombre']);
        });

        DB::table('cat_fuentesindicadores')->insert([
            ['nombre'=>'INEGI', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre'=>'CONEVAL', 'created_at'=>NOW(), 'updated_at'=>NOW()],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_fuentesindicadores');
    }
}
