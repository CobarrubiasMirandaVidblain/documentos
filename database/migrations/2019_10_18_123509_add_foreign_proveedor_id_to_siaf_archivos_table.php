<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Llaves foráneas para tabla de siaf_archivos.
    ALCANCE: SIAF
*/

class AddForeignProveedorIdToSiafArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->unsignedInteger('proveedor_id')->nullable();
        });
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->foreign('proveedor_id')->references('id')->on('siaf_cat_proveedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->dropForeign(['proveedor_id']);
            $table->dropIndex('siaf_archivos_proveedor_id_foreign');
        });
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->dropColumn('proveedor_id');
        });
    }
}
