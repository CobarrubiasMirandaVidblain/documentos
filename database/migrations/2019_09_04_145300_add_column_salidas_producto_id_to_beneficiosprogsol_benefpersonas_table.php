<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: 
    ALCANCE: 
*/

class AddColumnSalidasProductoIdToBeneficiosprogsolBenefpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('beneficiosprogsol_benefpersonas', function (Blueprint $table) {
				$table->unsignedInteger('salidas_producto_id')->after('beneficiosprogramas_solicitud_id')->nullable();
				$table->foreign('salidas_producto_id')->references('id')->on('salidas_productos');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('beneficiosprogsol_benefpersonas', function (Blueprint $table) {
				$table->dropForeign(['salidas_producto_id']);
				$table->dropIndex('beneficiosprogsol_benefpersonas_salidas_producto_id_foreign');
				$table->dropColumn('salidas_producto_id');
			});
    }
}
