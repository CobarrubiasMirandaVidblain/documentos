<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony.
  MOTIVO: Para relacionar lo beneficios con una persona.
  ALCANCE: Atención ciudadana y Apoyos funcionales.
*/

class AddForeignBeneficiosprogsolPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiosprogsol_personas', function (Blueprint $table) {
            $table->foreign('beneficiosprogramas_solicitud_id', 'benefprogsol_personas_benefprog_solicitud_id_foreign')->references('id')->on('beneficiosprogramas_solicitudes');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiosprogsol_personas', function (Blueprint $table) {
            $table->dropForeign('benefprogsol_personas_benefprog_solicitud_id_foreign');
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["usuario_id"]);

            $table->dropIndex('benefprogsol_personas_benefprog_solicitud_id_foreign');
            $table->dropIndex('beneficiosprogsol_personas_persona_id_foreign');
            $table->dropIndex('beneficiosprogsol_personas_usuario_id_foreign');
        });
    }
}
