<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos que nos proporiconan las áreas de montos invertidos en programas
  ALCANCE: Para estadísticas en sistema Eventos
*/

class AddForeignInveInversionesprogramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('inve_inversionesprogramas', function (Blueprint $table) {
      $table->foreign('anio_programa_id')->references('id')->on('anios_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('rubro_id')->references('id')->on('inve_cat_rubros')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('inve_inversionesprogramas', function (Blueprint $table) {
      $table->dropForeign(['anio_programa_id']);
      $table->dropForeign(['rubro_id']);
      $table->dropForeign(['municipio_id']);
      // $table->dropIndex('inve_inversionesprogramas_anio_programa_id_foreign');  // No se crea por el unique que ya se agregó
      $table->dropIndex('inve_inversionesprogramas_rubro_id_foreign');
      $table->dropIndex('inve_inversionesprogramas_municipio_id_foreign');
    });
  }
}
