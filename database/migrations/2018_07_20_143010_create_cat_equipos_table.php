<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->string('num_serie',100);
            $table->string('num_inventario',40);
            $table->string('direccion_ip',20)->nullable();
            $table->string('capacidad',40)->nullable();
            $table->integer('marca_id')->unsigned();
            $table->integer('modelo_id')->unsigned();
            $table->integer('tipoequipo_id')->unsigned();
            $table->integer('resguardo_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_equipos');
    }
}
