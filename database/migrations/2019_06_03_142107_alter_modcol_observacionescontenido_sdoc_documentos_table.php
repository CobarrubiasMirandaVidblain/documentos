<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Para almacenar correctamente las observaciones del documento
  ALCANCE: Módulo SICODOC
*/

class AlterModcolObservacionescontenidoSdocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_documentos', function (Blueprint $table) {
            $table->string('observaciones', 255)->change();
            $table->string('contenido', 255)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_documentos', function (Blueprint $table) {
            $table->string('observaciones', 255)->change();
            $table->string('contenido', 255)->change();
        });
    }
}
