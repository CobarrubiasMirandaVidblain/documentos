<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCargasgasilinaUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cargasgasolina_unidades', function (Blueprint $table) {
            $table->foreign('estadounidad_id')->references('id')->on('estadounidades')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('cargagasolina_id')->references('id')->on('cat_gasolinacargas')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cargasgasolina_unidades', function (Blueprint $table) {
            $table->dropForeign(["estadounidad_id"]);
            $table->dropForeign(["cargagasolina_id"]);
        });
    }
}
