<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos generales en el registro de pasajeros principalmente cuando son de otro estado o extranjeros
  ALCANCE: Dif te lleva
*/

class AlterAddcolsPaisidEntidadidToDtllPasajeros extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_pasajeros', function (Blueprint $table) {
      $table->unsignedInteger('pais_id')->after('persona_id');
      $table->unsignedInteger('entidad_id')->after('pais_id');
    });

    DB::statement ('UPDATE dtll_pasajeros set pais_id=165, entidad_id=20');

    Schema::table('dtll_pasajeros', function(Blueprint $table){
        $table->foreign('pais_id')->references('id')->on('cat_paises')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('entidad_id')->references('id')->on('cat_entidades')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_pasajeros', function (Blueprint $table) {
      $table->dropForeign(['pais_id']);
      $table->dropForeign(['entidad_id']);
      $table->dropIndex('dtll_pasajeros_pais_id_foreign');
      $table->dropIndex('dtll_pasajeros_entidad_id_foreign');
      $table->dropColumn('pais_id');
      $table->dropColumn('entidad_id');
    });
  }
}
