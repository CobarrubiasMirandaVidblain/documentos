<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTipopagoIdToSiafComprobaciones extends Migration
{
    /* 
     * PETICIÓN: Manuel
     * MOTIVO: Facilitar consulta a la tabla
     * ALCANCE: Fondo Rotatorio
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->unsignedInteger('tipopago_id')->after('monto_total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->dropColumn('tipopago_id');
        });
    }
}
