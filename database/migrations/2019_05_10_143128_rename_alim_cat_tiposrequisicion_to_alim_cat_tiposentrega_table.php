<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAlimCatTiposrequisicionToAlimCatTiposentregaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('alim_cat_tiposrequisicion', 'alim_cat_tiposentrega');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('alim_cat_tiposentrega', 'alim_cat_tiposrequisicion');
    }
}
