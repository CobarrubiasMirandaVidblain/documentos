<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para identificar qué área puede generar una ficha para evento
    ALCANCE: Eventos
*/

class AlterAddcolGeneraFichaToEvenAreasEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('even_areas_eventos', function (Blueprint $table) {
            $table->boolean('genera_ficha')->default(0)->after('evento_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_areas_eventos', function (Blueprint $table) {
            $table->dropColumn('genera_ficha');
        });
    }
}
