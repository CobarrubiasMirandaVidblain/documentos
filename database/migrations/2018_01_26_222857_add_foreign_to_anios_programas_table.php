<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToAniosProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('anios_programas', function (Blueprint $table) {
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('programa_id')->references('id')->on('programas')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('anios_programas', function (Blueprint $table) {
            $table->dropForeign(['ejercicio_id']);
            $table->dropForeign([ 'programa_id']);
        });
    }
}
