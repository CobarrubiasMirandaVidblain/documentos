<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_responsables
  ALCANCE: Para el sistema SICODOC
*/




class AddForeignSdocResponsablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_responsables', function (Blueprint $table) {
        $table->foreign('areas_responsables_id')->references('id')->on('areas_responsables');
        $table->foreign('instituciones_responsables_id')->references('id')->on('sdoc_instituciones_responsables');
        $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_responsables', function (Blueprint $table) {
        $table->dropForeign(['areas_responsables_id']);
        $table->dropForeign(['instituciones_responsables_id']);
        $table->dropForeign(['usuario_id']);   
        
        $table->dropIndex('sdoc_responsables_areas_responsables_id_foreign');    
        $table->dropIndex('sdoc_responsables_instituciones_responsables_id_foreign');    
        $table->dropIndex('sdoc_responsables_usuario_id_foreign');    
        });
    }
}
