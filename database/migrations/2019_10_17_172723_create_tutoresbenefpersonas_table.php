<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutoresbenefpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutoresbenefpersonas', function (Blueprint $table) {
            $table->increments('id');
						$table->unsignedInteger('tutor_persona_id');
						$table->unsignedInteger('tutorado_beneficioprog_persona_id');
            
						$table->unsignedInteger('usuario_id');
            $table->timestamps();
						$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutoresbenefpersonas');
    }
}
