<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimAlimentariosRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_alimentarios_requisiciones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('programacion_id');
            $table->unsignedInteger('requisicion_id');
            $table->unsignedInteger('numero_dotaciones');
            $table->unsignedInteger('numero_oficio_validacion');
            $table->decimal('inversion',12,2);
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('programa_id');
            $table->unsignedInteger('recibo_id');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_alimentarios_requisiciones');
    }
}
