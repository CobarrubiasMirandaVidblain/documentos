<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToDependenciasSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dependencias_solicitudes', function (Blueprint $table) {
            $table->foreign('dependencia_id')->references('id')->on('cat_dependencias')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('solicitud_id')->references('id')->on('solicitudes')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dependencias_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['solicitud_id']);
            $table->dropForeign(['dependencia_id']);
        });
    }
}
