<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel, Jhony, Emanuel.
    MOTIVO: Para que se relaicione con la tabla beneficiosprogramas
    ALCANCE: Solicitudes
*/

class AlterDelcolPersonaIdAddcolBeneficioprogPersonaIdToBeneficiosprogsolBenefpersonas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Quitamos la columna persona_id
        Schema::table('beneficiosprogsol_benefpersonas', function (Blueprint $table) {
            $table->dropForeign(["persona_id"]);
            $table->dropIndex('beneficiosprogsol_benefpersonas_persona_id_foreign');
            $table->dropColumn(["persona_id"]);
        });

        // Agregamos la columna foránea beneficiopersona_id
        Schema::table('beneficiosprogsol_benefpersonas', function (Blueprint $table) {
            $table->unsignedInteger('beneficiopersona_id')->after('beneficiosprogramas_solicitud_id');
            $table->foreign('beneficiopersona_id')->references('id')->on('beneficiosprog_personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Agregamos la columna persona_id
        Schema::table('beneficiosprogsol_benefpersonas', function (Blueprint $table) {
            $table->unsignedInteger('persona_id')->after('beneficiosprogramas_solicitud_id');
            $table->foreign('persona_id')->references('id')->on('personas');
        });

        //Quitamos la columna beneficiopersona_id
        Schema::table('beneficiosprogsol_benefpersonas', function (Blueprint $table) {
            $table->dropForeign(["beneficiopersona_id"]);
            $table->dropIndex('beneficiosprogsol_benefpersonas_beneficiopersona_id_foreign');
            $table->dropColumn(["beneficiopersona_id"]);
        });
    }
}
