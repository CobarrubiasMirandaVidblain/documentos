<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
	PETICIÓN: Manuel.
	MOTIVO: Para guardar los numeros de cuenta de bancos
	ALCANCE: siaf
*/

class CreateSiafNumCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('siaf_num_cuentas', function (Blueprint $table) {
					$table->increments('id');
					$table->integer('ejercicio');
					$table->string('num_cuenta',50);
					$table->string('concepto');
					$table->unsignedInteger('banco_id');
					$table->timestamps();
					$table->softDeletes();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_num_cuentas');
    }
}
