<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar las asignaciones de tipo gastos a comprobar
    ALCANCE: fondorotatorio
*/

class CreateSiafAsignacionesGastosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_asignaciones_gastos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',150);
            $table->float('saldo_disponible');
            $table->unsignedInteger('asignacion_id');
            $table->unsignedInteger('tipo_pago_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_asignaciones_gastos');
    }
}
