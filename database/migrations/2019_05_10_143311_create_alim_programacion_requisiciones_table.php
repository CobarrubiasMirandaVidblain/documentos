<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de requisiciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimProgramacionRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('requisicion_id');
            $table->unsignedInteger('programacion_id');
            $table->tinyInteger('num_oficio_validacion')->unsigned();
            $table->unsignedInteger('estado_programa_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_programacion_requisiciones');
    }
}
