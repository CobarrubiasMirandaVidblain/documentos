<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Administrador de la BD
  MOTIVO: El nombre de la tabla cat_estados_programas no debe tener el prefijo cat_
  ALCANCE: BD
*/

class AlterRenameCatEstadosProgramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    /********* QUITAMOS LAS RELACIONES PARA PODER RENOMBRAR *********/
    Schema::table('cat_estados_programas', function (Blueprint $table) {
      $table->dropForeign(['estado_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['usuario_id']);
      // $table->dropIndex('cat_estados_programas_estado_id_foreign');  //Por el index unique este index ya no se crea
      $table->dropIndex('cat_estados_programas_programa_id_foreign');
      $table->dropIndex('cat_estados_programas_usuario_id_foreign');
      
      $table->dropUnique(['estado_id', 'programa_id','deleted_at']);
    });
    Schema::table('bien_refrendos_personas', function (Blueprint $table) {
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('bien_refrendos_personas_estado_programa_id_foreign');
    });
    Schema::table('bien_refrendos', function (Blueprint $table) {
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('bien_refrendos_estado_programa_id_foreign');
    });

    /********* RENOMBRAMOS *********/
    Schema::rename('cat_estados_programas', 'estados_programas');

    /********* ESTABLECEMOS NUEVAMENTE LAS RELACIONES *********/
    Schema::table('estados_programas', function (Blueprint $table) {
      $table->unique(['estado_id', 'programa_id','deleted_at']);
      $table->foreign('estado_id')->references('id')->on('cat_estados')->onUpdte('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_id')->references('id')->on('programas')->onUpdte('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('bien_refrendos_personas', function (Blueprint $table) {
      $table->foreign('estado_programa_id')->references('id')->on('estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('bien_refrendos', function (Blueprint $table) {
      $table->foreign('estado_programa_id')->references('id')->on('estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    /********* QUITAMOS LAS RELACIONES PARA PODER RENOMBRAR *********/
    Schema::table('estados_programas', function (Blueprint $table) {
      $table->dropForeign(['estado_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['usuario_id']);
      // $table->dropIndex('cat_estados_programas_estado_id_foreign');  //Por el index unique este index ya no se crea
      $table->dropIndex('estados_programas_programa_id_foreign');
      $table->dropIndex('estados_programas_usuario_id_foreign');
      
      $table->dropUnique(['estado_id', 'programa_id','deleted_at']);
    });
    Schema::table('bien_refrendos_personas', function (Blueprint $table) {
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('bien_refrendos_personas_estado_programa_id_foreign');
    });
    Schema::table('bien_refrendos', function (Blueprint $table) {
      $table->dropForeign(['estado_programa_id']);
      $table->dropIndex('bien_refrendos_estado_programa_id_foreign');
    });

    /********* RENOMBRAMOS *********/
    Schema::rename('estados_programas', 'cat_estados_programas');

    /********* ESTABLECEMOS NUEVAMENTE LAS RELACIONES *********/
    Schema::table('cat_estados_programas', function (Blueprint $table) {
      $table->unique(['estado_id', 'programa_id','deleted_at']);
      $table->foreign('estado_id')->references('id')->on('cat_estados')->onUpdte('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_id')->references('id')->on('programas')->onUpdte('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('bien_refrendos_personas', function (Blueprint $table) {
      $table->foreign('estado_programa_id')->references('id')->on('cat_estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('bien_refrendos', function (Blueprint $table) {
      $table->foreign('estado_programa_id')->references('id')->on('cat_estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
