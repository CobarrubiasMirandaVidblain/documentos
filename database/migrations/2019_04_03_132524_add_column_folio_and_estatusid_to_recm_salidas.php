<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega el campo estatus para que pueda cancelar la salida
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnFolioAndEstatusidToRecmSalidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_salidas', function (Blueprint $table) {
            $table->unsignedInteger('estatus_id')->after('fecha_hora');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_salidas',function(Blueprint $table){
            $table->dropColumn('estatus_id');
        });
    }
}
