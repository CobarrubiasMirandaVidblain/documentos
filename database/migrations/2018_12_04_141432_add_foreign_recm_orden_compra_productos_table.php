<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmOrdenCompraProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_orden_compra_productos
        Schema::table('recm_orden_compra_productos', function (Blueprint $table) {
            $table->foreign('producto_requisicion_id')->references('id')->on('recm_producto_requisicions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('orden_compra_id')->references('id')->on('recm_ordenescompra')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_orden_compra_productos', function (Blueprint $table) {
            $table->dropForeign(['producto_requisicion_id']);
            $table->dropForeign(['orden_compra_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_orden_compra_productos_usuario_id_foreign');
            $table->dropIndex('recm_orden_compra_productos_orden_compra_id_foreign');
            $table->dropIndex('recm_orden_compra_productos_producto_requisicion_id_foreign');
            
        });
    }
}
