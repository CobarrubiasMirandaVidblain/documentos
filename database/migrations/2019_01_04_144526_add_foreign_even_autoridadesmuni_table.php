<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AddForeignEvenAutoridadesmuniTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_autoridadesmuni', function (Blueprint $table) {
      $table->foreign('fichainfo_id')->references('id')->on('even_fichasinfo')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('cargo_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('partidopolitico_id')->references('id')->on('cat_partidospoliticos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_autoridadesmuni', function (Blueprint $table) {
      $table->dropForeign(['fichainfo_id']);
      $table->dropForeign(['cargo_id']);
      $table->dropForeign(['partidopolitico_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('even_autoridadesmuni_fichainfo_id_foreign');
      $table->dropIndex('even_autoridadesmuni_cargo_id_foreign');
      $table->dropIndex('even_autoridadesmuni_partidopolitico_id_foreign');
      $table->dropIndex('even_autoridadesmuni_usuario_id_foreign');
    });
  }
}
