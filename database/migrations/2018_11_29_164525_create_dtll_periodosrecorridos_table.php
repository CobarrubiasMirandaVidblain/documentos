<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian y Jhony
  MOTIVO: Reestructura del funcionamiento del sistema
  ALCANCE: DIF te lleva
*/

class CreateDtllPeriodosrecorridosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dtll_periodosrecorridos', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('recorrido_id');
      $table->datetime('fecha_ini');
      $table->datetime('fecha_fin');

      $table->unsignedInteger('usuario_id');
			$table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dtll_periodosrecorridos');
  }
}
