<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignGirapreregisidGiraspregisprogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
        $table->foreign('girapreregis_id')->references('id')->on('giraspreregis')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
        $table->dropForeign(['girapreregis_id']);
        $table->dropForeign(['programa_id']);
      });
    }
}
