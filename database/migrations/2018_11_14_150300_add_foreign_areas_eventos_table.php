<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos de Dirección General
  ALCANCE: Para todos
*/

class AddForeignAreasEventosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('areas_eventos', function (Blueprint $table) {
      $table->foreign('area_id')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('evento_id')->references('id')->on('atnc_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('areas_eventos', function (Blueprint $table) {
      $table->dropForeign(['area_id']);
      $table->dropForeign(['evento_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('areas_eventos_area_id_foreign');
      $table->dropIndex('areas_eventos_evento_id_foreign');
      $table->dropIndex('areas_eventos_usuario_id_foreign');
    });
  }
}
