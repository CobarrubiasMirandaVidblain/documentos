<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos de Dirección General
  ALCANCE: Para todos
*/

class CreateAreasEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_eventos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('area_id');
            $table->unsignedInteger('evento_id');
            $table->string('observaciones')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_eventos');
    }
}
