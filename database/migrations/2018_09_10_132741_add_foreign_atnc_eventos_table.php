<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAtncEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('atnc_eventos', function (Blueprint $table) {
        $table->foreign('localidad_id')->references('id')->on('cat_localidades')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('atnc_eventos',function(Blueprint $table){
        $table->dropForeign(['localidad_id']);
        $table->dropForeign(['municipio_id']);
        $table->dropForeign(['usuario_id']);
        $table->dropIndex('atnc_eventos_localidad_id_foreign');
        $table->dropIndex('atnc_eventos_municipio_id_foreign');
        $table->dropIndex('atnc_eventos_usuario_id_foreign');
      });
    }
}
