
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToAreasResponsablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas_responsables', function (Blueprint $table) {
            $table->foreign('area_id')->references('id')->on('cat_areas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('empleado_id')->references('id')->on('empleados')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas_responsables', function (Blueprint $table) {
            $table->dropForeign(['area_id']);
            $table->dropForeign(['empleado_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
