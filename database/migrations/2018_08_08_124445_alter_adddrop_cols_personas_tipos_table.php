<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdddropColsPersonasTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('personas_tipos', function(Blueprint $table){
        $table->dropColumn('tabla');
        $table->integer('tablabd_id')->unsigned()->after('persona_id');
        $table->integer('modulo_id')->unsigned()->after('tablabd_id');

        $table->foreign('tablabd_id')->references('id')->on('cat_tablasbd')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('modulo_id')->references('id')->on('cat_modulos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('personas_tipos', function(Blueprint $table){
        $table->string('tabla')->nullble()->after('persona_id');
        
        $table->dropForeign(['tablabd_id']);
        $table->dropForeign(['modulo_id']);

        $table->dropColumn('tablabd_id');
        $table->dropColumn('modulo_id');
      });
    }
}
