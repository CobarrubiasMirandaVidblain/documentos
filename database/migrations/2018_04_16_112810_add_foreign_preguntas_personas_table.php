<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPreguntasPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preguntas_personas', function (Blueprint $table) {
            $table->foreign('pregunta_id')->references('id')->on('preguntas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preguntas_personas', function (Blueprint $table) {
            $table->dropForeign(["pregunta_id"]);
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["empleado_id"]);
        });
    }
}
