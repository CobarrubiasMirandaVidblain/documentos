<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->string('rfc')->unique();
            $table->integer('num_empleado')->unique()->nullable();
            $table->string('segurosocial')->nullable();
            $table->integer('nivel_id')->unsigned();
            $table->integer('tiposempleado_id')->unsigned();
            $table->string('persona_emergencia')->nullable();
            $table->string('telefono_emergencia')->nullable();
            $table->dateTime('fechaingreso')->nullable();
            $table->dateTime('fechaingpuesto')->nullable();
            $table->dateTime('fechaingnombramiento')->nullable();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
