<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Migue.
    MOTIVO: Para quitar la relación con la tabla personas y ahora ingresar los datos de remitente en la tabla atnc_remitentes
    ALCANCE: Atención ciudadana
*/

class AddForeignAtncRemitentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atnc_remitentes', function (Blueprint $table) {
            $table->foreign('cargo_id')->references('id')->on('cat_cargos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atnc_remitentes', function (Blueprint $table) {
            $table->dropForeign(['cargo_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('atnc_remitentes_cargo_id_foreign');
            $table->dropIndex('atnc_remitentes_usuario_id_foreign');
        });
    }
}
