<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de licitaciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimDetallesdotacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_detallesdotacion', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('costo_producto_id');
            $table->unsignedInteger('dotacion_id');
            $table->tinyInteger('cantidad')->unsigned();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_detallesdotacion');
    }
}
