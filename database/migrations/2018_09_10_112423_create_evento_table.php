<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('tipo_evento_id')->unsigned();
            $table->integer('localidad_id')->unsigned();
            $table->integer('municipio_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->integer('usuario_id')->unsigned()->nullable();
           
            $table->string('nombreEvento');
            $table->date('fechaEvento');
            $table->string('calle');
            $table->string('numExt')->nullable();
            $table->string('numInt')->nullable();
            $table->string('colonia');
            $table->string('folio');
            $table->string('descripcionEvento');
            $table->string('userCaptura');
            $table->integer('codigoPostal')->unsigned();


            $table->timestamps();
            $table->softDeletes();

            // Relaciones

            $table->foreign('tipo_evento_id')->references('id')->on('cat_eventos');
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('status_id')->references('id')->on('cat_statusprocesos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento');
    }
}
