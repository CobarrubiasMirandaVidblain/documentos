<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de las personas que asisten a una caravana
    ALCANCE: Itinerantes
*/

class CreateItnBeneficiospersonasCaravanaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itn_beneficiospersonas_caravana', function (Blueprint $table) {
            $table->increments('id');

						$table->unsignedInteger('caravana_id');
						$table->unsignedInteger('beneficioprog_persona_id');
						$table->string('seccion',10)->nullable();
						$table->string('observaciones')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
						$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itn_beneficiospersonas_caravana');
    }
}
