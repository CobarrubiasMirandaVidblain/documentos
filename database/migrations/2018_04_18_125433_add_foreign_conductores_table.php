<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignConductoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conductores', function (Blueprint $table) {
            $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('licencia_id')->references('id')->on('licencias')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conductores', function (Blueprint $table) {
            $table->dropForeign(["empleado_id"]);
            $table->dropForeign(["licencia_id"]); 
        });
    }
}
