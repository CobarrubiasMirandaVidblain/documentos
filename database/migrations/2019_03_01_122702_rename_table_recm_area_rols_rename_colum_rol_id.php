<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se renombra columna y cambio de nombre de la tabla
  ALCANCE: para no hacer otra tabla en el sistema de recursos materiales
*/

class RenameTableRecmAreaRolsRenameColumRolId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('recm_area_rols', 'recm_area_usuario_responsable');

        Schema::table('recm_area_usuario_responsable', function (Blueprint $table) {
            $table->renameColumn('rol_id', 'usuario_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_area_usuario_responsable', function (Blueprint $table) {
            $table->renameColumn('usuario_id','rol_id');
        });
        Schema::rename('recm_area_usuario_responsable', 'recm_area_rols');
    }
}
