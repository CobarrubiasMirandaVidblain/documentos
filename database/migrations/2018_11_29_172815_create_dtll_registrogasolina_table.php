<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllRegistrogasolinaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dtll_registrogasolina', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('controlruta_id');
            $table->unsignedInteger('tipocarga_id');
            $table->tinyInteger('litros');
            $table->float('importe');
            $table->string('folio');
            $table->datetime('fecha');
            $table->string('foto');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_registrogasolina');
    }
}
