<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipanteEventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participante_evento', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('tipo_evento_id')->unsigned();
            $table->integer('participante_id')->unsigned();

            $table->foreign('tipo_evento_id')->references('id')->on('evento');
            $table->foreign('participante_id')->references('id')->on('personas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participante_evento');
    }
}
