<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar la observacion del por que se cancela la requisicion
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnObservacionToRecmRequisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->string('observacion')->nullable()->after('fecha_captura'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions',function(Blueprint $table){
            $table->dropColumn('observacion');
        });
    }
}
