<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para registrar los productos que solicita una persona.
    ALCANCE: Apoyos Funcionales
*/

class AddForeignAfuPeticionesproductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('afu_peticionesproductos', function (Blueprint $table) {
            $table->foreign('salidas_producto_id')->references('id')->on('salidas_productos');
            $table->foreign('beneficioprogsol_benefpersona_id')->references('id')->on('beneficiosprogsol_benefpersonas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('afu_peticionesproductos', function (Blueprint $table) {
            $table->dropForeign(['salidas_producto_id']);
            $table->dropForeign(['beneficioprogsol_benefpersona_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('afu_peticionesproductos_salidas_producto_id_foreign');
            $table->dropIndex('afu_peticionesproductos_beneficioprogsol_benefpersona_id_foreign');
            $table->dropIndex('afu_peticionesproductos_usuario_id_foreign');
        });
    }
}
