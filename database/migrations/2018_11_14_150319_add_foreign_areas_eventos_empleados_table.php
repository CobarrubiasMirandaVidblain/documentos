<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos de Dirección General
  ALCANCE: Para todos
*/

class AddForeignAreasEventosEmpleadosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('areas_eventos_empleados', function (Blueprint $table) {
      $table->foreign('area_evento_id')->references('id')->on('areas_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('areas_eventos_empleados', function (Blueprint $table) {
      $table->dropForeign(['area_evento_id']);
      $table->dropForeign(['empleado_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('areas_eventos_empleados_area_evento_id_foreign');
      $table->dropIndex('areas_eventos_empleados_empleado_id_foreign');
      $table->dropIndex('areas_eventos_empleados_usuario_id_foreign');
    });
  }
}
