<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class CreatePetaClinicassaludUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peta_clinicassalud_usuarios', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('institucion_usuario_id');
            $table->unsignedInteger('clinicasalud_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['institucion_usuario_id','clinicasalud_id'],'peta_clinicas_usrs_institucion_usuario_id_clinicasalud_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peta_clinicassalud_usuarios');
    }
}
