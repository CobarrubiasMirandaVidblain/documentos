<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
    PETICIÓN: Maai.
    MOTIVO: Enderezar la información guardada en primer apellido y segundo apellido, para que quede como primer apellido bien requisitado y segundo como opcional
    ALCANCE: General
*/

class ChangeSegundoApellidoToDefaultBlankToPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			  DB::statement ('UPDATE personas SET segundo_apellido="" WHERE segundo_apellido is null');

        Schema::table('personas', function(Blueprint $table){
					$table->string('segundo_apellido',250)->default('')->nullable(false)->change();
				});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas', function(Blueprint $table){
					$table->string('segundo_apellido')->nullable()->default(null)->change();
				});
    }
}
