<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian 
  MOTIVO: Se presentó en la necesidad en el desarrollo del sistema DIF te lleva
  ALCANCE: Para todos
*/

class AlterAddcolsUnidadesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::table('unidades', function (Blueprint $table) {
        $table->string('matricula',20)->change();
        $table->string('modelo',50)->change();
        $table->string('marca',20)->change();
        $table->smallInteger('anio')->unsigned()->change();
        $table->string('condiciones')->nullable()->change();
        $table->integer('kilometraje_inicial')->nullable()->change();
        $table->string('tipo_servicio',15)->change();
        $table->string('observaciones')->nullable()->change();
        $table->string('poliza_garantia')->nullable()->change();
        $table->string('numero_unidad',15)->change();

        $table->string('numero_serie',20)->after('numero_unidad')->nullable();                    // Lo pondremos de mientras nullable, porque ya hay datos, pero debe ser not null
        $table->string('numero_motor',15)->after('numero_serie')->nullable();                     // Lo pondremos de mientras nullable, porque ya hay datos, pero debe ser not null
        $table->boolean('numero_cilindros')->tinyInteger('numero_cilindros')->unsigned()->after('numero_motor')->nullable();   //Como va a pasar de tipo date a tinyInteger, para que no nos de error de compatibilidad, primero lo vamos a volver boolean, lo pondremos de mientras nullable, porque ya hay datos, pero debe ser not null
        $table->string('clave_vehicular',15)->after('numero_cilindros')->nullable();              // Lo pondremos de mientras nullable, porque ya hay datos, pero debe ser not null
        $table->string('tipo')->after('clave_vehicular')->nullable();                             // Lo pondremos de mientras nullable, porque ya hay datos, pero debe ser not null
        $table->string('combustible',10)->after('tipo')->nullable();                              // Lo pondremos de mientras nullable, porque ya hay datos, pero debe ser not null
        $table->boolean('litros')->tinyInteger('litros')->unsigned()->nullable()->after('combustible');  //como va a pasar de tipo date a tinyInteger, para que no nos de error de compatibilidad, primero lo vamos a volver boolean
        $table->smallInteger('toneladas')->unsigned()->nullable()->after('litros');
        $table->string('uso_vehiculo',25)->nullable()->after('toneladas');
        $table->string('clase',15)->nullable()->after('uso_vehiculo');
        $table->string('origen_vehiculo',25)->nullable()->after('clase');
        $table->string('fabricante',20)->nullable()->after('origen_vehiculo');
        $table->string('procedencia',15)->nullable()->after('fabricante');
        $table->string('color_exterior',25)->nullable()->after('procedencia');
        $table->string('color_interior',25)->nullable()->after('color_exterior');
        $table->boolean('capacidad_pasajeros')->tinyInteger('capacidad_pasajeros')->unsigned()->nullable()->after('color_interior');  //como va a pasar de tipo date a tinyInteger, para que no nos de error de compatibilidad, primero lo vamos a volver boolean
        $table->boolean('numero_puertas')->tinyInteger('numero_puertas')->unsigned()->nullable()->after('capacidad_pasajeros');       //como va a pasar de tipo date a tinyInteger, para que no nos de error de compatibilidad, primero lo vamos a volver boolean
        $table->string('numero_inventario',25)->nullable()->after('numero_puertas');
        $table->string('numero_pedido',25)->nullable()->after('numero_inventario');
        $table->string('descripcion')->nullable()->after('numero_pedido');

        $table->unsignedInteger('usuario_id')->after('descripcion')->nullable();
      });

      // Les asignamos un dato vacío mientras
      DB::statement('UPDATE unidades SET numero_serie="", numero_motor="", numero_cilindros=0, clave_vehicular="", tipo="", combustible="", usuario_id=1 ');

      // Cambiamos las columnas a not null
      Schema::table('unidades', function (Blueprint $table) {
        $table->string('numero_serie',20)->change();
        $table->string('numero_motor',15)->change();
        $table->boolean('numero_cilindros')->tinyInteger('numero_cilindros')->unsigned()->change();
        $table->string('clave_vehicular',15)->change();
        $table->string('tipo')->change();
        $table->string('combustible',10)->change();
        $table->unsignedInteger('usuario_id')->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('unidades', function (Blueprint $table) {
        $table->dropColumn('numero_serie');
        $table->dropColumn('numero_motor');
        $table->dropColumn('numero_cilindros');
        $table->dropColumn('clave_vehicular');
        $table->dropColumn('tipo');
        $table->dropColumn('combustible');
        $table->dropColumn('litros');
        $table->dropColumn('toneladas');
        $table->dropColumn('uso_vehiculo');
        $table->dropColumn('clase');
        $table->dropColumn('origen_vehiculo');
        $table->dropColumn('fabricante');
        $table->dropColumn('procedencia');
        $table->dropColumn('color_exterior');
        $table->dropColumn('color_interior');
        $table->dropColumn('capacidad_pasajeros');
        $table->dropColumn('numero_puertas');
        $table->dropColumn('numero_inventario');
        $table->dropColumn('numero_pedido');
        $table->dropColumn('descripcion');
        $table->dropColumn('usuario_id');

        $table->string('matricula')->change()->nullable();
        $table->string('modelo')->change()->nullable();
        $table->string('marca')->change()->nullable();
        $table->integer('anio')->change()->nullable();
        $table->string('condiciones')->change()->nullable();
        $table->integer('kilometraje_inicial')->change()->nullable();
        $table->string('tipo_servicio')->change()->nullable();
        $table->string('observaciones')->nullable()->change();
        $table->string('poliza_garantia')->change()->nullable();
        $table->string('numero_unidad')->nullable()->change();
      });
    }
}
