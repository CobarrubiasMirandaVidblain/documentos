<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class AddForeignCatClinicassaludTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_clinicassalud', function (Blueprint $table) {
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_clinicassalud', function (Blueprint $table) {
            $table->dropForeign(['localidad_id']);
            $table->dropForeign(['municipio_id']);
            
            $table->dropIndex('cat_clinicassalud_localidad_id_foreign');
            $table->dropIndex('cat_clinicassalud_municipio_id_foreign');
        });
    }
}
