<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: agregar el campo no_tramite en la tabla siaf_asignaciones_pagos
    ALCANCE: SIAF
*/

class AddColumnNoTramiteToSiafAsignacionesPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_asignaciones_pagos', function (Blueprint $table) {
            $table->unsignedInteger('no_tramite')->nullable()->after('pago_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_asignaciones_pagos', function (Blueprint $table) {
            $table->dropColumn('no_tramite');
        });
    }
}
