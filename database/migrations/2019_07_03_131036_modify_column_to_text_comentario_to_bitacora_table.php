<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para poner más datos que especifiquen porqué se inserta el registro
    ALCANCE: General
*/

class ModifyColumnToTextComentarioToBitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bitacora', function (Blueprint $table) {
            $table->text('comentario')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bitacora', function (Blueprint $table) {
            $table->varchar('comentario')->nullable()->change();
        });
    }
}
