<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscuelaprogramaPersonasbasicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escuelaprograma_personasbasicas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('escuela_programa_id')->unsigned();
            $table->integer('personabasica_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escuelaprograma_personasbasicas');
    }
}
