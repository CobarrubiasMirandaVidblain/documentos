<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSiafReintegrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_reintegros', function (Blueprint $table) {
            $table->foreign('pago_id')->references('id')->on('siaf_pagos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_reintegros', function (Blueprint $table) {
            $table->dropForeign(['pago_id']);
            $table->dropIndex('siaf_reintegros_pago_id_foreign');
        });
    }
}
