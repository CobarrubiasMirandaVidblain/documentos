<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmOrdenescompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_ordenescompra
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('recm_requisicions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('proveedor_id')->references('id')->on('recm_cat_proveedors')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['proveedor_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_ordenescompra_usuario_id_foreign');
            $table->dropIndex('recm_ordenescompra_requisicion_id_foreign');
            $table->dropIndex('recm_ordenescompra_proveedor_id_foreign');
            
        });
    }
}
