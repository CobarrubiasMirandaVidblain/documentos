<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDtllPasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('dtll_pasajeros', function(Blueprint $table){
        $table->foreign('tiposervicio_id')->references('id')->on('dtll_cat_tiposervicios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('tipopasajero_id')->references('id')->on('dtll_cat_tipospasajeros')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('lugarPago_id')->references('id')->on('dtll_cat_lugarespago')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
        // $table->foreign('pais_id')->references('id')->on('cat_paises')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('dtll_pasajeros', function(Blueprint $table){
        $table->dropForeign(['usuario_id']);
        // $table->dropForeign(['pais_id']);
        $table->dropForeign(['persona_id']);
        $table->dropForeign(['lugarPago_id']);
        $table->dropForeign(['tipopasajero_id']);
        $table->dropForeign(['tiposervicio_id']);
      });
    }
}
