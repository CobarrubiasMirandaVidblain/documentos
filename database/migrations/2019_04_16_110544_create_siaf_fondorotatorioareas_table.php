<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class CreateSiafFondorotatorioAreasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('siaf_fondorotatorioareas', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('area_id');
      $table->decimal('techo_presupuestal',20,6);

      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('siaf_fondorotatorioareas');
  }
}
