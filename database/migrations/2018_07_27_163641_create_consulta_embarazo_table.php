<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaEmbarazoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('consulta_embarazo', function (Blueprint $table) {
        $table->integer('consulta_id')->unsigned();
        $table->tinyInteger('trimestre_gestacional');
        $table->tinyInteger('primer_embarazo')->nullable();
        $table->tinyInteger('acido_folico')->nullable();
        $table->string('anticonceptivo',50)->nullable();
        $table->string('terapia_hornomal',50)->nullable();
        $table->string('descanso_puerperio',50)->nullable();
        $table->string('riesgo_de',100)->nullable();
        $table->tinyInteger('infeccion_urinaria')->nullable();
        $table->tinyInteger('hemorragia')->nullable();
        $table->tinyInteger('apoyo_traslado')->nullable();
        $table->string('resultado_analisis_clinicos')->nullable();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulta_embarazo');
    }
}
