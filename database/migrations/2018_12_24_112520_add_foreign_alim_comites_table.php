<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimComitesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_comites', function (Blueprint $table) {
      $table->foreign('persona_id')->references('id')->on('personas');
      $table->foreign('cargo_id')->references('id')->on('cat_cargos');
      $table->foreign('programacion_id')->references('id')->on('alim_programacion');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_comites', function (Blueprint $table) {
      $table->dropForeign(['persona_id']);
      $table->dropForeign(['cargo_id']);
      $table->dropForeign(['programacion_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_comites_persona_id_foreign');
      $table->dropIndex('alim_comites_cargo_id_foreign');
      $table->dropIndex('alim_comites_programacion_id_foreign');
      $table->dropIndex('alim_comites_usuario_id_foreign');
    });
  }
}
