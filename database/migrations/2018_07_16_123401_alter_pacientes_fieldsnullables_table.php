<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPacientesFieldsnullablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function (Blueprint $table){
          $table->integer('seguro_id')->unsigned()->nullable()->change();
          $table->string('folioseguro',100)->nullable()->change();
          $table->string('rfc',15)->nullable()->change();
          $table->integer('entidad_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('pacientes', function (Blueprint $table){
        $table->integer('seguro_id')->unsigned()->change();
        $table->string('folioseguro')->change();
        $table->string('rfc')->change();
        $table->integer('entidad_id')->unsigned()->change();
      });
    }
}
