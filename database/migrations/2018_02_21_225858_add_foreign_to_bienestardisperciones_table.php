<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToBienestardispercionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bienestardispersiones', function (Blueprint $table) {
            $table->foreign('personas_programa_id')->references('id')->on('personas_programas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bienestardispersiones', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['personas_programa_id']);
        });
    }
}
