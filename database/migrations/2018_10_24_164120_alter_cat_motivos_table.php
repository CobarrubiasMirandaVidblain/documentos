<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jerza 
  MOTIVO: Se presentó en la necesidad en el desarrollo del sistema Peticiones, se modificó para poderse usar la tabla motivo_programas
  ALCANCE: Se modificó la tabla para todos
*/

class AlterCatMotivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cat_motivos', function (Blueprint $table) {
        $table->dropColumn('tipo');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cat_motivos', function (Blueprint $table) {
        $table->string('tipo')->after('motivo')->nullable();
      });

      // Respaldamos los datos de la tabla cat_motivos
      DB::statement('UPDATE cat_motivos SET tipo=(SELECT p.nombre FROM motivos_programas mp, programas p WHERE mp.motivo_id=cat_motivos.id AND mp.programa_id=p.id)');

      Schema::table('cat_motivos', function (Blueprint $table) {
        $table->string('tipo')->change();
      });
    }
}
