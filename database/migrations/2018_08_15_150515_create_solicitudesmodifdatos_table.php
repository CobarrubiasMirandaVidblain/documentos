<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesmodifdatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('solicitudesmodifdatos', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('persona_id')->unsigned();
        $table->string('motivo');
        $table->date('fecha_solicitud');
        $table->string('ruta');
        $table->integer('status_solicitud_id')->unsigned();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('solicitudesmodifdatos');
    }
}
