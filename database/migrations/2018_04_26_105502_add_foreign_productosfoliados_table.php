<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignProductosfoliadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productosfoliados', function (Blueprint $table) {
            $table->foreign('detallesentradas_producto_id')->references('id')->on('detallesentradas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('detallessalidas_producto_id')->references('id')->on('detallessalidas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productosfoliados', function (Blueprint $table) {
            $table->dropForeign(["detallesentradas_producto_id"]);
            $table->dropForeign(["detallessalidas_producto_id"]);
        });
    }
}
