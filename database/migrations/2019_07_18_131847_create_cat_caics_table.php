<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para tener un catálogo de caics
    ALCANCE: General
*/

class CreateCatCaicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_caics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('categoria',10);

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['nombre']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_caics');
    }
}
