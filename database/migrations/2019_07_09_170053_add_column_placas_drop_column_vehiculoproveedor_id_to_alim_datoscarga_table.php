<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para el ingreso de una entrega de proveedor
    ALCANCE: Alimentarios
*/

class AddColumnPlacasDropColumnVehiculoproveedorIdToAlimDatoscargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Borramos columna foránea vehiculoproveedor_id
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->dropForeign(['vehiculoproveedor_id']);
            $table->dropIndex('alim_datoscarga_vehiculoproveedor_id_foreign');
        });
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->dropColumn('vehiculoproveedor_id');
        });

        // Agregamos la columna placas
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->string('placas',10)->default('')->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Borramos la columna placas
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->dropColumn('placas');
        });

        // Agregamos una columna foránea vehiculoproveedor_id
        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->unsignedInteger('vehiculoproveedor_id')->after('id');
        });
        
        $registro = DB::table('vehiculos')->first();
        if ($registro)
            DB::table('alim_datoscarga')->update(['vehiculoproveedor_id'=>$registro->id]);

        Schema::table('alim_datoscarga', function (Blueprint $table) {
            $table->foreign('vehiculoproveedor_id')->references('id')->on('vehiculos');
        });
        
    }
}
