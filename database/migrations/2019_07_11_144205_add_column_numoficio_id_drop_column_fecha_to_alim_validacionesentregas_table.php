<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para ingresar los datos de entrega de los proveedores
    ALCANCE: Alimentarios
*/

class AddColumnNumoficioIdDropColumnFechaToAlimValidacionesentregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_validacionesentregas', function (Blueprint $table) {
            $table->unsignedInteger('oficiovalidacion_id')->after('entregaproveedor_id');
            $table->dropColumn('fecha');
            $table->dropColumn('num_oficio');
        });

        Schema::table('alim_validacionesentregas', function(Blueprint $table){
            $table->foreign('oficiovalidacion_id')->references('id')->on('alim_oficiosvalidacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_validacionesentregas', function(Blueprint $table){
            $table->dropForeign(['oficiovalidacion_id']);
            $table->dropIndex('alim_validacionesentregas_oficiovalidacion_id_foreign');
        });

        Schema::table('alim_validacionesentregas', function (Blueprint $table) {
            $table->dropColumn('oficiovalidacion_id');
            $table->date('fecha')->after('entregaproveedor_id');
            $table->string('num_oficio',20)->after('fecha');
        });
    }
}
