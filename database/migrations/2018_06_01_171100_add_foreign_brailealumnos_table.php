<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignBrailealumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brailealumnos', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('discapacidad_id')->references('id')->on('cat_discapacidades')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brailealumnos', function (Blueprint $table) {
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["discapacidad_id"]);
        });
    }
}
