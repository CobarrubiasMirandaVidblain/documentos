<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar la comprobacion con el folio de caja
    ALCANCE: BancaDIF
*/
class AddForeignSiafComprobacionCaja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobacion_caja', function (Blueprint $table) {
            $table->foreign('comprobacion_id')->references('id')->on('siaf_comprobaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobacion_caja', function (Blueprint $table) {
            $table->dropForeign(['comprobacion_id']);
            $table->dropIndex('siaf_comprobacion_caja_comprobacion_id_foreign');
        });
    }
}
