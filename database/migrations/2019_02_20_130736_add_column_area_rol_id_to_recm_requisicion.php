<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega la columna area_rol_id
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnAreaRolIdToRecmRequisicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->unsignedInteger('area_rol_id')->after('usuario_id');
        });

        $registro = DB::table('recm_area_rols')->first();
        if ($registro)
          DB::table('recm_requisicions')->update(['area_rol_id'=>$registro->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions',function(Blueprint $table){
            $table->dropColumn('area_rol_id');
        });
    }
}
