<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los presidentes municipales.
    ALCANCE: General
*/

class CreatePresidentesmunicipalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presidentesmunicipales', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre',30)->nullable();
            $table->string('primer_apellido',40)->nullable();
            $table->string('segundo_apellido',40)->nullable();
            $table->unsignedInteger('cargo_id');
            $table->string('telefono_particular',60)->nullable();
            $table->string('telefono_fijo',85)->nullable();
            $table->unsignedInteger('metodo_eleccion_id')->nullable();
            $table->string('mayoria_relativa')->nullable();
            $table->string('periodo',10)->nullable();
            $table->unsignedInteger('municipio_id')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presidentesmunicipales');
    }
}
