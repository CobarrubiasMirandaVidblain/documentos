<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeGruposprofesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_GruposProfesores', function(Blueprint $table){
        $table->foreign('grupo_horario_id')->references('id')->on('vale_GruposHorarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('profesor_id')->references('id')->on('vale_Profesores')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_GruposProfesores', function(Blueprint $table){
        $table->dropForeign(['grupo_horario_id']);
        $table->dropForeign(['profesor_id']);
      });
    }
}
