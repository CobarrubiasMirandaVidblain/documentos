<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requisiciones', function (Blueprint $table) {
            $table->foreign('autoriza_id')->references('id')->on('autorizaciones')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('partida_id')->references('id')->on('partidas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requisiciones', function (Blueprint $table) {
            $table->dropForeign(['autoriza_id']);
            $table->dropForeign(['partida_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
