<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para guardar el numero de tramite que se genera en la base de slqserver.
    ALCANCE: SIAF
*/

class AddColunmNoTramiteToSiafComprobacionesCaja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobacion_caja', function (Blueprint $table) {
            $table->unsignedInteger('no_tramite')->nullable()->after('monto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobacion_caja', function (Blueprint $table) {
            $table->dropColumn('no_tramite');
        });
    }
}
