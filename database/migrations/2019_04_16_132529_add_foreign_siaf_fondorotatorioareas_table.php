<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafFondorotatorioareasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_fondorotatorioareas', function (Blueprint $table) {
      $table->foreign('area_id')->references('id')->on('cat_areas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_fondorotatorioareas', function (Blueprint $table) {
      $table->dropForeign(['area_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_fondorotatorioareas_area_id_foreign');
      $table->dropIndex('siaf_fondorotatorioareas_usuario_id_foreign');
    });
  }
}
