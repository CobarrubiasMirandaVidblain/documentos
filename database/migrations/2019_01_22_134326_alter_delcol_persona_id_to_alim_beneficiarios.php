<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de alimentarios
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AlterDelcolPersonaIdToAlimBeneficiarios extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_beneficiarios', function (Blueprint $table) {
      $table->dropForeign(['persona_id']);
      $table->dropIndex('alim_beneficiarios_persona_id_foreign');
      $table->dropColumn('persona_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_beneficiarios', function (Blueprint $table) {
      $table->unsignedInteger('persona_id')->after('persona_programa_id');
    });

    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_beneficiarios', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que se va a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_beneficiarios')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_beneficiarios SET persona_id=(SELECT MIN(id) FROM personas)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
