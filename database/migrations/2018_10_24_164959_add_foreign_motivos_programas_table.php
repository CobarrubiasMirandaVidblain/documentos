<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jerza 
  MOTIVO: Se presentó en la necesidad en el desarrollo del sistema Peticiones, se modificó para poderse usar la tabla motivo_programas
  ALCANCE: para todos
*/

class AddForeignMotivosProgramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('motivos_programas', function (Blueprint $table) {
      $table->foreign('motivo_id')->references('id')->on('cat_motivos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('motivos_programas', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['motivo_id']);

      $table->dropIndex('motivos_programas_usuario_id_foreign');
      $table->dropIndex('motivos_programas_programa_id_foreign');
      $table->dropIndex('motivos_programas_motivo_id_foreign');
    });
  }
}
