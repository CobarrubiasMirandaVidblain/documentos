<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimLicitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_licitaciones', function (Blueprint $table) {
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_licitaciones', function (Blueprint $table) {
            $table->dropForeign(['ejercicio_id']);
            $table->dropForeign(['proveedor_id']);
            $table->dropForeign(['usuario_id']);
            // $table->dropIndex('alim_licitaciones_ejercicio_id_foreign'); Ya no es necesario por el unique
            $table->dropIndex('alim_licitaciones_proveedor_id_foreign');
            $table->dropIndex('alim_licitaciones_usuario_id_foreign');
        });
    }
}
