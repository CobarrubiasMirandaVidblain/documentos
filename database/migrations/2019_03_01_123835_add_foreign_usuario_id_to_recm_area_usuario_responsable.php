<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agregan relaciones a la tabla
  ALCANCE: para eñ sistema de recursos materiales
*/

class AddForeignUsuarioIdToRecmAreaUsuarioResponsable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_area_usuario_responsable', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');            
            $table->foreign('area_id')->references('id')->on('cat_areas');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_area_usuario_responsable', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['area_id']);
            $table->dropIndex('recm_area_usuario_responsable_usuario_id_foreign');
            $table->dropIndex('recm_area_usuario_responsable_area_id_foreign');
        });
    }
}
