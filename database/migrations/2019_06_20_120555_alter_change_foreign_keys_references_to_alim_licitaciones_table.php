<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Corrección relación de llaves foráneas
    ALCANCE: Alimentarios
*/

class AlterChangeForeignKeysReferencesToAlimLicitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Desligamos
        Schema::table('alim_licitaciones', function (Blueprint $table) {
            $table->dropForeign(['ejercicio_id']);
            $table->dropForeign(['proveedor_id']);
            $table->dropIndex('alim_licitaciones_proveedor_id_foreign');
            $table->dropUnique(['ejercicio_id','proveedor_id']);
        });

        //Cambiamos el id si es que hay datos en la tabla alim_licitaciones
        $alim_licitaciones = DB::table('alim_licitaciones')->first();
        if ($alim_licitaciones) {
            $modulos_proveedores = DB::table('modulos_proveedores')->first();
            if ($modulos_proveedores)
                DB::table('alim_licitaciones')->update(['proveedor_id'=>$modulos_proveedores->id]);
            else {
                $recm_cat_proveedors = DB::table('recm_cat_proveedors')->first();
                $cat_modulos = DB::table('cat_modulos')->where('nombre','=','ASISTENCIA ALIMENTARIA')->first();
                if ($recm_cat_proveedors){
                    $modulos_proveedores_id = DB::table('modulos_proveedores')->insertGetId([
                                                'proveedor_id'=>$recm_cat_proveedors->id, 
                                                'modulo_id'=>$cat_modulos->id, 
                                                'created_at'=>NOW(), 'updated_at'=>NOW()
                                            ]);
                    DB::table('alim_licitaciones')->update(['proveedor_id'=>$modulos_proveedores_id]);
                }
            }
        }

        // Relacionamos
        Schema::table('alim_licitaciones', function (Blueprint $table) {
            $table->renameColumn('proveedor_id', 'modulo_proveedor_id');
            $table->unique(['ejercicio_id','modulo_proveedor_id']);
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('modulo_proveedor_id')->references('id')->on('modulos_proveedores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Desligamos
        Schema::table('alim_licitaciones', function (Blueprint $table) {
            $table->dropForeign(['modulo_proveedor_id']);
            $table->dropForeign(['ejercicio_id']);
            $table->dropIndex('alim_licitaciones_modulo_proveedor_id_foreign');
            $table->dropUnique(['ejercicio_id','modulo_proveedor_id']);
        });

        // Relacionamos
        Schema::table('alim_licitaciones', function (Blueprint $table) {
            $table->renameColumn('modulo_proveedor_id', 'proveedor_id');
            $table->unique(['ejercicio_id','proveedor_id']);
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
        });
    }
}
