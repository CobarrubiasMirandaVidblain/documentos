<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para ingresar los datos de entrega de los proveedores
    ALCANCE: Alimentarios
*/

class AddColumnEntregaRechazadaToAlimValidacionesentregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_validacionesentregas', function (Blueprint $table) {
            $table->boolean('validacion_aprobada')->after('oficiovalidacion_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_validacionesentregas', function (Blueprint $table) {
            $table->dropColumn('validacion_aprobada');
        });
    }
}
