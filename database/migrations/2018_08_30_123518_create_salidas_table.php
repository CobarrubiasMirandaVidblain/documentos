<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invc_salidas', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('egreso');
            $table->string('folio');
            $table->string('destino');
            $table->string('localidad')->nullable();
            $table->string('solicitante');
            $table->string('vehiculo')->nullable();
            $table->string('conductor')->nullable();
            $table->string('numero_oficio')->nullable();
            $table->string('puesto_solicitante')->nullable();
            $table->string('ine_solicitante')->nullable();
            $table->text('observaciones')->nullable();
            $table->unsignedInteger('bodega_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invc_salidas');
    }
}