<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSolicitudesPersonasColsnullsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('solicitudes_personas', function(Blueprint $table)
    {
      $table->boolean('entregado')->nullable()->change();
      $table->string('observacionentrega')->nullable()->change();
      $table->boolean('evaluado')->nullable()->change();
      $table->string('observacionevaluado')->nullable()->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('solicitudes_personas', function(Blueprint $table)
    {
      $table->boolean('entregado')->change();
      $table->string('observacionentrega')->change();
      $table->boolean('evaluado')->change();
      $table->string('observacionevaluado')->change();
    });
  }
}
