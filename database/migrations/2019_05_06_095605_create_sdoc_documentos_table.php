<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SICODOC
  ALCANCE: Para el sistema SICODOC
*/

class CreateSdocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('progresivo');
            $table->datetime('fechaentrada');
            $table->unsignedInteger('remitente_id');
            $table->unsignedInteger('destinatario_id');
            $table->unsignedInteger('tipodocumento_id');
            $table->string('numerodocumento');
            $table->string('asunto')->nullable();
            $table->string('contenido')->nullable();
            $table->string('observaciones')->nullable();
            $table->datetime('fechavencimiento')->nullable();
            $table->unsignedInteger('estatus_id')->default(1);
            $table->string('folio')->nullable();
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_documentos');
    }
}
