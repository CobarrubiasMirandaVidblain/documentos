<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar las asignaciones con los pagos, solo para las asignaciones de tipo gastos a comprobar
    ALCANCE: fondorotatorio
*/

class CreateAsignacionesPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_asignaciones_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('asignacion_id');
            $table->unsignedInteger('pago_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_asignaciones_pagos');
    }
}
