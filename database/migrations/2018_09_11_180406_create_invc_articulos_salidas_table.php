<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvcArticulosSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invc_articulos_salidas', function (Blueprint $table) {
            $table->integer('cantidad');
            $table->unsignedInteger('salida_id');
            $table->unsignedInteger('bodega_id');
            $table->unsignedInteger('articulo_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invc_articulos_salidas');
    }
}
