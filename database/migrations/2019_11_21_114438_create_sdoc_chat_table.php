<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para almacenar la conversación que se tiene acerca de la respuesta sin oficio
    ALCANCE: SICODOC
*/

class CreateSdocChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_chat', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('seguimiento_documento_id');
            $table->unsignedInteger('area_id');
            $table->text('texto');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_chat');
    }
}
