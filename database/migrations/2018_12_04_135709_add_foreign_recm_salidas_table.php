<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_salidas
        Schema::table('recm_salidas', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('requisicion_id')->references('id')->on('recm_requisicions')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_salidas', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['requisicion_id']);
            $table->dropIndex('recm_salidas_usuario_id_foreign');
            $table->dropIndex('recm_salidas_requisicion_id_foreign');
            
        });
    }
}
