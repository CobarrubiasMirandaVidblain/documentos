<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AddForeignEvenFichasinfoTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_fichasinfo', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('even_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('vestimenta_id')->references('id')->on('even_vestimenta')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('tipoevento_id')->references('id')->on('even_cat_tiposevento')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('persona_convoca_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('cargo_persona_convoca_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('persona_preside_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('cargo_persona_preside_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('presidenta_participacion_id')->references('id')->on('even_cat_participaciones')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('director_participacion_id')->references('id')->on('even_cat_participaciones')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_fichasinfo', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropForeign(['vestimenta_id']);
      $table->dropForeign(['tipoevento_id']);
      $table->dropForeign(['persona_convoca_id']);
      $table->dropForeign(['cargo_persona_convoca_id']);
      $table->dropForeign(['persona_preside_id']);
      $table->dropForeign(['cargo_persona_preside_id']);
      $table->dropForeign(['presidenta_participacion_id']);
      $table->dropForeign(['director_participacion_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('even_fichasinfo_evento_id_foreign');
      $table->dropIndex('even_fichasinfo_vestimenta_id_foreign');
      $table->dropIndex('even_fichasinfo_tipoevento_id_foreign');
      $table->dropIndex('even_fichasinfo_persona_convoca_id_foreign');
      $table->dropIndex('even_fichasinfo_cargo_persona_convoca_id_foreign');
      $table->dropIndex('even_fichasinfo_persona_preside_id_foreign');
      $table->dropIndex('even_fichasinfo_cargo_persona_preside_id_foreign');
      $table->dropIndex('even_fichasinfo_presidenta_participacion_id_foreign');
      $table->dropIndex('even_fichasinfo_director_participacion_id_foreign');
      $table->dropIndex('even_fichasinfo_usuario_id_foreign');
    });
  }
}
