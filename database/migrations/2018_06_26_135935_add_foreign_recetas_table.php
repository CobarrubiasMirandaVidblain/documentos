<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recetas', function (Blueprint $table) {
            $table->foreign('consulta_id')->references('id')->on('consultas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('medicamento_id')->references('id')->on('cat_medicamentos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recetas', function (Blueprint $table) {
            $table->dropForeign(["consulta_id"]);
            $table->dropForeign(["medicamento_id"]);
        });
    }
}
