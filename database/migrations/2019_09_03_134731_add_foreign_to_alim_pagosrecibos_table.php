<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para capturar en alim_pagosrecibos los datos del pago de recibo y ya no capturarlos en la tabla alim_recibos
    ALCANCE: Alimentarios y Caja
*/

class AddForeignToAlimPagosrecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_pagosrecibos', function (Blueprint $table) {
            $table->foreign('recibo_id')->references('id')->on('alim_recibos');
            $table->foreign('cancela_usuario_id')->references('id')->on('usuarios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_pagosrecibos', function (Blueprint $table) {
            $table->dropForeign(['recibo_id']);
            $table->dropForeign(['cancela_usuario_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_pagosrecibo_recibo_id_foreign');
            $table->dropIndex('alim_pagosrecibo_cancela_usuario_id_foreign');
            $table->dropIndex('alim_pagosrecibo_usuario_id_foreign');
        });
    }
}
