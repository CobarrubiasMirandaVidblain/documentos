<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para llevar el registro de números de folio de oficios por año y por área
    ALCANCE: SICODOC
*/

class AddColumnEjercicioIdAreaIdToSdocFoliodisponibleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Agregamos los campos
        Schema::table('sdoc_foliodisponible', function (Blueprint $table) {
            $table->unsignedInteger('ejercicio_id')->after('numero_oficio');
            $table->unsignedInteger('area_id')->after('ejercicio_id');
        });
        // Asignamos un valor default por si ya hay registros
        $registro = DB::table('cat_ejercicios')->first();
        if ($registro)
            DB::table('sdoc_foliodisponible')->update(['ejercicio_id'=>$registro->id]);
        $registro = DB::table('cat_areas')->first();
        if ($registro)
            DB::table('sdoc_foliodisponible')->update(['area_id'=>$registro->id]);
        // Creamos la relación
        Schema::table('sdoc_foliodisponible', function (Blueprint $table) {
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('area_id')->references('id')->on('cat_areas');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_foliodisponible', function (Blueprint $table) {
            $table->dropForeign(['ejercicio_id']);
            $table->dropForeign(['area_id']);
            $table->dropIndex('sdoc_foliodisponible_ejercicio_id_foreign');
            $table->dropIndex('sdoc_foliodisponible_area_id_foreign');
        });
        Schema::table('sdoc_foliodisponible', function (Blueprint $table) {
            $table->dropColumn('ejercicio_id');
            $table->dropColumn('area_id');
        });
    }
}
