<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimCancelacionesrecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_cancelacionesrecibos', function (Blueprint $table) {
            $table->foreign('recibo_id')->references('id')->on('alim_recibos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_cancelacionesrecibos', function (Blueprint $table) {
            $table->dropForeign(['recibo_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_cancelacionesrecibos_recibo_id_foreign');
            $table->dropIndex('alim_cancelacionesrecibos_usuario_id_foreign');
        });
    }
}
