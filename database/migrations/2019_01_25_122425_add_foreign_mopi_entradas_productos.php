<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignMopiEntradasProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_entradas_productos', function (Blueprint $table) {
            $table->foreign('entrada_id')->references('id')->on('mopi_entradas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_producto_id')->references('id')->on('mopi_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_entradas_productos', function (Blueprint $table) {
            $table->dropForeign(['entrada_id']);
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropIndex('mopi_entradas_productos_presentacion_producto_id_foreign');
            $table->dropIndex('mopi_entradas_productos_entrada_id_foreign');
        });
    }
}
