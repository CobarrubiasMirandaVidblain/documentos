<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_respuestas_documentos
  ALCANCE: Para el sistema SICODOC
*/


class AddForeignSdocRespuestasDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
  {
    Schema::table('sdoc_respuestas_documentos', function (Blueprint $table) {
      $table->foreign('documento_id')->references('id')->on('sdoc_documentos');
      $table->foreign('documentorespuesta_id')->references('id')->on('sdoc_documentos');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('sdoc_respuestas_documentos', function (Blueprint $table) {
      $table->dropForeign(['documento_id']);
      $table->dropForeign(['documentorespuesta_id']);
      $table->dropForeign(['usuario_id']);
      
      $table->dropIndex('sdoc_respuestas_documentos_documento_id_foreign');
      $table->dropIndex('sdoc_respuestas_documentos_documentorespuesta_id_foreign');
      $table->dropIndex('sdoc_respuestas_documentos_usuario_id_foreign');    
    });
  }
}
