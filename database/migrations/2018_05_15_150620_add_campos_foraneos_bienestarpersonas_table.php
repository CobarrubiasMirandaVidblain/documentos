<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCamposForaneosBienestarpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bienestarpersonas', function (Blueprint $table) {
            $table->integer('estadocivil_id')->unsigned()->nullable();
            $table->foreign('estadocivil_id')->references('id')->on('cat_estadosciviles')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('ocupacion_id')->unsigned()->nullable();
            $table->foreign('ocupacion_id')->references('id')->on('cat_ocupaciones')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->integer('escolaridad_id')->unsigned()->nullable();
            $table->foreign('escolaridad_id')->references('id')->on('cat_escolaridades')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bienestarpersonas', function (Blueprint $table) {
            $table->dropForeign(["estadocivil_id"]);
            $table->dropForeign(["ocupacion_id"]);
            $table->dropForeign(["escolaridad_id"]);
        });
    }
}
