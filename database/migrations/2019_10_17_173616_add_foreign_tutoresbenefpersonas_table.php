<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de tutores de una persona que recibe un beneficio
    ALCANCE: General
*/

class AddForeignTutoresbenefpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutoresbenefpersonas', function (Blueprint $table) {
            $table->foreign('tutor_persona_id')->references('id')->on('personas');	
            $table->foreign('tutorado_beneficioprog_persona_id')->references('id')->on('beneficiosprog_personas');	
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutoresbenefpersonas', function (Blueprint $table) {
            $table->dropForeign(['tutor_persona_id']);
            $table->dropForeign(['tutorado_beneficioprog_persona_id']);
            $table->dropForeign(['usuario_id']);
						$table->dropIndex('tutoresbenefpersonas_tutor_persona_id_foreign');
						$table->dropIndex('tutoresbenefpersonas_tutorado_beneficioprog_persona_id_foreign');
						$table->dropIndex('tutoresbenefpersonas_usuario_id_foreign');
        });
    }
}
