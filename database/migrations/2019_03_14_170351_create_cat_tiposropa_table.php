<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos extra a la tabla personas
  ALCANCE: Para todos los sitemas
*/

class CreateCatTiposropaTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cat_tiposropa', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre');
      $table->timestamps();
      $table->softDeletes();
    });

    DB::table('cat_tiposropa')->insert([
      ['nombre'=>'GRUESA', 'created_at'=>NOW()],
      ['nombre'=>'LIGERA', 'created_at'=>NOW()]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('cat_tiposropa');
  }
}
