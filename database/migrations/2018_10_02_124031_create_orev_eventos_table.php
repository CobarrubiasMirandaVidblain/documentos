<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrevEventosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('orev_eventos', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('tipo_evento_id')->unsigned();
      $table->integer('localidad_id')->unsigned();
      $table->integer('municipio_id')->unsigned();
      $table->integer('status_id')->unsigned();
      
      $table->string('folio',20);
      $table->string('nombreEvento');
      $table->date('fechaEvento');
      $table->string('calle',100);
      $table->string('numExt',10)->nullable();
      $table->string('numInt',10)->nullable();
      $table->string('colonia',100);
      $table->tinyInteger('codigoPostal')->unsigned();
      $table->string('descripcionEvento');

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('orev_eventos');
  }
}
