<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcesosRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procesos_requisiciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('requisicion_id');
            $table->string('estatus_anterior');
            $table->string('estatus_nuevo');
            $table->string('observacion');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procesos_requisiciones');
    }
}
