<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se renombra columna para generar nueva relacion
  ALCANCE: modulo de orgeventos
*/

class RenameColunmUserCapturaToOrevEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            $table->renameColumn('UserCaptura', 'area_id');
            // $table->renameColumn('nombreEvento', 'beneficiosprogramas_id');
            $table->dropColumn('nombreEvento');
            $table->unsignedInteger('beneficiosprogramas_id')->after('folio');
        });
        DB::statement('ALTER TABLE `orev_eventos` MODIFY `area_id` INTEGER UNSIGNED NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            $table->renameColumn('area_id', 'UserCaptura');
            // $table->renameColumn('beneficiosprogramas_id','nombreEvento');
            $table->string('nombreEvento')->after('folio');
            $table->dropColumn('beneficiosprogramas_id');
        });
        // DB::statement('ALTER TABLE `orev_eventos` MODIFY `UserCaptura` INTEGER;');
    }
}
