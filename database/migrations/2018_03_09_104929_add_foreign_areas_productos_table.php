<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAreasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('areas_productos', function (Blueprint $table) {
            $table->integer('programa_id')->unsigned()->after('area_id')->nullable();
            $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('areas_productos', function (Blueprint $table) {
            $table->dropForeign(["programa_id"]);
           
        });
    }
}
