<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenResponsablesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_responsables', function (Blueprint $table) {
      $table->increments('id');
      
      $table->string('nombre',100);
      $table->string('cargo',100)->nullable();
      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['nombre', 'cargo','deleted_at']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_responsables');
  }
}
