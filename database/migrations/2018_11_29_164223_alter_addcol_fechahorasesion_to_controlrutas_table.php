<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian y Jhony
  MOTIVO: Reestructura del funcionamiento del sistema
  ALCANCE: DIF te lleva
*/

class AlterAddcolFechahorasesionToControlrutasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('controlrutas', function (Blueprint $table) {
      $table->datetime('fechahora_sesion_ini')->after('conductor_id');
      $table->datetime('fechahora_sesion_fin')->after('fechahora_sesion_ini');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('controlrutas', function (Blueprint $table) {
      $table->dropColumn('fechahora_sesion_ini');
      $table->dropColumn('fechahora_sesion_fin');
    });
  }
}
