<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega campo de entregar en: por que no siempre es en el mismo lugar
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnEntregaToRecmOrdenescompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->string('entrega')->after('fecha_captura');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra',function(Blueprint $table){
            $table->dropColumn('entrega');
        });
    }
}
