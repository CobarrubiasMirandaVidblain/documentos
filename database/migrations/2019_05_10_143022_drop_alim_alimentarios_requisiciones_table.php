<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAlimAlimentariosRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_alimentarios_requisiciones', function (Blueprint $table) {
            $table->dropForeign(['programacion_id']);
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['estado_programa_id']);
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['usuario_id']);
        });

        Schema::dropIfExists('alim_alimentarios_requisiciones');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('alim_alimentarios_requisiciones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('programacion_id');
            $table->unsignedInteger('requisicion_id');
            $table->unsignedInteger('numero_dotaciones');
            $table->unsignedInteger('numero_oficio_validacion');
            $table->decimal('inversion',12,2);
            $table->unsignedInteger('estado_programa_id');
            $table->unsignedInteger('programa_id');
            $table->unsignedInteger('recibo_id');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('alim_alimentarios_requisiciones', function (Blueprint $table) {
            $table->foreign('programacion_id')->references('id')->on('alim_programacion');
            $table->foreign('requisicion_id')->references('id')->on('alim_requisiciones');
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }
}
