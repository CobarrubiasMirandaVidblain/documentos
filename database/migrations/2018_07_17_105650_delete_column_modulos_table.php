<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if( env('DB_ISADMIN') ){
          $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];
          for($i = 0; $i < count($conexiones); $i++){
            Schema::connection($conexiones[$i])->table('cat_modulos', function (Blueprint $table) {
              $table->dropColumn('url');
            });
          }
        }else{
          Schema::table('cat_modulos', function (Blueprint $table) {
              $table->dropColumn('url');
          });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if( env('DB_ISADMIN') ){
        $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];
        for($i = 0; $i < count($conexiones); $i++){
          Schema::connection($conexiones[$i])->table('cat_modulos', function (Blueprint $table) {
            $table->string('url')->after('prefix');
          });
        }
      }else{
        Schema::table('cat_modulos', function (Blueprint $table) {
          $table->string('url')->after('prefix');
        });
      }
    }
}
