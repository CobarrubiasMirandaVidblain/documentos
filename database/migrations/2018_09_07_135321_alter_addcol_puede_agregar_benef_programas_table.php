<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolPuedeAgregarBenefProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('programas', function(Blueprint $table)
      {
        $table->tinyInteger('puede_agregar_benef')->after('oficial');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('programas',function(Blueprint $table){
        $table->dropColumn('puede_agregar_benef');
      });
    }
}
