<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para que la tabla de alim_desayunos deje de relacionarse con alim_cat_escuelas y ahora se relacione con cat_escuelas
  ALCANCE: Alimentarios
*/

class DropTablesAlimCatEscuelasAndAlimCatNivelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('alim_cat_escuelas');
        Schema::dropIfExists('alim_cat_niveles');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('alim_cat_niveles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('alim_cat_niveles')->insert([
            ['nombre' => 'NO DEFINIDO', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => 'PREESCOLAR', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => 'PRIMARIA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
        ]);

        Schema::create('alim_cat_escuelas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->string('clave',12);
            $table->unsignedInteger('nivel_id');
            $table->tinyInteger('turno')->unsigned();
            $table->unsignedInteger('padre_id')->nullable();
            $table->tinyInteger('extension');
            $table->unsignedInteger('municipio_id');
            $table->unsignedInteger('localidad_id')->nullable();
            $table->string('latitud',50)->nullable();
            $table->string('longitud',50)->nullable();
            $table->tinyInteger('oficial')->unsigned();
            $table->unsignedInteger('usuario_id');
            
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('alim_cat_escuelas', function (Blueprint $table) {
            $table->foreign('nivel_id')->references('id')->on('alim_cat_niveles');
            $table->foreign('padre_id')->references('id')->on('alim_cat_escuelas');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });

        $registro = DB::table('alim_cat_escuelas')->first();
        $registro2 = DB::table('cat_escuelas')->first();
        if (!$registro && $registro2)
            DB::statement('INSERT INTO alim_cat_escuelas (id, nombre, clave, nivel_id, turno, padre_id, extension, municipio_id, localidad_id, latitud, longitud, oficial, usuario_id, updated_at, created_at)
                        SELECT id, nombre, clave, nivel_id, turno_id, padre_id, extension, municipio_id, localidad_id, latitud, longitud, oficial, 91, updated_at, created_at
                        FROM cat_escuelas');
    }
}
