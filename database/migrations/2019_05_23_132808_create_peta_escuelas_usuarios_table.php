<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class CreatePetaEscuelasUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peta_escuelas_usuarios', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('institucion_usuario_id');
            $table->unsignedInteger('escuela_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['institucion_usuario_id','escuela_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peta_escuelas_usuarios');
    }
}
