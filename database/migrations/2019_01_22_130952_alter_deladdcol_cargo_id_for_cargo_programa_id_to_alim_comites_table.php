<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de alimentarios
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AlterDeladdcolCargoIdForCargoProgramaIdToAlimComitesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_comites', function (Blueprint $table) {
      $table->dropForeign(['cargo_id']);
      $table->dropIndex('alim_comites_cargo_id_foreign');
      $table->dropColumn('cargo_id');
      
      $table->unsignedInteger('cargo_programa_id')->after('persona_id');
    });

    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_comites', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que se va a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_comites')->get();
      if ($temp->count()>0){
        $programa = DB::table('programas')->where('nombre','ASISTENCIA ALIMENTARIA')->first();
        if ($programa === null){ //Si no hay un programa lo creamos
          $programa = DB::table('programas')->insert(
            ['nombre' => 'ASISTENCIA ALIMENTARIA', 'activo' => 1, 'descripcion'=>'ALIMENTARIOS', 'tipo'=>'PROGRAMA', 'oficial'=>1, 'puede_agregar_benef'=>0, 'usuario_id'=>1]
          );
        }
        $cargos_programas = DB::table('cargos_programas')->where('programa_id',$programa->id)->count();
        if ($cargos_programas === 0){ //Si no hay cargos, creamos uno por default
          DB::table('cargos_programas')->insert(
            ['cargo_id' => DB::table('cat_cargos')->first()->id, 'programa_id'=>$programa->id, 'created_at'=>NOW()]
          );
        }
        DB::statement('UPDATE alim_comites SET cargo_programa_id=(SELECT MIN(id) FROM cargos_programas)');
      }
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('cargo_programa_id')->references('id')->on('cargos_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_comites', function (Blueprint $table) {
      $table->dropForeign(['cargo_programa_id']);
      $table->dropIndex('alim_comites_cargo_programa_id_foreign');
      $table->dropColumn('cargo_programa_id');
      
      $table->unsignedInteger('cargo_id')->after('persona_id');
    });

    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('alim_comites', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que se va a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('alim_comites')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE alim_comites SET cargo_id=(SELECT MIN(id) FROM cat_cargos)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('cargo_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
