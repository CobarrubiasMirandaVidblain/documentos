<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivosTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if(env('DB_ISADMIN'))
        {
            $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];

            for($i = 0; $i < count($conexiones); $i++)
            {
                Schema::connection($conexiones[$i])->create('archivos', function(Blueprint $table) {
                    $table->increments('id');
                    $table->integer('modulo_id')->unsigned();
                    $table->integer('tablabd_id')->unsigned();
                    $table->integer('registro_num')->unsigned();
                    $table->string('url');
                    $table->string('short_url');
                    $table->timestamps();
                    $table->softDeletes();
                });
            }
        }
        else
        {
            Schema::create('archivos', function(Blueprint $table) {
                $table->increments('id');
                $table->integer('modulo_id')->unsigned();
                $table->integer('tablabd_id')->unsigned();
                $table->integer('registro_num')->unsigned();
                $table->string('url');
                $table->string('short_url');
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if(env('DB_ISADMIN'))
        {
            $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];

            for($i = 0; $i < count($conexiones); $i++)
            {
                Schema::connection($conexiones[$i])->dropIfExists('archivos');
            }
        }
        else Schema::dropIfExists('archivos');
    }
}
