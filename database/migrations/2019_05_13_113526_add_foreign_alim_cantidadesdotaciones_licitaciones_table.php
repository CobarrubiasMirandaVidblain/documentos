<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimCantidadesdotacionesLicitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_cantidadesdotaciones_licitaciones', function (Blueprint $table) {
            $table->foreign('licitacion_id')->references('id')->on('alim_licitaciones');
            $table->foreign('dotacion_id')->references('id')->on('alim_dotaciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_cantidadesdotaciones_licitaciones', function (Blueprint $table) {
            $table->dropForeign(['licitacion_id']);
            $table->dropForeign(['dotacion_id']);
            $table->dropForeign(['usuario_id']);
            // $table->dropIndex('alim_cantidadesdotaciones_licitaciones_licitacion_id_foreign'); // Ya no es necesario por el unique
            $table->dropIndex('alim_cantidadesdotaciones_licitaciones_dotacion_id_foreign');
            $table->dropIndex('alim_cantidadesdotaciones_licitaciones_usuario_id_foreign');
        });
    }
}
