<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Se agrega tabla para ingresar datos de usuarios en alimentarios
  ALCANCE: Para Alimentarios
*/

class CreateAlimRegionesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_regiones_usuarios', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('region_id');
            $table->unsignedInteger('access_usuario_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_regiones_usuarios');
    }
}
