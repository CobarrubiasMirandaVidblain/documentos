<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de requisiciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimRequisiciones2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_requisiciones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('licitacion_id');
            $table->tinyInteger('num_bimestre')->unsigned();
            $table->tinyInteger('num_entrega')->unsigned();
            $table->integer('num_oficio')->unsigned();
            $table->datetime('fecha_oficio');
            $table->tinyInteger('especial')->unsigned()->comment('Se dan a finales de año, para poder darle nuevamente dotación a alguna cocina si hay dotaciones sobrantes');
            $table->unsignedInteger('tipo_entrega_id');
            $table->unsignedInteger('estado_programa_id');
            $table->tinyInteger('periodo')->unsignedInteger()->comment('Número de meses para los que se les distribuye la dotación');
            $table->string('observaciones');

            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['num_oficio']);
            $table->unique(['num_bimestre', 'num_entrega', 'num_oficio'],'alim_requisiciones_ofireq_bim_entrega_del_at_unique'); // Especificamos un nombre porque eloquent crea un nombre muy largo y no lo acepta mysql
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_requisiciones');
    }
}
