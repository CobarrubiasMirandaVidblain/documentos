<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los presidentes municipales.
    ALCANCE: Eventos
*/

class AddForeignPresidentemunicipalIdToEvenAutoridadesmuniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->foreign('presidentemunicipal_id')->references('id')->on('presidentesmunicipales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->dropForeign(['presidentemunicipal_id']);
            $table->dropIndex('even_autoridadesmuni_presidentemunicipal_id_foreign');
        });
    }
}
