<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFolioInternoToRecmOrdenescompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->unsignedInteger('folio_interno')->nullable()->after('proveedor_id'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra',function(Blueprint $table){
            $table->dropColumn('folio_interno');
        });
    }
}
