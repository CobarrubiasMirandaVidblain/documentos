<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSolicitudesPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('solicitudes_personas', function (Blueprint $table) {
            $table->foreign('programas_solicitud_id')->references('id')->on('programas_solicitudes')
                ->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('persona_id')->references('id')->on('personas')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes_personas', function (Blueprint $table) {
            $table->dropForeign(["programas_solicitud_id"]);
            $table->dropForeign(["persona_id"]);
        });
    }
}

