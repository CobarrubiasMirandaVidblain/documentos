<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se elimina foreign keys para tabla instituciones_rubros
  ALCANCE: Base Principal
*/


class DropForeignInstitucionesrubrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('instituciones_rubros', function(Blueprint $table){
        $table->dropForeign(['institucion_id']);
        //$table->dropColumn('institucion_id');
        $table->dropForeign(['rubro_id']);
        //$table->dropColumn('rubro_id');
        $table->dropIndex('instituciones_rubros_institucion_id_foreign');
        $table->dropIndex('instituciones_rubros_rubro_id_foreign');

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('instituciones_rubros', function(Blueprint $table){
        //$table->unsignedInteger('institucion_id')->after('id');
        //$table->unsignedInteger('rubro_id')->after('institucion_id');
        $table->foreign('institucion_id')->references('id')->on('cat_instituciones')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('rubro_id')->references('id')->on('cat_rubros')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }
}
