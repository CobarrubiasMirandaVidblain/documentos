<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para reestringir que se dupliquen los nombres y claves de la escuela, esto para cuando se agreguen no oficiales
  ALCANCE: Para el sistema de alimentarios
*/

class AddUniqueClaveNombreOficialToAlimCatEscuelasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_cat_escuelas', function (Blueprint $table) {
      $table->unique(['clave', 'nombre', 'oficial']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_cat_escuelas', function (Blueprint $table) {
      $table->dropUnique(['clave', 'nombre', 'oficial']);
    });
  }
}
