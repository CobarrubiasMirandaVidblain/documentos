<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para identificar acciones no oficiales a los que pertenecen los beneficiarios de alimentarios
  ALCANCE: modulo de Alimentarios
*/

class CreateAlimTiposaccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_tiposaccion', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programa_id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['programa_id','nombre']);
        });

        $registro = DB::table('programas')->first();
        if ($registro)
            DB::table('alim_tiposaccion')->insert([
                ['programa_id'=>608, 'nombre'=>'ASISTENCIA ALIMENTARIA A SUJETOS VULNERABLES', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>608, 'nombre'=>'ASISTENCIA ALIMENTARIA A SUJETOS VULNERABLES (MUJERES DE 15-35 AÑOS)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>608, 'nombre'=>'ASISTENCIA ALIMENTARIA A SUJETOS VULNERABLES (MUJERES EN EMBARAZO O LACTANCIA)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>608, 'nombre'=>'ATENCIÓN A MENORES DE 5 AÑOS EN RIESGO NO ESCOLARIZADOS (1 AÑO A 5 AÑOS 11 MESES)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>608, 'nombre'=>'ATENCIÓN A MENORES DE 5 AÑOS EN RIESGO NO ESCOLARIZADOS (6 MESES A 11 MESES)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>608, 'nombre'=>'DESAYUNOS ESCOLARES  (6 A 12 AÑOS)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>609, 'nombre'=>'DESAYUNOS ESCOLARES FRIOS', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'DISCAPACITADOS', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'ADULTOS MAYORES(DE 60 A  65 AÑOS)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'MUJERES EMBARAZADAS Y/O LACTANDO', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'DESPLAZADOS', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'MADRES SOLTERAS CON NIÑOS MENORES DE 12 AÑOS', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'NIÑOS Y NIÑAS DE 1 A 12 AÑOS', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'MUJERES EN VIUDEZ', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'ADULTOS MAYORES (60 EN ADELANTE)', 'created_at'=>NOW(), 'updated_at'=>NOW()],
                ['programa_id'=>611, 'nombre'=>'CARENCIA ALIMENTARIA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
            ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_tiposaccion');
    }
}
