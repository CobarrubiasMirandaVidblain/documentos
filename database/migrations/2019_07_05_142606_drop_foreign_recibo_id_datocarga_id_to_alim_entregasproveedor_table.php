<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para el ingreso de una entrega de proveedor
    ALCANCE: Alimentarios
*/

class DropForeignReciboIdDatocargaIdToAlimEntregasproveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Eliminamos la relación
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropForeign(['recibo_id']);
            $table->dropForeign(['datocarga_id']);
            $table->dropIndex('alim_entregasproveedor_recibo_id_foreign');
            $table->dropIndex('alim_entregasproveedor_datocarga_id_foreign');
        });
        //Eliminamos las columnas
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropColumn('recibo_id');
            $table->dropColumn('datocarga_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Creamos los campos
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->unsignedInteger('recibo_id');
            $table->unsignedInteger('datocarga_id');
        });
        //Actualizamos el registro para poder relacionar
        $registro = DB::table('alim_recibos')->first();
        if ($registro)
            DB::table('alim_entregasproveedor')->update(['recibo_id'=>$registro->id]);
        
        $registro = DB::table('alim_datoscarga')->first();
        if ($registro)
            DB::table('alim_entregasproveedor')->update(['datocarga_id'=>$registro->id]);
        //Relacionamos
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->foreign('recibo_id')->references('id')->on('alim_recibos');
            $table->foreign('datocarga_id')->references('id')->on('alim_datoscarga');
        });
    }
}
