<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar la asignacion con un tipo
    ALCANCE: fondorotatorio
*/

class AddForeignTipoAsignacionToSiafAsignaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_asignaciones', function (Blueprint $table) {
            $table->foreign('tipo_asignacion_id')->references('id')->on('siaf_cat_tipos_asignaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_asignaciones', function (Blueprint $table) {
            $table->dropForeign(['tipo_asignacion_id']);
            $table->dropIndex('siaf_asignaciones_tipo_asignacion_id_foreign');
        });
    }
}
