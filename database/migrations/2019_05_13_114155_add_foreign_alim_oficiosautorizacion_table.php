<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimOficiosautorizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->foreign('tipo_oficio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('region_id')->references('id')->on('proveedores');
            $table->foreign('dotacion_id')->references('id')->on('proveedores');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->dropForeign(['tipo_oficio_id']);
            $table->dropForeign(['region_id']);
            $table->dropForeign(['dotacion_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_oficiosautorizacion_tipo_oficio_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_region_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_dotacion_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_usuario_id_foreign');
        });
    }
}
