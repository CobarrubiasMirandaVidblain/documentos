<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValeProefesoresgruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vale_proefesoresgrupos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('grupodisponible_id')->unsigned();
            $table->integer('empleado_id')->unsigned();
            $table->boolean('activo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vale_proefesoresgrupos');
    }
}
