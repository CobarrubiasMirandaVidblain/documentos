<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatTablasbdTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if(env('DB_ISADMIN'))
        {
            $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];

            for($i = 0; $i < count($conexiones); $i++)
            {
                Schema::connection($conexiones[$i])->create('cat_tablasbd', function(Blueprint $table) {
                    $table->increments('id');
                    $table->string('nombre', 70);
                    $table->timestamps();
                    $table->softDeletes();
                });
            }
        }
        else
        {
            Schema::create('cat_tablasbd', function(Blueprint $table) {
                $table->increments('id');
                $table->string('nombre', 70);
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    function up2($table)
    {
        $table->increments('id');
        $table->string('nombre', 70);
        $table->timestamps();
        $table->softDeletes();
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if(env('DB_ISADMIN'))
        {
            $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];

            for($i = 0; $i < count($conexiones); $i++)
            {
                Schema::connection($conexiones[$i])->dropIfExists('cat_tablasbd');
            }
        }
        else Schema::dropIfExists('cat_tablasbd');
    }
}
