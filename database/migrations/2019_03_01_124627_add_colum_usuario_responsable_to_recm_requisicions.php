<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agregan columna para relacionar al usuario de rm que atendera esa requisicion
  ALCANCE: para el sistema de requisiciones recmat
*/

class AddColumUsuarioResponsableToRecmRequisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->unsignedInteger('usuario_responsable')->after('area_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions',function(Blueprint $table){
            $table->dropColumn('usuario_responsable');
        });
    }
}
