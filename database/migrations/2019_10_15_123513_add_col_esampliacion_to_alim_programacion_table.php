<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para indicar si una programación es ampliación de una cocina
    ALCANCE: Alimentarios
*/

class AddColEsampliacionToAlimProgramacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_programacion', function (Blueprint $table) {
            $table->boolean('esampliacion')->unsigned()->default(0)->after('alimentario_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_programacion', function (Blueprint $table) {
            $table->dropColumn('esampliacion');
        });
    }
}
