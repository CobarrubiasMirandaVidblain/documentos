<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignGruposnivelesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gruposniveles', function (Blueprint $table) {
            $table->integer('anios_programa_id')->unsigned()->nullable();
            $table->integer('padre_id')->unsigned()->nullable();
            $table->foreign('anios_programa_id')->references('id')->on('anios_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('padre_id')->references('id')->on('gruposniveles')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gruposniveles', function (Blueprint $table) {
            $table->dropForeign(["anios_programa_id"]);
            $table->dropForeign(["padre_id"]);
        });
    }
}
