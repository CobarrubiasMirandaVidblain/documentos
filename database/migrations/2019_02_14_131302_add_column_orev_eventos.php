<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO:
  ALCANCE:
*/

class AddColumnOrevEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            $table->integer('userCaptura')->nullable()->after('descripcionEvento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orev_eventos',function(Blueprint $table){
            $table->dropColumn('userCaptura');
        });
    }
}
