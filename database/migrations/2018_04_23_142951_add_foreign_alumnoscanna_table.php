<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlumnoscannaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alumnoscanna', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('gruponivel_id')->references('id')->on('gruposniveles')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alumnoscanna', function (Blueprint $table) {
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["gruponivel_id"]);
        });
    }
}
