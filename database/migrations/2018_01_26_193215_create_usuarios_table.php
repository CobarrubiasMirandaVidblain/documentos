<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario');
            $table->integer('persona_id')->unsigned();
            $table->string('password');
            $table->string('email')->unique()->nullable();
            $table->string('token')->nullable();
            $table->string('remember_token')->nullable();
            $table->dateTime('vida_token');
            $table->boolean('autorizado');
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
