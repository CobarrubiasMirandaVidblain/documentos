<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimRequisicionesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_requisiciones', function (Blueprint $table) {
      $table->foreign('estado_id')->references('id')->on('alim_cat_estados');
      $table->foreign('tiporequisicion_id')->references('id')->on('alim_cat_tiposrequisicion');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_requisiciones', function (Blueprint $table) {
      $table->dropForeign(['estado_id']);
      $table->dropForeign(['tiporequisicion_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_requisiciones_estado_id_foreign');
      $table->dropIndex('alim_requisiciones_tiporequisicion_id_foreign');
      $table->dropIndex('alim_requisiciones_usuario_id_foreign');
    });
  }
}
