<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para identificar acciones no oficiales a los que pertenecen los beneficiarios de alimentarios
  ALCANCE: modulo de Alimentarios
*/

class AddForeignTipoaccionIdToAlimBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->foreign('tipoaccion_id')->references('id')->on('alim_tiposaccion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->dropForeign(['tipoaccion_id']);
            $table->dropIndex('alim_beneficiarios_tipoaccion_id_foreign');  
        });
    }
}
