<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar la clave de inegi basada al momento en el iter2010
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AlterAddcolCveinegiToCatLocalidadesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('cat_localidades', function (Blueprint $table) {
      $table->string('cve_localidad',4)->after('longitud')->nullable()->comment('Para hacer match con el catálogo de SQLserver');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cat_localidades', function (Blueprint $table) {
      $table->dropColumn('cve_localidad');
    });
  }
}
