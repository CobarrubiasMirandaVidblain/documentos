<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCatinstitucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_instituciones', function (Blueprint $table) {
            $table->foreign('entidad_id')->references('id')->on('cat_entidades');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('tipoinstitucion_id')->references('id')->on('cat_tiposinstitucion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_instituciones', function (Blueprint $table) {
            $table->dropForeign(['entidad_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['tipoinstitucion_id']);
            
            $table->dropIndex('cat_instituciones_entidad_id_foreign');
            $table->dropIndex('cat_instituciones_usuario_id_foreign');   
            $table->dropIndex('cat_instituciones_tipoinstitucion_id_foreign');  
        });
    }
}
