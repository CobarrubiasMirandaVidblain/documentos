<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los diputados locales.
    ALCANCE: Eventos
*/

class CreateEvenAutoridadesdipuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('even_autoridadesdipu', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('fichainfo_id');
            $table->unsignedInteger('diputadolocal_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('even_autoridadesdipu');
    }
}
