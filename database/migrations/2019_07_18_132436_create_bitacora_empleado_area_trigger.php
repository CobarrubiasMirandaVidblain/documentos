<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitacoraEmpleadoAreaTrigger extends Migration
{
    /**
     * PETICIÓN: Anthony
     * MOTIVO: Trigger que se ejecutará para guardar cambios en el área de un empleado
     * ALCANCE: GENERAL
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::connection()->getpdo()->exec("
        CREATE TRIGGER insert_bitacora_empleado_area
        BEFORE UPDATE
           ON empleados FOR EACH ROW
        BEGIN
        IF NEW.area_id <> OLD.area_id THEN 
            INSERT INTO bitacora_empleado_area
           (empleado_id,area_id,usuario_id,created_at)
          VALUES
           (OLD.id,OLD.area_id,OLD.usuario_id,CURRENT_TIMESTAMP());
        END IF;
        END;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::connection()->getpdo()->exec("DROP TRIGGER insert_bitacora_empleado_area;");
    }
}
