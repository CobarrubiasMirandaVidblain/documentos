<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar la localidad
  ALCANCE: DIF te lleva
*/

class AlterUniqueFolioPagoToDtllPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_pagos', function (Blueprint $table) {
      $table->unique(['folioPago','deleted_at']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_pagos', function (Blueprint $table) {
      $table->dropUnique(['folioPago','deleted_at']);
    });
  }
}
