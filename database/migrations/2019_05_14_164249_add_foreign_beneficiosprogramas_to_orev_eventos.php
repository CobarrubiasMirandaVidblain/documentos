<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: se relaciona con area y beneficio programa
  ALCANCE: modulo de orgeventos
*/

class AddForeignBeneficiosprogramasToOrevEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            $table->foreign('area_id')->references('id')->on('cat_areas');
            $table->foreign('beneficiosprogramas_id')->references('id')->on('beneficiosprogramas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            $table->dropForeign(['area_id']);
            $table->dropForeign(['beneficiosprogramas_id']);
            $table->dropIndex('orev_eventos_area_id_foreign');
            $table->dropIndex('orev_eventos_beneficiosprogramas_id_foreign');
        });
    }
}
