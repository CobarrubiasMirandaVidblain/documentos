<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimSujetosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_sujetos', function (Blueprint $table) {
      $table->foreign('alimentario_id')->references('id')->on('alim_alimentarios');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_sujetos', function (Blueprint $table) {
      $table->dropForeign(['alimentario_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_sujetos_alimentario_id_foreign');
      $table->dropIndex('alim_sujetos_usuario_id_foreign');
    });
  }
}
