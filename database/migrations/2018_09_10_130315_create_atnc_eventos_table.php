<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtncEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atnc_eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('localidad',100)->nullable();
            $table->integer('localidad_id')->unsigned()->nullable();
            $table->integer('municipio_id')->unsigned();
            $table->date('fecha');
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atnc_eventos');
    }
}
