<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllRegistrokmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dtll_registrokm', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('controlruta_id');
            $table->decimal('km', 11, 2);
            $table->datetime('fecha');
            $table->string('foto');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_registrokm');
    }
}
