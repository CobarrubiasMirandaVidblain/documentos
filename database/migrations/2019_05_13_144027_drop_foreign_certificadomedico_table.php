<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se elimina foreign key para tabla cred_certificadomedico
  ALCANCE: Base Principal
*/


class DropForeignCertificadomedicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cred_certificadomedico', function(Blueprint $table){
        
        //$table->dropForeign('cred_certificadomedico_institucion_rubro_id_foreign');
        //$table->dropColumn('institucion_rubro_id');
        $table->dropForeign(['institucion_rubro_id']);
        //$table->dropColumn('institucion_rubro_id');
        $table->dropIndex('cred_certificadomedico_institucion_rubro_id_foreign');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cred_certificadomedico', function(Blueprint $table){
        //$table->unsignedInteger('institucion_rubro_id')->after('ruta_imagen');
        $table->foreign('institucion_rubro_id')->references('id')->on('instituciones_rubros')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }
}
