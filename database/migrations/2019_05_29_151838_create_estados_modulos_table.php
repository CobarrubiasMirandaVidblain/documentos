<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


/*
    * PETICIÓN: Beto.
    * MOTIVO: Tabla para guardar la relación de los modulos con sus estatus que tendran dentro del sistema
    * ALCANCE: Fondo Rotatorio
*/

class CreateEstadosModulosTable extends Migration
{
    /**
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_modulos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('estado_id');
            $table->unsignedInteger('modulo_id');
            $table->tinyInteger('orden')->unsigned();
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['estado_id', 'modulo_id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_modulos');
    }
}
