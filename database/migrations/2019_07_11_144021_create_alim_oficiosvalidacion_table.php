<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para ingresar los datos de oficios validados por alimentarios
    ALCANCE: Alimentarios
*/

class CreateAlimOficiosvalidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_oficiosvalidacion', function (Blueprint $table) {
            $table->increments('id');

            $table->smallInteger('num_oficio');
            $table->unsignedInteger('ejercicio_id');
            $table->date('fecha_oficio');
            $table->unsignedInteger('programa_id');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_oficiosvalidacion');
    }
}
