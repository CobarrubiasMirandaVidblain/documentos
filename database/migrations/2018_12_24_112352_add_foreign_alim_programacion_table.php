<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimProgramacionTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_programacion', function (Blueprint $table) {
      $table->foreign('anios_programa_id')->references('id')->on('anios_programas');
      $table->foreign('estado_id')->references('id')->on('alim_cat_estados');
      $table->foreign('financiamiento_id')->references('id')->on('alim_cat_financiamientos');
      $table->foreign('solicitud_id')->references('id')->on('solicitudes');
      $table->foreign('alimentario_id')->references('id')->on('alim_alimentarios');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_programacion', function (Blueprint $table) {
      $table->dropForeign(['anios_programa_id']);
      $table->dropForeign(['estado_id']);
      $table->dropForeign(['financiamiento_id']);
      $table->dropForeign(['solicitud_id']);
      $table->dropForeign(['alimentario_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_programacion_anios_programa_id_foreign');
      $table->dropIndex('alim_programacion_estado_id_foreign');
      $table->dropIndex('alim_programacion_financiamiento_id_foreign');
      $table->dropIndex('alim_programacion_solicitud_id_foreign');
      $table->dropIndex('alim_programacion_alimentario_id_foreign');
      $table->dropIndex('alim_programacion_usuario_id_foreign');
    });
  }
}
