<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Llaves foráneas para tabla de siaf_num_cuentas.
    ALCANCE: SIAF
*/

class AddForeignSiafNumCuentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('siaf_num_cuentas', function (Blueprint $table) {
				$table->foreign('banco_id')->references('id')->on('cat_bancos');	
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('siaf_num_cuentas', function (Blueprint $table) {
				$table->dropForeign(['banco_id']);
				$table->dropIndex('siaf_num_cuentas_banco_id_foreign');
			});
    }
}
