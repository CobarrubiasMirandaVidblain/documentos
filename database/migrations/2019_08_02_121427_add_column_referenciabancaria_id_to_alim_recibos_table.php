<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReferenciabancariaIdToAlimRecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->unsignedInteger('referenciabancaria_id')->nullable()->after('programacion_requisicion_id');
        });

        Schema::table('alim_recibos', function(Blueprint $table){
            $table->foreign('referenciabancaria_id')->references('id')->on('alim_referenciasbancarias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_recibos', function (Blueprint $table) {
            $table->dropForeign(['referenciabancaria_id']);
            $table->dropIndex('alim_recibos_referenciabancaria_id_foreign');
        });

        Schema::table('alim_recibos', function (Blueprint $table){
            $table->dropColumn('referenciabancaria_id');
        });
    }
}
