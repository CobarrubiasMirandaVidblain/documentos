<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Llaves foráneas para tabla de siaf_empleados_tarjetas.
    ALCANCE: SIAF
*/

class AddForeignSiafEmpleadoTarjeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_empleados_tarjeta', function (Blueprint $table) {
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->foreign('banco_id')->references('id')->on('cat_bancos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_empleados_tarjeta', function (Blueprint $table) {
            $table->dropForeign(['empleado_id']);
            $table->dropForeign(['banco_id']);
            $table->dropIndex('siaf_empleados_tarjeta_empleado_id_foreign');
            $table->dropIndex('siaf_empleados_tarjeta_banco_id_foreign');
        });
    }
}
