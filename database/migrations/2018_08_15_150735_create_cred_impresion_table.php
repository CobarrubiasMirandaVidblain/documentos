<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCredImpresionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cred_impresion', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('personas_tipos_id')->unsigned();
        $table->integer('tipo_impresion_id')->unsigned();
        $table->date('fecha_impresion');
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cred_impresion');
    }
}
