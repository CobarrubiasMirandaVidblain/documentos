<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPresentacionProductoIdToRecmOrdenCompraProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_orden_compra_productos', function (Blueprint $table) {
            $table->renameColumn('producto_requisicion_id', 'presentacion_producto_id');
            $table->dropForeign(['producto_requisicion_id']);
            $table->dropIndex('recm_orden_compra_productos_producto_requisicion_id_foreign');
            $table->foreign('presentacion_producto_id')->references('id')->on('recm_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_orden_compra_productos', function (Blueprint $table) {
            $table->renameColumn('presentacion_producto_id', 'producto_requisicion_id');
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropIndex('recm_orden_compra_productos_presentacion_producto_id_foreign');
            $table->foreign('producto_requisicion_id')->references('id')->on('recm_producto_requisicions')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }
}
