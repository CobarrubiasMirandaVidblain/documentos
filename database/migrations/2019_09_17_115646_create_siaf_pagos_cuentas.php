<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
	PETICIÓN: Manuel.
	MOTIVO: Para relacionar los pagos con la cuenta que se pago
	ALCANCE: siaf
*/

class CreateSiafPagosCuentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::create('siaf_pagos_cuentas', function (Blueprint $table) {
				$table->increments('id');
				$table->date('fecha_pagado');
				$table->unsignedInteger('pago_id');
				$table->unsignedInteger('numcuenta_id');
				$table->timestamps();
				$table->softDeletes();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::dropIfExists('siaf_pagos_cuentas');
    }
}
