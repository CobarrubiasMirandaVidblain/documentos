<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCorreccionesSolicitudesmodifdatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('solicitudesmodifdatos', function(Blueprint $table){
        $table->dropForeign(['status_solicitud_id']);
        // $table->dropIndex('solicitudesmodifdatos_status_solicitud_id_foreign');
      });

      Schema::rename('solicitudesmodifdatos', 'cred_solicitudesmodifdatos');
      Schema::rename('cat_statussolicitudes', 'cred_cat_statussolicitudes');

      Schema::table('cred_solicitudesmodifdatos', function(Blueprint $table){
        $table->integer('usuario_id')->unsigned()->after('status_solicitud_id');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('status_solicitud_id')->references('id')->on('cred_cat_statussolicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
      });

      Schema::table('cred_impresion', function(Blueprint $table){
        $table->integer('usuario_id')->unsigned()->after('fecha_impresion');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });

      Schema::table('cred_certificadomedico', function(Blueprint $table){
        $table->integer('usuario_id')->unsigned()->after('institucion_rubro_id');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cred_certificadomedico', function(Blueprint $table){
        $table->dropForeign(['usuario_id']);
        $table->dropIndex('cred_certificadomedico_usuario_id_foreign');
        $table->dropColumn('usuario_id');
      });

      Schema::table('cred_impresion', function(Blueprint $table){
        $table->dropForeign(['usuario_id']);
        $table->dropIndex('cred_impresion_usuario_id_foreign');
        $table->dropColumn('usuario_id');
      });


      Schema::table('cred_solicitudesmodifdatos', function(Blueprint $table){
        $table->dropForeign(['status_solicitud_id']);
        $table->dropIndex('cred_solicitudesmodifdatos_status_solicitud_id_foreign');

        $table->dropForeign(['usuario_id']);
        $table->dropIndex('cred_solicitudesmodifdatos_usuario_id_foreign');
        $table->dropColumn('usuario_id');
      });

      Schema::rename('cred_cat_statussolicitudes', 'cat_statussolicitudes');
      Schema::rename('cred_solicitudesmodifdatos', 'solicitudesmodifdatos');

      Schema::table('solicitudesmodifdatos', function(Blueprint $table){
        $table->foreign('status_solicitud_id')->references('id')->on('cred_impresion')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }
}
