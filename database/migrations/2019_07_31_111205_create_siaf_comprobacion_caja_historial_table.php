<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para registrar el historico de estatus de la comprobacion
    ALCANCE: BancaDIF
*/

class CreateSiafComprobacionCajaHistorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_comprobacion_caja_historial', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('comprobacioncaja_id');
            $table->unsignedInteger('estado_modulo_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_comprobacion_caja_historial');
    }
}
