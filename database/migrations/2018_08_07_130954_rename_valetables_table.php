<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameValetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::rename('vale_asistencias', 'z_vale_asistencias');
      Schema::rename('vale_evaluaciones', 'z_vale_evaluaciones');
      Schema::rename('vale_grupos', 'z_vale_grupos');
      Schema::rename('vale_grupos_alumnos', 'z_vale_grupos_alumnos');
      Schema::rename('vale_gruposdias', 'z_vale_gruposdias');
      Schema::rename('vale_gruposdisponibles', 'z_vale_gruposdisponibles');
      Schema::rename('vale_observaciones', 'z_vale_observaciones');
      Schema::rename('vale_proefesoresgrupos', 'z_vale_profesoresgrupos');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::rename('z_vale_asistencias', 'vale_asistencias');
      Schema::rename('z_vale_evaluaciones', 'vale_evaluaciones');
      Schema::rename('z_vale_grupos', 'vale_grupos');
      Schema::rename('z_vale_grupos_alumnos', 'vale_grupos_alumnos');
      Schema::rename('z_vale_gruposdias', 'vale_gruposdias');
      Schema::rename('z_vale_gruposdisponibles', 'vale_gruposdisponibles');
      Schema::rename('z_vale_observaciones', 'vale_observaciones');
      Schema::rename('z_vale_profesoresgrupos', 'vale_proefesoresgrupos');
    }
}
