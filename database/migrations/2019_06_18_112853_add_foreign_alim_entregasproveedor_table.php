<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimEntregasproveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->foreign('recibo_id')->references('id')->on('alim_recibos');
            $table->foreign('recibe_comite_id')->references('id')->on('alim_comites');
            $table->foreign('datocarga_id')->references('id')->on('alim_datoscarga');
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropForeign(['recibo_id']);
            $table->dropForeign(['recibe_comite_id']);
            $table->dropForeign(['datocarga_id']);
            $table->dropForeign(['estado_programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_entregasproveedor_recibo_id_foreign');
            $table->dropIndex('alim_entregasproveedor_recibe_comite_id_foreign');
            $table->dropIndex('alim_entregasproveedor_datocarga_id_foreign');
            $table->dropIndex('alim_entregasproveedor_estado_programa_id_foreign');
            $table->dropIndex('alim_entregasproveedor_usuario_id_foreign');
        });
    }
}
