<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel 
  MOTIVO: Se presentó en la necesidad de desarrollo para el sistema Lari_DIF
  ALCANCE: Para Lari_DIF
*/

class AlterAdduniqueToLariUsuariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('lari_usuarios', function (Blueprint $table) {
      $table->unique(['usuario','deleted_at']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('lari_usuarios', function (Blueprint $table) {
      $table->dropUnique(['usuario','deleted_at']);
    });
  }
}
