<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Se borra el campo num_acta_nac porque se pasó a la tabla personas
  ALCANCE: General
*/

class AlterDropColumnNumActaNacToAlimBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $registro = DB::table('alim_beneficiarios')->whereNotNull('num_acta_nac');
        if ($registro) {
            DB::statement('UPDATE alim_beneficiarios b, personas_programas pp, personas p
                        SET p.num_acta_nacimiento=b.num_acta_nac
                        WHERE 
                                b.persona_programa_id=pp.id AND pp.persona_id=p.id
                                AND b.num_acta_nac IS NOT NULL AND p.num_acta_nacimiento IS NULL'
                        );
        }

        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->dropColumn('num_acta_nac');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->string('num_acta_nac',20)->nullable()->after('persona_programa_id');
        });

        $registro = DB::table('personas')->whereNotNull('num_acta_nacimiento');
        if ($registro) {
            DB::statement('UPDATE alim_beneficiarios b, personas_programas pp, personas p
                        SET b.num_acta_nac=p.num_acta_nacimiento
                        WHERE 
                            b.persona_programa_id=pp.id AND pp.persona_id=p.id
                            AND b.num_acta_nac IS NULL AND p.num_acta_nacimiento IS NOT NULL'
                        );
        }
    }
}
