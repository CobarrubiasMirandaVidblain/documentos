<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class CreateSiafHistorialAreasEnlacesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('siaf_historial_areas_enlaces', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('fondorotatorioarea_id');
      $table->unsignedInteger('enlacefinanciero_id');
      $table->date('fecha_asignacion');
      $table->date('fecha_baja')->nullable();
      $table->tinyInteger('activo');
      $table->unsignedInteger('empleado_id');
      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('siaf_historial_areas_enlaces');
  }
}
