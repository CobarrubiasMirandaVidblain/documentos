<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToUsuariosRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuarios_roles', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('rol_id')->references('id')->on('cat_roles')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('modulo_id')->references('id')->on('cat_modulos')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('responsable')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuarios_roles', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['rol_id']);
            $table->dropForeign(['modulo_id']);
            $table->dropForeign(['responsable']);
        });
    }
}
