<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCitasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citas', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citas', function (Blueprint $table) {
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["empleado_id"]);
            $table->dropForeign(["programa_id"]);
        });
    }
}
