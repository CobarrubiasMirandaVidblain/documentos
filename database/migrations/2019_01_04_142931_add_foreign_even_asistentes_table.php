<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AddForeignEvenAsistentesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_asistentes', function (Blueprint $table) {
      $table->foreign('fichainfo_id')->references('id')->on('even_fichasinfo')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('cargo_id')->references('id')->on('cat_cargos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_asistentes', function (Blueprint $table) {
      $table->dropForeign(['fichainfo_id']);
      $table->dropForeign(['persona_id']);
      $table->dropForeign(['cargo_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('even_asistentes_fichainfo_id_foreign');
      $table->dropIndex('even_asistentes_persona_id_foreign');
      $table->dropIndex('even_asistentes_cargo_id_foreign');
      $table->dropIndex('even_asistentes_usuario_id_foreign');
    });
  }
}
