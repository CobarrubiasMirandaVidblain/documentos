<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Llaves foráneas para tabla de siaf_pagos_cuentas.
    ALCANCE: SIAF
*/

class AddForeignSiafPagosCuentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('siaf_pagos_cuentas', function (Blueprint $table) {
				$table->foreign('pago_id')->references('id')->on('siaf_pagos');	
				$table->foreign('numcuenta_id')->references('id')->on('siaf_num_cuentas');	
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('siaf_pagos_cuentas', function (Blueprint $table) {
				$table->dropForeign(['pago_id']);
				$table->dropForeign(['numcuenta_id']);
				$table->dropIndex('siaf_pagos_cuentas_pago_id_foreign');
				$table->dropIndex('siaf_pagos_cuentas_numcuenta_id_foreign');
			});
    }
}
