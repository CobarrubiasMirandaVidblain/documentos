<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenFichasinfoTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_fichasinfo', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('evento_id');
      $table->string('numero_ficha',20)->nullable();
      $table->unsignedInteger('vestimenta_id')->nullable();
      $table->smallInteger('aforo')->unsigned()->nullable();
      $table->unsignedInteger('tipoevento_id')->nullable();
      $table->tinyInteger('temperatura')->unsigned()->nullable();
      $table->tinyInteger('precipitacion')->unsigned()->nullable();
      $table->tinyInteger('humedad')->unsigned()->nullable();
      $table->tinyInteger('viento')->unsigned()->nullable();
      $table->unsignedInteger('persona_convoca_id');
      $table->unsignedInteger('cargo_persona_convoca_id');
      $table->unsignedInteger('persona_preside_id');
      $table->unsignedInteger('cargo_persona_preside_id');
      $table->string('objetivo',100);
      $table->unsignedInteger('presidenta_participacion_id');
      $table->unsignedInteger('director_participacion_id')->nullable();

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_fichasinfo');
  }
}
