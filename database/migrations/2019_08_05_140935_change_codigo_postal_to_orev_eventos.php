<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCodigoPostalToOrevEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            $table->unsignedInteger('codigoPostal')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orev_eventos', function (Blueprint $table) {
            // $table->unsignedTinyInteger('codigoPostal')->change();
        });
    }
}
