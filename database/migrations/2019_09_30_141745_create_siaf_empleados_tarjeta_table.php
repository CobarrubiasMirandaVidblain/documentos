<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Tabla para guardar las tarjetas del banco de los empleados.
    ALCANCE: SIAF
*/

class CreateSiafEmpleadosTarjetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_empleados_tarjeta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_tarjeta',16);
            $table->unsignedInteger('banco_id');
            $table->unsignedInteger('empleado_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_empleados_tarjeta');
    }
}
