<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de caravanas
    ALCANCE: Itinerantes
*/

class AddForeignItnCaravanaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itn_caravana', function (Blueprint $table) {
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');	
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');	
            $table->foreign('usuario_id')->references('id')->on('usuarios');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itn_caravana', function (Blueprint $table) {
            $table->dropForeign(['localidad_id']);
            $table->dropForeign(['municipio_id']);
            $table->dropForeign(['usuario_id']);
						$table->dropIndex('itn_caravana_localidad_id_foreign');
						$table->dropIndex('itn_caravana_municipio_id_foreign');
						$table->dropIndex('itn_caravana_usuario_id_foreign');
        });
    }
}
