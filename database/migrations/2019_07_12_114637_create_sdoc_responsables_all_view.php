<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Anthony.
    MOTIVO: Para crear vista de responsables de areas internas e instituciones
    ALCANCE: SICODOC
*/

class CreateSdocResponsablesAllView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW sdoc_responsables_all AS 
        SELECT * FROM
            (	
            SELECT
                r.id,
                r.tipo,
                i.id as tabla_id,
                i.nombre as area,
                i.encargado as responsable,
                i.cargoencargado as cargo
            FROM 
                sdoc_responsables r
            INNER JOIN 
                cat_instituciones i on (r.instituciones_responsables_id = i.id)
            WHERE
                i.deleted_at is null) as ri
         UNION ALL
            (SELECT
                r.id,
                r.tipo,
                a.id as tabla_id,
                a.nombre as area, 
                CONCAT_WS(' ',p.nombre, p.primer_apellido, p.segundo_apellido) as responsable,
                cn.nivel as cargo
            FROM
                sdoc_responsables r
                INNER JOIN areas_responsables ar ON r.areas_responsables_id =ar.id
                INNER JOIN cat_areas a ON ar.area_id = a.id
                INNER JOIN empleados e ON ar.empleado_id = e.id
                INNER JOIN personas p ON e.persona_id = p.id
                INNER JOIN cat_niveles cn ON e.nivel_id=cn.id
            WHERE
                ar.deleted_at is null)
            order by id   
            "
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS sdoc_responsables_all");
    }
}
