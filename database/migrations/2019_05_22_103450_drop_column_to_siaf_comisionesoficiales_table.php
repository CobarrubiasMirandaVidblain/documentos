<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnToSiafComisionesoficialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->dropForeign(['origen_municipio_id']);
            $table->dropForeign(['destino_municipio_id']);
            $table->dropForeign(['cuota_id']);
            $table->dropForeign(['localidad_id']);
            $table->dropIndex('siaf_comisionesoficiales_origen_municipio_id_foreign');  
            $table->dropIndex('siaf_comisionesoficiales_destino_municipio_id_foreign');  
            $table->dropIndex('siaf_comisionesoficiales_cuota_id_foreign');
            $table->dropIndex('siaf_comisionesoficiales_localidad_id_foreign');
            $table->dropColumn('origen_municipio_id');
            $table->dropColumn('destino_municipio_id');
            $table->dropColumn('cuota_id');
            $table->dropColumn('localidad_id');
            $table->dropColumn('localidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->unsignedInteger('origen_municipio_id');
            $table->unsignedInteger('destino_municipio_id');
            $table->unsignedInteger('cuota_id');
            $table->unsignedInteger('localidad_id');
            $table->string('localidad');
        });
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->foreign('origen_municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('destino_municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('cuota_id')->references('id')->on('siaf_tabuladorviaticos');
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
        });
    }
}
