<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SICODOC
  ALCANCE: Para el sistema SICODOC
*/

class CreateSdocCatEstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_cat_estatus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('color')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['nombre','color']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_cat_estatus');
    }
}
