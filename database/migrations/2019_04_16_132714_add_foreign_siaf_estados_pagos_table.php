<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafEstadosPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_estados_pagos', function (Blueprint $table) {
      $table->foreign('pago_id')->references('id')->on('siaf_pagos');
      $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_estados_pagos', function (Blueprint $table) {
      $table->dropForeign(['pago_id']);
      $table->dropForeign(['estado_programa_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_estados_pagos_pago_id_foreign');
      $table->dropIndex('siaf_estados_pagos_estado_programa_id_foreign');
      $table->dropIndex('siaf_estados_pagos_usuario_id_foreign');
    });
  }
}
