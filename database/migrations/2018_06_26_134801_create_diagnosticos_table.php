<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosticos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('consulta_id')->unsigned();
            $table->boolean('primeravezpadecimiento');
            $table->boolean('primeravezanual');
            $table->string('diagnostico');
            $table->integer('cie10_id')->unsigned();
            $table->integer('discapacidad_id')->unsigned()->nullable();
            $table->integer('suive_id')->unsigned();
            $table->integer('clasificacion_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosticos');
    }
}
