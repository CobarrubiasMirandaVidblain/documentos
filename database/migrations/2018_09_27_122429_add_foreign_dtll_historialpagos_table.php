<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDtllHistorialpagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('dtll_historialpagos', function(Blueprint $table){
        $table->foreign('pasajero_id')->references('id')->on('dtll_pasajeros')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('dtll_historialpagos', function(Blueprint $table){
        $table->dropForeign(['pasajero_id']);
        $table->dropForeign(['usuario_id']);
      });
    }
}
