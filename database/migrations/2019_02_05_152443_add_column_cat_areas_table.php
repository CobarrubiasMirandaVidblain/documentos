<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar siglas de nombre de áreas
  ALCANCE: General
*/

class AddColumnCatAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_areas', function (Blueprint $table) {
            $table->string('siglas')->nullable()->after('padre_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_areas',function(Blueprint $table){
            $table->dropColumn('siglas');
        });
    }
}
