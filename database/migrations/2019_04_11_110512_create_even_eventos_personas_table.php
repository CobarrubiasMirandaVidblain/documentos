<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Se agrega tabla para asignar las personas a un evento
  ALCANCE: Para el sistema de eventos
*/

class CreateEvenEventosPersonasTable extends Migration
{
  /**
    * Run the migrations.
    *
    * @return void
    */
  public function up()
  {
    Schema::create('even_eventos_personas', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('evento_id');
      $table->unsignedInteger('persona_id')->nullable();
      $table->string('nombre',30)->nullable();
      $table->string('primer_apellido',40)->nullable();
      $table->string('segundo_apellido',40)->nullable();
      $table->string('correo',100);

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
    * Reverse the migrations.
    *
    * @return void
    */
  public function down()
  {
    Schema::dropIfExists('even_eventos_personas');
  }
}
