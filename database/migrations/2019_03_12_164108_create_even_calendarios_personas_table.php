<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos que requiere la api de google calendar
  ALCANCE: Para el sistema de eventos
*/

class CreateEvenCalendariosPersonasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_calendarios_personas', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('calendario_id');
      $table->unsignedInteger('persona_id');
      $table->string('kind',50);
      $table->string('etag',50);
      $table->string('ruleid',150);
      $table->string('scope_type',50);
      $table->string('scope_value',150);
      $table->string('role',50);
      $table->string('correo',100);
      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_calendarios_personas');
  }
}
