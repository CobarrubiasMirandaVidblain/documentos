<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: Para ingresar la información de beneficiarios de leche
    ALCANCE: Alimentarios
*/

class CreateAlimLecheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_leche', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('alimentario_id');
            $table->unsignedInteger('caic_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_leche');
    }
}
