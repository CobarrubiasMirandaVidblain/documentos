<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class CreateMopiEntradasProductosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('mopi_entradas_productos', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('cantidad');
      $table->float('precio');
      $table->float('subtotal');
      $table->date('fecha_recepcion');
      $table->unsignedInteger('entrada_id');
      $table->unsignedInteger('presentacion_producto_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('mopi_entradas_productos');
  }
}
