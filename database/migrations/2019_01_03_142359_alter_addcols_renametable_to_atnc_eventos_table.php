<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AlterAddcolsRenametableToAtncEventosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    // Desamarramos la tabla atnc_eventos
    Schema::table('giraspreregis', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropIndex('giraspreregis_evento_id_foreign');
    });

    Schema::table('atnc_eventos_solicitudes', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropIndex('atnc_eventos_solicitudes_evento_id_foreign');
    });

    Schema::table('areas_eventos_empleados', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropForeign(['empleado_id']);
      $table->dropForeign(['area_evento_id']);
      $table->dropIndex('areas_eventos_empleados_usuario_id_foreign');
      $table->dropIndex('areas_eventos_empleados_empleado_id_foreign');
      $table->dropIndex('areas_eventos_empleados_area_evento_id_foreign');
    });

    Schema::table('areas_eventos', function (Blueprint $table) {
      $table->dropForeign(['area_id']);
      $table->dropForeign(['evento_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('areas_eventos_area_id_foreign');
      $table->dropIndex('areas_eventos_evento_id_foreign');
      $table->dropIndex('areas_eventos_usuario_id_foreign');
    });

    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->dropForeign(['gira_id']);
      $table->dropIndex('atnc_eventos_gira_id_foreign');
    });


    // Renombramos la tabla atnc_eventos a eventos
    Schema::rename('atnc_eventos', 'even_eventos');
    Schema::rename('areas_eventos', 'even_areas_eventos');
    Schema::rename('areas_eventos_empleados', 'even_areas_eventos_empleados');


    // Volvemos a amarrar la tabla eventos

    Schema::table('giraspreregis', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('even_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('atnc_eventos_solicitudes', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('even_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('even_areas_eventos_empleados', function (Blueprint $table) {
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('area_evento_id')->references('id')->on('even_areas_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('even_areas_eventos', function (Blueprint $table) {
      $table->foreign('area_id')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('evento_id')->references('id')->on('even_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('even_eventos', function (Blueprint $table) {
      $table->foreign('gira_id')->references('id')->on('atnc_cat_giras')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('localidad_id')->references('id')->on('cat_localidades')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->unsignedInteger('usuario_id')->change();
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });


    // Agregamos las columnas a la tabla eventos

    Schema::table('even_eventos', function (Blueprint $table) {
      $table->unsignedInteger('tipoevento_id')->after('gira_id');
      $table->string('calle')->after('tipoevento_id')->nullable();
      $table->time('hora')->after('fecha')->nullable();
      $table->string('observacion')->after('hora')->nullable();
      $table->string('id_evento_google')->after('observacion');

      $table->foreign('tipoevento_id')->references('id')->on('even_cat_tiposevento')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    // Quitamos las columnas de la tabla eventos
    
    Schema::table('even_eventos', function (Blueprint $table) {
      $table->dropForeign(['tipoevento_id']);
      $table->dropIndex('even_eventos_tipoevento_id_foreign');

      $table->dropColumn('tipoevento_id');
      $table->dropColumn('calle');
      $table->dropColumn('hora');
      $table->dropColumn('observacion');
      $table->dropColumn('id_evento_google');
    });

    // Desamarramos las tablas
    Schema::table('giraspreregis', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropIndex('giraspreregis_evento_id_foreign');
    });

    Schema::table('atnc_eventos_solicitudes', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropIndex('atnc_eventos_solicitudes_evento_id_foreign');
    });

    Schema::table('even_areas_eventos_empleados', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropForeign(['empleado_id']);
      $table->dropForeign(['area_evento_id']);
      $table->dropIndex('even_areas_eventos_empleados_usuario_id_foreign');
      $table->dropIndex('even_areas_eventos_empleados_empleado_id_foreign');
      $table->dropIndex('even_areas_eventos_empleados_area_evento_id_foreign');
    });

    Schema::table('even_areas_eventos', function (Blueprint $table) {
      $table->dropForeign(['area_id']);
      $table->dropForeign(['evento_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('even_areas_eventos_area_id_foreign');
      $table->dropIndex('even_areas_eventos_evento_id_foreign');
      $table->dropIndex('even_areas_eventos_usuario_id_foreign');
    });

    Schema::table('even_eventos', function (Blueprint $table) {
      $table->dropForeign(['gira_id']);
      $table->dropForeign(['localidad_id']);
      $table->dropForeign(['municipio_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('even_eventos_gira_id_foreign');
      $table->dropIndex('even_eventos_localidad_id_foreign');
      $table->dropIndex('even_eventos_municipio_id_foreign');
      $table->dropIndex('even_eventos_usuario_id_foreign');
    });


    // Renombramos la tabla atnc_eventos a eventos
    Schema::rename('even_eventos', 'atnc_eventos');
    Schema::rename('even_areas_eventos', 'areas_eventos');
    Schema::rename('even_areas_eventos_empleados', 'areas_eventos_empleados');


    // Volvemos a amarrar la tabla eventos

    Schema::table('giraspreregis', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('atnc_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('atnc_eventos_solicitudes', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('atnc_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('areas_eventos_empleados', function (Blueprint $table) {
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('area_evento_id')->references('id')->on('areas_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('areas_eventos', function (Blueprint $table) {
      $table->foreign('area_id')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('evento_id')->references('id')->on('atnc_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });

    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->foreign('gira_id')->references('id')->on('atnc_cat_giras')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
