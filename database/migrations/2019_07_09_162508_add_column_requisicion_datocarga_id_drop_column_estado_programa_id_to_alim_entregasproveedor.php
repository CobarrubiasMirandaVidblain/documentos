<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para el ingreso de una entrega de proveedor
    ALCANCE: Alimentarios
*/

class AddColumnRequisicionDatocargaIdDropColumnEstadoProgramaIdToAlimEntregasproveedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Agregamos una columna foránea requisicion_datocarga_id
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->unsignedInteger('requisicion_datocarga_id')->after('id');
        });
        $registro = DB::table('alim_requisiciones_datoscarga')->first();
        if ($registro)
            DB::table('alim_entregasproveedor')->update(['requisicion_datocarga_id'=>$registro->id]);
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->foreign('requisicion_datocarga_id')->references('id')->on('alim_requisiciones_datoscarga');
        });
        

        //Borramos columna foránea estado_programa_id
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropForeign(['estado_programa_id']);
            $table->dropIndex('alim_entregasproveedor_estado_programa_id_foreign');
        });
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropColumn('estado_programa_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Borramos columna foránea requisicion_datocarga_id
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropForeign(['requisicion_datocarga_id']);
            $table->dropIndex('alim_entregasproveedor_requisicion_datocarga_id_foreign');
        });
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->dropColumn('requisicion_datocarga_id');
        });

        // Agregamos columna foranea estado_programa_id
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->unsignedInteger('estado_programa_id')->after('recibe_comite_id');
        });
        $registro = DB::table('estados_programas')->first();
        if ($registro)
            DB::table('alim_entregasproveedor')->update(['estado_programa_id'=>$registro->id]);
        Schema::table('alim_entregasproveedor', function (Blueprint $table) {
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
        });

        
    }
}
