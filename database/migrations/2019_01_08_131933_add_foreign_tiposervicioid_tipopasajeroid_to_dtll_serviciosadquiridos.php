<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class AddForeignTiposervicioidTipopasajeroidToDtllServiciosadquiridos extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_serviciosadquiridos', function (Blueprint $table) {
      $table->foreign('tiposervicio_id')->references('id')->on('dtll_cat_tiposservicio')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('tipopasajero_id')->references('id')->on('dtll_cat_tipospasajero')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    // Borramos manualmente las foreign key porque el nombre de columna tiene mayúsculas y  minúsculas
    DB::statement ('ALTER TABLE dtll_serviciosadquiridos DROP FOREIGN KEY dtll_serviciosadquiridos_tiposervicio_id_foreign'); 
    DB::statement ('ALTER TABLE dtll_serviciosadquiridos DROP FOREIGN KEY dtll_serviciosadquiridos_tipopasajero_id_foreign');
    // Borramos los índices
    Schema::table('dtll_serviciosadquiridos', function (Blueprint $table) {
      $table->dropIndex('dtll_serviciosadquiridos_tiposervicio_id_foreign');
      $table->dropIndex('dtll_serviciosadquiridos_tipopasajero_id_foreign');
    });
    
  }
}
