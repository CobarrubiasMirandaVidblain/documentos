<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmCatProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_cat_proveedors
        Schema::table('recm_cat_proveedors', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_cat_proveedors', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_cat_proveedors_usuario_id_foreign');
            
        });
    }
}
