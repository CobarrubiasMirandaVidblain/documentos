<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenActividadesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_actividades', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('fichainfo_id');
      $table->time('hora');
      $table->string('actividad');
      $table->smallInteger('tiempo');
      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_actividades');
  }
}
