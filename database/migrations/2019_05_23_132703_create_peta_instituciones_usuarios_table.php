<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class CreatePetaInstitucionesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peta_instituciones_usuarios', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('institucion_id');
            $table->unsignedInteger('access_usuario_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->unique(['institucion_id','access_usuario_id'],'peta_instit_usrs_institucion_id_access_usuario_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peta_instituciones_usuarios');
    }
}
