<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCollengthParadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('paradas', function(Blueprint $table){
        $table->string('latitud',20)->nullable()->change();
        $table->string('longitud',20)->nullable()->change();
        //$table->dropIndex('latitud');                                   /* Si marca error de can't drop index, comentar esta línea y ejecutar*/
        $table->dropUnique(['latitud','longitud']);                 /* Si marca error de can't drop index, descomentar esta línea y ejecutar*/
        $table->unique(['latitud','longitud','deleted_at']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('paradas', function(Blueprint $table){
        $table->dropUnique(['latitud','longitud','deleted_at']);
        $table->unique(['latitud','longitud']);
        $table->string('longitud',20)->nullable()->change();
        $table->string('latitud',20)->nullable()->change();
      });
    }
}
