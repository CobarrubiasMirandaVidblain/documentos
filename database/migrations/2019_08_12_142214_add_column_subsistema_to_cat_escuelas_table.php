<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para registar el subsistema de una escuela, por ejemplo si es cobao, cecyte, telesecundaria, etc.
    ALCANCE: General
*/

class AddColumnSubsistemaToCatEscuelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('cat_escuelas', function (Blueprint $table) {
					$table->unsignedInteger('subsistema_id')->after('turno_id')->default(1);
			});

			Schema::table('cat_escuelas', function (Blueprint $table) {
					$table->foreign('subsistema_id')->references('id')->on('cat_subsistemasescuela');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('cat_escuelas', function (Blueprint $table) {
				$table->dropForeign(['subsistema_id']);
				$table->dropIndex('cat_escuelas_subsistema_id_foreign');
				$table->dropColumn('subsistema_id');
			});
    }
}
