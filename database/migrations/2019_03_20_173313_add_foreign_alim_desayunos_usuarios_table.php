<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar los usuarios que tendrá una escuela del programa desayunos
  ALCANCE: Para el sistema de alimentarios
*/

class AddForeignAlimDesayunosUsuariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_desayunos_usuarios', function (Blueprint $table) {
      $table->foreign('usuario_id')->references('id')->on('usuarios');
      $table->foreign('desayuno_id')->references('id')->on('alim_desayunos');
      $table->foreign('usuariosis_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_desayunos_usuarios', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropForeign(['desayuno_id']);
      $table->dropForeign(['usuariosis_id']);
      $table->dropIndex('alim_desayunos_usuarios_usuario_id_foreign');
      $table->dropIndex('alim_desayunos_usuarios_desayuno_id_foreign');
      $table->dropIndex('alim_desayunos_usuarios_usuariosis_id_foreign');
    });
      
  }
}
