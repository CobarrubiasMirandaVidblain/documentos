<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimCatEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_cat_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('alim_cat_estados')->insert([
            ['nombre' => 'ALTA', 'created_at'=>NOW()],
            ['nombre' => 'ACTIVA', 'created_at'=>NOW()],
            ['nombre' => 'CANCELADA', 'created_at'=>NOW()],
            ['nombre' => 'BAJA', 'created_at'=>NOW()],
            ['nombre' => 'REVISION', 'created_at'=>NOW()],
            ['nombre' => 'ENVIADA', 'created_at'=>NOW()],
            ['nombre' => 'RECIBIDA', 'created_at'=>NOW()]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_cat_estados');
    }
}
