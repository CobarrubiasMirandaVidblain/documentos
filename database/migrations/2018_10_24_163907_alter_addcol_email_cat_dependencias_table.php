<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
PETICIÓN: Jerza 
MOTIVO: Lo solicitó Juan
*/

class AlterAddcolEmailCatDependenciasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('cat_dependencias', function (Blueprint $table) {
      $table->string('email',80)->after('telefono');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cat_dependencias', function (Blueprint $table) {
      $table->dropColumn('email');
    });
  }
}
