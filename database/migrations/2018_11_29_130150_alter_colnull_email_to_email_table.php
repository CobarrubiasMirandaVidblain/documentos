<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos
  ALCANCE: Para Atención Ciudadana
*/
class AlterColnullEmailToEmailTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('cat_dependencias', function (Blueprint $table) {
      $table->string('email',80)->nullable()->change();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cat_dependencias', function (Blueprint $table) {
      $table->string('email',80)->change();
    });
  }
}
