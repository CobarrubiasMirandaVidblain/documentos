<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCredImpresionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cred_impresion', function(Blueprint $table){
        $table->foreign('tipo_impresion_id')->references('id')->on('cred_tipoimpresion')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('personas_tipos_id')->references('id')->on('personas_tipos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cred_impresion', function(Blueprint $table){
        $table->dropForeign(['tipo_impresion_id']);
        $table->dropForeign(['personas_tipos_id']);
      });
    }
}
