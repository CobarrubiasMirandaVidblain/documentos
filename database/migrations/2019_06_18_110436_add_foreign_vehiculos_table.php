<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para tener catálogo de vehículos.
    ALCANCE: General
*/

class AddForeignVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vehiculos', function (Blueprint $table) {
            $table->foreign('marca_id')->references('id')->on('cat_marcasvehiculos');
            $table->foreign('tipo_id')->references('id')->on('cat_tiposvehiculos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehiculos', function (Blueprint $table) {
            $table->dropForeign(['marca_id']);
            $table->dropForeign(['tipo_id']);
            $table->dropIndex('vehiculos_tipo_id_foreign');
        });
    }
}
