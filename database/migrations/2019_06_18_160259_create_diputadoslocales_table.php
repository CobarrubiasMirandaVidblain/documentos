<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los diputados locales.
    ALCANCE: General
*/

class CreateDiputadoslocalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diputadoslocales', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre',30);
            $table->string('primer_apellido',40);
            $table->string('segundo_apellido',40)->nullable();
            $table->unsignedInteger('cargo_id');
            $table->unsignedInteger('partidopolitico_id');
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('cabecera',40);
            $table->string('distrito',20);
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diputadoslocales');
    }
}
