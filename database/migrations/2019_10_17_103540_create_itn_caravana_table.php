<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de caravanas
    ALCANCE: Itinerantes
*/

class CreateItnCaravanaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itn_caravana', function (Blueprint $table) {
            $table->increments('id');
						$table->date('fecha');
						$table->unsignedInteger('localidad_id');
						$table->unsignedInteger('municipio_id');

						$table->unsignedInteger('usuario_id');
            $table->timestamps();
						$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itn_caravana');
    }
}
