<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar la localidad
  ALCANCE: DIF te lleva
*/

class AlterAddcolLocalidadToDtllPasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('dtll_pasajeros', function (Blueprint $table) {
        $table->string('localidad')->nullable()->after('entidad_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('dtll_pasajeros', function (Blueprint $table) {
        $table->dropColumn('localidad');
      });
    }
}
