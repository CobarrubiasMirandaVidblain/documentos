<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDropaddColSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('solicitudes', function(Blueprint $table)
      {
        $table->dropForeign(['titular_id']);
        // $table->dropIndex('solicitudes_titular_id_foreign');
        $table->renameColumn('titular_id', 'persona_id');
        //$table->dropColumn('titular_id');
      });

      Schema::table('solicitudes', function(Blueprint $table)
      {
        //$table->integer('persona_id')->unsigned()->after('tiposremitente_id');
        $table->integer('persona_id')->unsigned()->change();
        $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
      });

      /*
      ALTER TABLE solicitudes DROP INDEX solicitudes_persona_id_foreign
      ALTER TABLE solicitudes MODIFY titular_id INT NOT NULL AFTER tiposremitente_id

      alter table solicitudes drop column persona_id
      select * from solicitudes 

      alter table solicitudes add column persona_id int after tiposremitente_id


      alter table solicitudes add constraint solicitudes_persona_id_foreign foreign key (persona_id) references personas (id) on delete CASCADE on update CASCADE
      ALTER TABLE Orders ADD CONSTRAINT FK_PersonOrder FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);   -- Especificando una foreign key con un alter con combre de reestricción
      */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('solicitudes', function(Blueprint $table){
        $table->dropForeign(['persona_id']);
        $table->dropIndex('solicitudes_persona_id_foreign');
        $table->renameColumn('persona_id', 'titular_id');
        $table->foreign('titular_id')->references('id')->on('titulares')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }
}
