<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRespuestascerradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('respuestascerradas', function (Blueprint $table) {
            $table->foreign('preguntas_persona_id')->references('id')->on('preguntas_personas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('opcion_id')->references('id')->on('preguntasopciones')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestascerradas', function (Blueprint $table) {
            $table->dropForeign(["preguntas_persona_id"]);
            $table->dropForeign(["opcion_id"]);
             
        });
    }
}
