<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function(Blueprint $table){
          $table->integer('pais_id')->unsigned()->after('rfc');
          $table->integer('entidad_id')->unsigned()->after('pais_id');
          $table->integer('usuario_id')->unsigned()->after('entidad_id');
          $table->dropColumn('migrante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pacientes', function (Blueprint $table){
          $table->dropColumn('pais_id');
          $table->dropColumn('entidad_id');
          $table->dropColumn('usuario_id');
          $table->boolean('migrante')->after('indigena');
        });
    }
}
