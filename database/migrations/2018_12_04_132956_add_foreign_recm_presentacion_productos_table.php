<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmPresentacionProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_presentacion_productos
        Schema::table('recm_presentacion_productos', function (Blueprint $table) {
            $table->foreign('presentacion_id')->references('id')->on('recm_cat_presentacions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('producto_id')->references('id')->on('recm_cat_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('partida_id')->references('id')->on('recm_cat_partidas')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_presentacion_productos', function (Blueprint $table) {
            $table->dropForeign(['presentacion_id']);
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['partida_id']);
            $table->dropIndex('recm_presentacion_productos_presentacion_id_foreign');
            $table->dropIndex('recm_presentacion_productos_producto_id_foreign');
            $table->dropIndex('recm_presentacion_productos_partida_id_foreign');
        });
    }
}
