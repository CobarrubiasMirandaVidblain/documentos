<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para ingresar datos de indicadores
    ALCANCE: General
*/

class AddForeignToIndicadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('indicadores', function (Blueprint $table) {
            $table->foreign('indicador_id')->references('id')->on('cat_indicadores');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('fuente_id')->references('id')->on('cat_fuentesindicadores');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('indicadores', function (Blueprint $table) {
            $table->dropForeign(['indicador_id']);
            $table->dropForeign(['municipio_id']);
            $table->dropForeign(['fuente_id']);
            $table->dropUnique('indicadores_indic_id_mun_id_per_ini_fuente_id_unique');
            $table->dropIndex('indicadores_municipio_id_foreign');
            $table->dropIndex('indicadores_fuente_id_foreign');
        });
    }
}
