<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para ingresar datos de las nuevas áreas 2019
  ALCANCE: Para todos los sistemas
*/

class CreateAreasProgramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('areas_programas', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('area_id');
      $table->unsignedInteger('programa_id');

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['area_id', 'programa_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('areas_programas');
  }
}
