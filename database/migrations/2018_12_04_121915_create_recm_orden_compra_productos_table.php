<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmOrdenCompraProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_orden_compra_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('producto_requisicion_id');
            $table->unsignedInteger('orden_compra_id');
            $table->unsignedInteger('usuario_id');
            $table->float('precio_unitario_real');
            $table->float('subtotal');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_orden_compra_productos');
    }
}
