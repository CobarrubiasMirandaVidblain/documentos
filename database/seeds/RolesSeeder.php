<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Rol;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

		Rol::create([
            'nombre' => 'SUPERADMINISTRADOR',
            'descripcion' => 'SUPERADMINISTRADOR',
            'valor' => 1,
            'modulo_id' => 1,
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

		Rol::create([
            'nombre' => 'ADMINISTRADOR',
            'descripcion' => 'ADMINISTRADOR',
            'valor' => 1,
            'modulo_id' => 1,
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);

		Rol::create([
            'nombre' => 'CAPTURISTA',
            'descripcion' => 'CAPTURISTA',
            'valor' => 1,
            'modulo_id' => 1,
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}
