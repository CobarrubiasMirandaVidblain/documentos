<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Distrito;

class DistritosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
        Distrito::create( [ 'nombre' => 'SILACAYOAPAM', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'HUAJUAPAN', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'COIXTLAHUACA', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'TEOTITLAN', 'region_id'=>1, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'CUICATLAN', 'region_id'=>1, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'TUXTEPEC', 'region_id'=>5, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'CHOAPAM', 'region_id'=>5, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'JUXTLAHUACA', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'TEPOSCOLULA', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'NOCHIXTLAN', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'ETLA', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'IXTLAN', 'region_id'=>6, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'VILLA ALTA', 'region_id'=>6, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'MIXE', 'region_id'=>6, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'PUTLA', 'region_id'=>7, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'TLAXIACO', 'region_id'=>4, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'ZAACHILA', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'ZIMATLAN', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'CENTRO', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'TLACOLULA', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'JAMILTEPEC', 'region_id'=>2, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'JUQUILA', 'region_id'=>2, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'SOLA DE VEGA', 'region_id'=>7, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'EJUTLA', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'OCOTLAN', 'region_id'=>8, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'MIAHUATLAN', 'region_id'=>7, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'YAUTEPEC', 'region_id'=>7, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'TEHUANTEPEC', 'region_id'=>3, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'JUCHITAN', 'region_id'=>3, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Distrito::create( [ 'nombre' => 'POCHUTLA', 'region_id'=>2, 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
    }
}
