<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Usuario;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	Model::unguard();
    	Usuario::create( [
        'usuario'=>'admin',
		'persona_id'=>1,
		'password'=>bcrypt('123456'),
		'email'=>'administrador@gmail.com',
		'vida_token'=>date('Y-m-d H:m:s'),
		'autorizado'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );	

    }
}
