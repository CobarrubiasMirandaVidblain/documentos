<?php

use Illuminate\Database\Seeder;

class TiposArchivoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sdoc_cat_tiposarchivo')->insert([
            ['nombre' => 'DOCUMENTO','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'ANEXO','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'ACUSE','created_at' => date("Y-m-d H:i:s")]            
        ]);
    }
}
