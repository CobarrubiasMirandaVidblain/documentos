<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Nivel;

class NivelesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
    	Nivel::create( [
        'clave'=>'2N0803',
        'nivel'=>'TECNICO 8',
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );

		Nivel::create( [
        'clave'=>'2E1207C',
        'nivel'=>'TECNICO 12C',
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );

		Nivel::create( [
        'clave'=>'2E1207B',
        'nivel'=>'TECNICO 12B',
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );

		Nivel::create( [
        'clave'=>'2M1109C',
        'nivel'=>'TECNICO 11C',
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );

    }
}
