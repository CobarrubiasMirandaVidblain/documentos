<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Modulo;

class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		Modulo::create([
            'nombre' => 'INTRANET',
            'descripcion' => 'INTRANET PARA EL SISTEMA DIF OAXACA',
            'activo' => 1,
            'prefix' => 'home',
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
		Modulo::create([
            'nombre' => 'BIENESTAR',
            'descripcion' => 'SISTEMA PARA EL CONTROL DE BENEFICIARIOS DEL PROGRAMA BIENESTAR',
            'activo' => 1,
            'prefix' => 'bienestar',
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
		Modulo::create([
            'nombre' => 'APOYOS FUNCIONALES',
            'descripcion' => 'MÓDULO PARA EL PROGRAMA DE APOYOS FUNCIONALES DE LA DIRECCIÓN DE ATENCIÓN A PERSONAS CON DISCAPACIDAD',
            'activo' => 1,
            'prefix' => 'afuncionales',
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
		Modulo::create([
            'nombre' => 'ATENCION CIUDADANA',
            'descripcion' => 'MÓDULO DE SOLICITUDES PARA ATENCIÓN CIUDADANA DEL SISTEMA DIF OAXACA',
            'activo' => 1,
            'prefix' => 'atnciudadana',
            'usuario_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
        ]);
    }
}
