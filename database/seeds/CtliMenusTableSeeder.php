<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CtliMenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m1 = DB::table('ctli_menus')->insert([
            'nombre' => 'DIRECCIÓN DE PLANEACIÓN Y MONITOREO',
            'alias' => 'PLANEACIÓN',
            'slug' => 'direccion-de-planeacion-y-monitoreo',
            'padre' => 0,
            'orden' => 0,
        ]);
        $m2 = DB::table('ctli_menus')->insert([
            'nombre' => 'UNIDAD DE INFORMÁTICA',
            'alias' => 'PLANEACIÓN',
            'slug' => 'unidad-de-informatica',
            'padre' => 1,
            'orden' => 0,
        ]);
        $m3 = DB::table('ctli_menus')->insert([
            'nombre' => 'DEPARTAMENTO DE DESARROLLO DE SISTEMAS',
            'alias' => 'SISTEMAS',
            'slug' => 'departamento-de-desarrollo-de-sistemas',
            'padre' => 2,
            'orden' => 0,
        ]);
    }
}
