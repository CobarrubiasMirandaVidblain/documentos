<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Area;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
    
    	Area::create( [
        'nombre'=>'UNIDAD DE INFORMATICA',
        'tipoarea_id'=>3,
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Area::create( [
        'nombre'=>'UNIDAD DE COMUNICACION SOCIAL',
        'tipoarea_id'=>3,
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Area::create( [
        'nombre'=>'UNIDAD DE GIRAS',
        'tipoarea_id'=>3,
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Area::create( [
        'nombre'=>'UNIDAD DE CENTROS ASISTENCIALES',
        'tipoarea_id'=>3,
        'usuario_id'=>1,
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );


  



        
		
		
		
    }
}
