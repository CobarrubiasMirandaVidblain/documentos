<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use App\Models\CatIndicador;

class CatIndicadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN CON CARENCIA DE ACCESO A LA SEGURIDA SOCIAL','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN EN SITUACIÓN DE POBREZA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN TOTAL POR MUNICIPIO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'NÚMERO DE PERSONAS QUE REPORTARON DIFICULTADES PARA REALIZAR ALGUNA ACTIVIDAD','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN DE 60 AÑOS O MÁS','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'MADRES ADOLESCENTES (MADRES MENORES DE 19 AÑOS)','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN EN HOGARES CUYO JEFE DE FAMILIA ES MUJER','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN DE 6 A 14 AÑOS QUE NO ASISTE A LA ESCUELA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'PBREZA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'PBREZA EXTREMA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'CARENCIA POR ACCESO A LA ALIMENTACIÓN','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		CatIndicador::create( [ 'nombre' =>'POBLACIÓN QUE SE CONSIDERA AFRODESCENDIENTE','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
    }
}
