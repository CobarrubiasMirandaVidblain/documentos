<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Empleado;

class EmpleadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();

        Empleado::create([
            'persona_id' => 1,
            'rfc' => 'ADMIN1234',
            'num_empleado' => '111',
            'segurosocial' => '11112223333',
            'nivel_id' => 1,
            'tiposempleado_id' => 1,
            'persona_emergencia' => 'Administrador del sistema',
            'telefono_emergencia' => '9515015071',
            'fechaingreso' => date('Y-m-d H:m:s'),
            'fechaingpuesto' => date('Y-m-d H:m:s'),
            'fechaingnombramiento' => date('Y-m-d H:m:s'),
            'usuario_id' => 1,
            'area_id' => 1,
            'validado' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => date('Y-m-d H:m:s')
		]);
    }
}
