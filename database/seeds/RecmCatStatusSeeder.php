<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\RecmCatEstatus;

class RecmCatStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RecmCatEstatus::create( [ 'estatus' => 'NUEVO', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-info'] );
        RecmCatEstatus::create( [ 'estatus' => 'ENVIADO A MATERIALES', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-primary' ] );
        RecmCatEstatus::create( [ 'estatus' => 'RECHAZADO', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-danger'] );
        RecmCatEstatus::create( [ 'estatus' => 'PROCESO DE ENTREGA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-warning' ] );
        RecmCatEstatus::create( [ 'estatus' => 'EN COMPRA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-warning'] );
        RecmCatEstatus::create( [ 'estatus' => 'ENTREGADO', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-default'] );
        RecmCatEstatus::create( [ 'estatus' => 'ENVIADO A REVISION', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'RECHAZADO COMPROBACIÓN', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'ENVIADO AFECTACION PRESUPUESTAL', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'SIN COBERTURA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'ENVIADO A CLC', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'PAGO DIRECTO A PROVEEDOR', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'CLC MODO TRADICIONAL', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'ENVIADO A BANCO PARA SPEI', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'ENVIADO PAGO A FINANZAS', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'SPEI ENVIADO', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
        RecmCatEstatus::create( [ 'estatus' => 'REGISTRO EN CONTABILIDA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), 'color'=>'label-success'] );
    }
}
