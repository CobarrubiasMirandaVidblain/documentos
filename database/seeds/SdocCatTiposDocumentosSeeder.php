<?php

use Illuminate\Database\Seeder;

class SdocCatTiposDocumentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sdoc_cat_tiposdocumento')->insert([
            //['nombre' => 'ACUERDO ADMINISTRATIVO','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'CALENDARIO','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'CARTA INVITACION','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'CIRCULAR','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'CONVOCATORIA','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'CORREO ELECTRONICO','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'CORREOS DE MEXICO','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'CURRICULUM','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'DICTAMEN TÉCNICO','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'ENTREGA INTERNA','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'FICHA INFORMATIVA','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'INVITACION','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'MEMORANDUM','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'NOMINACIÓN','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'OBSEQUIO','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'OFICIO','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'OFICIO COMPROBACIÓN','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'PAQUETERIA','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'SOBRE','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'TARJETA','created_at' => date("Y-m-d H:i:s")],
            ['nombre' => 'TARJETA INFORMATIVA','created_at' => date("Y-m-d H:i:s")],
            //['nombre' => 'VALE CARTA COMPROMISO','created_at' => date("Y-m-d H:i:s")]
        ]);
    }
}
