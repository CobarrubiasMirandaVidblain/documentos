<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tipoarea;

class TipoareasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
    
    	Tipoarea::create( [
        'tipo'=>'DIRECCIÓN',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tipoarea::create( [
        'tipo'=>'COORDINACIÓN',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tipoarea::create( [
        'tipo'=>'UNIDAD',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tipoarea::create( [
        'tipo'=>'DEPARTAMENTO',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tipoarea::create( [
        'tipo'=>'OFICINA',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
    }
}
