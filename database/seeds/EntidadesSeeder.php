<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Entidad;

class EntidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		Entidad::create( [ 'entidad' =>'AGUASCALIENTES','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'BAJA CALIFORNIA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'BAJA CALIFORNIA SUR','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'CAMPECHE','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'CHIAPAS','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'CHIHUAHUA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'CIUDAD DE MÉXICO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'COAHUILA DE ZARAGOZA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'COLIMA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'DURANGO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'GUANAJUATO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'GUERRERO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'HIDALGO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'JALISCO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'MÉXICO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'MICHOACÁN DE OCAMPO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'MORELOS','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'NAYARIT','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'NUEVO LEÓN','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'OAXACA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'PUEBLA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'QUERÉTARO DE ARTEAGA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'QUINTANA ROO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'SAN LUIS POTOSÍ','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'SINALOA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'SONORA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'TABASCO','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'TAMAULIPAS','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'TLAXCALA','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'VERACRUZ DE IGNACIO DE LA LLAVE','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'YUCATÁN','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Entidad::create( [ 'entidad' =>'ZACATECAS','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );

		
    }
}
