<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\UsuariosRol;


class UsuariosrolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	Model::unguard();
		UsuariosRol::create( [ 
			'usuario_id'=>1,
			'rol_id'=>1,
			'modulo_id'=>1,
			'responsable'=>1,
			'autorizado'=>1,
			'created_at'=> date('Y-m-d H:m:s'), 
			'updated_at'=>date('Y-m-d H:m:s'), 
		] );
    }
}
