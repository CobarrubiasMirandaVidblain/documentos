<?php
use \Excel as Excel;

use Modules\BancaDIF\Entities\Archivo;

class ArchivosBackupTableRefactor
{
    /**
     * Run the database refactoring.
     *
     * @return void
     */
    public function run()
    {
        $excel = Excel::create('FondoArchivosTable', function($excel) {

            $excel->sheet('Archivos', function($sheet) {

                $sheet->fromArray(Archivo::all());

            });

        })->store('csv');

    }
}